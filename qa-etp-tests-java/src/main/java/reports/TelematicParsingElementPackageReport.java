package reports;

import api.dev.AssemblerPackage;
import api.dev.MongoDevApiConnection;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.Block;
import com.mongodb.DBObject;
import cucumber.api.java.ContinueNextStepsFor;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

public class TelematicParsingElementPackageReport {
    AssemblerPackage aPackage = new AssemblerPackage();
    private HttpResponse<JsonNode> response;
    private LinkedHashSet<String> stack = new LinkedHashSet<>();
    String limit1 = "limit";
    String packages1 = "packages";

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры POST запроса: ")
    public void request(String arg0, String arg1, String arg2, String arg3) {
        aPackage.createAssemblerBody(arg0, arg1, Collections.singletonList(arg2), arg3);
        getUrl();
        getBody();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры POST запроса: ")
    public void request(String from, String to, List<String> strings) {
        aPackage.createAssemblerBody(from, to, strings, "5");
        getUrl();
        getBody();
    }

    @Attachment("Тело зарпоса: ")
    private Object getBody() {
        return aPackage.execute().getBody();
    }

    @Attachment("URL запроса: ")
    private String getUrl() {
        return aPackage.execute().getHttpRequest().getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ POST запроса: ")
    public void response() throws UnirestException {
        try {
            response = aPackage.execute().asJson();
            getResponseStatusText(response);
            getResponseBody(response);
        } catch (UnirestException e) {
            Assert.assertEquals(aPackage.execute().asString().getStatus(), 400);
            errorStatustextResponse();
            errorBodyResponse();
            return;
        }
    }

    @Attachment("Тело ответа: ")
    private String errorBodyResponse() {
        try {
            return aPackage.execute().asString().getBody();
        } catch (UnirestException e) {
            Logger.getLogger(">>>").info(e.getMessage());
        }
        return null;
    }

    @Attachment("Код состояния POST запроса: ")
    private String errorStatustextResponse() {
        try {
            return aPackage.execute().asString().getStatusText() + ", " + aPackage.execute().asString().getStatus();
        } catch (UnirestException e) {
            Logger.getLogger(">>>").info(e.getMessage());
        }
        return null;
    }

    @Attachment("Код состояния POST запроса: ")
    private String getResponseStatusText(HttpResponse<JsonNode> jsonNodeHttpResponse) {
        return jsonNodeHttpResponse.getStatus() + ", " + jsonNodeHttpResponse.getStatusText();
    }

    @Attachment("Тело ответа: ")
    private JsonNode getResponseBody(HttpResponse<JsonNode> jsonNodeHttpResponse) {

        return jsonNodeHttpResponse.getBody();
    }

    @Attachment("Валидация: ID записей WebAPI совпадают с ID записей в MongoDB ")
    public boolean checkRecords() throws ParseException {
        getStack();
        ArrayList<Document> arrayList = new ArrayList();
        DBObject dbObject = getDbObject();
        int limit = Integer.parseInt(String.valueOf(aPackage.getAssemblerBody().get(limit1)));
        JSONArray packages = response.getBody().getArray().getJSONObject(0).getJSONArray(packages1);
        getTelematicParsingElements(dbObject, arrayList, limit);
        for (int i = 0; i < packages.length(); i++) {
            checkRecordsIDs(arrayList.get(i), packages.getJSONObject(i));
        }
        return true;
    }

    public boolean checkMoreRecords() throws ParseException {
        getStack();
        ArrayList<Document> arrayList = new ArrayList();
        DBObject dbObject = getDbObject();
        int limit = Integer.parseInt(String.valueOf(aPackage.getAssemblerBody().get(limit1)));
        JSONArray packages = response.getBody().getArray().getJSONObject(0).getJSONArray(packages1);
        getTelematicParsingElements(dbObject, arrayList, limit);
        for (int i = 0; i < packages.length(); i++) {
            checkRecords(arrayList.get(i), packages.getJSONObject(i));
        }
        return false;
    }

    private void getStack() {
        stack = new LinkedHashSet();
        JSONArray deviceCodes = (JSONArray) aPackage.getAssemblerBody().get("deviceCodes");
        for (int i = 0; i < deviceCodes.length(); i++) {
            stack.add(String.valueOf(deviceCodes.get(i)));
        }
    }

    private DBObject getDbObject() throws ParseException {
        String pattern = "yyyy-MM-dd HH:mm:ss";
        Date start = new SimpleDateFormat(pattern).parse(aPackage.getAssemblerBody().getString("start"));
        Date end = new SimpleDateFormat(pattern).parse(aPackage.getAssemblerBody().getString("end"));
        String format = DateFormatUtils.format(start.getTime(), pattern);
        String format1 = DateFormatUtils.format(end.getTime(), pattern);
        SimpleDateFormat format2 = new SimpleDateFormat(pattern);
        format2.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parse2 = format2.parse(format);
        Date parse3 = format2.parse(format1);
        return BasicDBObjectBuilder.start()
                .add("DeviceCode", new BasicDBObject("$in", stack))
                .add("DeviceTime", new BasicDBObject("$gte", parse2).append("$lte", parse3))
                .add("ParsedPacket.TelematicValues.GpsData.Latitude", new BasicDBObject("$gte", 0))
                .add("ParsedPacket.TelematicValues.GpsData.Longitude", new BasicDBObject("$gte", 0))
                .get();
    }

    private boolean checkRecordsIDs(Document o, JSONObject jsonObject) {
        return String.valueOf(o.get("_id")).equalsIgnoreCase(String.valueOf(jsonObject.get("id")));
    }

    @Attachment("Валидация: сравнение полученных данных с коллекцией TelematicParsingElements")
    private String checkRecords(Document o, JSONObject jsonObject) throws ParseException {
        StringBuilder builder = new StringBuilder();
        String pattern = "yyyy-MM-dd'T'HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parse = simpleDateFormat.parse(jsonObject.getString("deviceTime"));
        Date parse1 = simpleDateFormat.parse(jsonObject.getString("serverTime"));
        builder.append("Device ID: \n");
        checkField(builder, String.valueOf(o.get("_id")), jsonObject.get("id"));
        builder.append("Device Code: \n");
        checkField(builder, o.get("DeviceCode"), jsonObject.get("deviceCode"));
        builder.append("Device EndPoint: \n");
        checkField(builder, o.get("EndPoint"), jsonObject.get("endPoint"));
        builder.append("Device Time: \n");
        checkField(builder, o.getDate("DeviceTime").toString(), parse.toString());
        builder.append("Device Protocol: \n");
        checkField(builder, o.get("Protocol").toString(), jsonObject.get("protocol"));
        builder.append("Server Time: \n");
        checkField(builder, o.getDate("ServerTime").toString(), parse1.toString());
        builder.append("Mileage: \n");
        String parsedPacket = "ParsedPacket";
        String telematicValues = "TelematicValues";
        String value = "Value";
        String parsedPacket1 = "parsedPacket";
        String telematicValues1 = "telematicValues";
        String value1 = "value";
        checkField(builder, Integer.parseInt(String.valueOf(o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get("Mileage", Document.class).get(value))), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject("Mileage").get(value1));
        builder.append("Ignition: \n");
        checkField(builder, o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get("Ignition", Document.class).get(value), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject("Ignition").get(value1));
        builder.append("Geopos:PDOP: \n");
        checkField(builder, Integer.parseInt(String.valueOf(o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get("Geopos:PDOP", Document.class).get(value))), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject("Geopos:PDOP").get(value1));
        builder.append("Geopos:VDOP: \n");
        checkField(builder, Integer.parseInt(String.valueOf(o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get("Geopos:VDOP", Document.class).get(value))), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject("Geopos:VDOP").get(value1));
        builder.append("Geopos:HDOP: \n");
        checkField(builder, Integer.parseInt(String.valueOf(o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get("Geopos:HDOP", Document.class).get(value))), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject("Geopos:HDOP").get(value1));
        builder.append("Longitude: \n");
        String gpsData = "GpsData";
        checkField(builder, o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get(gpsData, Document.class).get("Longitude"), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject(gpsData).get("longitude"));
        builder.append("Latitude: \n");
        checkField(builder, o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get(gpsData, Document.class).get("Latitude"), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject(gpsData).get("latitude"));
        builder.append("Altitude: \n");
        checkField(builder, o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get(gpsData, Document.class).get("Altitude"), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject(gpsData).get("altitude"));
        builder.append("Speed: \n");
        checkField(builder, o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get(gpsData, Document.class).get("Speed"), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject(gpsData).get("speed"));
        builder.append("Course: \n");
        checkField(builder, o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get(gpsData, Document.class).get("Course"), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject(gpsData).get("course"));
        builder.append("Satellites: \n");
        checkField(builder, o.get(parsedPacket, Document.class).get(telematicValues, Document.class).get(gpsData, Document.class).get("Satellites"), jsonObject.getJSONObject(parsedPacket1).getJSONObject(telematicValues1).getJSONObject(gpsData).get("satellites"));
        return builder.toString();
    }

    private boolean checkField(StringBuilder builder, Object o, Object o1) {
        Assert.assertEquals(o, o1);
        builder.append("MongoDB: " + o + "\n");
        builder.append("DevAPI: " + o1 + "\n");
        return o.equals(o1);
    }

    private ArrayList<Document> getTelematicParsingElements(DBObject dbObject, ArrayList<Document> arrayList, int limit) {
        new MongoDevApiConnection().getCollection("TelematicParsingElements").find((Bson) dbObject).limit(limit).forEach((Block<? super Document>) arrayList::add);
        return arrayList;
    }

    @Attachment("Запрос в коллекцию TelematicParsingElements: ")
    public String mongoRequest() throws ParseException {
        getStack();
        String pattern = "yyyy-MM-dd HH:mm:ss";
        Date parse = new SimpleDateFormat(pattern).parse(String.valueOf(aPackage.getAssemblerBody().get("start")));
        Date parse1 = new SimpleDateFormat(pattern).parse(String.valueOf(aPackage.getAssemblerBody().get("end")));
        String pattern1 = "yyyy-MM-dd'T'HH:mm:ss";
        String format = DateFormatUtils.format(parse.getTime(), pattern1);
        String format1 = DateFormatUtils.format(parse1.getTime(), pattern1);
        SimpleDateFormat format2 = new SimpleDateFormat(pattern1);
        format2.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parse2 = format2.parse(format);
        Date parse3 = format2.parse(format1);
        return "db.TelematicParsingElements.find({\"DeviceCode\":{$in:" + parseStack(stack) + "},\"DeviceTime\" :{$gte: ISODate(\"" + format2.format(parse2) + "Z\"),$lte : ISODate(\"" + format2.format(parse3) + "Z\")},\"ParsedPacket.TelematicValues.GpsData.Latitude\":{$gte:0},\"ParsedPacket.TelematicValues.GpsData.Latitude\":{$gte:0}}).limit(" + aPackage.getAssemblerBody().get(limit1) + ")";
    }

    private String parseStack(LinkedHashSet<String> stack) {
        StringBuilder stringBuilder = new StringBuilder();
        if (stack.isEmpty()) {
            stringBuilder.append("[");
            for (int i = 0; i < stack.size(); i++) {
                stringBuilder.append("\"");
                stringBuilder.append(stack.add(String.valueOf(i)));
                stringBuilder.append("\"");

            }
            stringBuilder.append("]");
            return stringBuilder.toString();
        }
        return null;
    }

    public List<Document> responseFromMongo() throws ParseException {
        getStack();
        List<Document> arrayList = new ArrayList<>();
        DBObject dbObject = getDbObject();
        new MongoDevApiConnection().getCollection("TelematicParsingElements").find((Bson) dbObject).limit(Integer.parseInt(String.valueOf(aPackage.getAssemblerBody().get(limit1)))).forEach((Block<? super Document>) arrayList::add);
        return arrayList;
    }

    @Attachment("Валидация: количество пакетов ")
    public String checkCount() throws ParseException {
        StringBuilder builder = new StringBuilder();
        JSONArray packages = response.getBody().getArray().getJSONObject(0).getJSONArray(packages1);
        List<Document> objects = responseFromMongo();
        Assert.assertEquals(packages.length(), objects.size());
        builder.append("кол-во пакетов DevAPI = " + packages.length() + "\n");
        builder.append("кол-во пакетов MongoDB = " + objects.size() + "\n");
        return builder.toString();
    }

    @Attachment("Валидация: количество пакетов не превышает заданный лимит ")
    public boolean checkCountPack() {
        JSONArray packages = response.getBody().getArray().getJSONObject(0).getJSONArray(packages1);
        if (Integer.parseInt(String.valueOf(aPackage.getAssemblerBody().get(limit1))) != 0) {
            return packages.length() <= Integer.parseInt(String.valueOf(aPackage.getAssemblerBody().get(limit1)));
        }
        return packages.length() >= Integer.parseInt(String.valueOf(aPackage.getAssemblerBody().get(limit1)));
    }

    public int getPointer() {
        if (response.getBody().getArray().getJSONObject(0).getJSONArray(packages1).length() == 0 && Integer.parseInt(String.valueOf(aPackage.getAssemblerBody().get(limit1))) == 0) {
            return 0;
        } else if (Integer.parseInt(String.valueOf(aPackage.getAssemblerBody().get(limit1))) == 0) {
            return 1;
        } else return 2;
    }
}