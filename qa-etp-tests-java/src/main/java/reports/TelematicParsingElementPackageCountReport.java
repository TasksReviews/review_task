package reports;

import api.dev.AssemblerCountPackage;
import api.dev.MongoDevApiConnection;
import api.utils.Prop;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.RequestBodyEntity;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.Block;
import com.mongodb.DBObject;
import cucumber.api.java.ContinueNextStepsFor;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.junit.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

public class TelematicParsingElementPackageCountReport {
    private AssemblerCountPackage assemblerReflector;
    private HttpResponse jsonNodeHttpResponse;
    private RequestBodyEntity execute;

    public TelematicParsingElementPackageCountReport() {
        assemblerReflector = new AssemblerCountPackage();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("POST запрос: ")
    public void request(String arg0, String arg1, String... arg2) {
        assemblerReflector.createAssemblerBody(arg0, arg1, Arrays.asList(arg2));
        execute = assemblerReflector.execute();
        getUrl(execute);
        getBodyMap();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("POST запрос: ")
    public void requestEnd(String s) {
        assemblerReflector.createAssemblerBodyWithoutEnd(s);
        getUrl(assemblerReflector.execute());
        getBodyMap();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("POST запрос: ")
    public void requestStart(String s) {
        assemblerReflector.createAssemblerBodyWithoutStart(s);
        getUrl(assemblerReflector.execute());
        getBodyMap();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("POST запрос: ")
    public void requestWithoutToken(String format1, String format2, List<String> strings) {
        assemblerReflector.createAssemblerBody(format1, format2, strings);
        execute = assemblerReflector.executeWithoutToken();
        getUrlWithoutToken(execute);
        getBodyMap();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("POST запрос: ")
    public void request(String arg0, String arg1, List<String> strings) {
        assemblerReflector.createAssemblerBody(arg0, arg1, strings);
        execute = assemblerReflector.execute();
        getUrl(execute);
        getBodyMap();
    }

    @Attachment("URL POST Запроса: ")
    private String getUrlWithoutToken(RequestBodyEntity execute) {
        return execute.getHttpRequest().getUrl();
    }

    @Attachment("URL POST Запроса: ")
    private String getUrl(RequestBodyEntity reflect) {
        return reflect.getHttpRequest().getUrl();
    }

    @Attachment("Тело POST запроса: ")
    private String getBodyMap() {
        return String.valueOf(assemblerReflector.getAssemblerBody());
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ POST запроса: ")
    public void response() throws UnirestException {
        try {
            //Тест позитивный, в ответе получаем кол-во пакетов код ответа 200 OK
            jsonNodeHttpResponse = assemblerReflector.execute().asString();
            Integer.parseInt((String) jsonNodeHttpResponse.getBody());
            getStatusText(jsonNodeHttpResponse);
            getBody(jsonNodeHttpResponse);
        } catch (NumberFormatException e) {
            //Тест негативный, в ответе получаем String, код ответа 400 BadRequest
            Assert.assertEquals(jsonNodeHttpResponse.getStatus(), 400);
            Assert.assertEquals(jsonNodeHttpResponse.getStatusText(), "Bad Request");
            Assert.assertEquals(jsonNodeHttpResponse.getBody(), "Start date can not be greater end date");
            getStatusText(jsonNodeHttpResponse);
            getBody(jsonNodeHttpResponse);
            return;
        }
    }

    public void response(int arg0) {
        try {
            jsonNodeHttpResponse = assemblerReflector.execute().asString();
            Assert.assertEquals(jsonNodeHttpResponse.getStatus(), arg0);
            Assert.assertEquals(jsonNodeHttpResponse.getStatusText(), "Internal Server Error");
            Assert.assertEquals(jsonNodeHttpResponse.getBody(), "");
            getStatusText(jsonNodeHttpResponse);
        } catch (UnirestException e) {
            Logger.getLogger(">>>").info(e.getMessage());
        }
    }

    @Attachment("Статус POST ответа: ")
    private String getStatusText(HttpResponse jsonNodeHttpResponse) {
        return jsonNodeHttpResponse.getStatus() + ", " + jsonNodeHttpResponse.getStatusText();
    }

    @Attachment("Тело POST ответа: ")
    private String getBody(HttpResponse<String> jsonNodeHttpResponse) {
        return jsonNodeHttpResponse.getBody();
    }

    public void checkCount() throws ParseException {
        Stack<String> stack = new Stack<>();
        JSONArray deviceCodes = (JSONArray) assemblerReflector.getAssemblerBody().get("deviceCodes");
        for (int i = 0; i < deviceCodes.length(); i++) {
            stack.add(String.valueOf(deviceCodes.get(i)));
        }
        String pattern = "yyyy-MM-dd HH:mm:ss";
        Date start = new SimpleDateFormat(pattern).parse(assemblerReflector.getAssemblerBody().getString("start"));
        Date end = new SimpleDateFormat(pattern).parse(assemblerReflector.getAssemblerBody().getString("end"));
        String pattern1 = "yyyy-MM-dd HH:mm:ss.sss";
        String format = DateFormatUtils.format(start.getTime(), pattern1);
        String format1 = DateFormatUtils.format(end.getTime(), pattern1);
        SimpleDateFormat format2 = new SimpleDateFormat(pattern1);
        format2.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parse2 = format2.parse(format);
        Date parse3 = format2.parse(format1);
        DBObject dbObject = BasicDBObjectBuilder.start()
                .add("DeviceCode", new BasicDBObject("$in", stack))
                .add("DeviceTime", new BasicDBObject("$gte", parse2).append("$lte", parse3))
                .add("ParsedPacket.TelematicValues.GpsData.Latitude", new BasicDBObject("$gte", 0))
                .add("ParsedPacket.TelematicValues.GpsData.Longitude", new BasicDBObject("$gte", 0))
                .get();
        long telematicParsingElements = new MongoDevApiConnection().getCollection("TelematicParsingElements").count((Bson) dbObject);
        getMongoRequest(stack, format2.format(parse2), format2.format(parse3));
        getCountPackages(Integer.parseInt((String) jsonNodeHttpResponse.getBody()), telematicParsingElements);
    }

    @Attachment("Валидация: запрос в MongoDB: ")
    private String getMongoRequest(Stack<String> arrayList, String parse2, String parse3) {
        StringBuilder sb = new StringBuilder();
        if (arrayList.size() > 1) {
            for (int i = 0; i < arrayList.size() - 1; i++) {
                sb.append("\"" + arrayList.get(i) + "\"");
                sb.append(", ");
            }
            sb.append("\"" + arrayList.get(arrayList.size() - 1) + "\"");
        } else if (arrayList.size() == 1) {
            sb.append("\"" + arrayList.get(arrayList.size() - 1) + "\"");
        }

        return "db.TelematicParsingElements.find({\"DeviceCode\":{$in: [" + sb + "]},\"ServerTime\":{$gte:ISODate(\"" + parse2 + "Z\"),$lte:ISODate(\"" + parse3 + "Z\")},\"ParsedPacket.TelematicValues.GpsData.Latitude\":{$gte:0.0},\"ParsedPacket.TelematicValues.GpsData.Longitude\" : {$gte : 0.0}}).count()";
    }

    @Attachment("Валидация: кол-во записей в коллекции TelematicParsingElements = {0}")
    private boolean getCountPackages(int body, long arrayList) {
        try {
            Assert.assertEquals(arrayList, body);
        } catch (NumberFormatException e) {
            e.getMessage();
        }
        return arrayList == body;
    }

    public void showToken() {

        assemblerReflector.getToken();
    }

    public void ResponseWithoutToken() throws UnirestException {
        jsonNodeHttpResponse = assemblerReflector.execute().asString();
        Assert.assertEquals(401, jsonNodeHttpResponse.getStatus());
        getStatusText(jsonNodeHttpResponse);
        getBody(jsonNodeHttpResponse);
    }
}
