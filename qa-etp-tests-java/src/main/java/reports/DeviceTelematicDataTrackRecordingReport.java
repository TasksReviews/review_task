package reports;

import api.dev.*;
import api.utils.Prop;
import com.google.gson.*;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.RequestBodyEntity;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.t1.core.AbstractClass;
import cucumber.api.java.ContinueNextStepsFor;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.junit.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

public class DeviceTelematicDataTrackRecordingReport {
    private PostExecutor postExecutor;
    private Gson gson;
    private HttpResponse<JsonNode> response;
    private String mongoCollection = "mongoCollection";
    private String deviceCode = "DeviceCode";

    public DeviceTelematicDataTrackRecordingReport() {
        gson = new GsonBuilder().setPrettyPrinting().create();
        postExecutor = new TrackRecording();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры POST запроса: ")
    public void getOffRequest(List<String> arg1) {
        postExecutor.clearList();
        arg1.forEach(postExecutor::add);
        postExecutor.setMonitoringState("Stop");
        RequestBodyEntity execute = postExecutor.execute();
        getUrl(execute);
        getBody(execute);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры POST запроса: ")
    public void getOffNullRequest() {
        postExecutor.clearList();
        postExecutor.setMonitoringState("Stop");
        RequestBodyEntity execute = postExecutor.execute();
        postExecutor.clearList();
        getUrl(execute);
        getBody(execute);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры POST запроса: ")
    public void getOnRequest(List<String> arg1) {
        postExecutor.clearList();
        arg1.forEach(postExecutor::add);
        postExecutor.setMonitoringState("Start");
        RequestBodyEntity execute = postExecutor.execute();
        getUrl(execute);
        getBody(execute);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры POST запроса: ")
    public void getOnNullRequest() {
        postExecutor.clearList();
        postExecutor.setMonitoringState("Start");
        RequestBodyEntity execute = postExecutor.execute();
        postExecutor.clearList();
        getUrl(execute);
        getBody(execute);
    }

    @Attachment("URL запроса")
    private String getUrl(RequestBodyEntity execute) {
        return execute.getHttpRequest().getUrl();
    }

    @Attachment("Тело запроса")
    private Object getBody(RequestBodyEntity execute) {
        return execute.getBody();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ POST запроса: ")
    public synchronized void getResponse() throws UnirestException, ParseException {
        try {
            response = postExecutor.execute().asJson();
            getStatus(response);
            getBody(response);
            checklists(response);
            checkBody(response);
            checkDate(response);
        } catch (UnirestException e) {
            getStatus(postExecutor.execute().asString().getStatus(), postExecutor.execute().asString().getStatusText());
            getBody(postExecutor.execute().asString().getBody());
        }
    }

    @Attachment("Статус ответа: ")
    private String getStatus(int status, String statusText) {
        return status + ", " + statusText;
    }

    @Attachment("Время завершения запроса: ")
    private String checkDate(HttpResponse<JsonNode> response) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH);
        String date = response.getHeaders().get("Date").get(0);
        Date parse = format.parse(date);
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
        return format1.format(parse) + "Z";
    }

    private String checkBody(HttpResponse<JsonNode> response) throws UnirestException {
        try {
            Assert.assertEquals("check result code: ", response.getBody().getObject().get("resultCode"), ("ok"));
            return "resultCode = " + response.getBody().getObject().get("resultCode");
        } catch (NullPointerException e) {
            getBody(postExecutor.execute().asString().getBody());
        }
        return null;
    }

    @Attachment("Ответ POST запроса:")
    private String getBody(String body) {
        return body;
    }

    @Attachment("Код состояния запроса: ")
    private String getStatus(HttpResponse<JsonNode> response) {
        return response.getStatus() + ", " + response.getStatusText();
    }

    @Attachment("Тело ответа: ")
    private JsonNode getBody(HttpResponse<JsonNode> response) {
        return response.getBody();
    }

    private String checklists(HttpResponse<JsonNode> response) {
        Document document = new Document();
        String statusText = response.getStatusText();
        int status = response.getStatus();
        document.put("status equals - ", status);
        document.put("statusText equals - ", statusText);
        return gson.toJson(document);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Проверка информации о записях трэка для ТМУ с кодами: ")
    public synchronized void checkRecords() {
        MongoClient client = new MongoDevApiConnection().getClient();
        MongoCollection<Document> collection = client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("TrackRecordingElements");
        if (collection.count() == 0) {
            Assert.assertTrue("", collection.count() == 0);
            getNullReport("TrackRecordingElements");
        } else {
            for (Object o : postExecutor.getImeiList())
                try {
                    collection.find(new BasicDBObject(deviceCode, Prop.getDeviceCodePIDR((String) o)))
                            .forEach((Consumer<? super Document>) document -> {
                                Object localDeviceCode = document.get("DeviceCode");
                                Assert.assertEquals("", localDeviceCode, Prop.getDeviceCodePIDR((String) o));
                                getResponseFromMongo(document, String.valueOf(localDeviceCode));
                                getDeviceCodeReport(localDeviceCode, Prop.getDeviceCodePIDR((String) o));
                                try {
                                    getReceivedTimeReport(document.get("CommandReceivedTime"), response.getHeaders().get("Date").get(0));
                                } catch (ParseException e) {
                                    Logger.getLogger(">>>").info(e.getMessage());
                                }
                            });
                } catch (NullPointerException e) {
                    getNullReport(String.valueOf(collection));
                    Logger.getLogger(":::>>>").info("collection is empty!");
                }
            for (int i = 0; i < postExecutor.getImeiList().length(); i++)
                generateMongoRequest(i, Prop.getDeviceCodePIDR((String) postExecutor.getImeiList().get(i)));
        }
    }

    @Attachment("Валидация: были проверены поля: ")
    private String getReceivedTimeReport(Object document, String response) throws ParseException {
        JsonObject object = new JsonObject();
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy");
        format.setTimeZone(TimeZone.getTimeZone("MSK"));
        Date parse = format.parse(String.valueOf(document));
        Date parse2 = new SimpleDateFormat().parse(response);
        Assert.assertEquals(parse.getTime(), parse2.getTime());
        object.add("Command Received Time", (JsonElement) document);
        object.add("Response Date", new JsonParser().parse(response));
        return gson.toJson(object);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Проверка информации о записях трэка для ТМУ с кодами: ")
    public void checkOffRecords() {
        HashSet<String> stack = new HashSet<>();
        for (int i = 0; i < postExecutor.getImeiList().length(); i++) {
            stack.add(String.valueOf(postExecutor.getImeiList().get(i)));
        }
        MongoClient client = new MongoDevApiConnection().getClient();
        try {
            postExecutor.clearList();
            stack.forEach(postExecutor::add);
            postExecutor.setMonitoringState("Stop");
            postExecutor.execute().asJson();
            MongoCollection<Document> collection = client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("TrackRecordingElements");
            DBObject dbObject = BasicDBObjectBuilder.start().add(deviceCode, new BasicDBObject("$in", stack)).get();
            getRequestToMongoCount(stack);
            getCountOffCommand(collection.count((Bson) dbObject), 0);
        } catch (UnirestException e) {
            e.printStackTrace();
        } finally {
            client.close();
        }
    }

    @Attachment("Валидация: количество записей в коллекции TrackRecordingElements = {0}")
    private String getCountOffCommand(long count, int i) {
        Assert.assertEquals(count, i);
        return "кол-во записей в коллекции TrackRecordingElements = " + count;
    }

    @Attachment("Запрос в коллекцию \"TrackRecordingElements\"")
    private String getRequestToMongoCount(HashSet<String> stack) {
        StringBuilder strings = new StringBuilder();
        Object[] objects = stack.toArray();
        strings.append("[");

        if (stack.size() == 1) {
            strings.append("\"" + objects[0] + "\"");
        } else {
            for (int i = 0; i <= stack.size() - 2; i++) {
                strings.append("\"" + objects[i] + "\",");
            }
            strings.append("\"" + objects[objects.length - 1] + "\"");
            strings.append("]");
            return "db.TrackRecordingElements.find({\"DeviceCode\": {$in : " + strings + "}}).count()";
        }
        return null;
    }

    @Attachment("В колекции  {0} записей нет")
    private String getNullReport(String trackRecordingElements) {
        return "Collection " + trackRecordingElements + " is empty";
    }

    @Attachment("Валидация: было проверено поле DeviceCode")
    private String getDeviceCodeReport(Object deviceCode, Object o) {
        return "Device Codes from request: " + o + " and from mongo: " + deviceCode + " are equals";
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("MongoDB: запрос в коллекцию \"TrackRecordingElements\" для ТМУ с кодом {1}")
    private void generateMongoRequest(int i, Object deviceCode) {
        Object o = postExecutor.getImeiList().get(i);
        getRequestToMongo(o);
    }

    @SuppressWarnings("deprecation")
    @Attachment("Запрос в коллекцию \"TrackRecordingElements\"")
    private String getRequestToMongo(Object o) {
        return "db.TrackRecordingElements.find({\"DeviceCode\":\"" + Prop.getDeviceCodePIDR((String) o) + "\"})";
    }

    @Attachment("Информация записи трэка для ТМУ с кодом {1}")
    private String getResponseFromMongo(Document document, String deviceCode) {
        return gson.toJson(document);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры POST запроса: ")
    public void getOnRequest(List<String> ts, String arg1) {
        postExecutor.clearList();
        ts.forEach(postExecutor::add);
        postExecutor.setMonitoringState(arg1);
        RequestBodyEntity execute = postExecutor.execute();
        getBody(execute);
        getUrl(execute);
    }

    @Attachment("Валидация: были проверены поля: ")
    public synchronized String checkOnRecords() throws InterruptedException {
        wait(3000);
        Set<String> stack = new HashSet<>();
        for (int i = 0; i < postExecutor.getImeiList().length(); i++) {
            stack.add(Prop.getDeviceCodePIDRTest(String.valueOf(postExecutor.getImeiList().get(i))));
        }
//        DBObject dbObject = BasicDBObjectBuilder
//                .start()
//                .add(deviceCode, new BasicDBObject("$in", stack))
//                .get();
        MongoClient client = new MongoDevApiConnection().getClient();
        try {
            MongoCollection<Document> collection = client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("TrackRecordingElements");
            getCountOffCommand(stack.size(), postExecutor.getImeiList().length());
            getRequestToMongoCount((HashSet<String>) stack);
            StringBuilder stringBuilder = new StringBuilder();
            for (Object o : postExecutor.getImeiList()) {
                try {
                    collection.find(new BasicDBObject("DeviceCode", o)).forEach((Consumer<? super Document>) document -> {
                        Date commandReceivedTime = document.getDate("CommandReceivedTime");
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                        df.setTimeZone(TimeZone.getTimeZone("UTC"));
                        stringBuilder.append("DeviceCode = " + document.get(deviceCode).toString() + "\n");
                        stringBuilder.append("CommandReceivedTime = " + df.format(commandReceivedTime) + "Z\n");
                    });
                } catch (NullPointerException e) {
                    Logger.getLogger(":::>>>").info("collection is empty!");
                }
            }
            return stringBuilder.toString();
        } finally {
            client.close();
        }
    }
}