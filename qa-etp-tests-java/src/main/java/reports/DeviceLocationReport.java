package reports;


import api.dev.DeviceLocation;
import api.dev.MongoDevApiConnection;
import api.dev.PostExecutor;
import api.utils.Prop;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.RequestBodyEntity;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.t1.core.AbstractClass;
import cucumber.api.java.ContinueNextStepsFor;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DeviceLocationReport {
    private final Gson gson;
    private final PostExecutor postExecutor;
    private MongoCollection<Document> collection;
    private HttpResponse<JsonNode> response;
    private String deviceTime = "DeviceTime";
    private String deviceCode = "DeviceCode";
    private String receivedTime = "ReceivedTime";
    private String string = "Fields are equals: ";

    public DeviceLocationReport() {
        gson = new GsonBuilder().setPrettyPrinting().create();
        postExecutor = new DeviceLocation();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры POST запроса: ")
    public void getRequest(List<String> arg1) {
        arg1.forEach(this::accept);
        RequestBodyEntity execute = postExecutor.execute();
        getUrl(execute);
        getBody(execute);
    }

    @Attachment("URL запроса: ")
    private String getUrl(RequestBodyEntity execute) {
        Logger.getLogger(">>>").info("getUrl: " + execute.getHttpRequest().getUrl());
        return execute.getHttpRequest().getUrl();
    }

    @Attachment("Тело запроса: ")
    private Object getBody(RequestBodyEntity execute) {
        Logger.getLogger(">>>").info("getBody: " + execute.getBody());
        return execute.getBody();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ POST запроса:")
    public void getNegativeResponse() throws UnirestException {
        if (postExecutor.execute().asString().getStatus() == 400) {
            HttpResponse<String> stringHttpResponse = postExecutor.execute().asString();
            getResponseStringStatus(stringHttpResponse);
            getResponseStringBody(stringHttpResponse);
        }
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ POST запроса: ")
    public void getResponse() throws UnirestException {
        if (postExecutor.execute().asString().getStatus() == 200) {
            response = postExecutor.execute().asJson();
            getResponseStatusText(response);
            getResponseBody(response);
            checklists(response);
        }
    }

    @Attachment("Тело ответа: ")
    private String getResponseStringBody(HttpResponse<String> stringHttpResponse) {
        return stringHttpResponse.getBody();
    }


    @Attachment("Код состояния запроса: ")
    private String getResponseStringStatus(HttpResponse<String> stringHttpResponse) {
        return stringHttpResponse.getStatus() + ", " + stringHttpResponse.getStatusText();
    }

    @Attachment("Тело ответа: ")
    private String getResponseBody(HttpResponse<JsonNode> response) {
        Logger.getLogger(">>>").info("getResponseBody: " + gson.toJson(response.getBody().getObject()));
        return gson.toJson(response.getBody().getObject());
    }


    @Attachment("Код состояния запроса: ")
    private String getResponseStatusText(HttpResponse<JsonNode> response) {
        Assert.assertTrue("check status text: ", response.getStatusText().equalsIgnoreCase("OK"));
        Logger.getLogger(">>>").info("getResponseStatusText: " + response.getStatusText());
        return response.getStatus() + ", " + response.getStatusText();
    }

    @Attachment("Валидация: код состояния запроса: ")
    private String checklists(HttpResponse<JsonNode> response) {
        Document document = new Document();
        String statusText = response.getStatusText();
        int status = response.getStatus();
        document.put("status equals - ", status);
        document.put("statusText equals - ", statusText);
        return gson.toJson(document);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Запрос в MongoDB")
    public void generateMongoRequest() throws ParseException {
        JSONArray location = response.getBody().getObject().getJSONArray("location");
        for (int i = 0; i < location.length(); i++) {
            String date = response.getHeaders().get("Date").get(0);
            Date parse = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH).parse(date);
            MongoClient client = new MongoDevApiConnection().getClient();
            try {
                Object o = client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("Devices").find(new BasicDBObject("Imei", location.getJSONObject(i).get("imei"))).first().get("Code");
                getMongoRequest(o, parse);
            } finally {
                client.close();
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Attachment("Запрос в коллекцию \"DeviceFullRecords\"")
    private String getMongoRequest(Object deviceCode, Object deviceTime) throws ParseException {
        String parse = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format((Date) deviceTime);
        return "db.DeviceFullRecords.find({\"DeviceCode\": \"" + deviceCode + "\",\"DeviceTime\":{$lte:" + parse + "}}).sort({\"DeviceTime\":-1}).limit(1)";
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ из MongoDB")
    public JSONArray getResponseFromMongo() throws ParseException {
        JSONArray imeiList = postExecutor.getImeiList();
        for (int i = 0; i < imeiList.length(); i++) {
            getFromMongoReport(imeiList.get(i));
        }
        return null;
    }

    @SuppressWarnings("deprecation")
    @Attachment("Ответ из коллекции DeviceFullRecords: ")
    private String getFromMongoReport(Object o) throws ParseException {
        MongoClient client = new MongoDevApiConnection().getClient();
        try {
            collection = client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("DeviceFullRecords");
            String date = response.getHeaders().get("Date").get(0);
            Date parse = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH).parse(date);
            BasicDBObject deviceTime = new BasicDBObject(deviceCode, Prop.getDeviceCodePIDR((String) o)).append("DeviceTime", new BasicDBObject("$lte", parse));
            return gson.toJson(collection.find(deviceTime).sort(new Document(this.deviceTime, -1)).first());
        } finally {
            client.close();
        }
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Были проверены поля: ")
    public void checkFields() throws ParseException {
        for (int i = 0; i < postExecutor.getImeiList().length(); i++) {
            Document deviceFullRecordObject = getDeviceFullRecordObject(postExecutor.getImeiList().get(i));
            JSONObject locations = response.getBody().getObject().getJSONArray("location").getJSONObject(i);
            Document object = new Document();
            object.put(deviceCode, postExecutor.getImeiList().get(i));
            getDeviceCodeLog(deviceFullRecordObject, String.valueOf(deviceFullRecordObject.get(deviceCode)));
            object.put(deviceTime, locations.get("deviceTime"));
            getDeviceTimeLog(deviceFullRecordObject, deviceFullRecordObject.getDate(deviceTime));
            object.put(receivedTime, locations.get("receivedTime"));
            getReceivedTimeLog(deviceFullRecordObject, deviceFullRecordObject.getDate(receivedTime));
            getLongitudeLog(deviceFullRecordObject, locations.get("longitude"));
            getLatitudeLog(deviceFullRecordObject, locations.get("latitude"));
            getCourceLog(deviceFullRecordObject, locations.get("course"));
        }
    }


    private Document getDeviceFullRecordObject(Object o) throws ParseException {
        String date = response.getHeaders().get("Date").get(0);
        Date parse = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH).parse(date);
        DBObject dbObject = BasicDBObjectBuilder.start()
                .add("DeviceCode", o)
                .add("DeviceTime", new BasicDBObject("$lte", parse))
                .get();
        MongoClient client = new MongoDevApiConnection().getClient();
        try {
            return client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("DeviceFullRecords").find((Bson) dbObject).sort(new BasicDBObject("DeviceTime", -1)).limit(1).first();
        } finally {
            client.close();
        }
    }

    @Attachment("Поле Course: ")
    private String getCourceLog(Document recordObject, Object course) throws ParseException {
        Object course1 = recordObject.get("Course");
        DecimalFormat decimalFormat = new DecimalFormat("000.0");
        Object parse;
        Object parse1;
        try {
            parse = decimalFormat.parse(String.valueOf(course));
            parse1 = decimalFormat.parse(String.valueOf(course1));
            Assert.assertEquals(parse, parse1);
            Logger.getLogger(">>>").info("getCourceLog: " + course1 + " = " + course);
            return string + course1 + " = " + course;
        } catch (ParseException e) {
            Logger.getLogger(">>>").info("getCourceLog: " + course1 + " = " + course);
            return string + course1 + " = " + course;

        }
    }

    @Attachment("Поле Longitude: ")
    private String getLongitudeLog(Document recordObject, Object longitude) {
        DecimalFormat decimalFormat = new DecimalFormat("##.#");
        String longitude1 = decimalFormat.format(recordObject.getDouble("Longitude") - recordObject.getDouble("Longitude") % 1);
        String format = decimalFormat.format((Integer) longitude / 1000000);
        Assert.assertEquals("check longitude: ", longitude1, format);
        Logger.getLogger(">>>").info("getLongitudeLog: " + longitude1 + " = " + format);
        return string + longitude1 + " = " + format;
    }

    @Attachment("Поле Latitude: ")
    private String getLatitudeLog(Document recordObject, Object latitude) {
        DecimalFormat decimalFormat = new DecimalFormat("##.#");
        String longitude1 = decimalFormat.format(recordObject.getDouble("Latitude") - recordObject.getDouble("Latitude") % 1);
        String format = decimalFormat.format((Integer) latitude / 1000000);
        Assert.assertEquals("check latitude: ", longitude1, format);
        Logger.getLogger(">>>").info("getLatitudeLog: " + longitude1 + " = " + format);
        return string + longitude1 + " = " + format;
    }

    @Attachment("Поле ReceivedTime: ")
    private String getReceivedTimeLog(Document recordObject, Date reseivedTime) {
        Assert.assertEquals("check received time: ", recordObject.getDate("ReceivedTime").getTime(), reseivedTime.getTime());
        Logger.getLogger(">>>").info("getReceivedTimeLog: " + recordObject.getDate("ReceivedTime") + " = " + receivedTime);
        return string + recordObject.getDate("ReceivedTime") + "Z" + " = " + receivedTime;
    }


    @Attachment("Поле DeviceTime: ")
    private String getDeviceTimeLog(Document recordObject, Date deviceTime) {
        Assert.assertEquals("check device time: ", recordObject.getDate("DeviceTime").getTime(), deviceTime.getTime());
        Logger.getLogger(">>>").info("getDeviceTimeLog: " + recordObject.getDate("DeviceTime") + " = " + deviceTime);
        return string + recordObject.getDate("DeviceTime") + "Z" + " = " + deviceTime;
    }

    @SuppressWarnings("deprecation")
    @Attachment("Поле DeviceCode: ")
    private String getDeviceCodeLog(Document recordObject, String s) {
        Assert.assertEquals("check device code: ", s, recordObject.getString(deviceCode));
        Logger.getLogger(">>>").info("getDeviceCodeLog: " + s + "=" + recordObject.getString(deviceCode));
        return string + s + " = " + recordObject.getString(deviceCode);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры POST запроса: ")
    public void getRequest() {
        RequestBodyEntity execute = postExecutor.execute();
        getUrl(execute);
        getBody(execute);
    }

    private void accept(String imei) {
        postExecutor.add(imei);
    }
}
