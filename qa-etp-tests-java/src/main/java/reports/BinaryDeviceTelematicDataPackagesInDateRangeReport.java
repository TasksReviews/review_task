package reports;

import Infrastructure.Protobuf.Contracts.TelematicRecords.TelematicRecordOuterClass;
import api.dev.BinaryPackagesInDateRange;
import api.dev.GetExecutor;
import api.dev.MongoDevApiConnection;
import api.utils.Prop;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.t1.core.AbstractClass;
import cucumber.api.java.ContinueNextStepsFor;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.TimeZone;
import java.util.function.Consumer;

public class BinaryDeviceTelematicDataPackagesInDateRangeReport {
    private GetExecutor binaryExecutor = new BinaryPackagesInDateRange();
    private HttpResponse<JsonNode> jsonNodeHttpResponse;

    @ContinueNextStepsFor(AssertionError.class)
    @Step("GET Запрос: ")
    public void setBinaryRequest(String arg0, String arg1, String arg2) throws UnirestException {
        binaryExecutor.setFrom(arg0);
        binaryExecutor.setTo(arg1);
        binaryExecutor.setLimit(arg2);
        getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("GET Запрос: ")
    public void setBinaryRequest(String arg0, String arg1, String arg2, String arg3) throws UnirestException {
        binaryExecutor.setFrom(arg0);
        binaryExecutor.setTo(arg1);
        binaryExecutor.setLimit(arg2);
        binaryExecutor.setSkip(arg3);
        getUrl();
    }

    @Attachment("URL запроса: ")
    private String getUrl() throws UnirestException {
        return binaryExecutor.execute().getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ GET Запроса: ")
    public void getResponse() throws UnirestException {
        try {
            jsonNodeHttpResponse = binaryExecutor.execute().asJson();
            getStatus(jsonNodeHttpResponse);
            getBody();
        } catch (UnirestException e) {
            HttpResponse<String> stringHttpResponse = binaryExecutor.execute().asString();
            getNegativeStatus(stringHttpResponse);
        }
    }

    @Attachment("Статус GET запроса: ")
    private String getNegativeStatus(HttpResponse<String> stringHttpResponse) {
        return stringHttpResponse.getStatusText() + ", " + stringHttpResponse.getStatus();
    }

    @Attachment("Статус GET запроса: ")
    private String getStatus(HttpResponse<JsonNode> jsonNodeHttpResponse) {
        return jsonNodeHttpResponse.getStatusText() + ", " + jsonNodeHttpResponse.getStatus();
    }

    @Attachment("Тело GET запроса: ")
    private JsonNode getBody() {
        return jsonNodeHttpResponse.getBody();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Проверка полей ответа: ")
    public void checkBinaryFields() throws InvalidProtocolBufferException, ParseException {
        JSONArray array = jsonNodeHttpResponse.getBody().getArray();
        if (array.length() != 0) {
            for (int i = 0; i < array.length(); i++) {
                StringBuilder builder = new StringBuilder();
                byte[] decode = Base64.getDecoder().decode(array.get(i).toString());
                TelematicRecordOuterClass.TelematicRecord telematicRecord = TelematicRecordOuterClass.TelematicRecord.parseFrom(decode);
                ArrayList<Document> documents = mongoCollectionByParams(telematicRecord.getDeviceCode());
                builder.append("DeviceCode: ");
                checkBinaryField(builder, telematicRecord.getDeviceCode(), documents.get(i).getString("DeviceCode"));
                builder.append("DeviceTime: ");
                long l1 = (telematicRecord.getDeviceTime() - 621355968000000000L) / 10000;
                long deviceTime1 = documents.get(i).getDate("DeviceTime").getTime();
                String formatUTC = DateFormatUtils.format(l1, "yyyy-MM-dd'T'hh:MM:ss");
                String format = DateFormatUtils.format(deviceTime1, "yyyy-MM-dd'T'hh:MM:ss");
                checkBinaryField(builder, formatUTC, format);
                builder.append("ReceivedTime: ");
                long l2 = (telematicRecord.getServerTime() - 621355968000000000L) / 10000;
                long receivedTime1 = documents.get(i).getDate("ReceivedTime").getTime();
                String format1UTC = DateFormatUtils.format(l2, "yyyy-MM-dd'T'hh:MM:ss");
                String format2 = DateFormatUtils.format(receivedTime1, "yyyy-MM-dd'T'hh:MM:ss");
                checkBinaryField(builder, format1UTC, format2);
                builder.append("Latitude: ");
                String format1 = new DecimalFormat("0.000").format(telematicRecord.getGpsData().getLatitude() / 1000000);
                try {
                    String format3 = new DecimalFormat("0.000").format(documents.get(i).get("Latitude"));
                    checkBinaryField(builder, format1, format3);
                } catch (IllegalArgumentException e) {
                    checkBinaryField(builder, format1, null);
                }
                builder.append("Altitude: ");
                checkBinaryField(builder, telematicRecord.getGpsData().getAltitude(), documents.get(i).get("Altitude"));
                builder.append("Longitude: ");
                format1 = new DecimalFormat("0.0000").format(telematicRecord.getGpsData().getLongitude() / 1000000);
                try {
                    String format3 = new DecimalFormat("0.0000").format(documents.get(i).get("Longitude"));
                    checkBinaryField(builder, format1, format3);
                } catch (IllegalArgumentException e) {
                    checkBinaryField(builder, format1, null);
                }
                builder.append("Course: ");
                checkBinaryField(builder, telematicRecord.getGpsData().getCourse(), documents.get(i).get("Course"));
//                builder.append("Satellites: ");
//                checkBinaryField(builder, telematicRecord.getGpsData().getSatellites(), documents.get(i).get("Satellites"));
                builder.append("Speed: ");
                checkBinaryField(builder, telematicRecord.getGpsData().getSpeed(), documents.get(i).get("Speed"));
                builder.append("AccelerationAvgX: ");
                checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationAvgX(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationAvgX"));
                builder.append("AccelerationAvgY: ");
                checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationAvgY(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationAvgY"));
                builder.append("AccelerationAvgZ: ");
                checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationAvgZ(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationAvgZ"));
                builder.append("AccelerationAvgX: ");
                checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationMaxX(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationMaxX"));
                builder.append("AccelerationAvgY: ");
                checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationMaxY(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationMaxY"));
                builder.append("AccelerationAvgZ: ");
                checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationMaxZ(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationMaxZ"));
                builder.append("Duration: ");
                checkBinaryField(builder, (double) telematicRecord.getAcceleration().getDuration(), documents.get(i).get("ExtendedData", Document.class).get("Duration"));
                showDocument(builder);
            }
        } else {
            getResponseBody();
        }
    }

    @Attachment("В ответе записей нет.")
    private JsonNode getResponseBody() {
        Assert.assertEquals(jsonNodeHttpResponse.getBody().getArray().length(), 0);
        return jsonNodeHttpResponse.getBody();
    }

    @Attachment("Проверенные поля: ")
    private String showDocument(StringBuilder builder) {
        return builder.toString();
    }

    private void checkBinaryField(StringBuilder builder, Object response, Object mongo) {
        try {

            if (mongo != null) {
                Assert.assertEquals(response, mongo);
                builder.append("MongoDB: " + mongo + "\n");
                builder.append("Response: " + response + "\n");
            } else {
                builder.append("MongoDB: " + mongo + "\n");
                builder.append("Response: " + response + "\n");
            }
        } catch (ComparisonFailure failure) {
            builder.append("MongoDB: " + mongo + "\n");
            builder.append("Response: " + response + "\n");
        }
    }

    private ArrayList<Document> mongoCollectionByParams(String deviceCode) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parse = simpleDateFormat.parse(binaryExecutor.getFrom());
        Date parse1 = simpleDateFormat.parse(binaryExecutor.getTo());
        DBObject dbObject = BasicDBObjectBuilder.start()
                .add("DeviceCode", deviceCode)
                .add("DeviceTime", new BasicDBObject("$gte", parse).append("$lte", parse1))
                .get();
        ArrayList<Document> list = new ArrayList<>();
        MongoClient client = new MongoDevApiConnection().getClient();
        try {
            FindIterable<Document> deviceFullRecords = client.getDatabase(AbstractClass.getProperty("mongoCollection"))
                    .getCollection("DeviceFullRecords")
                    .find((Bson) dbObject)
                    .limit(Integer.parseInt(binaryExecutor.getLimit()))
                    .skip(Integer.parseInt(binaryExecutor.getSkip()))
                    .sort(new BasicDBObject("DeviceTime", 1));
            deviceFullRecords.forEach((Consumer<? super Document>) list::add);
        } catch (NumberFormatException e) {
        }
        if (list.size() != 0) {
            return list;
        } else {
            dbObject = BasicDBObjectBuilder.start()
                    .add("DeviceCode", Prop.getDeviceCodePIDR(deviceCode))
                    .add("DeviceTime", new BasicDBObject("$gte", parse).append("$lte", parse1))
                    .get();
            list = new ArrayList<>();
            try {
                FindIterable<Document> deviceFullRecords = client.getDatabase(AbstractClass.getProperty("mongoCollection"))
                        .getCollection("DeviceFullRecords")
                        .find((Bson) dbObject)
                        .limit(Integer.parseInt(binaryExecutor.getLimit()))
                        .skip(Integer.parseInt(binaryExecutor.getSkip()))
                        .sort(new BasicDBObject("DeviceTime", 1));
                deviceFullRecords.forEach((Consumer<? super Document>) list::add);

            } catch (NumberFormatException e) {
            } finally {
                client.close();
            }
            return list;
        }
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Статус ответа = {0}")
    public void getNegativeResponse(int arg0) throws UnirestException {
        HttpResponse<String> stringHttpResponse = binaryExecutor.execute().asString();
        getNegativeStatus(stringHttpResponse);
    }
}