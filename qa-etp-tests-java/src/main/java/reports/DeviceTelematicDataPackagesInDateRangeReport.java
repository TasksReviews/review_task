package reports;

import api.dev.GetExecutor;
import api.dev.MongoDevApiConnection;
import api.dev.PackagesInDateRange;
import api.utils.Prop;
import com.google.gson.*;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.t1.core.AbstractClass;
import cucumber.api.java.ContinueNextStepsFor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.http.util.Asserts;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

public class DeviceTelematicDataPackagesInDateRangeReport {
    GetExecutor executor;
    private Gson gson;
    private Document document = new Document();
    private String telematicRecords1;

    public DeviceTelematicDataPackagesInDateRangeReport() {
        executor = new PackagesInDateRange();
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void request(String arg0, String arg1, String arg2) throws UnirestException {
        executor.setFrom(arg0);
        executor.setTo(arg1);
        executor.setLimit(arg2);
        Logger.getLogger(">>>").info(getUrl(executor.execute()));
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void request(String arg0, String arg1, String arg2, String arg3) throws UnirestException {
        executor.setFrom(arg0);
        executor.setTo(arg1);
        executor.setLimit(arg2);
        executor.setSkip(arg3);
        Logger.getLogger(">>>").info(getUrl(executor.execute()));
    }

    @Attachment("URL запроса")
    private String getUrl(GetRequest execute) {
        return execute.getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ GET запроса: ")
    public void getNegativeResponse() throws UnirestException {
        try {
            Assert.assertEquals(executor.execute().asString().getStatus(), 400);
            HttpResponse<String> stringHttpResponse = executor.execute().asString();
            negativeBodyResponse("Response: ", stringHttpResponse.getBody());
            negativeStatusResponse("Status: ", String.valueOf(stringHttpResponse.getStatus()));
            negativeStatusTextResponse("Status text: ", stringHttpResponse.getStatusText());
        } catch (AssertionError error) {
            Assert.fail("Status: " + String.valueOf(executor.execute().asString().getStatus()) + ", " +
                    "Status text: " + executor.execute().asString().getStatusText());
        }
    }

    //Проверка сообщения в теле запроса для некорректного Limit
    public void checkResponseBodyLimit(String limit) throws UnirestException {
        String message = "";
        try {
            //Для limit типа Integer
            if (Integer.parseInt(limit) <= 0) {
                message = "{\"Limit\":[\"Value for Limit must be between 1 and 2147483647.\"]}";
            } else {
                return;
            }
        } catch (NumberFormatException e) {
            switch (limit) {
                case "":
                    message = "{\"Limit\":[\"The value '' is invalid.\"]}";
                    break;
                case "2018-06-28%2011:00:00":
                    message = String.format("{\"Limit\":[\"The value '%s' is not valid for Limit.\"]}", limit.replace("%20", " "));
                    break;
                default:
                    message = String.format("{\"Limit\":[\"The value '%s' is not valid for Limit.\"]}", limit);
                    break;
            }

        }
        HttpResponse<String> response = executor.execute().asString();
        try {

            Assert.assertEquals(response.getBody(), message);
            validationResponseBody(response.getBody());
        } catch (AssertionError e) {
            Assert.fail(String.format("Response body is not valid. Expected [%s] but found [%s]", message, response.getBody()));
        }
    }

    @Attachment("Валидация: получено верное тело ответа")
    private String validationResponseBody(String message) {
        return String.format("Тело ответа верное: %s", message);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ GET запроса: ")
    public void getResponse() throws UnirestException {
        try {
            getResponseData();
        } catch (NullPointerException e) {
            String status = String.valueOf(executor.execute().asString().getStatus() + ", " + executor.execute().asString().getStatusText());
            negativeStatusResponse("Status: ", status);
            negativeBodyResponse("Response: ", executor.execute().asString().getBody());
            throw e;
        }
    }

    @Attachment("Тело GET запроса: ")
    private String negativeBodyResponse(String s, String body) {
        return s + " = " + body;
    }

    @Attachment("Статус GET запроса: ")
    private String negativeStatusResponse(String s, String body) {
        return s + " = " + body;
    }

    @Attachment("Текст статуса GET запроса: ")
    private String negativeStatusTextResponse(String s, String body) {
        return s + " = " + body;
    }

    private void getResponseData() throws UnirestException {
        try {
            Assert.assertEquals(executor.execute().asJson().getStatus(), 200);
            Assert.assertEquals(executor.execute().asJson().getStatusText(), "OK");
            getStatus(executor.execute().asJson());
            getBody(executor.execute().asJson());
            getDate(executor.execute().asJson());
        } catch (AssertionError | UnirestException error) {
            negativeStatusResponse("Status: ", String.valueOf(executor.execute().asString().getStatus()));
            negativeStatusTextResponse("Status text: ", executor.execute().asString().getStatusText());
            Assert.fail("Status: " + String.valueOf(executor.execute().asString().getStatus()) + ", " +
                    "Status text: " + executor.execute().asString().getStatusText());
        }
    }

    @Attachment("Время завершения запроса")
    private List<String> getDate(HttpResponse<JsonNode> response) {
        return response.getHeaders().get("Date");
    }

    @Attachment("Код состояния запроса: ")
    private String getStatus(HttpResponse<JsonNode> response) {
        Logger.getLogger(">>>").info(response.getStatus());
        return response.getStatus() + ", " + response.getStatusText();
    }

    @Attachment("Тело ответа WebAPI")
    private JSONArray getBody(HttpResponse<JsonNode> response) {
        telematicRecords1 = "telematicRecords";
        System.out.println("response = " + response.getBody().getObject());
        return response.getBody().getObject().getJSONArray(telematicRecords1);
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Валидация: сравнение телематических записей трэка из коллекции \"DeviceFullRecords\" с ответом WebAPI")
    public void dataFromMongo() throws UnirestException, ParseException {
        JSONArray records = executor.execute().asJson().getBody().getObject().getJSONArray("telematicRecords");
        for (int i = 0; i < records.length(); i++) {
            String imei = (String) records.getJSONObject(i).get("imei");
            ArrayList<Document> list = mongoCollectionByParams(imei);
            checkLimits(list.size(), records.length());
            Date parse;
            Date parse1;
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                parse = simpleDateFormat.parse(executor.getFrom());
                parse1 = simpleDateFormat.parse(executor.getTo());
            } catch (ParseException e) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd'T'HH'%3A'mm'%3A'ss");
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                parse = simpleDateFormat.parse(executor.getFrom());
                parse1 = simpleDateFormat.parse(executor.getTo());
            }
            checkFields(parse, parse1, list.get(i), (JSONObject) records.get(i));
        }
    }

    private ArrayList<Document> mongoCollectionByParams(String deviceCode) throws ParseException {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parse;
        Date parse1;
        try {
            parse = simpleDateFormat.parse(executor.getFrom());
            parse1 = simpleDateFormat.parse(executor.getTo());
        } catch (ParseException e) {
            simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd'T'HH'%3A'mm'%3A'ss");
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            parse = simpleDateFormat.parse(executor.getFrom());
            parse1 = simpleDateFormat.parse(executor.getTo());
        }
        DBObject dbObject = BasicDBObjectBuilder.start()
                .add("DeviceCode", deviceCode)
                .add("DeviceTime", new BasicDBObject("$gte", parse).append("$lte", parse1))
                .get();
        ArrayList<Document> list = new ArrayList<>();
        MongoClient client = new MongoDevApiConnection().getClient();
        try {
            FindIterable<Document> deviceFullRecords = client.getDatabase(AbstractClass.getProperty("mongoCollection"))
                    .getCollection("DeviceFullRecords")
                    .find((Bson) dbObject)
                    .limit(Integer.parseInt(executor.getLimit()))
                    .skip(Integer.parseInt(executor.getSkip()))
                    .sort(new BasicDBObject("DeviceTime", 1));
            deviceFullRecords.forEach((Consumer<? super Document>) list::add);
            return list;
        } catch (NumberFormatException e) {

        } finally {
            client.close();
        }
        return null;
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Число записей в ответе запроса: {0}, число записей в коллекции DeviceFullRecords: {1}")
    private boolean checkLimits(int list, int records) {
        try {
            Assert.assertTrue("", list == records);
            Logger.getLogger(">>>").info("get list size from Mongo: " + list);
            Logger.getLogger(">>>").info("get records size from getResponse: " + records);
            return list == records;
        } catch (AssertionError error) {
            return false;
        }
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("были проверены поля для ТМУ ")
    private void checkFields(Date parse, Date parse1, Document document, JSONObject jsonObject) {
//         В JSONObject нет deviceCode а в document нет Imei
        checkDeviceImei(jsonObject.getString("imei"), document.get("DeviceCode"));
        DecimalFormat decimalFormat = new DecimalFormat("00.000000");
        if (document.getDouble("Latitude") != null) {
            double latitude = document.getDouble("Latitude") * 1000000;
            double latitude1 = jsonObject.getDouble("latitude");
            checkLatitude(decimalFormat, (int) latitude, (int) latitude1);
        } else getNullDoc("Latitude", document.getDouble("Latitude"), jsonObject.getDouble("latitude"));
        if (document.getDouble("Longitude") != null) {
            double longitude = document.getDouble("Longitude") * 1000000;
            double longitude1 = jsonObject.getDouble("longitude");
            checkLongitude(decimalFormat, (int) longitude, (int) longitude1);
        } else getNullDoc("Longitude", document.getDouble("Longitude"), jsonObject.getDouble("longitude"));
        if (document.get("ExtendedData", Document.class).get("Geopos:Altitude meters") != null) {
            double altitude = document.get("ExtendedData", Document.class).getDouble("Geopos:Altitude meters") * 1000000;
            double altitude1 = jsonObject.getDouble("altitude");
            checkAltitude(decimalFormat, (int) altitude, (int) altitude1);
        } else
            getNullDoc("Altitude", document.get("ExtendedData", Document.class).getDouble("Geopos:Altitude meters"), jsonObject.getDouble("altitude"));
        if (document.getInteger("Course") != null) {
            Object course = document.getInteger("Course");
            int course1 = jsonObject.getInt("course");
            checkCourse(decimalFormat, course, course1);
        } else getNullDoc("Course", document.getDouble("Course"), jsonObject.getDouble("course"));
        if (document.getInteger("Satellites") != null) {
            int satellites = document.getInteger("Satellites");
            int satellites1 = jsonObject.getInt("satellites");
            checkSatellites(decimalFormat, satellites, satellites1);
        } else getNullDoc("Satellites", document.getDouble("Satellites"), jsonObject.getDouble("satellites"));
        if (document.get("Speed") != null) {
            Integer speed = document.getInteger("Speed");
            int speed1 = jsonObject.getInt("speed");
            checkSpeed(decimalFormat, speed, speed1);
        } else getNullDoc("Speed", document.getDouble("Speed"), jsonObject.getDouble("speed"));
        if (document.get("Mileage") != null) {
            int mileage = document.getDouble("Mileage").intValue();
            int xdeltaMileage = (int) jsonObject.getDouble("mileage");
            checkMileage(mileage, xdeltaMileage);
        } else getNullDoc("Mileage", document.getDouble("Mileage"), jsonObject.getDouble("mileage"));
        if (document.get("Ignition") != null) {
            Boolean ignition = document.getBoolean("Ignition");
            boolean ignition1 = jsonObject.getBoolean("ignition");
            checkIgnition(ignition, ignition1);
        } else getNullDoc("Ignition", document.get("Ignition"), jsonObject.get("ignition"));

        String accelerationMaxX1 = "accelerationMaxX";
        String accelerationMaxY1 = "accelerationMaxY";
        String accelerationMaxZ1 = "accelerationMaxZ";
        String accelerationAvgX1 = "accelerationAvgX";
        String accelerationAvgY1 = "accelerationAvgY";
        String accelerationAvgZ1 = "accelerationAvgZ";
        checkAccTresholdExceeding(jsonObject, document, accelerationMaxX1, accelerationMaxY1, accelerationMaxZ1, accelerationAvgX1, accelerationAvgY1, accelerationAvgZ1);

        Date deviceTime = document.getDate("DeviceTime");
        Date receivedTime = document.getDate("ReceivedTime");

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        format1.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date deviceTime1 = new Date();
        Date receivedTime1 = new Date();
        try {
            deviceTime1 = format1.parse((String) jsonObject.get("deviceTime"));
            receivedTime1 = format1.parse((String) jsonObject.get("receivedTime"));
        } catch (ParseException e) {
            Logger.getLogger(">>>").info(e.getMessage());
        }
        mongoResponse(document);
        checkDeviceTime(parse, parse1, deviceTime, deviceTime1);
        checkReceivedTime(parse, parse1, receivedTime, receivedTime1);
        showDocument();
    }

    private String checkAltitude(DecimalFormat decimalFormat, int altitude, int altitude1) {
        try {
            Assert.assertTrue("", decimalFormat.format(Integer.valueOf(altitude)).equals(decimalFormat.format(Integer.valueOf(altitude1))));
            document.remove("MongoDB Longitude: ", altitude);
            document.remove("Response Longitude: ", altitude1);
            document.put("MongoDB Longitude: ", altitude);
            document.put("Response Longitude: ", altitude1);
            Logger.getLogger(">>>").info("get longitude from Mongo: " + altitude);
            Logger.getLogger(">>>").info("get longitude from getResponse: " + altitude1);
        } catch (AssertionError e) {
            document.remove("MongoDB Longitude: ", altitude);
            document.remove("Response Longitude: ", altitude1);
            document.put("MongoDB Longitude: ", altitude);
            document.put("Response Longitude: ", altitude1);
        }
        return "Longitude is checked";
    }

    private void checkDeviceImei(String imei, Object deviceCode) {
        Assert.assertEquals(deviceCode, imei);
        document.remove("MongoDB imei: ", imei);
        document.remove("Response imei: ", deviceCode);
        document.put("MongoDB imei: ", imei);
        document.put("Response imei: ", deviceCode);
    }

    private void getNullDoc(String field, Object mongoField, Object responseField) {
        if (mongoField instanceof Boolean || responseField instanceof Boolean) {
            Assert.assertEquals(responseField, false);
            document.remove("MongoDB " + field + ": ", mongoField);
            document.remove("Response " + field + ": ", responseField);
            document.put("MongoDB " + field + ": ", "null");
            document.put("Response " + field + ": ", responseField);
        } else {
            Assert.assertEquals(responseField, 0.0);
            document.remove("MongoDB " + field + ": ", mongoField);
            document.remove("Response " + field + ": ", responseField);
            document.put("MongoDB " + field + ": ", "null");
            document.put("Response " + field + ": ", responseField);
        }
    }

    private void checkAccTresholdExceeding(JSONObject jsonObject, Document document1, String
            accelerationMaxX1, String accelerationMaxY1, String accelerationMaxZ1, String accelerationAvgX1, String
                                                   accelerationAvgY1, String accelerationAvgZ1) {
        Object extendedData = document1.get("ExtendedData", Document.class).get("Acceleration:MAX Aceleration X");
        if (extendedData != null) {
            checkAccelerationMaxX(extendedData, jsonObject.getDouble(accelerationMaxX1));
        } else checkAccelerationMaxX(extendedData, jsonObject.get(accelerationMaxX1));
        Object extendedData1 = document1.get("ExtendedData", Document.class).get("Acceleration:MAX Aceleration Y");
        if (extendedData1 != null) {
            checkAccelerationMaxY(extendedData1, jsonObject.getDouble(accelerationMaxY1));
        } else checkAccelerationMaxY(extendedData1, jsonObject.get(accelerationMaxY1));
        Object extendedData2 = document1.get("ExtendedData", Document.class).get("Acceleration: MAX Aceleration Z");
        if (extendedData2 != null) {
            checkAccelerationMaxZ(extendedData2, jsonObject.getDouble(accelerationMaxZ1));
        } else checkAccelerationMaxZ(extendedData2, jsonObject.get(accelerationMaxZ1));
        Object extendedData3 = document1.get("ExtendedData", Document.class).get("Acceleration:Average Aceleration X");
        if (extendedData3 != null) {
            checkAccelerationAvgX(extendedData3, jsonObject.getDouble(accelerationAvgX1));
        } else
            checkAccelerationAvgX(extendedData3, jsonObject.get(accelerationAvgX1));
        Object extendedData4 = document1.get("ExtendedData", Document.class).get("Acceleration:Average Aceleration Y");
        if (extendedData4 != null) {
            checkAccelerationAvgY(extendedData4, jsonObject.getDouble(accelerationAvgY1));
        } else checkAccelerationAvgY(extendedData4, jsonObject.get(accelerationAvgY1));
        Object extendedData5 = document1.get("ExtendedData", Document.class).get("Acceleration: Average Aceleration Z");
        if (extendedData5 != null) {
            checkAccelerationAvgZ(extendedData5, jsonObject.getDouble(accelerationAvgZ1));
        } else checkAccelerationAvgZ(extendedData5, jsonObject.get(accelerationAvgZ1));
        Object jsonElement = document1.get("ExtendedData", Document.class).get("Duration");
        if (jsonElement != null) {
            checkDuration(jsonElement, jsonObject.getDouble("accelerationDuration"));
        } else checkDuration(jsonElement, String.valueOf(jsonObject.get("accelerationDuration")));
    }

    private String checkDuration(Object o2, Object duration) {
        try {
            Asserts.check(o2.equals(duration), "MongoDB Duration: " + o2 + "Response Duration: " + duration);
            document.remove("MongoDB Duration: ", o2);
            document.remove("Response Duration: ", duration);
            document.put("MongoDB Duration: ", o2);
            document.put("Response Duration: ", duration);
            Logger.getLogger(">>>").info("get Duration from Mongo: " + o2);
            Logger.getLogger(">>>").info("get Duration from getResponse: " + duration);
        } catch (NullPointerException e) {
            Assert.assertEquals(duration, "0");
            document.remove("MongoDB Duration: ", o2);
            document.remove("Response Duration: ", duration);
            document.put("MongoDB Duration: ", "null");
            document.put("Response Duration: ", duration);
        }
        return "Duration is chcecked";
    }

    private String checkAccelerationAvgZ(Object o1, Object accelerationAvgZ) {
        try {
            Asserts.check(o1.equals(accelerationAvgZ), "MongoDB Duration: " + o1 + "Response Duration: " + accelerationAvgZ);
            document.remove("MongoDB AccelerationAvgZ: ", o1);
            document.remove("Response AccelerationAvgZ: ", accelerationAvgZ);
            document.put("MongoDB AccelerationAvgZ: ", o1);
            document.put("Response AccelerationAvgZ: ", accelerationAvgZ);
            Logger.getLogger(">>>").info("get AccelerationAvgZ from Mongo: " + o1);
            Logger.getLogger(">>>").info("get AccelerationAvgZ from getResponse: " + accelerationAvgZ);
        } catch (NullPointerException e) {
            Assert.assertEquals(accelerationAvgZ, 0);
            document.remove("MongoDB AccelerationAvgZ: ", o1);
            document.remove("Response AccelerationAvgZ: ", accelerationAvgZ);
            document.put("MongoDB AccelerationAvgZ: ", "null");
            document.put("Response AccelerationAvgZ: ", accelerationAvgZ);

        }
        return "AccelerationAvgZ is chcecked";
    }

    private String checkAccelerationMaxZ(Object o, Object accelerationMaxZ) {
        try {
            Asserts.check(o.equals(accelerationMaxZ), "MongoDB Duration: " + o + "Response Duration: " + accelerationMaxZ);
            document.remove("MongoDB AccelerationMaxZ: ", o);
            document.remove("Response AccelerationMaxZ: ", accelerationMaxZ);
            document.put("MongoDB AccelerationMaxZ: ", o);
            document.put("Response AccelerationMaxZ: ", accelerationMaxZ);
            Logger.getLogger(">>>").info("get AccelerationMaxZ from Mongo: " + o);
            Logger.getLogger(">>>").info("get AccelerationMaxZ from getResponse: " + accelerationMaxZ);
        } catch (NullPointerException e) {
            Assert.assertEquals(accelerationMaxZ, 0);
            document.remove("MongoDB AccelerationMaxZ: ", o);
            document.remove("Response AccelerationMaxZ: ", accelerationMaxZ);
            document.put("MongoDB AccelerationMaxZ: ", "null");
            document.put("Response AccelerationMaxZ: ", accelerationMaxZ);
        }
        return "AccelerationMaxZ is chcecked";
    }

    @Attachment("Проверенные поля: ")
    private String showDocument() {
        return gson.toJson(document);
    }

    @Attachment("MongoDB Ответ из коллекции ")
    private String nullDeviceCodeReport(String s) {
        return "Ответ:  " + s;
    }

    private void zeroFieldReport(String name, Object object) {
        document.put(name, object);
    }

    private String checkAccelerationAvgY(Object o1, Object accelerationAvgY) {
        try {
            Asserts.check(o1.equals(accelerationAvgY), "MongoDB Duration: " + o1 + "Response Duration: " + accelerationAvgY);
            document.remove("MongoDB AccelerationAvgY: ", o1);
            document.remove("Response AccelerationAvgY: ", accelerationAvgY);
            document.put("MongoDB AccelerationAvgY: ", o1);
            document.put("Response AccelerationAvgY: ", accelerationAvgY);
            Logger.getLogger(">>>").info("get AccelerationAvgY from Mongo: " + o1);
            Logger.getLogger(">>>").info("get AccelerationAvgY from getResponse: " + accelerationAvgY);
        } catch (NullPointerException e) {
            Assert.assertEquals(accelerationAvgY, 0);
            document.remove("MongoDB AccelerationAvgY: ", o1);
            document.remove("Response AccelerationAvgY: ", accelerationAvgY);
            document.put("MongoDB AccelerationAvgY: ", "null");
            document.put("Response AccelerationAvgY: ", accelerationAvgY);
        }
        return "AccelerationAvgY is chcecked";
    }

    private String checkAccelerationMaxY(Object o, Object accelerationMaxY) {
        try {
            Asserts.check(o.equals(accelerationMaxY), "MongoDB Duration: " + o + "Response Duration: " + accelerationMaxY);
            document.remove("MongoDB AccelerationMaxY: ", o);
            document.remove("Response AccelerationMaxY: ", accelerationMaxY);
            document.put("MongoDB AccelerationMaxY: ", o);
            document.put("Response AccelerationMaxY: ", accelerationMaxY);
            Logger.getLogger(">>>").info("get AccelerationMaxY from Mongo: " + o);
            Logger.getLogger(">>>").info("get AccelerationMaxY from getResponse: " + accelerationMaxY);
        } catch (NullPointerException e) {
            Assert.assertEquals(accelerationMaxY, 0);
            document.remove("MongoDB AccelerationMaxY: ", o);
            document.remove("Response AccelerationMaxY: ", accelerationMaxY);
            document.put("MongoDB AccelerationMaxY: ", "null");
            document.put("Response AccelerationMaxY: ", accelerationMaxY);
        }

        return "AccelerationMaxY is chcecked";
    }

    private String checkAccelerationAvgX(Object o1, Object accelerationAvgX) {
        try {
            Asserts.check(o1.equals(accelerationAvgX), "MongoDB Duration: " + o1 + "Response Duration: " + accelerationAvgX);
            document.remove("MongoDB AccelerationAvgX: ", o1);
            document.remove("Response AccelerationAvgX: ", accelerationAvgX);
            document.put("MongoDB AccelerationAvgX: ", o1);
            document.put("Response AccelerationAvgX: ", accelerationAvgX);
            Logger.getLogger(">>>").info("get AccelerationAvgX from Mongo: " + o1);
            Logger.getLogger(">>>").info("get AccelerationAvgX from getResponse: " + accelerationAvgX);
        } catch (NullPointerException e) {
            Assert.assertEquals(accelerationAvgX, 0);
            document.remove("MongoDB AccelerationAvgX: ", o1);
            document.remove("Response AccelerationAvgX: ", accelerationAvgX);
            document.put("MongoDB AccelerationAvgX: ", "null");
            document.put("Response AccelerationAvgX: ", accelerationAvgX);
        }
        return "AccelerationAvgX is chcecked";
    }

    private String checkAccelerationMaxX(Object o, Object accelerationMaxX) {
        try {
            Asserts.check(o.equals(accelerationMaxX), "MongoDB Duration: " + o + "Response Duration: " + accelerationMaxX);
            document.remove("MongoDB AccelerationMaxX: ", o);
            document.remove("Response AccelerationMaxX: ", accelerationMaxX);
            document.put("MongoDB AccelerationMaxX: ", o);
            document.put("Response AccelerationMaxX: ", accelerationMaxX);
            Logger.getLogger(">>>").info("get AccelerationMaxX from Mongo: " + o);
            Logger.getLogger(">>>").info("get AccelerationMaxX from getResponse: " + accelerationMaxX);
        } catch (NullPointerException e) {
            Assert.assertEquals(accelerationMaxX, 0);
            document.remove("MongoDB AccelerationMaxX: ", o);
            document.remove("Response AccelerationMaxX: ", accelerationMaxX);
            document.put("MongoDB AccelerationMaxX: ", "null");
            document.put("Response AccelerationMaxX: ", accelerationMaxX);
        }
        return "AccelerationMaxX is chcecked";
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("MongoDB: запрос в коллекцию \"DeviceFullRecords\"")
    public void mongoRequest() throws ParseException {
        getRequest();
    }

    @Attachment("Запрос в коллекцию \"DeviceFullRecords\"")
    private String getRequest() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        Date dateFrom;
        Date dateTo;
        try {
            dateFrom = format.parse(executor.getFrom());
            dateTo = format.parse(executor.getTo());
        } catch (ParseException e) {
            format = new SimpleDateFormat("yyyy/MM/dd'T'HH'%3A'mm'%3A'ss");
            dateFrom = format.parse(executor.getFrom());
            dateTo = format.parse(executor.getTo());
        }
        SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        String format1 = format3.format(dateFrom.getTime() - 10800000);
        String format2 = format3.format(dateTo.getTime() - 10800000);
        Logger.getLogger(">>>").info("db.DeviceFullRecords.find({\"DeviceTime\" :{$gte: ISODate(\"" + format1 + "\"),$lte : ISODate(\"" + format2 + "\")}}).sort({\"DeviceTime\":1}).limit(" + executor.getLimit() + ").skip(" + executor.getSkip() + ")");
        return "db.DeviceFullRecords.find({\"DeviceTime\" :{$gte: ISODate(\"" + format1 + "\"),$lte : ISODate(\"" + format2 + "\")}}).sort({\"DeviceTime\":1}).limit(" + executor.getLimit() + ").skip(" + executor.getSkip() + ")";
    }

    @Attachment("MongoDB: Ответ из коллекции DeviceFullRecords")
    private String mongoResponse(Document deviceFullRecords) {
        Logger.getLogger(">>>").info(gson.toJson(deviceFullRecords));
        return gson.toJson(deviceFullRecords);
    }

    private String checkIgnition(Boolean ignition, boolean ignition1) {
        try {
            Assert.assertTrue("", ignition.equals(ignition1));
            this.document.remove("MongoDB Ignition: ", ignition);
            this.document.remove("Response Ignition: ", ignition1);
            this.document.put("MongoDB Ignition: ", ignition);
            this.document.put("Response Ignition: ", ignition1);
            Logger.getLogger(">>>").info("get ignition from Mongo: " + ignition);
            Logger.getLogger(">>>").info("get ignition from getResponse: " + ignition1);
        } catch (AssertionError e) {
            this.document.remove("MongoDB Ignition: ", ignition);
            this.document.remove("Response Ignition: ", ignition1);
            this.document.put("MongoDB Ignition: ", ignition);
            this.document.put("Response Ignition: ", ignition1);
        }
        return "Ignition is checked";
    }

    private String checkSpeed(DecimalFormat decimalFormat, Integer speed, Integer speed1) {
        try {
            Assert.assertTrue("", decimalFormat.format(speed).equals(decimalFormat.format(speed1)));
            document.remove("MongoDB Speed: ", speed);
            document.remove("Response Speed: ", speed1);
            document.put("MongoDB Speed: ", speed);
            document.put("Response Speed: ", speed1);
            Logger.getLogger(">>>").info("get speed from Mongo: " + speed);
            Logger.getLogger(">>>").info("get speed from getResponse: " + speed1);
        } catch (AssertionError e) {
            document.remove("MongoDB Speed: ", speed);
            document.remove("Response Speed: ", speed1);
            document.put("MongoDB Speed: ", speed);
            document.put("Response Speed: ", speed1);
        }
        return "Speed is checked";
    }

    private String checkMileage(Integer mileage, Integer xdeltaMileage) {
        try {
            Assert.assertTrue("", mileage == xdeltaMileage);
            document.remove("MongoDB Mileage: ", mileage);
            document.remove("Response Mileage: ", xdeltaMileage);
            document.put("MongoDB Mileage: ", mileage);
            document.put("Response Mileage: ", xdeltaMileage);
            Logger.getLogger(">>>").info("get Mileage from Mongo: " + mileage);
            Logger.getLogger(">>>").info("get Mileage from getResponse: " + xdeltaMileage);
        } catch (AssertionError e) {
            document.remove("MongoDB Mileage: ", mileage);
            document.remove("Response Mileage: ", xdeltaMileage);
            document.put("MongoDB Mileage: ", mileage);
            document.put("Response Mileage: ", xdeltaMileage);

        }
        return "Mileage is checked";
    }

    private String checkSatellites(DecimalFormat decimalFormat, Integer satellites, Integer satellites1) {
        try {
            Assert.assertTrue("", decimalFormat.format(satellites).equals(decimalFormat.format(satellites1)));
            document.remove("MongoDB Satellites: ", satellites);
            document.remove("Response Satellites: ", satellites1);
            document.put("MongoDB Satellites: ", satellites);
            document.put("Response Satellites: ", satellites1);
            Logger.getLogger(">>>").info("get satellites from Mongo: " + satellites);
            Logger.getLogger(">>>").info("get satellites from getResponse: " + satellites1);
        } catch (AssertionError e) {
            document.remove("MongoDB Satellites: ", satellites);
            document.remove("Response Satellites: ", satellites1);
            document.put("MongoDB Satellites: ", satellites);
            document.put("Response Satellites: ", satellites1);
        }
        return "Satellites are checked";
    }

    private String checkCourse(DecimalFormat decimalFormat, Object course, Integer course1) {
        try {
            Assert.assertTrue(decimalFormat.format(course).equals(decimalFormat.format(course1)));
            document.remove("MongoDB Course: ", course);
            document.remove("Response Course: ", course1);
            document.put("MongoDB Course: ", course);
            document.put("Response Course: ", course1);
            Logger.getLogger(">>>").info("get course from Mongo: " + course);
            Logger.getLogger(">>>").info("get course from getResponse: " + course1);
        } catch (AssertionError e) {
            document.remove("MongoDB Course: ", course);
            document.remove("Response Course: ", course1);
            document.put("MongoDB Course: ", course);
            document.put("Response Course: ", course1);
        }
        return "Course is checked";
    }

    private String checkLongitude(DecimalFormat decimalFormat, Integer longitude, Integer longitude1) {
        try {
            Assert.assertTrue("", decimalFormat.format(Integer.valueOf(longitude)).equals(decimalFormat.format(Integer.valueOf(longitude1))));
            document.remove("MongoDB Longitude: ", longitude);
            document.remove("Response Longitude: ", longitude1);
            document.put("MongoDB Longitude: ", longitude);
            document.put("Response Longitude: ", longitude1);
            Logger.getLogger(">>>").info("get longitude from Mongo: " + longitude);
            Logger.getLogger(">>>").info("get longitude from getResponse: " + longitude1);
        } catch (AssertionError e) {
            document.remove("MongoDB Longitude: ", longitude);
            document.remove("Response Longitude: ", longitude1);
            document.put("MongoDB Longitude: ", longitude);
            document.put("Response Longitude: ", longitude1);
        }
        return "Longitude is checked";
    }

    private String checkLatitude(DecimalFormat decimalFormat, Integer latitude, Integer latitude1) {
        try {
            Assert.assertTrue("", decimalFormat.format(Integer.valueOf(latitude)).equals(decimalFormat.format(Integer.valueOf(latitude1))));
            document.remove("MongoDB Latitude: ", latitude);
            document.remove("Response Latitude: ", latitude1);
            document.put("MongoDB Latitude: ", latitude);
            document.put("Response Latitude: ", latitude1);
            Logger.getLogger(">>>").info("get Latitude from Mongo: " + latitude);
            Logger.getLogger(">>>").info("get Latitude from getResponse: " + latitude1);
        } catch (AssertionError e) {
            document.remove("MongoDB Latitude: ", latitude);
            document.remove("Response Latitude: ", latitude1);
            document.put("MongoDB Latitude: ", latitude);
            document.put("Response Latitude: ", latitude1);
        }
        return "Latitude is checked";
    }

    private String checkDeviceTime(Date from, Date to, Date mongoTime, Date responseTime) {
        try {
//        Assert.assertTrue("", responseTime == mongoTime);
            Assert.assertTrue("", mongoTime.getTime() >= from.getTime());
            Assert.assertTrue("", responseTime.getTime() >= from.getTime());
            Assert.assertTrue("", mongoTime.getTime() <= to.getTime());
            Assert.assertTrue("", responseTime.getTime() <= to.getTime());
            document.remove("MongoDB deviceTime: ", mongoTime);
            document.remove("Response deviceTime: ", responseTime);
            document.put("MongoDB deviceTime: ", mongoTime);
            document.put("Response deviceTime: ", responseTime);
            Logger.getLogger(">>>").info("get deviceTime from Mongo: " + mongoTime);
            Logger.getLogger(">>>").info("get  deviceTime from getResponse: " + responseTime);
            Logger.getLogger(">>>").info("get deviceTime from request: " + from);
            Logger.getLogger(">>>").info("get deviceTime to request: " + to);
        } catch (NullPointerException e) {
            document.remove("MongoDB deviceTime: ", mongoTime);
            document.remove("Response deviceTime: ", responseTime);
            document.put("MongoDB deviceTime: ", mongoTime);
            document.put("Response deviceTime: ", responseTime);
        }
        return "Device time is checked";
    }

    private String checkReceivedTime(Date from, Date to, Date mongoTime, Date responseTime) {
        try {
//        Assert.assertTrue((mongoTime) == responseTime);
            Assert.assertTrue("", from.getTime() <= (mongoTime.getTime()));
            Assert.assertTrue("", from.getTime() <= responseTime.getTime());
            document.remove("MongoDB receivedTime: ", mongoTime);
            document.remove("Response receivedTime: ", responseTime);
            document.put("MongoDB receivedTime: ", mongoTime);
            document.put("Response receivedTime: ", responseTime);
            Logger.getLogger(">>>").info("get receivedTime from Mongo: " + mongoTime);
            Logger.getLogger(">>>").info("get  receivedTime from getResponse: " + responseTime);
            Logger.getLogger(">>>").info("get receivedTime from request: " + from);
            Logger.getLogger(">>>").info("get receivedTime to request: " + to);
        } catch (NullPointerException e) {
            document.remove("MongoDB receivedTime: ", mongoTime);
            document.remove("Response receivedTime: ", responseTime);
            document.put("MongoDB receivedTime: ", mongoTime);
            document.put("Response receivedTime: ", responseTime);
        }
        return "Received time is checked";
    }
}