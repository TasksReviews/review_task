package reports;


import api.dev.GetExecutor;
import api.dev.MongoDevApiConnection;
import api.dev.TrackRecordedPackages;
import api.utils.Prop;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.t1.core.AbstractClass;
import cucumber.api.java.ContinueNextStepsFor;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

public class ApiDeviceTelematicDataTrackRecordedPackagesReport {
    private GetExecutor executor = new TrackRecordedPackages();
    private GetRequest execute;
    private Date parse;
    private Date parse1;
    private String datePattern = "yyyy-MM-dd HH:mm:ss:SSS";
    private LinkedHashSet stack;
    private String trackRecords1;

    public ApiDeviceTelematicDataTrackRecordedPackagesReport() {
        trackRecords1 = "trackRecords";
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void request(String format, String format1, Date time) throws UnirestException {
        executor.setFrom(format);
        executor.setTo(format1);
        executor.setLimit("10");
        execute = executor.execute();
        getUrl(execute);
        getRequestTime(DateFormatUtils.format(time, datePattern));
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void request(String format, String format1, String arg0, Date time) throws UnirestException {
        executor.setFrom(format);
        executor.setTo(format1);
        executor.setLimit("10");
        executor.setSkip(arg0);
        execute = executor.execute();
        getUrl(execute);
        getRequestTime(DateFormatUtils.formatUTC(time, datePattern));

    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void requestWithLimit(String format, String format1, int arg0, Date time) throws UnirestException {
        executor.setFrom(format);
        executor.setTo(format1);
        executor.setLimit(String.valueOf(arg0));
        execute = executor.execute();
        getUrl(execute);
        getRequestTime(DateFormatUtils.formatUTC(time, datePattern));

    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void requestWithLessLimit(String format, String format1, int arg0, Date time) throws UnirestException {
        executor.setFrom(format);
        executor.setTo(format1);
        executor.setLimit(String.valueOf(arg0 - 1));
        execute = executor.execute();
        getUrl(execute);
        getRequestTime(DateFormatUtils.formatUTC(time, datePattern));

    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void requestWithoutLimit(String format, String format1, Date time) throws UnirestException {
        executor.setFrom(format);
        executor.setTo(format1);
        execute = executor.execute();
        getUrl(execute);
        getRequestTime(DateFormatUtils.formatUTC(time, datePattern));
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void request(String format, String format1, String arg0, String arg1, Date time) throws UnirestException {
        executor.setFrom(format);
        executor.setTo(format1);
        executor.setLimit(arg0);
        executor.setSkip(arg1);
        execute = executor.execute();
        getUrl(execute);
        getRequestTime(DateFormatUtils.formatUTC(time, datePattern));
    }

    public void checkCountNotNull() {
        JSONArray trackRecords;
        try {
            trackRecords = execute.asJson().getBody().getObject().getJSONArray(trackRecords1);
            Assert.assertTrue(trackRecords.length() > 0);
        } catch (UnirestException e) {
            Logger.getLogger(">>>").info(e.getMessage());
        }

    }

    @Attachment("Валидация: количествово записей в коллекции \"DeviceFullRecords\" = {0}")
    private String getReportCount(int length) {
        return "Количествово записей в коллекции DeviceFullRecords = " + length;
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void limitRequest(String format, String format1, String arg0, Date time) {
        executor.setFrom(format);
        executor.setTo(format1);
        executor.setLimit(arg0);
        execute = executor.execute(executor.getFrom(), executor.getTo(), executor.getLimit());
        getUrl(execute);
        getRequestTime(DateFormatUtils.formatUTC(time, datePattern));
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET запроса: ")
    public void negativeRequest(String format, String format1) {
        executor.setFrom(format);
        executor.setTo(format1);
        executor.setLimit("10");
        execute = executor.execute(executor.getFrom(), executor.getTo(), executor.getLimit());
        getUrl(execute);
    }

    @Attachment("URL запроса: ")
    private String getUrl(GetRequest execute) {
        return execute.getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ GET запроса: ")
    public void getResponse() throws UnirestException {
        getStatusText();
        getResponseBody();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ GET запроса: ")
    public void getLimitResponse() throws UnirestException {
        getStatusText();
        getResponseBody();
        checkCountLimit(Integer.parseInt(executor.getLimit()));
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ GET запроса:")
    public void getNegativeResponse() throws UnirestException {
        HttpResponse<String> response = execute.asString();
        try {
            Assert.assertEquals(response.getStatus(), 400);
            getStatusText();
            getNegativeBody();
        } catch (AssertionError error) {
            Assert.fail(String.format("Invalid status code. Expected [400, Bad Request], but found %s, %s",
                    response.getStatus(), response.getStatusText()));
        }
    }

    //Проверка сообщения в теле запроса для некорректного Limit
    public void checkResponseBodyLimit(String limit) throws UnirestException {
        String message = "";
        try {
            //Для limit типа Integer
            if (Integer.parseInt(limit) <= 0) {
                message = "{\"Limit\":[\"Value for Limit must be between 1 and 2147483647.\"]}";
            } else {
                return;
            }
        } catch (NumberFormatException e) {
            switch (limit) {
                case "":
                    message = "{\"Limit\":[\"The value '' is invalid.\"]}";
                    break;
                case "2018/05/18%2020:00:00":
                    message = String.format("{\"Limit\":[\"The value '%s' is not valid for Limit.\"]}", limit.replace("%20", " "));
                    break;
                default:
                    message = String.format("{\"Limit\":[\"The value '%s' is not valid for Limit.\"]}", limit);
                    break;
            }

        }
        HttpResponse<String> response = execute.asString();
        try {

            Assert.assertEquals(response.getBody(), message);
            validationResponseBody(response.getBody());
        } catch (AssertionError e) {
            Assert.fail(String.format("Response body is not valid. Expected [%s] but found [%s]", message, response.getBody()));
        }
    }

    @Attachment("Валидация: получено верное тело ответа")
    private String validationResponseBody(String message) {
        return String.format("Тело ответа верное: %s", message);
    }

    @Attachment("Валидация: кол-во записей <= {0}")
    private boolean checkCountLimit(int i) throws UnirestException {
        try {
            return execute.asJson().getBody().getObject().getJSONArray(trackRecords1).length() <= i;
        } catch (UnirestException | JSONException e) {
            return execute.asString().getBody().equalsIgnoreCase("There are no track recording devices");
        }
    }

    @Attachment("Тело ответа: ")
    private String getResponseBody() throws UnirestException {
        try {
            getStack();
            return String.valueOf(execute.asJson().getBody());
        } catch (Exception e) {
            return execute.asString().getBody();
        }
    }

    private Set<String> getStack() {
        stack = new LinkedHashSet<>();
        try {
            JSONArray trackRecords = execute.asJson().getBody().getObject().getJSONArray(trackRecords1);
            for (int i = 0; i < trackRecords.length(); i++) {
                Object deviceCode = trackRecords.getJSONObject(i).get("imei");
                stack.add(Prop.getDeviceCodePIDR(String.valueOf(deviceCode)));
            }
        } catch (UnirestException | JSONException e) {
            Logger.getLogger(">>>").info(e.getMessage());
        }
        return stack;
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ GET запроса: ")
    public void getNoResponse() throws UnirestException {
        getStatusText();
        getNoResponseBody();
    }

    @Attachment("Тело ответа: ")
    private String getNoResponseBody() throws UnirestException {
        try {
            execute.asJson().getBody();
        } catch (Exception noResponseBody) {
            return execute.asString().getBody();
        }
        return execute.asString().getBody();
    }

    @Attachment("Код состояния запроса: ")
    private String getStatusText() throws UnirestException {
        return execute.asString().getStatus() + ", " + execute.asString().getStatusText();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ GET запроса: ")
    public void negativeResponse() throws UnirestException {
        getStatusText();
        getNegativeBody();
    }

    @Attachment("Тело ответа: ")
    private String getNegativeBody() throws UnirestException {
        return execute.asString().getBody();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("MongoDB: запрос в коллекцию \"DeviceFullRecords\"")
    public void mongoRequest() throws UnirestException {
        try {
            getRequest(stack);
            getReportCount(execute.asJson().getBody().getObject().getJSONArray(trackRecords1).length());
        } catch (UnirestException | JSONException | NullPointerException e) {
            getErrorReport(execute.asString().getBody());
        }
    }

    @Attachment("Ответ из коллекции MongoDB: ")
    private String getErrorReport(String body) {
        return body;
    }

    @Attachment("Запрос в коллекцию \"DeviceFullRecords\": ")
    private String getRequest(LinkedHashSet stack) {
        StringBuilder prettyStack = getPrettyStack(stack);
        String pattern = "yyyy-MM-dd'%20'HH:mm:ss";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            parse = format.parse(executor.getFrom());
            parse1 = format.parse(executor.getTo());
        } catch (ParseException e) {
            String pattern1 = "yyyy/MM/dd'%20'HH:mm:ss";
            format = new SimpleDateFormat(pattern1);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            try {
                parse = format.parse(executor.getFrom());
                parse1 = format.parse(executor.getTo());
            } catch (ParseException e1) {
                zeroFieldReport("Request date: ", parse);
                zeroFieldReport("Request date: ", parse1);
            }
        }
        String pattern1 = "yyyy-MM-dd'T'HH:mm:ss.sss";
        SimpleDateFormat format1 = new SimpleDateFormat(pattern1);
        String s = "Z\"),$lte : ISODate(\"";
        Logger.getLogger(">>>").info("db.DeviceFullRecords.find({\"DeviceCode\":{$in:" + prettyStack + "},\"DeviceTime\" :{$gte: ISODate(\"" + format1.format(parse) + s + format1.format(parse1) + "Z\")}}).limit(" + executor.getLimit() + ")");
        if (executor.getSkip().equalsIgnoreCase("0")) {
            return "db.DeviceFullRecords.find({\"DeviceCode\":{$in:" + prettyStack + "},\"DeviceTime\" :{$gte: ISODate(\"" + format1.format(parse) + s + format1.format(parse1) + "Z\")}}).limit(" + executor.getLimit() + ")";
        } else
            return "db.DeviceFullRecords.find({\"DeviceCode\":{$in:" + prettyStack + "},\"DeviceTime\" :{$gte: ISODate(\"" + format1.format(parse) + s + format1.format(parse1) + "Z\")}}).limit(" + executor.getLimit() + ").skip(" + executor.getSkip() + ")";
    }

    private StringBuilder getPrettyStack(LinkedHashSet stack) {
        StringBuilder stringBuilder = new StringBuilder();
        Object[] objects = stack.toArray();
        stringBuilder.append("[");
        if (objects.length == 1) {
            {
                stringBuilder.append("\"");
                stringBuilder.append(objects[0]);
                stringBuilder.append("\"");
            }
        } else if (objects.length > 1) {
            for (int i = 0; i <= objects.length - 2; i++) {
                stringBuilder.append("\"");
                stringBuilder.append(objects[i]);
                stringBuilder.append("\",");
            }
            stringBuilder.append("\"");
            stringBuilder.append(objects[objects.length - 1]);
            stringBuilder.append("\"");
        }
        stringBuilder.append("]");
        return stringBuilder;
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Валидация: сравнение телематических записей трэка из коллекции \"DeviceFullRecords\" с ответом WebAPI")
    public void dataFromMongo() throws ParseException, UnirestException {
        ArrayList<Document> list = new ArrayList<>();
        MongoClient client = new MongoDevApiConnection().getClient();
        try {
            DBObject dbObject = getDbFilter();
            MongoCollection<Document> deviceFullRecords1 = client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("DeviceFullRecords");
            if (execute.asString().getBody().equalsIgnoreCase("There are no track recording devices")) {
                Assert.assertTrue(deviceFullRecords1.count((Bson) dbObject) == 0);
                zeroFieldReport("Количесвто записей в коллекции DeviceFullRecords ", deviceFullRecords1.count((Bson) dbObject));
                return;
            }
            FindIterable<Document> deviceFullRecords = deviceFullRecords1.find((Bson) dbObject).sort(new BasicDBObject("DeviceTime", 1)).limit(Integer.parseInt(executor.getLimit())).skip(Integer.parseInt(executor.getSkip()));
            deviceFullRecords.forEach((Consumer<? super Document>) list::add);
        } catch (NumberFormatException | NullPointerException e) {
            nullDeviceCodeReport(String.valueOf(list));
        }
        try {
            JSONArray localRecords = execute.asJson().getBody().getObject().getJSONArray(trackRecords1);
            for (int i = 0; i < localRecords.length(); i++) {
                checkFields(list.get(i), localRecords.getJSONObject(i), localRecords.getJSONObject(i).get("imei"));
            }
        } catch (Exception e) {
            zeroFieldReport(trackRecords1, null);
        }finally {
            client.close();
        }
    }

    private DBObject getDbFilter() throws ParseException {
        String localPattern = "yyyy-MM-dd'T'HH:mm:ss";
        String format = DateFormatUtils.format(parse.getTime(), localPattern);
        String format1 = DateFormatUtils.format(parse1.getTime(), localPattern);
        SimpleDateFormat format2 = new SimpleDateFormat(localPattern);
        format2.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parse2 = format2.parse(format);
        Date parse3 = format2.parse(format1);
        return BasicDBObjectBuilder.start()
                .add("DeviceCode", new BasicDBObject("$in", stack))
                .add("DeviceTime", new BasicDBObject("$gte", parse2).append("$lte", parse3))
                .get();
    }

    @Attachment("Код недоступен")
    private String nullDeviceCodeReport(String s) {
        return "Ответ:  " + s;
    }

    @Attachment("Валидация поля: ")
    private String zeroFieldReport(String name, Object object) {
        return name + "== " + object;
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Проверка полей для ТМУ с кодом {2}")
    private void checkFields(Document document, JSONObject jsonObject, Object device) throws ParseException {
        StringBuilder stringBuilder = new StringBuilder();
        DecimalFormat decimalFormat = new DecimalFormat("00.0");
        stringBuilder.append("Imei: \n");
        String deviceCode = "imei";
        checkField(stringBuilder, jsonObject.get(deviceCode), Prop.getDeviceImeiPIDR((String) document.get("DeviceCode")));
        stringBuilder.append("Satellites: \n");
        checkField(stringBuilder, jsonObject.get("satellites"), (document.get("Satellites")));
//        checkAcceleration(document, jsonObject, stringBuilder);
        stringBuilder.append("Latitude: \n");
        try {
            checkField(stringBuilder, decimalFormat.format(jsonObject.getDouble("latitude") / 1000000), decimalFormat.format(document.get("Latitude")));
        } catch (Exception e) {
            checkField(stringBuilder, jsonObject.getInt("latitude") / 1000000, document.get("Latitude"));
        }
        stringBuilder.append("Longitude: \n");
        try {
            checkField(stringBuilder, decimalFormat.format(jsonObject.getDouble("longitude") / 1000000), decimalFormat.format(document.get("Longitude")));
        } catch (Exception e) {
            checkField(stringBuilder, jsonObject.getInt("longitude") / 1000000, document.get("Longitude"));
        }
        stringBuilder.append("Altitude: \n");
        try {
            checkField(stringBuilder, decimalFormat.format(jsonObject.getDouble("altitude") / 1000000), decimalFormat.format(document.get("ExtendedData", Document.class).getDouble("Geopos:Altitude meters")));
        } catch (Exception e) {
            checkField(stringBuilder, jsonObject.getInt("altitude") / 1000000, document.get("ExtendedData", Document.class).getDouble("Geopos:Altitude meters"));
        }
        stringBuilder.append("Ignition: \n");
        checkField(stringBuilder, jsonObject.get("ignition"), document.get("Ignition"));
        stringBuilder.append("Course: \n");
        checkField(stringBuilder, jsonObject.get("course"), document.get("Course"));
        stringBuilder.append("Speed: \n");
        checkField(stringBuilder, jsonObject.get("speed"), document.get("Speed"));
        stringBuilder.append("Mileage: \n");
        checkField(stringBuilder, decimalFormat.format(jsonObject.getDouble("mileage")), decimalFormat.format(document.get("Mileage")));
        String pattern = "yyyy-MM-dd'T'HH:mm:ss";
        Date parse2 = new SimpleDateFormat(pattern).parse(jsonObject.getString("deviceTime"));
        Date parse3 = new SimpleDateFormat(pattern).parse(jsonObject.getString("receivedTime"));
        String format = new SimpleDateFormat(pattern).format(parse2);
        String format1 = new SimpleDateFormat(pattern).format(parse3);
        String deviceTime = DateFormatUtils.formatUTC(document.getDate("DeviceTime"), pattern);
        String receivedTime = DateFormatUtils.formatUTC(document.getDate("ReceivedTime"), pattern);
        stringBuilder.append("Device Time: \n");
        checkField(stringBuilder, format, deviceTime);
        stringBuilder.append("Received Time: \n");
        checkField(stringBuilder, format1, receivedTime);
        showDocument(stringBuilder);
    }

    private void checkAcceleration(Document document, JSONObject jsonObject, StringBuilder stringBuilder) {
        Long accelerationX = document.getLong("AccelerationX");
        String s = "WebAPI - ";
        String s1 = "MongoDB - ";
        stringBuilder.append("AccelerationX: \n");
        if (accelerationX == null) {
            stringBuilder.append(s + jsonObject.get("accelerationX") + "\n");
            stringBuilder.append(s1 + accelerationX + "\n");
        } else {
            checkField(stringBuilder, jsonObject.get("accelerationX"), accelerationX / 1000000);
        }
        stringBuilder.append("AccelerationY: \n");
        Long accelerationY = document.getLong("AccelerationY");
        if (accelerationY == null) {
            stringBuilder.append(s + jsonObject.get("accelerationY") + "\n");
            stringBuilder.append(s1 + accelerationY + "\n");
        } else {
            checkField(stringBuilder, jsonObject.get("accelerationY"), accelerationY / 1000000);
        }
        stringBuilder.append("AccelerationZ: \n");
        Long accelerationZ = document.getLong("AccelerationZ");
        if (accelerationZ == null) {
            stringBuilder.append(s + jsonObject.get("accelerationZ") + "\n");
            stringBuilder.append(s1 + accelerationZ + "\n");
        } else {
            checkField(stringBuilder, jsonObject.get("accelerationZ"), accelerationZ / 1000000);
        }
    }

    @Attachment("Проверенные поля: ")
    private String showDocument(StringBuilder stringBuilder) {
        return stringBuilder.toString();
    }

    @Attachment("Время отправки запроса")
    public String getRequestTime(String time) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(time + "Z");
        return stringBuilder.toString();
    }

    private void checkField(StringBuilder stringBuilder, Object field, Object field1) {
        if (field1 != null) {
            Assert.assertEquals(field, field1);
            stringBuilder.append("WebAPI - " + field + "\n");
            stringBuilder.append("MongoDB - " + field1 + "\n");
        } else if (field instanceof Boolean) {
            Assert.assertEquals(field, false);
            stringBuilder.append("WebAPI - " + field + "\n");
            stringBuilder.append("MongoDB - " + field1 + "\n");
        } else {
            Assert.assertEquals(field, 0);
            stringBuilder.append("WebAPI - " + field + "\n");
            stringBuilder.append("MongoDB - " + field1 + "\n");
        }
    }
}
