package reports;

import Infrastructure.Protobuf.Contracts.TelematicRecords.TelematicRecordOuterClass;
import api.dev.BinaryTrackRecordedPackages;
import api.dev.MongoDevApiConnection;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.t1.core.AbstractClass;
import cucumber.api.java.ContinueNextStepsFor;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

public class TelematicDataBinaryTrackRecordedPackagesReport {
    private BinaryTrackRecordedPackages packages = new BinaryTrackRecordedPackages();

    @ContinueNextStepsFor(AssertionError.class)
    @Step("GET Запрос: ")
    public void getRequest(String format, String format1, Calendar instance2) {
        packages.setFrom(format);
        packages.setTo(format1);
        packages.setLimit("20");
        packages.execute();
        getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("GET Запрос: ")
    public void getRequest(String format, String format1, String arg0, Calendar instance2) {
        packages.setFrom(format);
        packages.setTo(format1);
        packages.setLimit(arg0);
        packages.execute();
        getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("GET Запрос: ")
    public void getRequest(String format, String format1, int arg0, Calendar instance2) {
        packages.setFrom(format);
        packages.setTo(format1);
        packages.setLimit("10");
        packages.setLimit(String.valueOf(arg0 - 1));
        packages.execute();
        getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("GET Запрос: ")
    public void getRequest(String format, String format1, String arg0, String arg1, Calendar instance2) {
        packages.setFrom(format);
        packages.setTo(format1);
        packages.setLimit(arg0);
        packages.setSkip(arg1);
        packages.execute();
        getUrl();

    }

    @Attachment("URL GET Запроса: ")
    private String getUrl() {
        return packages.execute().getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Ответ GET запроса: ")
    public void getResponse() throws UnirestException {
        getStatus();
        getBody();
    }

    @Attachment("Статус ответа: ")
    private String getStatus() throws UnirestException {
        try {
            return packages.execute().asJson().getStatus() + ", " + packages.execute().asJson().getStatusText();
        } catch (UnirestException e) {
            return packages.execute().asString().getStatus() + ", " + packages.execute().asString().getStatusText();
        }
    }

    @Attachment("Тело ответа: ")
    private String getBody() throws UnirestException {
        try {
            return String.valueOf(packages.execute().asJson().getBody());
        } catch (UnirestException e) {
            return packages.execute().asString().getBody();
        }
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Проверка полей: ")
    public void checkRecordedFields() throws UnirestException {
        try {
            JSONArray array = packages.execute().asJson().getBody().getArray();
            if (array.length() != 0 && packages.execute().asJson().getStatus() == 200) {
                for (int i = 0; i < array.length(); i++) {
                    StringBuilder builder = new StringBuilder();
                    byte[] decode = Base64.getDecoder().decode(array.get(i).toString());
                    TelematicRecordOuterClass.TelematicRecord telematicRecord = TelematicRecordOuterClass.TelematicRecord.parseFrom(decode);
                    ArrayList<Document> documents = mongoCollectionByParams(telematicRecord.getDeviceCode());
                    builder.append("DeviceCode: ");
                    checkBinaryField(builder, telematicRecord.getDeviceCode(), documents.get(i).getString("DeviceCode"));
                    builder.append("DeviceTime: ");
                    long l1 = (telematicRecord.getDeviceTime() - 621355968000000000L) / 10000;
                    long deviceTime1 = documents.get(i).getDate("DeviceTime").getTime();
                    String formatUTC = DateFormatUtils.format(l1, "yyyy-MM-dd'T'hh:MM:ss");
                    String format = DateFormatUtils.format(deviceTime1, "yyyy-MM-dd'T'hh:MM:ss");
                    checkBinaryField(builder, formatUTC, format);
                    builder.append("ReceivedTime: ");
                    long l2 = (telematicRecord.getServerTime() - 621355968000000000L) / 10000;
                    long receivedTime1 = documents.get(i).getDate("ReceivedTime").getTime();
                    String format1UTC = DateFormatUtils.format(l2, "yyyy-MM-dd'T'hh:MM:ss");
                    String format2 = DateFormatUtils.format(receivedTime1, "yyyy-MM-dd'T'hh:MM:ss");
                    checkBinaryField(builder, format1UTC, format2);
                    builder.append("Latitude: ");
                    String format1 = new DecimalFormat("0.000").format(telematicRecord.getGpsData().getLatitude() / 1000000);
                    try {
                        String format3 = new DecimalFormat("0.000").format(documents.get(i).get("Latitude"));
                        checkBinaryField(builder, format1, format3);
                    } catch (IllegalArgumentException e) {
                        checkBinaryField(builder, format1, null);
                    }
//                    builder.append("Altitude: ");
//                    checkBinaryField(builder, telematicRecord.getGpsData().getAltitude(), documents.get(i).get("Altitude"));
                    builder.append("Longitude: ");
                    format1 = new DecimalFormat("0.0000").format(telematicRecord.getGpsData().getLongitude() / 1000000);
                    try {
                        String format3 = new DecimalFormat("0.0000").format(documents.get(i).get("Longitude"));
                        checkBinaryField(builder, format1, format3);
                    } catch (IllegalArgumentException e) {
                        checkBinaryField(builder, format1, null);
                    }
                    builder.append("Course: ");
                    checkBinaryField(builder, telematicRecord.getGpsData().getCourse(), documents.get(i).get("Course"));
                    builder.append("Satellites: ");
                    checkBinaryField(builder, telematicRecord.getGpsData().getSatellites(), documents.get(i).get("Satellites"));
                    builder.append("Speed: ");
                    checkBinaryField(builder, telematicRecord.getGpsData().getSpeed(), documents.get(i).get("Speed"));
                    builder.append("AccelerationAvgX: ");
                    checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationAvgX(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationAvgX"));
                    builder.append("AccelerationAvgY: ");
                    checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationAvgY(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationAvgY"));
                    builder.append("AccelerationAvgZ: ");
                    checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationAvgZ(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationAvgZ"));
                    builder.append("AccelerationAvgX: ");
                    checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationMaxX(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationMaxX"));
                    builder.append("AccelerationAvgY: ");
                    checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationMaxY(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationMaxY"));
                    builder.append("AccelerationAvgZ: ");
                    checkBinaryField(builder, telematicRecord.getAcceleration().getAccelerationMaxZ(), documents.get(i).get("ExtendedData", Document.class).get("AccelerationMaxZ"));
                    builder.append("Duration: ");
                    checkBinaryField(builder, telematicRecord.getAcceleration().getDuration(), documents.get(i).get("ExtendedData", Document.class).get("Duration"));
                    showDocument(builder);
                }
            } else if (packages.execute().asJson().getStatus() == 400) {
                getResponseStringBody();
            }
        } catch (Exception e) {
            getResponseBody();
        }
    }

    @Attachment("В ответе записей нет.")
    private String getResponseBody() throws UnirestException {
        try {
            Assert.assertEquals(packages.execute().asJson().getBody().getArray().length(), 0);
            return String.valueOf(packages.execute().asJson().getBody());
        } catch (UnirestException | AssertionError err) {
            return packages.execute().asString().getBody();
        }
    }

    @Attachment("В ответе записей нет.")
    private String getResponseStringBody() throws UnirestException {
        try {
            Assert.assertEquals(packages.execute().asJson().getBody().getArray().length(), 1);
            return String.valueOf(packages.execute().asJson().getBody());
        } catch (UnirestException e) {
            return packages.execute().asString().getBody();
        }
    }

    @Attachment("Проверенные поля: ")
    private String showDocument(StringBuilder builder) {
        return builder.toString();
    }

    private void checkBinaryField(StringBuilder builder, Object response, Object mongo) {
        try {

            if (mongo != null) {
                Assert.assertEquals(response, mongo);
                builder.append("MongoDB: " + mongo + "\n");
                builder.append("Response: " + response + "\n");
            } else {
                builder.append("MongoDB: " + mongo + "\n");
                builder.append("Response: " + response + "\n");
            }
        } catch (ComparisonFailure failure) {
            builder.append("MongoDB: " + mongo + "\n");
            builder.append("Response: " + response + "\n");
        }
    }

    private ArrayList<Document> mongoCollectionByParams(String deviceCode) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'%20'HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parse = new Date();
        try {
            parse = simpleDateFormat.parse(packages.getFrom());
        } catch (ParseException e) {

        }
        Date parse1 = simpleDateFormat.parse(packages.getTo());
        DBObject dbObject = BasicDBObjectBuilder.start()
                .add("DeviceCode", deviceCode)
                .add("DeviceTime", new BasicDBObject("$gte", parse).append("$lte", parse1))
                .get();
        ArrayList<Document> list = new ArrayList<>();
        MongoClient client = new MongoDevApiConnection().getClient();
        try {
            FindIterable<Document> deviceFullRecords = client.getDatabase(AbstractClass.getProperty("mongoCollection"))
                    .getCollection("DeviceFullRecords")
                    .find((Bson) dbObject)
                    .limit(Integer.parseInt(packages.getLimit()))
                    .skip(Integer.parseInt(packages.getSkip()))
                    .sort(new BasicDBObject("DeviceTime", 1));
            deviceFullRecords.forEach((Consumer<? super Document>) list::add);
        } catch (NumberFormatException e) {
        } finally {
            client.close();
        }
        return list;
    }
}
