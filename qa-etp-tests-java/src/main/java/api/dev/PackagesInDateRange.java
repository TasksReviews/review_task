package api.dev;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.t1.core.AbstractClass;

/**
 * params from = "10/21/2017%2000:00:00" - String
 * params to = "10/21/2017%2000:00:00" - String
 * params skip = "0" - String
 * params limit = "100" - String
 */

public class PackagesInDateRange extends GetExecutor {
    private String packagesDateRange;

    public PackagesInDateRange() {
        packagesDateRange = AbstractClass.getProperty("packagesInDateRange");
    }

    @Override
    public GetRequest execute() {
        return Unirest.get(AbstractClass.getProperty("testapi") + packagesDateRange + "?from=" + from + "&to=" + to + "&limit=" + limit + "&skip=" + skip).header("Content-Type", "application/json")
                .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Mzg1NzcyMzQsImlzcyI6InNlcnZlciIsImF1ZCI6InNlcnZlciJ9.hw7coIifAD_q_VyRtsI4ib4_Tinvt2Ytx9kjuSLm1OE")
                .header("Cache-Control", "no-cache");
    }

    public GetRequest execute(String from, String to, String limit) {
        setFrom(from);
        setTo(to);
        setLimit(limit);
        return Unirest.get(AbstractClass.getProperty("testapi") + packagesDateRange + "?from=" + from + "&to=" + to + "&limit=" + limit).header("Content-Type", "application/json")
                .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Mzg1NzcyMzQsImlzcyI6InNlcnZlciIsImF1ZCI6InNlcnZlciJ9.hw7coIifAD_q_VyRtsI4ib4_Tinvt2Ytx9kjuSLm1OE")
                .header("Cache-Control", "no-cache");
    }
}