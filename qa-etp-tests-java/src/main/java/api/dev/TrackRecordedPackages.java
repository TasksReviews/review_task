package api.dev;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.t1.core.AbstractClass;

public class TrackRecordedPackages extends GetExecutor {

    @Override
    public GetRequest execute() {
        return Unirest.get(AbstractClass.getProperty("testapi") + "/api/deviceTelematicData/TrackRecordedPackages" + "?from=" + from + "&to=" + to + "&limit=" + limit + "&skip=" + skip)
                .header("Authorization", AbstractClass.getProperty("authorization"))
                .header("Content-Type", "application/json");
    }

    @Override
    public GetRequest execute(String from, String to, String limit) {
        setFrom(from);
        setTo(to);
        setLimit(limit);
        setSkip("0");
        return Unirest.get(AbstractClass.getProperty("testapi") + "/api/deviceTelematicData/TrackRecordedPackages" + "?from=" + getFrom() + "&to=" + getTo() + "&limit=" + getLimit())
                .header("Authorization", AbstractClass.getProperty("authorization"))
                .header("Content-Type", "application/json");
    }
}