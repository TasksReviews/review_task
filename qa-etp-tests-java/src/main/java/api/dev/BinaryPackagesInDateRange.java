package api.dev;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.t1.core.AbstractClass;

import java.io.IOException;

/**
 * params from = "10/21/2017%2000:00:00" - String
 * params to = "10/21/2017%2000:00:00" - String
 * params skip = "0" - String
 * params limit = "100" - String
 */

public class BinaryPackagesInDateRange extends GetExecutor {
    private String s = "?from=";
    private String authorization = "authorization";
    private String string = "&limit=";

    public BinaryPackagesInDateRange() {
        Unirest.setObjectMapper(new com.mashape.unirest.http.ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            @Override
            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public GetRequest execute() {
        String method = "/api/devicetelematicdata/BinaryPackagesInDateRange";
        string = "&limit=";
        return Unirest.get(AbstractClass.getProperty("testapi") + method + s + getFrom() + "&to=" + getTo() + string + getLimit() + "&skip=" + getSkip()).header(authorization, AbstractClass.getProperty(authorization));
    }

    @Override
    public GetRequest execute(String from, String to, String limit) {
        setFrom(from);
        setTo(to);
        setLimit(limit);
        String method = "/api/devicetelematicdata/BinaryPackagesInDateRange";
        return Unirest.get(AbstractClass.getProperty("testapi") + method + s + getFrom() + "&to=" + getTo() + string + getLimit()).header(authorization, AbstractClass.getProperty(authorization));
    }
}
