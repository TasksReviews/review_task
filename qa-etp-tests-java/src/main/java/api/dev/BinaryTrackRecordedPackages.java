package api.dev;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.t1.core.AbstractClass;

/**
 * params from example '12.12.2012'
 * params to example '12.12.2017'
 * params limit example '1000'
 * */
public class BinaryTrackRecordedPackages extends GetExecutor {

    @Override
    public GetRequest execute() {
        String method = "/api/deviceTelematicData/BinaryTrackRecordedPackages";
        return Unirest.get(AbstractClass.getProperty("testapi") + method + "?from=" + from + "&to=" + to + "&limit=" + limit)
                .header("authorization", AbstractClass.getProperty("authorization"))
                .header("Content-Type", "application/json");
    }

    @Override
    public GetRequest execute(String from, String to, String limit) {
        return null;
    }

}
