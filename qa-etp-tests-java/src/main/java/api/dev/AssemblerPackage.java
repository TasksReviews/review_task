package api.dev;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.RequestBodyEntity;
import com.t1.core.AbstractClass;
import ru.yandex.qatools.allure.annotations.Attachment;

public class AssemblerPackage extends PostExecutor {

    @Override
    public RequestBodyEntity execute() {
         return Unirest.post(AbstractClass.getProperty("testapi") + "/api/telematicparsingelement/Package")
                .header("Content-Type", "application/json")
                .header("Authorization", AbstractClass.getProperty("assemblerAuthrization")).body(getAssemblerBody());
    }
    public RequestBodyEntity executeWithoutToken() {
        return Unirest.post(AbstractClass.getProperty("testapi") + "/api/telematicparsingelement/Package")
                .header("Content-Type", "application/json").body(getAssemblerBody());
    }

    @Attachment("Token: ")
    public String getToken() {
        return AbstractClass.getProperty("assemblerAuthrization");
    }
}
