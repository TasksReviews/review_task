package api.dev;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.t1.core.AbstractClass;
import org.apache.log4j.Logger;
import org.bson.Document;

import java.io.Serializable;

public class MongoDevApiConnection implements Serializable {

    public MongoClient getClient() {
        MongoClientURI uri = new MongoClientURI(AbstractClass.getProperty("mongodb"));
        MongoClient mongoClient = new MongoClient(uri);
        return mongoClient;
    }

    private MongoDatabase getMongoDataBase() {
        return getClient().getDatabase(AbstractClass.getProperty("mongoCollection"));
    }


    public MongoCollection<Document> getCollection(String deviceFullRecords) {


        return getMongoDataBase().getCollection(AbstractClass.getProperty(deviceFullRecords));
    }
}
