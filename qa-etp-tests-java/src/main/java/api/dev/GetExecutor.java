package api.dev;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;

public abstract class GetExecutor {
    protected String from = "";
    protected String to = "";
    protected String limit = "";
    protected String skip = "0";

    /**
     * @param from = "12.12.2000"
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @param to = "12.12.2000"
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @param limit = "100"
     */
    public void setLimit(String limit) {
        this.limit = limit;
    }

    /**
     * @param skip = "0"
     */
    public void setSkip(String skip) {
        this.skip = skip;
    }

    public abstract GetRequest execute() throws UnirestException;

    public abstract GetRequest execute(String from, String to, String limit);

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getLimit() {
        return limit;
    }

    public String getSkip() {
        return skip;
    }

}
