package api.dev;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.RequestBodyEntity;
import com.t1.core.AbstractClass;
import ru.yandex.qatools.allure.annotations.Attachment;

public class AssemblerCountPackage extends PostExecutor {

    @Override
    public RequestBodyEntity execute() {
        return Unirest.post(AbstractClass.getProperty("testapi") + "/api/telematicparsingelement/CountPackage")
                .header("Content-Type", "application/json")
                .header("Authorization", AbstractClass.getProperty("assemblerAuthrization")).body(getAssemblerBody());
    }

    @Attachment("Token: ")
    public String getToken() {
        return AbstractClass.getProperty("assemblerAuthrization");
    }

    public RequestBodyEntity executeWithoutToken() {
        return Unirest.post(AbstractClass.getProperty("testapi") + "/api/telematicparsingelement/CountPackage")
                .header("Content-Type", "application/json")
                .body(getAssemblerBody());

    }
}
