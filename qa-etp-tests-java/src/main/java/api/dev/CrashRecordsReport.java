package api.dev;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import cucumber.api.java.ContinueNextStepsFor;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.hamcrest.core.Is;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Assert;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

public class CrashRecordsReport {
    private CrashRecords crashRecords = new CrashRecords();

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Переметры GET запроса: ")
    public void request(String arg0, String arg1) {
        crashRecords.setFrom(arg0);
        crashRecords.setTo(arg1);
        GetRequest execute = crashRecords.execute();
        getUrl(execute);
    }

    @Attachment("URL GET запроса: ")
    private String getUrl(GetRequest execute) {
        return execute.getUrl();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET ответа: ")
    public void response() {
        try {
            getStatusText();
            getBody();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step("Параметры GET ответа: ")
    public void negativeResponse() {
        try {
            getNegativeStatusText();
            getBody();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }

    @Attachment("Статус GET ответа")
    private String getNegativeStatusText() throws UnirestException {
        Assert.assertEquals(crashRecords.execute().asString().getStatus(), 400);
        return crashRecords.execute().asString().getStatus() + ", " + crashRecords.execute().asString().getStatusText();
    }

    @Attachment("Ответ GET запроса: ")
    private String getBody() throws UnirestException {
        return crashRecords.execute().asString().getBody();
    }

    @Attachment("Статус GET ответа")
    private String getStatusText() throws UnirestException {
        if (crashRecords.execute().asString().getStatus() == 400) {
            return crashRecords.execute().asString().getStatus() + ", " + crashRecords.execute().asString().getStatusText();
        }
        return crashRecords.execute().asString().getStatusText();
    }

    @Step
    @ContinueNextStepsFor(AssertionError.class)
    public void getDataFromMongo() throws UnirestException, ParseException {
        try {
            JSONArray crashes = crashRecords.execute().asJson().getBody().getArray().getJSONObject(0).getJSONArray("crashes");
            Set<String> stack = new HashSet();
            for (int i = 0; i < crashes.length(); i++) {
                String imei = crashes.getJSONObject(i).getString("imei");
                stack.add(String.valueOf(imei));
                DBObject dbObject = BasicDBObjectBuilder.start()
                        .add("DeviceCode", new BasicDBObject("$in", stack))
                        .get();
                FindIterable<Document> limit = new MongoDevApiConnection().getCollection("RoadAccidents").find((Bson) dbObject).sort(new BasicDBObject("CrashTime", 1));
                ArrayList<Document> list = new ArrayList<>();
                limit.forEach((Consumer<? super Document>) list::add);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
                Date deviceTime = simpleDateFormat.parse((String) crashes.getJSONObject(i).get("deviceTime"));
                Date receivedTime = simpleDateFormat.parse((String) crashes.getJSONObject(i).get("receivedTime"));
                Date crashTime = simpleDateFormat1.parse(String.valueOf(list.get(i).get("CrashTime")));
                Date receivedTime1 = simpleDateFormat1.parse(String.valueOf(list.get(i).get("ReceivedTime")));
                DecimalFormat decimalFormat = new DecimalFormat("##.00");
                checkCrashTime(deviceTime, crashTime);
                checkReceivedTime(receivedTime, receivedTime1);
                checkLongitude(decimalFormat, crashes, list);
                checkLatitude(decimalFormat, crashes, list);
                checkAltitude(decimalFormat, crashes, list);
                checkCourse(crashes, list);
                checkSpeed(crashes, list);
                checkSatellites(crashes, list);
            }
        } catch (JSONException e) {
            getBody();
        }
    }

    @Step
    @ContinueNextStepsFor(AssertionError.class)
    private void checkSatellites(JSONArray crashes, ArrayList<Document> list) {
        for (int i = 0; i < crashes.length(); i++) {
            checkField("Satellites", list.get(i).getDouble("Satellites"), crashes.getJSONObject(i).getDouble("satellites"));
        }
    }

    @Step
    @ContinueNextStepsFor(AssertionError.class)
    private void checkSpeed(JSONArray crashes, ArrayList<Document> list) {
        for (int i = 0; i < crashes.length(); i++) {
            checkField("Speed", list.get(i).getDouble("Speed"), crashes.getJSONObject(i).getDouble("speed"));
        }
    }

    @Step
    @ContinueNextStepsFor(AssertionError.class)
    private void checkCourse(JSONArray crashes, ArrayList<Document> list) {
        for (int i = 0; i < crashes.length(); i++) {
            checkField("Course", list.get(i).getInteger("Course"), crashes.getJSONObject(i).getInt("course"));
        }
    }

    @Step
    @ContinueNextStepsFor(AssertionError.class)
    private void checkAltitude(DecimalFormat decimalFormat, JSONArray crashes, ArrayList<Document> list) {
        for (int i = 0; i < crashes.length(); i++) {
            checkField("Altitude", decimalFormat.format(list.get(i).get("Altitude")), decimalFormat.format(crashes.getJSONObject(i).getDouble("altitude") / 1000000));
        }
    }

    @Step
    @ContinueNextStepsFor(AssertionError.class)
    private void checkLatitude(DecimalFormat decimalFormat, JSONArray crashes, ArrayList<Document> list) {
        for (int i = 0; i < crashes.length(); i++) {
            checkField("Latitude", decimalFormat.format(list.get(i).get("Latitude")), decimalFormat.format(crashes.getJSONObject(i).getDouble("latitude") / 1000000));
        }
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step
    private void checkLongitude(DecimalFormat decimalFormat, JSONArray crashes, ArrayList<Document> list) {
        for (int i = 0; i < crashes.length(); i++) {
            checkField("Longitude", decimalFormat.format(list.get(i).get("Longitude")), decimalFormat.format(crashes.getJSONObject(i).getDouble("longitude") / 1000000));
        }
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step
    private void checkReceivedTime(Date receivedTime, Date receivedTime1) {
        checkField("ReceivedTime", receivedTime1.getTime() - 10800000, receivedTime.getTime());
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Step
    private void checkCrashTime(Date deviceTime, Date crashTime) {
        checkField("CrashTime", crashTime.getTime() - 10800000, deviceTime.getTime());
    }

    @Attachment
    private StringBuilder checkField(String fieldName, Object o, Object o1) {
        StringBuilder builder = new StringBuilder();
        if (o != null) {
            Assert.assertThat(o, Is.is(o1));
        } else {
            Assert.assertThat(o1, Is.is(0));
        }
        builder.append(fieldName + "\n");
        builder.append("Mongo: " + o + "\n");
        builder.append("Response: " + o1 + "\n");
        return builder;
    }
}
