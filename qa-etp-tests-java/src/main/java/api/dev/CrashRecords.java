package api.dev;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.t1.core.AbstractClass;

/**
 * params from - example '12.12.2012'
 * params to example '12.12.2017'
 */
public class CrashRecords extends GetExecutor {

    @Override
    public GetRequest execute() {

        String method = "/api/deviceTelematicData/crashRecords";
        return Unirest.get(AbstractClass.getProperty("testapi") + method + "?from=" + from + "&to=" + to).header("authorization", AbstractClass.getProperty("authorization"));
    }

    @Override
    public GetRequest execute(String from, String to, String limit) {
        return null;
    }

}
