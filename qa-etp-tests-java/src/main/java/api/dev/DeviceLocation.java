package api.dev;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.RequestBodyEntity;
import com.t1.core.AbstractClass;

/**
 * api, method,authorization - заданы в методе execute()
 * <p>
 * example {"ImeiList":"<imei>"}
 */
public class DeviceLocation extends PostExecutor {

	@Override
    public RequestBodyEntity execute() {
        return Unirest.post(AbstractClass.getProperty("testapi") + AbstractClass.getProperty("deviceLocation")).header("authorization", AbstractClass.getProperty("authorization"))
                .header("Content-Type", "application/json")
                .body("{\"ImeiList\":" + getImeiList() + "}");
    }
}
