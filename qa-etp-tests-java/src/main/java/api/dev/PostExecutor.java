package api.dev;

import com.mashape.unirest.request.body.RequestBodyEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public abstract class PostExecutor {
    private JSONArray imeiList = new JSONArray();
    private String monitoringState = "Stop";
    private JSONObject assemblerBody = new JSONObject();
    private String start = "start";
    private String deviceCodes = "deviceCodes";
    private String limit = "limit";

    public JSONObject getAssemblerBody() {
        return assemblerBody;
    }

    public void createAssemblerBody(String start, String end, List<String> deviceCodes) {
        assemblerBody.put(this.start, start);
        assemblerBody.put("end", end);
        assemblerBody.put(this.deviceCodes, deviceCodes);
    }

    public void createAssemblerBody(String start, String end, List<String> deviceCodes, String limit) {
        assemblerBody.put("start", start);
        assemblerBody.put("end", end);
        assemblerBody.put("deviceCodes", deviceCodes);
        assemblerBody.put(this.limit, limit);
    }

    public void createAssemblerBody(List<String> deviceCodes) {
        assemblerBody.put("deviceCodes", deviceCodes);
    }

    public void setMonitoringState(String monitoringState) {
        this.monitoringState = monitoringState;
    }

    public String getMonitoringState() {
        return monitoringState;
    }

    public JSONArray add(String... strings) {
        for (String imei : strings
                ) {
            imeiList.put(imei);
        }
        return imeiList;
    }

    public JSONArray clearList() {
        for (int i = imeiList.length() - 1; i >= 0; i--) {
            imeiList.remove(i);
        }
        return imeiList;
    }

    public org.json.JSONArray getImeiList() {
        return imeiList;
    }

    public abstract RequestBodyEntity execute();

    public void createAssemblerBodyWithoutEnd(String s) {
        assemblerBody.remove("end");
        assemblerBody.put(this.start, s);
        assemblerBody.remove(this.deviceCodes);
        assemblerBody.remove(this.limit);
    }

    public void createAssemblerBodyWithoutStart(String s) {
        assemblerBody.put("end", s);
        assemblerBody.remove(this.start);
        assemblerBody.remove(this.deviceCodes);
        assemblerBody.remove(this.limit);
    }
}

