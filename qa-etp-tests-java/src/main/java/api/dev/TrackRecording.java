package api.dev;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.RequestBodyEntity;
import com.t1.core.AbstractClass;

/**
 * api, method,authorization - заданы в методе execute()
 * установить StartMonitoringState нужно с помощью метода setStartMonitoringState()
 * <p>
 * example {"ImeiList":"<imei>"}
 */
public class TrackRecording extends PostExecutor {

    @Override
    public RequestBodyEntity execute() {
        return Unirest.post(AbstractClass.getProperty("testapi") + AbstractClass.getProperty("trackRecording") + "?MonitoringState=" + getMonitoringState())
                .header("Authorization", AbstractClass.getProperty("authorization"))
                .header("Content-Type", "application/json")
                .body("{\"ImeiList\":" + getImeiList() + "}");
    }
}