package api.utils;


import api.dev.MongoDevApiConnection;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.t1.core.AbstractClass;
import org.bson.conversions.Bson;

public interface Prop {
    MongoClient client = new MongoDevApiConnection().getClient();

    static String getDeviceCodePIDR(String imei) {
        DBObject dbObject = BasicDBObjectBuilder.start().add("Imei", imei).add("Model", "FMT100").get();
        return String.valueOf(client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("Devices").find((Bson) dbObject).first().get("Code"));
    }

    static String getDeviceCodePIDRTest(String imei) {
        DBObject dbObject = BasicDBObjectBuilder.start().add("Code", imei).add("Model", "FMT100").get();
        return String.valueOf(client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("Devices").find((Bson) dbObject).first().get("Code"));
    }

    static String getDeviceImeiPIDR(String code) {
        DBObject dbObject = BasicDBObjectBuilder.start().add("Code", code).add("Model", "FMT100").get();
        return String.valueOf(client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("Devices").find((Bson) dbObject).first().get("Imei"));
    }

    static String getDeviceCode(String imei) {
        return String.valueOf(client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("Devices").find(new BasicDBObject("Imei", imei)).first().get("Code"));
    }

    static String getDeviceImei(String code) {
        return String.valueOf(client.getDatabase(AbstractClass.getProperty("mongoCollection")).getCollection("Devices").find(new BasicDBObject("Code", code)).first().get("Imei"));
    }
    static void closeConnection(){
        client.close();
    }
}
