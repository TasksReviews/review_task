package com.t1.core;

import org.apache.log4j.LogManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class AbstractClass {

    protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("Test");
    private static final String ERROR = "Error";

    protected AbstractClass() {
        LogManager.getLogger("com.jayway.jsonpath.internal.path").setLevel(org.apache.log4j.Level.ERROR);  //otherwise will get "Evaluating path: {}"
        LogManager.getLogger("org.mongodb.driver").setLevel(org.apache.log4j.Level.ERROR); //otherwise will get connection state,sended command etc.
    }

    public static String getProperty(String property) {
        Properties prop = new Properties();
        InputStream input = null;
        String result = null;
        try {

            input = AbstractClass.class.getClassLoader().getResourceAsStream("config.properties");
            prop.load(input);
            result = prop.getProperty(property);
            if (result == null || result.equals("")) {
                result = property;
            }
        } catch (IOException ex) {
            log.error(ERROR, ex);
        } finally {

            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    log.error(ERROR, e);
                }
            }
        }
        return result;
    }
}