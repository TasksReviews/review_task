#Created by Romanova Daria 08.07.2018
Feature: GET_ApiDeviceTelematicDataTrackRecordedPackages [Limit_Negative]

  @QA-3090
  Scenario Outline: GET_ApiDeviceTelematicDataTrackRecordedPackages [Limit_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить команду о выключении записи трэка для ТМУ с кодами
      | 352093084476158 |
      | 352093084546448 |
      | 352093084549137 |
      | 352093083720895 |
    And Отправить команду о включении записи трэка для ТМУ с кодами
      | 352093084476158 |
      | 352093084546448 |
      | 352093084549137 |
      | 352093083720895 |
    # Запрос отправлять сразу после отправки команды о включении записи трэка
    # Параметры from и to : дата - сегодня, время - несколько минут после отправки команды о включении записи трэка
    And Отправить запрос для получения телематических записей трека с некорректным Limit = "<Limit>"
    # Проверить, что для всех значений "<Limit>" код состояния ответа - 400 Bad Request
    # Проверить, что для пустого параметра limit тело ответа равно "The value '' is invalid."
    # Проверить, что для limit =< 0 тело ответа равно "Value for Limit must be between 1 and 2147483647."
    # Проверить, что для limit = NULL, limit = 2018/05/18%2020:00:00,
    # limit = asdf, limit = 20.53, limit = true
    # тело ответа равно "The value '<Limit>' is not valid for Limit."
    Examples:
      | Limit                 |
      |                       |
      | 0                     |
      | -10                   |
      | NULL                  |
      | true                  |
      | asdf                  |
      | 2018/05/18%2020:00:00 |
