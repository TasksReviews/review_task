Feature: GET_ApiDeviceTelematicDataBinaryTrackRecordedPackages [динамические From и To]

  @QA-3273
  Scenario: GET_ApiDeviceTelematicDataBinaryTrackRecordedPackages [динамические From и To]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить команду о выключении записи трэка для ТМУ с кодами
      | 352093084476158 |
      | 352093084546448 |
      | 352093084549137 |
      | 352093083720895 |
    And Отправить команду о включении записи трэка для ТМУ с кодами
      | 352093084476158 |
      | 352093084546448 |
      | 352093084549137 |
      | 352093083720895 |
# limit = 2000
# Запрос отправлять через сутки после отправки команды о включении записи трэка
    And Отправить запрос для получения телематических записей трека в бинарном виде (дата From - в прошлом, дата To - в будущем)
# Cравнить ВСЕ полученные данные с DeviceFullRecords
# Нужно взять imei из ответа DevAPI, обратиться с ним в коллекцию Devices и получить deviceCode.
# Затем использовать этот deviceCode для запроса в DeviceFullRecords.
# Запрос отправлять через сутки после отправки команды о включении записи трэка
    And Отправить запрос для получения телематических записей трека в бинарном виде (дата From и дата To - в будущем)
# Cравнить ВСЕ полученные данные с DeviceFullRecords
# Нужно взять imei из ответа DevAPI, обратиться с ним в коллекцию Devices и получить deviceCode.
# Затем использовать этот deviceCode для запроса в DeviceFullRecords.
