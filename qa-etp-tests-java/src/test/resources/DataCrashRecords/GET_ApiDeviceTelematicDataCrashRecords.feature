Feature: GET_ApiDeviceTelematicDataCrashRecords

  @QA-3015
  Scenario Outline: GET_ApiDeviceTelematicDataCrashRecords
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить запрос для получения списка ДТП за период c From = "<From>", To = "<To>"
# Сравнить ВСЕ полученные данные DevApi с данными из коллекции RoadAccidents
# Проверить, что кол-во элементов в массиве "crash" равно кол-ву записей в коллекции DeviceFullRecords
# Проверить, что все полученные события принадлежат указанному периоду
    Then Сравнить полученные данные с RoadAccidents

    Examples:
      | From                | To                  |
      | 2018-01-02T10:00:00 | 2018-08-23T12:00:00 |
      | 2018-01-02T10:00:00 | 2018-08-23T12:00:00 |
      | 2018-01-02T10:00:00 | 2018-08-23T12:00:00 |
