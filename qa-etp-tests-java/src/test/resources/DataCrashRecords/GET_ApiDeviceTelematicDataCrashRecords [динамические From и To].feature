Feature: GET_ApiDeviceTelematicDataCrashRecords [динамические From и To]

  @QA-3254
  Scenario: GET_ApiDeviceTelematicDataCrashRecords [динамические From и To]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить запрос для получения телематических данных (дата From - в прошлом, дата To - в будущем)
# Сравнить ВСЕ полученные данные DevApi с данными из коллекции RoadAccidents
# Проверить, что кол-во элементов в ответе DevApi равно кол-ву записей в коллекции DeviceFullRecords
# Проверить, что все полученные события принадлежат указанному периоду
    Then Сравнить полученные данные с RoadAccidents
    And Отправить запрос для получения телематических данных (когда дата From и дата To - в будущем)
# проверить, что приходит пустой массив "crash"
    Then Сравнить полученные данные с RoadAccidents