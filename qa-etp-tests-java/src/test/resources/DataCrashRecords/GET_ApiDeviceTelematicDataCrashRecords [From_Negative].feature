Feature: GET_ApiDeviceTelematicDataCrashRecords [From_Negative]

  @QA-3081
  Scenario Outline: GET_ApiDeviceTelematicDataCrashRecords [From_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить некорректный запрос для получения списка ДТП за период c From = "<From>", To = "<To>"
# Проверить, что для всех значений "<From>" код состояния ответа - 400 Bad Request
# Проверить, что для пустого параметра From тело ответа равно "The value '' is invalid."
# Проверить, что для всех остальных значений From тело ответа равно "The value '<From>' is not valid for From."
# Проверить, что код состояния ответа - 400 Bad Request

    Examples:
      | From                  | To                    |
      | 2017/22/44%2010:00:00 | 2017/12/11%2011:00:00 |
      | 2017-12-41%2010:00:00 | 2017-12-11%2011:00:00 |
      | 2017/12/11%2050:00:00 | 2017/12/12%2000:00:00 |
      | 2017-12-11%2000:60:00 | 2017-12-12%2000:00:00 |
      | 2017/12/11%2000:00:61 | 2017/12/12%2000:00:00 |
      | NULL                  | 2017/12/12%2000:00:00 |
      | true                  | 2017/12/12%2000:00:00 |
      | asdfgh                | 2017/12/12%2000:00:00 |
      |                       | 2017-12-12%2000:00:00 |