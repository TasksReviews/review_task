Feature: GET_ApiDeviceTelematicDataCrashRecords [To_Negative]

  @QA-3080
  Scenario Outline: GET_ApiDeviceTelematicDataCrashRecords [To_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить некорректный запрос для получения списка ДТП за период c From = "<From>", To = "<To>"
# Проверить, что для всех значений "<To>" код состояния ответа - 400 Bad Request
# Проверить, что для пустого параметра To тело ответа равно "The value '' is invalid."
# Проверить, что для всех остальных значений To тело ответа равно "The value '<To>' is not valid for To."
    Then Проверить, что код состояния ответа - 400 Bad Request
    Examples:
      | From                  | To                    |
      | 2017/12/11%2010:00:00 | 2017/13/11%2011:00:00 |
      | 2017-12-11%2010:00:00 | 2017-12-41%2011:00:00 |
      | 2017/12/11%2000:00:00 | 2017/12/12%2025:00:00 |
      | 2017-12-11%2000:00:00 | 2017-12-12%2000:61:00 |
      | 2017/12/11%2010:00:00 | 2017/12/11%2012:00:60 |
      | 2017/12/11%2010:00:00 | NULL                  |
      | 2017/12/11%2010:00:00 | 5.234                 |
      | 2017/12/11%2010:00:00 | true                  |
      | 2017/12/11%2010:00:00 | asdfgh                |
      | 2017/12/11%2010:00:00 |                       |