Feature: GET_ApiDeviceTelematicDataCrashRecords [From больше To]

  @QA-3079
  Scenario Outline: GET_ApiDeviceTelematicDataCrashRecords [From больше To]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# проверить, что код состояния ответа "400 Bad Request"
# проверить, что тело ответа "Start date can not be greater end date"
    Then Отправить запрос для получения списка ДТП за период c From = "<From>", To = "<To>"

    Examples:
      | From                  | To                    |
      | 2017/12/12%2010:00:00 | 2017/12/11%2011:00:00 |
      | 2017/12/12%2012:00:00 | 2017/12/12%2011:00:00 |