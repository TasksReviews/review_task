Feature: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [дата - сегодня]

    @QA-3228
    Scenario: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [дата - сегодня]
        Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# Отправить GET запрос /api/deviceTelematicData/BinaryPackagesInDateRange
# Необходимо учесть, что время, передаваемое в параметрах from и to переводится в UTC, т.е. будет меньше на 3 часа
        # Задать параметр limit = 10
        When Отправить запрос для получения телематических данных в бинарном виде за сегодня (дата - сегодня, время - наступило)
# Проверить, что кол-во элементов в массиве telematicRecords равно кол-ву записей в коллекции DeviceFullRecords
        # Сравнить все поля WebApi с MongoDb
        Then Сравнить бинарные данные с DeviceFullRecords
        When Отправить запрос для получения телематических данных в бинарном виде за сегодня (дата - сегодня, время - в будущем)
        #Проверить, что возвращается "telematicRecords": []
        Then Сравнить бинарные данные с DeviceFullRecords