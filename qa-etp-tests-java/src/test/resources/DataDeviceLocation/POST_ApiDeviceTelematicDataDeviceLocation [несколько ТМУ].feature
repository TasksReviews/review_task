Feature:  POST_ApiDeviceTelematicDataDeviceLocation [несколько ТМУ]

  @QA-2982
  Scenario:  POST_ApiDeviceTelematicDataDeviceLocation [несколько ТМУ]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Запросить данные о последней позиции всех заданных ТМУ
      | 352093084476158 |
      | 352093084546448 |
      | 352093084549137 |
      | 352093083720895 |
  #Запись должна быть последней в БД
    Then Сравнить полученные записи с DeviceFullRecords