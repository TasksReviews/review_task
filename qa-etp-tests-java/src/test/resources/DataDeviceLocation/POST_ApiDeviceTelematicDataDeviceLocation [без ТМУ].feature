Feature: POST_ApiDeviceTelematicDataDeviceLocation [без ТМУ]

  @QA-3469
  Scenario: POST_ApiDeviceTelematicDataDeviceLocation [без ТМУ]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# error "400 BadRequest" message "Devices codes are empty"
    Then Запросить данные о последней позиции ТМУ без указания номера ТМУ
