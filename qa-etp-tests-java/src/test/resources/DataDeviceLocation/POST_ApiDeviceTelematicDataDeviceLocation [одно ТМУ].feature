Feature: POST_ApiDeviceTelematicDataDeviceLocation [одно ТМУ]

  @QA-2980
  Scenario Outline: POST_ApiDeviceTelematicDataDeviceLocation [одно ТМУ]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Запросить данные о последней позиции всех заданных ТМУ с кодами "<ImeiList>"
  #Запись должна быть последней в БД
    Then Сравнить полученные записи с DeviceFullRecords
    Examples:
      | ImeiList        |
      | 352093084476158 |
      | 352093084546448 |
      | 352093084549137 |
      | 352093083720895 |