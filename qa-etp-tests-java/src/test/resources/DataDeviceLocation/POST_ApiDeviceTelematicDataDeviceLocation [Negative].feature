Feature: POST_ApiDeviceTelematicDataDeviceLocation [Negative]

  @QA-3470
  Scenario Outline: POST_ApiDeviceTelematicDataDeviceLocation [Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    Then Запросить данные о последней позиции всех заданных ТМУ с кодами "<ImeiList>"

    Examples:
      | ImeiList         |
      | NULL             |
      | 4                |
      | 3.58281005999111 |
      | true             |
      | asdfghh          |