Feature: POST_ApiTelematicParsingElementCountpackage [без end]

  @QA-3761
  Scenario: POST_ApiTelematicParsingElementCountpackage [без end]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить POST запрос /api/telematicParsingElement/CountPackage без параметра end
#Значения параметров deviceCodes и start - любые валидные
    And Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements без параметра end
    Then Проверить, что код состояния ответа - 500 Internal Server Error