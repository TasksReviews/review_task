Feature: POST_ApiTelematicParsingElementCountpackage [дата - сегодня]

  @QA-3546
  Scenario: POST_ApiTelematicParsingElementCountpackage [дата - сегодня]
    Given Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements за сегодня (дата start и end - сегодня), время start и end наступило
    And Сравнить с числом записей в коллекции TelematicParsingElements
    And Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements за сегодня (дата start и end - сегодня), время start наступило, время end в будущем
    And Сравнить с числом записей в коллекции TelematicParsingElements
    And Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements за сегодня (дата start и end - сегодня), время start и end в будущем
# возращается число 0
    Then Сравнить с числом записей в коллекции TelematicParsingElements
