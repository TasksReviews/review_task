Feature: POST_ApiTelematicParsingElementCountpackage [несколько ТМУ]

  @QA-3543
  Scenario: POST_ApiTelematicParsingElementCountpackage [несколько ТМУ]
    Given  Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements c Start = "2018-04-02 06:00:00", End = "2018-04-02 07:00:00" для deviceCodes
      | 510191 |
      | 510190 |
    Then Сравнить с числом записей в коллекции TelematicParsingElements
