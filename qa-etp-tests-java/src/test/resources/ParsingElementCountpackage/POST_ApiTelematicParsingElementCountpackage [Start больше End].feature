Feature: POST_ApiTelematicParsingElementCountpackage [Start больше End]

  @QA-3539
  Scenario Outline: POST_ApiTelematicParsingElementCountpackage [Start больше End]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    #Отправить POST запрос /api/telematicParsingElement/CountPackage
    And Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements c Start = "<Start>", End = "<End>", deviceCodes = "<deviceCodes>"
    #DeviceCodes - массив типа String
    #Все значения "<deviceCodes>" необходимо преобразовывать в массив
    #Проверить, что тело ответа равно "Start date can not be greater end date"

    Examples:
      | Start               | End                 | deviceCodes |
      | 2018-04-02 06:00:00 | 2018-04-02 05:00:00 | 510191      |
      | 2018-04-03 06:00:00 | 2018-04-02 07:00:00 | 510191      |
