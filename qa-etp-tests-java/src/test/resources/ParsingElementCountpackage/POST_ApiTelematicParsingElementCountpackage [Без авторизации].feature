Feature: POST_ApiTelematicParsingElementCountpackage [Без авторизации]

  @QA-3759
  Scenario: POST_ApiTelematicParsingElementCountpackage [Без авторизации]
    #Отправить POST запрос /api/telematicParsingElement/CountPackage
#без jwt-token с параметрами из Data table
    Given Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements c параметрами
      | Start               | End                 | deviceCodes |
      | 2018-04-02 06:00:00 | 2018-04-02 07:00:00 | 510191      |
#Проверить, что ответ от сервера 401 Unauthorized
    Then Выполнение запроса недоступно
