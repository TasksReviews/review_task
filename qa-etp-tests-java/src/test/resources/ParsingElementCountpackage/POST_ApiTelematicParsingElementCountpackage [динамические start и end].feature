Feature: POST_ApiTelematicParsingElementCountpackage [динамические start и end]

  @QA-3547
  Scenario: POST_ApiTelematicParsingElementCountpackage [динамические start и end]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить POST запрос /api/telematicParsingElement/CountPackage
#c параметром "deviceCodes": ["510191"]
    When Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements (дата start наступила, дата end - в будущем)
    Then Сравнить с числом записей в коллекции TelematicParsingElements
    When Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements (дата start и дата end - в будущем)
#Проверить, что возвращается 0 записей
    Then Сравнить с числом записей в коллекции TelematicParsingElements
#Отправить POST запрос /api/telematicParsingElement/CountPackage
#c параметром "deviceCodes": ["510191"]