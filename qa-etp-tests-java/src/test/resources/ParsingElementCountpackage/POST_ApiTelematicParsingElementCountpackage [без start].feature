Feature: POST_ApiTelematicParsingElementCountpackage [без start]

  @QA-3760
  Scenario: POST_ApiTelematicParsingElementCountpackage [без start]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить POST запрос /api/telematicParsingElement/CountPackage без параметра start
#Значения параметров deviceCodes и end - любые валидные
    And Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements без параметра start
    Then Проверить, что код состояния ответа - 500 Internal Server Error