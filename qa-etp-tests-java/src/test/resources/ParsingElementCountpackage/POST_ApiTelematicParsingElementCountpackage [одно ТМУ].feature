Feature: POST_ApiTelematicParsingElementCountpackage [одно ТМУ]

  @QA-3542
  Scenario Outline: POST_ApiTelematicParsingElementCountpackage [одно ТМУ]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить POST запрос /api/telematicParsingElement/CountPackage
#DeviceCode = 510192 отсутствует в базе, проверить, что метод возвращает 0
    And Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements c Start = "<Start>", End = "<End>", deviceCodes = "<deviceCodes>"
    Then Сравнить с числом записей в коллекции TelematicParsingElements
    Examples:
      | Start               | End                 | deviceCodes |
      | 2018-04-02 06:00:00 | 2018-04-02 07:00:00 | 510191      |
      | 2018-04-02 06:00:00 | 2018-04-03 07:00:00 | 510190      |
      | 2018-04-02 06:00:00 | 2018-04-02 06:00:00 | 510190      |
      | 2018-04-02 06:00:00 | 2018-04-02 07:00:00 | 510192      |