Feature: POST_ApiDeviceTelematicDataTrackRecording [ImeiList_Negative]

  @QA-3468
  Scenario Outline: POST_ApiDeviceTelematicDataTrackRecording [ImeiList_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# команда  MonitoringState=Stop передается в строке запроса
    And Отправить команду о выключении записи трэка для ТМУ с кодами "<ImeiList>"
# команда  MonitoringState=Start передается в строке запроса
    Then Отправить команду о включении записи трэка для ТМУ с кодами "<ImeiList>"

    Examples:
      | ImeiList         |
      | NULL             |
      | 4                |
      | 3.58281005999111 |
      | true             |
      | asdfgh           |