Feature: POST_ApiDeviceTelematicDataTrackRecording [одно ТМУ+выключение записи трэка]

  @QA-2985
  Scenario Outline: POST_ApiDeviceTelematicDataTrackRecording [одно ТМУ+выключение записи трэка]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
      # команда  MonitoringState=Start передается в строке запроса
    And Отправить команду о включении записи трэка для ТМУ с кодами "<ImeiList>"
      # команда  MonitoringState=Stop передается в строке запроса
    When Отправить команду о выключении записи трэка для ТМУ с кодами "<ImeiList>"
    Then Проверить отсутствие записей в коллекции TrackRecordingElements для ТМУ с кодами

    Examples:
      | ImeiList        |
      | 352093084476158 |
      | 352093084546448 |
      | 352093084549137 |
      | 352093083720895 |