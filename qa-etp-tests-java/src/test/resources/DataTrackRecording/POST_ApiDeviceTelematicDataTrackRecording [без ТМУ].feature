Feature: POST_ApiDeviceTelematicDataTrackRecording [без ТМУ]

  @QA-3087
  Scenario: POST_ApiDeviceTelematicDataTrackRecording [без ТМУ]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# команда  MonitoringState=Stop передается в строке запроса
# error "400 BadRequest" message "Devices codes are empty"
    And Отправить команду о выключении записи трэка для ТМУ без указания номера ТМУ
# команда  MonitoringState=Start передается в строке запроса
# error "400 BadRequest" message "Devices codes are empty"
    Then Отправить команду о включении записи трэка для ТМУ без указания номера ТМУ