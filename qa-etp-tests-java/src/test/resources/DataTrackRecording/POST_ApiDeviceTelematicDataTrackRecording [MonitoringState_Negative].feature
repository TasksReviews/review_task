Feature: POST_ApiDeviceTelematicDataTrackRecording [MonitoringState_Negative]

  @QA-3086
  Scenario Outline: POST_ApiDeviceTelematicDataTrackRecording [MonitoringState_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# команда  MonitoringState=Stop передается в строке запроса
    And Отправить команду о выключении записи трэка для ТМУ с кодами "<ImeiList>"

    And Отправить команду о включении записи трэка для ТМУ с кодами "<ImeiList>" и командами "<MonitoringState>"
    Then Проверить отсутствие записей в коллекции TrackRecordingElements для ТМУ с кодами "<ImeiList>"

    Examples:
      | ImeiList        | MonitoringState |
      | 352093084476158 |                 |
      | 352093084546448 | stopppppp       |
      | 352093084549137 | NULL            |
      | 352093083720895 | 5               |
      | 352093084546448 | 5.12345         |
      | 352093083720895 | true            |

