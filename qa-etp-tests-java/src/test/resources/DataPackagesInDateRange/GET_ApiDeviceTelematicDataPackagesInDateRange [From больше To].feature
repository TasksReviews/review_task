Feature: GET_ApiDeviceTelematicDataPackagesInDateRange [From больше To]

  @QA-3072
  Scenario Outline: GET_ApiDeviceTelematicDataPackagesInDateRange [From больше To]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# будет ошибка 400 Bad Request и сообщение "Start date can not be greater end date"
    Then Отправить запрос для получения телематических данных c некорректными From = "<From>", To = "<To>", Limit = "<Limit>"

    Examples:
      | From                    | To                      | Limit |
      | 2018-08-06T11%3A04%3A28 | 2018-07-09T18%3A19%3A28 | 10    |
      | 2018-08-06T11%3A04%3A28 | 2018-07-09T18%3A19%3A28 | 10    |