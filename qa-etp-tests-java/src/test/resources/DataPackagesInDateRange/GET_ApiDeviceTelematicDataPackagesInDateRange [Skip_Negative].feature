Feature: GET_ApiDeviceTelematicDataPackagesInDateRange [Skip_Negative]

  @QA-3069
  Scenario Outline: GET_ApiDeviceTelematicDataPackagesInDateRange [Skip_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить запрос для получения телематических данных c некорректными From = "<From>", To = "<To>", Limit = "<Limit>", Skip = "<Skip>"
# Проверить, что запрос возвращает ошибку 400 BadRequest
# Проверить сообщение об ошибке в теле ответа
# "The value "<Skip>" is not valid for Skip." - для непустых значений Skip
# "The value '' is invalid." - для пустого Skip
    Examples:
      | From                    | To                      | Limit | Skip  |
      | 2018/03/11T00%3A00%3A00 | 2018/03/12T00%3A00%3A00 | 50   |       |
      | 2018/03/11T10%3A00%3A00 | 2018/03/11T11%3A00%3A00 | 50   | -10   |
      | 2018/03/11T00%3A00%3A00 | 2018/03/12T00%3A00%3A00 | 50   | NULL  |
      | 2018-03-11T00%3A00%3A00 | 2018-03-12T00%3A00%3A00 | 50   | 5.23  |
      | 2018/03/11T10%3A00%3A00 | 2018/03/11T11%3A00%3A00 | 50   | false |
      | 2018/03/11T00%3A00%3A00 | 2018/03/12T00%3A00%3A00 | 50   | asdfg |