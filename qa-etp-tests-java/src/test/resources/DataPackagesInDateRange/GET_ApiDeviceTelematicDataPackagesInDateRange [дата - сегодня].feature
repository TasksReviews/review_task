Feature: GET_ApiDeviceTelematicDataPackagesInDateRange [дата - сегодня]

  @QA-3169
  Scenario: GET_ApiDeviceTelematicDataPackagesInDateRange [дата - сегодня]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить GET запрос /api/deviceTelematicData/packagesInDateRange
#Необходимо учесть, что время, передаваемое в параметрах from и to
#переводится в UTC, т.е. будет меньше на 3 часа
#Задать параметр limit = 10
    When Отправить запрос для получения телематических данных за сегодня (дата - сегодня, время - наступило)
#Проверить, что кол-во элементов в массиве telematicRecords равно
#кол-ву записей в коллекции DeviceFullRecords
#Сравнить все поля WebApi с MongoDb
    Then Сравнить полученные данные с DeviceFullRecords
    When Отправить запрос для получения телематических данных за сегодня (дата - сегодня, время - в будущем)
#Проверить, что возвращается "telematicRecords": []
    Then Сравнить полученные данные с DeviceFullRecords