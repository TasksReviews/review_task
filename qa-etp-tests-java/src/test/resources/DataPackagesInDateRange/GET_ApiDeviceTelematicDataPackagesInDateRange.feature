Feature: GET_ApiDeviceTelematicDataPackagesInDateRange

  @QA-2994
  Scenario Outline: GET_ApiDeviceTelematicDataPackagesInDateRange
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить запрос для получения телематических данных c From = "<From>", To = "<To>", Limit = "<Limit>"
    Then Сравнить полученные данные с DeviceFullRecords

    Examples:
      | From                    | To                      | Limit |
      | 2017-08-06T11%3A04%3A28 | 2018-08-09T18%3A19%3A28 | 10    |
      | 2017-08-06T11%3A04%3A28 | 2018-08-09T18%3A19%3A28 | 10    |
      | 2017-08-06T11%3A04%3A28 | 2018-08-09T18%3A19%3A28 | 10    |
      | 2017-08-06T11%3A04%3A28 | 2018-08-09T18%3A19%3A28 | 10    |
      | 2017-08-06T11%3A04%3A28 | 2018-08-09T18%3A19%3A28 | 10    |
