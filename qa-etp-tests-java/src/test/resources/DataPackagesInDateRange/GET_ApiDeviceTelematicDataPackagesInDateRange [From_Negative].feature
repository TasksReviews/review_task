#Created by Romanova Daria 08.07.2018
Feature: GET_ApiDeviceTelematicDataPackagesInDateRange [From_Negative]

  @QA-3071
  Scenario Outline: GET_ApiDeviceTelematicDataPackagesInDateRange [From_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    #Отправить GET запрос /api/deviceTelematicData/packagesInDateRange
    And Отправить запрос для получения телематических данных c некорректными From = "<From>", To = "<To>", Limit = "<Limit>"
    #Проверить, что для всех значений "<From>"
    #код состояния ответа - 400 Bad Request
    #Проверить, что для пустого параметра From
    #тело ответа равно "The value '' is invalid."
    #Проверить, что для всех остальных значений From
    #тело ответа равно "The value '<From>' is not valid for From."
    Examples:
      | From                    | To                        | Limit |
      | 2018/13/18T00%3A00%3A00 | 2018/05/18%2021%3A00%3A00 | 100   |
      | 2018/05/32T00%3A00%3A00 | 2018/05/18%2021%3A00%3A00 | 100   |
      | 2018/05/18T25%3A00%3A00 | 2018/05/18T21%3A00%3A00   | 100   |
      | 2018/05/18T00%3A61%3A00 | 2018/05/18T21%3A00%3A00   | 100   |
      | 2018/05/18T00%3A00%3A61 | 2018/05/18T21%3A00%3A00   | 100   |
      |                         | 2018/05/18T21%3A00%3A00   | 100   |
      | NULL                    | 2018/05/18T21%3A00%3A00   | 100   |
      | abcdef                  | 2018/05/18T21%3A00%3A00   | 100   |
      | 20.53                   | 2018/05/18T21%3A00%3A00   | 100   |
      | true                    | 2018/05/18T21%3A00%3A00   | 100   |