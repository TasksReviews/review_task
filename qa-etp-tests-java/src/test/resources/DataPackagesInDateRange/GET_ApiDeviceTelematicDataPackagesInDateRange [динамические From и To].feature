Feature: GET_ApiDeviceTelematicDataPackagesInDateRange [динамические From и To]

  @QA-3170
  Scenario: GET_ApiDeviceTelematicDataPackagesInDateRange [динамические From и To]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить GET запрос /api/deviceTelematicData/packagesInDateRange
    When Отправить запрос для получения телематических данных (дата From наступила, дата To - в будущем)
#Проверить, что кол-во элементов в массиве telematicRecords
#равно кол-ву записей в коллекции DeviceFullRecords
#Сравнить все поля WebApi с MongoDb
    Then Сравнить полученные данные с DeviceFullRecords
    When Отправить запрос для получения телематических данных (дата From и дата To - в будущем)
#Проверить, что возвращается пустой массив "telematicRecords": []
    Then Сравнить полученные данные с DeviceFullRecords