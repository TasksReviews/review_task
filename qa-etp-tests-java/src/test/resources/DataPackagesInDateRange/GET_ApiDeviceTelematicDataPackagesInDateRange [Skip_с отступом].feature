Feature: GET_ApiDeviceTelematicDataPackagesInDateRange [Skip_с отступом]

  @QA-2997
  Scenario Outline: GET_ApiDeviceTelematicDataPackagesInDateRange [Skip_с отступом]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить запрос для получения телематических данных c From = "<From>", To = "<To>", Limit = "<Limit>", Skip = "<Skip>"
# проверить, что пришло ожидаемое количество записей = "<Limit>"
# проверить, что отступ = "<Skip>"
# проверить, что все полученные события принадлежат указанному периоду
    Then Сравнить полученные данные с DeviceFullRecords

    Examples:
      | From                    | To                      | Limit | Skip |
      | 2017/03/01T10%3A00%3A00 | 2018/08/01T11%3A00%3A00 | 50   | 1    |
      | 2017-03-01T10%3A00%3A00 | 2018-08-01T11%3A00%3A00 | 50   | 10   |
      | 2017/03/01T00%3A00%3A00 | 2018/08/02T00%3A00%3A00 | 50   | 20   |
