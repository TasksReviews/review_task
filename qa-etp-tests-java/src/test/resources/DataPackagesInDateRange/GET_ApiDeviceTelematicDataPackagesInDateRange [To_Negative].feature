Feature: GET_ApiDeviceTelematicDataPackagesInDateRange [To_Negative]

  @QA-3066
  Scenario Outline: GET_ApiDeviceTelematicDataPackagesInDateRange [To_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# будет ошибка 400 Bad Request
#    And Отправить запрос для получения телематических данных без параметра To
# будет ошибка 400 Bad Request
    Then Отправить запрос для получения телематических данных c некорректными From = "<From>", To = "<To>", Limit = "<Limit>"


    Examples:
      | From                    | To                      | Limit |
      | 2017/12/11T10%3A00%3A00 | 2017/13/11T11%3A00%3A00 | 120   |
      | 2017-12-11T10%3A00%3A00 | 2017-12-41T11%3A00%3A00 | 120   |
      | 2017/12/11T00%3A00%3A00 | 2017/12/12T25%3A00%3A00 | 120   |
      | 2017-12-11T00%3A00%3A00 | 2017-12-12T00%3A61%3A00 | 120   |
      | 2017/12/11T10%3A00%3A00 | 2017/12/11T12%3A00%3A60 | 120   |
      | 2017/12/11T10%3A00%3A00 | NULL                    | 120   |
      | 2017/12/11T10%3A00%3A00 | 5                       | 120   |
      | 2017/12/11T10%3A00%3A00 | 5.234                   | 120   |
      | 2017/12/11T10%3A00%3A00 | true                    | 120   |
      | 2017/12/11T10%3A00%3A00 | qwert                   | 120   |
      | 2017/12/11T10%3A00%3A00 |                         | 120   |