#Created by Romanova Daria 08.07.2018
Feature: GET_ApiDeviceTelematicDataPackagesInDateRange [Limit_Negative]

  @QA-3070
  Scenario Outline: GET_ApiDeviceTelematicDataPackagesInDateRange [Limit_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    #Отправить GET запрос /api/deviceTelematicData/packagesInDateRange
    And Отправить запрос для получения телематических данных c некорректными From = "<From>", To = "<To>", Limit = "<Limit>"
    #Проверить, что для всех значений "<Limit>"
    #код состояния ответа - 400 Bad Request
    #Проверить, что для пустого параметра limit
    #тело ответа равно "The value '' is invalid."
    #Проверить, что для limit =< 0
    #тело ответа равно "Value for Limit must be between 1 and 2147483647."
    #Проверить, что для limit = NULL, limit = 2018/05/18%2020:00:00,
    #limit = abcdef, limit = 20.53, limit = true
    #тело ответа равно "The value '<Limit>' is not valid for Limit."
    Examples:
      | From                    | To                      | Limit                 |
      | 2018-06-28T10%3A00%3A00 | 2018-06-28T11%3A00%3A00 |                       |
      | 2018-06-28T10%3A00%3A00 | 2018-06-28T11%3A00%3A00 | NULL                  |
      | 2018-06-28T10%3A00%3A00 | 2018-06-28T11%3A00%3A00 | 0                     |
      | 2018-06-28T10%3A00%3A00 | 2018-06-28T11%3A00%3A00 | -10                   |
      | 2018-06-28T10%3A00%3A00 | 2018-06-28T11%3A00%3A00 | 2018-06-28%2011:00:00 |
      | 2018-06-28T10%3A00%3A00 | 2018-06-28T11%3A00%3A00 | abcdef                |
      | 2018-06-28T10%3A00%3A00 | 2018-06-28T11%3A00%3A00 | 20.53                 |
      | 2018-06-28T10%3A00%3A00 | 2018-06-28T11%3A00%3A00 | true                  |