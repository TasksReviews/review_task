Feature: GET_ApiDeviceTelematicDataPackagesInDateRange [Limit_с ограничением]

  @QA-2996
  Scenario Outline: GET_ApiDeviceTelematicDataPackagesInDateRange [Limit_с ограничением]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить запрос для получения телематических данных c From = "<From>", To = "<To>", Limit = "<Limit>"
   # проверить, что пришло ожидаемое количество записей = "<Limit>"
   # проверить, что все полученные события принадлежат указанному периоду
    Then Сравнить полученные данные с DeviceFullRecords

    Examples:
      | From                    | To                      | Limit |
      | 2017/03/01T10%3A00%3A00 | 2018/08/01T11%3A00%3A00 | 1     |
      | 2017-03-01T10%3A00%3A00 | 2018-08-01T11%3A00%3A00 | 10    |
      | 2017/01/12T05%3A49%3A10 | 2018/07/12T09%3A00%3A00 | 100   |
      | 2017/01/12T05%3A49%3A10 | 2018/07/12T05%3A50%3A00 | 100   |
