Feature: POST_ApiTelematicParsingElementPackage [Limit]

  @QA-3499
  Scenario Outline: POST_ApiTelematicParsingElementPackage [Limit]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить POST запрос /api/telematicParsingElement/Package
    And Отправить запрос для получения данных из коллекции TelematicParsingElements c Start = "<Start>", End = "<End>", deviceCodes = "<deviceCodes>", Limit = "<Limit>"
#DeviceCodes - массив типа String
#Все значения "<deviceCodes>" необходимо преобразовывать в массив
    Then Сравнить полученные записи с TelematicParsingElements

    Examples:
      | Start               | End                 | deviceCodes | Limit |
      | 2018-05-12 05:00:00 | 2018-05-12 11:00:00 | 510191      | 1     |
      | 2018-05-12 05:00:00 | 2018-05-12 11:00:00 | 510191      | 200   |
      | 2018-05-12 05:00:00 | 2018-05-12 11:00:00 | 510191      | 0     |
#Получить кол-во записей в MongoDb за указанный период запомнить в
#переменную count
#Если limit < count:
#Проверить, что кол-во записей, которые вернул метод, совпадают
#с кол-вом записей в коллеции TelematicParsingElements и <= limit
#Если limit > count:
#Проверить, что кол-во записей, которые вернул метод, совпадают
#с кол-вом записей в коллеции TelematicParsingElements и = count
#В случае limit = 0 метод должен возвращать все записи из
#коллекции TelematicParsingElements за период
#Сравнить все поля, полученные WebApi, с MongoDb
#Если метод возвращает 0 записей, проверку полей не производить