Feature: POST_ApiTelematicParsingElementPackage [динамические From и To]

  @QA-3497
  Scenario: POST_ApiTelematicParsingElementPackage [динамические From и To]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить POST запрос /api/telematicParsingElement/Package
#c параметрами "deviceCodes": ["510191"], "limit": 10
When Отправить запрос для получения данных из коллекции TelematicParsingElements за сегодня (дата start и end - сегодня), время start наступило, время end в будущем
#Проверить, что кол-во элементов в массиве packages равно кол-ву записей
#записей в коллекции TelematicParsingElements
#Сравнить все поля WebApi с MongoDb
Then Сравнить полученные записи с TelematicParsingElements
When Отправить запрос для получения данных из коллекции TelematicParsingElements за сегодня (дата start и end - сегодня), время start и end в будущем
#Проверить, что возращается пустой массив "packages": []
#Проверить, что кол-во записей в MongoDb = 0
Then Сравнить полученные записи с TelematicParsingElements