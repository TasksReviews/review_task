Feature: POST_ApiTelematicParsingElementPackage [несколько ТМУ]

  @QA-3489
  Scenario: POST_ApiTelematicParsingElementPackage [несколько ТМУ]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить POST запрос /api/telematicParsingElement/Package
#с параметрами из Data table
    And Отправить запрос для получения данных из коллекции TelematicParsingElements c параметрами
      | deviceCodes | start               | end                 |
      | 510191      | 2018-04-02 06:00:00 | 2018-04-02 07:00:00 |
      | 510190      | 2018-04-02 06:00:00 | 2018-04-02 07:00:00 |
#Проверить, что метод возвращает два массива packages[] -
#один для "deviceCode": "510190", второй для "deviceCode": "510191"
#Проверить, что кол-во элементов в двух массивах packages[] равно
#кол-ву записей в MongoDb
#Сравнить все поля WebApi с MongoDb
    Then Сравнить поля полученных записей с TelematicParsingElements