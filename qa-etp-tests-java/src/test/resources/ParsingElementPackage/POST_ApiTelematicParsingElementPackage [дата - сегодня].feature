Feature: POST_ApiTelematicParsingElementPackage [дата - сегодня]

  @QA-3496
  Scenario: POST_ApiTelematicParsingElementPackage [дата - сегодня]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить POST запрос /api/telematicParsingElement/Package
#"deviceCodes": ["510191"]
#"limit" : 10
    When Отправить запрос для получения данных из коллекции TelematicParsingElements за сегодня (дата start и end - сегодня), время start и end наступило
#Получить кол-во записей в коллекции TelematicParsingElements
#за выбранный период для DeviceCode : "510191" и запомнить в переменную count
#Если limit =< count
#Проверить, что кол-во элементов в массиве packages = limit
#Если limit > count
#Проверить, что кол-во элементов в массиве packages = count
#Сравнить все поля WebApi с MongoDb
    Then Сравнить полученные записи с TelematicParsingElements
    When Отправить запрос для получения данных из коллекции TelematicParsingElements за сегодня (дата start и end - сегодня), время start наступило, время end в будущем
#Провести проверки из предыдущего шага Then
    Then Сравнить полученные записи с TelematicParsingElements
    When Отправить запрос для получения данных из коллекции TelematicParsingElements за сегодня (дата start и end - сегодня), время start и end в будущем
#Проверить, что возращается пустой массив "packages": []
    Then Сравнить полученные записи с TelematicParsingElements
