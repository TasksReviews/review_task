Feature: POST_ApiTelematicParsingElementPackage [одно ТМУ]

  @QA-3488
  Scenario Outline: POST_ApiTelematicParsingElementPackage [одно ТМУ]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
#Отправить POST запрос /api/telematicParsingElement/Package
#Установить limit = 10
    And Отправить запрос для получения данных из коллекции TelematicParsingElements c Start = "<Start>", End = "<End>", deviceCodes = "<deviceCodes>"
#DeviceCodes - массив типа String
#Все значения "<deviceCodes>" необходимо преобразовывать в массив
    Then Сравнить полученные записи с TelematicParsingElements

    Examples:
      | Start               | End                 | deviceCodes |
      | 2018-04-02 06:00:00 | 2018-04-02 07:00:00 | 510191      |
      | 2018-04-02 06:00:00 | 2018-04-03 07:00:00 | 510190      |
      | 2018-04-02 06:00:00 | 2018-04-02 06:00:00 | 510190      |
#Получить кол-во записей в MongoDb за указанный период запомнить в
#переменную count
#Если limit < count:
#Проверить, что кол-во записей, которые вернул метод, совпадают
#с кол-вом записей в коллеции TelematicParsingElements и <= limit
#Если limit > count:
#Проверить, что кол-во записей, которые вернул метод, совпадают
#с кол-вом записей в коллеции TelematicParsingElements и = count
#В случае limit = 0 метод должен возвращать все записи из
#коллекции TelematicParsingElements за период
#Сравнить все поля, полученные WebApi, с MongoDb
#Если метод возвращает 0 записей, проверку полей не производить