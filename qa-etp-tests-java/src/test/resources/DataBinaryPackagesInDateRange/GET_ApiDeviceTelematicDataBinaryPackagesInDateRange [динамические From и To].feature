Feature: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [динамические From и To]

  @QA-3235
  Scenario: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [динамические From и To]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# Отправить GET запрос /api/deviceTelematicData/BinaryPackagesInDateRange
    When Отправить запрос для получения телематических данных в бинарном виде за сегодня (дата - сегодня, время - наступило)
# Проверить, что кол-во элементов в массиве telematicRecords равно кол-ву записей в коллекции DeviceFullRecords
# Сравнить все поля WebApi с MongoDb
    Then Сравнить бинарные данные с DeviceFullRecords
# Проверить, что возвращается пустой массив "telematicRecords": []
    When Отправить запрос для получения телематических данных в бинарном виде за сегодня (дата - сегодня, время - в будущем)
#    Then Сравнить бинарные данные с DeviceFullRecords