Feature: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [From больше To]

  @QA-3075
  Scenario Outline: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [From больше To]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# проверить, что код состояния ответа - 400 Bad Request
# проверить, что в теле ответа приходит "Start date can not be greater end date"
    Then Отправить запрос для получения телематических данных в бинарном виде c From = "<From>", To = "<To>", Limit = "<Limit>"
    Then Проверить, что код состояния ответа - 400 Bad Request
    Examples:
      | From                    | To                      | Limit |
      | 2017-08-06T11%3A04%3A28 | 2018-07-09T18%3A19%3A28 | 10    |
      | 2017-08-06T11%3A04%3A28 | 2018-07-09T18%3A19%3A28 | 10    |
      | 2017-08-06T11%3A04%3A28 | 2018-07-09T18%3A19%3A28 | 10    |
      | 2017-08-06T11%3A04%3A28 | 2018-07-09T18%3A19%3A28 | 10    |
      | 2017-08-06T11%3A04%3A28 | 2018-07-09T18%3A19%3A28 | 10    |
