Feature: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [Skip_Negative]

  @QA-3073
  Scenario Outline: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [Skip_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить запрос для получения телематических данных в бинарном виде c From = "<From>", To = "<To>", Limit = "<Limit>", Skip = "<Skip>"
# Проверить, что запрос возвращает ошибку 400 BadRequest
# Проверить сообщение об ошибке в теле ответа
# "The value "<Skip>" is not valid for Skip." - для непустых значений Skip
# "The value '' is invalid." - для пустого Skip
    Then Проверить, что код состояния ответа - 400 Bad Request

    Examples:
      | From                            | To                              | Limit | Skip  |
      | 2018-06-01T03%3A00%3A00.000000Z | 2018-07-23T15%3A52%3A13.966659Z | 1000  |       |
      | 2018-06-01T03%3A00%3A00.000000Z | 2018-07-23T15%3A52%3A13.966659Z | 1000  | -10   |
      | 2018-06-01T03%3A00%3A00.000000Z | 2018-07-23T15%3A52%3A13.966659Z | 10000 | NULL  |
      | 2018-06-01T03%3A00%3A00.000000Z | 2018-07-23T15%3A52%3A13.966659Z | 10000 | 5.23  |
      | 2018-06-01T03%3A00%3A00.000000Z | 2018-07-23T15%3A52%3A13.966659Z | 10000 | false |
      | 2018-06-01T03%3A00%3A00.000000Z | 2018-07-23T15%3A52%3A13.966659Z | 10000 | asdgh |