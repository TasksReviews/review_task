Feature: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [From_Nagative]

  @QA-3077
  Scenario Outline: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [From_Nagative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# Отправить GET запрос /api/deviceTelematicData/BinaryPackagesInDateRange
    And Отправить запрос для получения телематических данных в бинарном виде c From = "<From>", To = "<To>", Limit = "<Limit>"
# Проверить, что для всех значений "<From>" код состояния ответа - 400 Bad Request
# Проверить, что для пустого параметра From тело ответа равно "The value '' is invalid."
# Проверить, что для всех остальных значений From тело ответа равно "The value '<From>' is not valid for From."
    Then Проверить, что код состояния ответа - 400 Bad Request

    Examples:
      | From                    | To                      | Limit |
      | 2018-22-01T03%3A00%3A00 | 2018-07-23T15%3A52%3A13 | 1200  |
      | 2018-12-41T03%3A00%3A00 | 2018-07-23T15%3A52%3A13 | 1200  |
      | 2018-12-01T03%3A50%3A00 | 2018-07-23T15%3A52%3A13 | 1200  |
      | 2018-06-01T03%3A60%3A00 | 2018-07-23T15%3A52%3A13 | 1200  |
      | 2018-06-01T03%3A00%3A61 | 2018-07-23T15%3A52%3A13 | 1200  |
      | NULL                    | 2018-07-23T15%3A52%3A13 | 1200  |
      | 5                       | 2018-07-23T15%3A52%3A13 | 1200  |
      | 5.234                   | 2018-07-23T15%3A52%3A13 | 1200  |
      | true                    | 2018-07-23T15%3A52%3A13 | 1200  |
      | qwert                   | 2018-07-23T15%3A52%3A13 | 1200  |
      |                         | 2018-07-23T15%3A52%3A13 | 1200  |
