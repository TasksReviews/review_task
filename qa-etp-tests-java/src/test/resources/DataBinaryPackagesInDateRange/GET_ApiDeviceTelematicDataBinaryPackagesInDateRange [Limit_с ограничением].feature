Feature: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [Limit_с ограничением]

  @QA-3048
  Scenario Outline: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [Limit_с ограничением]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить запрос для получения телематических данных в бинарном виде c From = "<From>", To = "<To>", Limit = "<Limit>"
# проверить, что пришло ожидаемое количество записей = "<Limit>"
# проверить, что все полученные события принадлежат указанному периоду
# cравнить все поля WebApi с MongoDb
    Then Сравнить бинарные данные с DeviceFullRecords

    Examples:
      | From                    | To                      | Limit |
      | 2018-06-01T03%3A00%3A00 | 2018-07-23T15%3A52%3A13 | 1     |
      | 2018-06-01T03%3A00%3A00 | 2018-07-23T15%3A52%3A13 | 10    |
      | 2018-06-01T03%3A00%3A00 | 2018-07-23T15%3A52%3A13 | 20    |