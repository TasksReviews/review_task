Feature: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [Skip_с отступом]

  @QA-3049
  Scenario Outline: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [Skip_с отступом]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
    And Отправить запрос для получения телематических данных в бинарном виде c From = "<From>", To = "<To>", Limit = "<Limit>", Skip = "<Skip>"
# проверить, что пришло ожидаемое количество записей = "<Limit>"
# проверить, что отступ = "<Skip>"
# проверить, что все полученные события принадлежат указанному периоду
    Then Сравнить бинарные данные с DeviceFullRecords

    Examples:
      | From                            | To                              | Limit | Skip |
      | 2018-06-01T03%3A00%3A00.000000Z | 2018-07-23T15%3A52%3A13.966659Z | 50    | 1    |
      | 2018-06-01T03%3A00%3A00.000000Z | 2018-07-23T15%3A52%3A13.966659Z | 50    | 3    |