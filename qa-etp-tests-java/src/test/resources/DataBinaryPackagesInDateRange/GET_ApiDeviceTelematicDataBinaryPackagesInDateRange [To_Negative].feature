Feature: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [To_Negative]

  @QA-3078
  Scenario Outline: GET_ApiDeviceTelematicDataBinaryPackagesInDateRange [To_Negative]
    Given Залогиниться под пользователем с ролью суперадмин с помощью jwt-token
# Отправить GET запрос /api/deviceTelematicData/BinaryPackagesInDateRange
    And Отправить запрос для получения телематических данных в бинарном виде c From = "<From>", To = "<To>", Limit = "<Limit>"
# Проверить, что для всех значений "<To>" код состояния ответа - 400 Bad Request
# Проверить, что для пустого параметра To тело ответа равно "The value '' is invalid."
# Проверить, что для всех остальных значений To тело ответа равно "The value '<To>' is not valid for To."
    Then Проверить, что код состояния ответа - 400 Bad Request

    Examples:
      | From                            | To                              | Limit |
      | 2018-12-11T10%3A00%3A00.000000Z | 2018-13-11T11%3A00%3A00.966659Z | 1200  |
      | 2018-12-11T10%3A00%3A00.000000Z | 2018-13-11T11%3A00%3A00.966659Z | 1200  |
      | 2018-12-11T12%3A00%3A00.000000Z | 2018-12-12T11%3A25%3A00.966659Z | 1200  |
      | 2018-12-11T12%3A00%3A60.000000Z | 2018-12-12T11%3A25%3A61.966659Z | 1200  |
      | 2018-12-11T12%3A00%3A00.000000Z | 2018-12-12T11%3A25%3A60.966659Z | 1200  |
      | 2018-12-11T12%3A00%3A00.000000Z | NULL                            | 1200  |
      | 2018-12-11T12%3A00%3A00.000000Z | 5                               | 1200  |
      | 2018-12-11T12%3A00%3A00.000000Z | 5.234                           | 1200  |
      | 2018-12-11T12%3A00%3A00.000000Z | true                            | 1200  |
      | 2018-12-11T12%3A00%3A00.000000Z | qwert                           | 1200  |
      | 2018-12-11T12%3A00%3A00.000000Z |                                 | 1200  |