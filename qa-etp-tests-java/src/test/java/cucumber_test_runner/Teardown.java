package cucumber_test_runner;


import com.t1.core.AbstractClass;
import cucumber.api.Scenario;
import cucumber.api.java.After;

public class Teardown extends AbstractClass {
	private Scenario scenario;

    public Teardown() {
        super();
    }

    // common teardown method to delete accounts/vehicles etc.
	@After
	public static void afterTest_common(Scenario scenario) {
	   // this.scenario = scenario;
		log.info(">>>>>>>Execution for test "+scenario.getName()+" has been completed. Start cleanup.");
		log.debug(">>>>>>>cleanup completed.");
	}

	/**
	 * @return the scenario
	 */
	public Scenario getScenario() {
		return scenario;
	}

	/**
	 * @param scenario the scenario to set
	 */
	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}
}
