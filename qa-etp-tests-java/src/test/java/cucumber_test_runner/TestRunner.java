package cucumber_test_runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/XrayFeaturesByModules/", plugin = {
                "html:target/cucumber-reports/html/", "junit:target/cucumber-reports/junit.xml",
                "json:target/cucumber-reports/jsonreport.json" }, glue = { "classpath:cucumber_test_runner",
                                "classpath:Infrastructure.Protobuf.Contracts.TelematicRecords", "classpath:com.t1.core",
                                "classpath:api.dev", "classpath:reports", "classpath:api.utils",
                                "classpath:DevAPIMethods", "classpath:Login" })
public class TestRunner {

}
