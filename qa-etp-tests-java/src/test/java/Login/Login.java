package Login;

import com.t1.core.AbstractClass;

import org.apache.log4j.Logger;

import cucumber.api.java.en.Given;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;
public class Login {

    @Step("Авторизация: пользователь с ролью суперадмин")
    @Given("^Залогиниться под пользователем с ролью суперадмин с помощью jwt-token$")
    public void lgoinAsUerWithJWTToken() {
        Logger.getLogger(">>>").info("method started: lgoinAsUerWithJWTToken");
        getJWTToken();
        Logger.getLogger(">>>").info("method ended");
    }

    @Attachment("параметр запроса: JWTToken")
    private String getJWTToken() {
        Logger.getLogger(">>>").info(AbstractClass.getProperty("authorization"));
        return AbstractClass.getProperty("authorization");
    }
}
