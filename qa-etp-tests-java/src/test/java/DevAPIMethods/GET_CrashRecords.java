package DevAPIMethods;

import api.dev.CrashRecordsReport;
import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.PendingException;
import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class GET_CrashRecords {
    private CrashRecordsReport crashRecordsReport = new CrashRecordsReport();

    @ContinueNextStepsFor(AssertionError.class)
    @Then("^Отправить запрос для получения списка ДТП за период c From = \"([^\"]*)\", To = \"([^\"]*)\"$")
    public void отправитьЗапросДляПолученияСпискаДТПЗаПериодCFromTo(String arg0, String arg1) {
        crashRecordsReport.request(arg0, arg1);
        crashRecordsReport.response();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Then("^Сравнить полученные данные с RoadAccidents$")
    public void сравнитьПолученныеДанныеСRoadAccidents() throws UnirestException, ParseException {
        crashRecordsReport.getDataFromMongo();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @And("^Отправить некорректный запрос для получения списка ДТП за период c From = \"([^\"]*)\", To = \"([^\"]*)\"$")
    public void отправитьНекорректныйЗапросДляПолученияСпискаДТПЗаПериодCFromTo(String arg0, String arg1) throws Throwable {
        crashRecordsReport.request(arg0, arg1);
        crashRecordsReport.negativeResponse();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @And("^Выполнить запрос для получения телематических данных за сегодня \\(дата - сегодня, время - наступило\\)$")
    public void выполнитьЗапросДляПолученияТелематическихДанныхЗаСегодняДатаСегодняВремяНаступило() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.HOUR, -24);
        to.add(Calendar.HOUR, -12);
        crashRecordsReport.request(simpleDateFormat.format(from.getTime()), simpleDateFormat.format(to.getTime()));
        crashRecordsReport.response();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @And("^Выполнить запрос для получения телематических данных за сегодня \\(дата - сегодня,  время - в будущем\\)$")
    public void выполнитьЗапросДляПолученияТелематическихДанныхЗаСегодняДатаСегодняВремяВБудущем() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.MINUTE, 10);
        to.add(Calendar.MINUTE, 30);
        crashRecordsReport.request(simpleDateFormat.format(from.getTime()), simpleDateFormat.format(to.getTime()));
        crashRecordsReport.response();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @And("^Отправить запрос для получения телематических данных \\(дата From - в прошлом, дата To - в будущем\\)$")
    public void отправитьЗапросДляПолученияТелематическихДанныхДатаFromВПрошломДатаToВБудущем() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.MINUTE, -30);
        to.add(Calendar.MINUTE, 10);
        crashRecordsReport.request(simpleDateFormat.format(from.getTime()), simpleDateFormat.format(to.getTime()));
        crashRecordsReport.response();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @And("^Отправить запрос для получения телематических данных \\(когда дата From и дата To - в будущем\\)$")
    public void отправитьЗапросДляПолученияТелематическихДанныхКогдаДатаFromИДатаToВБудущем() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.MINUTE, 10);
        to.add(Calendar.MINUTE, 30);
        crashRecordsReport.request(simpleDateFormat.format(from.getTime()), simpleDateFormat.format(to.getTime()));
        crashRecordsReport.response();
    }
}
