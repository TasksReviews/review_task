package DevAPIMethods;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import reports.BinaryDeviceTelematicDataPackagesInDateRangeReport;

public class GET_ApiDeviceTelematicDataBinaryPackagesInDateRange {
    BinaryDeviceTelematicDataPackagesInDateRangeReport report = new BinaryDeviceTelematicDataPackagesInDateRangeReport();

    @And("^Отправить запрос для получения телематических данных в бинарном виде c From = \"([^\"]*)\", To = \"([^\"]*)\", Limit = \"([^\"]*)\"$")
    public void sendRequestToGetTelematicRecordsInBinaryForTodayWithFromToLimit(String arg0, String arg1, String arg2)
            throws Throwable {
        report.setBinaryRequest(arg0, arg1, arg2);
        report.getResponse();
    }

    @And("^Отправить запрос для получения телематических данных в бинарном виде c From = \"([^\"]*)\", To = \"([^\"]*)\", Limit = \"([^\"]*)\", Skip = \"([^\"]*)\"$")
    public void sendRequestToGetTelematicRecordsInBinaryForTodayWithFromToSkipLimit(String arg0, String arg1,
            String arg2, String arg3) throws Throwable {
        report.setBinaryRequest(arg0, arg1, arg2, arg3);
        report.getResponse();
    }

    @Then("^Сравнить бинарные данные с DeviceFullRecords$")
    public void checkRecordsInDeviceFullRecords() throws Throwable {
        report.checkBinaryFields();
    }

    @Then("^Проверить, что код состояния ответа - (\\d+) Bad Request$")
    public void checkResponseStatusEqualsBadRequest(int arg0) throws Throwable {
        report.getNegativeResponse(arg0);
    }

    @And("^Отправить запрос для получения телематических данных в бинарном виде c Limit = \"([^\"]*)\"$")
    public void sendRequestToGetTelematicRecordsInBinaryForTodayWithLimit(String arg0) throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.MINUTE, -30);
        to.add(Calendar.MINUTE, -10);
        report.setBinaryRequest(simpleDateFormat.format(from.getTime()), simpleDateFormat.format(to.getTime()), arg0);
        report.getResponse();
    }

    @When("^Отправить запрос для получения телематических данных в бинарном виде за сегодня \\(дата - сегодня, время - наступило\\)$")
    public void sendRequestToGetTelematicRecordsInBinaryForTodayTimeInPresent() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss.000000");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.MINUTE, -10);
        to.add(Calendar.MINUTE, -5);
        String format = simpleDateFormat.format(from.getTime()) + "Z";
        String format1 = simpleDateFormat.format(to.getTime()) + "Z";
        report.setBinaryRequest(format, format1, "1");
        report.getResponse();
    }

    @When("^Отправить запрос для получения телематических данных в бинарном виде за сегодня \\(дата - сегодня, время - в будущем\\)$")
    public void sendRequestToGetTelematicRecordsInBinaryForTodayTimeInFuture() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.MINUTE, 1);
        to.add(Calendar.MINUTE, 10);
        report.setBinaryRequest(simpleDateFormat.format(from.getTime()), simpleDateFormat.format(to.getTime()), "1");
        report.getResponse();
    }
}
