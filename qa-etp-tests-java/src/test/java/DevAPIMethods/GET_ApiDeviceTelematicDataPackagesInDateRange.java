package DevAPIMethods;

import cucumber.api.java.Before;
import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;
import reports.DeviceTelematicDataPackagesInDateRangeReport;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GET_ApiDeviceTelematicDataPackagesInDateRange {
    DeviceTelematicDataPackagesInDateRangeReport report;

    @Before
    public void initClass() {
        report = new DeviceTelematicDataPackagesInDateRangeReport();
    }

    @ContinueNextStepsFor(AssertionError.class)
    @And("^Отправить запрос для получения телематических данных c From = \"([^\"]*)\", To = \"([^\"]*)\", Limit = \"([^\"]*)\"$")
    public void sendRequestToGetDataWithoutSkip(String arg0, String arg1, String arg2) throws Throwable {
        Logger.getLogger(">>>").info("method started: sendRequestToGetDataWithoutSkip");
        report.request(arg0, arg1, arg2);
        report.getResponse();
        Logger.getLogger(">>>").info("method ended");
    }

    @ContinueNextStepsFor(AssertionError.class)
    @And("^Отправить запрос для получения телематических данных c некорректными From = \"([^\"]*)\", To = \"([^\"]*)\", Limit = \"([^\"]*)\"$")
    public void sendRequestToGetDataWithoutSkipNegative(String arg0, String arg1, String arg2) throws Throwable {
        Logger.getLogger(">>>").info("method started: sendRequestToGetDataWithoutSkip");
        report.request(arg0, arg1, arg2);
        report.getNegativeResponse();
        report.checkResponseBodyLimit(arg2);
        Logger.getLogger(">>>").info("method ended");
    }

    @ContinueNextStepsFor(AssertionError.class)
    @And("^Отправить запрос для получения телематических данных c некорректными From = \"([^\"]*)\", To = \"([^\"]*)\", Limit = \"([^\"]*)\", Skip = \"([^\"]*)\"$")
    public void sendRequestToGetDataWithSkipNegative(String arg0, String arg1, String arg2, String arg3) throws Throwable {
        Logger.getLogger(">>>").info("method started: sendRequestToGetDataWithSkip");
        report.request(arg0, arg1, arg2, arg3);
        report.getNegativeResponse();
        Logger.getLogger(">>>").info("method ended");
    }

    @ContinueNextStepsFor(AssertionError.class)
    @And("^Отправить запрос для получения телематических данных c From = \"([^\"]*)\", To = \"([^\"]*)\", Limit = \"([^\"]*)\", Skip = \"([^\"]*)\"$")
    public void sendRequestToGetDataWithSkip(String arg0, String arg1, String arg2, String arg3) throws Throwable {
        Logger.getLogger(">>>").info("method started: sendRequestToGetDataWithSkip");
        report.request(arg0, arg1, arg2, arg3);
        report.getResponse();
        Logger.getLogger(">>>").info("method ended");
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Then("^Сравнить полученные данные с DeviceFullRecords$")
    public void checkDataWithDeviceFullRecords() throws Throwable {
        Logger.getLogger(">>>").info("method started: checkDataWithDeviceFullRecords");
        report.mongoRequest();
        report.dataFromMongo();
        Logger.getLogger(">>>").info("method ended");
    }

    @ContinueNextStepsFor(AssertionError.class)
    @Then("^Отправить запрос для получения телематических данных с параметром Skip = ([^\"]*)$")
    public void sendRequestWithSkip(String arg0) throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String time = simpleDateFormat.format(calendar.getTime());
        calendar.add(Calendar.MINUTE, 1);
        String time1 = simpleDateFormat.format(calendar.getTime());
        report.request(time, time1, "100", arg0);
    }

    @When("^Отправить запрос для получения телематических данных за сегодня \\(дата - сегодня, время - наступило\\)$")
    public void sendRequestForTelematicDataForTodayDateInPresent() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar1.setTime(new Date());
        calendar.add(Calendar.HOUR, -10);
        calendar1.add(Calendar.MINUTE, -1);
        String time = simpleDateFormat.format(calendar.getTime());
        String time1 = simpleDateFormat.format(calendar1.getTime());
        report.request(time, time1, "10");
        report.getResponse();
        report.mongoRequest();
        report.dataFromMongo();
    }

    @When("^Отправить запрос для получения телематических данных за сегодня \\(дата - сегодня, время - в будущем\\)$")
    public void sendRequestForTelematicDataForTodayDateInFuture() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar1.setTime(new Date());
        calendar.add(Calendar.HOUR, 3);
        calendar1.add(Calendar.HOUR, 5);
        String time = simpleDateFormat.format(calendar.getTime());
        String time1 = simpleDateFormat.format(calendar1.getTime());
        report.request(time, time1, "10");
        report.getResponse();
        report.mongoRequest();
        report.dataFromMongo();
    }

    @When("^Отправить запрос для получения телематических данных \\(дата From наступила, дата To - в будущем\\)$")
    public void sendRequestForTelematicDataFromInPresentToInFuture() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar1.setTime(new Date());
        calendar.add(Calendar.HOUR, -24);
        calendar1.add(Calendar.HOUR, 24);
        String time = simpleDateFormat.format(calendar.getTime());
        String time1 = simpleDateFormat.format(calendar1.getTime());
        report.request(time, time1, "10");
        report.getResponse();
        report.mongoRequest();
        report.dataFromMongo();

    }

    @When("^Отправить запрос для получения телематических данных \\(дата From и дата To - в будущем\\)$")
    public void sendRequestForTelematicDataFromAndToInFuture() throws Throwable {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH'%3A'mm'%3A'ss");
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar1.setTime(new Date());
        calendar.add(Calendar.HOUR, 24);
        calendar1.add(Calendar.HOUR, 48);
        String time = simpleDateFormat.format(calendar.getTime());
        String time1 = simpleDateFormat.format(calendar1.getTime());
        report.request(time, time1, "10");
        report.getResponse();
        report.mongoRequest();
        report.dataFromMongo();
    }
}