package DevAPIMethods;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.log4j.Logger;
import reports.DeviceTelematicDataTrackRecordingReport;

import java.util.Collections;

public class POST_ApiDeviceTelematicDataTrackRecording {
    DeviceTelematicDataTrackRecordingReport report;

    @Before
    public void initFields() {
        report = new DeviceTelematicDataTrackRecordingReport();
    }

    @Given("^Отправить команду о выключении записи трэка для ТМУ с кодами \"([^\"]*)\"$")
    public void setCommandToSwitchOffTracks(String arg2) throws Throwable {
        Logger.getLogger(">>>").info("method started: setCommandToSwitchOffTracks");
        report.getOffRequest(Collections.singletonList(arg2));
        report.getResponse();
        Logger.getLogger(">>>").info("method ended");
    }

    @Given("^Отправить команду о включении записи трэка для ТМУ с кодами \"([^\"]*)\"$")
    public void setCommandToSwitchOnTracks(String arg2) throws Throwable {
        Logger.getLogger(">>>").info("method started: setCommandToSwitchOnTracks");
        report.getOnRequest(Collections.singletonList(arg2));
        report.getResponse();
        Logger.getLogger(">>>").info("method ended");
    }

    @Given("^Отправить команду о выключении записи трэка для ТМУ с кодами$")
    public void setCommandToSwitchOffTracks(DataTable arg1) throws Throwable {
        Logger.getLogger(">>>").info("method started: setCommandToSwitchOffTracks");
        report.getOffRequest(arg1.asList(String.class));
        report.getResponse();
        report.checkOffRecords();
        Logger.getLogger(">>>").info("method ended");
    }

    @Given("^Отправить команду о включении записи трэка для ТМУ с кодами$")
    public synchronized  void setCommandToSwitchOnTracks(DataTable arg1) throws Throwable {
        Logger.getLogger(">>>").info("method started: setCommandToSwitchOnTracks");
        report.getOnRequest(arg1.asList(String.class));
        report.getResponse();
        report.checkOnRecords();
        Logger.getLogger(">>>").info("method ended");
    }

    @Then("^Проверить наличие записей в коллекции TrackRecordingElements для ТМУ с кодами$")
    public synchronized void checkRecordsInMongoDb() {
        Logger.getLogger(">>>").info("method started: checkRecordsInMongoDb");
        report.checkRecords();
        Logger.getLogger(">>>").info("method ended");
    }

    @Then("^Проверить отсутствие записей в коллекции TrackRecordingElements для ТМУ с кодами$")
    public synchronized void checkRecordsAreAbsentInMongo() {
        Logger.getLogger(">>>").info("method started: checkRecordsAreAbsentInMongo");
        report.checkRecords();
        Logger.getLogger(">>>").info("method ended");
    }

    @And("^Отправить команду о выключении записи трэка для ТМУ без указания номера ТМУ$")
    public void sendRequestOffTelematicRecordsWithOutDeviceCode() throws Throwable {
        Logger.getLogger(">>>").info("method started: sendRequestOffTelematicRecordsWithOutDeviceCode");
        report.getOffNullRequest();
        report.getResponse();
        Logger.getLogger(">>>").info("method ended");
    }

    @Then("^Отправить команду о включении записи трэка для ТМУ без указания номера ТМУ$")
    public void sendRequestOnTelematicRecordsWithOutDeviceCode() throws Throwable {
        Logger.getLogger(">>>").info("method started: sendRequestOnTelematicRecordsWithOutDeviceCode");
        report.getOnNullRequest();
        report.getResponse();
        Logger.getLogger(">>>").info("method ended");
    }

    @And("^Отправить команду о включении записи трэка для ТМУ с кодами \"([^\"]*)\" и командами \"([^\"]*)\"$")
    public void sendRequestOnTelematicRecordsWithCodesAndCommands(String arg0, String arg1) throws Throwable {
        report.getOnRequest(Collections.singletonList(arg0), arg1);
        report.getResponse();

    }

    @Then("^Проверить отсутствие записей в коллекции TrackRecordingElements для ТМУ с кодами \"([^\"]*)\"$")
    public synchronized void checkNoReportsInTrackRecordingElementsForDeviceCode(String arg0) throws Throwable {
        report.checkRecords();
    }
}