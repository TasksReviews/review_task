package DevAPIMethods;

import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.log4j.Logger;
import reports.DeviceLocationReport;

import java.text.ParseException;
import java.util.Collections;


public class POST_ApiDeviceTelematicDataDeviceLocation {
    DeviceLocationReport report;

    @Before
    public void initFields() {
        report = new DeviceLocationReport();

    }

    @Given("^Запросить данные о последней позиции всех заданных ТМУ с кодами \"([^\"]*)\"$")
    public void getDataAboutLastRecDeviceWithCode(String arg1) throws UnirestException {
        Logger.getLogger(">>>").info("method started: getDataAboutLastRecDeviceWithCode");
        report.getRequest(Collections.singletonList(arg1));
        report.getResponse();
        Logger.getLogger(">>>").info("method ended");
    }

    @Given("^Запросить данные о последней позиции всех заданных ТМУ$")
    public void getDataAboutLastRecDeviceWithCode(DataTable arg1) throws UnirestException {
        Logger.getLogger(">>>").info("method started: getDataAboutLastRecDeviceWithCode");
        report.getRequest(arg1.asList(String.class));
        report.getResponse();
        Logger.getLogger(">>>").info("method ended");
    }

    @Then("^Сравнить полученные записи с DeviceFullRecords$")
    public void checkDataWithDataBaseDformat2eviceFullRecords() throws ParseException {
        Logger.getLogger(">>>").info("method started: checkDataWithDataBaseDformat2eviceFullRecords");
        report.generateMongoRequest();
        report.getResponseFromMongo();
        report.checkFields();
        Logger.getLogger(">>>").info("method ended");
    }

    @Then("^Запросить данные о последней позиции ТМУ без указания номера ТМУ$")
    public void getRequestWithoutDeviceCode() throws Throwable {
        report.getRequest();
        report.getNegativeResponse();
    }
}