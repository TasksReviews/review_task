package DevAPIMethods;

import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;

import reports.ApiDeviceTelematicDataTrackRecordedPackagesReport;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class GET_ApiDeviceTelematicDataTrackRecordedPackages {
    private ApiDeviceTelematicDataTrackRecordedPackagesReport report;

    public GET_ApiDeviceTelematicDataTrackRecordedPackages() {
        report = new ApiDeviceTelematicDataTrackRecordedPackagesReport();
    }

    @And("^Отправить запрос для получения телематических записей трека, когда дата From - в прошлом, дата To - в будущем$")
    public synchronized void sendRequestFromInPastToInFuture() throws Throwable {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MONTH, -3);
        instance1.add(Calendar.MINUTE, 2);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(format, format1, instance2.getTime());
        report.getResponse();
        report.mongoRequest();
        report.dataFromMongo();
    }

    @And("^Отправить запрос для получения телематических записей трека, когда дата From и дата To - в будущем$")
    public synchronized void sendRequestFromToInFuture() throws Throwable {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MONTH, 2);
        instance1.add(Calendar.MINUTE, 3);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(format, format1, instance2.getTime());
        report.getResponse();
        report.mongoRequest();
        report.dataFromMongo();
    }

    @Then("^Отправить запрос для получения телематических записей трека за сегодня$")
    public synchronized void sendRequestTodayRecords() throws InterruptedException, UnirestException, ParseException {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.HOUR, -24);
        instance1.add(Calendar.MINUTE, 5);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(format, format1, instance2.getTime());
        report.getNoResponse();
        report.mongoRequest();
        report.dataFromMongo();
    }

    @Then("^Отправить запрос для получения телематических записей трека c To = \"([^\"]*)\"$")
    public synchronized void sendRequestIncorrectTo(String arg0) throws Throwable {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -2);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(format, format1, arg0, instance2.getTime());
        report.getResponse();
    }

    @Then("^Отправить запрос для получения телематических записей трека с параметром Skip < (\\d+)$")
    public synchronized void sendRequestSkipLessThenZero(int arg0) throws UnirestException, InterruptedException {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, 1);
        instance1.add(Calendar.MINUTE, 2);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(format, format1, String.valueOf(arg0 - 1), instance2.getTime());
        report.getResponse();
    }

    @And("^Отправить запрос для получения телематических записей трека с Limit = \"([^\"]*)\", Skip = \"([^\"]*)\"$")
    public synchronized void sendRequestWithSkipAndLimit(String arg0, String arg1) throws Throwable {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MONTH, -3);
        instance1.add(Calendar.MINUTE, 2);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(format, format1, arg0, arg1, instance2.getTime());
        report.getResponse();
        report.mongoRequest();
        report.dataFromMongo();
    }

    @And("^Отправить запрос для получения телематических записей трека за сегодня дата - сегодня, время - несколько минут после отправки команды о включении$")
    public synchronized void sendRequestTodayAfterWithParameters()
            throws InterruptedException, UnirestException, ParseException {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.HOUR, -12);
        instance1.add(Calendar.MINUTE, 5);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(format, format1, instance2.getTime());
        report.getResponse();
        report.checkCountNotNull();
        report.mongoRequest();
        report.dataFromMongo();
    }

    @And("^Отправить запрос для получения телематических записей трека за сегодня дата - сегодня, время - несколько минут до отправки команды о включении$")
    public synchronized void sendRequestTodayBeforeWithParameters() throws Throwable {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -2);
        instance1.add(Calendar.MINUTE, -1);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(format, format1, instance2.getTime());
        report.getNoResponse();
    }

    @And("^Отправить запрос для получения телематических записей трека с Limit = \"([^\"]*)\"$")
    public synchronized void sendRequestWithLimit(String arg0)
            throws InterruptedException, UnirestException, ParseException {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Date requestTime = Calendar.getInstance().getTime();
        instance.add(Calendar.MONTH, -3);
        instance1.add(Calendar.MINUTE, 1);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.limitRequest(format, format1, arg0, requestTime);
        report.getResponse();
        report.mongoRequest();
        report.dataFromMongo();
    }

    @Then("^Отправить запрос для получения телематических записей трека при условии, что дата From > даты To$")
    public synchronized void sendRequestWithFromMoreThanTo() throws Throwable {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -1);
        instance1.add(Calendar.MINUTE, -15);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(format, format1, instance2.getTime());
        report.getNegativeResponse();
    }

    @Then("^Отправить запрос для получения телематических записей трека c From = \"([^\"]*)\"$")
    public synchronized void sendRequestWithFromEquals(String arg0) throws Throwable {
        wait(3000);
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance1.add(Calendar.MINUTE, -5);
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.request(arg0, format1, instance2.getTime());
        report.getResponse();
    }

    @And("^Отправить запрос для получения телематических данных без параметра Limit$")
    public synchronized void sendRequestWithoutLimit() throws Throwable {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -2);
        instance1.add(Calendar.MINUTE, -5);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.requestWithoutLimit(format, format1, instance2.getTime());
        report.getResponse();
    }

    @And("^Отправить запрос для получения телематических данных с параметром Limit = (\\d+)$")
    public synchronized void sendRequestWithLimitEquals(int arg0) throws Throwable {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -2);
        instance1.add(Calendar.MINUTE, -5);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.requestWithLimit(format, format1, arg0, instance2.getTime());
        report.getResponse();
    }

    @Then("^Отправить запрос для получения телематических записей трека с Limit < (\\d+)$")
    public synchronized void sendRequestWithLimitLess(int arg0) throws Throwable {
        wait(3000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -2);
        instance1.add(Calendar.MINUTE, -5);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.requestWithLessLimit(format, format1, arg0, instance2.getTime());
        report.getResponse();
    }

    @And("^Отправить запрос для получения телематических записей трека с некорректным Limit = \"([^\"]*)\"$")
    public synchronized void sendRequestWithLimitNegative(String arg0) throws Throwable {
        wait(3000);
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        Date requestTime = Calendar.getInstance().getTime();
        from.add(Calendar.MINUTE, -2);
        to.add(Calendar.MINUTE, 1);
        String df1 = DateFormatUtils.formatUTC(from.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String df2 = DateFormatUtils.formatUTC(to.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.limitRequest(df1, df2, arg0, requestTime);
        report.getNegativeResponse();
        report.checkResponseBodyLimit(arg0);
    }
}
