package DevAPIMethods;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.time.DateFormatUtils;
import reports.TelematicParsingElementPackageCountReport;
import reports.TelematicParsingElementPackageReport;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.SimpleDateFormat;
import java.util.*;

public class TelematicParsingElementPackage {
    TelematicParsingElementPackageCountReport report = new TelematicParsingElementPackageCountReport();
    TelematicParsingElementPackageReport packageReport = new TelematicParsingElementPackageReport();

    @Step("Авторизация: ")

    public void showToken() {
        report.showToken();
    }


    @Given("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements c Start = \"([^\"]*)\", End = \"([^\"]*)\", deviceCodes = \"([^\"]*)\"$")
    public void sendRequestToGetCountOfRecordsFromCollectionTelematicParsingElementCStartEndDeviceCodes(String arg0, String arg1, String arg2) throws Throwable {
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = format2.parse(arg0);
        Date parse1 = format2.parse(arg1);
        String format = DateFormatUtils.formatUTC(parse.getTime(), "yyyy-MM-dd HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(parse1.getTime(), "yyyy-MM-dd HH:mm:ss");
        report.request(format, format1, arg2);
        report.response();
    }

    @Then("^Сравнить с числом записей в коллекции TelematicParsingElements$")
    public void checkCountOfRecordsInCollectionTelematicParsingElements() throws Throwable {
        report.checkCount();
    }

    @Given("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements c Start = \"([^\"]*)\", End = \"([^\"]*)\" для deviceCodes$")
    public void sendRequestToGetCountOfRecordsFromCollectionTelematicParsingElementCStartEndDeviceCodes(String arg0, String arg1, DataTable arg2) throws Throwable {
        showToken();
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        instance.setTimeInMillis(format2.parse(arg0).getTime());
        instance1.setTimeInMillis(format2.parse(arg1).getTime());
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        List<String> strings = arg2.asList(String.class);
        report.request(format, format1, strings);
        report.response();
    }

    @Given("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements за сегодня \\(дата start и end - сегодня\\), время start и end наступило$")
    public void sendRequestToGetCountOfRecordsFromCollectionTelematicParsingElementsStartEndTimeInPresent() throws Throwable {
        showToken();
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -5);
        instance1.add(Calendar.MINUTE, -1);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        report.request(format, format1, "510191");
        report.response();
    }

    @And("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements за сегодня \\(дата start и end - сегодня\\), время start наступило, время end в будущем$")
    public void sendRequestToGetCountOfRecordsFromCollectionTelematicParsingElementsStartEndTimeInFuture() throws Throwable {
        showToken();
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -10);
        instance1.add(Calendar.MINUTE, 60);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        report.request(format, format1, "510191");
        report.response();
    }

    @And("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements за сегодня \\(дата start и end - сегодня\\), время start и end в будущем$")
    public void sendRequestToGetCountOfRecordsFromCollectionTelematicParsingElementsStartEndTimeInFarFuture() throws Throwable {
        showToken();
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, 10);
        instance1.add(Calendar.MINUTE, 15);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        report.request(format, format1, "510191");
        report.response();
    }

    @Given("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements \\(дата start наступила, дата end - в будущем\\)$")
    public void sendRequestToGetCountReportsInTelematicParsingElementsFromInPastToInFuture() throws Throwable {
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -5);
        instance1.add(Calendar.HOUR, 24);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        report.request(format, format1, "510191");
        report.response();
    }

    @And("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements \\(дата start и дата end - в будущем\\)$")
    public void sendRequestToGetCountReportsInTelematicParsingElementsFromInFutureToInFuture() throws Throwable {
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        instance.add(Calendar.HOUR, 48);
        instance1.add(Calendar.HOUR, 72);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd HH:mm:ss.sss");
        report.request(format, format1, "510191");
        report.response();
    }

    @And("^Отправить запрос для получения данных из коллекции TelematicParsingElements c Start = \"([^\"]*)\", End = \"([^\"]*)\", deviceCodes = \"([^\"]*)\", Limit = \"([^\"]*)\"$")
    public void sendRequestToGetCountReportsInTelematicParsingElementCStartEndDeviceCodesLimit(String arg0, String arg1, String arg2, String arg3) throws Throwable {
        packageReport.request(arg0, arg1, arg2, arg3);
        packageReport.response();
    }

    @Then("^Сравнить полученные записи с TelematicParsingElements$")
    public void sendRequestToGetCountReportsInTelematicParsingElements() throws Throwable {
        switch (packageReport.getPointer()) {
            case 0:
                packageReport.mongoRequest();
                packageReport.checkCount();
                break;
            case 1:
                packageReport.mongoRequest();
                packageReport.checkCount();
                packageReport.checkRecords();
                break;
            case 2:
                packageReport.mongoRequest();
                packageReport.checkCountPack();
                packageReport.checkCount();
                packageReport.checkRecords();
                break;
        }
    }

    @When("^Отправить запрос для получения данных из коллекции TelematicParsingElements за сегодня \\(дата start и end - сегодня\\), время start и end наступило$")
    public void sendRequestToGetCountReportsInTelematicParsingElementsTodayDateStartAndEndTodayTimeStartAndEndInPresent() throws Throwable {
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        instance.add(Calendar.HOUR, -4);
        instance1.add(Calendar.HOUR, -1);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd HH:mm:ss");
        packageReport.request(format, format1, "510191", "0");
        packageReport.response();
    }

    @When("^Отправить запрос для получения данных из коллекции TelematicParsingElements за сегодня \\(дата start и end - сегодня\\), время start наступило, время end в будущем$")
    public void sendRequestToGetCountReportsInTelematicParsingElementsTodayDateStartAndEndTodayTimeStartInPresentTimeEndInFuture() throws Throwable {
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        instance.add(Calendar.HOUR, -2);
        instance1.add(Calendar.HOUR, 1);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd HH:mm:ss");
        packageReport.request(format, format1, "510191", "0");
        packageReport.response();
    }

    @When("^Отправить запрос для получения данных из коллекции TelematicParsingElements за сегодня \\(дата start и end - сегодня\\), время start и end в будущем$")
    public void sendRequestToGetCountReportsInTelematicParsingElementsTodayDateStartAndEndTodayTimeStartAndEndInFuture() throws Throwable {
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        instance.add(Calendar.HOUR, 5);
        instance1.add(Calendar.HOUR, 6);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd HH:mm:ss");
        packageReport.request(format, format1, "510191", "0");
        packageReport.response();
    }

    @And("^Отправить запрос для получения данных из коллекции TelematicParsingElements c Start = \"([^\"]*)\", End = \"([^\"]*)\", deviceCodes = \"([^\"]*)\"$")
    public void sendRequestToGetCountReportsInTelematicParsingElementCStartEndDeviceCodes(String arg0, String arg1, String arg2) throws Throwable {
        packageReport.request(arg0, arg1, arg2, "0");
        packageReport.response();
    }

    @And("^Отправить запрос для получения данных из коллекции TelematicParsingElements c параметрами$")
    public void sendRequestToGetCountReportsInTelematicParsingElementsWithParameters(DataTable dataTable) throws Throwable {
        List<List<String>> lists = dataTable.asLists(String.class);
        ArrayList<String> strings = new ArrayList<>();
        strings.add(lists.get(1).get(0));
        strings.add(lists.get(2).get(0));
        String from = lists.get(1).get(1);
        String to = lists.get(1).get(2);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(format.parse(from).getTime(), "yyyy-MM-dd HH:mm:ss");
        String format2 = DateFormatUtils.formatUTC(format.parse(to).getTime(), "yyyy-MM-dd HH:mm:ss");
        packageReport.request(format1, format2, strings);
        packageReport.response();
    }

    @Then("^Сравнить поля полученных записей с TelematicParsingElements$")
    public void sendRequestToGetCountReportsForTelematicParsingElements() throws Throwable {
        switch (packageReport.getPointer()) {
            case 0:
                packageReport.mongoRequest();
                packageReport.checkCount();
                break;
            case 1:
                packageReport.mongoRequest();
                packageReport.checkCount();
                packageReport.checkMoreRecords();
                break;
            case 2:
                packageReport.mongoRequest();
                packageReport.checkCountPack();
                packageReport.checkCount();
                packageReport.checkMoreRecords();
                break;
        }
    }

    @When("^Отправить запрос для получения данных из коллекции TelematicParsingElements \\(дата From наступила, дата To - в будущем\\)$")
    public void sendRequestToGetCountReportsInTelematicParsingElementsDateFromInPresentDateToInFuture() {
    }

    @When("^Отправить запрос для получения данных из коллекции TelematicParsingElements \\(дата From и дата To - в будущем\\)$")
    public void sendRequestToGetCountReportsInTelematicParsingElementsDateFromAndDateToInFuture() {

    }

    @Given("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements c параметрами$")
    public void sendRequestToGetCountReportsByTelematicParsingElementsWithParameters(DataTable dataTable) throws Throwable {
        List<List<String>> lists = dataTable.asLists(String.class);
        ArrayList<String> strings = new ArrayList<>();
        strings.add(lists.get(1).get(2));
        String from = lists.get(1).get(0);
        String to = lists.get(1).get(1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(format.parse(from).getTime(), "yyyy-MM-dd HH:mm:ss");
        String format2 = DateFormatUtils.formatUTC(format.parse(to).getTime(), "yyyy-MM-dd HH:mm:ss");
        report.requestWithoutToken(format1, format2, Collections.singletonList(strings.get(0)));
    }

    @Then("^Выполнение запроса недоступно$")
    public void sendRequestIsImposible() throws Throwable {
        report.ResponseWithoutToken();
    }

    @And("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements без параметра end$")
    public void sendRequestToGetCountMessagesFromTPECollectionWithoutEndParameter() {
        Calendar instance = Calendar.getInstance();
        String s = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd%20HH:mm:ss");
        report.requestEnd(s);
    }

    @Then("^Проверить, что код состояния ответа - (\\d+) Internal Server Error$")
    public void sendRequestToCheckRequestCodeEqualsBadRequest(int arg0) {
        report.response(arg0);
    }

    @And("^Отправить запрос для получения количества сообщений из коллекции TelematicParsingElements без параметра start$")
    public void sendRequestToGetCountMessagesFromTPECOllectionsWithoutStart() {
        Calendar instance = Calendar.getInstance();
        String s = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd%20HH:mm:ss");
        report.requestStart(s);
    }
}