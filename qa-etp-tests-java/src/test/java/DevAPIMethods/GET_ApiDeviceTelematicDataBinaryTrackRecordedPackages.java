package DevAPIMethods;

import com.google.protobuf.InvalidProtocolBufferException;
import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.apache.commons.lang3.time.DateFormatUtils;
import reports.ApiDeviceTelematicDataTrackRecordedPackagesReport;
import reports.TelematicDataBinaryTrackRecordedPackagesReport;

import java.text.ParseException;
import java.util.Calendar;

public class GET_ApiDeviceTelematicDataBinaryTrackRecordedPackages {
    private TelematicDataBinaryTrackRecordedPackagesReport report = new TelematicDataBinaryTrackRecordedPackagesReport();

    @And("^Отправить запрос для получения телематических записей трека в бинарном виде за сегодня \\(дата - сегодня, время - несколько минут после отправки команды о включении\\)$")
    public synchronized void sendRequestToGetTelematicRecordsInBinaryForTodayFromInPersentToInFuture() throws UnirestException, ParseException, InvalidProtocolBufferException, InterruptedException {
        wait(30000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, 1);
        instance1.add(Calendar.MINUTE, 5);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, format1, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }

    @And("^Отправить запрос для получения телематических записей трека в бинарном виде за сегодня \\(дата - сегодня, время - несколько минут до отправки команды о включении\\)$")
    public synchronized void sendRequestToGetTelematicRecordsInBinaryForTodayTimeAfterSwitchOnCommand() throws UnirestException, ParseException, InvalidProtocolBufferException, InterruptedException {
        wait(30000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -12);
        instance1.add(Calendar.MINUTE, -5);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, format1, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }

    @And("^Отправить запрос для получения телематических записей трека в бинарном виде с Limit = \"([^\"]*)\"$")
    public synchronized void sendRequestToGetTelematicRecordsInBinaryForTodayWithLimit(String arg0) throws Throwable {
        wait(30000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -1);
        instance1.add(Calendar.MINUTE, 1);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, format1, arg0, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }

    @And("^Отправить запрос для получения телематических записей трека в бинарном виде с Limit = \"([^\"]*)\", Skip = \"([^\"]*)\"$")
    public synchronized void sendRequestToGetTelematicRecordsInBinaryForTodayWithSkipLimit(String arg0, String arg1) throws Throwable {
        wait(30000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -15);
        instance1.add(Calendar.MINUTE, 1);
        String format = DateFormatUtils.format(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.format(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, format1, arg0, arg1, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }

    @Then("^Отправить запрос для получения телематических записей трека в бинарном виде с параметром Skip < (\\d+)$")
    public void sendRequestToGetTelematicRecordsInBinaryForTodayWithSkip(int arg0) throws Throwable {
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -30);
        instance1.add(Calendar.MINUTE, 1);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, format1, arg0, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }

    @Then("^Отправить запрос для получения телематических записей трека в бинарном виде c To = \"([^\"]*)\"$")
    public void sendRequestToGetTelematicRecordsInBinaryForTodayWithTo(String arg0) throws Throwable {
        Calendar instance = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -10);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, arg0, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }

    @Then("^Отправить запрос для получения телематических записей трека в бинарном виде c From = \"([^\"]*)\"$")
    public void sendRequestToGetTelematicRecordsInBinaryForTodayWithFrom(String arg0) throws Throwable {
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance1.add(Calendar.MINUTE, 1);
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(arg0, format1, instance2);
        report.getResponse();
        report.checkRecordedFields();

    }

    @Then("^Отправить запрос для получения телематических записей трека в бинарном виде при условии, что дата From > даты To$")
    public void sendRequestToGetTelematicRecordsInBinaryForTodayFromMoreThanTo() throws Throwable {
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -1);
        instance1.add(Calendar.MINUTE, -15);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, format1, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }

    @Then("^Отправить запрос для получения телематических записей трека в бинарном виде за сегодня$")
    public synchronized void sendRequestToGetTelematicRecordsInBinaryForToday() throws Throwable {
        wait(30000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, 15);
        instance1.add(Calendar.MINUTE, 20);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, format1, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }

    @And("^Отправить запрос для получения телематических записей трека в бинарном виде \\(дата From - в прошлом, дата To - в будущем\\)$")
    public synchronized void отправитьЗапросДляПолученияТелематическихЗаписейТрекаВБинарномВидеДатаFromВПрошломДатаToВБудущем() throws Throwable {
        wait(30000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MONTH, -10);
        instance1.add(Calendar.MINUTE, 10);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, format1, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }

    @And("^Отправить запрос для получения телематических записей трека в бинарном виде \\(дата From и дата To - в будущем\\)$")
    public synchronized void отправитьЗапросДляПолученияТелематическихЗаписейТрекаВБинарномВидеДатаFromИДатаToВБудущем() throws Throwable {
        wait(30000);
        Calendar instance = Calendar.getInstance();
        Calendar instance1 = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.add(Calendar.MINUTE, 10);
        instance1.add(Calendar.MINUTE, 20);
        String format = DateFormatUtils.formatUTC(instance.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        String format1 = DateFormatUtils.formatUTC(instance1.getTime(), "yyyy-MM-dd'%20'HH:mm:ss");
        report.getRequest(format, format1, instance2);
        report.getResponse();
        report.checkRecordedFields();
    }
}
