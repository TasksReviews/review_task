package customer;

import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Customer;

import cucumber.api.java.en.Then;

public class GET_Customer_UnboundAccounts_dateCreated extends AccountRegistrationAuth{
	public Customer customer;


	@Then("^получить информацию о всех аккаунтах не привязанных к клиенту$")
	public void getListofAccountsUnboundToCustomer() {
		new Customer().getAcctsUnBoundToCustomer(null, 0, 9999);
	}

	@Then("^получить информацию о \"([^\"]*)\" аккаунтах пропустив первые \"([^\"]*)\" не привязанных к клиенту, созданных позже \"([^\"]*)\"$")
	public void getListofAccountsUnboundToCustomer(int limit, int skip, String creationDate) {
			new Customer().getAcctsUnBoundToCustomer(creationDate, skip, limit);
	}
}