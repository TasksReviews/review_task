package customer;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Customer;

import cucumber.api.java.en.Then;
import io.qameta.allure.Step;

public class POST_DriveStyleSettings_Manage extends AbstractClass {

	String defaultCustSettingsStr = "{\"penaltyScores\":{\"id\":\"59bf6149b4090628234aad7f\",\"overSpeeding\":{\"isRuleUse\":true,\"overSpeedingValue\":60,"
			+"\"penaltyScores\":{\"interval1\":{\"start\":0,\"finish\":20,\"coefficientValue\":0,\"step\":1,\"unit\":\"км/ч\"},"
			+"\"interval2\":{\"start\":20,\"finish\":40,\"coefficientValue\":1.5,\"step\":1,\"unit\":\"км/ч\"},"
			+"\"interval3\":{\"start\":40,\"finish\":60,\"coefficientValue\":2,\"step\":1,\"unit\":\"км/ч\"},"
			+"\"interval4\":{\"start\":60,\"finish\":80,\"coefficientValue\":2.5,\"step\":1,\"unit\":\"км/ч\"}},\"isCoefficientsUse\":true,"
			+"\"penaltyCoefficients\":{\"interval1\":{\"start\":1,\"finish\":4,\"coefficientValue\":1,\"step\":1,\"unit\":\"сек.\"},"
			+"\"interval2\":{\"start\":4,\"finish\":13,\"coefficientValue\":1.3,\"step\":1,\"unit\":\"сек.\"},"
			+"\"interval3\":{\"start\":13,\"finish\":28,\"coefficientValue\":1.7,\"step\":1,\"unit\":\"сек.\"}},\"ignoreSpeedOver5\":true,\"ignoreSpeedOver20\":true},"
			+"\"sharpAccelerations\":{\"penaltyScores\":{\"interval1\":{\"start\":0,\"finish\":200,\"coefficientValue\":0,\"step\":50,\"unit\":\"mg.\"},"
			+"\"interval2\":{\"start\":200,\"finish\":300,\"coefficientValue\":2,\"step\":50,\"unit\":\"mg.\"},"
			+"\"interval3\":{\"start\":300,\"finish\":500,\"coefficientValue\":2.5,\"step\":50,\"unit\":\"mg.\"},"
			+"\"interval4\":{\"start\":500,\"finish\":1000,\"coefficientValue\":3,\"step\":50,\"unit\":\"mg.\"}},\"isCoefficientsUse\":true,"
			+"\"penaltyCoefficients\":{\"interval1\":{\"start\":100,\"finish\":300,\"coefficientValue\":1,\"step\":100,\"unit\":\"мс.\"},"
			+"\"interval2\":{\"start\":300,\"finish\":500,\"coefficientValue\":1.3,\"step\":100,\"unit\":\"мс.\"},"
			+"\"interval3\":{\"start\":500,\"finish\":1000,\"coefficientValue\":1.7,\"step\":100,\"unit\":\"мс.\"}}},"
			+"\"sharpBrakings\":{\"penaltyScores\":{\"interval1\":{\"start\":0,\"finish\":200,\"coefficientValue\":0,\"step\":50,\"unit\":\"mg.\"},"
			+"\"interval2\":{\"start\":200,\"finish\":400,\"coefficientValue\":2,\"step\":50,\"unit\":\"mg.\"},\"interval3\":{\"start\":400,\"finish\":600,\"coefficientValue\":2.5,\"step\":50,\"unit\":\"mg.\"},"
			+"\"interval4\":{\"start\":600,\"finish\":1000,\"coefficientValue\":3,\"step\":50,\"unit\":\"mg.\"}},\"isCoefficientsUse\":true,"
			+"\"penaltyCoefficients\":{\"interval1\":{\"start\":100,\"finish\":300,\"coefficientValue\":1,\"step\":100,\"unit\":\"мс.\"},\"interval2\":{\"start\":300,\"finish\":500,\"coefficientValue\":1.3,\"step\":100,\"unit\":\"мс.\"},"
			+"\"interval3\":{\"start\":500,\"finish\":1000,\"coefficientValue\":1.7,\"step\":100,\"unit\":\"мс.\"}}},"
			+"\"sharpTurns\":{\"penaltyScores\":{\"interval1\":{\"start\":0,\"finish\":200,\"coefficientValue\":0,\"step\":50,\"unit\":\"mg.\"},\"interval2\":{\"start\":200,\"finish\":400,\"coefficientValue\":2,\"step\":50,\"unit\":\"mg.\"},"
			+"\"interval3\":{\"start\":400,\"finish\":500,\"coefficientValue\":2.5,\"step\":50,\"unit\":\"mg.\"},\"interval4\":{\"start\":500,\"finish\":1000,\"coefficientValue\":3,\"step\":50,\"unit\":\"mg.\"}},\"isCoefficientsUse\":true,"
			+"\"penaltyCoefficients\":{\"interval1\":{\"start\":100,\"finish\":300,\"coefficientValue\":1,\"step\":100,\"unit\":\"мс.\"},\"interval2\":{\"start\":300,\"finish\":500,\"coefficientValue\":1.3,\"step\":100,\"unit\":\"мс.\"},"
			+"\"interval3\":{\"start\":500,\"finish\":1000,\"coefficientValue\":1.7,\"step\":100,\"unit\":\"мс.\"}}}},\"coefficients\":{\"id\":\"59bf5facb4090628234aad6c\","
			+"\"speed\":{\"penaltyCoefficients\":{\"interval1\":{\"start\":5,\"finish\":60,\"coefficientValue\":1,\"step\":5,\"unit\":\"км/ч\"},\"interval2\":{\"start\":60,\"finish\":100,\"coefficientValue\":1.3,\"step\":5,\"unit\":\"км/ч\"},"
			+"\"interval3\":{\"start\":100,\"finish\":305,\"coefficientValue\":1.7,\"step\":5,\"unit\":\"км/ч\"}}},"
			+"\"drivingExperience\":{\"penaltyCoefficients\":{\"interval1\":{\"start\":0,\"finish\":3,\"coefficientValue\":1.3,\"step\":1,\"unit\":\"лет\"},\"interval2\":{\"start\":3,\"finish\":5,\"coefficientValue\":1,\"step\":1,\"unit\":\"лет\"},"
			+"\"interval3\":{\"start\":5,\"finish\":10,\"coefficientValue\":1.5,\"step\":1,\"unit\":\"лет\"},\"interval4\":{\"start\":10,\"finish\":30,\"coefficientValue\":1.3,\"step\":1,\"unit\":\"лет\"}}},"
			+"\"accidentsCounts\":{\"penaltyCoefficients\":{\"interval1\":{\"start\":0,\"finish\":1,\"coefficientValue\":1,\"step\":1,\"unit\":\"\"},\"interval2\":{\"start\":1,\"finish\":2,\"coefficientValue\":1.3,\"step\":1,\"unit\":\"\"},"
			+"\"interval3\":{\"start\":2,\"finish\":5,\"coefficientValue\":1.5,\"step\":1,\"unit\":\"\"},\"interval4\":{\"start\":5,\"finish\":10,\"coefficientValue\":1.7,\"step\":1,\"unit\":\"\"}}},"
			+"\"roadSigns\":{\"title\":\"RoadSigns\",\"roadSignsCoefficients\":[{\"name\":\"Камера контроля скорости\",\"coefficientValue\":1},{\"name\":\"Аварийно-опасный перекресток\",\"coefficientValue\":1},"
			+"{\"name\":\"Железнодорожный переезд\",\"coefficientValue\":1},{\"name\":\"Школьная зона\",\"coefficientValue\":1},{\"name\":\"Лежачий полицейский\",\"coefficientValue\":1},{\"name\":\"Пост ДПС\",\"coefficientValue\":1},"
			+"{\"name\":\"Опасный поворот\",\"coefficientValue\":1},{\"name\":\"Опасный участок дороги\",\"coefficientValue\":1},{\"name\":\"Пешеходный переход\",\"coefficientValue\":1},{\"name\":\"Дикие животные\",\"coefficientValue\":1},"
			+"{\"name\":\"Камера фотофиксации\",\"coefficientValue\":1},{\"name\":\"Муляж камеры фотофиксации\",\"coefficientValue\":1}]},"
			+"\"tripTime\":{\"title\":\"TripTime\",\"nightTime\":{\"description\":\"23:00-06:00\",\"name\":\"Ночное время\",\"title\":\"NightTime\",\"coefficientValue\":1},"
			+"\"dayTime\":{\"description\":\"06:00-23:00\",\"name\":\"Дневное время\",\"title\":\"DayTime\",\"coefficientValue\":1},\"peakHour\":{\"description\":\"07:00-11:00\",\"name\":\"Час пик\",\"title\":\"PeakHour\",\"coefficientValue\":1},"
			+"\"weekdays\":{\"description\":\"\",\"name\":\"Будни\",\"title\":\"Weekdays\",\"coefficientValue\":1},\"weekend\":{\"description\":\"\",\"name\":\"Выходные\",\"title\":\"Weekend\",\"coefficientValue\":1},"
			+"\"holidays\":{\"description\":\"\",\"name\":\"Праздники\",\"title\":\"Holidays\",\"coefficientValue\":1}}},"
			+"\"vehiclePenalties\":{\"id\":\"59bf6298b4090628234aad92\",\"carPenalty\":{\"carCoefficients\":[{\"category\":\"A\",\"name\":\"Особо малый класс\",\"coefficientValue\":1},{\"category\":\"B\",\"name\":\"Малый класс\",\"coefficientValue\":1},"
			+"{\"category\":\"C\",\"name\":\"Малый средний класс\",\"coefficientValue\":1},{\"category\":\"D\",\"name\":\"Средний класс\",\"coefficientValue\":1},{\"category\":\"E\",\"name\":\"Высший средний класс\",\"coefficientValue\":1},"
			+"{\"category\":\"F\",\"name\":\"Высший класс\",\"coefficientValue\":1},{\"category\":\"S\",\"name\":\"Спорткарты, купе и кабриолеты\",\"coefficientValue\":1},"
			+"{\"category\":\"M\",\"name\":\"Минивэны, универсалы повышенной вместимости\",\"coefficientValue\":1},{\"category\":\"J\",\"name\":\"Автомобили повышенной проходимости\",\"coefficientValue\":1}]},"
			+"\"busPenalty\":{\"busCoefficients\":[{\"category\":\"\",\"name\":\"Малые\",\"coefficientValue\":1},{\"category\":\"\",\"name\":\"Средние\",\"coefficientValue\":1},{\"category\":\"\",\"name\":\"Большие\",\"coefficientValue\":1},"
			+"{\"category\":\"\",\"name\":\"Особо большие\",\"coefficientValue\":1}]},"
			+"\"truckPenalty\":{\"truckCoefficients\":[{\"category\":\"A2\",\"name\":\"Грузовики\",\"coefficientValue\":1},{\"category\":\"B2\",\"name\":\"Грузовики\",\"coefficientValue\":1},"
			+"{\"category\":\"C2\",\"name\":\"Грузовики\",\"coefficientValue\":1},{\"category\":\"D2\",\"name\":\"Опасные грузы\",\"coefficientValue\":1}]}}}";
	
	@Then("^Обновить настройки анализа стиля вождения: \"penaltyScores.overSpeeding.isRuleUse\" = <\"([^\"]*)\">$")
	public void обновить_настройки_анализа_стиля_вождения_true(Boolean isRuleUse) {

		JSONParser parser = new JSONParser();
		try {
			JSONObject defaultCustSettings = (JSONObject) parser.parse(defaultCustSettingsStr);
			JSONObject penaltyScores = (JSONObject) defaultCustSettings.get("penaltyScores");
			JSONObject overSpeeding = (JSONObject) penaltyScores.get("overSpeeding");
			overSpeeding.put("isRuleUse", isRuleUse);
			penaltyScores.put("overSpeeding", overSpeeding);
			defaultCustSettings.put("penaltyScores", penaltyScores);
			
			Customer customer = new Customer();
			String accountId = ((JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0)).get("id").toString();
			String customerId = getAuthorizedUserCustomer(accountId);
			
				customer.updateCustomerSettings(customerId,defaultCustSettings);
			
		} catch (ParseException e) {
			
			log.error(ERROR,e);
		}
		
//		String accountId = ((JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0)).get("id").toString();
//		String customer = getAuthorizedUserCustomer(accountId);
		
//		Bson filter = Filters.and(eq("CustomerId",new ObjectId(customer) ) ); 
//		HashMap<String,Object> overSpeedingRuleUse = new HashMap<String,Object>();
//		overSpeedingRuleUse.put( "OverSpeeding.IsRuleUse",isRuleUse);
//		com.t1.core.mongoDB.MongoConnection.updateCollection("DriveStylePenaltyScores",filter,overSpeedingRuleUse);
	}
	

	@Then("^Обновить настройки анализа стиля вождения: \"penaltyScores.overSpeeding.isRuleUse\" = <\"([^\"]*)\">, \"penaltyScores.overSpeeding.overSpeedingValue\" = <(\\d+)>$")
	public void обновить_настройки_анализа_стиля_вождения(Boolean isRuleUse, int  overSpeedingValue)  {
		JSONParser parser = new JSONParser();
		try {
			JSONObject defaultCustSettings = (JSONObject) parser.parse(defaultCustSettingsStr);
			JSONObject penaltyScores = (JSONObject) defaultCustSettings.get("penaltyScores");
			JSONObject overSpeeding = (JSONObject) penaltyScores.get("overSpeeding");
			overSpeeding.put("isRuleUse", isRuleUse);
			overSpeeding.put("overSpeedingValue", overSpeedingValue);
			penaltyScores.put("overSpeeding", overSpeeding);
			defaultCustSettings.put("penaltyScores", penaltyScores);
			
			Customer customer = new Customer();
			String accountId = ((JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0)).get("id").toString();
			String customerId = getAuthorizedUserCustomer(accountId);
			
				customer.updateCustomerSettings(customerId,defaultCustSettings);
			
		} catch (ParseException e) {
			
			log.error(ERROR,e);
		}
	}
	
	@Step("изменение настроек аккаунта {1}")
	public void changeCustomerDefaultSettings (String customerId, JSONObject settings){
		Customer customerFromPrevStep = (Customer) SharedContainer.getLastObjectFromSharedContainer("customers");
	//	customerFromPrevStep.updateCustomerSettings(customerId,settings);
	}
}
