package customer;

import org.json.simple.JSONObject;

import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Customer;

import cucumber.api.java.en.Then;

public class GET_Customer_Current_CustomerId extends AccountRegistrationAuth{


	@Then("^получить информацию о используемом клиенте$")
	public void getCrrentCustomerInfo() {
		String accountId = ((JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0)).get("id").toString();
		Customer customer = new Customer();
		customer.setInitMap(null);
		customer.setCustomerId(getAuthorizedUserCustomer(accountId));
		customer.getCurrentCustomerInfo(null);
	}

	@Then("^получить информацию о используемом клиенте по id$")
	public void getCrrentCustomerInfoById(){
		String accountId = ((JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0)).get("id").toString();
		Customer customer = new Customer();
		customer.setInitMap(null);
		customer.setCustomerId(getAuthorizedUserCustomer(accountId));
		customer.getCurrentCustomerInfo(customer.getCustomerId());
	}


}