package customer;

import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Customer;

import cucumber.api.java.en.Then;

public class POST_CustomerDelete extends AccountRegistrationAuth{
	public Customer customer;


	@Then("^Удалить клиента созданного в предыдущем шаге$")
	public void addAccountRelationToCustomer(){
		Customer customerFromPrevStep = (Customer) SharedContainer.getLastObjectFromSharedContainer("customers");
		customerFromPrevStep.deleteCustomer(customerFromPrevStep.getCustomerId());
	}




}