package customer;

import java.util.ArrayList;

import com.sun.jna.platform.win32.Advapi32Util;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Customer;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;

public class POST_CustomerCreate extends AccountRegistrationAuth {

    public Customer customer;


    @Then("^создать нового клиента$")
    public void addAccountRelationToCustomer() {
        //	account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
        customer = new Customer();
        customer.customerCreate();
        SharedContainer.setContainer("customers", (new ArrayList() {{
            add(customer);
        }}));
    }
}