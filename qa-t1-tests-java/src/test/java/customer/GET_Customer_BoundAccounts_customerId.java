package customer;

import org.json.simple.JSONObject;

import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Customer;
import com.t1.core.mongodb.GetInfoFromMongo;

import cucumber.api.java.en.Then;

public class GET_Customer_BoundAccounts_customerId extends AccountRegistrationAuth {
    public Customer customer;


    @Then("^получить информацию о всех аккаунтах привязанных к тестовому клиенту$")
    public void getListofAccountsboundToCustomer() {
        JSONObject accountFromPrevStep = (JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0);
        new Customer().getAcctsBoundToCustomer(getAuthorizedUserCustomer(accountFromPrevStep.get("id").toString()), 0, 9999);

    }

    @Then("^получить информацию о \"([^\"]*)\" аккаунтах пропустив первые \"([^\"]*)\" привязанных к случайному клиенту$")
    public void getListofAccountsboundToCustomer(int limit, int skip) {

        JSONObject randomCustomer = (JSONObject) GetInfoFromMongo.getRandomDocumentFromCollection("Customers", null, "").get(0);
        //JSONObject accountFromPrevStep = (JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0);
        try {
            new Customer().getAcctsBoundToCustomer(ReplyToJSON.extractPathFromJson(randomCustomer, "$._id.$oid"), skip, limit);
        } catch (Exception e) {
            log.error(ERROR, e);
        }
    }
}