package customer;

import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Customer;

import cucumber.api.java.en.Then;

public class POST_CustomerUpdate extends AccountRegistrationAuth{
	public Customer customer;


	@Then("^Изменить клиента созданного в предыдущем шаге$")
	public void addAccountRelationToCustomer(){
		Customer customerFromPrevStep = (Customer) SharedContainer.getLastObjectFromSharedContainer("customers");
		customer = new Customer();
		customer.setCustomerId(customerFromPrevStep.getCustomerId());
		customer.updateCustomer();
	}




}