package customer;

import java.util.List;

import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Customer;

import cucumber.api.java.en.Then;

public class POST_CustomerRemoveAccount extends AccountRegistrationAuth{
	public AccountRegistrationAuth account;
	public Customer customer;

	@Then("^Удалить связь тестового клиента с созданным аккаунтом$")
	public void removeAccountRelationToTestCustomer(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		Customer.removeAccountRelationToCustomer(account.getAccountId(),getProperty("testCustomerId"));
	}

	@Then("^Удалить связи тестового клиента с созданными аккаунтами$")
	public void removeAccountsRelationToTestCustomer() {
		List accounts = (List<AccountRegistrationAuth>) SharedContainer.getFromContainer("accounts");
		for (Object obj : accounts) {
			account = (AccountRegistrationAuth) obj;
			Customer.removeAccountRelationToCustomer(account.getAccountId(), getProperty("testCustomerId"));
		}
	}

	@Then("^Удалить связь между созданным клиентом и созданным аккаунтом$")
	public void removeAccountRelationToCreatedCustomer(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		customer = (Customer) SharedContainer.getFromContainer("customers").get(0);
		customer.removeAccountRelationToCustomer(account.getAccountId(),customer.getCustomerId());
	}

}