package customer;

import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Customer;

import cucumber.api.java.en.Then;

public class GET_Customer_id extends AccountRegistrationAuth{
	public Customer customer;


	@Then("^получить информацию о клиенте созданном в предыдущем шаге$")
	public void getCustomerInfo(){
		Customer customerFromPrevStep = (Customer) SharedContainer.getLastObjectFromSharedContainer("customers");
		customerFromPrevStep.getCustomerInfoById(customerFromPrevStep.getCustomerId());
	}




}