package customer;

import java.util.List;

import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Customer;

import cucumber.api.java.en.Then;

public class POST_CustomerAddAccount extends AccountRegistrationAuth{
	public AccountRegistrationAuth account;
	public Customer customer;

	@Then("^привязать созданный аккаунт к тестовому клиенту$")
	public void addAccountRelationToTestCustomer(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		Customer.addAccountRelationToCustomer(account.getAccountId(),getProperty("testCustomerId"));
	}

	@Then("^привязать созданные аккаунты к тестовому клиенту$")
	public void addAccountsRelationToTestCustomer() {
		List accounts = (List<AccountRegistrationAuth>) SharedContainer.getFromContainer("accounts");
		for (Object obj : accounts) {
			account = (AccountRegistrationAuth) obj;
			Customer.addAccountRelationToCustomer(account.getAccountId(), getProperty("testCustomerId"));
		}
	}

	@Then("^привязать созданный аккаунт к созданному клиенту$")
	public void addAccountRelationToCreatedCustomer(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		customer = (Customer) SharedContainer.getFromContainer("customers").get(0);
		customer.addAccountRelationToCustomer(account.getAccountId(),customer.getCustomerId());
	}

}