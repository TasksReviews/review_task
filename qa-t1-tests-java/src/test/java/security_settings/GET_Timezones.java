package security_settings;

import java.util.List;

import org.json.simple.JSONObject;

import com.jayway.jsonpath.JsonPath;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountInfoAndSettings;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.GetAccountInfoFromMongo;

import cucumber.api.java.en.Then;

public class GET_Timezones extends AccountRegistrationAuth{
	
	@Then("^Запросить список всех часовых поясов$")
	public static void getTimeZones(){
		String token=null;
		log.debug("Start>> GET_Timezones get list of timezones");
		try {
			if ((SharedContainer.getContainers().containsKey("demoToken"))  && SharedContainer.getFromContainer("demoToken").get(0) != "") {
				token = getToken("demoToken");
			} else {
				token = getToken("userToken");
			}
			
			assertTrue("token received:",token != null,"для запроса часовых поясов необходимо пройти авторизацию");
			
		List<JSONObject> timeZones = (List<JSONObject>) new AccountInfoAndSettings().getAllTimeZones();
		log.debug("end of get all timeZones");
		} catch (Exception e) {	
			log.error("Test failed: " + e);
			assertTrue(false, e.toString());
		}
	}

public static String getAccountTimezone(){
	
	
	JSONObject accFromPrevStep = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("authorizedAccount_t1client");
	String accIdFromPrevStep = accFromPrevStep.get("id").toString();
	List<org.json.simple.JSONObject> mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(accIdFromPrevStep,"");
	String accountStrTimeZone = JsonPath.read(mongoAccountInfo.get(0), "$.Settings.SystemSettings.Timezone");
	String timezone = getUTCTimezoneFromString(accountStrTimeZone).get("timeZone");
	
	return timezone;
}

}