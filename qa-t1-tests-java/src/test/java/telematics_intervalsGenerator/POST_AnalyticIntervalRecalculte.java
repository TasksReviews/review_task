package telematics_intervalsGenerator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.mongodb.GetDevices;

import cucumber.api.java.en.Then;

public class POST_AnalyticIntervalRecalculte extends AbstractClass{
	public POST_AnalyticIntervalRecalculte intervalGeneratorObj;

	public static String url;
	public static JSONObject urlParameters = new JSONObject();

		//TODO: complete these steps. recalculate call should be moved to separate class probably related to  interval generator
	@Then("^вызвать пересчет аналитических интервалов для созданного ТМУ c периодом \"([^\"]*)\"$")
	public void getAnalyticIntervalbeRecalculted(String datePeriod) {
		try {
		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		
		 
		 Map<String, String> dateTimePeriod = getDateTimePeriodInUTC(datePeriod);
		String token = getAnyToken();
		String host = getProperty("analyticIntervalGeneratorHost");
		this.url = host + "/AnalyticInterval/Recalculate";


		this.urlParameters.clear();

		this.urlParameters.put("accountLogin", "demo");
		this.urlParameters.put("periodStart",dateTimePeriod.get("dateTimeFrom") );
		this.urlParameters.put("periodEnd",dateTimePeriod.get("dateTimeTo") );
		List deviceCodes = new ArrayList();
		deviceCodes.add(telematics_device.getTrackerCode());
		this.urlParameters.put("deviceCodes", deviceCodes);
//			log.debug("params: "+jsonParamsToString(urlParameters,true));
			JSONObject analyticIntervalReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters,true), token,"application/json",true);
//			log.debug("analyticIntervalReply=" + StringEscapeUtils.unescapeJava(analyticIntervalReply));
			
		} catch (Exception e){
			log.error(ERROR,e);
		}
	}
	
	@Then("^вызвать генератор аналитических интервалов для ТМУ \"([^\"]*)\" за период с \"([^\"]*)\" по \"([^\"]*)\"$")
	public void getAnalyticIntervalbeRecalculted(String deviceId, String dateFrom, String dateTill) {
		try {
//		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		
			Map<String, String> dateTimePeriod = getDateTimePeriod(dateFrom, dateTill);
		
		
		JSONObject deviceMongo = (JSONObject)GetDevices.getLastDeviceById(deviceId,"").get(0);
		
		
	//	JSONObject jsonObj =  ReadFile.readJsonFile(path);
		String token = getAnyToken();
		String host = getProperty("analyticIntervalGeneratorHost");
		this.url = host + "/AnalyticInterval/Recalculate";


		this.urlParameters.clear();

		this.urlParameters.put("accountLogin", "demo");//dba" );
//		this.urlParameters.put("accountLogin", "dba" );
		this.urlParameters.put("periodStart", dateTimePeriod.get("dateTimeFrom") );
		this.urlParameters.put("periodEnd",dateTimePeriod.get("dateTimeTo") );
		List deviceCodes = new ArrayList();
		deviceCodes.add(deviceMongo.get("Code"));
		this.urlParameters.put("deviceCodes",deviceCodes);
			log.debug("params: "+jsonParamsToString(urlParameters,true));
			JSONObject analyticIntervalReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters,true), token,"application/json",true);
			log.debug("analyticIntervalReply=" + StringEscapeUtils.unescapeJson(analyticIntervalReply.toString()));
			setToAllureChechkedFields();
		} catch (Exception e){
			log.error(ERROR,e);
			setToAllureChechkedFields();
		}
	}
	
	
	@Then("^Пересчитать аналитические интервалы с \"periodStart\": \"([^\"]*)\" по \"periodEnd\": \"([^\"]*)\"$")
	public void getAnalyticIntervalbeRecalculted(String dateFrom, String dateTill) {
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		Map<String, String> dateTimePeriod = getDateTimePeriod(dateFrom, dateTill);
		
		getAnalyticIntervalbeRecalculted(telematics_device.getTrackerId(), dateTimePeriod.get("dateTimeFrom"), dateTimePeriod.get("dateTimeTo"));
		
	}
	
	
	@Then("^Пересчитать интервалы для созданного ТМУ за период с даты привязки к объекту по текущий день$")
	public void getAnalyticIntervalbeRecalcultedSinceDeviceBounded() {

		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");

		 String currTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(new Date());
		 String devboundTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(new Date(telematics_device.getBoundTime()));
			try {
				
				String token = getCurAuthAccToken("authorizedAccount_t1client");
				String host = getProperty("analyticIntervalGeneratorHost");
				this.url = host + "/AnalyticInterval/Recalculate";


				this.urlParameters.clear();

				this.urlParameters.put("accountLogin", "demo");//dba" );
//				this.urlParameters.put("accountLogin", "dba" );
				this.urlParameters.put("periodStart", devboundTimeStamp );
				this.urlParameters.put("periodEnd",currTimeStamp );
				List deviceCodes = new ArrayList();
				deviceCodes.add(telematics_device.getTrackerCode());
				this.urlParameters.put("deviceCodes",deviceCodes);
					log.debug("params: "+jsonParamsToString(urlParameters,true));
					JSONObject analyticIntervalReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters,true), token,"application/json",true);
					log.debug("analyticIntervalReply=" + StringEscapeUtils.unescapeJson(analyticIntervalReply.toString()));
					setToAllureChechkedFields();
				} catch (Exception e){
					log.error(ERROR,e);
					setToAllureChechkedFields();
				}
	}
	
	@Then("^Пересчитать интервалы для созданного ТМУ за период с <(\\d+), (\\d+)> по <(\\d+), (\\d+)>$")
	public void getAnalyticIntervalbeRecalcultedSinceDeviceBounded_Step(int fromDaysSubst,int fromHoursAdd,int tillDaysSubst,int tillHoursAdd) {
		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		getAnalyticIntervalbeRecalcultedSinceDeviceBounded(telematics_device, fromDaysSubst, fromHoursAdd, tillDaysSubst, tillHoursAdd);
	}
	
	@Then("^Пересчитать интервалы для созданного ТМУ (\\d+) за период с <(\\d+), (\\d+)> по <(\\d+), (\\d+)>$")
	public void getAnalyticIntervalbeRecalcultedSinceDeviceBounded_Step(int deviceIndex, int fromDaysSubst,int fromHoursAdd,int tillDaysSubst,int tillHoursAdd) {
		
		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers").get(deviceIndex-1);
		getAnalyticIntervalbeRecalcultedSinceDeviceBounded(telematics_device, fromDaysSubst, fromHoursAdd, tillDaysSubst, tillHoursAdd);
	}
	
	public void getAnalyticIntervalbeRecalcultedSinceDeviceBounded(TelematicVirtualTracker telematics_device, int fromDaysSubst,int fromHoursAdd,int tillDaysSubst,int tillHoursAdd) {

		
		 Long currTimeInMs = new Date().getTime();
		 long inMs1hr =  60*60 * 1000L;
		 long inMs1day =  24*60*60 * 1000L;
		 long todayMidnight = currTimeInMs -(currTimeInMs % inMs1day)-(3*inMs1hr);
		 Long unbound = todayMidnight -(inMs1day*tillDaysSubst)+(inMs1hr*tillHoursAdd);
		 String periodEndStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(unbound);
		 Long boundDate = todayMidnight - (inMs1day*fromDaysSubst)+(inMs1hr*fromHoursAdd);
		 String periodStartStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(boundDate);
			try {
				
				String token = getCurAuthAccToken("authorizedAccount_t1client");
				String host = getProperty("analyticIntervalGeneratorHost");
				this.url = host + "/AnalyticInterval/Recalculate";


				this.urlParameters.clear();

				this.urlParameters.put("accountLogin", "demo");//dba" );
//				this.urlParameters.put("accountLogin", "dba" );
				this.urlParameters.put("periodStart", periodStartStr );
				this.urlParameters.put("periodEnd",periodEndStr );
				List deviceCodes = new ArrayList();
				deviceCodes.add(telematics_device.getTrackerCode());
				this.urlParameters.put("deviceCodes",deviceCodes);
					log.debug("params: "+jsonParamsToString(urlParameters,true));
					JSONObject analyticIntervalReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters,true), token,"application/json",true);
					log.debug("analyticIntervalReply=" + StringEscapeUtils.unescapeJson(analyticIntervalReply.toString()));
					setToAllureChechkedFields();
				} catch (Exception e){
					log.error(ERROR,e);
					setToAllureChechkedFields();
				}
	}
}