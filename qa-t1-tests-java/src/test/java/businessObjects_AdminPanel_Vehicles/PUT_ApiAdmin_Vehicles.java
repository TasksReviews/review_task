package businessObjects_AdminPanel_Vehicles;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Customer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class PUT_ApiAdmin_Vehicles extends AbstractClass {

	@Given("^В Админке привязать ТМУ к ТС$")
	public void addDeviceToVehicle(){
		
		
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers").get(0);
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
				
		Customer customerFromPrevStep = getCustomer();

//			Employee newEmpl = new Employee(false);
		vehicleFromPrevStep.setDeviceId(telematics_device.getTrackerId());
		JSONObject deviceObj =new JSONObject();
		deviceObj.put("id",telematics_device.getTrackerId());
		vehicleFromPrevStep.putUrlParameters("device", deviceObj);
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
		SharedContainer.setContainer("vehicles", vehicleFromPrevStep);
		
		telematics_device.setResourceId(vehicleFromPrevStep.getVehicleId());
		telematics_device.setResourceType("Vehicle");
		telematics_device.updateInSharedContainer();

	}
	
	@Given("^В Админке сменить ТС ТМУ$")
	public void changeDeviceToVehicle(){
		List<TelematicVirtualTracker> telematics_deviceList = (List<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		TelematicVirtualTracker telematics_device = null;
		Customer customerFromPrevStep = getCustomer();
		 for(TelematicVirtualTracker device : telematics_deviceList){
			 if (device.getResourceId()!=null){
				 if(! vehicleFromPrevStep.getDeviceId().equals(device.getTrackerId()) && device.getResourceId().equals(vehicleFromPrevStep.getVehicleId()) ){
					 telematics_device = device;
					 break;
				 }
			 } else {
				 if (! vehicleFromPrevStep.getDeviceId().equals(device.getTrackerId())){
					 telematics_device = device;
					 break;
				 }
			 }
//				 if((device.resourceId!=null || ! vehicleFromPrevStep.deviceId.equals(device.trackerId))|| 
//					(device.resourceId!=null || ! device.resourceId.equals(vehicleFromPrevStep.vehicleId) )){
//					 telematics_device = device;
//					 break;
//				 }
			 }
		 
		vehicleFromPrevStep.setDeviceId(telematics_device.getTrackerId());
		JSONObject deviceObj =new JSONObject();
		deviceObj.put("id",telematics_device.getTrackerId());
		vehicleFromPrevStep.putUrlParameters("device", deviceObj);
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
		SharedContainer.setContainer("vehicles", vehicleFromPrevStep);

	}

	@Given("^В Админке отвязать ТМУ от ТС$")
	public void removeDeviceRelationToVehicle(){
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
				
		Customer customerFromPrevStep = getCustomer();
		String oldDeviceId = vehicleFromPrevStep.getDeviceId();
		JSONObject customer = new JSONObject();
		customer.put("id", vehicleFromPrevStep.getCustomerId());
		vehicleFromPrevStep.putUrlParameters("customer", customer);
		vehicleFromPrevStep.setDeviceId(null);
		JSONObject device = new JSONObject();
		device.put("id", null);
		vehicleFromPrevStep.putUrlParameters("device", device);
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
		vehicleFromPrevStep.updateInSharedContainer();
		
		TelematicVirtualTracker oldTelematicsDevice = TelematicVirtualTracker.getFromSharedContainer(oldDeviceId);
		oldTelematicsDevice.setResourceId(null);
		oldTelematicsDevice.setResourceType(null);
		oldTelematicsDevice.updateInSharedContainer();
	}
	
	@Then("^В Админке отвязать ТМУ (\\d+) от ТС (\\d+)$")
	public void removeDeviceRelationToVehicle(int deviceIndex, int vehIndex){
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(vehIndex-1);
				
		Customer customerFromPrevStep = getCustomer();
		String oldDeviceId = vehicleFromPrevStep.getDeviceId();
		JSONObject customer = new JSONObject();
		customer.put("id", vehicleFromPrevStep.getCustomerId());
		vehicleFromPrevStep.putUrlParameters("customer", customer);
		vehicleFromPrevStep.setDeviceId(null);
		JSONObject device = new JSONObject();
		device.put("id", null);
		vehicleFromPrevStep.putUrlParameters("device", device);
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
		vehicleFromPrevStep.updateInSharedContainer();
		
		TelematicVirtualTracker oldTelematicsDevice = TelematicVirtualTracker.getFromSharedContainer(oldDeviceId);
		oldTelematicsDevice.setResourceId(null);
		oldTelematicsDevice.setResourceType(null);
		oldTelematicsDevice.updateInSharedContainer();
	}
	
	@Given("^В Админке привязать Сотрудника к ТС$")
	public void addEmployeeToVehicle(){
		
//		List customerFromPrevStepLst = (List) SharedContainer.getObjectsFromSharedContainer("all","customers");
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
				
		Customer customerFromPrevStep = getCustomer();
		
		vehicleFromPrevStep.setEmployeeId(employeeFromPrevStep.getEmployeeId());
		JSONObject emlpoyeeObj =new JSONObject();
		emlpoyeeObj.put("id",employeeFromPrevStep.getEmployeeId());
		vehicleFromPrevStep.putUrlParameters("employee", emlpoyeeObj);
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
		SharedContainer.setContainer("vehicles", vehicleFromPrevStep);

		employeeFromPrevStep.setVehicleId(vehicleFromPrevStep.getVehicleId());
		employeeFromPrevStep.updateInSharedContainer();
	}
	
	@Given("^В Админке привязать Сотрудника (\\d) к ТС$")
	public void addEmployee_X_ToVehicle(int emplInedx){
		Employee employeeFromPrevStep = (Employee) SharedContainer.getObjectsFromSharedContainer("all","employees").get(emplInedx-1);
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		Customer customerFromPrevStep = getCustomer();
		
		vehicleFromPrevStep.setEmployeeId(employeeFromPrevStep.getEmployeeId());
		JSONObject emlpoyeeObj =new JSONObject();
		emlpoyeeObj.put("id",employeeFromPrevStep.getEmployeeId());
		vehicleFromPrevStep.putUrlParameters("employee", emlpoyeeObj);
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
		SharedContainer.setContainer("vehicles", vehicleFromPrevStep);

		employeeFromPrevStep.setVehicleId(vehicleFromPrevStep.getVehicleId());
		employeeFromPrevStep.updateInSharedContainer();
	}
	 
	 @Given("^В Админке отвязать Сотрудника от ТС$")
		public void removeEmployeeInVehicle(){
			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		
			Customer customerFromPrevStep = getCustomer();
			String oldEmployeeId = vehicleFromPrevStep.getEmployeeId();
			vehicleFromPrevStep.setEmployeeId(null);
			JSONObject emlpoyeeObj =new JSONObject();
			emlpoyeeObj.put("employeeId",null);
			vehicleFromPrevStep.putUrlParameters("employee", emlpoyeeObj);
			vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
			vehicleFromPrevStep.updateInSharedContainer();

			Employee oldEmployee = Employee.getFromSharedContainer(oldEmployeeId);
			oldEmployee.setVehicleId(null);
			oldEmployee.updateInSharedContainer();
		}
	//
//	@Given("^В Админке отвязать ТМУ от ТС$")
//	public void removeDevicerelationInAdminPanel(){
//		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
//	}
	
		@Then("^В Админке привязать ТС к группе$")
		public void addEmplToGroup() {
			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
			Customer customerFromPrevStep = getCustomer();

			JSONObject customer = new JSONObject();
			customer.put("id", vehicleFromPrevStep.getCustomerId());

			vehicleFromPrevStep.setGroupId(group.getGroupId());
			vehicleFromPrevStep.setGroupName(group.getName());
			JSONObject groupInfo = new JSONObject();
			groupInfo.put("id", group.getGroupId());
			groupInfo.put("name", group.getName());
			
			vehicleFromPrevStep.getUrlParameters().clear();
			vehicleFromPrevStep.putUrlParameters("customer", customer);
			vehicleFromPrevStep.putUrlParameters("group", groupInfo);

			JSONObject updadteEmployeeReply = (JSONObject) vehicleFromPrevStep.updateVehiclesInAdminPanelResources().get(0);
			SharedContainer.addToUsedObjects("employees",vehicleFromPrevStep.getEmployeeId());
			vehicleFromPrevStep.updateInSharedContainer();
			
			group.addObjectToGroup(vehicleFromPrevStep.getVehicleId(),"Vehicle");
			group.updateInSharedContainer();
		}
		

		@Then("^В Админке сменить для ТС группу$")
		public void updateVehGroupChange() {
			log.debug("Start vehicle update: change group");
			 Group groupFromPrevStep = null;
			 Vehicle vehicleFromPrevStep = null;
			 List<Vehicle> vehicleList = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all","vehicles");
			 List<Group> groupList = (List<Group>) SharedContainer.getObjectsFromSharedContainer("all","groups");
			 for(Vehicle vehicle : vehicleList){
					// emplGroup=employee.groupId;
					 if(vehicle.getGroupId()!=null && ! vehicle.getGroupId().equals("000000000000000000000000")){
						 vehicleFromPrevStep = vehicle;
						 break;
					 }
				 }
				 for(Group group : groupList){
					if(!group.getGroupId().equals(vehicleFromPrevStep.getGroupId())){
						groupFromPrevStep = group;
						break;
					}
				 }

			String vehGroupIdBeforeReq = vehicleFromPrevStep.getGroupId();
					
			JSONObject customer = new JSONObject();
			customer.put("id", vehicleFromPrevStep.getCustomerId());
			vehicleFromPrevStep.setGroupId(groupFromPrevStep.getGroupId());
			JSONObject group = new JSONObject();
			group.put("name", groupFromPrevStep.getName());
			group.put("id", groupFromPrevStep.getGroupId());
			vehicleFromPrevStep.getUrlParameters().clear();
			vehicleFromPrevStep.putUrlParameters("customer", customer);

			vehicleFromPrevStep.putUrlParameters("group", group);
			vehicleFromPrevStep.updateVehiclesInAdminPanelResources();

			groupFromPrevStep.addObjectToGroup(vehicleFromPrevStep.getVehicleId(),"Vehicle");
			groupFromPrevStep.updateInSharedContainer();
			// remove the object from old group			
			Group.getFromSharedContainer(vehGroupIdBeforeReq).removeObjectInGroup(vehicleFromPrevStep.getVehicleId());
			
			log.debug("End vehicle update: change group");
		}	 
		
	@Then("^В Админке выбрать для ТС (\\d+) \\(с группой (\\d+)\\) группу (\\d+)$")
	public void updateVehGroupChange(int vehIndex, int oldGroupIndex, int groupIndex) {
		log.debug("Start vehicle update: change group");

		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles")
				.get(vehIndex - 1);
		Group groupFromPrevStep = (Group) SharedContainer.getObjectsFromSharedContainer("all", "groups")
				.get(groupIndex - 1);

		String vehGroupIdBeforeReq = vehicleFromPrevStep.getGroupId();

		JSONObject customer = new JSONObject();
		customer.put("id", vehicleFromPrevStep.getCustomerId());
		vehicleFromPrevStep.setGroupId(groupFromPrevStep.getGroupId());
		JSONObject group = new JSONObject();
		group.put("name", groupFromPrevStep.getName());
		group.put("id", groupFromPrevStep.getGroupId());
		vehicleFromPrevStep.getUrlParameters().clear();
		vehicleFromPrevStep.putUrlParameters("customer", customer);

		vehicleFromPrevStep.putUrlParameters("group", group);
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();

		groupFromPrevStep.addObjectToGroup(vehicleFromPrevStep.getVehicleId(), "Vehicle");
		groupFromPrevStep.updateInSharedContainer();
		// remove the object from old group
		Group.getFromSharedContainer(vehGroupIdBeforeReq).removeObjectInGroup(vehicleFromPrevStep.getVehicleId());

		log.debug("End vehicle update: change group");
	}
	 
	@Then("^В Админке отвязать группу от ТС$")
	public void updateVehGroupRemove() {
		log.debug("Start vehicle update: remove group");

		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		String oldGroup = vehicleFromPrevStep.getGroupId();
		JSONObject dummyGroup = new JSONObject();
		dummyGroup.put("name", "Без группы");
		dummyGroup.put("id", "000000000000000000000000");
		
		JSONObject customer = new JSONObject();
		customer.put("id", vehicleFromPrevStep.getCustomerId());
		vehicleFromPrevStep.setGroupId("000000000000000000000000");
		vehicleFromPrevStep.getUrlParameters().clear();
		vehicleFromPrevStep.putUrlParameters("customer", customer);
		vehicleFromPrevStep.putUrlParameters("group", dummyGroup);
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
		vehicleFromPrevStep.updateInSharedContainer();
		
		Group group = Group.getFromSharedContainer(oldGroup);
		group.removeObjectInGroup(vehicleFromPrevStep.getVehicleId());
		group.updateInSharedContainer();
		
		log.debug("End vehicle update: remove group");
	}
	
	
	@Then("^В Админке отвязать ТС от группы$")
	public void updateVehGroupRemove2() {
		updateVehGroupRemove();
	}
	//
	@Then("^В Админке отредактировать текстовые поля ТС$")
	public void updateVehTextFields() {
		log.debug("Start vehicle update: text fields");

		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));

		vehicleFromPrevStep.setName(initMap.get("name").toString());
		vehicleFromPrevStep.setVinCode(initMap.get("vinCode").toString());
		vehicleFromPrevStep.setNumber(initMap.get("vehicle_number").toString());
		vehicleFromPrevStep.setMark(initMap.get("mark").toString());
		vehicleFromPrevStep.setGarageNumber(initMap.get("garageNumber").toString());
		vehicleFromPrevStep.setMaxSpeed(initMap.get("maxSpeed").toString());
		vehicleFromPrevStep.setDescription(initMap.get("description").toString());
		
		
		JSONObject customer = new JSONObject();
		customer.put("id", vehicleFromPrevStep.getCustomerId());
		
		vehicleFromPrevStep.getUrlParameters().clear();
		vehicleFromPrevStep.putUrlParameters("name", vehicleFromPrevStep.getName());
		vehicleFromPrevStep.putUrlParameters("vinCode", vehicleFromPrevStep.getVinCode());
		vehicleFromPrevStep.putUrlParameters("Number",  vehicleFromPrevStep.getNumber());
		vehicleFromPrevStep.putUrlParameters("mark",  vehicleFromPrevStep.getMark());
		vehicleFromPrevStep.putUrlParameters("garageNumber",  vehicleFromPrevStep.getGarageNumber());
		vehicleFromPrevStep.putUrlParameters("maxSpeed",  vehicleFromPrevStep.getMaxSpeed());
		vehicleFromPrevStep.putUrlParameters("description",  vehicleFromPrevStep.getDescription());
		vehicleFromPrevStep.putUrlParameters("customer", customer);
		if(vehicleFromPrevStep.getGroupId()!=null){
			JSONObject group = new JSONObject();
			group.put("name", vehicleFromPrevStep.getGroupName());
			group.put("id", vehicleFromPrevStep.getGroupId());
			vehicleFromPrevStep.putUrlParameters("group", group);
		}
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();

		log.debug("End vehicle update: text fields");
	}

	
	@Given("^В Админке деактивировать ТС$")
	public void deactivateVeh() {
		log.debug("Start vehicle update: Deactivate");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		JSONObject customer = new JSONObject();
		customer.put("id", vehicleFromPrevStep.getCustomerId());
		vehicleFromPrevStep.setIsDeactivated("true");
		vehicleFromPrevStep.getUrlParameters().clear();
		vehicleFromPrevStep.putUrlParameters("customer", customer);
		vehicleFromPrevStep.putUrlParameters("isDeactivated", true);
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
		vehicleFromPrevStep.updateInSharedContainer();
		log.debug("End vehicle update: Deactivate");
	}

	@Then("^В Админке активировать ТС$")
	public void activateVeh() {
		log.debug("Start vehicle update: Activate");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		JSONObject customer = new JSONObject();
		customer.put("id", vehicleFromPrevStep.getCustomerId());
		vehicleFromPrevStep.setIsDeactivated("false");
		vehicleFromPrevStep.setDeviceId(null); //after deactivation relation to device has to be destroyed
		vehicleFromPrevStep.setTelematicsDevice(null);
		vehicleFromPrevStep.getUrlParameters().clear();
		vehicleFromPrevStep.putUrlParameters("customer", customer);
		vehicleFromPrevStep.putUrlParameters("isDeactivated", false);
		if(vehicleFromPrevStep.getUrlParameters().containsKey("device"))
			vehicleFromPrevStep.getUrlParameters().remove("device");
		vehicleFromPrevStep.updateVehiclesInAdminPanelResources();
		vehicleFromPrevStep.updateInSharedContainer();
		log.debug("End vehicle update: Activate");
	}
	
	
	public Customer getCustomer(){
		Customer customerFromPrevStep;
		List customerFromPrevStepLst = (List) SharedContainer.getObjectsFromSharedContainer("all","customers");
		if(customerFromPrevStepLst == null || customerFromPrevStepLst.isEmpty()){
			customerFromPrevStep = new Customer();
			JSONObject json_cust=	 (JSONObject) SharedContainer.getLastObjectFromSharedContainer("customer");
			try {//this is wrong way! but at least we get id and name
				customerFromPrevStep.setCustomerId(ReplyToJSON.extractPathFromJson(json_cust, "$._id.$oid"));
			} catch (Exception e) {
				log.error(ERROR,e);
			}
			customerFromPrevStep.setName(json_cust.get("Name").toString());
		} else {
			customerFromPrevStep = (Customer) customerFromPrevStepLst.get(0);
		}
		return customerFromPrevStep;
	}
}
