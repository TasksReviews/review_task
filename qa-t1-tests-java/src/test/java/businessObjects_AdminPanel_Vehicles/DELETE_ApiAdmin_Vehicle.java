package businessObjects_AdminPanel_Vehicles;

import com.t1.core.SharedContainer;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Given;

public class DELETE_ApiAdmin_Vehicle {

	
	//Api/Admin/Vehicle/
	@Given("В Админке удалить ТС$")
	public void deleteSeveralEmployeesInAdminPanel(){
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		vehicleFromPrevStep.deleteVehicleInAdminPanel();
		
	}
	
}
