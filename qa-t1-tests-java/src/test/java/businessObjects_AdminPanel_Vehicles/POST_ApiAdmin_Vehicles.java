package businessObjects_AdminPanel_Vehicles;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Customer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_ApiAdmin_Vehicles extends AbstractClass{

	@SuppressWarnings("rawtypes")
	@Given("^В Админке создать (\\d+) ТС с минимальным набором полей для созданного клиента$")
	public void createInAdminPanel_MinFields(int numberOfVehicles) {
		Customer customerFromPrevStep = getCustomerFromContainer();
		for (int i = 0; i < numberOfVehicles; i++) {
			log.debug("Start>> vehicle_create minFields adm panel");
			Vehicle vehicle = new Vehicle(true);
			vehicle.setCustomerId(customerFromPrevStep.getCustomerId());
			JSONObject customerId = new JSONObject();
			customerId.put("id", customerFromPrevStep.getCustomerId());
			vehicle.putUrlParameters("customer", customerId);
			vehicle.createInAdminPanelNewVehicle();
			vehicle.addToSharedContainer();
			log.debug("end of vehicle adm panel");
		}
	}
	
	
	@Given("^В Админке создать ТС с группой$")
	public void createInAdminPanel_MinFieldsWithGroup() {
		Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
		Customer customerFromPrevStep = getCustomerFromContainer();
		log.debug("Start>> vehicle_create minFields adm panel");
		Vehicle vehicle = new Vehicle(true);
		vehicle.setCustomerId(customerFromPrevStep.getCustomerId());
		JSONObject customerId = new JSONObject();
		customerId.put("id", customerFromPrevStep.getCustomerId());
		
		vehicle.setGroupId(group.getGroupId());
		JSONObject groupObj =new JSONObject();
		groupObj.put("id",group.getGroupId());
		groupObj.put("name",group.getName());
		
		vehicle.putUrlParameters("customer", customerId);
		vehicle.putUrlParameters("group", groupObj);
		
		vehicle.createInAdminPanelNewVehicle();
		vehicle.addToSharedContainer();
		
		group.addObjectToGroup(vehicle.getVehicleId(),"Vehicle");
		group.updateInSharedContainer();
		
		log.debug("end of vehicle adm panel");

	}
	
	
	@Given("^В Админке создать ТС с группой и с Сотрудником$")
	public void createInAdminPanel_MinFieldsWithGroupAndEmpl() {
		Group group = (Group) SharedContainer.getObjectsFromSharedContainer("all","groups").get(0);
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Customer customerFromPrevStep = getCustomerFromContainer();

		log.debug("Start>> vehicle_create minFields adm panel");
		Vehicle vehicle = new Vehicle(true);
		vehicle.setCustomerId(customerFromPrevStep.getCustomerId());
		JSONObject customerId = new JSONObject();
		customerId.put("id", customerFromPrevStep.getCustomerId());
		
		vehicle.setGroupId(group.getGroupId());
		JSONObject groupObj =new JSONObject();
		groupObj.put("id",group.getGroupId());
		groupObj.put("name",group.getName());
		
		vehicle.setEmployeeId(employeeFromPrevStep.getEmployeeId());
		JSONObject emlpoyeeObj =new JSONObject();
		emlpoyeeObj.put("id",employeeFromPrevStep.getEmployeeId());
		
		vehicle.putUrlParameters("customer", customerId);
		vehicle.putUrlParameters("group", groupObj);
		vehicle.putUrlParameters("employee", emlpoyeeObj);
		
		vehicle.createInAdminPanelNewVehicle();
		vehicle.addToSharedContainer();
		
		group.addObjectToGroup(vehicle.getVehicleId(),"Vehicle");
		group.updateInSharedContainer();
		
		log.debug("end of vehicle adm panel");

	}
	
	@Then("^В Админке создать ТС с Сотрудником$")
	public void createInAdminPanel_MinFieldsWithEmpl() {
		createInAdminPanel_MinFieldsWithEmpl(-1);

	}
	
	@Then("^В Админке создать ТС с Сотрудником (\\d+)$")
	public void createInAdminPanel_MinFieldsWithEmpl_Step(int emplIndex) {
		createInAdminPanel_MinFieldsWithEmpl(emplIndex-1);

	}
	
	public void createInAdminPanel_MinFieldsWithEmpl(int emplIndex) {
		Employee employeeFromPrevStep = null;
		if(emplIndex==-1){
			employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		} else {
			employeeFromPrevStep = (Employee) SharedContainer.getObjectsFromSharedContainer("all","employees").get(emplIndex);
		}
		Customer customerFromPrevStep = getCustomerFromContainer();

		log.debug("Start>> vehicle_create minFields adm panel");
		Vehicle vehicle = new Vehicle(true);
		vehicle.setCustomerId(customerFromPrevStep.getCustomerId());
		JSONObject customerId = new JSONObject();
		customerId.put("id", customerFromPrevStep.getCustomerId());
		
		vehicle.setEmployeeId(employeeFromPrevStep.getEmployeeId());
		JSONObject emlpoyeeObj =new JSONObject();
		emlpoyeeObj.put("id",employeeFromPrevStep.getEmployeeId());
		
		vehicle.putUrlParameters("customer", customerId);
		vehicle.putUrlParameters("employee", emlpoyeeObj);
		
		vehicle.createInAdminPanelNewVehicle();
		vehicle.addToSharedContainer();
		
		log.debug("end of vehicle adm panel");

	}
	
	@Given("В Админке создать (\\d) ТС с ТМУ для созданного клиента$")
	public void createSeveralEmployeesWithTrackerInAdminPanel(int totalVehicles){
		Customer customerFromPrevStep = getCustomerFromContainer();
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		for(int i=0; i<totalVehicles; i++){
			Vehicle vehicle = new Vehicle(true);
			vehicle.setCustomerId(customerFromPrevStep.getCustomerId());
			vehicle.setCustomerName(customerFromPrevStep.getName());
			JSONObject json_customer = new JSONObject();
			json_customer.put("id",vehicle.getCustomerId());
			json_customer.put("name",vehicle.getCustomerName());
			vehicle.putUrlParameters("customer",json_customer);
			
			vehicle.setDeviceId(telematics_device.getTrackerId());
			JSONObject deviceObj =new JSONObject();
			deviceObj.put("id",telematics_device.getTrackerId());
			vehicle.putUrlParameters("device", deviceObj);

			vehicle.createInAdminPanelNewVehicle();
			vehicle.addToSharedContainer();
		}
		
	}
	//should we move it to sharedContainer class or just correct customer creation step??
	public static Customer getCustomerFromContainer(){
		List customerFromPrevStepLst = (List) SharedContainer.getObjectsFromSharedContainer("any","customers");
		Customer customerFromPrevStep;
		if(customerFromPrevStepLst == null || customerFromPrevStepLst.isEmpty()){
			customerFromPrevStep = new Customer();
			JSONObject json_cust=	 (JSONObject) SharedContainer.getLastObjectFromSharedContainer("customer");
			try {//this is wrong way! but at least we get id and name
				customerFromPrevStep.setCustomerId(ReplyToJSON.extractPathFromJson(json_cust, "$._id.$oid"));
			} catch (Exception e) {
				log.error(ERROR,e);
			}
			customerFromPrevStep.setName(json_cust.get("Name").toString());
		} else {
			customerFromPrevStep = (Customer) customerFromPrevStepLst.get(0);
		}
		return customerFromPrevStep;
	}
}
