package security_accounts;

import org.json.simple.JSONObject;

import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountInfoAndSettings;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.GetAccountInfoFromMongo;

import cucumber.api.java.en.Then;

public class GET_AccountProfile extends AccountRegistrationAuth{
	
	@Then("^Запросить данные профиля$")
	public static void getAccountProfile(){

		log.debug("Start>> GET_getAccountProfile");
//String accountId = "569f8e57cc6b7527b0e4bdff";
		JSONObject accountFromPrevStep = (JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0);
		String accountId = accountFromPrevStep.get("id").toString();
		JSONObject receivedAccountInfo = (JSONObject) new AccountInfoAndSettings().getAccountProfileInfo(accountId);
		log.debug("end of get all accounts");
	}


	
	@Then("^Запросить данные случайнонго профиля$")
	public static void getRandomAccountProfile(){

		log.debug("Start>> GET_getRandomAccountProfile");
		try {
		JSONObject receivedAccountInfoFromMongo = (JSONObject)	GetAccountInfoFromMongo.getRandomAccountInfo().get(0);
		String	accountId = ReplyToJSON.extractPathFromJson(receivedAccountInfoFromMongo, "_id.$oid");
		JSONObject receivedAccountInfo = (JSONObject) new AccountInfoAndSettings().getAccountProfileInfo(accountId);//accountId);
		log.debug("end of get all accounts");
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные полученных оповещений':" + e.toString());
		}
	}

	@Then("^Запросить данные случайнонго профиля принадлежаего тому же клиенту$")
	public static void getRandomAccountProfileSameCustomer(){
		try {
		log.debug("Start>> GET_getRandomAccountProfileSameCustomer");

		JSONObject accountFromPrevStep = (JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0);
		String accIdFromPrevStep = accountFromPrevStep.get("id").toString();
		JSONObject accountFromPrevStepInfoMongo = (JSONObject) GetAccountInfoFromMongo.getAccountInfoByAccountId(accIdFromPrevStep, "CustomerId").get(0);
		String customerId = ReplyToJSON.extractPathFromJson(accountFromPrevStepInfoMongo, "CustomerId.$oid");
		log.debug("customerId for acc from prev step is: "+customerId);
		JSONObject receivedAccountInfoFromMongo = (JSONObject)	GetAccountInfoFromMongo.getRandomAccountInfoByCustomer(customerId).get(0);
		String	accountId = ReplyToJSON.extractPathFromJson(receivedAccountInfoFromMongo, "_id.$oid");
		log.debug("got accId for same customer:"+accountId);
		JSONObject receivedAccountInfo = (JSONObject) new AccountInfoAndSettings().getAccountProfileInfo(accountId);//accountId);
		log.debug("end of get all accounts");
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные полученных оповещений':" + e.toString());
		}
	}
}