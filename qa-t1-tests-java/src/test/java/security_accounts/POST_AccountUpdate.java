package security_accounts;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_AccountUpdate extends AccountRegistrationAuth{
	public AccountRegistrationAuth account; 

	@Then("^Изменить существующий аккаунт из профиля$")
	public  void updateAccFromProfile() {
		try {
			String token ="";
			if(SharedContainer.getContainers().containsKey("demoToken")){
				 token = getToken("demoToken");
			} else {
				 token = getToken("devToken");
			}

			AccountRegistrationAuth accountfromPrevStep = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			log.info("Start 'updateAccFromProfile'");
			//init must be made into  AccountRegistrationAuth
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
			account	= new AccountRegistrationAuth(initMap,false);
			account.setAccountId(accountfromPrevStep.getAccountId());
		JSONObject updatedAcc	= account.updateAccFromProfile(initMap, token, "");

			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));

		} catch (Exception e) {
			log.error(ERROR,e);
		}

	}

	

}