package security_accounts;

import java.util.ArrayList;

import cucumber.api.PendingException;
import org.json.simple.JSONObject;

import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_AccountCreate extends AccountRegistrationAuth {
    public AccountRegistrationAuth account;

    @Then("^создать новый аккаунт из профиля$")
    public void registerNewAccFromProfile() {
        try {
            String token = "";
            if (SharedContainer.getContainers().containsKey("demoToken")) {
                token = getToken("demoToken");
            } else {
                token = getToken("devToken");
            }

            log.info("Start 'registerNewAccFromProfile'");
            //why we do init here? must be made into AccountRegistrationAuth
            JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account", getProperty("stdPattern"));
            account = new AccountRegistrationAuth(initMap, false);
            JSONObject registeredNewAcc = account.registerNewAccFromProfile(initMap, token, "");
            account.setAccountId(registeredNewAcc.get("accountId").toString());
            SharedContainer.setContainer("accounts", (new ArrayList() {{
                add(account);
            }}));

        } catch (Exception e) {
            log.error(ERROR, e);
        }

    }


    @Then("^создать пользователя, для которого разрешены (\\d+) объектов и установлен флаг \"([^\"]*)\" = false$")
    public void создатьПользователяДляКоторогоРазрешеныОбъектовИУстановленФлагFalse(int arg0, String arg1) throws Throwable {

    }

    @Then("^проверить, что для пользователя сохранены (\\d+) разрешенных объектов$")
    public void проверитьЧтоДляПользователяСохраненыРазрешенныхОбъектов(int arg0) throws Throwable {

    }

    @Then("^проверить, что для пользователя сохранен флаг \"([^\"]*)\" = false$")
    public void проверитьЧтоДляПользователяСохраненФлагFalse(String arg0) throws Throwable {

    }

    @Then("^авторизоваться созданным пользователем$")
    public void авторизоватьсяСозданнымПользователем() throws Throwable {

    }
}