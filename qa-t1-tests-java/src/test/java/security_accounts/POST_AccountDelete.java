package security_accounts;

import java.util.HashMap;

import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_AccountDelete extends AccountRegistrationAuth{
	public AccountRegistrationAuth account; 

	@Then("^удалить существующий аккаунт из БД$")
	public  void deleteLastcreatedAccount() {
		try {
			String token ="";
			 token = getToken("devToken");

			AccountRegistrationAuth accountfromPrevStep = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			log.info("Start 'account/delete'");
			HashMap deletedAcc	= accountfromPrevStep.accountDelete();

		} catch (Exception e) {
			log.error(ERROR,e);
		}

	}
	
	

}