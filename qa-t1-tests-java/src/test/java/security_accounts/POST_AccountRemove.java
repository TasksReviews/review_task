package security_accounts;

import java.util.HashMap;

import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_AccountRemove extends AccountRegistrationAuth{
	public AccountRegistrationAuth account; 

	@Then("^перевести в состояние удаленного существующий аккаунт из профиля$")
	public  void removeAccFromProfile() {
		try {


			AccountRegistrationAuth accountfromPrevStep = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			log.info("Start 'перевести в состояние удаленного существующий аккаунт из профиля'");
			HashMap deletedAcc	= accountfromPrevStep.removeAccountFromProfile();


		} catch (Exception e) {
			log.error(ERROR,e);
		}

	}

	

}