package businessObjects_Groups;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang3.RandomStringUtils;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class POST_OmGroups_Update extends AbstractClass {
    @Then("^Изменить имя группы на \"([^\"]*)\"$")
    public void updateGroupName(String inputPattern)  {
        log.debug("Start group_update");
        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
        switch (inputPattern){
            case "латиница":    group.setName(RandomStringUtils.random(25, getProperty("specLatinCharPattern")));break;
            case "кириллица":   group.setName(RandomStringUtils.random(25, getProperty("specCyrilicCharPattern")));break;
            case "цифры": 	    group.setName(RandomStringUtils.random(25, getProperty("stdNumbPattern")));break;
            case "спецсимволы": group.setName(RandomStringUtils.random(25, getProperty("specSymbolCharPattern")));break;
        }
        group.updateGroup();
        group.updateObjectsGroupInfoInContainerFromResources();
        log.debug("End group_update");
    }
    
    @Then("^Отредактировать текстовые поля группы$")
    public void updateGroupTextFields()  {
    	log.debug("Start group_update");
        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
        group.setName("JFW_groupName"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern")));
        group.updateGroup();
        group.updateObjectsGroupInfoInContainerFromResources();
        log.debug("End group_update");
    }
    
    
    @SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	@Then("^Привязать к группе с (\\d+) сотрудника и (\\d+) ТС$")
    public void updateGroupAddEmplVeh(int numberOfEmpl, int numberOfVeh)  {
    	log.debug("Start group_update");
        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
        ArrayList<Employee> employeeLst =  SharedContainer.getObjectsFromSharedContainer("all", "employees");
        ArrayList<Vehicle> vehicleLst =  SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
        ArrayList resourcesLst = (ArrayList) group.getUrlParameters().get("resources");
		for (Employee employee : employeeLst) {
			if (numberOfEmpl > 0){
			//no info about this empl in group
				if (! group.getObjects().contains(employee.getEmployeeId()) ){//&& ! employee.groupId.contains(group.groupId)) {
					group.addObjectToGroup(employee.getEmployeeId(), "Employee");
					numberOfEmpl--;
				}
			}
		}
       for(Vehicle vehicle: vehicleLst){
    	   if (numberOfVeh > 0){
	    	   if (! group.getObjects().contains(vehicle.getVehicleId())) {
	    	   group.addObjectToGroup(vehicle.getVehicleId(),"Vehicle");
	    	   numberOfVeh--;
	    	   }
    	   }
       }
        group.updateGroup();
        group.updateObjectsGroupInfoInContainerFromResources();
        log.debug("End group_update");
    }
    
    @Then("^Привязать к группе (\\d+) сотрудника и (\\d+) ТС$")
    public void updateGroupAddEmplVeh2(int numberOfEmpl, int numberOfVeh) {
    	updateGroupAddEmplVeh(numberOfEmpl, numberOfVeh);
    }
    
    
	@Then("^Отвязать от группы с (\\d+) сотрудника и (\\d+) ТС$")
    public void updateGroupRemoveEmplVeh(int numberOfEmpl, int numberOfVeh)  {
    	log.debug("Start group_update");
        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
        ArrayList<HashMap<String,String>> resourcesLst = (ArrayList) group.getUrlParameters().get("resources");
		for (int i=0; i< resourcesLst.size(); i++) {
			HashMap<String,String> resource = resourcesLst.get(i);
			if (numberOfEmpl>0 && resource.get("resourceType").equals("Employee")   &&  group.getObjects().size()>0) {
				group.getResources().remove(resource);
				group.getObjects().remove(resource.get("id"));
				numberOfEmpl--;
			}
		}
		
		for (int i=0; i< resourcesLst.size(); i++) {
			HashMap<String,String> resource = resourcesLst.get(i);
			if (numberOfVeh>0 && resource.get("resourceType").equals("Vehicle")   &&  group.getObjects().size()>0) {
				group.getResources().remove(resource);
				group.getObjects().remove(resource.get("id"));
				numberOfVeh--;
			}
		}
		
		group.putUrlParameters("objects", group.getObjects());
		group.putUrlParameters("resources", group.getResources());
        group.updateGroup();
        group.updateObjectsGroupInfoInContainerFromResources();
        log.debug("End group_update");
    }
    
	@Then("^Отвязать от группы (\\d+) сотрудника и (\\d+) ТС$")
	public void updateGroupRemoveEmplVeh2(int numberOfEmpl, int numberOfVeh) {
		updateGroupRemoveEmplVeh( numberOfEmpl,  numberOfVeh) ;
	}
	
	@Then("^Изменить цвет группы на \"([^\"]*)\"$")
	public void changeGroupColor(String newColor)  {
    	log.debug("Start update_group_color");
        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
        group.setColor(newColor);
        group.putUrlParameters("color", group.getColor());
        group.updateGroup();
        group.updateObjectsGroupInfoInContainerFromResources();
        log.debug("End update_group_color");
	}
}
