package businessObjects_Groups;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_OmGroups_Add extends AbstractClass{

	@Given("^Создать (\\d+) [Г,г]рупп с (\\d+) [С,с]отрудниками и (\\d+) ТС$")
	public void createSeveralGroups_With_empl_veh(int totalGroups, int totalEmployees, int totalVehicles) {
		for(int i=0;i<totalGroups; i++){
 			createGroup_With_empl_veh(totalEmployees, totalVehicles);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Given("^Создать [Г,г]руппу с (\\d+) [С,с]отрудниками и (\\d+) ТС$")
	public void createGroup_With_empl_veh(int totalEmployees, int totalVehicles) {
		ArrayList<Employee> listOfEmployees;
		ArrayList<Vehicle> listOfVehicles;
		ArrayList<HashMap<String, String>> objects = new ArrayList<HashMap<String, String>>();
		
		if (totalEmployees > 0) {
			listOfEmployees = SharedContainer.getObjectsFromSharedContainer("all", "employees");

		//	for (int i = 0; i < totalEmployees; i++) {
				for (Employee employee : listOfEmployees) {
					if (employee.getGroupId() == null && totalEmployees > 0) {
						HashMap<String, String> object = new HashMap<String, String>();
						object.put("id", employee.getEmployeeId());
						object.put("resourceType", "Employee");
						objects.add(object);
						employee.setGroupId("000000000000000000000000");
						employee.updateInSharedContainer();
//						SharedContainer.removeFromContainer("employees",employee.employeeId);
//						SharedContainer.setContainer("employees", employee);
						totalEmployees--;
					}
				}
		//	}
		}
		if (totalVehicles > 0) {
			listOfVehicles = SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
				for (Vehicle vehicle : listOfVehicles) {
					if (vehicle.getGroupId() == null && totalVehicles>0) {
						HashMap<String, String> object = new HashMap<String, String>();
						object.put("id", vehicle.getVehicleId());
						object.put("resourceType", "Vehicle");
						objects.add(object);
						vehicle.setGroupId("000000000000000000000000");
						vehicle.updateInSharedContainer();
//						SharedContainer.removeFromContainer("vehicles", vehicle.vehicleId);
//						SharedContainer.setContainer("vehicles", vehicle);
						totalVehicles--;
					}
				}
			//}
		}
		Group group = new Group(objects);
		JSONObject jsonReply= group.addGroupInClientPanel();
		group.updateGroupInfoInResourcesInSharedContainer(group, jsonReply);
		group.addToSharedContainer();
		
		group.updateObjectsGroupInfoInContainerFromResources();
	}




@Then("^Создать [Г,г]руппу с color \"([^\"]*)\"$")
	public void createGroupWithColor(String inputPattern)throws Throwable{
		log.info("Got pattern:=" + inputPattern);
	ArrayList<Vehicle> listOfVehicles;
	ArrayList<Employee> listOfEmployees;
	ArrayList<HashMap<String, String>> objects = new ArrayList<HashMap<String, String>>();
	listOfVehicles = SharedContainer.getObjectsFromSharedContainer("any", "vehicles");
	for (Vehicle vehicle : listOfVehicles) {
		if (vehicle.getGroupId() == null) {
			HashMap<String, String> object = new HashMap<String, String>();
			object.put("id", vehicle.getVehicleId());
			object.put("resourceType", "Vehicle");
			objects.add(object);
			vehicle.setGroupId("000000000000000000000000");
			SharedContainer.removeFromContainer("vehicles", vehicle.getVehicleId());
			SharedContainer.setContainer("vehicles", vehicle);
		}
	}
	listOfEmployees = SharedContainer.getObjectsFromSharedContainer("any", "employees");
	for (Employee employee : listOfEmployees) {
		if (employee.getGroupId() == null) {
			HashMap<String, String> object = new HashMap<String, String>();
			object.put("id", employee.getEmployeeId());
			object.put("resourceType", "Employee");
			objects.add(object);
			employee.setGroupId("000000000000000000000000");
			SharedContainer.removeFromContainer("employees",employee.getEmployeeId());
			SharedContainer.setContainer("employees", employee);
		}
	}
		Group group = new Group(objects);
		group.setColor(inputPattern);
		group.putUrlParameters("color",  inputPattern);
		group.addGroupInClientPanel();
		group.updateInSharedContainer();
		group.updateObjectsGroupInfoInContainerFromResources();;
	}

	@And("^Создать [Г,г]руппу с name \"([^\"]*)\"$")
	public void createGroupWithName(String inputPattern) throws Throwable {
		log.info("Got pattern:=" + inputPattern);
		String pattern = null;

		ArrayList<Vehicle> listOfVehicles;
		ArrayList<Employee> listOfEmployees;
		ArrayList<HashMap<String, String>> objects = new ArrayList<HashMap<String, String>>();
		listOfVehicles = SharedContainer.getObjectsFromSharedContainer("any", "vehicles");
		for (Vehicle vehicle : listOfVehicles) {
			if (vehicle.getGroupId() == null) {
				HashMap<String, String> object = new HashMap<String, String>();
				object.put("id", vehicle.getVehicleId());
				object.put("resourceType", "Vehicle");
				objects.add(object);
				vehicle.setGroupId("000000000000000000000000");
				SharedContainer.removeFromContainer("vehicles", vehicle.getVehicleId());
				SharedContainer.setContainer("vehicles", vehicle);
			}
		}
		listOfEmployees = SharedContainer.getObjectsFromSharedContainer("any", "employees");
		for (Employee employee : listOfEmployees) {
			if (employee.getGroupId() == null) {
				HashMap<String, String> object = new HashMap<String, String>();
				object.put("id", employee.getEmployeeId());
				object.put("resourceType", "Employee");
				objects.add(object);
				employee.setGroupId("000000000000000000000000");
				SharedContainer.removeFromContainer("employees",employee.getEmployeeId());
				SharedContainer.setContainer("employees", employee);
			}
		}
		if (inputPattern.equals("латиница")){
			pattern = getProperty("specLatinCharPattern");
		} else if (inputPattern.equals("кириллица")) {
			pattern = getProperty("specCyrilicCharPattern");
		} else if (inputPattern.equals("цифры")) {
			pattern = getProperty("stdNumbPattern");
		} else if (inputPattern.equals("спецсимволы")) {
			pattern = getProperty("specSymbolCharPattern");
		}
		Group group = new Group(pattern, objects);
		group.addGroupInClientPanel();
		SharedContainer.setContainer("groups", group);
		
		group.updateObjectsGroupInfoInContainerFromResources();
	}
	

	

}
