package businessObjects_Groups;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicsTrack;
import com.t1.core.mongodb.GetGroupInfoFromMongo;

import cucumber.api.java.en.Then;

public class GET_Groups extends AbstractClass{
	public TelematicsTrack device;
	@Then("^получить случайную группу из БД$")
	public void getRandomTelematicsDevice(){
		 final JSONObject randomGroup = (JSONObject) GetGroupInfoFromMongo.getRandomGroupFromMongo().get(0);
//		log.debug("Start>> employee_create");
//		employee = new Employee();
//		JSONObject createdemployee = employee.createNewEmployee().get(0);
		SharedContainer.setContainer("json_group", (new ArrayList(){{add(randomGroup);}}));
//		//employee.deleteEmployee(employee.employeeId);
	}
}
