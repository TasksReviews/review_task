package businessObjects_Groups;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Group;

import cucumber.api.java.en.Then;

public class POST_OmGroups_Delete extends AbstractClass {
    @Then("^Удалить группу$")
    public void deleteGroupById() {
        log.debug("Start group_delete");
        Group group = (Group)SharedContainer.getLastObjectFromSharedContainer("groups");
        group.deleteGroup();
        log.debug("End group_delete");
    }
}
