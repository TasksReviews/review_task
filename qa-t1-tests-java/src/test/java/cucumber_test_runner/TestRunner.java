package cucumber_test_runner;
  
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
 
//TO RUN IT FROM COMMAND LINE:
//mvn clean test "-Dcucumber.options=src/test/java/com/t1/test/api/security/authorization/POST_register_cucumber.feature --plugin junit:target/cucumber-reports/junit-report.xml --tags @@Your_tag"
//mvn clean test "-Dcucumber.options=src/test/resources/XrayFeature/xrayFeatures.feature --plugin junit:target/cucumber-reports/junit-report.xml --tags @@Your_tag"
//mvn site

@RunWith(Cucumber.class) 
 
@CucumberOptions(features = "src/test/resources/XrayFeaturesByModules/",
plugin = {"html:target/cucumber-reports/html/", "junit:target/cucumber-reports/junit.xml","json:target/cucumber-reports/jsonreport.json"}
,glue = {"classpath:security_authorization", "classpath:security_settings", "classpath:security_accounts", "classpath:security_adminPanelAccounts", 
		"classpath:security_confirmation","classpath:security_contacts",
		"classpath:businessObjects_Departments","classpath:businessObjects_Employees","classpath:businessObjects_Vehicles","classpath:businessObjects_Groups","classpath:businessObjects_Resources",
		"classpath:businessObjects_AdminPanel_Departments","classpath:businessObjects_AdminPanel_Employees","classpath:businessObjects_AdminPanel_Vehicles",
		"classpath:businessObjects_AdminPanel_Groups","classpath:businessObjects_AdminPanel_Resources",
		"classpath:imitation_playback","classpath:imitation_virtualTrackers",
		
		"classpath:telematics_Devices","classpath:telematics_Geozones","classpath:telematics_intervalsGenerator","classpath:telematics_tracks",
		"classpath:customer",
		"classpath:reports", "classpath:reports_templates",
		"classpath:notifications_notifications","classpath:notifications_notificationRules","classpath:notifications_roadEventGenerator",
		"classpath:technicalSupport_schedule",
		"classpath:businessObjects_RelationChecks",
		"classpath:webapi",
		"classpath:cucumber_test_runner"}
)//tags = "@Mytag",
 
public class TestRunner {

}