package cucumber_test_runner;

import static com.mongodb.client.model.Filters.eq;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.mongodb.GetInfoFromMongo;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import ru.yandex.qatools.allure.annotations.Step;

public class BeforeTest extends AbstractClass{
	private Scenario scenario;
public static Boolean globalPrereqDone = false;
	@Before
	public void before(Scenario scenario) {
	    this.scenario = scenario;
	    log.info(">>>>>>>Start beforeTest for: "+scenario.getName());
	    if(!globalPrereqDone)
//	    	log.debug("createTestCustomerAndAccountsIfDontExists() call commented");
//	    	globalPrereqDone= true;
//	    	createTestCustomerAndAccountsIfDontExists();
	    log.info(">>>>>>>Start execution of test : "+scenario.getName());
	}

	@SuppressWarnings("unchecked")
	@Step("создание тестового клиента и тестовых аккаунтов если они не существуют")
	public void createTestCustomerAndAccountsIfDontExists(){
		log.debug("start createTestCustomerAndAccountsIfDontExists()");
	//	testCustomerId=59312de95532f50eb0250d02
		List<org.json.simple.JSONObject> mongoCustomerInfo = GetInfoFromMongo.getInfoById("Customers",getProperty("testCustomerId"),"");
		if (mongoCustomerInfo.isEmpty() || mongoCustomerInfo.size()==0){
			log.warn("test customer wasn't found! going to create it ");
			Document doc = new Document("_id",new ObjectId(getProperty("testCustomerId")))
					.append("Name" , "autotest_qa")
					.append("Address" , "autotest_qa") 
					.append("Description" , "autotest_qa DONT TOUCH!!! DONT DELETE!!!")
					.append("Phone" , "autotest_qa")
				    .append("Email" , "autotest_qa");
			
			com.mongodb.client.MongoCollection<Document> collection = com.t1.core.mongodb.MongoConnection.getMongoCollection("Customers");
			collection.insertOne(doc);
		}
		//check if all accounts are in DB by id's
		List<ObjectId> accountIds = new ArrayList<ObjectId>();
		accountIds.add(new ObjectId("59312de95532f50eb0250d03"));		accountIds.add(new ObjectId("59312de95532f50eb0250cff"));
		accountIds.add(new ObjectId("5982fbb95532f30c74b9e152"));		accountIds.add(new ObjectId("59aea7925532f3209c58a1cf"));
		accountIds.add(new ObjectId("59aea7c85532f3209c58a1d2"));		accountIds.add(new ObjectId("59aea81a5532f3209c58a1d7"));
		accountIds.add(new ObjectId("59aea8455532f3209c58a1db"));		accountIds.add(new ObjectId("59aea8875532f3209c58a1df"));
		accountIds.add(new ObjectId("59fc485423a3531490d3677a"));		accountIds.add(new ObjectId("59fc48f723a3531490d36780"));
		accountIds.add(new ObjectId("59fc494523a3531490d36784"));		accountIds.add(new ObjectId("59fc499523a3531490d36789"));
		accountIds.add(new ObjectId("5a33df0c23a3531f4c7150b1"));
		Bson filter = Filters.and ( 
				eq("CustomerId",new ObjectId(getProperty("testCustomerId"))  ),
				Filters.in("_id",accountIds  )
				) ;
		long totalAccountsFound = com.t1.core.mongodb.GetInfoFromMongo.countFilteredByCustomFilters("Accounts", filter);
		if(totalAccountsFound!=13){

			//some accounts missing check if test account exists 1 by 1
			HashMap<String,String> testAccounts = new HashMap<String,String>();
			testAccounts.put(getProperty("t1APIUser"),			"59312de95532f50eb0250d03");//demo
			testAccounts.put(getProperty("t1APIAdmin"),			"59312de95532f50eb0250cff");
			testAccounts.put(getProperty("t1APICourier"),		"5a33df0c23a3531f4c7150b1");
			testAccounts.put(getProperty("t1APIDeveloper"),		"5982fbb95532f30c74b9e152");
			testAccounts.put(getProperty("t1APIDriver"),		"59aea7925532f3209c58a1cf");
			testAccounts.put(getProperty("t1APIOperator"),		"59aea7c85532f3209c58a1d2");
			testAccounts.put(getProperty("t1APIDispatcher"),	"59aea81a5532f3209c58a1d7");
			testAccounts.put(getProperty("t1APITransportExpert"),"59aea8455532f3209c58a1db");
			testAccounts.put(getProperty("t1APIChief"),			"59aea8875532f3209c58a1df");
			testAccounts.put(getProperty("t1APIAnalyst"),		"59fc485423a3531490d3677a");
			testAccounts.put(getProperty("t1APITechnicalExpert"),"59fc48f723a3531490d36780");
			testAccounts.put(getProperty("t1APIFinancialExpert"),"59fc494523a3531490d36784");
			testAccounts.put(getProperty("t1APIInsuranceAnalyst"),"59fc499523a3531490d36789");
			//get all roles 
			List<org.json.simple.JSONObject> allAccountRoles =  GetInfoFromMongo.getInfoByCustomFilters("AccountRoles", null, Arrays.asList("_id", "Name"));
			
			for(String accountLogin : testAccounts.keySet()){
				filter = Filters.and ( eq("CustomerId",new ObjectId(getProperty("testCustomerId"))  ),
											eq("Login",accountLogin  )) ;
				
				List<org.json.simple.JSONObject> mongoAccountInfo = com.t1.core.mongodb.GetInfoFromMongo.getInfoByCustomFilters("Accounts", filter, new ArrayList());
				
				if (mongoAccountInfo.isEmpty() || mongoCustomerInfo.size()==0){
					log.warn("test account "+accountLogin+" wasn't found! going to create it ");
					
					String accountId =testAccounts.get(accountLogin);
					String accountRoleId ="";
					//looking for role id
					for (org.json.simple.JSONObject role : (List<org.json.simple.JSONObject>) allAccountRoles){
						if(role.get("Name").toString().toLowerCase().contains(accountLogin.substring(12, accountLogin.length()).toLowerCase())){
							try {
								accountRoleId = ReplyToJSON.extractPathFromJson(role, "$._id.$oid");
							} catch (Exception e) {
								log.error(ERROR,e);
							}
							break;		
						}
					}
					try {

						//prepare doc to insert account
					JSONObject testAccountJson = new JSONObject();
					
					testAccountJson.put("_id",new ObjectId(accountId));//"59992de95532f50eb0250d03"));
					testAccountJson.put("CustomerId",new ObjectId(getProperty("testCustomerId")) );
					testAccountJson.put("Type", "Person");
					testAccountJson.put("Profile", com.t1.core.AbstractClass.toJson("{\"_t\" : \"PersonProfile\", \"Firstname\" : \""+accountLogin+"\", \"Lastname\" : \""+accountLogin+"\","
							+" \"Middlename\" : null, \"Sex\" : \"Male\", \"Birth\" : null, \"DepartmentId\" : null, \"AvatarUri\" : null, \"EmployeeId\" : null}"));
					ArrayList<ObjectId> rolesList = new ArrayList<ObjectId>();
					rolesList.add(new ObjectId(accountRoleId));
					testAccountJson.put("Roles",rolesList );
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.ENGLISH);
					format.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
					testAccountJson.put("Created",format.parse("2017-06-02T09:20:41.129Z") );
					
					testAccountJson.put("Status", "Active" );
					testAccountJson.put("Relations", new ArrayList());
					testAccountJson.put("Login", accountLogin);
					testAccountJson.put("Password", "Af69A8W1Mp8Ed0l4RadlAfPZkrw2QGK1mIR7XwLzxMw=");
					testAccountJson.put("PasswordSalt", "MWcCI4YhEQ==");
					
					JSONObject conacts0 = new JSONObject();
					conacts0.put("Name", null);
					conacts0.put("Type", "Email");
					conacts0.put("Value", accountLogin+"@email.com");
					conacts0.put("Created",   format.parse("2017-06-02T09:20:41.113Z"));
					conacts0.put("Confirmed", format.parse("2017-06-02T09:20:41.285Z"));
					conacts0.put("IsPrimary", false);
					
					JSONObject conacts1 = new JSONObject();
					conacts1.put("Name", null);
					conacts1.put("Type", "Phone");
					conacts1.put("Value", "+70"+RandomStringUtils.random(9, getProperty("stdNumbPattern")));
					conacts1.put("Created",   format.parse("2017-06-02T09:20:41.113Z"));
					conacts1.put("Confirmed", format.parse("2017-06-02T09:20:41.285Z"));
					conacts1.put("IsPrimary", true);
					
					ArrayList contacts = new ArrayList();
					contacts.add(conacts0);
					contacts.add(conacts1);
					testAccountJson.put("Contacts", contacts);
					
					JSONObject viewSettings = com.t1.core.AbstractClass.toJson("{\"Parkings\" : true,\"OverSpeedTurns\" : true,\"Idlings\" : true,\"SharpBrakings\" : true,\"SharpSpeedups\" : true,"
							+"\"SharpTurns\" : true,\"SharpSpeedupUserLimits\" : true,\"SharpSpeedupUserLimitsValues\" : {\"SharpSpeedupMinX\" : 0.0,\"SharpSpeedupMaxX\" : 0.0,\"SharpSpeedupMinY\" : 0.0,"
							+"\"SharpSpeedupMaxY\" : 0.0},\"Discharges\" : true,\"Refuelings\" : true,\"PanicButtons\" : true,\"OverSpeeds\" : false,\"OverSpeedUserLimits\" : true,\"OverSpeedLimit\" : 60,"
							+"\"PinsSize\" : 48}");
					viewSettings.put("OverSpeedLimit", 60);
					viewSettings.put("PinsSize", 48);
					
					JSONObject systemSettings =new JSONObject();
					systemSettings.put("Timezone", "Russian Standard Time");
					systemSettings.put("Language", "Russian");
					systemSettings.put("UpdateFrequency", 60);
					
					JSONObject objectViewSettings = com.t1.core.AbstractClass.toJson("{\"ConnectedObjectData\" : true,\"EmployeeViewFields\" : {\"Photo\" : true,\"FullName\" : true,\"Phone\" : true,"
							+"\"Type\" : false,\"Group\" : false,\"Device\" : false},\"VehicleViewFields\" : {\"Photo\" : true,\"Name\" : true,\"MarkModelNumber\" : true,\"GarageNumber\" : false,"
							+"\"Group\" : false,\"DeviceModel\" : false}}");
					
					JSONObject settings =new JSONObject();
					settings.put("ViewSettings", viewSettings);
					settings.put("SystemSettings", systemSettings);
					settings.put("ObjectViewSettings", objectViewSettings);
					
				testAccountJson.put("Settings", settings);

					Document doc = new Document();
					doc.putAll(testAccountJson);
					
					com.mongodb.client.MongoCollection<Document> collection = com.t1.core.mongodb.MongoConnection.getMongoCollection("Accounts");
					collection.insertOne(doc);
					log.debug("added account "+accountId);
					} catch (Exception e) {
						globalPrereqDone= true;
						log.error("something went wrong while trying to insert account "+accountId, e);
					}
				}
				}
			 
			} else {
				log.debug("all test accounts found.");
			}
		globalPrereqDone= true;
	}
}
