package cucumber_test_runner;

import com.t1.core.AbstractClass;
import com.t1.core.TearDownExecutor;

import cucumber.api.Scenario;
import cucumber.api.java.After;

public class Teardown extends AbstractClass {
	private Scenario scenario;

	// common teardown method to delete accounts/vehicles etc.
	@After
	public static void afterTest_common(Scenario scenario) {
	   // this.scenario = scenario;
		log.info(">>>>>>>Execution for test "+scenario.getName()+" has been completed. Start cleanup.");
		TearDownExecutor.teardownDeleteEntities(teardownData);
		log.debug(">>>>>>>cleanup completed.");
	}
}
