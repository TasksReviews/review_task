package reports_templates;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Geozone;
import com.t1.core.api.Group;
import com.t1.core.api.Report;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class POST_ReportTemplates extends AbstractClass{
	public Report report; 
//"<reportType>","<reportCriteriaParams>", "<resourceType>","<resourcesFilterInclude>", "<dateFrom>","<dateTo>",
//	"<periodicity>","<detalization>","<geozoneIds>","<name>", "<description>"

	@Then("^запросить создание шаблона отчета с параметрами из таблицы: \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"(.*)\", \"(.*)\"$")
	public void createReportTemplate(String reportType, String reportCriteriaParams, String resourceType, final String resourcesFilterInclude, String dateFrom, String dateTo, 
						  String periodicity, String detalization, final String geozoneIds, String name,String description){
		log.debug("Start>> report template step");
//		String periodicity ="Manually";
//		String detalizationType ="";
//		if(isDeepDetailed){
//			detalizationType ="DeepDetailed";
//		} else {
//			detalizationType ="Consolidated";
//		}
		Matcher matcher = Pattern.compile("\\[(.*)\\]\\{(.*)\\}").matcher(name);
		if (matcher.matches()) {
			String pattern = matcher.group(1);
			int totalChars = Integer.parseInt(matcher.group(2));
			pattern = preparePattern(pattern);
			name = RandomStringUtils.random(totalChars, pattern);
		}
		matcher = Pattern.compile("\\[(.*)\\]\\{(.*)\\}").matcher(description);
		if (matcher.matches()) {
			String pattern = matcher.group(1);
			int totalChars = Integer.parseInt(matcher.group(2));
			pattern = preparePattern(pattern);
			description = RandomStringUtils.random(totalChars, pattern);
		}	
		report = new Report( reportType, name, description, reportCriteriaParams, resourceType, resourcesFilterInclude, dateFrom, dateTo, periodicity, 
				geozoneIds,  detalization);
		
		JSONObject createdvehicle = (JSONObject) report.requestReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}

	//qa-2216

	@Then("^запросить создание шаблона отчета с минимальным количеством полей: \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"(.*)\", \"(.*)\", \"([^\"]*)\"$")
	public void createReportTemplateMinFields(String reportType, String reportCriteriaParams, String dateFrom, String dateTo,  
						  String periodicity, String detalization,String name,String description,String resourceType){
		log.debug("Start>> report template step");
		String geozoneIdsParam= null;
		String inputResourcesFilterInclude= null;

		//сегодня 00:00|сегодня 23:59
		if(dateFrom.contains("сегодня 00:00") && dateTo.contains("сегодня 23:59")){
			Map<String, String> resDatePeriod = getDateTimePeriodInUTC("календарные сутки");
			dateFrom=resDatePeriod.get("dateTimeFrom").toString();
			dateTo=resDatePeriod.get("dateTimeTo").toString();
		}
		Matcher matcher = Pattern.compile("\\[(.*)\\]\\{(.*)\\}").matcher(name);
		if (matcher.matches()) {
			String pattern = matcher.group(1);
			int totalChars = Integer.parseInt(matcher.group(2));
			pattern = preparePattern(pattern);
			name = RandomStringUtils.random(totalChars, pattern);
		}
		matcher = Pattern.compile("\\[(.*)\\]\\{(.*)\\}").matcher(description);
		if (matcher.matches()) {
			String pattern = matcher.group(1);
			int totalChars = Integer.parseInt(matcher.group(2));
			pattern = preparePattern(pattern);
			description = RandomStringUtils.random(totalChars, pattern);
		}	

		
		
		report = new Report( reportType, name, description, reportCriteriaParams, resourceType, inputResourcesFilterInclude, dateFrom, dateTo, 
								periodicity, geozoneIdsParam,  detalization);
		
		JSONObject createdvehicle = (JSONObject) report.requestReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	//QA-3660   "<reportType>","<detalization>"
	@Then("^запросить создание шаблона отчета с параметрами из таблицы: \"([^\"]*)\",\"([^\"]*)\"$")
	public void createReportTemplateByTypeAndPeriodicity(String reportType, String detalization){
		log.debug("Start>> report template step");

		report = new Report( reportType);
		report.setDetalization(detalization);
		report.putUrlParameters("detalization",detalization);

		report.requestReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
//QA-3661
	@Then("^запросить создание шаблона отчета с параметрами из таблицы: \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
	public void createReportTemplate(String reportType, String periodicity, String dateFrom, String dateTo){
		log.debug("Start>> report template step");

		//сегодня 00:00|сегодня 23:59
		if(dateFrom.contains("вчера 00:00") && dateTo.contains("вчера 23:59")){
			//getDateTimePeriod (dateTimePeriod, TimeZone.getTimeZone("UTC"),"",false)
			Map<String, String> resDatePeriod = getDateTimePeriod("вчера",TimeZone.getTimeZone("UTC"),standartTimeFormatNoSpace,false);
			dateFrom=resDatePeriod.get("dateTimeFrom")+"+03:00";
			dateTo=resDatePeriod.get("dateTimeTo")+"+03:00";
		}

		report = new Report( reportType);
		report.setPeriodicity(periodicity);
		report.putUrlParameters("periodicity",periodicity);

		report.setDateFrom(dateFrom);
		report.putUrlParameters("dateFrom",dateFrom);
		report.setDateTo( dateTo);
		report.putUrlParameters("dateTo",dateTo);
		report.requestReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	
	public String preparePattern (String input){
		String pattern ="";
		if(input.contains("A-Z")){
			pattern +="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			input = input.replace("A-Z", "");
		}
		if(input.contains("a-z")){
			pattern +="abcdefghijklmnopqrstuvwxyz";
			input = input.replace("a-z", "");
		}
		if(input.contains("0-9")){
			pattern +="0123456789";
			input = input.replace("0-9", "");
		}
		pattern +=input;
		return pattern;
	}

	@Then("^Отправить запрос создания шаблона отчета с reportType = \"([^\"]*)\", reportCriteria = \"([^\"]*)\"$")
	public void createReportTemplate(String reportType, final String reportCriteriaParams){
		log.debug("Start>> report template step");
		String geozoneIdsParam= null;
		String name="JFW_repTemplname"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		String description="JFW_repTemplDescr"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
	//	if(geozoneIds!=""){
	//		geozoneIdsParam = geozoneIds;
	//	}
		report = new Report( reportType);
		//set received reportCriteria instead of random one
		report.setReportCriteria((new ArrayList(){{add(reportCriteriaParams);}}));
		JSONObject contentConfig = (JSONObject) report.getUrlParameters().get("contentConfig");
		contentConfig.put("reportCriteria", new ArrayList(){{add(reportCriteriaParams);}});
		report.putUrlParameters("contentConfig", contentConfig);
		//end
		JSONObject createdReportTemplate = (JSONObject) report.requestReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	
	@SuppressWarnings("unchecked")
	@Then("^Отправить запрос создания шаблона отчета с reportType= \"([^\"]*)\", shiftList = \"([^\"]*)\", onlyFullShifts = \"([^\"]*)\"$")
	public void createReportTemplate_reportType_shiftList_onlyFullShifts(String reportType, List shiftListReq, boolean onlyFullShifts) {
		log.debug("Start>> report template step");
		String name="JFW_repTemplname"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		String description="JFW_repTemplDescr"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		
		report = new Report( reportType);
		//set received shiftList instead of random one
		assertTrue (shiftListReq.size() <6, "тест расчитан на 5 смен +1 стандартная");

		report.getShiftsSettings().put("onlyFullShifts", onlyFullShifts);
		List shiftList = new ArrayList();
		for (String shift : (List<String>) shiftListReq){
			if(shift.contains("пользовательская")){
				int starthour =18; //6 shifts possible
				JSONObject shiftFields = new JSONObject();
				shiftFields.put("timeFrom", starthour+shiftList.size()+":01");
				shiftFields.put("timeTo", starthour+shiftList.size()+1+":00");
				shiftFields.put("name", "Смена "+(shiftList.size()+2));
				shiftList.add(shiftFields);

			} else if(shift.contains("дефолтная")){
				shiftList.add( ((List)toJson(report.getReportVals().get("shiftsSettings")).get("shiftList")).get(0));
			} else  if(shift.contains("[]")){
				shiftList = new ArrayList();
			} else {
				assertTrue(false, "unsupported type of shift! "+shift);
			}
		}
		report.getShiftsSettings().put("shiftList", shiftList);
		report.putUrlParameters("shiftsSettings", report.getShiftsSettings());
		//end
		JSONObject createdReportTemplate = (JSONObject) report.requestReportTemplate();
		report.addToSharedContainer();
	}
	
	@Then("^Отправить запрос создания шаблона отчета с graphTimeUnit= \"([^\"]*)\", dataTimeUnit= \"([^\"]*)\", isDeepDetailed= \"([^\"]*)\",период \"([^\"]*)\"$")
	public void createReportTemplate(String graphTimeUnit, String dataTimeUnit, Boolean isDeepDetailed, String datePeriod){
		log.debug("Start>> report template step");
	
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});
		Map<String, String> resDatePeriod = getDateTimePeriodInUTC(datePeriod);
		report = new Report( reportType);
	
		//set recived params
		report.setDateFrom(resDatePeriod.get("dateTimeFrom").toString());
		report.setDateTo(resDatePeriod.get("dateTimeTo").toString());
//		report.setDataTimeUnit(dataTimeUnit);
//		report.setGraphTimeUnit(graphTimeUnit);
//		report.setDetalizationType(detalization);
		//update url
		report.putUrlParameters("dateFrom", report.getDateFrom());
		report.putUrlParameters("dateTo", report.getDateTo());
		report.putUrlParameters("graphTimeUnit", graphTimeUnit);
		report.putUrlParameters("dataTimeUnit", dataTimeUnit);
		report.putUrlParameters("isDeepDetailed", isDeepDetailed);
		//end
		JSONObject createdReportTemplate = (JSONObject) report.requestReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	
	
	
	@Then("^Отправить запрос создания шаблона отчета с resourceType= \"([^\"]*)\", resourcesFilter\\.include= (.*)$")
	public void createReportTemplate_resourceType_resourcesFilter_include(String resourceType, ArrayList<String> inputResourcesFilterInclude) {
		log.debug("Start>> report template step");
		ArrayList<String> resourcesFilterInclude = new ArrayList();
		int indexNumber=-1;
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});

		report = new Report( reportType);
		//edit repor params according to test input params
		report.setResourceType(resourceType);
		ArrayList<Employee> employeesFromPrevStep = (ArrayList<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
		ArrayList<Vehicle> vehiclesFromPrevStep = (ArrayList<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all","vehicles");
		for(String resource: inputResourcesFilterInclude){
			resource= resource.replace("\"", "").replace("[", "").replace("]", "");
			if(resource.contains("ТС")){
				 indexNumber= Integer.parseInt(resource.substring(resource.indexOf("ТС")+2,resource.length()));
				 resourcesFilterInclude.add(vehiclesFromPrevStep.get(indexNumber-1).getVehicleId());
			}else if(resource.contains("Сотрудник")){
				indexNumber= Integer.parseInt(resource.substring(resource.indexOf("трудник")+7,resource.length()));
				 resourcesFilterInclude.add(employeesFromPrevStep.get(indexNumber-1).getEmployeeId());
			}
		}
		report.setResourcesFilterInclude(resourcesFilterInclude);
		report.putUrlParameters("ResourceType", report.getResourceType());
		JSONObject urlResourcesFilter = (JSONObject) report.getUrlParameters().get("resourcesFilter");
		urlResourcesFilter.put("include", resourcesFilterInclude);
		report.putUrlParameters("resourcesFilter", urlResourcesFilter);
		
		//end
				JSONObject createdReportTemplate = (JSONObject) report.requestReportTemplate();
				SharedContainer.setContainer("reportTemplates", report);
	}
	//qa-2911
	@Then("^Отправить запрос создания шаблона отчета с resourcesFilter\\.groups = \"([^\"]*)\", resourcesFilter\\.include = \"([^\"]*)\", resourcesFilter\\.exclude = \"([^\"]*)\"$")
	public void createReportTemplate_resourcesFilter_groups_resourcesFilter_include(int resourcesFilterGroups, int inputResourcesFilterInclude, int inputResourcesFilterExclude) {
		log.debug("Start>> report template step");
		ArrayList<String> resourcesFilterInclude = new ArrayList();
		ArrayList<String> resourcesFilterExclude = new ArrayList();
		int indexNumber=-1;
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});
		ArrayList<String> groupsIds = new ArrayList<String>();
		report = new Report( reportType);
		//edit repor params according to test input params
		
		ArrayList<String> objectsInGroups = new ArrayList<String>();
		ArrayList<Group> groupsFromPrevStep = (ArrayList<Group>) SharedContainer.getObjectsFromSharedContainer(resourcesFilterGroups,"groups");
		for(Group group : groupsFromPrevStep){
			groupsIds.add(group.getGroupId());
			objectsInGroups.addAll( group.getObjects());
		}
		ArrayList<Employee> employeesFromPrevStep = (ArrayList<Employee>) SharedContainer.getObjectsFromSharedContainer("any","employees");
		ArrayList<Vehicle> vehiclesFromPrevStep = (ArrayList<Vehicle>) SharedContainer.getObjectsFromSharedContainer("any","vehicles");

		if(inputResourcesFilterInclude >0) {
			for (Employee empl : employeesFromPrevStep ){
				if(!objectsInGroups.contains(empl.getEmployeeId())){
					resourcesFilterInclude.add(empl.getEmployeeId());
				}
			}
			
			for (Vehicle veh : vehiclesFromPrevStep ){
				if(!objectsInGroups.contains(veh.getVehicleId())){
					resourcesFilterInclude.add(veh.getVehicleId());
				}
			}
		}

		if(inputResourcesFilterExclude>0){
			for(int i=0; inputResourcesFilterExclude>0; i++, inputResourcesFilterExclude--){
					int rndIndex = 0 + (int) (Math.random() * (objectsInGroups.size()-1-i));
					resourcesFilterExclude.add(objectsInGroups.get( rndIndex));
			}
		}

		report.setResourcesFilterGroups(groupsIds);
		report.setResourcesFilterInclude(resourcesFilterInclude);
		report.setResourcesFilterExclude(resourcesFilterExclude);
		report.putUrlParameters("ResourceType", report.getResourceType());
		JSONObject urlResourcesFilter = (JSONObject) report.getUrlParameters().get("resourcesFilter");
		urlResourcesFilter.put("groups", groupsIds);
		urlResourcesFilter.put("include", resourcesFilterInclude);
		urlResourcesFilter.put("exclude", resourcesFilterExclude);
		report.putUrlParameters("resourcesFilter", urlResourcesFilter);
		
		//end
				JSONObject createdReportTemplate = (JSONObject) report.requestReportTemplate();
				SharedContainer.setContainer("reportTemplates", report);
	}
	
	
	@Then("^Отправить запрос создания шаблона отчета с reportType= \"([^\"]*)\", geozoneIds = (.*)$")
	public void createReportTemplate_reportType_geozoneIds(String reportType, ArrayList<String>  inputGeozoneIds) throws Throwable {
		log.debug("Start>> report template step");
		int indexNumber=-1;
		ArrayList<String> geozoneIds = new ArrayList();
		List<Geozone> geozonesList =  SharedContainer.getFromContainer("geozones");
		String geozoneIdsParam= null;
		String name="JFW_repTemplname"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		String description="JFW_repTemplDescr"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		
		for(String geozoneReq: inputGeozoneIds){
			geozoneReq= geozoneReq.replace("\"", "").replace("[", "").replace("]", "");
			if(geozoneReq.contains("Геозона")){
				 indexNumber= Integer.parseInt(geozoneReq.substring(geozoneReq.indexOf("Геозона")+7,geozoneReq.length()));
				 geozoneIds.add(geozonesList.get(indexNumber-1).getGeozoneId());
			}
		}

		report = new Report( reportType);
		report.setGeoZoneIds(geozoneIds);
		report.putUrlParameters("geozoneIds", report.getGeoZoneIds());
		JSONObject createdReportTemplate = (JSONObject) report.requestReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	
	@Then("^Отправить запрос создания шаблона отчета с detalization = \"([^\"]*)\", resourcesFilter\\.include: ТС\"([^\"]*)\",сотрудников\"([^\"]*)\",группа1:ТС\"([^\"]*)\",сотрудников\"([^\"]*)\", группа2:ТС\"([^\"]*)\",сотрудников\"([^\"]*)\", за период = \"([^\"]*)\", с periodicity = \"([^\"]*)\"$")
	public void createReportTemplate_Detalization_resourcesFilter_include_period(String detalization, int veh,int empl, int gr1Veh,int gr1Empl,int gr2Veh,int gr2Empl, String period, String periodicity)  {
		Map<String, String> resDatePeriod = new HashMap();
		ArrayList<String> resourcesFilterInclude = new ArrayList();
		ArrayList<Group> groupsFromPrevStep = new ArrayList<Group>();
//		assertTrue(graphTimeUnit.equals("Day"), "graphTimeUnit "+graphTimeUnit+" не был реализован!");
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});
		report = new Report( reportType);
		if(period.contains("час")){
		resDatePeriod = getDateTimePeriodInUTC("час");
		
	}
	if(period.contains("календарные сутки")){
		resDatePeriod = getDateTimePeriodInUTC("календарные сутки");
	}
	if(period.matches("\\d{1,2} дня$")){
		resDatePeriod = getDateTimePeriodInUTC(period);
	}
	
	if(period.matches("\\d{1,2}[ ]{0,2}день, в пределах месяца.*")){
		resDatePeriod.put("dateTimeFrom","2017-08-01 00:00:00");//1.08 - 31.08
		resDatePeriod.put("dateTimeTo","2017-08-31 23:59:59");
	}
	if(gr1Veh>0 || gr2Veh>0 || veh>0){
		report.setResourceType("Vehicles");
	} else {
		report.setResourceType("Employees");
	}
		
	//prepare resourcesFilter.include
		if (veh-(gr1Veh+gr2Veh) > 0) {//how to distinguish what veh should be used if remains any? :(
			ArrayList<Vehicle> vehiclesFromPrevStep = (ArrayList<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
			for (int i = 0; i < veh; i++) {
				resourcesFilterInclude.add(vehiclesFromPrevStep.get(i).getVehicleId());
			}
		}

		if (empl-(gr1Empl+gr2Empl) > 0) {
			ArrayList<Employee> employeesFromPrevStep = (ArrayList<Employee>) SharedContainer.getObjectsFromSharedContainer("all", "employees");
			for (int i = 0; i < empl; i++) {
				resourcesFilterInclude.add(employeesFromPrevStep.get(i).getEmployeeId());
			}
		}
	if (gr1Veh>0 || gr2Veh>0 ||gr1Empl >0 ||gr2Empl >0){
		 groupsFromPrevStep = (ArrayList<Group>) SharedContainer.getObjectsFromSharedContainer("all","groups");
	
	if(gr1Veh >0 || gr1Empl >0){
	 ArrayList<HashMap<String, String>> group1resources = ((Group) groupsFromPrevStep.get(0)).getResources();
	 for(HashMap<String, String> resource : group1resources){
		 if(resource.get("resourceType").equals("Vehicle") && gr1Veh >0){
			 resourcesFilterInclude.add(resource.get("id"));
			 gr1Veh--;
		 }
		 if(resource.get("resourceType").equals("Employee") && gr1Empl >0){
			 resourcesFilterInclude.add(resource.get("id"));
			 gr1Empl--;
		 }
	 }
	 
	}
	if(gr2Veh >0 || gr2Empl >0){
		 ArrayList<HashMap<String, String>> group2resources = ((Group) groupsFromPrevStep.get(1)).getResources();
		 for(HashMap<String, String> resource : group2resources){
			 if(resource.get("resourceType").equals("Vehicle") && gr2Veh >0){
				 resourcesFilterInclude.add(resource.get("id"));
				 gr2Veh--;
			 }
			 if(resource.get("resourceType").equals("Employee") && gr2Empl >0){
				 resourcesFilterInclude.add(resource.get("id"));
				 gr2Empl--;
			 }
		 }
		}
	}
	//set recived params
	report.setDateFrom(resDatePeriod.get("dateTimeFrom").toString());
	report.setDateTo(resDatePeriod.get("dateTimeTo").toString());
	report.setDetalization(report.detalizationType(detalization));
	report.setPeriodicity(periodicity);
	report.setResourcesFilterInclude(resourcesFilterInclude);
	
	//update url
	report.putUrlParameters("dateFrom", report.getDateFrom());
	report.putUrlParameters("dateTo", report.getDateTo());
	report.putUrlParameters("detalization", detalization);
	report.putUrlParameters("periodicity", periodicity);
	
	report.putUrlParameters("ResourceType", report.getResourceType());
	JSONObject urlResourcesFilter = (JSONObject) report.getUrlParameters().get("resourcesFilter");
	urlResourcesFilter.put("include", resourcesFilterInclude);
	report.putUrlParameters("resourcesFilter", urlResourcesFilter);
	
	//end
			JSONObject createdReportTemplate = (JSONObject) report.requestReportTemplate();
			SharedContainer.setContainer("reportTemplates", report);
	
	}
	
	
	@Then("^Отправить запрос создания шаблона отчета с dateFrom= \"([^\"]*)\", dateTo= \"([^\"]*)\"$")
	public void createReportTemplate_dateFrom_dateTo(String dateFrom, String dateTo) {
		HashMap resDatePeriod = new HashMap();
		long inMs24hrs = 24* 60*60 * 1000;
		long randDaysX = (2 + (int) (Math.random() * 100));
		long randDaysY = (2 + (int) (Math.random() * randDaysX-1));
		 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		 df.setTimeZone(TimeZone.getTimeZone("UTC"));
		 long fromInMs = 0;
		long currTimeInMs = System.currentTimeMillis();
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});
		report = new Report( reportType);
		if (!dateFrom.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}")) {
			// not a date
			// текущее время - 2 минуты
			if (dateFrom.matches("текущее время - \\d{1,2}[ ]{0,2}минут.*")) {
				Matcher matcher = Pattern.compile(".* - (\\d{1,2}) мин.*").matcher(dateFrom);
				if (matcher.matches()) {
					long mins = Long.parseLong(matcher.group(1));
					fromInMs = currTimeInMs - (mins * 60 * 1000);
					resDatePeriod.put("dateTimeFrom", df.format(fromInMs));
					// resDatePeriod.put("dateTimeTo","2017-08-31 23:59:59");
				}
			}
			// текущее время - 1 сутки
			if (dateFrom.matches("текущее время - \\d{1,2}[ ]{0,2}сут.*")) {
				Matcher matcher = Pattern.compile(".* - (\\d{1,2}) сут.*").matcher(dateFrom);
				if (matcher.matches()) {
					long days = Long.parseLong(matcher.group(1));
					fromInMs = currTimeInMs - (days * inMs24hrs);
					resDatePeriod.put("dateTimeFrom", df.format(fromInMs));
					// resDatePeriod.put("dateTimeTo","2017-08-31 23:59:59");
				}
			}
			// текущее время - Х суток
			if (dateFrom.matches("текущее время - Х[ ]{0,2}сут.*")) {
				fromInMs = currTimeInMs - (randDaysX * inMs24hrs);
				resDatePeriod.put("dateTimeFrom", df.format(fromInMs));
			}

		}
		if (!dateTo.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}")) {
			//From + 1 минута 
			if (dateTo.matches("From \\+ \\d{1,2}[ ]{0,2}минут.*")) {
				Matcher matcher = Pattern.compile(".* \\+ (\\d{1,2}) мин.*").matcher(dateTo);
				if (matcher.matches()) {
					long mins = Long.parseLong(matcher.group(1));
					long toInMs = fromInMs + (mins * 60 * 1000);
					resDatePeriod.put("dateTimeTo", df.format(toInMs));
				}
			}
			//From + X,  Х(00:01 ; 23:59] 
			if (dateTo.matches("From \\+ X.*")) {
				long plusSecs = (1 + (int) (Math.random() * inMs24hrs-1));
					long toInMs = fromInMs + (plusSecs);
					resDatePeriod.put("dateTimeTo", df.format(toInMs));
				
			}
			// текущее время - У суток, Х>Y 
			if (dateTo.matches("текущее время - У[ ]{0,2}сут.*")) {
				long toInMs = currTimeInMs - (randDaysY * inMs24hrs);
				resDatePeriod.put("dateTimeTo", df.format(toInMs));
			}
			
		}
		//set recived params
		report.setDateFrom(resDatePeriod.get("dateTimeFrom").toString());
		report.setDateTo(resDatePeriod.get("dateTimeTo").toString());
		
		//update url
		report.putUrlParameters("dateFrom", report.getDateFrom());
		report.putUrlParameters("dateTo", report.getDateTo());
		
		//end
		JSONObject createdReportTemplate = (JSONObject) report.requestReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}	
	
	@Then("^Отправить запрос создания шаблона отчета$")
	public void createReportTemplate() {
		log.debug("Start>> report template step");
		String reportType =pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle",
											"ReportOnFuel" ,"ReportOnVisitsToGeozones", "ReportOnLossOfCommunication"}); 
		report = new Report(reportType);
		JSONObject createdReportTemplate = (JSONObject) report.requestReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	
	@Then("^создать \"([^\"]*)\" шаблонов отчетов$")
	public void createSeveralReportTemplates(int totlaNoOfTemplates) {
		for(int i=0;i<totlaNoOfTemplates; i++){
			createReportTemplate();
		}
	}
}



