package reports_templates;

import java.util.ArrayList;

import org.json.simple.JSONArray;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Report;

import cucumber.api.java.en.Then;

public class GET_ReportTemplates extends AbstractClass{
	public Report report; 
	
	@Then("^Отправить запрос получения списка отчетов$")
	public void getReportTemplatesList(){
		log.debug("Start>> getReportTemplateList step");
		JSONArray listOfTamplates = (JSONArray) new Report().requestReportTemplatesList();
		
		if (SharedContainer.container.containsKey("reportTemplates")){
			ArrayList<Report> templFromPrevStep = (ArrayList<Report>) SharedContainer.getObjectsFromSharedContainer("all", "reportTemplates");
			for(Report template : templFromPrevStep){
				try {
					assertContains("API returned created template",ReplyToJSON.extractListPathFromJson(listOfTamplates,"$..id").toString(),
									template.getReportTemplateId(),"созданный ранее шаблон отчета отсутствует в ответе API");
				} catch (Exception e) {
					log.error(ERROR,e);
				}
			}
		}
		setToAllureChechkedFields();
		
	}


}



