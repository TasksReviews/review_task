package reports_templates;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Geozone;
import com.t1.core.api.Group;
import com.t1.core.api.Report;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class PUT_ReportTemplates extends AbstractClass{
	public Report report; 
	
	@Then("^запросить изменение шаблона отчета с параметрами из таблицы: \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void updateReportTemplate(String reportType, String reportCriteriaParams, String resourceType, final String resourcesFilterInclude, String dateFrom, String dateTo, 
						  String graphTimeUnit, String dataTimeUnit, final String geozoneIds,  Boolean isDeepDetailed){
		log.debug("Start>> report template step");
		String periodicity ="Manually";
		String detalizationType ="";
		if(isDeepDetailed){
			detalizationType ="DeepDetailed";
		} else {
			detalizationType ="Detailed";
		}
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		String name="JFW_repTemplname"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		String description="JFW_repTemplDescr"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		report = new Report( reportType, name, description, reportCriteriaParams, resourceType, resourcesFilterInclude, dateFrom, dateTo, periodicity, geozoneIds,  detalizationType);
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		JSONObject updatedReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}

	@Then("^запросить изменение шаблона отчета с минимальным количеством полей: \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void updateReportTemplate(String reportType, String reportCriteriaParams, String resourceType, final String resourcesFilterInclude,  
						  String graphTimeUnit, String dataTimeUnit,final String geozoneIds){
		log.debug("Start>> report template step");
		String periodicity ="Manually";
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		String geozoneIdsParam= null;
		String name="JFW_repTemplname"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		String description="JFW_repTemplDescr"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		if(geozoneIds!=""){
			geozoneIdsParam = geozoneIds;
		}
		report = new Report( reportType, name, description, reportCriteriaParams, resourceType, resourcesFilterInclude, null, null, periodicity, geozoneIdsParam,  "Consolidated");
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		JSONObject updatedReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}


	@Then("^Отправить запрос редактирования шаблона отчета с reportType = \"([^\"]*)\", reportCriteria = \"([^\"]*)\"$")
	public void updateReportTemplate(String reportType, final String reportCriteriaParams){
		log.debug("Start>> report template step");
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		String geozoneIdsParam= null;
		String name="JFW_repTemplname"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		String description="JFW_repTemplDescr"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
	//	if(geozoneIds!=""){
	//		geozoneIdsParam = geozoneIds;
	//	}
		report = new Report( reportType);
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		//set recived reportCriteria instead of random one
		report.setReportCriteria((new ArrayList(){{add(reportCriteriaParams);}}));
		JSONObject contentConfig = (JSONObject) report.getUrlParameters().get("contentConfig");
		contentConfig.put("reportCriteria", new ArrayList(){{add(reportCriteriaParams);}});
		report.putUrlParameters("contentConfig", contentConfig);
		//end
		JSONObject updatedReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	
	
	@Then("^Отправить запрос редактирования шаблона отчета с graphTimeUnit= \"([^\"]*)\", dataTimeUnit= \"([^\"]*)\", isDeepDetailed= \"([^\"]*)\",период \"([^\"]*)\"$")
	public void updateReportTemplate(String graphTimeUnit, String dataTimeUnit, Boolean isDeepDetailed, String datePeriod){
		log.debug("Start>> report template step");
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});
		Map<String, String> resDatePeriod = getDateTimePeriodInUTC(datePeriod);
		report = new Report( reportType);
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		//set recived params
		report.setDateFrom(resDatePeriod.get("dateTimeFrom").toString());
		report.setDateTo(resDatePeriod.get("dateTimeTo").toString());
//		report.setDataTimeUnit(dataTimeUnit);
//		report.setGraphTimeUnit(graphTimeUnit);
//		report.setIsDeepDetailed(isDeepDetailed);
		//update url
		report.putUrlParameters("dateFrom", report.getDateFrom());
		report.putUrlParameters("dateTo", report.getDateTo());
		report.putUrlParameters("graphTimeUnit", graphTimeUnit);
		report.putUrlParameters("dataTimeUnit", dataTimeUnit);
		report.putUrlParameters("isDeepDetailed", isDeepDetailed);
		//end
		JSONObject updatedReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	
	
	
	@Then("^Отправить запрос редактирования шаблона отчета с resourceType= \"([^\"]*)\", resourcesFilter\\.include= (.*)$")
	public void updateReportTemplate_resourceType_resourcesFilter_include(String resourceType, ArrayList<String> inputResourcesFilterInclude) {
		log.debug("Start>> report template step");
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		ArrayList<String> resourcesFilterInclude = new ArrayList();
		int indexNumber=-1;
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});

		report = new Report( reportType);
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		//edit repor params according to test input params
		report.setResourceType(resourceType);
		ArrayList<Employee> employeesFromPrevStep = (ArrayList<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
		ArrayList<Vehicle> vehiclesFromPrevStep = (ArrayList<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all","vehicles");
		for(String resource: inputResourcesFilterInclude){
			resource= resource.replace("\"", "").replace("[", "").replace("]", "");
			if(resource.contains("ТС")){
				 indexNumber= Integer.parseInt(resource.substring(resource.indexOf("ТС")+2,resource.length()));
				 resourcesFilterInclude.add(vehiclesFromPrevStep.get(indexNumber-1).getVehicleId());
			}else if(resource.contains("Сотрудник")){
				indexNumber= Integer.parseInt(resource.substring(resource.indexOf("трудник")+7,resource.length()));
				 resourcesFilterInclude.add(employeesFromPrevStep.get(indexNumber-1).getEmployeeId());
			}
		}
		report.setResourcesFilterInclude(resourcesFilterInclude);
		report.putUrlParameters("ResourceType", report.getResourceType());
		JSONObject urlResourcesFilter = (JSONObject) report.getUrlParameters().get("resourcesFilter");
		urlResourcesFilter.put("include", resourcesFilterInclude);
		report.putUrlParameters("resourcesFilter", urlResourcesFilter);
		
		//end
				JSONObject updatedReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
				SharedContainer.setContainer("reportTemplates", report);
	}
	
	//qa-2912
	@Then("^Отправить запрос редактирования шаблона отчета с resourcesFilter\\.groups = \"([^\"]*)\", resourcesFilter\\.include = \"([^\"]*)\", resourcesFilter\\.exclude = \"([^\"]*)\"$")
	public void createReportTemplate_resourcesFilter_groups_resourcesFilter_include(int resourcesFilterGroups, int inputResourcesFilterInclude, int inputResourcesFilterExclude) {
		log.debug("Start>> report template step");
		ArrayList<String> resourcesFilterInclude = new ArrayList();
		ArrayList<String> resourcesFilterExclude = new ArrayList();
		int indexNumber=-1;
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});
		ArrayList<String> groupsIds = new ArrayList<String>();
		report = new Report( reportType);
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		//edit repor params according to test input params
		
		ArrayList<String> objectsInGroups = new ArrayList<String>();
		ArrayList<Group> groupsFromPrevStep = (ArrayList<Group>) SharedContainer.getObjectsFromSharedContainer(resourcesFilterGroups,"groups");
		for(Group group : groupsFromPrevStep){
			groupsIds.add(group.getGroupId());
			objectsInGroups.addAll( group.getObjects());
		}
		ArrayList<Employee> employeesFromPrevStep = (ArrayList<Employee>) SharedContainer.getObjectsFromSharedContainer("any","employees");
		ArrayList<Vehicle> vehiclesFromPrevStep = (ArrayList<Vehicle>) SharedContainer.getObjectsFromSharedContainer("any","vehicles");

		if(inputResourcesFilterInclude >0) {
			for (Employee empl : employeesFromPrevStep ){
				if(!objectsInGroups.contains(empl.getEmployeeId())){
					resourcesFilterInclude.add(empl.getEmployeeId());
				}
			}
			
			for (Vehicle veh : vehiclesFromPrevStep ){
				if(!objectsInGroups.contains(veh.getVehicleId())){
					resourcesFilterInclude.add(veh.getVehicleId());
				}
			}
		}

		if(inputResourcesFilterExclude>0){
			for(int i=0; inputResourcesFilterExclude>0; i++, inputResourcesFilterExclude--){
					int rndIndex = 0 + (int) (Math.random() * (objectsInGroups.size()-1-i));
					resourcesFilterExclude.add(objectsInGroups.get( rndIndex));
			}
		}

		report.setResourcesFilterGroups(groupsIds);
		report.setResourcesFilterInclude(resourcesFilterInclude);
		report.setResourcesFilterExclude(resourcesFilterExclude);
		report.putUrlParameters("ResourceType", report.getResourceType());
		JSONObject urlResourcesFilter = (JSONObject) report.getUrlParameters().get("resourcesFilter");
		urlResourcesFilter.put("groups", groupsIds);
		urlResourcesFilter.put("include", resourcesFilterInclude);
		urlResourcesFilter.put("exclude", resourcesFilterExclude);
		report.putUrlParameters("resourcesFilter", urlResourcesFilter);
		
		//end
				JSONObject createdReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
				SharedContainer.setContainer("reportTemplates", report);
	}
	
	@Then("^Отправить запрос редактирования шаблона отчета с reportType= \"([^\"]*)\", geozoneIds = (.*)$")
	public void updateReportTemplate_reportType_geozoneIds(String reportType, ArrayList<String>  inputGeozoneIds) throws Throwable {
		log.debug("Start>> report template step");
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		int indexNumber=-1;
		ArrayList<String> geozoneIds = new ArrayList();
		List<Geozone> geozonesList =  SharedContainer.getFromContainer("geozones");
		String geozoneIdsParam= null;
		String name="JFW_repTemplname"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		String description="JFW_repTemplDescr"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		
		for(String geozoneReq: inputGeozoneIds){
			geozoneReq= geozoneReq.replace("\"", "").replace("[", "").replace("]", "");
			if(geozoneReq.contains("Геозона")){
				 indexNumber= Integer.parseInt(geozoneReq.substring(geozoneReq.indexOf("Геозона")+7,geozoneReq.length()));
				 geozoneIds.add(geozonesList.get(indexNumber-1).getGeozoneId());
			}
		}

		report = new Report( reportType);
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		report.setGeoZoneIds(geozoneIds);
		report.putUrlParameters("geozoneIds", report.getGeoZoneIds());
		JSONObject updatedReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	
	
	@Then("^Отправить запрос редактирования шаблона отчета с detalization = \"([^\"]*)\", resourcesFilter\\.include: ТС\"([^\"]*)\",сотрудников\"([^\"]*)\",группа1:ТС\"([^\"]*)\",сотрудников\"([^\"]*)\", группа2:ТС\"([^\"]*)\",сотрудников\"([^\"]*)\", за период = \"([^\"]*)\", с periodicity = \"([^\"]*)\"$")
	public void updateReportTemplate_Detalization_resourcesFilter_include_period(String detalization, int veh,int empl, int gr1Veh,int gr1Empl,int gr2Veh,int gr2Empl, String period, String periodicity)  {
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		Map<String, String> resDatePeriod = new HashMap();
		ArrayList<String> resourcesFilterInclude = new ArrayList();
		ArrayList<Group> groupsFromPrevStep = new ArrayList<Group>();
//		assertTrue(graphTimeUnit.equals("Day"), "graphTimeUnit "+graphTimeUnit+" не был реализован!");
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});
		report = new Report( reportType);
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		if(period.contains("час")){
		resDatePeriod = getDateTimePeriodInUTC("час");
		
	}
	if(period.contains("календарные сутки")){
		resDatePeriod = getDateTimePeriodInUTC("календарные сутки");
	}
	if(period.matches("\\d{1,2} дня$")){
		resDatePeriod = getDateTimePeriodInUTC(period);
	}
	
	if(period.matches("\\d{1,2}[ ]{0,2}день, в пределах месяца.*")){
		resDatePeriod.put("dateTimeFrom","2017-08-01 00:00:00");//1.08 - 31.08
		resDatePeriod.put("dateTimeTo","2017-08-31 23:59:59");
	}
	if(gr1Veh>0 || gr2Veh>0 || veh>0){
		report.setResourceType("Vehicles");
	} else {
		report.setResourceType("Employees");
	}
		
	//prepare resourcesFilter.include
		if (veh-(gr1Veh+gr2Veh) > 0) {//how to distinguish what veh should be used if remains any? :(
			ArrayList<Vehicle> vehiclesFromPrevStep = (ArrayList<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
			for (int i = 0; i < veh; i++) {
				resourcesFilterInclude.add(vehiclesFromPrevStep.get(i).getVehicleId());
			}
		}

		if (empl-(gr1Empl+gr2Empl) > 0) {
			ArrayList<Employee> employeesFromPrevStep = (ArrayList<Employee>) SharedContainer.getObjectsFromSharedContainer("all", "employees");
			for (int i = 0; i < empl; i++) {
				resourcesFilterInclude.add(employeesFromPrevStep.get(i).getEmployeeId());
			}
		}
	if (gr1Veh>0 || gr2Veh>0 ||gr1Empl >0 ||gr2Empl >0){
		 groupsFromPrevStep = (ArrayList<Group>) SharedContainer.getObjectsFromSharedContainer("all","groups");
	
	if(gr1Veh >0 || gr1Empl >0){
	 ArrayList<HashMap<String, String>> group1resources = ((Group) groupsFromPrevStep.get(0)).getResources();
	 for(HashMap<String, String> resource : group1resources){
		 if(resource.get("resourceType").equals("Vehicle") && gr1Veh >0){
			 resourcesFilterInclude.add(resource.get("id"));
			 gr1Veh--;
		 }
		 if(resource.get("resourceType").equals("Employee") && gr1Empl >0){
			 resourcesFilterInclude.add(resource.get("id"));
			 gr1Empl--;
		 }
	 }
	 
	}
	if(gr2Veh >0 || gr2Empl >0){
		 ArrayList<HashMap<String, String>> group2resources = ((Group) groupsFromPrevStep.get(1)).getResources();
		 for(HashMap<String, String> resource : group2resources){
			 if(resource.get("resourceType").equals("Vehicle") && gr2Veh >0){
				 resourcesFilterInclude.add(resource.get("id"));
				 gr2Veh--;
			 }
			 if(resource.get("resourceType").equals("Employee") && gr2Empl >0){
				 resourcesFilterInclude.add(resource.get("id"));
				 gr2Empl--;
			 }
		 }
		}
	}
	//set recived params
	report.setDateFrom(resDatePeriod.get("dateTimeFrom").toString());
	report.setDateTo(resDatePeriod.get("dateTimeTo").toString());
	report.setDetalization(report.detalizationType(detalization));
	report.setPeriodicity(periodicity);

	report.setResourcesFilterInclude(resourcesFilterInclude);
	
	//update url
	report.putUrlParameters("dateFrom", report.getDateFrom());
	report.putUrlParameters("dateTo", report.getDateTo());
	report.putUrlParameters("detalization", detalization);
	report.putUrlParameters("periodicity", periodicity);
	
	
	report.putUrlParameters("ResourceType", report.getResourceType());
	JSONObject urlResourcesFilter = (JSONObject) report.getUrlParameters().get("resourcesFilter");
	urlResourcesFilter.put("include", resourcesFilterInclude);
	report.putUrlParameters("resourcesFilter", urlResourcesFilter);
	
	//end
			JSONObject updatedReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
			SharedContainer.setContainer("reportTemplates", report);
	
	}
	
	
	@Then("^Отправить запрос редактирования шаблона отчета с dateFrom= \"([^\"]*)\", dateTo= \"([^\"]*)\"$")
	public void updateReportTemplate_dateFrom_dateTo(String dateFrom, String dateTo) {
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		HashMap resDatePeriod = new HashMap();
		long inMs24hrs = 24* 60*60 * 1000;
		long randDaysX = (2 + (int) (Math.random() * 100));
		long randDaysY = (2 + (int) (Math.random() * randDaysX-1));
		 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		 df.setTimeZone(TimeZone.getTimeZone("UTC"));
		 long fromInMs = 0;
		long currTimeInMs = System.currentTimeMillis();
		String reportType = pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle","ReportOnFuel","ReportOnLossOfCommunication"});
		report = new Report( reportType);
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		if (!dateFrom.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}")) {
			// not a date
			// текущее время - 2 минуты
			if (dateFrom.matches("текущее время - \\d{1,2}[ ]{0,2}минут.*")) {
				Matcher matcher = Pattern.compile(".* - (\\d{1,2}) мин.*").matcher(dateFrom);
				if (matcher.matches()) {
					long mins = Long.parseLong(matcher.group(1));
					fromInMs = currTimeInMs - (mins * 60 * 1000);
					resDatePeriod.put("dateTimeFrom", df.format(fromInMs));
					// resDatePeriod.put("dateTimeTo","2017-08-31 23:59:59");
				}
			}
			// текущее время - 1 сутки
			if (dateFrom.matches("текущее время - \\d{1,2}[ ]{0,2}сут.*")) {
				Matcher matcher = Pattern.compile(".* - (\\d{1,2}) сут.*").matcher(dateFrom);
				if (matcher.matches()) {
					long days = Long.parseLong(matcher.group(1));
					fromInMs = currTimeInMs - (days * inMs24hrs);
					resDatePeriod.put("dateTimeFrom", df.format(fromInMs));
					// resDatePeriod.put("dateTimeTo","2017-08-31 23:59:59");
				}
			}
			// текущее время - Х суток
			if (dateFrom.matches("текущее время - Х[ ]{0,2}сут.*")) {
				fromInMs = currTimeInMs - (randDaysX * inMs24hrs);
				resDatePeriod.put("dateTimeFrom", df.format(fromInMs));
			}

		}
		if (!dateTo.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}")) {
			//From + 1 минута 
			if (dateTo.matches("From \\+ \\d{1,2}[ ]{0,2}минут.*")) {
				Matcher matcher = Pattern.compile(".* \\+ (\\d{1,2}) мин.*").matcher(dateTo);
				if (matcher.matches()) {
					long mins = Long.parseLong(matcher.group(1));
					long toInMs = fromInMs + (mins * 60 * 1000);
					resDatePeriod.put("dateTimeTo", df.format(toInMs));
				}
			}
			//From + X,  Х(00:01 ; 23:59] 
			if (dateTo.matches("From \\+ X.*")) {
				long plusSecs = (1 + (int) (Math.random() * inMs24hrs-1));
					long toInMs = fromInMs + (plusSecs);
					resDatePeriod.put("dateTimeTo", df.format(toInMs));
				
			}
			// текущее время - У суток, Х>Y 
			if (dateTo.matches("текущее время - У[ ]{0,2}сут.*")) {
				long toInMs = currTimeInMs - (randDaysY * inMs24hrs);
				resDatePeriod.put("dateTimeTo", df.format(toInMs));
			}
			
		}
		//set recived params
		report.setDateFrom(resDatePeriod.get("dateTimeFrom").toString());
		report.setDateTo(resDatePeriod.get("dateTimeTo").toString());
		
		//update url
		report.putUrlParameters("dateFrom", report.getDateFrom());
		report.putUrlParameters("dateTo", report.getDateTo());
		
		//end
		JSONObject updatedReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}	
	
	@Then("^Отправить запрос редактирования шаблона отчета$")
	public void updateReportTemplate() {
		log.debug("Start>> report template step");
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		String reportType =pickRandomFromList(	new String[]{"SummaryReport","TravelReport","ReportOnViolations","ReportOnDriveStyle",
											"ReportOnFuel" ,"ReportOnVisitsToGeozones", "ReportOnLossOfCommunication"}); 
		report = new Report(reportType);
		report.setReportTemplateId(reportTemplateFromPrevStep.getReportTemplateId());
		JSONObject updatedReportTemplate = (JSONObject) report.requestUpdateReportTemplate();
		SharedContainer.setContainer("reportTemplates", report);
	}
	
	@Then("^отредактировать \"([^\"]*)\" шаблонов отчетов$")
	public void updateSeveralReportTemplates(int totlaNoOfTemplates) {
		for(int i=0;i<totlaNoOfTemplates; i++){
			updateReportTemplate();
		}
	}
	
	@Then("^Отправить запрос редактирования шаблона отчета с reportType= \"([^\"]*)\", shiftList = \"([^\"]*)\", onlyFullShifts = \"([^\"]*)\"$")
	public void createReportTemplate_reportType_shiftList_onlyFullShifts(String reportType, List shiftListReq, boolean onlyFullShifts) {
		log.debug("Start>> edit report template step");
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
		String name="JFW_repTemplname"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		String description="JFW_repTemplDescr"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern"));
		
		//set received shiftList instead of existing one
		assertTrue (shiftListReq.size() <6, "тест расчитан на 5 смен +1 стандартная");

		reportTemplateFromPrevStep.getShiftsSettings().put("onlyFullShifts", onlyFullShifts);
		List shiftList = new ArrayList();
		for (String shift : (List<String>) shiftListReq){
			if(shift.contains("пользовательская")){
				int starthour =18; //6 shifts possible
				JSONObject shiftFields = new JSONObject();
				shiftFields.put("timeFrom", starthour+shiftList.size()+":01");
				shiftFields.put("timeTo", starthour+shiftList.size()+1+":00");
				shiftFields.put("name", "Смена "+(shiftList.size()+2));
				shiftList.add(shiftFields);

			} else if(shift.contains("дефолтная")){
				if(((List)toJson(reportTemplateFromPrevStep.getReportVals().get("shiftsSettings")).get("shiftList")).size()>0) {
					shiftList.add( ((List)toJson(reportTemplateFromPrevStep.getReportVals().get("shiftsSettings")).get("shiftList")).get(0));
				} else {
					JSONObject shiftFields = new JSONObject();
					shiftFields.put("timeFrom", "9:00");
					shiftFields.put("timeTo", "18:00");
					shiftFields.put("name", "Смена 1");
					shiftList.add(shiftFields);
				}
			} else  if(shift.contains("[]")){
				shiftList = new ArrayList();
			} else {
				assertTrue(false, "unsupported type of shift! "+shift);
			}
		}
		reportTemplateFromPrevStep.getShiftsSettings().put("shiftList", shiftList);
		reportTemplateFromPrevStep.putUrlParameters("shiftsSettings", reportTemplateFromPrevStep.getShiftsSettings());
		//end
		JSONObject createdReportTemplate = (JSONObject) reportTemplateFromPrevStep.requestUpdateReportTemplate();
		reportTemplateFromPrevStep.updateInSharedContainer();
	}
	
	@Then("^Отправить запрос редактирования шаблона отчета с reportType = \"([^\"]*)\", periodicity = \"([^\"]*)\", dateFrom = \"([^\"]*)\", dateTo = \"([^\"]*)\"$")
	public void updateReportTemplateByTypeAndPeriodicity(String reportType, String periodicity, String dateFrom, String dateTo){
			log.debug("Start>> report template update step");
			Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");
			//сегодня 00:00|сегодня 23:59
			if(dateFrom.contains("вчера 00:00") && dateTo.contains("вчера 23:59")){
				//getDateTimePeriod (dateTimePeriod, TimeZone.getTimeZone("UTC"),"",false)
				Map<String, String> resDatePeriod = getDateTimePeriod("вчера",TimeZone.getTimeZone("UTC"),standartTimeFormatNoSpace,false);
				dateFrom=resDatePeriod.get("dateTimeFrom")+"+03:00";
				dateTo=resDatePeriod.get("dateTimeTo")+"+03:00";
			}

			reportTemplateFromPrevStep.setPeriodicity(periodicity);
			reportTemplateFromPrevStep.putUrlParameters("periodicity",periodicity);

			reportTemplateFromPrevStep.setDateFrom(dateFrom);
			reportTemplateFromPrevStep.putUrlParameters("dateFrom",dateFrom);
			reportTemplateFromPrevStep.setDateTo( dateTo);
			reportTemplateFromPrevStep.putUrlParameters("dateTo",dateTo);
			reportTemplateFromPrevStep.requestUpdateReportTemplate();
			reportTemplateFromPrevStep.updateInSharedContainer();
		}
	
	@Then("^Отправить запрос редактирования шаблона отчета с reportType = \"([^\"]*)\", detalization = \"([^\"]*)\"$")
	public void createReportTemplateByTypeAndPeriodicity(String reportType, String detalization){
		log.debug("Start>> report template step");
		Report reportTemplateFromPrevStep = (Report) SharedContainer.getLastObjectFromSharedContainer("reportTemplates");

		reportTemplateFromPrevStep.setDetalization(detalization);
		reportTemplateFromPrevStep.putUrlParameters("detalization",detalization);

		reportTemplateFromPrevStep.requestUpdateReportTemplate();
		reportTemplateFromPrevStep.updateInSharedContainer();

	}
}



