package webapi;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.t1.core.AbstractClass;
import com.t1.core.selenium.T1LoginPage;
import com.t1.core.selenium.T1MonitoringPage;


/**
 * Created by IntelliJ IDEA.
 * User: ab83625
 * Date: 10.11.2010
 * To change this template use File | Settings | File Templates.
 */


public class TestLogin extends AbstractClass {

	WebDriver driver;
	T1LoginPage objLogin;
	T1MonitoringPage objMonitoringPage;
	// String browser = "chrome";
	String setBrowser = "firefox";

	// @BeforeTest
	public void setUp() {
		// Инициализация контекста.
		if (setBrowser.toLowerCase().contains("firefox")) {
		System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
			this.driver = new FirefoxDriver();
		} else if (setBrowser.toLowerCase().contains("chrome")) {
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
			this.driver = new ChromeDriver();
		} else {
			log.error("unsupported browser selected.");
		}

		driver.get("http://map-tone-test.qa.t1-group.ru");
    }

//	@Test(singleThreaded = true, invocationTimeOut = 1000000, groups = { "integration",
//			"GUI" }/*
//					 * , dependsOnGroups = { "functest" }
//					 */, priority = 2)

	public void test_Monitoring_Page_Displayed() {
		long timeoutInSeconds = 10;
		try {
		setUp();

		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);

		// Create Login Page object
		objLogin = new T1LoginPage(driver);
		// Verify login page title
		String loginPageTitle = objLogin.getLoginTitle();
		System.out.println("loginPageTitle>>>>>>>>>>>>>>>>>>" + loginPageTitle);
		assertTrue("loginPageTitle contains авторизация",loginPageTitle.toLowerCase().contains("авторизация"));
		// login to application
		objLogin.loginToT1("alex", "alex");
		// go the next page
		objMonitoringPage = new T1MonitoringPage(driver);
		// Verify home page

		wait.until(ExpectedConditions.elementToBeClickable(objMonitoringPage.monitoringPageLeftBar));

		System.out.println("============monitoring? = "
				+ objMonitoringPage.getMonitoringPageLeftBarText().toLowerCase());
		assertTrue("loginPageTitle contains мониторинг",objMonitoringPage.getMonitoringPageLeftBarText().toLowerCase().contains("мониторинг"));


		objMonitoringPage.clickOnObjectListBarToOpen();
		objMonitoringPage.clickOnMonitoringObjectsSwithcer();
		wait.until(ExpectedConditions.visibilityOf(objMonitoringPage.monitoringObjectListBarSearch));
		objMonitoringPage.setSearchingObject("demonstration bus");
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		Thread.sleep(2000);
		assertTrue("страница мониторинг содержит DemoBus",objMonitoringPage.getObjectFoundDemoBusText().contains("1"));

		objMonitoringPage.clickOnMonitoringObjectFoundDemoGroup();
		objMonitoringPage.clickOnMonitoringZoomOut();
		objMonitoringPage.clickOnMonitoringZoomOut();
		Thread.sleep(200);
		objMonitoringPage.clickOnMonitoringZoomOut();
		objMonitoringPage.clickOnMonitoringZoomOut();
		objMonitoringPage.clickOnMonitoringZoomOut();
		objMonitoringPage.clickOnObjectListBarToClose();

			Thread.sleep(10000);
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		closeBrowser();
		} catch (Exception e) {
			log.error(e);
		}
	}

	// @AfterTest
	public void closeBrowser() throws InterruptedException {
		if (driver != null) {

			String base = driver.getWindowHandle();

			Set<String> set = driver.getWindowHandles();

			if (set.size() > 1) {
				// set.remove(base);
				// assert set.size() == 1;

				driver.switchTo().window(set.toArray(new String[0])[0].toString());

			} else {
				driver.switchTo().window(base);
			}

			driver.close();
			// driver.switchTo().window(base);

			// Thread.sleep(2000);
			// driver.close();
			// Thread.sleep(2000);
			// driver.quit();
		} else {
			log.error("driver empty!");
		}
	}
}