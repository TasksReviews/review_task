package webapi;


import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;
import org.junit.After;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.Scenario;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Title;


public class DemoTest extends AbstractClass{
	String token;
	String accountId;
	private Scenario scenario;

	@SuppressWarnings("rawtypes")

//	
//	@After
//	public void after(Scenario scenario) {
//	    this.scenario = scenario;
//	    log.info("Execution for test : "+scenario.getName()+"has been completed.");
//	}
		  
	/*public void CreateAccountBeforeTest() throws Exception {
	  register account
	 AccountRegistration account = new AccountRegistration();
	 List<LinkedHashMap> accountInfoMongo = new
	 AccountRegistration().registerAccount();
	  TODO: we should use data from registration request not from mongodb
	 this.accountId = ReplyToJSON.extractPathFromJson(accountInfoMongo.get(0),
	 "$._id.$oid");
	 String phone = ReplyToJSON.extractPathFromJson(accountInfoMongo.get(0),
	 "$.Contacts[1].Value");
	
	  validate db contains we requested
	 account.checkRegistredAccount(accountInfoMongo);
	
	  get token
	 this.token = new AccountAutorization().confirmRegistration(accountId,
	 phone);
	 log.info("account was created: " + accountId);
	 }
	*/
	// @DataProvider(name = "demoTestData")
	// public Iterator<Object[]> provider() throws InterruptedException {
	// return getInputDataDefault();
	// }

//	@DataProvider(name = "demoTestData")
//	public Object[][] getTestData() throws InterruptedException {
//		return getInputDataStringArrayTestData();
//	}
	@Title("Тест: регистрация аккаунта")
	@Description("Описание: создание аккаунта, получение тс, удаление аккаунта")
	
//	@Test(singleThreaded = true, invocationCount = 1, invocationTimeOut = 1000000, groups = { "functest",
//			"integration" }, dataProvider = "demoTestData")

	public void mydemoTest(String[] inputttt) {
		try {
			// register account
			StringBuilder resultBuilder = new StringBuilder();
			//resultBuilder.toString().equals(null)
			//why we do init here? must be made into AccountRegistrationAuth
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
			AccountRegistrationAuth account = new AccountRegistrationAuth(initMap,false); 
			List<JSONObject> accountInfo = account.registerAccount();
			// TODO: we should use data from registration request not from
			// mongodb
			this.accountId = ReplyToJSON.extractPathFromJson(accountInfo.get(0), "$._id.$oid");
			String phone = ReplyToJSON.extractPathFromJson(accountInfo.get(0), "$.Contacts[1].Value");

			//java.io.File
//			/javax.xml.parsers.DocumentBuilderFactory
//			javax.xml.parsers.DocumentBuilder
//			org.w3c.dom.Document
//			File file = new File("userdata.xml");
//			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
//			        .newInstance();
//			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
//			Document document = documentBuilder.parse(file);
//			document.getElementsByTagName("user").getLength();
//	//	ListArray hhh =	document.getDocumentElement().getTagName().item(0);
//			String usr = document.getElementsByTagName("user").item(0).getTextContent();
//			String pwd = document.getElementsByTagName("password").item(0).getTextContent();
			
			
			// validate db contains we requested
		//	 account.checkRegistredAccount(accountInfo);

			// get token
			this.token = "";//new AccountAutorization().confirmRegistration(accountId, phone);
			log.info("account was created: " + accountId);
			// Assert.assertEquals(false, true);

		log.info(">>>>>>>>>get vehicleRes using token of just created user");
			 JSONObject allVehiclesAPI = getAllVehicles(token);
		String firstVehicleFromAPI = allVehiclesAPI.get("name").toString();
		log.info("first vehicle=" + firstVehicleFromAPI);

	
			// delete account
			HashMap deleteReply = account.accountDelete();
			log.info("account was deleted: " + accountId);

	log.info("test end>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		} catch (Exception e) {
			log.error(ERROR,e);
		}
	
	 }
	
	public org.json.simple.JSONObject getAllVehicles(String token) {
		String url = getProperty("host") + "/Vehicle/GetAll";

		try {

			log.debug("token=" + token);
			log.info("get vehicleRes using token of just created user");
			org.json.simple.JSONObject vehicleRes = (JSONObject) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token).get(0);
			log.info("first vehicle=" + vehicleRes.get("name"));
			log.info("vehicleRes=== " + vehicleRes);

			return vehicleRes;

		} catch (Exception e) {
			log.error(ERROR, e);
		}
		return null;

	}
	
//	org.w3c.dom.NodeList
//	org.w3c.dom.NamedNodeMap


	@After
	public void AfterTestMethod() {
		log.info("AFTER test <<<<<<<<<<<<<<<<<<<<<<<<");
	}

	
}
