package webapi;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jongo.Jongo;
import org.jongo.Mapper;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.jongo.marshall.jackson.JacksonMapper;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.t1.core.AbstractClass;

public class Test_Jongo extends AbstractClass {
//	@Test(singleThreaded = true, invocationCount = 1, invocationTimeOut = 1000000, groups = { "integration" })
	public static void jongoTest() {

		Mapper mapper = new JacksonMapper.Builder().build();
		MongoClient mongoClient = null;

		MongoCredential mongoCredential = MongoCredential.createScramSha1Credential("guest", "ToneMainTest",
				"f483hr34otr".toCharArray());

		mongoClient = new MongoClient(new ServerAddress("176.112.216.255", 27017), Arrays.asList(mongoCredential));

		DB db = mongoClient.getDB("ToneMainTest");

		// DB db = new MongoClient().getDB("ToneMainTest");

		Jongo jongo = new Jongo(db);
		MongoCollection friends = jongo.getCollection("accounts");

		MongoCursor<String> cursor = friends.find("{'Login': 'demo'}").as(String.class);
		String one = friends.findOne("{'Login': 'demo'}").as(String.class);
		System.out.println("one=" + one);
		List snapshots = new ArrayList();
        try {
            while (cursor.hasNext()) {
                String dbObject = cursor.next();
                snapshots.add(/*readFromDBObject(*/dbObject);//);
            }
        }
        catch(Exception e){
        	System.out.println("e=" + e);
        }
		System.out.println("all=" + snapshots);


	}

}