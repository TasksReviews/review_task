package webapi;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.t1.core.AbstractClass;

public class SeleniumTest extends AbstractClass {
	// Create a new instance of the Firefox driver
    // Notice that the remainder of the code relies on the interface, 
    // not the implementation.
//	@Test(singleThreaded = true, invocationTimeOut = 1000000, groups = { "GUI" }, dependsOnGroups = {
//			"functest" })
	public void selenium_start() throws InterruptedException{
		//FirefoxDriver
	//	System.setProperty("webdriver.gecko.driver","src/main/resources/geckodriver.exe");
	//	WebDriver driver = new FirefoxDriver();
		//ChromeDriver
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver();

    // And now use this to visit page
    driver.get("http://map-tone-test.qa.t1-group.ru");


    // Find the text input element by its name
    WebElement element = driver.findElement(By.id("username"));
    element.sendKeys("lIkffUWslY");
    element = driver.findElement(By.id("password"));
    element.sendKeys("qazxsw11");

    // Now submit the form. WebDriver will find the form for us from the element
    element = driver.findElement(By.xpath("//div[contains(@class, 'form-group remember')]/button"));
    element.submit();

    // Check the title of the page
    System.out.println("Page title is: " + driver.getTitle());
    
    // Google's search is rendered dynamically with JavaScript.
    // Wait for the page to load, timeout after 10 seconds
    (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
        public Boolean apply(WebDriver d) {
            return d.getTitle().startsWith("TOne");
        }
    });


    System.out.println("Page title is: " + driver.getTitle());

    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	driver.wait(10000);
    //Close the browser
   //  driver.quit();
	}
}
