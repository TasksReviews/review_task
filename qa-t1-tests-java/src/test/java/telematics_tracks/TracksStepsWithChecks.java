package telematics_tracks;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;

import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Then;
import org.json.simple.parser.JSONParser;

public class TracksStepsWithChecks extends AbstractClass {

    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что для индикатора <([^>]*)> получено значение <([^>]*)>$")
    public void checkForParamXReceivedValueY(String parameter, String expectedValue) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        JSONObject indicators = (JSONObject) ((List<JSONObject>) receivedTrack.get("resources")).get(0).get("indicators");
        if (expectedValue.startsWith("\"") && expectedValue.endsWith("\"")) {
            expectedValue = expectedValue.substring(1, expectedValue.length() - 1);
        }
        try {
            assertEquals("API: check resources[0]indicators." + parameter,
                    indicators.get(parameter).toString(), expectedValue, "wrong value for indicators." + parameter + " received.");
        } catch (NullPointerException e) {
            assertTrue(false, "поле resources[0]indicators." + parameter + "не найдено");
        }
        setToAllureChechkedFields();

    }

    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что вернулись (\\d+) Точек$")
    public void проверить_что_вернулись_Точек(int numOfPoints) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        try {
            String s = ReplyToJSON.extractPathFromJson(receivedTrack, "$.resources[0].points");
            JSONObject points = (JSONObject) new JSONParser().parse(s);
            assertEquals("API: check resources[0].points", points.size(), numOfPoints, "wrong value number of points received.");
            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
        }
    }

    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что для точки (\\d+) time = <([^>]*)>$")
    public void проверить_что_для_точки_time_число_текуший_день_время(int pointIndex, String expectedValue) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        Gson gson = new Gson();
        try {
            String s = ReplyToJSON.extractPathFromJson(receivedTrack, "$.resources[0]");
            JsonObject jsonObject = gson.fromJson(s, JsonObject.class);
            JsonObject points = jsonObject.getAsJsonObject("points");
            Object[] objects = points.keySet().toArray();
            String object = (String) objects[pointIndex - 1];
            Date date = new Date((Long.parseLong(object) * 1000) - 10800000);
            String s1 = DateFormatUtils.formatUTC(date, "yyyy-MM-dd HH:mm:ss");
            expectedValue = parseInputTime(expectedValue);
            assertEquals("API: check resources[0].points", s1, expectedValue, "wrong time in point " + pointIndex + " received.");
            setToAllureChechkedFields();

        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
        }
    }

    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что вернулись (\\d+) Ивентов$")
    public void проверить_что_вернулись_Ивентов(int expectedNumOfEvents) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        Gson gson = new Gson();
        int eventsFound = 0;
        try {
            String jsonArray = ReplyToJSON.extractPathFromJson(receivedTrack, "$.resources[0]");
            JsonObject jsonObject = gson.fromJson(jsonArray, JsonObject.class);
            JsonObject points = jsonObject.getAsJsonObject("events");
            for (int i = 0; i < points.size(); i++) {
                eventsFound++;
            }
            assertEquals("API: check resources[0].events", eventsFound, expectedNumOfEvents,
                    "wrong number of events found in reply ");
            setToAllureChechkedFields();
        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
        }
    }

    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что Ивент (\\d+) находится в точке с time = <([^>]*)>$")
    public void проверить_что_Ивент_находится_в_точке_с_time_число_текуший_день_время(int expectedEventIndex, String expectedValue) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        Gson gson = new Gson();
        int currentEventIndex = 1;
        try {
            String s = ReplyToJSON.extractPathFromJson(receivedTrack, "$.resources[0]");
            JsonObject jsonObject = gson.fromJson(s, JsonObject.class);
            JsonObject points = jsonObject.getAsJsonObject("events");
            Object[] objects = points.keySet().toArray();
            String object = (String) objects[expectedEventIndex - 1];
            Date date = new Date((Long.parseLong(object) * 1000) - 10800000);
            String s1 = DateFormatUtils.formatUTC(date, "yyyy-MM-dd HH:mm:ss");
            expectedValue = parseInputTime(expectedValue);
            assertEquals("API: check resources[0].events", s1, expectedValue, "wrong time in point " + expectedEventIndex + " received.");
            currentEventIndex++;
            setToAllureChechkedFields();
        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
        }
    }

    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что для Ивента (\\d+) для параметра <([^>]*)> получено значение <\"([^\"]*)\">$")
    public void проверить_что_для_Ивента_для_параметра_eventType_получено_значение(int expectedEventIndex, String param, String expectedValue) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        int currentEventIndex = 1;
        Gson gson = new Gson();
        try {
            String s = ReplyToJSON.extractPathFromJson(receivedTrack, "$.resources[0]");
            JsonObject jsonObject = gson.fromJson(s, JsonObject.class);
            JsonObject points = jsonObject.getAsJsonObject("events");
            Object[] objects = points.keySet().toArray();
            String object = (String) objects[expectedEventIndex - 1];
            String s1 = points.getAsJsonObject(object).getAsJsonObject().toString();
            JsonObject jsonObject1 = gson.fromJson(s1, JsonObject.class);
            Object[] objects1 = jsonObject1.keySet().toArray();
            JsonObject jsonElement = jsonObject1.getAsJsonObject(String.valueOf(objects1[0]));
            JsonPrimitive asJsonPrimitive = jsonElement.getAsJsonPrimitive(param);
            assertEquals("API: check resources[0].events", asJsonPrimitive.getAsString(), expectedValue,
                    "wrong value in point with event " + expectedEventIndex + " received.");
            currentEventIndex++;
            setToAllureChechkedFields();
        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
        }
    }

    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что вернулись (\\d+) Поездок$")
    public void проверить_что_вернулись_Поездок(int numOfTrips) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        try {
            ArrayList<JSONObject> trips = ReplyToJSON.extractListPathFromJson(receivedTrack, "$.resources[0].trips");
            assertEquals("API: check resources[0].trips", trips.size(), numOfTrips, "wrong value number of trips received.");
            setToAllureChechkedFields();
        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
        }
    }

    //Then Проверить, что для Поездки 1 dateStart = <число = (текуший день - 1); время = "08:50:00">, dateEnd = <число = (текуший день - 1); время = "09:10:00">
    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что для Поездки (\\d+) dateStart = <([^>]*)>, dateEnd = <([^>]*)>$")
    public void checkDateStartAndDateEndForTripX(int tripIndex, String dateStart, String dateEnd) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        int currentTripIndex = 1;
        try {
            List<JSONObject> trips = ReplyToJSON.extractListPathFromJson(receivedTrack, "$.resources[0].trips");
            dateStart = parseInputTime(dateStart);
            dateEnd = parseInputTime(dateEnd);

            assertEquals("API: check resources[0].trips.dateStart", trips.get(tripIndex - 1).get("dateStart"), dateStart, "wrong dateStart value for trip " + tripIndex + " received.");
            assertEquals("API: check resources[0].trips.dateEnd", trips.get(tripIndex - 1).get("dateEnd"), dateEnd, "wrong dateEnd value for trip " + tripIndex + " received.");
            setToAllureChechkedFields();

        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
        }
    }


    //	Then Проверить, что для Поездки 1 dateStart = <число = (текуший день - 1); время = "08:50:00">
    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что для Поездки (\\d+) dateStart = <([^>]*)>$")
    public void checkDateStartForTripX(int tripIndex, String dateStart) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        int currentTripIndex = 1;
        try {
            List<JSONObject> trips = ReplyToJSON.extractListPathFromJson(receivedTrack, "$.resources[0].trips");
            dateStart = parseInputTime(dateStart);
            assertEquals("API: check resources[0].trips.dateStart", trips.get(tripIndex - 1).get("dateStart"), dateStart, "wrong dateStart value for trip " + tripIndex + " received.");
            setToAllureChechkedFields();

        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
        }
    }

    //	Then Проверить, что для Поездки 1 dateEnd = <число = (текуший день - 1); время = "09:10:00">
    @ContinueNextStepsFor({AssertionError.class})
    @Then("^Проверить, что для Поездки (\\d+) dateEnd = <([^>]*)>$")
    public void checkDateEndForTripX(int tripIndex, String dateEnd) {
        JSONObject receivedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
        int currentTripIndex = 1;
        try {
            List<JSONObject> trips = ReplyToJSON.extractListPathFromJson(receivedTrack, "$.resources[0].trips");
            dateEnd = parseInputTime(dateEnd);

            assertEquals("API: check resources[0].trips.dateEnd", trips.get(tripIndex - 1).get("dateEnd"), dateEnd,
                    "wrong dateEnd value for trip " + tripIndex + " received.");
            setToAllureChechkedFields();

        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
        }
    }

    public String parseInputTime(String expectedValue) {
        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
        if (expectedValue.contains("число =") && expectedValue.contains("время =")) {
            Calendar calCur = Calendar.getInstance();
            String[] arr = expectedValue.split(";");
            if (arr[0].contains("текуший день")) {
                int daysToSubstract = 0;
                Pattern pattern = Pattern.compile("(\\d+)");
                Matcher matcher = pattern.matcher(arr[0]);
                if (matcher.find()) {
                    daysToSubstract = Integer.parseInt(matcher.group(1));
                }
//				log.debug("daysToSubstract==" + daysToSubstract);
                calCur.add(Calendar.DATE, -(daysToSubstract));
                calCur.get(Calendar.YEAR);
            }
            if (arr[1].contains("время")) {
                Pattern pattern = Pattern.compile("\"(.*)\"");
                Matcher matcher = pattern.matcher(arr[1]);
                if (matcher.find()) {
                    expectedValue = dfDate.format(calCur.getTime()) + " " + matcher.group(1);
                }
            }
        }
        return expectedValue;
    }
}
