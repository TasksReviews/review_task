package telematics_tracks;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.TelematicsTrack;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class POST_TelematicsTracks extends AbstractClass {
    TelematicsTrack track = null;

    @Then("^Отправить запрос получения треков для ТС с events = <\"([^\"]*)\">, за период с = <([^>]*)>, по = <([^>]*)>$")
    public void requestTelematicsTracksForVehicleForPeriod(String eventsType, List fromDate, List tillDate) {
        requestTrackForObjectByCategoryByTypeForPeriod("Vehicle", eventsType, fromDate, tillDate);
    }

    @Then("^Отправить запрос получения треков для Сотрудника с events = <\"([^\"]*)\">, за период с = <([^>]*)>, по = <([^>]*)>$")
    public void requestTelematicsTracksForEmployeeForPeriod(String eventsType, List fromDate, List tillDate) {
        requestTrackForObjectByCategoryByTypeForPeriod("Employee", eventsType, fromDate, tillDate);
    }

    public void requestTrackForObjectByCategoryByTypeForPeriod(String objectType, String eventsTypes, List fromDate, List tillDate) {
        log.debug("Start>> track test");
        if (SharedContainer.container.keySet().contains("reportResult")) {
            SharedContainer.removeFromContainer("reportResult");
        }
        List<String> resourceIds = new ArrayList<String>();
        List<String> events = new ArrayList<String>();

        String reportCriteria = "";
        String objectId = "";
        String resourceType = "";
        if (objectType.equals("Vehicle")) {
            Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
            resourceIds.add(vehicleFromPrevStep.getVehicleId());
        } else if (objectType.equals("Employee")) {
            Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
            resourceIds.add(employeeFromPrevStep.getEmployeeId());
        } else {
            assertTrue(false, "unsupported object type " + objectType);
        }

        List eventsFilter = new ArrayList();
        if (eventsTypes.contains("Все ивенты")) {
            eventsFilter.addAll(Arrays.asList("TrafficRulesSpeedLimit", "UserSpeedlimit", "StopEvent",
                    "IdleEvent", "SharpTurn", "SharpSpeedup", "SharpBraking", "PanicButton"));
        } else if (eventsTypes.equals("Превышение скорости по ПДД")) {
            eventsFilter.addAll(Arrays.asList("TrafficRulesSpeedLimit"));
        } else if (eventsTypes.equals("Превышение скорости")) {
            eventsFilter.addAll(Arrays.asList("UserSpeedlimit"));
        } else if (eventsTypes.contains("Стоянка")) {
            eventsFilter.addAll(Arrays.asList("StopEvent"));
        } else if (eventsTypes.contains("Холостой ход")) {
            eventsFilter.addAll(Arrays.asList("IdleEvent"));
        } else if (eventsTypes.contains("Резкое торможение")) {
            eventsFilter.addAll(Arrays.asList("SharpBraking"));
        } else if (eventsTypes.contains("Резкое ускорение")) {
            eventsFilter.addAll(Arrays.asList("SharpSpeedup"));
        } else if (eventsTypes.contains("Нажатие тревожной кнопки")) {
            eventsFilter.addAll(Arrays.asList("PanicButton"));
        } else if (eventsTypes.contains("Резкие повороты")) {
            eventsFilter.addAll(Arrays.asList("SharpTurn"));
        } else {
            assertTrue(false, "not implemented " + eventsTypes);
        }

        DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        track = new TelematicsTrack(resourceIds, eventsFilter, df.format(dateTimeCorrectionFromInputArr(fromDate)),
                df.format(dateTimeCorrectionFromInputArr(tillDate)), true);

        JSONObject repliedTrack = (JSONObject) track.requestTelematicsTrack();
        SharedContainer.setContainer("track", track);
        SharedContainer.setContainer("repliedTrack", repliedTrack);
        log.debug("end of track");
    }
}