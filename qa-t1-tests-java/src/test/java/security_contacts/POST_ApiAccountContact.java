package security_contacts;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.GetAccountInfoFromMongo;

import cucumber.api.java.en.Then;

public class POST_ApiAccountContact extends AbstractClass {

	public AccountRegistrationAuth account;


	
	 @Then("^запросить смену телефона для созданного аккаунта \\(этап 1\\)$")
	 public void сhangePhone_start_1(){
			account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);

			account.setOldPhone(account.getPhone());
			account.setPhone("+70"+RandomStringUtils.random(9, getProperty("stdNumbPattern")));
			try {
				JSONObject accountPhoneUpdateReply = account.accountChangePhoneStart();
				account.setConfirmationId(accountPhoneUpdateReply.get("confirmationId").toString());
				JSONObject replyContacts = (JSONObject) accountPhoneUpdateReply.get("contact");
				if(replyContacts.keySet().contains("confirmed")){
					assertEquals ("API:accountPhoneUpdateRequest_contact.confirmed",replyContacts.get("confirmed"),"", "contact.confirmed не корректен");
				}
			} catch (Exception e) {
				log.error("test failed: "+e);
			}
			
	 }

	 @Then("^подтвердить смену телефона для созданного аккаунта \\(этап 2\\)$")
	 public void сhangePhone_confirm_2(){
			account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			try {
				JSONObject accountConfirmationInfo = GetAccountInfoFromMongo.getAccountConfirmationInfoFromMongo(account.getAccountId(),account.getPhone(), account.getEmail());
				account.setConfirmationCode(ReplyToJSON.extractPathFromJson(accountConfirmationInfo, "$.Code"));
				JSONObject accountPhoneUpdateReply = account.accountChangePhoneConfirmation();
				assertDateIsAfter("API:accountPhoneUpdateConfirm_contact.confirmed",ReplyToJSON.extractPathFromJson(accountPhoneUpdateReply, "$.contact.confirmed"),
						ReplyToJSON.extractPathFromJson(accountPhoneUpdateReply, "$.contact.created"),"contact.confirmed не корректен");

			} catch (Exception e) {
				log.error("test failed: "+e);
			}
			
	 }

}