package security_authorization;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_renew extends AbstractClass{
	//QA-689 (qa-t1-tests\securityWebAPI\src\test\jmeter\securityPositiveTests\postRenewValideToken.jmx)
	public AccountRegistrationAuth account;

//	@Given("^зарегистрировать аккаунт со случайныйми значениями полей$")
// реализовано в POST_register:  public void positive_test_with_Random_Data()
//	@And("^подтвердить ргистрацию используя код пришедший в смс$")
//	 реализовано в POST_register_confirm:  public void confirm_by_SMS_confirmation_code() 
//	@Then("^авторизоваться используя login нового аккаунта$")
//	 реализовано в POST_login:	public void auth_with_user_login()

	@Then("^запросить продление токена$")
	public void post_renew(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		account.renewToken(account.getLogin() );
	}

//	@Then("^выйти из аккаунта$")
//	 реализовано в POST_logout:		public void post_logout()




}