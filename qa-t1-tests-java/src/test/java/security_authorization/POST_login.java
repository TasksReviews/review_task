package security_authorization;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.AccountRole;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_login extends AbstractClass{
	public AccountRegistrationAuth account;

	
//	@Given("^зарегистрировать аккаунт со случайныйми значениями полей$")
// реализовано в POST_register:  public void positive_test_with_Random_Data()
//	@And("^подтвердить ргистрацию используя код пришедший в смс$")
//	 реализовано в POST_register_confirm:  public void confirm_by_SMS_confirmation_code() 

	
	@Then("^авторизоваться используя login нового аккаунта$")
	public void auth_with_user_login(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		String token = account.loginInClientPanel(account.getLogin(), account.getPassword() );
		//TODO: T1-2091 возможно токен не должен меняться
//		assertEquals(account.token, token, "получены разные токены при подтверждении аккаунта и авторизации");
		account.setToken(token);
	}
	@Then("^авторизоваться используя phone нового аккаунта$")
	public void auth_with_user_phone(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		String token = account.loginInClientPanel(account.getPhone(), account.getPassword() );
		//TODO: T1-2091 возможно токен не должен меняться
//		assertEquals(account.token, token, "получены разные токены при подтверждении аккаунта и авторизации");
		account.setToken(token);
	}


	
//	@Then("^зарегистрировать аккаунт со случайныйми значениями полей, без phone$")
// реализовано в POST_register: 	public  void Email_NecessaryOnRegistration()
	// Confirmation with correct parameters - Email
//	@Then("^подтвердить ргистрацию используя код пришедший в email$")
//	 реализовано в POST_register_confirm:  void confirm_by_Email_confirmation_code()

	@Then("^авторизоваться используя email нового аккаунта$")
	public void auth_with_user_email(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		String token = account.loginInClientPanel(account.getEmail(), account.getPassword() );
		//TODO: T1-2091 возможно токен не должен меняться
//		assertEquals(account.token, token, "получены разные токены при подтверждении аккаунта и авторизации");
		account.setToken(token);
	}
	
	@Then("^авторизоваться используя аккаунт с правами администратора$")
	public void auth_with_admin_account(){
		String adminToken  = new AccountRegistrationAuth().loginAsT1Admin();
		SharedContainer.setContainer("adminToken", adminToken);
	}
	
	@Then("^авторизоваться используя стандартный аккаунт$")
	public void auth_with_default_account(){
		String userToken  = new AccountRegistrationAuth().loginAsT1Developer();//loginAsT1Admin();
		SharedContainer.setContainer("userToken", userToken);
	}
	
	@Then("^авторизоваться используя demo аккаунт$")
	public void auth_with_demo_account(){
		String demoToken  = new AccountRegistrationAuth().loginAsT1Demo();
		SharedContainer.setContainer("demoToken", demoToken);
		SharedContainer.setContainer("customer", "5915badd7b4da6bb6429823f");
	}
	
	@Then("^авторизоваться используя аккаунт с правами разработчика$")
	public void auth_with_developer_account(){
	String devToken  = new AccountRegistrationAuth().loginAsT1Developer();
	SharedContainer.setContainer("devToken", devToken);
	}

	
	@Then("^авторизация в учетку с правами <\"Руководитель\\/владелец\">$")
	public void auth_with_Chief(){
		AccountRegistrationAuth	account = new AccountRegistrationAuth();
		String token = account.loginInClientPanel(getProperty("t1APIChief"), getProperty("t1APIPassword") );
		SharedContainer.setContainer("ChiefToken", token);

	}
	

	@Then("^авторизация в учетку с правами \\(Диспетчер, Транспортный специалист или Руководитель\\/владелец\\)$")
	public void auth_with_accountDev_Chief_TransExp(){
		AccountRegistrationAuth	account = new AccountRegistrationAuth();//(AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		String account_role =					pickRandomFromList(	new String[]{"Dispatcher", "TransportExpert","Chief"});
		String token = account.loginInClientPanel(getProperty("t1API"+account_role), getProperty("t1APIPassword") );
		SharedContainer.setContainer(account_role+"Token", token);

	}

	@Then("^авторизация в учетку с правами \\(кроме Демо, Курьер, Водитель\\)$")
	public void auth_anyRole_except_Demo_Courier_Driver(){
		AccountRegistrationAuth	account = new AccountRegistrationAuth();//(AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		String account_role = pickRandomFromList(	new String[]{"Admin","Operator","Dispatcher", "Analyst","TransportExpert","TechnicalExpert","FinancialExpert","Chief","InsuranceAnalyst"});
		String token = account.loginInClientPanel(getProperty("t1API"+account_role), getProperty("t1APIPassword") );
		SharedContainer.setContainer(account_role+"Token", token);
	}
	
	@Then("^авторизация в учетку с правами \\(кроме Демо, Курьер, Водитель, Аналитик, Финансовый специалист, Аналитик страховой компании\\)$")
	public void auth_anyRole_except_Demo_Courier_Driver_Analyst_FinancialExpert_InsuranceAnalyst(){
		AccountRegistrationAuth	account = new AccountRegistrationAuth();//(AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		String account_role = pickRandomFromList(	new String[]{"Admin","Operator","Dispatcher","TransportExpert","TechnicalExpert","Chief"});
		String token = account.loginInClientPanel(getProperty("t1API"+account_role), getProperty("t1APIPassword") );
		SharedContainer.setContainer(account_role+"Token", token);
	}
	
	@Given("^авторизация в созданный аккаунт$")
	public void auth_with_created_user() {
		auth_with_user_login();
	}
	
	public void loginToAppForEachRole(String typeOfLogin) {	
	boolean requestIsPositive;
	account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
	List roles = AccountRole.getAllExistingRoles();
	String loginValue ="";
		if (typeOfLogin.equals("login")) {
			loginValue = account.getLogin();
		} else if (typeOfLogin.equals("phone")) {
			loginValue = account.getPhone();
		} else {
			loginValue = account.getEmail();
		}
	for (int i = 0; i < roles.size(); i++) {
		JSONObject role = (JSONObject) roles.get(i);
		String roleName = role.get("Name").toString();
	//	String roleId = ReplyToJSON.extractPathFromJson(role, "$._id.$oid").toString();
		log.debug("got role =" + roleName);// + " with id=" + roleId);
		AccountRole.setRoleToAccountId(roleName, account.getAccountId());

			 final String tokenToApp  = account.loginInClientPanel(loginValue, account.getPassword() );
			assertContains("API:role:"+roleName+" LoginAdmin.token", tokenToApp, "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImkzRkJ4TkNXc2tUeU41cnNCQlBWUmxTaTJmOCJ9.", 
							"должен был быть получен токен");
			setToAllureChechkedFields();
			SharedContainer.setContainer("tokenForAdmPanel", tokenToApp);

	}
	setToAllureChechkedFields();
}

	
	@Given("^проверить что успешная авторизация в приложении с использованием login нового аккаунта возможна с любой ролью$")
	public void loginUsingLoginForEachRole() {
		loginToAppForEachRole("login");
	}
	
	@Given("^проверить что успешная авторизация в приложении с использованием phone нового аккаунта возможна с любой ролью$")
	public void loginUsingPhoneForEachRole() {
		loginToAppForEachRole("phone");
	}
	
	@Given("^проверить что успешная авторизация в приложении с использованием email нового аккаунта возможна с любой ролью$")
	public void loginUsingEmailForEachRole() {
		loginToAppForEachRole("email");
	}

	@Given("^авторизация в учетку с ролью = \"([^\"]*)\"$")
	public void login_with_role(String role) {
		assertTrue(role != null && !role.equals(""), "Неверно указана роль: " + role + " !");
		String account_role = "";
		switch (role){
			case "Демо":                        account_role = "Demo";break;
			case "Диспетчер":                   account_role = "Dispatcher";break;
			case "Транспортный специалист":     account_role = "TransportExpert";break;
			case "Руководитель/владелец":       account_role = "Chief";break;
			case "Аналитик страховой компании": account_role = "InsuranceAnalyst";break;
			case "Оператор":                    account_role = "Operator";break;
			case "Водитель":                    account_role = "Driver";break;
			case "Курьер":                      account_role = "Courier";break;
			case "Аналитик":                    account_role = "Analyst";break;
			case "Технический специалист":      account_role = "TechnicalExpert";break;
			case "Финансовый специалист":       account_role = "FinancialExpert";break;
			default : throw new IllegalArgumentException("Указанная роль не существует: " + role + " !");
		}

		AccountRegistrationAuth	account = new AccountRegistrationAuth();
		String token = account.loginInClientPanel(getProperty("t1API" + account_role), getProperty("t1APIPassword"));
		SharedContainer.setContainer(account_role + " token", token);
	}
}