package security_authorization;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_confirmation_verify extends AbstractClass{

	public AccountRegistrationAuth account;
	
//	@Given("^зарегистрировать аккаунт со случайныйми значениями полей$")
// реализовано в POST_register:  public void positive_test_with_Random_Data()
//	@And("^подтвердить ргистрацию используя код пришедший в смс$")
//	 реализовано в POST_register_confirm:  public void confirm_by_SMS_confirmation_code() 

//	@Given("^запросить восстановление \\(смену\\) пароля с использованием телефона для созданного аккаунта \\(этап 1\\)$")
//   реализовано в POST_resetPassword_start: public void resetPassword_acc_with_phone_start_1() 

	
//	@Given("^запросить восстановление \\(смену\\) пароля с использованием email для созданного аккаунта \\(этап 1\\)$")
//   реализовано в POST_resetPassword_start: public void resetPassword_acc_without_phone_start_1() 
	
	
	 @Then("^запросить проверку полученного кода для смены пароля$")
	 public void post_confirmation_verify(){
			account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);

			try {
				account.confirmationVerify(account.getAccountId(),account.getConfirmationId(), account.getConfirmationCode(),"");

			} catch (Exception e) {
				log.error(ERROR,e);
			}
			
	 }


}