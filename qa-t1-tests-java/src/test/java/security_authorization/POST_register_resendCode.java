package security_authorization;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_register_resendCode extends AbstractClass{
	public AccountRegistrationAuth account;

	
//	@Given("^зарегистрировать аккаунт со случайныйми значениями полей$")
// реализовано в POST_register:  public void positive_test_with_Random_Data()
	
//	@Given("^Given зарегистрировать аккаунт со случайныйми значениями полей, без phone$")
// реализовано в POST_register:  public void email_NecessaryOnRegistration()
	

	
	@Then("^запросить повтор отправки кода подтверждения регистрации в смс$")
	public void post_register_resendCode(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		account.registerResendCode(account.getLogin(), account.getPhone(),"Sms", "");
	}

	@Then("^запросить повтор отправки кода подтверждения регистрации в email$")
	public void post_register_resendCode_no_phone(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		account.registerResendCode(account.getLogin(), account.getEmail(),"Email", "");
	}
}