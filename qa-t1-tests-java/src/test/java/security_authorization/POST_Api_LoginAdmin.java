package security_authorization;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.AccountRole;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_Api_LoginAdmin extends AbstractClass{
	public AccountRegistrationAuth account;


	
	@Then("^авторизоваться в панели администратора используя login нового аккаунта$")
	public void loginAdmin(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
	final String adminTokenForAdmPanel  = account.loginAsT1AdminInAdminPanel(account.getLogin(), account.getPassword() );  //new AccountRegistrationAuth().loginAsT1AdminInAdminPanel();
	SharedContainer.setContainer("adminTokenForAdmPanel", (new ArrayList(){{add(adminTokenForAdmPanel);}}));
	}
	
	@Then("^авторизоваться в панели администратора используя phone нового аккаунта$")
	public void loginAdminUsingPhone(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
	final String adminTokenForAdmPanel  = account.loginAsT1AdminInAdminPanel(account.getPhone(), account.getPassword() );  //new AccountRegistrationAuth().loginAsT1AdminInAdminPanel();
	SharedContainer.setContainer("adminTokenForAdmPanel", (new ArrayList(){{add(adminTokenForAdmPanel);}}));
	}
	
	@Then("^авторизоваться в панели администратора используя email нового аккаунта$")
	public void loginAdminUsingEmail(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
	final String adminTokenForAdmPanel  = account.loginAsT1AdminInAdminPanel(account.getEmail(), account.getPassword() );  //new AccountRegistrationAuth().loginAsT1AdminInAdminPanel();
	SharedContainer.setContainer("adminTokenForAdmPanel", (new ArrayList(){{add(adminTokenForAdmPanel);}}));
	}
	
	
	
	public void loginAdminForEachRole(String typeOfLogin) {	
	boolean requestIsPositive;
	account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
	List roles = AccountRole.getAllExistingRoles();
	String loginValue ="";
		if (typeOfLogin.equals("login")) {
			loginValue = account.getLogin();
		} else if (typeOfLogin.equals("phone")) {
			loginValue = account.getPhone();
		} else {
			loginValue = account.getEmail();
		}
	for (int i = 0; i < roles.size(); i++) {
		JSONObject role = (JSONObject) roles.get(i);
		String roleName = role.get("Name").toString();
	//	String roleId = ReplyToJSON.extractPathFromJson(role, "$._id.$oid").toString();
		log.debug("got role =" + roleName);// + " with id=" + roleId);
		AccountRole.setRoleToAccountId(roleName, account.getAccountId());
		
		if (roleName.equals("Developer")){
			requestIsPositive = true;
			 final String tokenForAdmPanel  = account.loginInAdminPanel(loginValue, account.getPassword(),requestIsPositive );
			assertContains("API:role:"+roleName+" LoginAdmin.token", tokenForAdmPanel, "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImkzRkJ4TkNXc2tUeU41cnNCQlBWUmxTaTJmOCJ9.", 
							"должен был быть получен токен");
			setToAllureChechkedFields();
			SharedContainer.setContainer("tokenForAdmPanel", (new ArrayList(){{add(tokenForAdmPanel);}}));
		} else{
			requestIsPositive = false;
			final String requestAdminLoginReply  = account.loginInAdminPanel(loginValue, account.getPassword(),requestIsPositive );
			assertEquals( "API:role:"+roleName+" LoginAdmin.code", requestAdminLoginReply   ,"Forbidden", "LoginAdmin.code не верен");
			setToAllureChechkedFields();
		}
	}
	setToAllureChechkedFields();
}


	
	@Given("^проверить что успешная авторизация в панели администратора использованием login нового аккаунта возможна только с ролью Developer$")
	public void loginAdminUsingLoginForEachRole() {
		loginAdminForEachRole("login");
	}
	@Given("^проверить что успешная авторизация в панели администратора использованием phone нового аккаунта возможна только с ролью Developer$")
	public void loginAdminUsingPhoneForEachRole() {
		loginAdminForEachRole("phone");
	}
	
	@Given("^проверить что успешная авторизация в панели администратора использованием email нового аккаунта возможна только с ролью Developer$")
	public void loginAdminUsingEmailForEachRole() {
		loginAdminForEachRole("email");
	}
	
  @Given("^Залогиниться в панели администратора под пользователем с ролью девелопера$")
  public void loginInAdminPanelAsDeveloper() {
	  AccountRegistrationAuth account = new AccountRegistrationAuth();
		 final String tokenForAdmPanel  = account.loginInAdminPanel(getProperty("t1APIDeveloper"), getProperty("t1APIPassword"),true );
		assertContains("API:role: t1APIDeveloper LoginAdmin.token", tokenForAdmPanel, "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImkzRkJ4TkNXc2tUeU41cnNCQlBWUmxTaTJmOCJ9.", 
						"должен был быть получен токен");
		setToAllureChechkedFields();
		SharedContainer.setContainer("tokenForAdmPanel",tokenForAdmPanel);
		
  }
	
	@Given ("^Авторизоваться в панели администратора$")
	  public void loginInAdminPanel() {
		loginInAdminPanelAsDeveloper();
		
	}
}