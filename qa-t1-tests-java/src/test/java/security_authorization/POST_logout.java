package security_authorization;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_logout extends AbstractClass{
	public AccountRegistrationAuth account;

//	@Given("^зарегистрировать аккаунт со случайныйми значениями полей$")
// реализовано в POST_register:  public void positive_test_with_Random_Data()
//	@And("^подтвердить ргистрацию используя код пришедший в смс$")
//	 реализовано в POST_register_confirm:  public void confirm_by_SMS_confirmation_code() 
//	@Then("^авторизоваться используя login нового аккаунта$")
//	 реализовано в POST_login:	public void auth_with_user_login()


	@Then("^выйти из аккаунта$")
	public void post_logout(){
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		account.logout(account.getLogin() );

	}
}