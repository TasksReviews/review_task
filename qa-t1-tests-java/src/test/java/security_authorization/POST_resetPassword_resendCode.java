package security_authorization;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.GetAccountInfoFromMongo;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class POST_resetPassword_resendCode extends AbstractClass{
	// QA-690
	// qa-t1-tests\securityWebAPI\src\test\jmeter\securityPositiveTests\postResetPasswordStart&PerformPositiveTC.jmx
	public AccountRegistrationAuth account;

	
//	@Given("^зарегистрировать аккаунт со случайныйми значениями полей$")
// реализовано в POST_register:  public void positive_test_with_Random_Data()
//	@And("^подтвердить ргистрацию используя код пришедший в смс$")
//	 реализовано в POST_register_confirm:  public void confirm_by_SMS_confirmation_code() 
//	@Then("^авторизоваться используя login нового аккаунта$")
//	 реализовано в POST_login:	public void auth_with_user_login()
//	@And выйти из аккаунтов
//	 реализовано в POST_logout:	public void post_logout()
	

	@And("^запросить восстановление \\(смену\\) пароля для созданного аккаунта указав телефон \\(этап 1\\)$")
	public void resetPassword_start_1() {
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		account.setConfirmationId(account.resetPasswordStart(account.getLogin(), account.getPhone(), ""));

		checkConfirmationCodeReceivedAndIsCorrect(this.account,account.getConfirmationCode(),"Sms");
	}

	
	
	@Then("^запросить повтор отправки кода подтверждения$")
	public void resetPassword_resendCode() {
		account.setConfirmationId(account.resetPasswordResendCode(account.getLogin(), account.getPhone(), ""));
	}
	
	public void checkConfirmationCodeReceivedAndIsCorrect(AccountRegistrationAuth account, String confirmationCodeReceived, String channelType){
		JSONObject deliveryNotification;
		try {
			log.debug("get info for account: "+account.getAccountId()+" account phone: "+account.getPhone());
			JSONObject accountConfirmationInfo = GetAccountInfoFromMongo.getAccountConfirmationInfoFromMongo(account.getAccountId(),account.getPhone(), account.getEmail());
			account.setConfirmationCode(ReplyToJSON.extractPathFromJson(accountConfirmationInfo, "$.Code"));
			//assertEquals(confirmationCodeReceived, account.confirmationCode, "получен код отличный от хранящегося в БД");
			assertTrue(ReplyToJSON.extractPathFromJson(accountConfirmationInfo, "$.Used").equals("false"), 	"код подтверждения не должен был быть использован");
			
			log.info("Get Delivery Notification from DB");
			if(channelType.equals("Sms")){
				deliveryNotification = GetAccountInfoFromMongo.getLastDeliveryNotificationByReceiverContact(channelType, account.getPhone(), "");
			}else {
				deliveryNotification = GetAccountInfoFromMongo.getLastDeliveryNotificationByReceiverContact(channelType, account.getEmail(), "");
			}
			account.setDeliveryNotificationState(ReplyToJSON.extractPathFromJson(deliveryNotification, "$.State"));
			account.setDeliveryNotificationMessageContent(ReplyToJSON.extractPathFromJson(deliveryNotification,	"$.MessageContent"));
			POST_register.doMainAsserts(account.getDeliveryNotificationState(), account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
			
		} catch (Exception e) {
			log.error(ERROR,e);
		}
		
	}

}