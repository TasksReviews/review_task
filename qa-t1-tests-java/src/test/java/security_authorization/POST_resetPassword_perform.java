package security_authorization;

import org.apache.commons.lang3.RandomStringUtils;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_resetPassword_perform extends AbstractClass {
	// QA-691
	// qa-t1-tests\securityWebAPI\src\test\jmeter\securityPositiveTests\postResetPasswordStart&PerformPositiveTC.jmx
	public AccountRegistrationAuth account1, account2;

	// @Given("^зарегистрировать \\d аккаунта со случайныйми значениями полей$")
	// реализовано в POST_resetPassword_start: public void	 register_2_accounts_with_Random_Data()

	// @And("^подтвердить ргистрацию двух аккаунтов используя код пришедший в
	// sms или email$")
	// реализовано в POST_resetPassword_start: public void confirm_registration_for_2accs_with_confirmation_code() 

	// @And("^авторизоваться используя login двух новых аккаунтов$")
	// реализовано в POST_resetPassword_start: public void auth_2_accs_with_user_login() 

	// @And("^выйти из аккаунтов$")
	// реализовано в POST_resetPassword_start: public void post_logout() 

	// @Then("^запросить восстановление \\(смену\\) пароля для созданных аккаунтов \\(этап 1\\)$")
	// реализовано в POST_resetPassword_start: public void resetPassword_start_1() 
	
	 @Then("^запросить восстановление \\(смену\\) пароля для созданных аккаунтов \\(этап 2\\)$")
	 public void resetPassword_start_2(){
			account1 = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			account2 = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(1);
			String new_password =	"JFW_"+RandomStringUtils.random(20, "AzQWERTYuiop~!@#$%^&_-=1230");
			String new_password2 =	"JFW_"+RandomStringUtils.random(20, "AzQWERTYuiop~!@#$%^&_-=1230");
			try {
				account1.resetPasswordPerform(account1.getAccountId(),account1.getConfirmationId(), account1.getConfirmationCode(),new_password,"");
				account1.resetPasswordPerform(account2.getAccountId(),account2.getConfirmationId(), account2.getConfirmationCode(),new_password2,"");
			} catch (Exception e) {
				log.error(ERROR,e);
			}
			
	 }


}