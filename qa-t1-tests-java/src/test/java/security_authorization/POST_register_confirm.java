package security_authorization;

import org.junit.Rule;
import org.junit.rules.ErrorCollector;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class POST_register_confirm extends AbstractClass {
	public AccountRegistrationAuth account;
	@Rule
	public ErrorCollector collector= new ErrorCollector();
//	Scenario: Confirmation with correct parameters - SMS
	
//	@Given("^зарегистрировать аккаунт со случайныйми значениями полей$")
// реализовано в POST_register:  public void positive_test_with_Random_Data()

	@Then("^подтвердить ргистрацию используя код пришедший в смс$")
	public  void post_confirm_registration_by_code_from_SMS() {
		log.info("start secondStep.");
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		// AccountRegistration account = new AccountRegistration(input);
		try {
			this.account.confirmRegistration(account.getAccountId());

		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			if (account == null){
			assertTrue(false,"невозможно подтвердить регистрацию аккаунта, аккаунт не был создан в предыдущем шаге");
			}
		}

	}

	// Confirmation with correct parameters - Email
//	@Given("^зарегистрировать аккаунт со случайныйми значениями полей, без phone$")
	// реализовано в POST_register:  public void email_NecessaryOnRegistration()

	@Then("^подтвердить ргистрацию используя код пришедший в email$")
	public  void post_confirm_registration_by_code_from_Email() {
		try {
			account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			account.confirmRegistration(account.getAccountId());
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			if (account == null){
			assertTrue(false,"невозможно подтвердить регистрацию аккаунта, аккаунт не был создан в предыдущем шаге");
			}
		}

	}
	
}