package security_authorization;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.GetAccountInfoFromMongo;

import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_resetPassword_start extends AbstractClass {
	// QA-690
	// qa-t1-tests\securityWebAPI\src\test\jmeter\securityPositiveTests\postResetPasswordStart&PerformPositiveTC.jmx
	public AccountRegistrationAuth account, account1, account2;

	@ContinueNextStepsFor({ AssertionError.class })
	@Given("^зарегистрировать \\d аккаунта со случайныйми значениями полей$")
	public void register_2_accounts_with_Random_Data() {
		for (int i = 1; i <= 2; i++) {
			//why we do init here? must be made into AccountRegistrationAuth
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		//	String[] input = initializeInputWithRandomValues();
			account1 = new AccountRegistrationAuth(initMap,false);
			account1.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());//input[3]);
			SharedContainer.setContainer("accounts", (new ArrayList() {{add(account1);}}));
		}
	}

	@And("^подтвердить ргистрацию двух аккаунтов используя код пришедший в sms или email$")
	public void confirm_registration_for_2accs_with_confirmation_code() {
		account1 = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		account2 = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(1);
		try {
			account1.confirmRegistration(account1.getAccountId());
			account2.confirmRegistration(account2.getAccountId());
		} catch (Exception e) {
			log.error(ERROR,e);
		}

	}

	@And("^авторизоваться используя login двух новых аккаунтов$")
	public void auth_2_accs_with_user_login() {
		String token1 = account1.loginInClientPanel(account1.getLogin(), account1.getPassword());
		String token2 = account2.loginInClientPanel(account2.getLogin(), account2.getPassword());
	}

	@And("^выйти из аккаунтов$")
	public void post_logout() {
		account1.logout(account1.getLogin());
		account2.logout(account2.getLogin());
	}

	@Then("^запросить восстановление \\(смену\\) пароля для созданных аккаунтов \\(этап 1\\)$")
	public void resetPassword_2accts_start_1() {

		String phone_valid_not_exist = RandomStringUtils.random(10, "1234567890");
		String email_valid_not_exist = RandomStringUtils.random(10, "qwertyuiopzxcvbnm123") + "@email"	+ RandomStringUtils.random(10, "qwertyuiop0123") + ".com";
		log.info("send non-existing contacts");
		account1.resetPasswordStart(account1.getLogin(), phone_valid_not_exist, "NotFound");
		account2.resetPasswordStart(account2.getLogin(), email_valid_not_exist, "NotFound");
		log.info("send existing contacts for just created accounts");
		account1.setConfirmationId(account1.resetPasswordStart(account1.getLogin(), account1.getPhone(), ""));
		account2.setConfirmationId(account2.resetPasswordStart(account2.getLogin(), account2.getEmail(), ""));

		checkConfirmationCodeReceivedAndIsCorrect(this.account1,account1.getConfirmationCode(),"Sms");
		checkConfirmationCodeReceivedAndIsCorrect(this.account2,account2.getConfirmationCode(),"Email"); //Email??  T1-2167
	}

	
	public void checkConfirmationCodeReceivedAndIsCorrect(AccountRegistrationAuth account, String confirmationCodeReceived, String channelType){
		JSONObject deliveryNotification;
		try {
			log.debug("get info for account: "+account.getAccountId()+" account phone: "+account.getPhone());
			JSONObject accountConfirmationInfo = GetAccountInfoFromMongo.getAccountConfirmationInfoFromMongo(account.getAccountId(),account.getPhone(), account.getEmail());
			account.setConfirmationCode(ReplyToJSON.extractPathFromJson(accountConfirmationInfo, "$.Code"));
			//assertEquals(confirmationCodeReceived, account.confirmationCode, "получен код отличный от хранящегося в БД");
			assertTrue(ReplyToJSON.extractPathFromJson(accountConfirmationInfo, "$.Used").equals("false"), 	"код подтверждения не должен был быть использован");
			assertTrue(ReplyToJSON.extractPathFromJson(accountConfirmationInfo, "$.Operation").equals("passwordChange"), "получен некорректный документ из БД");
			
			log.info("Get Delivery Notification from DB");
			if(channelType.equals("Sms")){
				deliveryNotification = GetAccountInfoFromMongo.getLastDeliveryNotificationByReceiverContact(channelType, account.getPhone(), "");
			}else {
				deliveryNotification = GetAccountInfoFromMongo.getLastDeliveryNotificationByReceiverContact(channelType, account.getEmail(), "");
			}
			account.setDeliveryNotificationState(ReplyToJSON.extractPathFromJson(deliveryNotification, "$.State"));
			account.setDeliveryNotificationMessageContent(ReplyToJSON.extractPathFromJson(deliveryNotification,	"$.MessageContent"));
			POST_register.doMainAsserts(account.getDeliveryNotificationState(), account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
			
		} catch (Exception e) {
			log.error(ERROR,e);
		}
		
	}
	
	
//	Added for other tests
	@Given("^запросить восстановление \\(смену\\) пароля с использованием телефона для созданного аккаунта \\(этап 1\\)$")
	public void resetPassword_acc_with_phone_start_1() {
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		account.setConfirmationId(account.resetPasswordStart(account.getLogin(), account.getPhone(), ""));
		checkConfirmationCodeReceivedAndIsCorrect(this.account,account.getConfirmationCode(),"Sms");
	}
	
	
	@Given("^запросить восстановление \\(смену\\) пароля с использованием email для созданного аккаунта \\(этап 1\\)$")
	public void resetPassword_acc_without_phone_start_1() {
		account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
		account.setConfirmationId(account.resetPasswordStart(account.getLogin(), account.getEmail(), ""));
		checkConfirmationCodeReceivedAndIsCorrect(this.account,account.getConfirmationCode(),"Email"); //Email??  T1-2167
	}
	
	
}