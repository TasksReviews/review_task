package security_authorization;

import java.util.ArrayList;
import java.util.Map;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.AccountRole;
import com.t1.core.mongodb.MongoConnection;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.ContinueNextStepsFor;
//import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import ru.yandex.qatools.allure.annotations.Step;

public class POST_register extends AbstractClass {
	public AccountRegistrationAuth account; 
	// QA-597 (qa-t1-tests\securityWebAPI\src\test\jmeter\securityPositiveTests\postRegisterPositiveTC.jmx)
	//Different sets of fields
	
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^зарегистрировать аккаунт со случайныйми значениями полей$")
	public void positive_test_with_Random_Data() {
		log.info("Start 'Positive_test_with_Random_Data'");
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
			account	= new AccountRegistrationAuth(initMap,false);
			account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
			doMainAsserts(account.getDeliveryNotificationState(), account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
	}
	
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^зарегистрировать аккаунт без поля login$")
	public  void positive_test_without_Login() {
		log.info("Start 'Positive_test_without_Login'");
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		// remove random login
		initMap.put("login",null);
		account	= new AccountRegistrationAuth(initMap,false);
		this.account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
		SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
		doMainAsserts(this.account.getDeliveryNotificationState(), this.account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());

	}
	
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^зарегистрировать аккаунт c пустым значением поля login$")
	public void positive_test_with_empty_Login() {
		log.info("Start 'Positive_test_with_empty_Login'");
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		// change random login to empty
		initMap.put("login","");
		account	= new AccountRegistrationAuth(initMap,false);
		this.account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
		SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
		doMainAsserts(this.account.getDeliveryNotificationState(), this.account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
	}

	@Given("^установить в БД поле phone = NecessaryOnRegistration$")
	public  void setPhone_NecessaryOnRegistration() {
		log.info("Start 'Phone_NecessaryOnRegistration'");
		// set mail NOT necessary on registration and phone necessary
		MongoConnection.updateSubsystemsVarsCollection("AuthConfig.AuthFields.1.AuthFlags","PossibleOnLogin, PossibleOnRegistration", "");
		MongoConnection.updateSubsystemsVarsCollection("AuthConfig.AuthFields.2.AuthFlags","PossibleOnLogin, NecessaryOnRegistration", "");
	}
	
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^зарегистрировать аккаунт без поля email$")
	public  void phone_NecessaryOnRegistration() {
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		// remove email
		initMap.put("email",null);
		account	= new AccountRegistrationAuth(initMap,false);
		this.account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
		SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
		doMainAsserts(this.account.getDeliveryNotificationState(), this.account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
	}

	@Given("^установить в БД поле email = NecessaryOnRegistration$")
	public  void setEmail_NecessaryOnRegistration() {
		log.info("Start 'Email_NecessaryOnRegistration'");
		// set mail necessary on registration and phone NOT necessary
		MongoConnection.updateSubsystemsVarsCollection("AuthConfig.AuthFields.1.AuthFlags",			"PossibleOnLogin, NecessaryOnRegistration", "");
		MongoConnection.updateSubsystemsVarsCollection("AuthConfig.AuthFields.2.AuthFlags",			"PossibleOnLogin, PossibleOnRegistration", "");
	}

	@ContinueNextStepsFor({AssertionError.class})
	@Then("^зарегистрировать аккаунт со случайныйми значениями полей, без phone$")
	public  void email_NecessaryOnRegistration() {
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		// remove phone
		initMap.put("accountPhone",null);
		account	= new AccountRegistrationAuth(initMap,false);
		this.account.startCommonAccountRegistration(initMap, "Email", initMap.get("email").toString());//input[4]);
		SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
		doMainAsserts(this.account.getDeliveryNotificationState(), this.account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());

	}

	@Then("^зарегистрировать аккаунт c Unicode значением поля login$")
	public  void positive_test_with_unicode_Login(DataTable table) {
		log.info("Start 'Positive_test_with_unicode_Login'");
		String unicodeLogin = "";
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		
		for (Map<String, String> map : table.asMaps(String.class, String.class)) {
			unicodeLogin = map.get("login");
			log.info("Got unicode login=" + unicodeLogin);

			// change random login to unicode
			initMap.put("login",unicodeLogin);// "%u043B%u043E%u0433%u0438%u043D";
			account	= new AccountRegistrationAuth(initMap,false);
			this.account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
			doMainAsserts(this.account.getDeliveryNotificationState(), this.account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
		}
	}

	@Then("^зарегистрировать аккаунт c Unicode значением поля firstname$")
	public  void positive_test_with_unicode_Firstname(DataTable table) {
		log.info("Start 'Positive_test_with_unicode_Firstname'");
		String unicodeFirstname = "";
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		
		for (Map<String, String> map : table.asMaps(String.class, String.class)) {
			unicodeFirstname = map.get("firstname");
			log.info("Got unicode Firstname=" + unicodeFirstname);
			// change random firstname to unicode
			initMap.put("firstname",unicodeFirstname);// "%u0438%u043C%u044F";
			account	= new AccountRegistrationAuth(initMap,false);
			this.account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
			doMainAsserts(this.account.getDeliveryNotificationState(), this.account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
		}
	}

	@Then("^зарегистрировать аккаунт c Unicode значением поля lastname$")
	public  void positive_test_with_unicode_Lastname(DataTable table) {
		log.info("Start 'Positive_test_with_unicode_Lastname'");
		String unicodeLastname = "";
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		
		for (Map<String, String> map : table.asMaps(String.class, String.class)) {
			unicodeLastname = map.get("lastname");
			log.info("Got unicode Lastname=" + unicodeLastname);
			// change random lastname to unicode
			initMap.put("lastname",unicodeLastname);// "%u0444%u0430%u043C%u0438%u043B%u0438%u044F";
			account	= new AccountRegistrationAuth(initMap,false);
			this.account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
			doMainAsserts(this.account.getDeliveryNotificationState(), this.account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
		}
	}

	@Then("^зарегистрировать аккаунт c Unicode значением поля password$")
	public  void positive_test_with_unicode_Password(DataTable table) {
		log.info("Start 'Positive_test_with_unicode_Password'");
		String unicodePassword = "";
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		
		for (Map<String, String> map : table.asMaps(String.class, String.class)) {
			unicodePassword = map.get("password");
			log.info("Got unicode Password=" + unicodePassword);
			// change random password to unicode
			initMap.put("password",unicodePassword);// "%u0441%u0435%u043A%u0440%u0435%u0442%u043D%u044B%u0439%u043F%u0430%u0440%u043E%u043B%u044C";
			account	= new AccountRegistrationAuth(initMap,false);
			this.account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
			doMainAsserts(this.account.getDeliveryNotificationState(), this.account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());

		}
	}
	

	@Then("^зарегистрировать аккаунт c различными значениями полей из таблицы: \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\"$")
	public  void positive_test_input_from_table(String login, String firstname, String lastname, String email, String password) {
		log.info("Start 'Positive_test_input_from_table'");
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
		initMap.put("firstname",firstname);
		initMap.put("lastname",lastname);
		initMap.put("login",login);
		initMap.put("email",email);
		initMap.put("password",password);
		account	= new AccountRegistrationAuth(initMap,false);
		this.account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
		SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
		doMainAsserts(this.account.getDeliveryNotificationState(), this.account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
	}

	@After("@QA-1444, @QA-1445")

	public  void After_Test_PhoneAndEmail_PossibleOnRegistration() {
		log.info("Start 'After_Test_PhoneAndEmail=PossibleOnRegistration'");
		// set mail and phone Possible on registration
		MongoConnection.updateSubsystemsVarsCollection("AuthConfig.AuthFields.1.AuthFlags",		"PossibleOnLogin, PossibleOnRegistration", "");
		MongoConnection.updateSubsystemsVarsCollection("AuthConfig.AuthFields.2.AuthFlags",		"PossibleOnLogin, PossibleOnRegistration", "");

	}
	
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^зарегистрировать \"([^\"]*)\" аккаунтов со случайныйми значениями полей$")
	public void positive_test_x_accounts_with_Random_Data(String numberOfAccountsStr) {
		int numberOfAccounts = Integer.parseInt(numberOfAccountsStr);
		for (int i=1; i<=numberOfAccounts; i++){
		log.info("Start 'Positive_test_with_Random_Data'");
		//why we do init here? must be made into AccountRegistrationAuth
		JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
			account	= new AccountRegistrationAuth(initMap,false);
			account.startCommonAccountRegistration(initMap, "Sms", initMap.get("accountPhone").toString());
			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
			doMainAsserts(account.getDeliveryNotificationState(), account.getDeliveryNotificationMessageContent(), account.getConfirmationCode());
		}
	}
	
	@Then("^добавить права администратора в mongoDB$")
	public  void setAdminRoleToAccountInMongo() {
		try {
			AccountRegistrationAuth account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			AccountRole.setAdminRoleToAccountId(account.getAccountId());
		} catch (Exception e) {
			log.error(ERROR,e);
			setToAllureChechkedFields();
		}

	}
	
	@Then("^добавить права разрабоотчика в mongoDB$")
	public  void setDeveloperRoleToAccountInMongo() {
		try {
			AccountRegistrationAuth account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			AccountRole.setDeveloperRoleToAccountId(account.getAccountId());
		} catch (Exception e) {
			log.error(ERROR,e);
			setToAllureChechkedFields();
		}

	}
	

	
/**
 * 
 * @param deliveryNotificationState
 * @param deliveryNotificationMessageContent
 * @param confirmationCode
 */
	@Step("Проверка: deliveryNotificationState=DeliveryDoneOk, confirmationCode (хранимый в NotificationDeliveries и Confirmations одинаков) корректный")
	static void doMainAsserts(String deliveryNotificationState, String deliveryNotificationMessageContent, String confirmationCode) {
			assertEquals("confirmationCode",deliveryNotificationMessageContent, confirmationCode,"неверный код подтверждения");
			//assertThat("проверка deliveryNotificationState", deliveryNotificationState, is(equalTo("DeliveryDoneOk")));
			assertEquals("deliveryNotificationState",deliveryNotificationState,"DeliveryDoneOk", "ожидалось DeliveryDoneOk");

	}
	// QA-597
	// (qa-t1-tests\securityWebAPI\src\test\jmeter\securityPositiveTests\postRegisterNegativeTC.jmx)
}