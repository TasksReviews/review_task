package telematics_Devices;

import java.util.Arrays;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.t1.core.AbstractClass;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GET_ApiAdminTelematicsDevicesPageSize extends AbstractClass {
    @When("^Запросить информацию о кол-ве ТМУ в MongoDb$")
    public void getInfoDeviceCountInDB() throws Throwable {
        MongoCredential credential = MongoCredential.createScramSha1Credential("qa", "admin", "emCjSVmyvtkPNU6eSngX".toCharArray());
        MongoClient mongoClient = new MongoClient(new ServerAddress("176.112.216.255", 27017),
                Arrays.asList(credential));
        Double count = (Double) mongoClient.getDatabase("ToneMainTest").runCommand(new BasicDBObject("eval", "db.getCollection('Devices').find({}).count()")).get("retval");
        log.debug("count of devices = " + count);

    }

    @And("^Запросить информацию по \"([^\"]*)\" устройствам на странице$")
//#Вызвать метод /Api/Admin/Telematics/Devices?pageSize=<pageSize> с разными значениями параметра pageSize
    public void getDevicesInPage(String arg0) throws Throwable {
        JSONObject response = (JSONObject) SendRequestToAPI.sendGetRequest(getProperty("host") + "/Telematics/Devices?pageSize=" + arg0, "GET", getProperty("dbuserAdmin"), getProperty("dbpasswordAdmin"), getCurAuthAccToken("authorizedAccount_admPanel", "Developer")).get(0);
        JSONArray items = (JSONArray) response.get("items");
        SharedContainer.setContainer("response", response);
    }

    @Then("^Проверить, что параметр totalPage зависит от pageSize$")
//#Проверить, что метод возвращает правильное значение totalPage
//#int totalPages = device_count%pageSize !== 0 ? device_count/pageSize + 1 : device_count/pageSize
//#Проверить, что items.length == <pageSize> (pageNumber = 1 по умолчанию)
    public void checkTotalPageToPageSize() throws Throwable {
        JSONObject response = (JSONObject) SharedContainer.getFromContainer("response").get(0);
        JSONObject pageInfo = (JSONObject) response.get("pageInfo");
        JSONArray items = (JSONArray) response.get("items");
        Long pageSize = (Long) pageInfo.get("pageSize");
        Long totalItem = (Long) pageInfo.get("totalItem");
        Long totalPage = (Long) pageInfo.get("totalPage");
        int total = totalItem.intValue() % pageSize.intValue() != 0 ? totalItem.intValue() / pageSize.intValue() + 1 : totalItem.intValue() / pageSize.intValue();
        assertEquals("check totalPage ", total, totalPage.intValue(), "Failed");
//TODO  assertEquals("check totalPage ", items.size(), pageSize.intValue(), "Failed");
    }
}
