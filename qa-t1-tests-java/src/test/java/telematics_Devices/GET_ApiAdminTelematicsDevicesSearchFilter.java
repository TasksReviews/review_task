package telematics_Devices;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetTelematicsDevicesInfoFromMongo;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GET_ApiAdminTelematicsDevicesSearchFilter extends AbstractClass {
    @And("^Создать (\\d+) новых ТМУ с параметрами$")
    public void createNewDeviceWithParameters(int arg0, DataTable dataTable) throws Throwable {
        List<List<String>> cells = dataTable.cells(0);
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i <= arg0; i++) {
            for (int j = 0; j < cells.get(0).size(); j++) {
                jsonObject.put(cells.get(0).get(j), cells.get(i).get(j));
            }
            JSONObject object = SendRequestToAPI.sendPostRequest(getProperty("host").concat("/VirtualTelematics/Tracker/Create"), jsonObject.toString(), getCurAuthAccToken("authorizedAccount_admPanel", "Developer"), false, true);
            String id = (String) object.get("id");
            setToAllureChechkedFields();
            SharedContainer.setContainer("Objects", object);
            SharedContainer.setContainer("IDS", id);
            TearDownExecutor.addVirtualTrackerToTeardown(id);
        }
    }

    @When("^Найти ТМУ, у которых поле \"([^\"]*)\" = \"([^\"]*)\"$")
    public void findDeviceWithField(String fieldName, String searchingFieldValue) throws Throwable {
    	String url = getProperty("adminPanelHost") + "/Api/Admin/Telematics/Devices/ByCustomerRelation?searchFilter=" + searchingFieldValue;
    	String token = getCurAuthAccToken("authorizedAccount_admPanel", "Developer");
        JSONObject object = (JSONObject) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token).get(0);
        JSONArray items = (JSONArray) object.get("items");
        ArrayList ids = SharedContainer.getFromContainer("IDS");
        for (int i = 0; i < items.size(); i++) {
            JSONObject item = (JSONObject) items.get(i);
            assertTrue("check field if exists: ", item.containsKey(fieldName));
            assertContains("check field id:", ids, item.get("id").toString(), "Failed");
            assertEquals("check field" + fieldName, item.get(fieldName), searchingFieldValue.toLowerCase());
            setToAllureChechkedFields();
        }
        SharedContainer.setContainer("Search", items);
    }
    @Then("^Получен список устройств, удовлетворяющих критерию поиска$")
    public void getListOfDevicesFromSearch() throws Throwable {
        ArrayList<JSONObject> search = SharedContainer.getFromContainer("Search");
        search.forEach(jsonObject -> {
            JSONObject id = (JSONObject) GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById((String) jsonObject.get("id")).get(0);
            try {
                String s = ReplyToJSON.extractPathFromJson(id, "_id.$oid");
                assertContains("check fields: ", jsonObject.get("id").toString(), s, "Failed");
            } catch (Exception e) {
            	log.error(ERROR,e);
            }
        });
    }
}
