package telematics_Devices;

import com.t1.core.*;
import com.t1.core.api.Customer;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.mongodb.GetTelematicsDevicesInfoFromMongo;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class GET_ApiAdminTelematicsDevicesrelationStatusRelated extends AbstractClass {


    private JSONObject response;

    @And("^Добавить нового клиента$")
    public void CreateNewClient() {
        log.debug("start customer create");
        Customer customer = new Customer();
        customer.customerCreate().get(0);
        SharedContainer.setContainer("customer", customer);
        customer.addToSharedContainer();
        log.debug("customer create END");
    }

    @Given("^Создать (\\d+) новых ТМУ с привязкой к клиенту$")
    public void createNewXDevicesWithRelation(int arg1) throws Throwable {
        Customer customer = (Customer) SharedContainer.getFromContainer("customer").get(0);
        // customer.customerCreate().get(0);
        for (int i = 0; i < arg1; i++) {
            TelematicVirtualTracker virtualTracker = new TelematicVirtualTracker(true);
            virtualTracker.createNewVirtualTracker();
            JSONObject request = new JSONObject();
            request.put("customerId", customer.getCustomerId());
            request.put("bindingDeviceIds", virtualTracker.getTrackerId());
            System.out.println("request = " + request);
            SendRequestToAPI.sendPostRequest(getProperty("adminPanelHost") + "/Api/Admin/Telematics/Device/SetCustomerRelation", request.toString(), getCurAuthAccToken("authorizedAccount_admPanel"), "application/json", true);
            JSONObject jsonObject1 = (JSONObject) GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(virtualTracker.getTrackerId()).get(0);
            SharedContainer.setContainer("tracker", jsonObject1);
            virtualTracker.setCustomerId(customer.getCustomerId());
            SharedContainer.setContainer("virtualTrackers", virtualTracker);
        }
    }

    @Given("^Создать (\\d+) новых ТМУ с привязкой к клиенту из предыдущего шага$")
    public void createNewXDevicesWithRelation2(int numOfDevices) {
        try {
            createNewXDevicesWithRelation(numOfDevices);
        } catch (Throwable e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
            assertTrue(false, "ошибка при попытке создать ТМУ:" + e.toString());
        }
    }

    @Given("^Создать (\\d+) новых ТМУ без привязки к клиенту$")
    public void createXDevicesWithoutRelations(int arg1) throws Throwable {
        Customer customer = (Customer) SharedContainer.getFromContainer("customer").get(0);
        for (int i = 0; i < arg1; i++) {
            TelematicVirtualTracker unVirtualTracker = new TelematicVirtualTracker(true);
            unVirtualTracker.createNewVirtualTracker();
            JSONObject request = new JSONObject();
            request.put("customerId", customer.getCustomerId());
            request.put("unbindingDeviceIds", unVirtualTracker.getTrackerId());
            SendRequestToAPI.sendPostRequest(getProperty("adminPanelHost") + "/Api/Admin/Telematics/Device/SetCustomerRelation", request.toString(), getCurAuthAccToken("authorizedAccount_admPanel", "Developer"), "", true);
            JSONObject jsonObject1 = (JSONObject) GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(unVirtualTracker.getTrackerId()).get(0);
            SharedContainer.setContainer("unVirtualTracker", jsonObject1);
        }
    }

    @When("^Запросить список всех устройств, привязанных к клиенту$")
    public void askForListOfRelatedDevices() throws Throwable {
//  #Проверить, что метод вернул все 3 созданных ТМУ с привязкой к клиенту
//  #Проверить, что метод не вернул 2 созданных ТМУ без привязки к клиенту
//  #Проверить все поля для каждого из 3 ТМУ - сравнить с MongoDb
//  #Проверить, что у всех полученных ТМУ isCustomerBound = true, CustomerId != "000000000000000000000000"
        Customer customer = (Customer) SharedContainer.getFromContainer("customer").get(0);
        String customerId = customer.getCustomerId();
        response = (JSONObject) SendRequestToAPI.sendGetRequest(getProperty("adminPanelHost") + "/Api/Admin/Telematics/Devices/ByCustomerId?objectRelationStatus=All&customerId=" + customerId, "GET", getProperty("dbuserAdmin"), getProperty("dbpasswordAdmin"), getCurAuthAccToken("authorizedAccount_admPanel", "Developer")).get(0);
        JSONArray items = (JSONArray) response.get("items");
        ArrayList<JSONObject> trackers = SharedContainer.getContainers().get("tracker");
        items.forEach((Consumer<JSONObject>) o -> {
            trackers.forEach(jsonObject -> {
                try {
                    assertTrue("", ReplyToJSON.extractPathFromJson(jsonObject, "CustomerId.$oid").equals(o.get("customerId")));
                    assertFalse("", ReplyToJSON.extractPathFromJson(jsonObject, "CustomerId.$oid").equals(o.get("000000000000000000000000")));

                } catch (Exception e) {
                    log.error(ERROR, e);
                }
                assertTrue("", jsonObject.get("Type").equals(o.get("type")));
                assertTrue("", o.get("isCustomerBound").equals(true));
            });
        });

    }

    @Then("^Получены только устройства, привязанные к клиенту$")
    public void isOnlyRelatedDevices() throws Throwable {
        Customer customer = (Customer) SharedContainer.getFromContainer("customer").get(0);
        JSONObject response = (JSONObject) SendRequestToAPI.sendGetRequest(getProperty("adminPanelHost") + "/Api/Admin/Telematics/Devices/ByCustomerId?objectRelationStatus=related&customerId=" + customer.getCustomerId(), "GET", getProperty("dbuserAdmin"), getProperty("dbpasswordAdmin"), getCurAuthAccToken("authorizedAccount_admPanel", "Developer")).get(0);
        JSONArray items = (JSONArray) response.get("items");
        ArrayList<JSONObject> trackers = SharedContainer.getContainers().get("tracker");
        items.forEach((Consumer<JSONObject>) o -> {
            trackers.forEach(jsonObject -> {
                try {
                    assertTrue("", ReplyToJSON.extractPathFromJson(jsonObject, "CustomerId.$oid").equals(o.get("customerId")));
                    assertFalse("", ReplyToJSON.extractPathFromJson(jsonObject, "CustomerId.$oid").equals(o.get("000000000000000000000000")));
                } catch (Exception e) {
                    log.error(ERROR, e);
                }
                assertTrue("", jsonObject.get("Type").equals(o.get("type")));
                assertTrue("", o.get("isCustomerBound").equals(true));
            });
        });
    }

    @Then("^Проверить, что метод вернул все (\\d+) созданных ТМУ с привязкой к клиенту, созданному ранее$")
    public void проверитьЧтоМетодВернулВсеСозданныхТМУСПривязкойККлиентуСозданномуРанее(int arg0) throws Throwable {
        ArrayList<JSONObject> trackers = SharedContainer.getContainers().get("tracker");
        Asserts.assertEquals("Проверено количество созданных ТМУ с привязкой к клиенту", trackers.size(), arg0, "количество созданных ТМУ с привязкой к клиенту передано неверно");
    }

    @Then("^Проверить, что метод не вернул (\\d+) созданных ТМУ без привязки к клиенту$")
    public void проверитьЧтоМетодНеВернулСозданныхТМУБезПривязкиККлиенту(int arg0) throws Throwable {
        JSONArray items = (JSONArray) response.get("items");
        Asserts.assertEquals("Проверено количество созданных ТМУ непривязанной к клиенту", items.size(), arg0, "количество созданных ТМУ непривязанной к клиенту передано неверно");
    }

    @Then("^Запросить список ТМУ с параметрами: customerRelationStatus = <All>, searchFilter = <имя кастомера>, pageSize = \"([^\"]*)\", pageNumber = \"([^\"]*)\"$")
    public void запроситьСписокТМУСПараметрамиCustomerRelationStatusAllSearchFilterИмяКастомераPageSizePageNumber(String pageSize, String pageNumber) throws Throwable {
        Customer customer = (Customer) SharedContainer.getFromContainer("customer").get(0);
        SendRequestToAPI.sendGetRequest(getProperty("adminPanelHost") + "/Api/Admin/Telematics/Devices/ByCustomerId?objectRelationStatus=All&customerId=" + customer.getCustomerId() + "&pageSize=" + pageSize + "&pageNumber=" + pageNumber + "", "GET", getProperty("dbuserAdmin"), getProperty("dbpasswordAdmin"), getCurAuthAccToken("authorizedAccount_admPanel", "Developer")).get(0);
        setToAllureChechkedFields();
    }
}
