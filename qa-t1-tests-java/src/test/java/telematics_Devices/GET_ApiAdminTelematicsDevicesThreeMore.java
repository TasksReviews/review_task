package telematics_Devices;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.mongodb.GetTelematicsDevicesInfoFromMongo;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GET_ApiAdminTelematicsDevicesThreeMore extends AbstractClass {

    @Given("^Создать (\\d+) новых ТМУ$")
    public void createXVirtualTrackers(int arg1) {
        for (int i = 0; i < arg1; i++) {
            TelematicVirtualTracker entity = new TelematicVirtualTracker(true);
            SharedContainer.setContainer("newTelematicDevice", entity.createNewVirtualTracker());
            entity.addToSharedContainer();
        }
    }

    @When("^Запросить информацию по всем устройствам$")
    public void askInfoForAllDevices() throws Exception {
// Вызвать метод /Api/Admin/Telematics/Devices без параметров
        JSONArray jsonArray = SendRequestToAPI.sendGetRequest(getProperty("host") + "/Telematics/Devices", "GET", getProperty("dbuserAdmin"), getProperty("dbpasswordAdmin"), getCurAuthAccToken("authorizedAccount_admPanel", "Developer"));
        JSONObject o = (JSONObject) jsonArray.get(0);
        JSONObject o1 = (JSONObject) o.get("pageInfo");
        assertEquals("check pageNumber", (long) o1.get("pageNumber"), 1);
        assertEquals("check pageSize", (long) o1.get("pageSize"), 25);
        setToAllureChechkedFields();
    }

    @Then("^Получены все созданные в предыдущих шагах ТМУ$")
    public void getInfoForAllDevicesRelated() throws Exception {
        JSONObject response = (JSONObject) SendRequestToAPI.sendGetRequest(getProperty("host") + "/Telematics/Devices?relationStatus=Related&", "GET", getProperty("dbuserAdmin"), getProperty("dbpasswordAdmin"), getCurAuthAccToken("authorizedAccount_admPanel", "Developer")).get(0);
        ArrayList newTelematicDevice = SharedContainer.getFromContainer("newTelematicDevice");
        Iterator iterator = newTelematicDevice.iterator();
        while (iterator.hasNext()) {
            ArrayList next = (ArrayList) iterator.next();
            for (int i = 0; i < next.size(); i++) {
                checkCode((JSONObject) next.get(i));
                checkId((JSONObject) next.get(i));
            }
        }
        JSONArray items = (JSONArray) response.get("items");
        JSONArray test = new JSONArray();
        for (Object item : items) {
            JSONObject o = (JSONObject) item;
            Object o1 = o.get("id");
            test.add(o1);
        }
    }

    private void checkId(JSONObject o) throws Exception {
        String oid = ReplyToJSON.extractPathFromJson(o, "_id.$oid");
        JSONObject tracker = (JSONObject) GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(oid).get(0);
        String trackerOid = ReplyToJSON.extractPathFromJson(tracker, "_id.$oid");
        assertEquals("check", oid, trackerOid, "failed");
    }

    private void checkCode(JSONObject o) throws Exception {
        String oid = ReplyToJSON.extractPathFromJson(o, "_id.$oid");
        String code = ReplyToJSON.extractPathFromJson(o, "Code");
        JSONObject tracker = (JSONObject) GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(oid).get(0);
        String trackerCode = ReplyToJSON.extractPathFromJson(tracker, "Code");
        assertEquals("check", code, trackerCode, "failed");
    }
}