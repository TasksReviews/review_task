package telematics_Devices;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.api.Customer;
import com.t1.core.mongodb.GetTelematicsDevicesInfoFromMongo;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GET_ApiAdminTelematicDevicesNotRelated extends AbstractClass {


    @When("^Запросить список всех устройств, непривязанных к клиенту$")
    public void askForAllDevicesUnRelatedToCustomer() throws Throwable {
//#Проверить, что метод вернул все 3 созданных ТМУ без привязки к клиенту
//#Проверить, что метод не вернул 2 созданных ТМУ с привязкой к клиенту, созданному ранее
//#Проверить все поля для каждого из 3 ТМУ - сравнить с MongoDb
//#Проверить, что у всех полученных ТМУ isCustomerBound = false, CustomerId = "000000000000000000000000"
        Customer customer = (Customer) SharedContainer.getFromContainer("customer").get(0);
        System.out.println("customer = " + customer.getInitMap());
        ArrayList virtualTracker = SharedContainer.getFromContainer("tracker");
        ArrayList unVirtualTracker = SharedContainer.getFromContainer("unVirtualTracker");
        JSONObject response = (JSONObject) SendRequestToAPI.sendGetRequest(getProperty("adminPanelHost") + "/Api/Admin/Telematics/Devices/ByCustomerId?objectRelationStatus=notRelated&customerId=" + "000000000000000000000000", "GET", getProperty("dbuserAdmin"), getProperty("dbpasswordAdmin"), getCurAuthAccToken("authorizedAccount_admPanel", "Developer")).get(0);
        JSONArray items = (JSONArray) response.get("items");
        JSONArray ids = new JSONArray();
        List<String> unStringsIds = new ArrayList<>();
        List<String> stringsIds = new ArrayList<>();
        for (Object item : items) {
            JSONObject o = (JSONObject) item;
            ids.add(o.get("id"));
            assertTrue("check ", o.get("isCustomerBound").equals(false), "Failed");
            assertTrue("check ", o.get("customerId").equals("000000000000000000000000"), "Failed");
        }
        for (Object anUnVirtualTracker : unVirtualTracker) {
            JSONObject object = (JSONObject) anUnVirtualTracker;
            String path = ReplyToJSON.extractPathFromJson(object, "_id.$oid");
            unStringsIds.add(path);
        }
        for (Object aVirtualTracker : virtualTracker) {
            JSONObject object = (JSONObject) aVirtualTracker;
            String path = ReplyToJSON.extractPathFromJson(object, "_id.$oid");
            stringsIds.add(path);
        }

        for (Object anUnVirtualTracker : unVirtualTracker) {
            JSONObject object = (JSONObject) anUnVirtualTracker;
            String oid = ReplyToJSON.extractPathFromJson(object, "_id.$oid");
            String customerid = ReplyToJSON.extractPathFromJson(object, "CustomerId.$oid");
            String code = ReplyToJSON.extractPathFromJson(object, "Code");
            String type = ReplyToJSON.extractPathFromJson(object, "Type");

            JSONObject jsonObject = (JSONObject) GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(oid).get(0);
            assertTrue("check ", jsonObject.toJSONString().contains(oid));
            assertTrue("check ", jsonObject.toJSONString().contains(customerid));
            assertTrue("check ", jsonObject.containsValue(code));
            assertTrue("check ", jsonObject.toJSONString().contains(type));
        }
    }

    @Then("^Получены только устройства, непривязанные к клиенту$")
    public void getOnlyDevicesWithoutRelations() throws Throwable {
        Customer customer = (Customer) SharedContainer.getFromContainer("customer").get(0);
        JSONObject response = (JSONObject) SendRequestToAPI.sendGetRequest(getProperty("adminPanelHost") + "/Api/Admin/Telematics/Devices/ByCustomerId?objectRelationStatus=notRelated&customerId=" + "000000000000000000000000", "GET", getProperty("dbuserAdmin"), getProperty("dbpasswordAdmin"), getCurAuthAccToken("authorizedAccount_admPanel", "Developer")).get(0);
        JSONArray items = (JSONArray) response.get("items");
        for (Object item1 : items) {
            JSONObject item = (JSONObject) item1;
            assertEquals("check isCustomerBound", item.get("isCustomerBound"), false, "Failed");
            assertTrue("check customerId", item.get("customerId").equals("000000000000000000000000"), "Failed");
        }
    }
}