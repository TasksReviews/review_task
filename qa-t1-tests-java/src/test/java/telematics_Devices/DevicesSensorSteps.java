package telematics_Devices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.mongodb.GetInfoFromMongo;
import com.t1.core.mongodb.MongoConnection;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class DevicesSensorSteps  extends AbstractClass{
	
	@Given("^Добавить в маппинг созданного ТМУ (\\d+) датчик с типом = <([^>]*)>, с источником данных = <([^>]*)>, с отображаемым именем = <([^>]*)>, с paramId = <([^>]*)>$")
	public void addSensorToDeviceX(int deviceIndex, String sensorType, String sensorSource, String sensorViewName, int paramId)  {
		TelematicVirtualTracker telematics_device = null;
		if(deviceIndex== -1){
			telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		} else {
			telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers").get(deviceIndex-1);
		}
		Document sensor = new Document();
		sensor.put("Type", getSensorType(sensorType));
		if (sensor.get("Type").equals("Mileage") || sensor.get("Type").equals("Acceleration")
				|| sensor.get("Type").equals("Temperature") || sensor.get("Type").equals("MachineHours")
				|| sensor.get("Type").equals("Other")) {
			sensor.put("_t", "CalibratableCommonParameter");
			sensor.put("IsBool", false);
			sensor.put("IsCalibratable", true);
			sensor.put("StartValue", 0.0);
			sensor.put("Coefficient1", 1.0);
			sensor.put("Coefficient2", 0.0);
			sensor.put("CorrectedValue", 0.0);
		} else {
			sensor.put("_t", "BoolParameter");
			sensor.put("IsBool", true);
			sensor.put("IsCalibratable", false);
			sensor.put("Value", null);
		}
		sensor.put("ParamId", paramId);
		sensor.put("SourceName", sensorSource);
		sensor.put("ViewName", sensorViewName);
		sensor.put("Unit", "some_unit");
		sensor.put("ViewEnabled", true);
		sensor.put("IsEnabled", true);
		sensor.put("IsAvailable", true);
		sensor.put("IsExtended", true);

		com.mongodb.client.MongoCollection<Document> collection = MongoConnection.getMongoCollection("Devices");
		Bson filter = Filters.eq("_id", new ObjectId(telematics_device.getTrackerId()));
		Bson updateField = new Document("$push", new Document("Mapping", sensor));
		UpdateOptions options = new UpdateOptions().upsert(true);
		UpdateResult updateResult = collection.updateOne(filter, updateField, options);
		long matchedCount = updateResult.getMatchedCount();
		long modifiedCount = updateResult.getModifiedCount();
		textToAllureRep("ответ MongoDb", "подходящих документов для обновления=" + matchedCount+ ", обновлено полей согласно запрошенным параметрам=" + modifiedCount);
		log.debug("added " + modifiedCount + " sensor to device "+telematics_device.getTrackerId());

		setToAllureChechkedFields();
	}
	
	
	@Then("^Добавить в маппинг созданного ТМУ датчик с типом = <([^>]*)>, с источником данных = <([^>]*)>, с отображаемым именем = <([^>]*)>, с paramId = <([^>]*)>$")
	public void addSensorToDevice(String sensorType, String sensorSource, String sensorViewName, int paramId) {
		addSensorToDeviceX(-1, sensorType, sensorSource, sensorViewName, paramId);
	}

	@Then("^В маппинге созданного ТМУ изменить для датчика с paramId = <(.*)> тип на <(.*)>, источник данных на <(.*)>, отображаемое имя на <(.*)>$")
	public void updateSensorRelatedToDevice(int paramId, String sensorType, String sensorSource, String sensorViewName) {
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		Document sensor = new Document();
		sensor.put("Type", getSensorType(sensorType));
		if (sensor.get("Type").equals("Mileage") || sensor.get("Type").equals("Acceleration")
				|| sensor.get("Type").equals("Temperature") || sensor.get("Type").equals("MachineHours")
				|| sensor.get("Type").equals("Other")) {
			sensor.put("_t", "CalibratableCommonParameter");
			sensor.put("IsBool", false);
			sensor.put("IsCalibratable", true);
			sensor.put("StartValue", 0.0);
			sensor.put("Coefficient1", 1.0);
			sensor.put("Coefficient2", 0.0);
			sensor.put("CorrectedValue", 0.0);
		} else {
			sensor.put("_t", "BoolParameter");
			sensor.put("IsBool", true);
			sensor.put("IsCalibratable", false);
			sensor.put("Value", null);
		}
		sensor.put("SourceName", sensorSource);
		sensor.put("ViewName", sensorViewName);
		//

		// find needed sensor index
		Bson filter = Filters.eq("_id", new ObjectId(telematics_device.getTrackerId()));
		JSONObject mapping = GetInfoFromMongo.getInfoByCustomFilters("Devices", filter, Arrays.asList("Mapping"), false).get(0);
		ArrayList<JSONObject> foundSensors = (ArrayList<JSONObject>) mapping.get("Mapping");
		int index = -1;
		for (int i = 0; i < foundSensors.size(); i++) {
			if (Integer.parseInt(foundSensors.get(i).get("ParamId").toString()) == paramId) {
				index = i;
				break;
			}
		}
		assertTrue(index >= 0, "sensor with paramId " + paramId + " wasn't found in MongoDB");
		// put to doc common old params
		sensor.put("ParamId", Integer.parseInt(foundSensors.get(index).get("ParamId").toString()));
		sensor.put("SourceName", foundSensors.get(index).get("SourceName"));
		sensor.put("ViewName", foundSensors.get(index).get("ViewName"));
		sensor.put("Unit", foundSensors.get(index).get("Unit"));
		sensor.put("ViewEnabled", foundSensors.get(index).get("ViewEnabled"));
		sensor.put("IsEnabled", foundSensors.get(index).get("IsEnabled"));
		sensor.put("IsAvailable", foundSensors.get(index).get("IsAvailable"));
		sensor.put("IsExtended", foundSensors.get(index).get("IsExtended"));

		com.mongodb.client.MongoCollection<Document> collection = MongoConnection.getMongoCollection("Devices");
		filter = Filters.eq("_id", new ObjectId(telematics_device.getTrackerId()));
		Bson updateField = new Document("$set", new Document("Mapping." + index, sensor));
		UpdateOptions options = new UpdateOptions().upsert(true);
		UpdateResult updateResult = collection.updateOne(filter, updateField, options);
		long matchedCount = updateResult.getMatchedCount();
		long modifiedCount = updateResult.getModifiedCount();
		textToAllureRep("ответ MongoDb", "подходящих документов для обновления=" + matchedCount+ ", обновлено полей согласно запрошенным параметрам=" + modifiedCount);
		log.debug("updated " + modifiedCount + " fileds");
		setToAllureChechkedFields();

	}

	@Then("^В маппинге созданного ТМУ удалить датчик с paramId = <(.*)>$")
	public void deleteSensorRelatedToDevice(int paramId) {
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		Document sensor = new Document();
		Bson filter = Filters.eq("_id", new ObjectId(telematics_device.getTrackerId()));
		JSONObject mapping = GetInfoFromMongo.getInfoByCustomFilters("Devices", filter, Arrays.asList("Mapping"), false).get(0);
		ArrayList<JSONObject> foundSensors = (ArrayList<JSONObject>) mapping.get("Mapping");
		int index = -1;
		for (int i = 0; i < foundSensors.size(); i++) {
			if (Integer.parseInt(foundSensors.get(i).get("ParamId").toString()) == paramId) {
				index = i;
				break;
			}
		}
		assertTrue(index >= 0, "sensor with paramId " + paramId + " wasn't found in MongoDB");

		sensor.put("ParamId", Integer.parseInt(foundSensors.get(index).get("ParamId").toString()));
		com.mongodb.client.MongoCollection<Document> collection = MongoConnection.getMongoCollection("Devices");
		filter = Filters.eq("_id", new ObjectId(telematics_device.getTrackerId()));
		Bson updateField = new Document("$pull", new Document("Mapping", sensor));

		UpdateResult updateResult = collection.updateOne(filter, updateField);
		long matchedCount = updateResult.getMatchedCount();
		long modifiedCount = updateResult.getModifiedCount();
		textToAllureRep("ответ MongoDb", "подходящих документов для обновления=" + matchedCount+ ", обновлено полей согласно запрошенным параметрам=" + modifiedCount);
		log.debug("updated " + modifiedCount + " fileds");

		setToAllureChechkedFields();

	}

	public String getSensorType(String input) {
		String result = "";
		ArrayList<String> possibleTypes = new ArrayList<String>();
		possibleTypes.add("HoodSignal");
		possibleTypes.add("DoorsSignal");
		possibleTypes.add("TrunkSignal");
		possibleTypes.add("Ignition");
		possibleTypes.add("PanicButton");
		possibleTypes.add("Mileage");
		possibleTypes.add("Acceleration");
		possibleTypes.add("Temperature");
		possibleTypes.add("MachineHours");
		possibleTypes.add("Other");

		HashMap<String, String> possibleTypesRu = new HashMap<>();
		possibleTypesRu.put("Капот", "HoodSignal");
		possibleTypesRu.put("Все двери", "DoorsSignal");
		possibleTypesRu.put("Багажник", "TrunkSignal");
		possibleTypesRu.put("Зажигание", "Ignition");
		possibleTypesRu.put("Кнопка тревоги", "PanicButton");
		possibleTypesRu.put("Пробег", "Mileage");
		possibleTypesRu.put("Ускорение", "Acceleration");
		possibleTypesRu.put("Температура", "Temperature");
		possibleTypesRu.put("Моточасы", "MachineHours");
		possibleTypesRu.put("Иной тип датчика", "Other");
		possibleTypesRu.put("Иной тип ", "Other");
		possibleTypesRu.put("Иной", "Other");
		if (input.equals(null) || input.equals("")) {
			result = pickRandomFromList(possibleTypes);
		} else if (possibleTypesRu.keySet().contains(input)) {
			result = possibleTypesRu.get(input);
		} else if (possibleTypes.contains(input)) {
			result = input;
		} else {
			assertTrue(false, "получен некорректный тип датчика: " + input);
		}
		return result;
	}
}
