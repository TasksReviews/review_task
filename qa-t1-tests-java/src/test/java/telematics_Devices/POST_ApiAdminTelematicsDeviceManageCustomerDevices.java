package telematics_Devices;

import com.t1.core.SharedContainer;
import com.t1.core.api.Customer;
import com.t1.core.api.TelematicVirtualTracker;

import cucumber.api.java.en.Then;

public class POST_ApiAdminTelematicsDeviceManageCustomerDevices {

	@Then("^В Админке отвязать ТМУ от клиента$")
	public void в_Админке_отвязать_ТМУ_от_клиента()  {
		Customer customerFromPrevStep = (Customer) SharedContainer.getLastObjectFromSharedContainer("customers");
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		telematics_device.setCustomerId(customerFromPrevStep.getCustomerId());
		telematics_device.unbindDeviceFromCustomer();
		telematics_device.updateInSharedContainer();
	}

	@Then("^В Админке привязать ТМУ к клиенту$")
	public void в_Админке_привязать_ТМУ_к_клиенту() {
		Customer customerFromPrevStep = (Customer) SharedContainer.getLastObjectFromSharedContainer("customers");
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		telematics_device.setCustomerId(customerFromPrevStep.getCustomerId());
		telematics_device.bindDeviceToCustomer();
		telematics_device.updateInSharedContainer();
	//	  SendRequestToAPI.sendPostRequest(getProperty("adminPanelHost") + "/Api/Admin/Telematics/Device/SetCustomerRelation", request.toString(), getCurAuthAccToken("authorizedAccount_admPanel", "Developer"), "", true);
    //      SharedContainer.setContainer("newTelematicDevice", virtualTracker);
	}
	
}
