package telematics_Devices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.TearDownExecutor;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.mongodb.GetInfoFromMongo;
import com.t1.core.mongodb.InsertSensorThresholds;
import com.t1.core.mongodb.MongoConnection;

import cucumber.api.java.en.Given;

public class DevicesSensorTresholdsSteps extends AbstractClass {
//This class supposed to update relation between sensors and devices into MongoDb

	@Given("^Для созданного ТМУ (\\d+) добавить запись об ограничении значений датчика <(.*)> c нижней границей = <(.*)>, с верхней границей = <(.*)>, с началом ограничения = <([^>]*)>, с концом ограничения = <([^>]*)>, с Enabled = <(.*)>$")
	public void addSensorTresholdForCreatedDeviceX(int deviceIndex, String sensorSource, double minTresh, double maxTresh, List startDateArr, List endDateArr, boolean isenabled) {
		TelematicVirtualTracker telematics_device = null;
		if(deviceIndex== -1){
			telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		} else {
			telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers").get(deviceIndex-1);
		}
		Document sensorTreshold = new Document();
		sensorTreshold.put("DeviceId", new ObjectId(telematics_device.getTrackerId()));
		sensorTreshold.put("SourceName",sensorSource);
		sensorTreshold.put("StartDate",dateTimeCorrectionFromInputArr(startDateArr));
		if(!endDateArr.isEmpty() && ! endDateArr.get(0).equals("null") ){
			sensorTreshold.put("EndDate", dateTimeCorrectionFromInputArr(endDateArr));
		}
		sensorTreshold.put("Min",minTresh);
		sensorTreshold.put("Max",maxTresh);
		sensorTreshold.put("Enabled",isenabled);
		InsertSensorThresholds.insertDocument(sensorTreshold, true);
	}
	
	@Given("^Для созданного ТМУ добавить запись об ограничении значений датчика <(.*)> c нижней границей = <(.*)>, с верхней границей = <(.*)>, с началом ограничения = <([^>]*)>, с концом ограничения = <([^>]*)>, с Enabled = <(.*)>$")
	public void addSensorTresholdForCreatedDevice(String sensorSource, double minTresh, double maxTresh, List startDateArr, List endDateArr, boolean isenabled) {
		addSensorTresholdForCreatedDeviceX(-1, sensorSource, minTresh, maxTresh, startDateArr, endDateArr, isenabled);
	}


	@Given("^Для созданного ТМУ для всех записей об ограничении значений датчика <(.*)> изменить \"Enabled\" на <(.*)>$")
	public void updateEnabledSenosorTresholdParamForCreatedDevice(String sensorSource,boolean isenabled) {
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		Bson filter = Filters.and (Filters.eq ("SourceName",sensorSource ), Filters.eq ("DeviceId", new ObjectId(telematics_device.getTrackerId()) ) ) ;
		com.mongodb.client.MongoCollection<Document> collection = MongoConnection.getMongoCollection("SensorThresholds");

		Bson updateField = new Document("$set", new Document("Enabled", isenabled));
		UpdateOptions options = new UpdateOptions().upsert(true);
		UpdateResult updateResult = collection.updateMany(filter, updateField, options);
		long matchedCount = updateResult.getMatchedCount();
		long modifiedCount = updateResult.getModifiedCount();
		textToAllureRep("ответ MongoDb", "подходящих документов для обновления=" + matchedCount + ", обновлено полей согласно запрошенным параметрам=" + modifiedCount);
		log.debug("updated " + modifiedCount + " fileds");
		
		setToAllureChechkedFields();
	}

//	@Given("^Для созданного ТМУ для всех записей об ограничении значений датчика <источник> изменить \"([^\"]*)\" на <true>$")
//	public void для_созданного_ТМУ_для_всех_записей_об_ограничении_значений_датчика_источник_изменить_на_true(String arg1) {
//	    
//	}
	
	@Given("^Для созданного ТМУ (\\d+) удалить все записи об ограничении значений датчика <(.*)>$")
	public void deleteSenosorTresholdForCreatedDeviceX(int deviceIndex, String sensorSource) {
		TelematicVirtualTracker telematics_device = null;
		if(deviceIndex== -1){
			telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		} else {
			telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers").get(deviceIndex-1);
		}
		Bson filter = Filters.and (Filters.eq ("SourceName",sensorSource ), Filters.eq ("DeviceId", new ObjectId(telematics_device.getTrackerId()) ) ) ;
		com.mongodb.client.MongoCollection<Document> collection = MongoConnection.getMongoCollection("SensorThresholds");
		
		ArrayList foundSensorThresholds = (ArrayList) GetInfoFromMongo.getInfoByCustomFilters("SensorThresholds", filter, Arrays.asList("_id"), false);
		List<String> foundIds = ReplyToJSON.extractListPathFromJson(foundSensorThresholds, "$..$oid");
		DeleteResult deleteResult = collection.deleteMany(filter);
		assertEquals("devices sensor's tresholds deleted", deleteResult.getDeletedCount(), foundSensorThresholds.size(), "не было удалено ожидаемое количество документов.");
		//remove deleted thresholds from teardown
		for (String id : foundIds){
			TearDownExecutor.removeEntityFromTeardown(id,"insertedSensorThresholdsRecords");
		}	
		setToAllureChechkedFields();
	}

	@Given("^Для созданного ТМУ удалить все записи об ограничении значений датчика <(.*)>$")
	public void deleteSenosorTresholdForCreatedDevice(String sensorSource) {
		deleteSenosorTresholdForCreatedDeviceX(-1,sensorSource);
	}

}
