package telematics_Devices;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.mongodb.GetTelematicsDevicesInfoFromMongo;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GET_ApiAdminTelematicsDevicesPageNumber extends AbstractClass {
    JSONObject response = new JSONObject();

    @When("^Запросить информацию по устройствам на странице \"([^\"]*)\"$")
    public void askInfoForTrackerOnPage(String arg0) throws Throwable {
        JSONObject newCustomer = (JSONObject) SharedContainer.getFromContainer("newCustomer").get(0);
        System.out.println("newCustomer = " + newCustomer);
        String path = ReplyToJSON.extractPathFromJson(newCustomer, "_id.$oid");
        response = (JSONObject) SendRequestToAPI.sendGetRequest(getProperty("host") + "/Telematics/Devices?relationStatus=Related&pageSize=1&pageNumber=" + arg0 + "&customerId=" + path, "GET", getProperty("dbuserAdmin"), getProperty("dbpasswordAdmin"), getCurAuthAccToken("authorizedAccount_admPanel", "Developer")).get(0);
        JSONArray items = (JSONArray) response.get("items");
        for (int i = 0; i < items.size(); i++) {
            JSONObject pageInfo = (JSONObject) response.get("pageInfo");
            JSONObject object = (JSONObject) items.get(i);
            assertEquals("check pageSize", pageInfo.get("pageSize"), (long) 1);
            assertEquals("check pageNumber", pageInfo.get("pageNumber"), Long.parseLong(arg0));
            String id = ReplyToJSON.extractPathFromJson(object, "id");
            GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(id);
        }

    }

    @Then("^Получена запрошенная страница$")
    public void getPage() throws Throwable {
        log.debug(response.get("pageInfo"));
        log.debug(response.get("items"));
        log.debug("end of test >>>");
    }
}
