package telematics_Devices;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Customer;
import com.t1.core.api.TelematicVirtualTracker;

import cucumber.api.java.en.Then;

public class GET_ApiAdminTelematicsDevicesByCustomerRelation extends AbstractClass {
    JSONObject response = new JSONObject();

    
    
    @Then("^Запросить список ТМУ с параметрами: customerRelationStatus = <All>, searchFilter = <имя кастомера>, pageSize = (\\d+), pageNumber = (\\d+)$")
    public void getDevicesRelatedToCutomerName_FilterBy_pageSize_pageNumber(int pageSize, int pageNumber) {
    	Customer customerFromPrevStep = (Customer) SharedContainer.getLastObjectFromSharedContainer("customers");
    	JSONObject	devicesListReply = new TelematicVirtualTracker(false).getVirtualTrackersList( pageSize,  pageNumber, customerFromPrevStep.getName(), "All" );
    	SharedContainer.setContainer("devicesListReply", devicesListReply);
    	
    }
    
}
