package telematics_Devices;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicVirtualTracker;

import cucumber.api.java.en.Then;

public class ApiAdminTelematicsDevicesStepsWithChecks  extends AbstractClass{

	
	@Then("^Проверить, что получено (\\d+) ТМУ$")
	public void проверить_что_получено_ТМУ(int expectedNumOfDevices) {
		try {
			JSONObject devicesListReply = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("devicesListReply");
			List<JSONObject> devicesList = ReplyToJSON.extractListPathFromJson(devicesListReply, "$.items");
			assertEquals("API: check num of received devices", devicesList.size(), expectedNumOfDevices, "получено некорректное количество ТМУ");
			setToAllureChechkedFields();
		} catch (Exception e) {
			log.error(ERROR, e);
//			assertTrue(false, e.toString());
			setToAllureChechkedFields();
		}
	}

	@Then("^Проверить, что для параметра \"([^\"]*)\" получено значение = (.*)$")
	public void проверить_что_для_параметра_получено_значение(String parameter, String expectedValue) {
		try {
			JSONObject devicesListReply = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("devicesListReply");
			String actualValue =  ReplyToJSON.extractPathFromJson(devicesListReply, "$.pageInfo."+parameter);
			assertEquals("API: check value of \""+parameter+"\"", actualValue, expectedValue, "получено некорректное значение для "+parameter);
			setToAllureChechkedFields();
		} catch (Exception e) {
			log.error(ERROR, e);
			assertTrue(false, e.toString());
			setToAllureChechkedFields();
		}
 
	}

	@Then("^Проверить, что получены ТМУ из списка (.*)$")
	public void проверить_что_получены_ТМУ_из_списка_ТМУ(List expectedDevices) {
		try {
			JSONObject devicesListReply = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("devicesListReply");
			List<TelematicVirtualTracker> devicesFromPrevStep =	(List<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers");
			List<JSONObject> devicesList = ReplyToJSON.extractListPathFromJson(devicesListReply, "$.items");
			assertEquals("API: check num of received devices equals to expecting", devicesList.size(), expectedDevices.size(), 
					"получено некорректное количество ТМУ");
			int i =0;
			for (Object expectedDevice :  expectedDevices){
				String expectedDeviceStr = expectedDevice.toString();
				int expDevIndex = Integer.parseInt(expectedDeviceStr.substring(expectedDeviceStr.length()-1, expectedDeviceStr.length()));
				TelematicVirtualTracker device=	devicesFromPrevStep.get(expDevIndex-1);
				assertEquals("API: check reply contains id of "+expectedDeviceStr, devicesList.get(i).get("id"), device.getTrackerId(), 
						"получен некорректный id для "+expectedDeviceStr);
				i++;
			}
			setToAllureChechkedFields();
		} catch (Exception e) {
			log.error(ERROR, e);
			assertTrue(false, e.toString());
			setToAllureChechkedFields();
		}
	    
	}

}
