package telematics_Devices;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicVirtualTracker;

import cucumber.api.java.en.Then;

public class POST_ApiAdminTelematicsDeviceUpdate extends AbstractClass{

	
	@Then("^В Админке отредактировать текстовые поля ТМУ$")
	public void editDeviceInAdmPanel() {
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		JSONObject initJson = new InitDefValues().initVirtualTracker(getProperty("stdPattern"));  //stdPattern

		telematics_device.setType(initJson.get("type").toString());
		telematics_device.setModel(initJson.get("model").toString());
		telematics_device.setManufacturer(initJson.get("manufacturer").toString());
		telematics_device.setSimCard(initJson.get("simCard").toString());

        telematics_device.putUrlParameters("type", telematics_device.getType());
        telematics_device.putUrlParameters("model", telematics_device.getModel());
        telematics_device.putUrlParameters("manufacturer", telematics_device.getManufacturer());
        telematics_device.putUrlParameters("simCard", telematics_device.getSimCard());
		
		telematics_device.updateVirtualTrackerInAdminPanel();
		telematics_device.updateInSharedContainer();
	}
}
