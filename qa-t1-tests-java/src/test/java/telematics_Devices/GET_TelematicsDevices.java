package telematics_Devices;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicsTrack;
import com.t1.core.mongodb.GetTelematicsDevicesInfoFromMongo;

import cucumber.api.java.en.Then;

public class GET_TelematicsDevices extends AbstractClass{
	public TelematicsTrack device;
	@Then("^получить случайное ТМУ из БД$")
	public void getRandomTelematicsDevice(){
		 final JSONObject randomDevice = (JSONObject) GetTelematicsDevicesInfoFromMongo.getRandomTelematicsDeviceFromMongo().get(0);
//		log.debug("Start>> employee_create");
//		employee = new Employee();
//		JSONObject createdemployee = employee.createNewEmployee().get(0);
		SharedContainer.setContainer("json_telematics_device", (new ArrayList(){{add(randomDevice);}}));
//		//employee.deleteEmployee(employee.employeeId);
	}
	
	@Then("^получить случайное ТМУ тестового клиента из БД$")
	public void getRandomTelematicsDeviceTestCustomer(){
		 final JSONObject randomDevice = (JSONObject) GetTelematicsDevicesInfoFromMongo.getRandomTelematicsDeviceTestCustomerFromMongo().get(0);
		SharedContainer.setContainer("json_telematics_device", (new ArrayList(){{add(randomDevice);}}));

	}
	
}
