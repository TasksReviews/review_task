package telematics_Devices;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Geozone;
import com.t1.core.api.NotificationRule;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.TelematicsTrack;
import com.t1.core.mongodb.GetDriveStylePenaltyScores;
import com.t1.core.mongodb.InsertTrackPointToDeviceFullRecords;

import cucumber.api.java.en.Then;

public class FullDeviceRecordsPreparation extends AbstractClass{//this class put track points according to rule type to have new events generated
	public TelematicsTrack device;

	@SuppressWarnings("unchecked")
	@Then("^подготовить тестовый трек для созданного ТМУ для срабатывания созданного правила, критерий \"([^\"]*)\" значение=\"([^\"]*)\", критерий_2 \"([^\"]*)\" значение_2=\"([^\"]*)\"$")
	public void insertTrackPointsForNotifEventsToFDR(String limitationName, String limitationValue,String limitationName2, String limitationValue2){//Notifications events, relates to rules!!!
		try{
			 JSONObject insertedTrackPoint = new JSONObject();
		NotificationRule rule = (NotificationRule) SharedContainer.getFromContainer("notificationRules").get(0);
		log.debug("Start>> insertion");

		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		 JSONObject fieldsToUpdate = new JSONObject();
		 
/*		 
события по длительности - нахождение в геозоне и стоянка - по расписанию чекают ПТЗ и правила
также работает превышение пробега - по расписанию

остальные правила проверяются на потоке ПТЗ
 проверка раз в 3 минуты делается, если ТС стоит, скажем, час>N и не присылает пакеты, а последняя точка зафиксировала стоянку, то через N минут должно сработать правило 
 - оно не может сработать на потоке, т.к. пакетов нет, поэтому такая проверка дополнительно реализована по расписанию
+		 GeozoneEntrance
+		 GeozoneExit
+		 GeozoneStanding
+		 UserSpeedlimit 
		 StopEvent
-		 IdleEvent
+		 TrafficRulesSpeedLimit
+		 SharpTurn
+		 SharpSpeedup
+		 SharpBraking
+		 SharpTurnSpeedLimit
		 Refueling
		 Discharge
		 FuelConsumption
+		 PanicButton
+		 TelematicDeviceMechanismOn
+		 TelematicDeviceMechanismOff
-		 MileageExceedance
	*/
		 String[] possibleLimitationDoorsValues = new String[] {"Ignition","Hood","Trunk","LeftFrontDoor","LeftBackDoor","RightFrontDoor","RightBackDoor"};
		 String pref="";
		 if(limitationValue.equals("Hood") || limitationValue.equals("Trunk")) pref = "Signal";
		 
		 switch (rule.getRoadEventType()) {
			case "UserSpeedlimit": {
			 int ruleLimitation = Integer.parseInt(rule.getLimitationValue());
			 fieldsToUpdate.put("Speed", (ruleLimitation+100));
			 fieldsToUpdate.put("Speed_upd", true);
			 fieldsToUpdate.put("Ignition", true);
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			  break;
		 }
			case "TrafficRulesSpeedLimit" : {
			 int ruleLimitation = Integer.parseInt(rule.getLimitationValue());
			 //overspeed
			// "xgeo_Speedlimit" : 20 + "Overspeed" : 20 < "Speed" : 46
			 fieldsToUpdate.put("xgeo_Speedlimit", 20);
			 fieldsToUpdate.put("Speed", (ruleLimitation+21));
			 fieldsToUpdate.put("Speed_upd", true);
			 fieldsToUpdate.put("Ignition", true);
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			  break;
		 }
			case "SharpTurn" : {//резкий поворот
			 double ruleLimitation = Double.parseDouble(rule.getLimitationValue());
			 fieldsToUpdate.put("Ignition", true); 
			 fieldsToUpdate.put("AccelerationY", (ruleLimitation+0.19d)); 
			 fieldsToUpdate.put("AccelerationY_upd", true);
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
		//	 speedLimit
			  break;
		 }
			case "SharpSpeedup" : {//резкое ускорение
			 double ruleLimitation = Double.parseDouble(rule.getLimitationValue());
			 fieldsToUpdate.put("Ignition", true); 
			 fieldsToUpdate.put("AccelerationX",(ruleLimitation+ 0.5d)); 
			 fieldsToUpdate.put("AccelerationX_upd", true);
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
		//	 acceleration
			  break;
		 }
			case "SharpBraking" : {//резкое торможение
			 double ruleLimitation = Double.parseDouble(rule.getLimitationValue());
			 fieldsToUpdate.put("Ignition", true);
			 fieldsToUpdate.put("AccelerationX", (0.0d)); 
			 fieldsToUpdate.put("AccelerationX_upd", true); 
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
		//	 acceleration
			  break;
		 }
			case "SharpTurnSpeedLimit" : {//резкое ускорение
			 int ruleLimitation = Integer.parseInt(rule.getLimitationValue());
			 fieldsToUpdate.put("Ignition", true); 
			 fieldsToUpdate.put("AccelerationY", (0.1d)); 
			 fieldsToUpdate.put("AccelerationY_upd", true);
			 
			 fieldsToUpdate.put("Speed", (ruleLimitation+51));
			 fieldsToUpdate.put("Speed_upd", true);
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			  //	 speedlimit
			  break;
		 }
		 
			case "PanicButton" : {
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 fieldsToUpdate.put("PanicButton_upd", true);
			 fieldsToUpdate.put("PanicButton", true);
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			  break;
		 }
		 
		    
			case "TelematicDeviceMechanismOn" : {
			 if(Arrays.asList(possibleLimitationDoorsValues).contains(limitationValue)){
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
					 fieldsToUpdate.put(limitationValue+pref+"_upd", true);
					 fieldsToUpdate.put(limitationValue+pref, true);
			  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 
		 }break;
			 }
			case "TelematicDeviceMechanismOff" : {
			 if(Arrays.asList(possibleLimitationDoorsValues).contains(limitationValue)){
				 fieldsToUpdate.put(limitationValue+pref+"_upd", false);
				 fieldsToUpdate.put(limitationValue+pref, true);
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
				 fieldsToUpdate.put(limitationValue+pref+"_upd", true);
				 fieldsToUpdate.put(limitationValue+pref, false);
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 }
			 break;
		 }
			case "Refueling" : {
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 fieldsToUpdate.put("Fuel", "150");
			 fieldsToUpdate.put("Fuel_upd", true);
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 break;
		 }
			case "Discharge" : {
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 fieldsToUpdate.put("Fuel", "22");
			 fieldsToUpdate.put("Fuel_upd", true);
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 break;
		 }
			case "StopEvent" : {
			 fieldsToUpdate.put("Ignition", false); 
			 fieldsToUpdate.put("Speed", 0);
			 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
			 DateFormat dfLimit = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
			 Date limitDateTime = dfLimit.parse(limitationValue);
			 
			 Date firstPointTime = new Date(System.currentTimeMillis() - limitDateTime.getTime()); //cur-20mins
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(firstPointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 //still stays but with current time
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 break;

		 }
			case "GeozoneEntrance" : {
			 Geozone geozoneFromPrevStep = (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozonesForNotificationRules");
			 
			 fieldsToUpdate.put("Ignition", true); 
			//set point outside geozone   //      
			 fieldsToUpdate.put("Longitude" ,37.6820000) ;
			 fieldsToUpdate.put("Longitude_upd" , true) ;
			 fieldsToUpdate.put("Latitude" ,  55.7150000) ;
			 fieldsToUpdate.put("Latitude_upd" , true) ;
			 
			 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
			 Date firstPointTime = new Date(System.currentTimeMillis() - 600 * 1000); //cur-20mins
			 
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(firstPointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			//set point intside geozone   //on border:  "longitude":37.5797995522358,   "latitude":55.7300289704315
			 fieldsToUpdate.put("Longitude" , 37.6500000) ;
			 fieldsToUpdate.put("Longitude_upd" , true) ;
			 fieldsToUpdate.put("Latitude" ,  55.7031000) ;
			 fieldsToUpdate.put("Latitude_upd" , true) ;
				
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 
			// geozoneFromPrevStep.checkIfPointInGeozone(geozoneFromPrevStep.geozoneId,"55.7031000","37.6500000");
			 break;
		 }
		 
			case "GeozoneExit" : {
			 Geozone geozoneFromPrevStep = (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozonesForNotificationRules");

			 
			 fieldsToUpdate.put("Ignition", true); 
			//set point inside geozone   //      
			 fieldsToUpdate.put("Longitude" , 37.6500000) ;
			 fieldsToUpdate.put("Longitude_upd" , true) ;
			 fieldsToUpdate.put("Latitude" ,  55.7031000) ;
			 fieldsToUpdate.put("Latitude_upd" , true) ;

			 
			 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
			 Date firstPointTime = new Date(System.currentTimeMillis() - 600 * 1000); //cur-10mins
			 
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(firstPointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			//set point outside geozone   //on border:  "longitude":37.5797995522358,   "latitude":55.7300289704315
			
			 fieldsToUpdate.put("Longitude" ,37.6820000) ;
			 fieldsToUpdate.put("Longitude_upd" , true) ;
			 fieldsToUpdate.put("Latitude" ,  55.7150000) ;
			 fieldsToUpdate.put("Latitude_upd" , true) ;
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 
//			 geozoneFromPrevStep.checkIfPointInGeozone(geozoneFromPrevStep.geozoneId,"55.7031000","37.6500000");
			 break;
		 }
		 
			case "GeozoneStanding" : {
			 fieldsToUpdate.put("Ignition", true); 
			 
				//set point outside geozone   //on border:  "longitude":37.5797995522358,   "latitude":55.7300289704315
				
			 fieldsToUpdate.put("Longitude" ,37.6820000) ;
			 fieldsToUpdate.put("Longitude_upd" , true) ;
			 fieldsToUpdate.put("Latitude" ,  55.7150000) ;
			 fieldsToUpdate.put("Latitude_upd" , true) ;
			 
			 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
//			 Date firstPointTime = new Date(System.currentTimeMillis() - 50*60 * 1000); //cur-30mins
			 DateFormat dfLimit = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
			 Date limitDateTime = dfLimit.parse(limitationValue2);
			 Date firstPointTime = new Date(System.currentTimeMillis() - limitDateTime.getTime());
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(firstPointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());

				//set point inside geozone   //      
			 fieldsToUpdate.put("Longitude" , 37.6500000) ;
			 fieldsToUpdate.put("Longitude_upd" , true) ;
			 fieldsToUpdate.put("Latitude" ,  55.7031000) ;
			 fieldsToUpdate.put("Latitude_upd" , true) ;
			 
			 Date sndPointTime = new Date(System.currentTimeMillis() - 49*60 * 1000); //cur-29mins
			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(sndPointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());

			 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
			 break;
		 }
			case  "IdleEvent": {
				 fieldsToUpdate.put("Ignition", true);
				 fieldsToUpdate.put("Speed", 0);
				 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
				 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
//				 Date firstPointTime = new Date(System.currentTimeMillis() - 6*60 * 1000); //cur-1mins
				 DateFormat dfLimit = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
				 Date limitDateTime = dfLimit.parse(limitationValue);
				 Date firstPointTime = new Date(System.currentTimeMillis() - limitDateTime.getTime());
				  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(firstPointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());

				  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
				  break;
				}
			
			case  "MileageExceedance": {
				 fieldsToUpdate.put("Ignition", true);
				 fieldsToUpdate.put("Speed", 40);
				 fieldsToUpdate.put("Mileage", (Integer.parseInt(rule.getLimitationValue())-1));
				 fieldsToUpdate.put("Mileage_upd", true); 
				 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			//	 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
				 
				 int initialMiladge = 1400;
				 fieldsToUpdate.put("Mileage", initialMiladge);//(140000 - Integer.parseInt(rule.limitationValue)));
				 fieldsToUpdate.put("Mileage_upd", true); 
//				 Long curUnixTime = System.currentTimeMillis();
//				 Date curDateTime = new Date(curUnixTime);
//				 curDateTime.setHours(0);
//				 Date midnightPointTime = new Date(curUnixTime - curDateTime.g.getTime()); //cur-1mins
				 
//				 Calendar c = new GregorianCalendar();
//				    c.set(Calendar.HOUR_OF_DAY, 0); 
//				    c.set(Calendar.MINUTE, 0);
//				    c.set(Calendar.SECOND, 0);
//				    Date midnightPointTime = c.getTime();
//				    
//				  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(midnightPointTime),  telematics_device.trackerId,  telematics_device.trackerCode);

				  df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
				  fieldsToUpdate.put("Mileage", (/*initialMiladge +*/ Integer.parseInt(rule.getLimitationValue())-1 ));
				 Date firstPointTime = new Date(System.currentTimeMillis() - 6*60 * 1000); //cur-1mins
				  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(firstPointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());

//				  fieldsToUpdate.put("Mileage", (Integer.parseInt(rule.limitationValue)));
//					 Date firstPoint1Time = new Date(System.currentTimeMillis() - 3*60 * 1000); //cur-1mins
//					  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(firstPoint1Time),  telematics_device.trackerId,  telematics_device.trackerCode);
				  fieldsToUpdate.put("Mileage", (initialMiladge + Integer.parseInt(rule.getLimitationValue()) ));
				  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());

					  
				  fieldsToUpdate.put("Mileage", (initialMiladge + Integer.parseInt(rule.getLimitationValue())+1 ));
				  insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());
				  break;
				}
			
			
			default:
				throw new IllegalArgumentException("Invalid\\Unsupported rule.roadEventType: " + rule.getRoadEventType());
		 }
			
		 final JSONObject sharedInsertedTrackPoint = insertedTrackPoint;
		 SharedContainer.setContainer("insertedTrackPoints", (new ArrayList(){{add(sharedInsertedTrackPoint);}}));


		}catch (Exception e) {
			log.error(ERROR,e);
		}

	    
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Then("^подготовить тестовый трек для созданного ТМУ для наступления события \"([^\"]*)\", критерий \"([^\"]*)\" значение=\"([^\"]*)\"$")
	public void insertTrackPointsForEventsToFDR(String eventType, String limitationName, String limitationValue){//relates to threshold into DriveStylePenaltyScores NOT TO RULES!
		try{
			ArrayList<JSONObject> insertedTrackPoints = new ArrayList<JSONObject>();
//		NotificationRule rule = (NotificationRule) SharedContainer.getFromContainer("notificationRules").get(0);
		log.debug("Start>> insertion to DeviceFullRecords");

		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		 JSONObject fieldsToUpdate = new JSONObject();
		 JSONObject driveStyleSettings = new JSONObject();
		 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
		 if(((JSONObject)SharedContainer.getLastObjectFromSharedContainer("authorizedAccount_t1client")).get("login").equals("autotest_qa_developer")){
			  driveStyleSettings = (JSONObject) GetDriveStylePenaltyScores.getDriveStyleSettings(getProperty("testCustomerId")).get(0);
		 } else {
			 //this will have sence if demo acc is used, need to get customerId for it
			 driveStyleSettings = (JSONObject) GetDriveStylePenaltyScores.getDriveStyleSettings().get(0);
		 }
		 switch (eventType) {
			case "UserSpeedlimit": {
				int settingsLimitation = 0;
	//			JSONObject driveStyleSettings = (JSONObject) GetDriveStylePenaltyScores.getDriveStyleSettings().get(0);
			 if (Boolean.valueOf(ReplyToJSON.extractPathFromJson(driveStyleSettings, "$.OverSpeeding.IsRuleUse"))){
				 fieldsToUpdate.put("xgeo_Speedlimit", 60);
				 settingsLimitation = 60;
			 } else {
			  settingsLimitation = (int)Double.parseDouble(ReplyToJSON.extractPathFromJson(driveStyleSettings, "$.OverSpeeding.OverSpeedingValue"));
			 }
			 fieldsToUpdate.put("Speed", (settingsLimitation-5));
			 fieldsToUpdate.put("Speed_upd", true);
			 fieldsToUpdate.put("Ignition", true);
			 Date snd1PointTime = new Date(System.currentTimeMillis() - 120 * 1000); //cur-2min
			 insertedTrackPoints.add(InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd1PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));

			 Date snd2PointTime = new Date(System.currentTimeMillis() - 90 * 1000); //cur-1.5min
			 fieldsToUpdate.put("Speed", (settingsLimitation+660));
			 insertedTrackPoints.add(InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd2PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
			  break;
		 }
//			case  "TrafficRulesSpeedLimit": {
//			 int ruleLimitation = Integer.parseInt(limitationValue);
//			 //overspeed
//		//	 JSONObject driveStyleSettings = (JSONObject) GetDriveStylePenaltyScores.getDriveStyleSettings().get(0);
//			 int settingsLimitation = Integer.parseInt(ReplyToJSON.extractPathFromJson(driveStyleSettings, "$.OverSpeeding.OverSpeedingValue"));
//			 
//			 fieldsToUpdate.put("Speed", (settingsLimitation+60));
//
//			// "xgeo_Speedlimit" : 20 + "Overspeed" : 20 < "Speed" : 46
//			 fieldsToUpdate.put("xgeo_Speedlimit", 20);
//			 fieldsToUpdate.put("Speed_upd", true);
//			 fieldsToUpdate.put("Ignition", true);
//			 insertedTrackPoints.add(InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.trackerId,  telematics_device.trackerCode));
//			  break;
//		 }

		  
			case  "SharpTurn": {//резкий поворот
		//			 JSONObject driveStyleSettings = (JSONObject) GetDriveStylePenaltyScores.getDriveStyleSettings().get(0);
				 //SharpTurns value are in mg, devided since DeviceFullRecords.AccelerationX values are in g!!!
				 double settingsLimitation = (Double.parseDouble(ReplyToJSON.extractPathFromJson(driveStyleSettings, "$.SharpTurns.PenaltyScores.Interval2.Start")))/1000;
				
			 fieldsToUpdate.put("Ignition", true); 
			 fieldsToUpdate.put("AccelerationY", (settingsLimitation - 0.01d)); 
			 fieldsToUpdate.put("AccelerationY_upd", true);
			 
			 Date snd1PointTime = new Date(System.currentTimeMillis() - 120 * 1000); //cur-2min
			 insertedTrackPoints.add(InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd1PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
			 fieldsToUpdate.put("AccelerationY", (settingsLimitation+0.39d)); 
			 fieldsToUpdate.put("Speed", (60));//not related to event
			 Date snd2PointTime = new Date(System.currentTimeMillis() - 90 * 1000); //cur-1.5min
			 insertedTrackPoints.add(InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd2PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
		//	 speedLimit
			  break;
		 }
			case "SharpSpeedup": {//резкое ускорение
				
		//		 JSONObject driveStyleSettings = (JSONObject) GetDriveStylePenaltyScores.getDriveStyleSettings().get(0);
				 //SharpAccelerations value are in mg, devided since DeviceFullRecords.AccelerationX values are in g!!!
				 double settingsLimitation = (Double.parseDouble(ReplyToJSON.extractPathFromJson(driveStyleSettings, "$.SharpAccelerations.PenaltyScores.Interval2.Start")))/1000;

//			 double ruleLimitation = Double.parseDouble(limitationValue);
			 fieldsToUpdate.put("Ignition", true); 
			 fieldsToUpdate.put("Speed", (60));//not related to event
			 Date snd1PointTime = new Date(System.currentTimeMillis() - 120 * 1000); //cur-2min
			 fieldsToUpdate.put("AccelerationX",( settingsLimitation -0.002));
			 insertedTrackPoints.add(InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd1PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));

			 fieldsToUpdate.put("AccelerationX",(settingsLimitation+ 0.002)); 
			 fieldsToUpdate.put("AccelerationX_upd", true);
			 Date snd2PointTime = new Date(System.currentTimeMillis() - 90 * 1000); //cur-1.5min
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd2PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
		//	 acceleration
			  break;
		 }
			case "SharpBraking": {//резкое торможение
		//		JSONObject driveStyleSettings = (JSONObject) GetDriveStylePenaltyScores.getDriveStyleSettings().get(0);
				 //SharpTurns value are in mg, devided since DeviceFullRecords.AccelerationX values are in g!!!
				 double settingsLimitation = (Double.parseDouble(ReplyToJSON.extractPathFromJson(driveStyleSettings, "$.SharpBrakings.PenaltyScores.Interval2.Start")))/1000;

			 fieldsToUpdate.put("Ignition", true);
			 fieldsToUpdate.put("AccelerationX_upd", true); 
			 fieldsToUpdate.put("AccelerationX", ( 0.0d)); 
			 Date snd1PointTime = new Date(System.currentTimeMillis() - 120 * 1000); //cur-2min
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd1PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
			 
			 fieldsToUpdate.put("AccelerationX", (settingsLimitation -0.59d)); 
			 Date snd2PointTime = new Date(System.currentTimeMillis() - 90 * 1000); //cur-1.5min
			 insertedTrackPoints.add(InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd2PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
		//	 acceleration
			  break;
		 }
//			case "SharpTurnSpeedLimit" :{//резкое ускорение
//			 int ruleLimitation = Integer.parseInt(limitationValue);
//			 fieldsToUpdate.put("Ignition", true); 
//			 fieldsToUpdate.put("AccelerationY", (0.1d)); 
//			 fieldsToUpdate.put("AccelerationY_upd", true);
//			 fieldsToUpdate.put("Speed", (51));
//			 fieldsToUpdate.put("Speed_upd", true);
//			 
//			 Date sndPointTime = new Date(System.currentTimeMillis() - 30 * 1000); //cur-30sec
//			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(sndPointTime),  telematics_device.trackerId,  telematics_device.trackerCode));
//
//			 fieldsToUpdate.put("AccelerationY", (400.1d)); 
//			 fieldsToUpdate.put("Speed", (ruleLimitation+511));
//			 fieldsToUpdate.put("Speed_upd", true);
//			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.trackerId,  telematics_device.trackerCode));
//			  //	 speedlimit
//			  break;
//		 }
		 
			case "PanicButton": {
			 Date snd1PointTime = new Date(System.currentTimeMillis() - 120 * 1000); //cur-2mins
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd1PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
			 fieldsToUpdate.put("PanicButton_upd", true);
			 fieldsToUpdate.put("PanicButton", true);
			 Date snd2PointTime = new Date(System.currentTimeMillis() - 90 * 1000); //cur-1.5min
			 insertedTrackPoints.add(InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd2PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
			  break;
			}

			case "StopEvent": {
			 fieldsToUpdate.put("Ignition", true); 
			 fieldsToUpdate.put("Speed", 0);
			 Date snd1PointTime = new Date(System.currentTimeMillis() - 3*60 * 1000); //cur-3mins
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd1PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));

			 fieldsToUpdate.put("Ignition", false); 
			 Date snd2PointTime = new Date(System.currentTimeMillis() - 2*60 * 1000); //cur-2mins
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd2PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));

			 //still stays but with current time (there exists asynch between servers, so not current time used :( )
			 Date snd3PointTime = new Date(System.currentTimeMillis() - 60 * 1000); //cur-2mins
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd3PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
			 break;
		 }
			case  "IdleEvent": {
			 fieldsToUpdate.put("Ignition", true);
			 fieldsToUpdate.put("Speed", 10);

			 long currentTimeMs = System.currentTimeMillis();
			 Date snd1PointTime = new Date(currentTimeMs - 16*60 * 1000); //cur-16mins
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd1PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));

			 fieldsToUpdate.put("Speed", 0);
			 Date snd2PointTime = new Date(currentTimeMs - 15*60 * 1000); //cur-15mins
			 insertedTrackPoints.add(InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd2PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));

			 Date snd3PointTime = new Date(currentTimeMs - 13*60 * 1000); //cur-13mins
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd3PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));

			 Date snd4PointTime = new Date(currentTimeMs - 11*60 * 1000); //cur-11mins
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd4PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));

			 Date snd5PointTime = new Date(currentTimeMs - 10*60 * 1000); //cur-10mins
			 insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd5PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));

					  
			  Date snd6PointTime = new Date(currentTimeMs - 2*60 * 1000); //cur-2mins
			  insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd6PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
			  fieldsToUpdate.put("Speed", 10);
			  Date snd7PointTime = new Date(currentTimeMs - 60 * 1000); //cur-1mins
			  insertedTrackPoints.add( InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords",df.format(snd7PointTime),  telematics_device.getTrackerId(),  telematics_device.getTrackerCode()));
			  break;
			}
			default:
				throw new IllegalArgumentException("Invalid\\Unsupported eventType: " + eventType);
		 }
		// insertedTrackPoint.entrySet().size()<1
		 final ArrayList<JSONObject> sharedInsertedTrackPoints = insertedTrackPoints;
		 SharedContainer.setContainer("insertedTrackPoints", sharedInsertedTrackPoints);

		}catch (Exception e) {
			log.error(ERROR,e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Then("^добавление точек в DeviceFullRecords, содержащих поездки$")
	public void insertTrackPointsForEventsToFDR(){
		try{
		JSONObject insertedTrackPoint = new JSONObject();
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		 JSONObject fieldsToUpdate = new JSONObject();
		 insertedTrackPoint = InsertTrackPointToDeviceFullRecords.insertDocument(fieldsToUpdate, "DeviceFullRecords","",  telematics_device.getTrackerId(),  telematics_device.getTrackerCode());

//		 |timezone						|timezoneOffset	|пояс  |
//		 |Dateline Standard Time		|-720			|-12:00|
		 
		 
		}catch (Exception e) {
			log.error(ERROR,e);
		}
	}
	
}
