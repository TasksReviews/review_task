package businessObjects_Departments;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicsTrack;
import com.t1.core.mongodb.GetDepartmentInfoFromMongo;

import cucumber.api.java.en.Then;

public class GET_Department extends AbstractClass{
	public TelematicsTrack device;
	@Then("^получить случайное подразделение из БД$")
	public void getRandomTelematicsDevice(){
		 final JSONObject randomDepartment = (JSONObject) GetDepartmentInfoFromMongo.getRandomDepartmentInfo().get(0);
		SharedContainer.setContainer("json_department", (new ArrayList(){{add(randomDepartment);}}));

	}
}
