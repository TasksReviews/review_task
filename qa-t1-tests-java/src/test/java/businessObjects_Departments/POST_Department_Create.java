package businessObjects_Departments;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Department;
import com.t1.core.api.Employee;

import cucumber.api.java.en.Then;

public class POST_Department_Create extends AbstractClass{
	public Employee employeePrevStep;
	Department department;

	@Then("^создать новое подразделение с минимальным количеством полей, без сотрудников$")
	public void minFields_noEmployees() {
		log.debug("Start>> minFields_noEmployees");
		 department = new Department(false, new ArrayList());
		JSONObject createdDepartment = department.createNewDepartment().get(0);
		SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	
	@Then("^cоздать новый Департамент с минимальным количеством полей и случайными значениями$")
	public void department_create(){
		log.debug("Start>> department_create_min_rand");

		 department = new Department( false,SharedContainer.getObjectsFromSharedContainer("all","employees"));//     employeePrevStep.employeeId);
		JSONObject createdDepartment = department.createNewDepartment().get(0);
		SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
		//vehicle.deleteVehicle(vehicle.vehicleId);
	}

	
	
	@Then("^создать новое подразделение с минимальным количеством полей$")
	public void minFields() {
		log.debug("Start>> department_create_minFields");
		 department = new Department(false, new ArrayList(){{add(SharedContainer.getLastObjectFromSharedContainer("employees"));}});
		JSONObject createdDepartment = department.createNewDepartment().get(0);
		SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	@Then("^создать новое подразделение с полным количеством полей$")
	public void withAllFields() {
//		log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
		log.debug("Start>> department_create_withAllField");

		 department = new Department(true, new ArrayList(){{ add(SharedContainer.getLastObjectFromSharedContainer("employees")); }} );
		JSONObject createdDepartment = department.createNewDepartment().get(0);
		SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	@Then("^создать новое подразделение с полным количеством полей, пустые значения в необязательных полях$")
	public void withAllFields_nonRequiredHasEmptyValues() {
		log.debug("Start>> department_create_withAllFields_NonRequiredhasemptyValues");

		department = new Department(true, SharedContainer.getObjectsFromSharedContainer("all","employees"));
		department.setAddress("");
		department.putUrlParameters("address", "");
		department.setDescription("");
		department.putUrlParameters("description", "");
		department.setPhone("");
		department.putUrlParameters("phone", "");
		department.setEmail("");
		department.putUrlParameters("email", "");
		
		JSONObject createdDepartment = department.createNewDepartment().get(0);
		SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	@Then("^создать новое подразделение с полным количеством полей содержащими кирилицу: \"([^\"]*)\"$")
	public void withAllFields_cyrilicValues(String inputPattern){
		log.debug("Start>> department_create_cyr");
			log.debug("Start>> employee_create");
			log.info("Got pattern:=" + inputPattern);
			department = new Department(true,inputPattern, SharedContainer.getObjectsFromSharedContainer("all","employees"));
			JSONObject createdDepartment = department.createNewDepartment().get(0);
			SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	@Then("^создать новое подразделение с полным количеством полей содержащими цифры: \"([^\"]*)\"$")
	public void withAllFields_numbersInValues(String inputPattern){
		log.debug("Start>> department_create_numbers");
			log.debug("Start>> employee_create");
			log.info("Got pattern:=" + inputPattern);
			department = new Department(true,inputPattern, SharedContainer.getObjectsFromSharedContainer("all","employees"));
			JSONObject createdDepartment = department.createNewDepartment().get(0);
			SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	@Then("^создать новое подразделение с полным количеством полей содержащими спец символы: (.*)$")
	public void withAllFields_SpecialCharachtersInFields(String inputPattern){
		log.debug("Start>> department_create_spec_chars");
			log.debug("Start>> employee_create");
			log.info("Got pattern:=" + inputPattern);
			department = new Department(true,inputPattern, SharedContainer.getObjectsFromSharedContainer("all","employees"));
			JSONObject createdDepartment = department.createNewDepartment().get(0);
			SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	
	
	@Then("^создать новое подразделение с полным количеством полей и 2 сотрудниками$")
	public void withAllFields_add2Employees() {
		log.debug("Start>> department_withAllFields_add2Employees");
		 department = new Department(true, SharedContainer.getObjectsFromSharedContainer(2,"employees"));
		 log.debug("got 2 employess");
		JSONObject createdDepartment = department.createNewDepartment().get(0);
		SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	
}