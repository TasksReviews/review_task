package businessObjects_Departments;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Department;
import com.t1.core.api.Employee;

import cucumber.api.java.en.Then;

public class POST_Department_Delete extends AbstractClass{
	public Employee employeePrevStep;
	Department department;

	
	@Then("^Удалить подразделение созданное в предыдущем шаге$")
	public void deleteDepartment() {
		log.debug("Start>> deleteDepartment");
		 department = new Department( false,SharedContainer.getObjectsFromSharedContainer("all","employees"));
		 Department departmentFromPrevStep = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
		 departmentFromPrevStep.deleteDepartment(departmentFromPrevStep.getDepartmentId());
	}
	
	

	
	
}