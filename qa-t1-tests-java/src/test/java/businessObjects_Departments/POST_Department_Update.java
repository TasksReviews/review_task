package businessObjects_Departments;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Department;
import com.t1.core.api.Employee;

import cucumber.api.java.en.Then;

public class POST_Department_Update extends AbstractClass{
	public Employee employeePrevStep;
	Department department;

	@Then("^изменить подразделение: на полное количество полей$")
	public void minFieldsToAllFields() {
		log.debug("Start>> minFieldsToAllFields");
		 department = new Department(true, SharedContainer.getObjectsFromSharedContainer("all","employees"));
		 Department departmentFromPrevStep = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
		 department.setDepartmentId(departmentFromPrevStep.getDepartmentId());
			JSONObject createdDepartment = department.updateDepartment().get(0);
	}
	
	@Then("^изменить подразделение: на минимальное количество полей$")
	public void allFieldsToMinFields() {
		log.debug("Start>> allFieldsToMinFields");
		 department = new Department( false,SharedContainer.getObjectsFromSharedContainer("all","employees"));
		 Department departmentFromPrevStep = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
		 department.setDepartmentId(departmentFromPrevStep.getDepartmentId());
		JSONObject createdDepartment = department.updateDepartment().get(0);
	}
	
	
	@Then("^изменить подразделение: на полный набор полей содержащий кирилицу: \"([^\"]*)\"$")
	public void withAllFields_cyrilicValues(String inputPattern){
		log.debug("Start>> department_update_withAllFields_cyrilicValues");
			log.debug("Start>> employee_create");
			log.info("Got pattern:=" + inputPattern);
			department = new Department(true,inputPattern, SharedContainer.getObjectsFromSharedContainer("all","employees"));
			Department departmentFromPrevStep = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
			department.setDepartmentId(departmentFromPrevStep.getDepartmentId());
			JSONObject createdDepartment = department.updateDepartment().get(0);
	}
	
	@Then("^изменить подразделение: на полный набор полей содержащий цифры: \"([^\"]*)\"$")
	public void withAllFields_numbersInValues(String inputPattern){
		log.debug("Start>> department_update_withAllFields_NonRequiredhasemptyValues");
			log.debug("Start>> employee_create");
			log.info("Got pattern:=" + inputPattern);
			department = new Department(true,inputPattern, SharedContainer.getObjectsFromSharedContainer("all","employees"));
			 Department departmentFromPrevStep = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
			 department.setDepartmentId(departmentFromPrevStep.getDepartmentId());
			JSONObject createdDepartment = department.updateDepartment().get(0);
	}
	
	
	@Then("^изменить подразделение: на полный набор полей содержащий спец символы: (.*)$")
	public void withAllFields_SpecialCharachtersInFields(String inputPattern){
		log.debug("Start>> department_update_withAllFields_NonRequiredhasemptyValues");
			log.debug("Start>> employee_create");
			log.info("Got pattern:=" + inputPattern);
			department = new Department(true,inputPattern, SharedContainer.getObjectsFromSharedContainer("all","employees"));
			 Department departmentFromPrevStep = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
			 department.setDepartmentId(departmentFromPrevStep.getDepartmentId());
			JSONObject createdDepartment = department.updateDepartment().get(0);
	}

	
	
	@Then("^изменить подразделение: полное количество полей и другой сотрудник$")
	public void withAllFields_update_1_employee() {
		log.debug("Start>> withAllFields_2Employees");
		ArrayList employeesArr = SharedContainer.getObjectsFromSharedContainer(2,"employees");
		//remove here employee 1 used in prev step
		employeesArr.remove(0); 
		 department = new Department(true, employeesArr);
		 //get dep id from prev step
		 Department departmentFromPrevStep = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
		 department.setDepartmentId(departmentFromPrevStep.getDepartmentId());
		 //all params ready so call method to update dep.
			JSONObject createdDepartment = department.updateDepartment().get(0);
		SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	
	@Then("^изменить подразделение с полным количеством полей и 2 сотрудниками$")
	public void withAllFields_update_2_employees() {
		log.debug("Start>> withAllFields_2Employees");
		ArrayList employeesArr = SharedContainer.getObjectsFromSharedContainer(4,"employees");
		//remove here employees 1,2 used in prev step
		employeesArr.remove(0); employeesArr.remove(0);
		 department = new Department(true, employeesArr);
		 //get dep id from prev step
		 Department departmentFromPrevStep = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
		 department.setDepartmentId(departmentFromPrevStep.getDepartmentId());
		 //all params ready so call method to update dep.
			JSONObject createdDepartment = department.updateDepartment().get(0);
		SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	
}