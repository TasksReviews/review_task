package demoTests;


import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.simple.JSONObject;
//import org.testng.annotations.Test;
//import com.t1.core.demo.ReplyToJSON;
//import com.t1.core.demo.SendRequestToAPI;

import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.mongodb.GetAccountInfoFromMongo;

public class DemoTest_old extends com.t1.core.AbstractClass {
	
//	@Test(singleThreaded = true, invocationTimeOut = 10000) // invocationCount =
															// 1,
	public void mydemoTest_old() {
	
		
	try {

	
	String host ="http://map-tone-test.qa.t1-group.ru";
	String url =host +"/register";
			String login = "Frame5TXgjQjEgfPK0";
			String phone = "75345365390";
	String confimationCode ="603427";
	String accountId ="58b59b5b5532f50af4055024";
	String urlParameters = "{\r\n\"login\" : \""+login+"\", \r\n\"firstname\" : \"faYjTgdlEl\", \r\n\"lastname\" : \"IOaTIQTQhl\"," + 
			" \r\n\"email\" : \"3iq3ewwtp1qr2oe3iuo2p1ueqot2tuirupti56@email.com\", \r\n\"phone\" : \""+phone+"\", \r\n\"password\" : \"qazxsw11\" \r\n}";
	
	String postParameters =null;
	String adminLogin ="alex";
	String adminPass ="alex";

	
	JSONObject registration = (JSONObject) SendRequestToAPI.sendPostRequest(url,urlParameters,"",false,true).get(0);
	accountId = registration.get("accountId").toString();

		
	List<org.json.simple.JSONObject> mongoAccountInfo=	GetAccountInfoFromMongo.getAccountInfoByAccountId(accountId,"");//"Contacts");
	String mails= ReplyToJSON.extractPathFromJson(mongoAccountInfo.get(0), "$.Contacts[0].Value");


	
	org.json.simple.JSONObject mongoConfirmation=  GetAccountInfoFromMongo.getConfirmationInfoByAccountId(accountId,"");
	
	confimationCode= ReplyToJSON.extractPathFromJson(mongoConfirmation ,"$.Code");//"$.Contacts[0].Value");

	log.info("send GET confirmation with code");
	//send GET confirmation with code
	url = host +"/register/confirm";
	urlParameters="?accountId="+accountId+"&code="+confimationCode+"&contact="+phone;
	org.json.simple.JSONObject confirmation = (JSONObject) SendRequestToAPI.sendGetRequest(url+urlParameters,"GET","","","").get(0);
	String token = confirmation.get("token").toString();
	
	
	
	log.info("get vehicleRes using token of just created user");
	url = host +"/Vehicle";
	org.json.simple.JSONObject vehicleRes = (JSONObject) SendRequestToAPI.sendGetRequest(url,"GET","","",token).get(0);
	log.info("first vehicle="+vehicleRes.get("name"));
	log.info("vehicleRes=== "+vehicleRes);
	 
	
	//login as admin
	url = host +"/login";
	urlParameters = "{\"login\" : \""+StringEscapeUtils.escapeJava(adminLogin)+"\",\"password\" : \""+StringEscapeUtils.escapeJava(adminPass)+"\"}";
	org.json.simple.JSONObject loginAsadmin = (JSONObject) SendRequestToAPI.sendPostRequest(url,urlParameters,"", false,true).get(0);
	String adminToken = loginAsadmin.get("token").toString();
	
	
	//delete account
	url = host +"/account/delete";
	postParameters ="{ \"accountId\" : \""+accountId+"\" }";
	
	org.json.simple.JSONObject deletedReply = (JSONObject) SendRequestToAPI.sendPostRequest(url ,postParameters,adminToken,false,true).get(0);
	
	log.info("deletedReply=="+deletedReply);
	
	
	log.info("end");
	
	
	
		} catch (Exception e) {
			log.error(ERROR,e);
		}
	
	 }
}
