package businessObjects_AdminPanel_Departments;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SeleniumLogDemo {
    WebDriver driver = new ChromeDriver();
    WebDriverWait webDriverWait = new WebDriverWait(driver, 10);

    public SeleniumLogDemo() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\malinovskiyv\\Downloads\\chromedriver_win32\\chromedriver.exe");
    }

    @Given("^Войти на тестовый сайт \"([^\"]*)\"$")
    public void войтиНаТестовыйСайт(String arg0) throws Throwable {
        driver.get(arg0);
    }

    @Then("^Залогиниться логин = \"([^\"]*)\", пароль = \"([^\"]*)\"$")
    public void залогинитьсяЛогинПароль(String arg0, String arg1) throws Throwable {
        WebElement login = driver.findElement(By.xpath("//*[@id=\"root\"]/div/form/div[1]/input"));
        WebElement password = driver.findElement(By.xpath("//*[@id=\"root\"]/div/form/div[2]/div/input"));
        WebElement enter = driver.findElement(By.xpath("//*[@id=\"root\"]/div/form/a"));
        login.sendKeys(arg0);
        password.sendKeys(arg1);
        enter.click();

    }

    @And("^Перейти в раздел Аналитическая платформа$")
    public void увеличитьКартуРаз() {
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div[3]/div[3]/ul/li[3]/a")));
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/div[3]/ul/li[3]/a")).click();
    }

    @When("^Выйти из тестового сайта$")
    public synchronized void выйтиИзТестовогоСайта() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/div[3]/ul/li/div/ul/li[1]/a")).click();
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div[3]/div[3]/ul/li[16]/a")));
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/div[3]/ul/li[16]/a")).click();
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"root\"]/div/div[5]/div[2]/div[3]/a[1]")));
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[5]/div[2]/div[3]/a[1]")).click();
        wait(3000);
        driver.close();
    }
}
