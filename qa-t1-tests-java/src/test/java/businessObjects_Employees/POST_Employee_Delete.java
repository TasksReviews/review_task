package businessObjects_Employees;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;

import cucumber.api.java.en.Then;

public class POST_Employee_Delete extends AbstractClass{
	public Employee employee; 
	
	@Then("^удалить созданного сотрудника$")
	public void employeeDelete(){
		log.debug("Start>> employee_delete");

		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		//get rules
//		NotificationRule rule = (NotificationRule) SharedContainer.getLastObjectFromSharedContainer("notificationRules");
//		employeeFromPrevStep.notifRuleRelated = rule.ruleId;
//		"notificationRules"
		//get reports

		JSONObject deletedEmployee = (JSONObject) employeeFromPrevStep.deleteEmployee();
		log.debug("end of Employee");
	}
}