package businessObjects_Employees;

import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_Employee_Create extends AbstractClass{
//	public Employee employee;
	@SuppressWarnings({ "unchecked", "rawtypes", "serial" })
	@Then("^создать нового [С,с]отрудника с минимальным количеством полей и случайными значениями$")
	public void minFields(){
		log.debug("Start>> employee_create");
		final Employee employee = new Employee(false);
		JSONObject createdemployee = 	employee.createNewEmployee().get(0);
		SharedContainer.setContainer("employees", employee);
		//employee.deleteEmployee(employee.employeeId);
	}
	
	@Given("^Создать (\\d+) [С,с]отрудника с минимальным набором полей$")
	public void createSeveralEmplWithMinFields(int numberOfEmployees){
		for(int i=0;i<numberOfEmployees;i++){
			minFields();
		}
	}
	
	@Then("^создать нового [С,с]отрудника с полным количеством полей и случайными значениями$")
	public void allFields(){
		log.debug("Start>> employee_create");
		Employee employee = new Employee(true);
//		employee.putUrlParameters("deactivationReason", "");
		//start main: employee creation
		JSONObject createdemployee = employee.createNewEmployee().get(0);
		SharedContainer.setContainer("employees", employee);
	}
	
	@Then("^создать нового [С,с]отрудника с полями \\(Middlename, Description, GeoZoneIds, DeactivationReason\\) содержащими пустые значения$")
	public void withEmptyFields(){
		log.debug("Start>> employee_create");
		Employee employee = new Employee(false);
		employee.putUrlParameters("middleName", "");
		employee.putUrlParameters("description", "");
		employee.putUrlParameters("geoZoneIds", "[]");
		employee.putUrlParameters("deactivationReason", "");

		JSONObject createdemployee = employee.createNewEmployee().get(0);
		SharedContainer.setContainer("employees", employee);
	}
	
	
	@Then("^создать нового [С,с]отрудника с полями содержащими кирилицу: \"([^\"]*)\"$")
	public void CyrilicOnlyInFields(String inputPattern){
			log.debug("Start>> employee_create");
			log.info("Got pattern:=" + inputPattern);
			Employee employee = new Employee(inputPattern);
			JSONObject createdemployee = employee.createNewEmployee().get(0);
			SharedContainer.setContainer("employees", employee);
	}

	@Then("^создать нового [С,с]отрудника с полями содержащими цифры: \"([^\"]*)\"$")
	public void NumbersOnlyInFields(String inputPattern){
			log.debug("Start>> employee_create");
			log.info("Got pattern:=" + inputPattern);
			Employee employee = new Employee(inputPattern);
			JSONObject createdemployee = employee.createNewEmployee().get(0);
			SharedContainer.setContainer("employees", employee);
	}

	@Then("^создать нового [С,с]отрудника с полями содержащими спец символы: (.*)$")
	public void SpecialCharachtersInFields(String inputPattern){
			log.debug("Start>> employee_create");
			log.info("Got pattern:=" + inputPattern);
			Employee employee = new Employee(inputPattern);
			JSONObject createdemployee = employee.createNewEmployee().get(0);
			SharedContainer.setContainer("employees", employee);
	}

	@Then("^создать нового [С,с]отрудника с полями Type = Сourier и Status = Contract$")
	public void type_eq_Сourier_and_Status_eq_Contract() throws Throwable {
		log.debug("Start>> employee_create");
		Employee employee = new Employee(false);
		employee.setEmployeeType("Courier");
		employee.setStatus("Contract");
		employee.putUrlParameters("type", "Courier");
		employee.putUrlParameters("status", "Contract");

		JSONObject createdemployee = employee.createNewEmployee().get(0);
		SharedContainer.setContainer("employees", employee);
	}
	
	@Then("^создать нового [С,с]отрудника с полным количеством полей включая файлы для Avatar и DriverLicense$")
	public void allFields_uploadAvatarAndDriverLicense(){
		log.debug("Start>> employee_create");
		Employee employee = new Employee(true);
		employee.putUrlParameters("avatarUri","custom");
		JSONObject driverLicense = (JSONObject) employee.getUrlParameters().get("driverLicense");
		driverLicense.put("DocumentUri", "new");
		driverLicense.put("EntityStatus", "New");
		employee.putUrlParameters("driverLicense",driverLicense);
		
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", 		  "src\\test\\resources\\Test_Data\\avatar\\avatar_01.jpg");
		filesMap.put("DriverLicense", "src\\test\\resources\\Test_Data\\driverLicense\\dl_01.jpg");
		
		JSONObject createdemployee = (JSONObject) employee.createNewEmployeeAllFieldsWithFiles(filesMap).get(0);
		SharedContainer.setContainer("employees", employee);
	}
	
	
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^зарегистрировать \"([^\"]*)\" [С,с]отрудника с минимальным набором полей и случайныйми значениями полей$")
	public void register_x_employees_with_Random_Data(String numberOfAccountsStr) {
		int numberOfAccounts = Integer.parseInt(numberOfAccountsStr);
		for (int i=1; i<=numberOfAccounts; i++){
			try{
			log.debug("Start>> employee_create");

			Employee employee = new Employee(false);
			JSONObject createdemployee = (JSONObject) employee.createNewEmployee().get(0);
			SharedContainer.setContainer("employees", employee);
			
			} catch (Exception e) {
				log.error(ERROR,e);
			}
		}
	}
	
	@Then("^Создать [С,с]отрудника с [Г,г]руппой$")
	public void minFieldsWithGroup(){
		log.debug("Start>> employee_create");
		 Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
			Employee employee = new Employee(false);
			employee.setGroupId(group.getGroupId());
			employee.setGroupName(group.getName());
			employee.putUrlParameters("groupId", employee.getGroupId());
			JSONObject createdemployee = 	employee.createNewEmployee().get(0);
			//update group object
			group.getObjects().add(employee.getEmployeeId());
			HashMap resource = new HashMap();
			resource.put("resourceType", "Employee");
			resource.put("id", employee.getEmployeeId());
			group.getResources().add(resource);
			group.updateInSharedContainer();
			
			employee.updateInSharedContainer();
	}
	

	@Then("^Создать [С,с]отрудника с [Г,г]руппой и с ТС$")
	public void minFieldsWithGroupAndVeh(){
		log.debug("Start>> employee_create");
		 Group group = (Group) SharedContainer.getObjectsFromSharedContainer("all","groups").get(0);
		 Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			Employee employee = new Employee(false);
			employee.setGroupId(group.getGroupId());
			employee.setGroupName(group.getName());
			employee.setVehicle(vehicle);
			employee.setVehicleId(vehicle.getVehicleId());
			employee.putUrlParameters("vehicleId", vehicle.getVehicleId());
			employee.putUrlParameters("groupId", employee.getGroupId());
			JSONObject createdemployee = 	employee.createNewEmployee().get(0);
			
			employee.addToSharedContainer();
			
			HashMap  resource= new HashMap();
			resource.put("id",employee.getEmployeeId());
			resource.put("resourceType","Employee");
			group.getResources().add(resource);
			group.getObjects().add(employee.getEmployeeId());
			group.updateInSharedContainer();

			vehicle.setEmployeeId(employee.getEmployeeId());
			vehicle.updateInSharedContainer();
	}
	
	@Then("^Создать [С,с]отрудника с ТС$")
	public void minFieldsWithVeh(){
		log.debug("Start>> employee_create");
		 Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			Employee employee = new Employee(false);
			employee.setVehicle(vehicle);
			employee.setVehicleId(vehicle.getVehicleId());
			employee.putUrlParameters("vehicleId", vehicle.getVehicleId());
			employee.putUrlParameters("groupId", employee.getGroupId());
			JSONObject createdemployee = 	employee.createNewEmployee().get(0);
			
			HashMap  resource= new HashMap();
			resource.put("id",employee.getEmployeeId());
			resource.put("resourceType","Employee");

			employee.addToSharedContainer();
			
			vehicle.setEmployeeId(employee.getEmployeeId());
			vehicle.updateInSharedContainer();
	}
	
	@Then("^Создать [С,с]отрудника с ТС (\\d+)$")
	public void minFieldsWithVeh(int vehIndex){
		log.debug("Start>> employee_create");
		 Vehicle vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(vehIndex-1);
			Employee employee = new Employee(false);
			employee.setVehicle(vehicle);
			employee.setVehicleId(vehicle.getVehicleId());
			employee.putUrlParameters("vehicleId", vehicle.getVehicleId());
			employee.putUrlParameters("groupId", employee.getGroupId());
			JSONObject createdemployee = 	employee.createNewEmployee().get(0);
			
			HashMap  resource= new HashMap();
			resource.put("id",employee.getEmployeeId());
			resource.put("resourceType","Employee");

			employee.addToSharedContainer();
			
			vehicle.setEmployeeId(employee.getEmployeeId());
			vehicle.updateInSharedContainer();
	}
	
	@Then("^Создать (\\d+) [С,с]отрудника с ТМУ$")
	public void minFieldsWithDevice(int numOfEmpl){
		log.debug("Start>> employee_create");
		TelematicVirtualTracker telematics_device = null;
		List<TelematicVirtualTracker> devicesList = (List<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers");
		 for(int i=0; i<numOfEmpl; i++){
			Employee employee = new Employee(false);
			for(TelematicVirtualTracker tracker : devicesList){
				if(tracker.getResourceId()== null){
					telematics_device = tracker;
					break;
				}
			 }
			employee.setTelematicsDevice(telematics_device);
			employee.setDeviceId(telematics_device.getTrackerId().toString());
			employee.putUrlParameters("deviceId", employee.getDeviceId());
			JSONObject createdemployee = 	employee.createNewEmployee().get(0);

			employee.addToSharedContainer();
			
			telematics_device.setResourceId(employee.getEmployeeId());
			telematics_device.setResourceType("Employee");
			telematics_device.updateInSharedContainer();
		 }
	}
}