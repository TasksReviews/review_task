package businessObjects_Employees;

import static com.mongodb.client.model.Filters.eq;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_Employee_Update  extends AbstractClass{
//	public Employee employeePrevStep;
	JSONObject editedEmployee = new JSONObject();
	
	
	@Given("^привязать (\\d+) созданное ТМУ к созданному cотруднику$")
	public void addDeviceRelation(int totalNumberUpdEmployees)  {
		ArrayList<TelematicVirtualTracker> telematicsDevicesListFromPrevStep = (ArrayList<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers");
		
		ArrayList<Employee> employeesFromPrevStep = (ArrayList<Employee>) SharedContainer.getObjectsFromSharedContainer(totalNumberUpdEmployees,"employees");

		TelematicVirtualTracker device = null;
		Employee employee = null;
		for(int i=0,used=0;i<totalNumberUpdEmployees && used<totalNumberUpdEmployees;i++){
			String employeeId = employeesFromPrevStep.get(i).getEmployeeId();
			if(employeesFromPrevStep.get(i).getDeviceId()==null && ! SharedContainer.isObjectUsed(employeeId)){
				employee = employeesFromPrevStep.get(i);
				for(  TelematicVirtualTracker tmpdevice : telematicsDevicesListFromPrevStep){
					if(! SharedContainer.isObjectUsed(tmpdevice.getTrackerId())){
						log.debug("we can use empl "+employeesFromPrevStep.get(i).getEmployeeId()+ "  device "+tmpdevice.getTrackerId());
						device = tmpdevice;
						break;
					}
				}
				
				employee.setTelematicsDevice(device);
				employee.setDeviceId(device.getTrackerId().toString());
				employee.putUrlParameters("deviceId", employee.getDeviceId());
				if(employee.getUrlParameters().containsKey("EmployeeStatus")){
					employee.getUrlParameters().remove("EmployeeStatus");
					employee.putUrlParameters("Status",employee.getStatus());
				}
				editedEmployee = (JSONObject) employee.updateEmployee().get(0);
				
				SharedContainer.addToUsedObjects("employees",employee.getEmployeeId());
				SharedContainer.addToUsedObjects("virtualTrackers",device.getTrackerId());
				
			}
		}
		
		log.debug("employees were updated ");

	}
	
	@Then("^Привязать ТМУ к Сотруднику$")
	public void addDeviceRelation()  {
		addDeviceRelation(1);
	}
	
	@Then("^Сменить [С,с]отруднику ТМУ$")
		public void changeDevice()  {
		 log.debug("update employee: change device");
		 Employee employeeFromPrevStep = null;
		 TelematicVirtualTracker device = null;

		 List<Employee> employeesList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
		 ArrayList<TelematicVirtualTracker> telematicsDevicesListFromPrevStep = (ArrayList<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers");
		 for(Employee employee : employeesList){
			 if(employee.getDeviceId()!=null && ! employee.getDeviceId().equals("000000000000000000000000")){
				 employeeFromPrevStep = employee;
				 break;
			 }
		 }
		 for(TelematicVirtualTracker tracker : telematicsDevicesListFromPrevStep){
			if(!tracker.getTrackerId().equals(employeeFromPrevStep.getDeviceId())){
				device = tracker;
				break;
			}
		 }
		 employeeFromPrevStep.setTelematicsDevice(device);
		 employeeFromPrevStep.setDeviceId( device.getTrackerId().toString());
		 employeeFromPrevStep.putUrlParameters("deviceId", employeeFromPrevStep.getDeviceId());
		 editedEmployee = (JSONObject) employeeFromPrevStep.updateEmployee().get(0);
		 employeeFromPrevStep.updateInSharedContainer();
		 
	 }
	
	@Then("^Отвязать ТМУ от Сотрудника$")
	public void removeDeviceRelation()  {
		 log.debug("update employee: remove device");
		Employee employeeFromPrevStep=null;
		List<Employee> employeeList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");

		for (Employee employee : employeeList ) {
			if (employee.getDeviceId() != null) {
				employeeFromPrevStep = employee;
			}
		}
		employeeFromPrevStep.setDeviceId(null);
		employeeFromPrevStep.getUrlParameters().remove("deviceId");
			if(employeeFromPrevStep.getGroupId()!=null && ! employeeFromPrevStep.getGroupId().equals("000000000000000000000000")){
				employeeFromPrevStep.putUrlParameters("groupId", employeeFromPrevStep.getGroupId());
			}
			editedEmployee = (JSONObject) employeeFromPrevStep.updateEmployee().get(0);

		log.debug("employees were updated ");
	}
	
	 @Given("^Отвязать группу от Сотрудника$")
		public void removeGroupRelation()  {
		 log.debug("update employee: remove group");
		 Employee employeeFromPrevStep= null;
		 List<Employee> employeesList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
		 for(Employee employee: employeesList){
			 if(employee.getGroupId()!=null)
			 employeeFromPrevStep = employee;
		 }
		 String emplOldGroupId= employeeFromPrevStep.getGroupId();
		 employeeFromPrevStep.setGroupId(null);
		 employeeFromPrevStep.putUrlParameters("groupId","000000000000000000000000");
		 editedEmployee = (JSONObject) employeeFromPrevStep.updateEmployee().get(0);
		 employeeFromPrevStep.removeEmplFromGroupObj(emplOldGroupId, employeeFromPrevStep.getEmployeeId());
	 }
	 @Given("^Отвязать сотрудника от группы$")
		public void removeGroupRelation2()  {
		 removeGroupRelation();
	 }
	 
	 @Given("^Привязать [С,с]отрудника к группе$")
		public void addGroupRelation()  {
		 log.debug("update employee: add group");
		 Employee employeeFromPrevStep = null;
		 List<Employee> employeesList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
		 Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
		 for(Employee employee : employeesList){
			 if(employee.getGroupId()==null || employee.getGroupId().equals("000000000000000000000000")){
				 employeeFromPrevStep = employee;
			 }
		 }
		 employeeFromPrevStep.setGroupId(group.getGroupId());
		 employeeFromPrevStep.setGroupName(group.getName());
		 employeeFromPrevStep.putUrlParameters("groupId",group.getGroupId());
		 editedEmployee = (JSONObject) employeeFromPrevStep.updateEmployee().get(0);
		 
		 group.addObjectToGroup(employeeFromPrevStep.getEmployeeId(),"Employee");
		 group.updateInSharedContainer();
		 employeeFromPrevStep.updateInSharedContainer();
	 }
	 
	 
	 @Given("^Сменить для cотрудника группу$")
		public void changeGroup()  {
		 log.debug("update employee: change group");
		 Employee employeeFromPrevStep = null;
		 Group groupFromPrevStep = null;
		// String emplGroup =null;
		 List<Employee> employeesList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
		 List<Group> groupList = (List<Group>) SharedContainer.getObjectsFromSharedContainer("all","groups");
		 for(Employee employee : employeesList){
			// emplGroup=employee.groupId;
			 if(employee.getGroupId()!=null && ! employee.getGroupId().equals("000000000000000000000000")){
				 employeeFromPrevStep = employee;
				 break;
			 }
		 }
		 for(Group group : groupList){
			if(!group.getGroupId().equals(employeeFromPrevStep.getGroupId())){
				groupFromPrevStep = group;
				break;
			}
		 }
		 
		 String emplGroupIdBeforeReq = employeeFromPrevStep.getGroupId();
		 employeeFromPrevStep.setGroupId(groupFromPrevStep.getGroupId());
		 employeeFromPrevStep.setGroupName(groupFromPrevStep.getName());
		 employeeFromPrevStep.putUrlParameters("groupId",groupFromPrevStep.getGroupId());
		 editedEmployee = (JSONObject) employeeFromPrevStep.updateEmployee().get(0);
		 employeeFromPrevStep.updateInSharedContainer();
		 
		 groupFromPrevStep.addObjectToGroup(employeeFromPrevStep.getEmployeeId(),"Employee");
		 groupFromPrevStep.updateInSharedContainer();
		 
			// remove the object from old group			
			Group.getFromSharedContainer(emplGroupIdBeforeReq).removeObjectInGroup(employeeFromPrevStep.getEmployeeId());
	 }
	 
		@Given("^Привязать ТС к Сотруднику$")
		public void addVehicleRelation()  {
			 log.debug("update employee: add veh");
			Vehicle vehicleFromPrevStep=null;
			Employee employeesFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
			List<Vehicle> listOfVehicles = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all","vehicles");
			for (Vehicle vehicle : listOfVehicles) {
				if (vehicle.getEmployeeId() == null) {
					vehicleFromPrevStep = vehicle;
				}
			}
				employeesFromPrevStep.setVehicle(vehicleFromPrevStep);
				employeesFromPrevStep.setVehicleId(vehicleFromPrevStep.getVehicleId());
				employeesFromPrevStep.putUrlParameters("vehicleId", vehicleFromPrevStep.getVehicleId());
				if(employeesFromPrevStep.getGroupId()!=null && ! employeesFromPrevStep.getGroupId().equals("000000000000000000000000")){
					employeesFromPrevStep.putUrlParameters("groupId", employeesFromPrevStep.getGroupId());
				}
				editedEmployee = (JSONObject) employeesFromPrevStep.updateEmployee().get(0);
					
				SharedContainer.addToUsedObjects("employees",employeesFromPrevStep.getEmployeeId());
				SharedContainer.addToUsedObjects("vehicles",vehicleFromPrevStep.getVehicleId());

			log.debug("employees were updated ");

		}
		
		@Given("^Привязать ТС (\\d+) к Сотруднику$")
		public void addVehicleRelation(int vehIndex)  {
			 log.debug("update employee: add veh");
			Employee employeesFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(vehIndex-1);

				employeesFromPrevStep.setVehicle(vehicleFromPrevStep);
				employeesFromPrevStep.setVehicleId(vehicleFromPrevStep.getVehicleId());
				employeesFromPrevStep.putUrlParameters("vehicleId", vehicleFromPrevStep.getVehicleId());
				if(employeesFromPrevStep.getGroupId()!=null && ! employeesFromPrevStep.getGroupId().equals("000000000000000000000000")){
					employeesFromPrevStep.putUrlParameters("groupId", employeesFromPrevStep.getGroupId());
				}
				editedEmployee = (JSONObject) employeesFromPrevStep.updateEmployee().get(0);
					
				SharedContainer.addToUsedObjects("employees",employeesFromPrevStep.getEmployeeId());
				SharedContainer.addToUsedObjects("vehicles",vehicleFromPrevStep.getVehicleId());

			log.debug("employees were updated ");

		}
		
		@Given("^Отвязать ТС от Сотрудника$")
		public void removeVehicleRelation()  {
			 log.debug("update employee: remove veh");
			Employee employeeFromPrevStep=null;
			List<Employee> employeeList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");

			for (Employee employee : employeeList ) {
				if (employee.getVehicleId() != null) {
					employeeFromPrevStep = employee;
				}
			}
			employeeFromPrevStep.setVehicle(null);
			employeeFromPrevStep.setVehicleId(null);
			employeeFromPrevStep.getUrlParameters().remove("vehicleId");
				if(employeeFromPrevStep.getGroupId()!=null && ! employeeFromPrevStep.getGroupId().equals("000000000000000000000000")){
					employeeFromPrevStep.putUrlParameters("groupId", employeeFromPrevStep.getGroupId());
				}
				editedEmployee = (JSONObject) employeeFromPrevStep.updateEmployee().get(0);
					

			log.debug("employees were updated ");

		}
		
	 @Then("Отредактировать текстовые поля Сотрудника")
	 public void updateTextFields(){
		 log.debug("empl update text fields");
		 Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		 JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("employee",getProperty("stdPattern"));
		 employeeFromPrevStep.setFirstname(initMap.get("firstname").toString());
		 employeeFromPrevStep.setLastname(initMap.get("lastname").toString());
		 employeeFromPrevStep.setMiddleName(initMap.get("middleName").toString());
		 employeeFromPrevStep.setPhone(initMap.get("phone").toString());
		 employeeFromPrevStep.setEmail(initMap.get("email").toString());
		 employeeFromPrevStep.setDescription(initMap.get("description").toString());

		 employeeFromPrevStep.putUrlParameters("firstname", employeeFromPrevStep.getFirstname());
		 employeeFromPrevStep.putUrlParameters("lastname", employeeFromPrevStep.getLastname());
		 employeeFromPrevStep.putUrlParameters("middleName", employeeFromPrevStep.getMiddleName());
		 employeeFromPrevStep.putUrlParameters("phone",  employeeFromPrevStep.getPhone());
		 employeeFromPrevStep.putUrlParameters("email", employeeFromPrevStep.getEmail());
		 employeeFromPrevStep.putUrlParameters("description", employeeFromPrevStep.getDescription());
		 if(employeeFromPrevStep.getGroupId()!=null)
			 employeeFromPrevStep.putUrlParameters("groupId", employeeFromPrevStep.getGroupId());

		 editedEmployee = (JSONObject) employeeFromPrevStep.updateEmployee().get(0);
		 log.debug("empl update text fields");
	 }
	 
	 
	 @Given("^Деактивировать Сотрудника$")
	 public void deactivateEmpl() {
		 log.debug("start deactivateEmpl");
		 Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		 employeeFromPrevStep.setIsDeactivated(true);
		 if(employeeFromPrevStep.getGroupId() != null)
			 employeeFromPrevStep.putUrlParameters("groupId", employeeFromPrevStep.getGroupId());
		 employeeFromPrevStep.putUrlParameters("id", employeeFromPrevStep.getVehicleId());
		 employeeFromPrevStep.putUrlParameters("isDeactivated", "true");
		 editedEmployee = (JSONObject) employeeFromPrevStep.updateEmployee().get(0);
		 employeeFromPrevStep.updateInSharedContainer();
		 log.debug("end deactivateEmpl");
	 }

	 @Then("^Активировать Сотрудника$")
	 public void activateEmpl() {
		 log.debug("start activateVeh");
		 Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		 employeeFromPrevStep.setIsDeactivated(false);
		employeeFromPrevStep.setDeviceId(null); //after deactivation relation to device has to be destroyed
		employeeFromPrevStep.setTelematicsDevice(null);
		 if(employeeFromPrevStep.getGroupId() != null)
			 employeeFromPrevStep.putUrlParameters("groupId", employeeFromPrevStep.getGroupId());
		 employeeFromPrevStep.putUrlParameters("id", employeeFromPrevStep.getVehicleId());
		 employeeFromPrevStep.putUrlParameters("isDeactivated", "false");
		 if(employeeFromPrevStep.getUrlParameters().containsKey("deviceId"))
			 employeeFromPrevStep.getUrlParameters().remove("deviceId");
		 editedEmployee = (JSONObject) employeeFromPrevStep.updateEmployee().get(0);
		 employeeFromPrevStep.updateInSharedContainer();
		 log.debug("end activateVeh");
	 }
	 
	 
		@Then("^Сдвинуть дату создания Сотрудника на (\\d+) дней назад, считая от текущей даты$")
		public void updateEmployeeCreationDate(int numOfDays) {
			Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
			Bson filter = Filters.and(eq("_id", new ObjectId(employeeFromPrevStep.getEmployeeId())));
			long currTimeInMs = System.currentTimeMillis();
			long inMs24hrs = 24 * 60 * 60 * 1000L;
			long newCreationDate = currTimeInMs - (numOfDays * inMs24hrs);
			HashMap<String, Object> newCreationDateObj = new HashMap<String, Object>();
			newCreationDateObj.put("Created", new Date(newCreationDate));
			com.t1.core.mongodb.MongoConnection.updateCollection("Employees", filter, newCreationDateObj);
	 	}
}
