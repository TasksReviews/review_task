package businessObjects_Vehicles;

import static com.mongodb.client.model.Filters.eq;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_Vehicle_Add extends AbstractClass{
	public Vehicle vehicle; 

	@Then("^cоздать новое ТС с минимальным количеством полей и случайными значениями$")
	public void minFields(){
		log.debug("Start>> vehicle_create minFields");
		vehicle = new Vehicle(true);
		 vehicle.createNewVehicle().get(0);
		 vehicle.addToSharedContainer();
		log.debug("end of vehicle");
	}
	
	@Then("Создать (\\d+) ТС с минимальным набором полей$")
	public void createSeveralVehWithMinFields(int numberOfVehicles){
		for(int i=0;i<numberOfVehicles;i++){
			minFields();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Then("^cоздать новое ТС с полным количеством полей и случайными значениями$")
	public void allFields(){
		
		log.debug("Start>> vehicle_create allFields");
		
	//	 Object listOfEmpl = SharedContainer.getLastObjectFromSharedContainer("employees").get(0);
		 
		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
			JSONObject telematics_geozone = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("json_telematics_geozone");
			JSONObject group = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("json_group");
			JSONObject department = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("json_department");
			vehicle = new Vehicle(true,SharedContainer.getObjectsFromSharedContainer(1,"employees"),
								group, telematics_device, telematics_geozone, department, "stdPattern");
		
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", 		  "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");
		filesMap.put("InsurancePolicy_1", "src\\test\\resources\\Test_Data\\insurancePolicy\\ip_04.jpg");

		JSONObject createdvehicle = (JSONObject) vehicle.createNewVehicleWithFiles(filesMap).get(0);

		log.debug("add 1 maintenance,repair,accident");
		JSONObject initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));

		vehicle.getMaintenancesReq().addAll((List)((JSONObject) initJson.get("technicalState")).get("maintenances"));
		vehicle.getRepairsReq().addAll((List)((JSONObject) initJson.get("technicalState")).get("repairs"));
		vehicle.getAccidentsReq().addAll((List)((JSONObject) initJson.get("technicalState")).get("accidents"));
		vehicle.putUrlParameters("technicalState", initJson.get("technicalState"));
		
		JSONObject editedVehicle = (JSONObject) vehicle.updateVehicle(filesMap).get(0);

//need to check and compare this data 
//		List accidents = GetVehicleAccidentsFromMongo.getInfoByVehicleId(vehicle.vehicleId);
//		List emplHist = GetVehicleEmployeeHistoryFromMongo.getInfoByVehicleId(vehicle.vehicleId);
//		List Maintenan = GetVehicleMaintenancesInfoFromMongo.getInfoByVehicleId(vehicle.vehicleId);
//		List repairs = GetVehicleRepairsInfoFromMongo.getInfoByVehicleId(vehicle.vehicleId);
//		List insurance = GetVehicle_EmployeeInsurancePoliciesInfoFromMongo.getInfoByVehicleId(vehicle.vehicleId);
		
		
		vehicle.addToSharedContainer();
		log.debug("end of vehicle");
	}
	
	
	@Then("^Создать ТС с группой$")
	public void minFieldsWithGroup() {

		log.debug("Start>> vehicle_create minFieldsWithGroup");
		Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");

		vehicle = new Vehicle(true);
		vehicle.setGroup(group.getInitJson());
		vehicle.setGroupId(group.getGroupId());
		vehicle.putUrlParameters("groupId", group.getGroupId());

		JSONObject createdvehicle = (JSONObject) vehicle.createNewVehicle().get(0);
		vehicle.addToSharedContainer();
		
		group.addObjectToGroup(vehicle.getVehicleId(),"Vehicle");
		group.updateInSharedContainer();
		
		log.debug("end of vehicle");
	}
	
	@Then("^Создать ТС с группой и с Сотрудником$")
	public void minFieldsWithGroupAndEmpl() {

		log.debug("Start>> vehicle_create minFieldsWithGroupAndEmpl");
		Group group = (Group) SharedContainer.getObjectsFromSharedContainer("all","groups").get(0);
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");

		vehicle = new Vehicle(true);
		vehicle.setGroup(group.getInitJson());
		vehicle.setGroupId(group.getGroupId());
		vehicle.setEmployeeId(employee.getEmployeeId());
		
		vehicle.putUrlParameters("groupId", vehicle.getGroupId());
		vehicle.putUrlParameters("employeeId", employee.getEmployeeId());
		
		JSONObject createdvehicle = (JSONObject) vehicle.createNewVehicle().get(0);
		vehicle.addToSharedContainer();
		

		group.addObjectToGroup(vehicle.getVehicleId(),"Vehicle");
		group.updateInSharedContainer();
		log.debug("end of vehicle");
	}
	
	@Then("^Создать ТС с Сотрудником$")
	public void minFieldsWithEmpl() {

		log.debug("Start>> vehicle_create minFieldsWithGroupAndEmpl");
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");

		vehicle = new Vehicle(true);
		vehicle.setEmployeeId(employee.getEmployeeId());
		
		vehicle.putUrlParameters("groupId", vehicle.getGroupId());
		vehicle.putUrlParameters("employeeId", employee.getEmployeeId());
		
		JSONObject createdvehicle = (JSONObject) vehicle.createNewVehicle().get(0);
		vehicle.addToSharedContainer();
		employee.setVehicleId(vehicle.getVehicleId());
		vehicle.updateInSharedContainer();
		log.debug("end of vehicle");
	}
	
	@Then("^Создать ТС с Сотрудником (\\d+)$")
	public void minFieldsWithEmpl(int emplIndex) {

		log.debug("Start>> vehicle_create minFieldsWithGroupAndEmpl");
		Employee employee = (Employee) SharedContainer.getObjectsFromSharedContainer("all","employees").get(emplIndex-1);

		vehicle = new Vehicle(true);
		vehicle.setEmployeeId(employee.getEmployeeId());
		
		vehicle.putUrlParameters("groupId", vehicle.getGroupId());
		vehicle.putUrlParameters("employeeId", employee.getEmployeeId());
		
		JSONObject createdvehicle = (JSONObject) vehicle.createNewVehicle().get(0);
		vehicle.addToSharedContainer();
		employee.setVehicleId(vehicle.getVehicleId());
		vehicle.updateInSharedContainer();
		log.debug("end of vehicle");
	}
	
	@Then("^создать новое ТС с минимальным количеством полей содержащими кирилицу: \"([^\"]*)\"$")
	public void minFields_cyrilicValues(String inputPattern) {

			log.debug("Start>> vehicle_create minFields_cyrilicValues" );
			log.info("Got pattern:=" + inputPattern);
			vehicle = new Vehicle(false,inputPattern);
			JSONObject createdVehicle = (JSONObject) vehicle.createNewVehicle().get(0);
			vehicle.addToSharedContainer();
	}
	
	@Then("^создать новое ТС с минимальным количеством полей содержащими цифры: \"([^\"]*)\"$")
	public void minFields_numbersInValues(String inputPattern){
			log.debug("Start>> vehicle_create minFields_numbersInValues");
			log.info("Got pattern:=" + inputPattern);
			vehicle = new Vehicle(false,inputPattern);
			JSONObject createdVehicle =(JSONObject) vehicle.createNewVehicle().get(0);
			vehicle.addToSharedContainer();
	}
	

	@Then("^создать новое ТС с минимальным количеством полей содержащими спец символы: (.*)$")
	public void minFields_SpecialCharachtersInFields(String inputPattern){
			log.debug("Start>> vehicle_create minFields_SpecialCharachtersInFields");
			log.info("Got pattern:=" + inputPattern);
			vehicle = new Vehicle(false,inputPattern);
			JSONObject createdVehicle =(JSONObject) vehicle.createNewVehicle().get(0);
			vehicle.addToSharedContainer();
	}
	
	@Then("^cоздать новое ТС с минимальным количеством полей и типом Car$")
	public void minFields_typeEqCar(){
		log.debug("Start>> vehicle_create_type=Car");
		vehicle = new Vehicle(true);
		vehicle.setType("Car");
		vehicle.putUrlParameters("type", "Car");
		JSONObject createdvehicle =(JSONObject) vehicle.createNewVehicle().get(0);
		vehicle.addToSharedContainer();
		log.debug("end of vehicle");
	}
	

	@Then("^cоздать новое ТС с минимальным количеством полей с заданным типом: \"([^\"]*)\"$")
	public void minFields_vehicleTypes(String inputType){
			log.debug("Start>> vehicle_create minFields_type" );
			log.info("Got pattern:=" + inputType);
			vehicle = new Vehicle(true);
			vehicle.setType(inputType);
			vehicle.putUrlParameters("type", inputType);
			JSONObject createdVehicle = (JSONObject) vehicle.createNewVehicle().get(0);
			vehicle.addToSharedContainer();
	}
	
	@Then("^создать новое ТС с минимальным количеством полей и привязать к созданному ТМУ$")
	public void minFields_withDevice() {
		
			log.debug("Start>> vehicle_create minFields_with device" );
			TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
			vehicle = new Vehicle(true);
			vehicle.setTelematicsDevice(telematics_device);
			vehicle.setDeviceId(telematics_device.getTrackerId().toString());
			vehicle.putUrlParameters("deviceId", vehicle.getDeviceId());
			JSONObject createdVehicle = (JSONObject) vehicle.createNewVehicle().get(0);
			vehicle.addToSharedContainer();
		
	}
	
	@Then("^Создать (\\d+) ТС с ТМУ$")
	public void create_X_Veh_withDevice(int numberOfVehicles) {
		for(int i=0; i<numberOfVehicles; i++){
			log.debug("Start>> vehicle_create minFields_with device" );
			TelematicVirtualTracker telematics_device = null;
			List<TelematicVirtualTracker> listOfDevices = (List<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers");
			for(TelematicVirtualTracker device : listOfDevices){
				if(device.getResourceId() == null){
					telematics_device = device;
					break;
				}
			}
			vehicle = new Vehicle(true);
			vehicle.setTelematicsDevice(telematics_device);
			vehicle.setDeviceId(telematics_device.getTrackerId().toString());
			vehicle.putUrlParameters("deviceId", vehicle.getDeviceId());
			JSONObject createdVehicle = (JSONObject) vehicle.createNewVehicle().get(0);
			vehicle.addToSharedContainer();
			
			telematics_device.setResourceId(vehicle.getVehicleId());
			telematics_device.setResourceType("Vehicle");
			telematics_device.setResourceName(vehicle.getName());
			if(createdVehicle.containsKey("Created"))
				telematics_device.setBoundTime((Long) ((JSONObject) createdVehicle.get("Created")).get("$date"));
			telematics_device.updateInSharedContainer();
		}
	}
	
//	steps that dont call web API methods
	
	@Given("^Отредактировать время создания ТС в коллекции \"([^\"]*)\" на (.*)$")
		public void editCreationTimeOfCreatedVehicle(String collection, String newCreationTime) {
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		Bson filter = Filters.and(eq("_id",new ObjectId(vehicleFromPrevStep.getVehicleId()) ) ); 
		long yesterdayMidnight=0;
		if(newCreationTime.equals("вчера 00:00:00")){
			long currTimeInMs = System.currentTimeMillis();
			long inMs24hrs = 24* 60*60 * 1000L;
			yesterdayMidnight = currTimeInMs -(currTimeInMs % inMs24hrs)-inMs24hrs;
		}
		HashMap<String,Object> newCreationDate = new HashMap<String,Object>();
		newCreationDate.put( "Created",new Date(yesterdayMidnight));
		com.t1.core.mongodb.MongoConnection.updateCollection("Vehicles",filter,newCreationDate);
	}
	

	
}