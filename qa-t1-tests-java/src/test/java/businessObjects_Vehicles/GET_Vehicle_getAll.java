package businessObjects_Vehicles;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class GET_Vehicle_getAll extends AbstractClass{
	public Vehicle vehicle; 
	
	@Then("^получить список ТС$")
	public void allFields(){
		
		log.debug("Start>> vehicle_get_all ");
		String token = getAnyToken();
		
		List<JSONObject> receivedVehicle = (List<JSONObject>) Vehicle.getAllVehicles1AndLastPages(token);
		log.debug("end of get all vehicles");
	}
}