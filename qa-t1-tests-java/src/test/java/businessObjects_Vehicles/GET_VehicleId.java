package businessObjects_Vehicles;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class GET_VehicleId extends AbstractClass{
	public Vehicle vehicle; 
	
	@Then("^получить ТС по id$")
	public void allFields(){
		
		log.debug("Start>> vehicle_get_id ");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		JSONObject receivedVehicle = (JSONObject) vehicleFromPrevStep.getVehiclebyId(vehicleFromPrevStep.getVehicleId()).get(0);
		log.debug("end of get vehicle by id");
	}
}