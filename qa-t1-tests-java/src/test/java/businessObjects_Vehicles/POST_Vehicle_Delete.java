package businessObjects_Vehicles;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class POST_Vehicle_Delete extends AbstractClass{
	public Vehicle vehicle; 
	
	@Then("^удалить созданное ТС$")
	public void vehicleDelete() {
		log.debug("Start>> vehicle_delete");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		JSONObject deletedvehicle = (JSONObject) vehicleFromPrevStep.deleteVehicle();
		log.debug("end of vehicle");

	}

}