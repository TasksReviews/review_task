package businessObjects_Vehicles;

import static com.mongodb.client.model.Filters.eq;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_Vehicle_Update extends AbstractClass{
	public Employee employeePrevStep;
	Vehicle vehicle;
	JSONObject editedVehicle = new JSONObject();

	@Then("^изменить ТС: на полное количество полей$")
	public void minFieldsToAllFields() {
		log.debug("Start>> minFieldsToAllFields");
		
		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
			JSONObject telematics_geozone = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("json_telematics_geozone");
			JSONObject group = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("json_group");
			JSONObject department = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("json_department");
			vehicle = new Vehicle(true,SharedContainer.getObjectsFromSharedContainer(1,"employees"),
								group, telematics_device, telematics_geozone, department, "stdPattern");
		
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", 		  "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");
		filesMap.put("InsurancePolicy_1", "src\\test\\resources\\Test_Data\\insurancePolicy\\ip_04.jpg");
//		
		
	//	 vehicle = new Vehicle(true, SharedContainer.getLastObjectFromSharedContainer("employees"));
		 Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		 vehicle.setVehicleId(vehicleFromPrevStep.getVehicleId());
		 editedVehicle = (JSONObject) vehicle.updateVehicle(filesMap).get(0);
	}
	
	@Then("^изменить ТС: на минимальное количество полей$")
	public void allFieldsToMinFields() {
		log.debug("Start>> allFieldsToMinFields");
		 vehicle = new Vehicle(true);
		 Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		 vehicle.setVehicleId(vehicleFromPrevStep.getVehicleId());
		 editedVehicle = (JSONObject) vehicle.updateVehicle().get(0);
	}
	
	@Then("^изменить ТС: полное количество полей, новые значения$")
	public void allFieldsEditValues() {

		 
		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer(2,"virtualTrackers").get(1);
			JSONObject telematics_geozone = (JSONObject) SharedContainer.getObjectsFromSharedContainer(2,"json_telematics_geozone").get(1);
			JSONObject group = (JSONObject) SharedContainer.getObjectsFromSharedContainer(2,"json_group").get(1);
			JSONObject department = (JSONObject) SharedContainer.getObjectsFromSharedContainer(2,"json_department").get(1);
			ArrayList employeeLst =  SharedContainer.getObjectsFromSharedContainer(2, "employees");
			employeeLst.remove(0);
			vehicle = new Vehicle(true, employeeLst, group, telematics_device, telematics_geozone, department, "stdPattern");
			 Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			 vehicle.setVehicleId(vehicleFromPrevStep.getVehicleId());
			 String insuranceIdFromPrevStep =  vehicleFromPrevStep.getInsuranceIdsList().get(0).toString();
			//update insurance field
			//log.debug(">>>>INS>>"+ vehicle.urlParameters.get("insurancePolicy").toString());
			ArrayList insuranceList = (ArrayList) vehicle.getUrlParameters().get("insurancePolicy");
			final JSONObject isuranceJson = (JSONObject)insuranceList.get(0);
			isuranceJson.put("entityStatus", "modify");
			isuranceJson.put("id", insuranceIdFromPrevStep);
			
			//put it back to urlParams
			vehicle.putUrlParameters("insurancePolicy",  new ArrayList(){{add(isuranceJson);}});
			
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", 		  "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");
		filesMap.put("InsurancePolicy_1", "src\\test\\resources\\Test_Data\\insurancePolicy\\ip_04.jpg");
//		
		
	//	 vehicle = new Vehicle(true, SharedContainer.getLastObjectFromSharedContainer("employees"));

		editedVehicle = (JSONObject) vehicle.updateVehicle(filesMap).get(0);
	}

	@Then("^изменить ТС: полное количество полей, новые значения для топлива$")
	public void allFieldsEditFuel() {
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");

		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");
		filesMap.put("InsurancePolicy_1", "src\\test\\resources\\Test_Data\\insurancePolicy\\ip_04.jpg");

		log.debug("send update without fuel");
		vehicleFromPrevStep.getUrlParameters().remove("fuel");
		vehicleFromPrevStep.setFuel(null);
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

		log.debug("send update fuel has no properties");
		vehicleFromPrevStep.putUrlParameters("fuel", "{}");
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

		log.debug("send update fuel with new values");
		JSONObject initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		vehicleFromPrevStep.setFuel((JSONObject) initJson.get("fuel"));
		vehicleFromPrevStep.putUrlParameters("fuel", vehicleFromPrevStep.getFuel());
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

	}
	
	@Then("^изменить ТС: полное количество полей, новые значения для страхового полиса$")
	public void allFieldsEditInsurancePolicy() throws Exception {
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");
		filesMap.put("InsurancePolicy_1", "src\\test\\resources\\Test_Data\\insurancePolicy\\ip_04.jpg");

		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");

		log.debug("edit 1 insurance");
		JSONObject initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		String insuranceIdFromPrevStep = vehicleFromPrevStep.getInsuranceIdsList().get(0).toString();

		ArrayList isuranceListInit = (ArrayList) initJson.get("insurancePolicy");
		final JSONObject isuranceJson = (JSONObject) isuranceListInit.get(0);
		vehicleFromPrevStep.setInsurancePolicyReq((ArrayList) initJson.get("insurancePolicy"));
		isuranceJson.put("entityStatus", "modify");
		isuranceJson.put("id", insuranceIdFromPrevStep);
		// put it back to urlParams
		vehicleFromPrevStep.putUrlParameters("insurancePolicy", new ArrayList() {{ add(isuranceJson); }});

		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

		log.debug("add 2 insurance");
		filesMap.put("InsurancePolicy_2", "src\\test\\resources\\Test_Data\\insurancePolicy\\ip_03.jpg");

		initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		isuranceListInit = (ArrayList) initJson.get("insurancePolicy");
		final JSONObject isuranceJson2nd = (JSONObject) isuranceListInit.get(0);
		isuranceJson2nd.put("DocumentIndex", 2);
		vehicleFromPrevStep.getInsurancePolicyReq().add(isuranceJson2nd);

		vehicleFromPrevStep.putUrlParameters("insurancePolicy", new ArrayList() {{add(isuranceJson2nd);}});
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

		log.debug("remove both");
		filesMap.remove("InsurancePolicy_1");
		filesMap.remove("InsurancePolicy_2");
		final JSONObject isuranceJson1 = new JSONObject();
		final JSONObject isuranceJson2 = new JSONObject();
		isuranceJson1.put("id", vehicleFromPrevStep.getInsuranceIdsList().get(0));
		isuranceJson1.put("EntityStatus", "Delete");
		isuranceJson2.put("id", vehicleFromPrevStep.getInsuranceIdsList().get(1));
		isuranceJson2.put("EntityStatus", "Delete");

		vehicleFromPrevStep.putUrlParameters("insurancePolicy", new ArrayList() {{ add(isuranceJson1); add(isuranceJson2); }});
		vehicleFromPrevStep.getInsurancePolicyReq().clear();
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

	}
	
	@Then("^изменить ТС: полное количество полей, внести/изменить информацию о техобслуживании$")
	public void allFieldsEditMaintenance() throws Exception {
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");
//		filesMap.put("InsurancePolicy_1", "src\\test\\resources\\Test_Data\\insurancePolicy\\ip_04.jpg");

		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");

		log.debug("add 1 maintenance");
		JSONObject initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		vehicleFromPrevStep.getMaintenancesReq().addAll((List)((JSONObject) initJson.get("technicalState")).get("maintenances"));
		vehicleFromPrevStep.putUrlParameters("technicalState", initJson.get("technicalState"));
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);
		
		
		log.debug("edit 1 maintenance");
		initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		String maintenancesId1 = vehicleFromPrevStep.getMaintenancesIdsList().get(0).toString();
		final JSONObject maintenancesJson1st = (JSONObject) ((List) ((JSONObject) initJson.get("technicalState")).get("maintenances")).get(0);
		maintenancesJson1st.put("id", maintenancesId1);
		maintenancesJson1st.put("entityStatus","Modify");
		vehicleFromPrevStep.getMaintenancesReq().clear();
		vehicleFromPrevStep.getMaintenancesReq().add(maintenancesJson1st);
		JSONObject technicalState = new JSONObject();
		technicalState.put("maintenances",new ArrayList(){{add(maintenancesJson1st);}}   );
		vehicleFromPrevStep.putUrlParameters("technicalState", technicalState);
		
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

		log.debug("add 2 maintenance");
		initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		vehicleFromPrevStep.getMaintenancesReq().add((JSONObject) ((List) ((JSONObject) initJson.get("technicalState")).get("maintenances")).get(0));
		vehicleFromPrevStep.putUrlParameters("technicalState", initJson.get("technicalState"));
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);


		log.debug("remove both");
		vehicleFromPrevStep.getMaintenancesReq().clear();
		final JSONObject maintenancesJson1 = new JSONObject();
		final JSONObject maintenancesJson2 = new JSONObject();
		maintenancesJson1.put("id", vehicleFromPrevStep.getMaintenancesIdsList().get(0).toString());
		maintenancesJson1.put("EntityStatus", "Delete");
		maintenancesJson2.put("id", vehicleFromPrevStep.getMaintenancesIdsList().get(1).toString());
		maintenancesJson2.put("EntityStatus", "Delete");

		technicalState.put("maintenances",new ArrayList(){{add(maintenancesJson1);add(maintenancesJson2);}}   );
		vehicleFromPrevStep.putUrlParameters("technicalState", technicalState);
		
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);


	}
	//
	@Then("^изменить ТС: полное количество полей, внести/изменить информацию о ремонте$")
	public void allFieldsEditRepairs() throws Exception {
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");
		filesMap.put("InsurancePolicy_1", "src\\test\\resources\\Test_Data\\insurancePolicy\\ip_04.jpg");

		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");

		log.debug("add 1 repair");
		JSONObject initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		vehicleFromPrevStep.getRepairsReq().addAll((List) ((JSONObject) initJson.get("technicalState")).get("repairs"));
		vehicleFromPrevStep.putUrlParameters("technicalState", initJson.get("technicalState"));
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);
		
		
		log.debug("edit 1 repair");
		initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		String repairsId1 = vehicleFromPrevStep.getRepairsIdsList().get(0).toString();
		final JSONObject repairsJson1st = (JSONObject) ((List) ((JSONObject) initJson.get("technicalState")).get("repairs")).get(0);
		repairsJson1st.put("id", repairsId1);
		repairsJson1st.put("entityStatus","Modify");
		vehicleFromPrevStep.getRepairsReq().clear();
		vehicleFromPrevStep.getRepairsReq().add(repairsJson1st);
		JSONObject technicalState = new JSONObject();
		technicalState.put("repairs",new ArrayList(){{add(repairsJson1st);}}   );
		vehicleFromPrevStep.putUrlParameters("technicalState", technicalState);
		
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

		log.debug("add 2 repair");
		initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		vehicleFromPrevStep.getRepairsReq().add((JSONObject) ((List) ((JSONObject) initJson.get("technicalState")).get("repairs")).get(0));
		vehicleFromPrevStep.putUrlParameters("technicalState", initJson.get("technicalState"));
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);


		log.debug("remove both");
		vehicleFromPrevStep.getRepairsReq().clear();
		final JSONObject repairsJson1 = new JSONObject();
		final JSONObject repairsJson2 = new JSONObject();
		repairsJson1.put("id", vehicleFromPrevStep.getRepairsIdsList().get(0).toString());
		repairsJson1.put("EntityStatus", "Delete");
		repairsJson2.put("id", vehicleFromPrevStep.getRepairsIdsList().get(1).toString());
		repairsJson2.put("EntityStatus", "Delete");

		technicalState.put("repairs",new ArrayList(){{add(repairsJson1);add(repairsJson2);}}   );
		vehicleFromPrevStep.putUrlParameters("technicalState", technicalState);
		
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

	}
	
	@Then("^изменить ТС: полное количество полей, внести/изменить информацию о ДТП$")
	public void allFieldsEditAccidents() throws Exception {
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");
//		filesMap.put("InsurancePolicy_1", "src\\test\\resources\\Test_Data\\insurancePolicy\\ip_04.jpg");

		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");

		log.debug("add 1 accident");
		JSONObject initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		vehicleFromPrevStep.getAccidentsReq().addAll((List) ((JSONObject) initJson.get("technicalState")).get("accidents"));
		vehicleFromPrevStep.putUrlParameters("technicalState", initJson.get("technicalState"));
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);
		
		
		log.debug("edit 1 accident");
		initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		String accidentsId1 = vehicleFromPrevStep.getAccidentsIdsList().get(0).toString();
		final JSONObject accidentsJson1st = (JSONObject) ((List) ((JSONObject) initJson.get("technicalState")).get("accidents")).get(0);
		accidentsJson1st.put("id", accidentsId1);
		accidentsJson1st.put("entityStatus","Modify");
		vehicleFromPrevStep.getAccidentsReq().clear();
		vehicleFromPrevStep.getAccidentsReq().add(accidentsJson1st);
		JSONObject technicalState = new JSONObject();
		technicalState.put("accidents",new ArrayList(){{add(accidentsJson1st);}}   );
		vehicleFromPrevStep.putUrlParameters("technicalState", technicalState);
		
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

		log.debug("add 2 accident");
		initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		vehicleFromPrevStep.getAccidentsReq().add((JSONObject) ((List) ((JSONObject) initJson.get("technicalState")).get("accidents")).get(0));
		vehicleFromPrevStep.putUrlParameters("technicalState", initJson.get("technicalState"));
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);


		log.debug("remove both");
		vehicleFromPrevStep.getAccidentsReq().clear();
		final JSONObject accidentsJson1 = new JSONObject();
		final JSONObject accidentsJson2 = new JSONObject();
		accidentsJson1.put("id", vehicleFromPrevStep.getAccidentsIdsList().get(0).toString());
		accidentsJson1.put("EntityStatus", "Delete");
		accidentsJson1.put("Damage", "some Damage");
		accidentsJson2.put("id", vehicleFromPrevStep.getAccidentsIdsList().get(1).toString());
		accidentsJson2.put("EntityStatus", "Delete");
		accidentsJson2.put("Damage", "some Damage");

		technicalState.put("accidents",new ArrayList(){{add(accidentsJson1);add(accidentsJson2);}}   );
		vehicleFromPrevStep.putUrlParameters("technicalState", technicalState);
		
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

	}
	
	@Then("^изменить ТС: полное количество полей, внести/изменить информацию о водителе$")
	public void allFieldsEditEmployee() throws Exception {
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");

		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");

		log.debug("add 1st employee");
		JSONObject initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		ArrayList<Employee> listOfEmployees = SharedContainer.getObjectsFromSharedContainer(2,"employees");
		vehicleFromPrevStep.setEmployeeId(listOfEmployees.get(0).getEmployeeId());
		
		vehicleFromPrevStep.putUrlParameters("employeeId", vehicleFromPrevStep.getEmployeeId());
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);
		

		log.debug("add 2nd employee");
		vehicleFromPrevStep.setEmployeeId(listOfEmployees.get(1).getEmployeeId());
		vehicleFromPrevStep.putUrlParameters("employeeId", vehicleFromPrevStep.getEmployeeId());
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);


		log.debug("remove employee");
		vehicleFromPrevStep.setEmployeeId(null);
		vehicleFromPrevStep.getUrlParameters().remove("employeeId");
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);

	}
	
	@Given("^привязать (\\d+) созданное ТМУ к созданному ТС$")
	public void makeRelation_between_DeviceAdnVehicle(int totalNumberUpdVehicles)  {
		ArrayList<TelematicVirtualTracker> telematicsDevicesListFromPrevStep = (ArrayList<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers");
		
		ArrayList<Vehicle> vehiclesFromPrevStep = (ArrayList<Vehicle>) SharedContainer.getObjectsFromSharedContainer(totalNumberUpdVehicles,"vehicles");

		TelematicVirtualTracker device = null;
		Vehicle vehicle = null;

		for(int i=0,used=0;i<totalNumberUpdVehicles && used<totalNumberUpdVehicles;i++){
			String vehicleId = vehiclesFromPrevStep.get(i).getVehicleId();
			if(vehiclesFromPrevStep.get(i).getDeviceId()==null && ! SharedContainer.isObjectUsed(vehicleId)){
				vehicle = vehiclesFromPrevStep.get(i);
				for(  TelematicVirtualTracker tmpdevice : telematicsDevicesListFromPrevStep){
					if(! SharedContainer.isObjectUsed(tmpdevice.getTrackerId())){
						log.debug("we can use veh "+vehiclesFromPrevStep.get(i).getVehicleId()+ "  device "+tmpdevice.getTrackerId());
						device = tmpdevice;
						break;
					}
				}
				
				vehicle.setTelematicsDevice(device);
				vehicle.setDeviceId(device.getTrackerId().toString());
				vehicle.putUrlParameters("deviceId", vehicle.getDeviceId());
				editedVehicle = (JSONObject) vehicle.updateVehicle().get(0);
				vehicle.updateInSharedContainer();
				SharedContainer.addToUsedObjects("vehicles",vehicle.getVehicleId());
				SharedContainer.addToUsedObjects("virtualTrackers",device.getTrackerId());
				
			}
		}
		
		log.debug("vehicles were updated ");

	}
	
	@Given("^Привязать ТМУ к ТС$")
	public void makeRelation_between_DeviceAdnVehicle()  {
		makeRelation_between_DeviceAdnVehicle(1);
	}
	
	@Given("^Сменить ТС ТМУ$")
	public void changeDeviceToVehicle(){
		List<TelematicVirtualTracker> telematics_deviceList = (List<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		TelematicVirtualTracker telematics_device = null;

		 for(TelematicVirtualTracker device : telematics_deviceList){
				 if(device.getResourceId()==null || ! device.getResourceId().equals(vehicleFromPrevStep.getVehicleId())){
					 telematics_device = device;
					 break;
				 }
			 }
		 
		vehicleFromPrevStep.setTelematicsDevice(telematics_device);
		vehicleFromPrevStep.setDeviceId(telematics_device.getTrackerId());
		vehicleFromPrevStep.putUrlParameters("deviceId", vehicleFromPrevStep.getDeviceId());
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle().get(0);
		vehicleFromPrevStep.updateInSharedContainer();
		SharedContainer.addToUsedObjects("vehicles",vehicleFromPrevStep.getVehicleId());
		SharedContainer.addToUsedObjects("virtualTrackers",telematics_device.getTrackerId());

	}
	
	@Then("^Отвязать ТМУ от ТС$")
	public void removeRelation_between_DeviceAdnVehicle()  {
		log.debug("start vehicle update");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		vehicleFromPrevStep.setTelematicsDevice(null);
		vehicleFromPrevStep.setDeviceId(null);
		vehicleFromPrevStep.putUrlParameters("deviceId", null);
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle().get(0);
		vehicleFromPrevStep.updateInSharedContainer();
		SharedContainer.addToUsedObjects("vehicles",vehicleFromPrevStep.getVehicleId());
		log.debug("vehicles were updated ");
	}
	
	@Given("^изменить ТС: внести созданного сотрудника в качестве водителя$")
	public void addEmployee_as_a_Driver_Step()  {
		addEmployee_as_a_Driver(0);
	}
	
	@Given("^Привязать Сотрудника к ТС$")
	public void addEmployee_to_Veh()  {
		addEmployee_as_a_Driver(0);
	}
	
	@Given("^Привязать Сотрудника (\\d+) к ТС$")
	public void addEmployee_to_Veh(int emplIndex)  {
		addEmployee_as_a_Driver(emplIndex-1);
	}
	
	public void addEmployee_as_a_Driver(int emplIndex)  {
		HashMap filesMap = new HashMap();
		filesMap.put("Avatar", "src\\test\\resources\\Test_Data\\avatar\\avatar_04.jpg");

		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");

		if(vehicleFromPrevStep.getGroupId()!=null && ! vehicleFromPrevStep.getGroupId().equals("000000000000000000000000")){
			vehicleFromPrevStep.putUrlParameters("groupId", vehicleFromPrevStep.getGroupId());
		}
		
		log.debug("add 1st employee");
		JSONObject initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));
		ArrayList<Employee> listOfEmployees = SharedContainer.getObjectsFromSharedContainer("all","employees");
		vehicleFromPrevStep.setEmployeeId(listOfEmployees.get(emplIndex).getEmployeeId());
		
		vehicleFromPrevStep.putUrlParameters("employeeId", vehicleFromPrevStep.getEmployeeId());
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle(filesMap).get(0);
		vehicleFromPrevStep.updateInSharedContainer();

	}
	
	@Then("^Отвязать Сотрудника от ТС$")
	public void removeRelation_between_EmployeeAdnVehicle()  {
		log.debug("start vehicle update: remove empl");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		vehicleFromPrevStep.setEmployeeId(null);
		vehicleFromPrevStep.putUrlParameters("employeeId", null);
		editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle().get(0);
		vehicleFromPrevStep.updateInSharedContainer();
	}
	
	@Given("^Привязать ТС к группе$")
	public void minFieldsWithGroup() {

		log.debug("Start>> vehicle update add Group");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");

//		vehicle = new Vehicle();
//		vehicle.group = group.initJson;
		vehicleFromPrevStep.getGroup().put("id", group.getGroupId());
		vehicleFromPrevStep.getGroup().put("name", group.getName());
		vehicleFromPrevStep.setGroupId(group.getGroupId());
		vehicleFromPrevStep.setGroupName(group.getName());
		vehicleFromPrevStep.putUrlParameters("groupId", group.getGroupId());

		JSONObject updatedvehicle = (JSONObject) vehicleFromPrevStep.updateVehicle().get(0);
		vehicleFromPrevStep.updateInSharedContainer();
		
		group.addObjectToGroup(vehicleFromPrevStep.getVehicleId(),"Vehicle");
		group.updateInSharedContainer();
		
		log.debug("end of vehicle");
	}
	
	 @Given("^Сменить для ТС группу$")
		public void changeGroup()  {
		 log.debug("update vehicle: change group");
		 Vehicle vehicleFromPrevStep = null;
		 Group groupFromPrevStep = null;
		// String emplGroup =null;
		 List<Vehicle> vehiclesList = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all","vehicles");
		 List<Group> groupList = (List<Group>) SharedContainer.getObjectsFromSharedContainer("all","groups");
		 for(Vehicle vehicle : vehiclesList){
			// emplGroup=employee.groupId;
			 if(vehicle.getGroupId()!=null && ! vehicle.getGroupId().equals("000000000000000000000000")){
				 vehicleFromPrevStep = vehicle;
				 break;
			 }
		 }
		 
		 String oldGroupId = vehicleFromPrevStep.getGroupId();
		 for(Group group : groupList){
			if(!group.getGroupId().equals(vehicleFromPrevStep.getGroupId())){
				groupFromPrevStep = group;
				break;
			}
		 }
		 vehicleFromPrevStep.setGroupId(groupFromPrevStep.getGroupId());
		 vehicleFromPrevStep.setGroupName(groupFromPrevStep.getName());
		 vehicleFromPrevStep.getGroup().put("id", groupFromPrevStep.getGroupId());
		 vehicleFromPrevStep.getGroup().put("name", groupFromPrevStep.getName());
		 vehicleFromPrevStep.putUrlParameters("groupId",groupFromPrevStep.getGroupId());
		 editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle().get(0);
		 
		Group group = Group.getFromSharedContainer(oldGroupId);
		group.removeObjectInGroup(vehicleFromPrevStep.getVehicleId());
		group.updateInSharedContainer();
			
		 groupFromPrevStep.addObjectToGroup(vehicleFromPrevStep.getVehicleId(),"Vehicle");
		 groupFromPrevStep.updateInSharedContainer();
	 }
	 
	 @Given("^Отвязать группу от ТС$")
		public void removeGroupRelation()  {
		 Vehicle vehicleFromPrevStep = null;
		 List<Vehicle> vehiclesList = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all","vehicles");
		 for(Vehicle vehicle: vehiclesList){
			 if(vehicle.getGroupId()!=null)
				 vehicleFromPrevStep = vehicle;
		 }
		 String oldGroup = vehicleFromPrevStep.getGroupId();
		 vehicleFromPrevStep.setGroupId(null);
//		 vehicleFromPrevStep.urlParameters.put("groupId","000000000000000000000000");
		 vehicleFromPrevStep.getUrlParameters().remove("groupId");
		 editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle().get(0);
		 vehicleFromPrevStep.updateInSharedContainer();
		 
			Group group = Group.getFromSharedContainer(oldGroup);
			group.removeObjectInGroup(vehicleFromPrevStep.getVehicleId());
			group.updateInSharedContainer();
	 }
	 
	 @Given("^Отвязать ТС от группы$")
		public void removeGroupRelation2()  {
		 removeGroupRelation();
	 }

		@Then("^Отредактировать текстовые поля ТС$")
		public void updateVehTextFields() {
			log.debug("Start vehicle update: text fields");

			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("vehicle",getProperty("stdPattern"));

			vehicleFromPrevStep.setName(initMap.get("name").toString());
			vehicleFromPrevStep.setVinCode(initMap.get("vinCode").toString());
			vehicleFromPrevStep.setNumber(initMap.get("vehicle_number").toString());
			vehicleFromPrevStep.setMark(initMap.get("mark").toString());
			vehicleFromPrevStep.setGarageNumber(initMap.get("garageNumber").toString());
			vehicleFromPrevStep.setMaxSpeed(initMap.get("maxSpeed").toString());
			vehicleFromPrevStep.setDescription(initMap.get("description").toString());
			 if(vehicleFromPrevStep.getGroupId() != null)
				 vehicleFromPrevStep.putUrlParameters("groupId", vehicleFromPrevStep.getGroupId());
			
			JSONObject customer = new JSONObject();
			customer.put("id", vehicleFromPrevStep.getCustomerId());
			
//			vehicleFromPrevStep.urlParameters.clear();
			vehicleFromPrevStep.putUrlParameters("name", vehicleFromPrevStep.getName());
			vehicleFromPrevStep.putUrlParameters("vinCode", vehicleFromPrevStep.getVinCode());
			vehicleFromPrevStep.putUrlParameters("Number",  vehicleFromPrevStep.getNumber());
			vehicleFromPrevStep.putUrlParameters("mark",  vehicleFromPrevStep.getMark());
			vehicleFromPrevStep.putUrlParameters("garageNumber",  vehicleFromPrevStep.getGarageNumber());
			vehicleFromPrevStep.putUrlParameters("maxSpeed",  vehicleFromPrevStep.getMaxSpeed());
			vehicleFromPrevStep.putUrlParameters("description",  vehicleFromPrevStep.getDescription());
			vehicleFromPrevStep.putUrlParameters("customer", customer);

			vehicleFromPrevStep.updateVehicle();

			log.debug("End vehicle update: text fields");
		}
	 
	 @Given("^Деактивировать ТС$")
	 public void deactivateVeh() {
		 log.debug("start deactivateVeh");
		 Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		 vehicleFromPrevStep.setIsDeactivated("true");
		 if(vehicleFromPrevStep.getGroupId() != null)
			 vehicleFromPrevStep.putUrlParameters("groupId", vehicleFromPrevStep.getGroupId());
		 if(vehicleFromPrevStep.getEmployeeId() != null){
			 vehicleFromPrevStep.setEmployeeId(null);
			 vehicleFromPrevStep.getUrlParameters().remove("employeeId");
		 }
		 vehicleFromPrevStep.putUrlParameters("id", vehicleFromPrevStep.getVehicleId());
		 vehicleFromPrevStep.putUrlParameters("isDeactivated", "true");
		 editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle().get(0);
		 vehicleFromPrevStep.updateInSharedContainer();
		 log.debug("end deactivateVeh");
	 }

	 @Then("^Активировать ТС$")
	 public void activateVeh() {
		 log.debug("start activateVeh");
		 Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		 vehicleFromPrevStep.setIsDeactivated("false");
		 vehicleFromPrevStep.setDeviceId(null); //after deactivation relation to device has to be destroyed
		 vehicleFromPrevStep.setTelematicsDevice(null);
		 if(vehicleFromPrevStep.getGroupId() != null){
			 vehicleFromPrevStep.putUrlParameters("groupId", vehicleFromPrevStep.getGroupId());
		 }
		 vehicleFromPrevStep.putUrlParameters("id", vehicleFromPrevStep.getVehicleId());
		 vehicleFromPrevStep.putUrlParameters("isDeactivated", "false");
		 if(vehicleFromPrevStep.getUrlParameters().containsKey("deviceId"))
			 vehicleFromPrevStep.getUrlParameters().remove("deviceId");
		 if(vehicleFromPrevStep.getUrlParameters().containsKey("employeeId"))
			 vehicleFromPrevStep.getUrlParameters().remove("employeeId");
		 editedVehicle = (JSONObject) vehicleFromPrevStep.updateVehicle().get(0);
		 vehicleFromPrevStep.updateInSharedContainer();
		 log.debug("end activateVeh");
	 }
	 
		@Then("^Сдвинуть дату создания ТС на (\\d+) дней назад, считая от текущей даты$")
		public void updateVehicleCreationDate(int numOfDays) {
			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			Bson filter = Filters.and(eq("_id", new ObjectId(vehicleFromPrevStep.getVehicleId())));
			long currTimeInMs = System.currentTimeMillis();
			long inMs24hrs = 24 * 60 * 60 * 1000L;
			long newCreationDate = currTimeInMs - (numOfDays * inMs24hrs);
			HashMap<String, Object> newCreationDateObj = new HashMap<String, Object>();
			newCreationDateObj.put("Created", new Date(newCreationDate));
			com.t1.core.mongodb.MongoConnection.updateCollection("Vehicles", filter, newCreationDateObj);
	 	}
}