package notifications_notificationRules;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Geozone;
import com.t1.core.api.Notification;
import com.t1.core.api.NotificationEvent;
import com.t1.core.api.NotificationRule;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class POST_NotificationRules extends AbstractClass{
	public Notification notification; 
	public NotificationRule rule;
	public NotificationEvent event; 
	
//for rules
	@Then("^создать новое правило согласно параметрам из таблицы: \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void create(String roadEventType, String inputResourcesFilterGroups, String inputResourcesFilterInclude, String inputResourcesFilterExclude,String daysOfWeek,String timeFrom,
			String timeTo, String title, String isImportant,String showOnDisplay,String roadEventArgumentsGeozones, String limitationName,String limitationValue,String limitationName2,String limitationValue2) {

	log.debug("Start>> create notif rule test="+timeFrom+"=="+timeTo+"=="+roadEventType);
		
	
	
		rule = new NotificationRule( roadEventType, inputResourcesFilterGroups, inputResourcesFilterInclude, inputResourcesFilterExclude,daysOfWeek,timeFrom,timeTo, 
									title, isImportant,showOnDisplay,roadEventArgumentsGeozones,limitationName,limitationValue,limitationName2,limitationValue2 );
		final JSONObject createdRule = (JSONObject) rule.createRule();
		
		SharedContainer.setContainer("notificationRules", (new ArrayList(){{add(rule);}}));
		
	}
	
	//for rules on\off
		@Then("^создать новое правило трижды, согласно параметрам из таблицы: \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
		public void create3Times(String roadEventType, String inputResourcesFilterGroups, String inputResourcesFilterInclude, String inputResourcesFilterExclude,String daysOfWeek,String timeFrom,
				String timeTo, String title, String isImportant,String showOnDisplay,String roadEventArgumentsGeozones, String limitationName,String limitationValue,String limitationName2,String limitationValue2) {

		log.debug("Start>> create notif rule test="+timeFrom+"=="+timeTo+"=="+roadEventType);

		for(int i=0; i<3;i++){
			rule = new NotificationRule( roadEventType, inputResourcesFilterGroups, inputResourcesFilterInclude, inputResourcesFilterExclude,daysOfWeek,timeFrom,timeTo, 
										title, isImportant,showOnDisplay,roadEventArgumentsGeozones,limitationName,limitationValue,limitationName2,limitationValue2 );
			final JSONObject createdRule = (JSONObject) rule.createRule();
			
			SharedContainer.setContainer("notificationRules", (new ArrayList(){{add(rule);}}));
		}	
		}
	
	//for roadEventsGenerator
	@Then("^создать новое правило согласно параметрам из таблицы: \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void create1(String roadEventType, String inputResourcesFilterGroups,  String inputResourcesFilterExclude,String daysOfWeek,String timeFrom,
			String timeTo, String title, String isImportant,String showOnDisplay,String roadEventArgumentsGeozones, String limitationName,String limitationValue,String limitationName2,String limitationValue2) {

	log.debug("Start>> create notif rule test="+timeFrom+"=="+timeTo+"=="+roadEventType);
		
	 
			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			String inputResourcesFilterInclude = vehicleFromPrevStep.getVehicleId();
			
			if(limitationName.equals("geozones")){
				if(SharedContainer.getContainers().containsKey("geozonesForNotificationRules")){
					List<Geozone> geozonesFromPrevStep =	SharedContainer.getContainers().get("geozonesForNotificationRules");
			//	Geozone geozoneFromPrevStep = (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozonesForNotificationRules").get(0);
					limitationValue="[";
					for(int i=0;i<geozonesFromPrevStep.size();i++){
						limitationValue +=geozonesFromPrevStep.get(i).getGeozoneId();
					}
				limitationValue +="]";// geozoneFromPrevStep.geozoneId;
				} else{
					assertTrue(false, "не найдены геозоны для правила оповещений");
				}
			}
		rule = new NotificationRule( roadEventType, inputResourcesFilterGroups, inputResourcesFilterInclude, inputResourcesFilterExclude,daysOfWeek,timeFrom,timeTo, 
									title, isImportant,showOnDisplay,roadEventArgumentsGeozones,limitationName,limitationValue,limitationName2,limitationValue2 );
		final JSONObject createdRule = (JSONObject) rule.createRule();
		
		SharedContainer.setContainer("notificationRules", (new ArrayList(){{add(rule);}}));
		
	}
	//for vehicle || employee delete check
	@Then("^создать новое правило \"Превышение установленного порога скорости\" для объекта созданного в предыдущем шаге.$")
	public void create() {

	log.debug("Start>> create notif rule UserSpeedlimit for vehicle || employee delete check");
		
		String daysOfWeek= "Monday,Tuesday,Wednesday,Thursday,Friday";
		String roadEventArgumentsType = "Tone.Core.Data.UserSpeedlimitArguments, Tone.Core";	
		String inputResourcesFilterInclude="";
		List emplArr = SharedContainer.getObjectsFromSharedContainer("all","employees");
		List vehicleArr = SharedContainer.getObjectsFromSharedContainer("all","vehicles");
		if(emplArr.size()>0){
			Employee employeeFromPrevStep = (Employee) emplArr.get(0);
			inputResourcesFilterInclude = employeeFromPrevStep.getEmployeeId();
		}else if(vehicleArr.size()>0){
			Vehicle vehicleFromPrevStep = (Vehicle) vehicleArr.get(0);
			inputResourcesFilterInclude = vehicleFromPrevStep.getVehicleId();
		}else {
			assertTrue(false,"невозможно создать правило,объекты из предыдущего шага не найдены");
		}

		rule = new NotificationRule( "UserSpeedlimit", "", inputResourcesFilterInclude, "",daysOfWeek,"00:00:01","23:59:59", 
									"JFW_UserSpeedLimit", "false","false",roadEventArgumentsType,"speedlimit","100","","" );
		final JSONObject createdRule = (JSONObject) rule.createRule();
		SharedContainer.setContainer("notificationRules", (new ArrayList(){{add(rule);}}));
		
	}
	
	
	public void update(){
		assertTrue(false, "NOT IMPLEMENTED");
	}
	
	
	@Then("^запросить выключение созданных правил оповещений$")
	public void notifocationRuleTurnOff() {
		List<NotificationRule> rulesList =  SharedContainer.getFromContainer("notificationRules");
		List rulesIds = new ArrayList<String>();
		for (NotificationRule rule : rulesList){
			rulesIds.add(rule.getRuleId());
		}
		JSONObject createdRules = (JSONObject) new NotificationRule().turnOffRule(rulesIds);
	}
	
	@Then("^запросить включение созданных правил оповещений$")
	public void notifocationRuleTurnOn() {
		List<NotificationRule> rulesList =  SharedContainer.getFromContainer("notificationRules");
		List rulesIds = new ArrayList<String>();
		for (NotificationRule rule : rulesList){
			rulesIds.add(rule.getRuleId());
		}
		
		JSONObject createdRules = (JSONObject) new NotificationRule().turnOnRule(rulesIds);
	}
	

//	http://map-tone-test.qa.t1-group.ru/NotificationRules/Delete/595258585532fa1354e3b561
//		Request Method:POST	

	@Then("^удалить созданное правило$")
	public void delete() {
		NotificationRule rule;
		rule = (NotificationRule) SharedContainer.getFromContainer("notificationRules").get(0);
		
		log.debug("Start>> delete notif rule test="+rule.getRuleId());
		JSONObject createdRules = (JSONObject) rule.deleteRule();
		SharedContainer.removeFromContainer("notificationRules",rule.getRuleId());
		

	}
	
	@Then("^удалить созданное правило по id: \"([^\"]*)\"$")
	public void delete(String ruleId) {

	log.debug("Start>> delete notif rule test="+ruleId);
		
	
	
		rule = new NotificationRule(   );
		JSONObject createdRules = (JSONObject) rule.deleteRule();

	}
}