package businessObjects_AdminPanel_Employees;

import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;

import cucumber.api.java.en.Given;

public class DELETE_ApiAdmin_Employees {

	@Given("^В Админке удалить Сотрудника$")
	public void deleteEmployeeAdminPanelResources(){

		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		employeeFromPrevStep.deleteEmployeeInAdminPanelResorces();
		SharedContainer.removeFromContainer("employees", employeeFromPrevStep.getEmployeeId());

	}
	
}
