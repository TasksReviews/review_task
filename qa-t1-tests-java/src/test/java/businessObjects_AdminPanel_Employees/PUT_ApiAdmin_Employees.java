package businessObjects_AdminPanel_Employees;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Customer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class PUT_ApiAdmin_Employees extends AbstractClass{

	
	@Given("^В Админке привязать 1 созданное ТМУ к созданному cотруднику$")
	public void addDeviceToEmpl(){
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Customer customerFromPrevStep = Employee.getCustomerFromContainer();
//			Employee newEmpl = new Employee(false);
		employeeFromPrevStep.setDeviceId(telematics_device.getTrackerId());
		JSONObject deviceObj =new JSONObject();
		deviceObj.put("id",telematics_device.getTrackerId());
		employeeFromPrevStep.putUrlParameters("device", deviceObj);
		employeeFromPrevStep.updateEmployeesInAdminPanelResources();
		employeeFromPrevStep.updateInSharedContainer();

	}
	
	@Given("^В Админке привязать ТМУ к Сотруднику$")
	public void addDeviceToEmpl2(){
		addDeviceToEmpl();
	}
	
	
	@Given("^В Админке привязать ТС к Сотруднику$")
	public void addVehToEmpl_Step(){
		addVehToEmpl(-1);
	}
	
	@Given("^В Админке привязать ТС (\\d+) к Сотруднику$")
	public void addVehToEmpl_Step(int vehIndex){
		addVehToEmpl(vehIndex-1);
	}
	
	public void addVehToEmpl(int vehIndex) {
		Vehicle vehicleFromPrevStep = null;
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Customer customerFromPrevStep = Employee.getCustomerFromContainer();
		if (vehIndex == -1) {
			List<Vehicle> listOfVehicles = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
			for (Vehicle vehicle : listOfVehicles) {
				if (vehicle.getEmployeeId() == null) {
					vehicleFromPrevStep = vehicle;
				}
			}
		} else {
			vehicleFromPrevStep = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(vehIndex);
		}
		JSONObject customer = new JSONObject();
		customer.put("id", employeeFromPrevStep.getCustomerId());

		employeeFromPrevStep.setVehicle(vehicleFromPrevStep);
		employeeFromPrevStep.setVehicleId(vehicleFromPrevStep.getVehicleId());
		JSONObject vehicleInfo = new JSONObject();
		vehicleInfo.put("id", vehicleFromPrevStep.getVehicleId());

		employeeFromPrevStep.getUrlParameters().clear();
		employeeFromPrevStep.putUrlParameters("customer", customer);
		employeeFromPrevStep.putUrlParameters("vehicle", vehicleInfo);

		JSONObject updadteEmployeeReply = (JSONObject) employeeFromPrevStep.updateEmployeesInAdminPanelResources().get(0);

		SharedContainer.addToUsedObjects("employees", employeeFromPrevStep.getEmployeeId());
		SharedContainer.addToUsedObjects("vehicles", vehicleFromPrevStep.getVehicleId());

		employeeFromPrevStep.updateInSharedContainer();

	}
	@Then("^В Админке привязать Сотрудника к группе$")
	public void addEmplToGroup() {
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
		Customer customerFromPrevStep = Employee.getCustomerFromContainer();

		JSONObject customer = new JSONObject();
		customer.put("id", employeeFromPrevStep.getCustomerId());

		employeeFromPrevStep.setGroupId(group.getGroupId());
		employeeFromPrevStep.setGroupName(group.getName());
		JSONObject groupInfo = new JSONObject();
		groupInfo.put("id", group.getGroupId());
		groupInfo.put("name", group.getName());
		
		employeeFromPrevStep.getUrlParameters().clear();
		employeeFromPrevStep.putUrlParameters("customer", customer);
		employeeFromPrevStep.putUrlParameters("group", groupInfo);

			JSONObject updadteEmployeeReply = (JSONObject) employeeFromPrevStep.updateEmployeesInAdminPanelResources().get(0);
				
			SharedContainer.addToUsedObjects("employees",employeeFromPrevStep.getEmployeeId());

			employeeFromPrevStep.updateInSharedContainer();
	}
	
	
	
	
	@Then("^В Админке сменить для cотрудника группу$")
	public void changeEmplGroup() {
		Group newEmplGroup = null;
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		List<Group> groupList = (List<Group>) SharedContainer.getObjectsFromSharedContainer("all","groups");
		Customer customerFromPrevStep = Employee.getCustomerFromContainer();
		JSONObject customer = new JSONObject();
		customer.put("id", employeeFromPrevStep.getCustomerId());

		
		for (Group group : groupList){
			if(!group.getObjects().contains(employeeFromPrevStep.getEmployeeId()))
				newEmplGroup = group;
		}
		
		employeeFromPrevStep.setGroupId(newEmplGroup.getGroupId());
		employeeFromPrevStep.setGroupName(newEmplGroup.getName());
		JSONObject groupInfo = new JSONObject();
		groupInfo.put("id", newEmplGroup.getGroupId());
		groupInfo.put("name", newEmplGroup.getName());
		
		employeeFromPrevStep.getUrlParameters().clear();
		employeeFromPrevStep.putUrlParameters("customer", customer);
		employeeFromPrevStep.putUrlParameters("group", groupInfo);

			JSONObject updadteEmployeeReply = (JSONObject) employeeFromPrevStep.updateEmployeesInAdminPanelResources().get(0);
				
			SharedContainer.addToUsedObjects("employees",employeeFromPrevStep.getEmployeeId());

			employeeFromPrevStep.updateInSharedContainer();
			
			newEmplGroup.addObjectToGroup(employeeFromPrevStep.getEmployeeId(), "Employee");
			newEmplGroup.updateInSharedContainer();
	}
	
	
	@SuppressWarnings("unchecked")
	@Then("^В Админке отредактировать текстовые поля Сотрудника$")
	public void editEmployeeTextFields() {
		log.debug("Start vehicle update: text fields");
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Customer customerFromPrevStep = Employee.getCustomerFromContainer();
		JSONObject init = new InitDefValues().initializeInputWithDefaultOrRandomValues("employee",getProperty("stdPattern"));
		
		employeeFromPrevStep.setFirstname(init.get("firstname").toString());
		employeeFromPrevStep.setLastname(init.get("lastname").toString());
		employeeFromPrevStep.setPhone(init.get("phone").toString());
		employeeFromPrevStep.setEmail(init.get("email").toString());
		employeeFromPrevStep.setStatus(init.get("status").toString());

		employeeFromPrevStep.putUrlParameters("firstname", employeeFromPrevStep.getFirstname());
		employeeFromPrevStep.putUrlParameters("lastname", employeeFromPrevStep.getLastname());
		employeeFromPrevStep.putUrlParameters("phone",  employeeFromPrevStep.getPhone());
		employeeFromPrevStep.putUrlParameters("email", employeeFromPrevStep.getEmail());
		employeeFromPrevStep.putUrlParameters("EmployeeStatus",  employeeFromPrevStep.getStatus());
		
		JSONObject updadteEmployeeReply = (JSONObject) employeeFromPrevStep.updateEmployeesInAdminPanelResources().get(0);
		employeeFromPrevStep.updateInSharedContainer();
	}
	
	@Given("^В Админке отвязать ТС от Сотрудника$")
	public void removeVehRelationInEmpl(){
		Vehicle vehicleFromPrevStep = null;
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Customer customerFromPrevStep = Employee.getCustomerFromContainer();
		JSONObject customer = new JSONObject();
		customer.put("id", employeeFromPrevStep.getCustomerId());

		employeeFromPrevStep.setVehicle(null);
		employeeFromPrevStep.setVehicleId(null);

		
		employeeFromPrevStep.getUrlParameters().clear();
		employeeFromPrevStep.putUrlParameters("customer", customer);
		JSONObject vehId = new JSONObject();
		vehId.put("id", null);
		employeeFromPrevStep.putUrlParameters("vehicle", vehId);

			JSONObject updadteEmployeeReply = (JSONObject) employeeFromPrevStep.updateEmployeesInAdminPanelResources().get(0);
				
			SharedContainer.addToUsedObjects("employees",employeeFromPrevStep.getEmployeeId());
			employeeFromPrevStep.updateInSharedContainer();

	}
	
	
	@Then("^В Админке сменить [С,с]отруднику ТМУ$")
	public void changeDeviceForEmployee()  {
	 log.debug("update employee: change device");
	 Employee employeeFromPrevStep = null;
	 TelematicVirtualTracker device = null;

	 Customer customerFromPrevStep = Employee.getCustomerFromContainer();
	 List<Employee> employeesList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
	 ArrayList<TelematicVirtualTracker> telematicsDevicesListFromPrevStep = (ArrayList<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers");
	 for(Employee employee : employeesList){
		 if(employee.getDeviceId()!=null && ! employee.getDeviceId().equals("000000000000000000000000")){
			 employeeFromPrevStep = employee;
			 break;
		 }
	 }
	 for(TelematicVirtualTracker tracker : telematicsDevicesListFromPrevStep){
		if(!tracker.getTrackerId().equals(employeeFromPrevStep.getDeviceId())){
			device = tracker;
			break;
		}
	 }
	 
		JSONObject customer = new JSONObject();
		customer.put("id", employeeFromPrevStep.getCustomerId());

		
	 employeeFromPrevStep.setTelematicsDevice(device);
	 employeeFromPrevStep.setDeviceId(device.getTrackerId().toString());
	 employeeFromPrevStep.putUrlParameters("customer", customer);
	 employeeFromPrevStep.putUrlParameters("deviceId", employeeFromPrevStep.getDeviceId());
	 JSONObject updadteEmployeeReply = (JSONObject) employeeFromPrevStep.updateEmployeesInAdminPanelResources().get(0);
	 employeeFromPrevStep.updateInSharedContainer();
	 
 }
	
	@Given("^В Админке отвязать ТМУ от Сотрудника$")
	public void removeDeviceRelationInEmpl(){
		Vehicle vehicleFromPrevStep = null;
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Customer customerFromPrevStep = Employee.getCustomerFromContainer();

		JSONObject customer = new JSONObject();
		customer.put("id", employeeFromPrevStep.getCustomerId());

		employeeFromPrevStep.setDeviceId(null);

		employeeFromPrevStep.getUrlParameters().clear();
		employeeFromPrevStep.putUrlParameters("customer", customer);
		JSONObject device = new JSONObject();
		device.put("id", null);
		employeeFromPrevStep.putUrlParameters("device", device);

			JSONObject updadteEmployeeReply = (JSONObject) employeeFromPrevStep.updateEmployeesInAdminPanelResources().get(0);
				
			SharedContainer.addToUsedObjects("employees",employeeFromPrevStep.getEmployeeId());
			employeeFromPrevStep.updateInSharedContainer();

	}

	@Then("В Админке привязать сотрудника к группе$")
	public void createEmployeeInAdminPanelWithGroup(){
		log.debug("Start employee update: add group");
		Employee employeeFromPrevStep = null;
		List<Employee> employeesFromPrevStep = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
		for (Employee employee : employeesFromPrevStep) {
			if (employee.getGroupId() == null || employee.getGroupId() =="" ||employee.getGroupId() =="000000000000000000000000") {
				employeeFromPrevStep = employee;
			}
		}
		
		List customerFromPrevStepLst = (List) SharedContainer.getObjectsFromSharedContainer("any","customers");
		Customer customerFromPrevStep;
		if(customerFromPrevStepLst == null || customerFromPrevStepLst.isEmpty()){
			customerFromPrevStep = new Customer();
			JSONObject json_cust=	 (JSONObject) SharedContainer.getLastObjectFromSharedContainer("customer");
			try {//this is wrong way! but at least we get id and name
				customerFromPrevStep.setCustomerId(ReplyToJSON.extractPathFromJson(json_cust, "$._id.$oid"));
			} catch (Exception e) {
				log.error(ERROR,e);
			}
			customerFromPrevStep.setName(json_cust.get("Name").toString());
		} else {
			customerFromPrevStep = (Customer) customerFromPrevStepLst.get(0);
		}
        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
        JSONObject groupInfo = new JSONObject();
        groupInfo.put("id", group.getGroupId());
        groupInfo.put("name", group.getName());
        employeeFromPrevStep.setGroupId(group.getGroupId());

        employeeFromPrevStep.setCustomerId(customerFromPrevStep.getCustomerId());
        employeeFromPrevStep.setCustomerName(customerFromPrevStep.getName());
		JSONObject json_customer = new JSONObject();
		json_customer.put("id",employeeFromPrevStep.getCustomerId());
		json_customer.put("name",employeeFromPrevStep.getCustomerName());
		employeeFromPrevStep.putUrlParameters("customer",json_customer);
		employeeFromPrevStep.putUrlParameters("group",groupInfo);
		employeeFromPrevStep.updateEmployeesInAdminPanelResources();
		employeeFromPrevStep.updateInSharedContainer();
		
		group.addObjectToGroup(employeeFromPrevStep.getEmployeeId(),"Employee");
		group.updateInSharedContainer();
		 
			 
			log.debug("End employee update: add group");

		
	}
	
	@Then("^В Админке отвязать группу от Сотрудника$")
	public void updateEmplGroupRemove() {
		log.debug("Start employee update: remove group");
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		String oldGroup =employeeFromPrevStep.getGroupId();
		JSONObject dummyGroup = new JSONObject();
		dummyGroup.put("name", "Без группы");
		dummyGroup.put("id", "000000000000000000000000");

		JSONObject customer = new JSONObject();
		customer.put("id", employeeFromPrevStep.getCustomerId());
		employeeFromPrevStep.setGroupId("000000000000000000000000");
		employeeFromPrevStep.getUrlParameters().clear();
		employeeFromPrevStep.putUrlParameters("customer", customer);
		employeeFromPrevStep.putUrlParameters("group", dummyGroup);
		String param = "{\"customer\":{\"id\":\""+employeeFromPrevStep.getCustomerId()+"\"},\"group\":{\"id\":\"000000000000000000000000\",\"name\":\"Без группы\"}}";
		employeeFromPrevStep.updateEmployeesInAdminPanelResources();
		employeeFromPrevStep.updateInSharedContainer();
		
		Group group = Group.getFromSharedContainer(oldGroup);
		group.removeObjectInGroup(employeeFromPrevStep.getEmployeeId());
		group.updateInSharedContainer();
		
		log.debug("End employee update: remove group");
	}
	
	@Then("^В Админке отвязать сотрудника от группы$")
	public void updateEmplGroupRemove2() {
		updateEmplGroupRemove();
	}
	
	
	@Then("^В Админке деактивировать Сотрудника$")
	public void updateEmplDeactivate() {
		log.debug("Start employee update: Deactivate");
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		
		JSONObject customer = new JSONObject();
		customer.put("id", employeeFromPrevStep.getCustomerId());
		employeeFromPrevStep.setIsDeactivated(true);
		employeeFromPrevStep.getUrlParameters().clear();
		employeeFromPrevStep.putUrlParameters("customer", customer);
		employeeFromPrevStep.putUrlParameters("isDeactivated", true);
		employeeFromPrevStep.updateEmployeesInAdminPanelResources();
		employeeFromPrevStep.updateInSharedContainer();
		log.debug("End employee update: Deactivate");
	}
	
	@Then("^В Админке активировать Сотрудника$")
	public void updateEmplActivate() {
		log.debug("Start employee update: Activate");
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		employeeFromPrevStep.setDeviceId(null); //after deactivation relation to device has to be destroyed
		employeeFromPrevStep.setTelematicsDevice(null);
		JSONObject customer = new JSONObject();
		customer.put("id", employeeFromPrevStep.getCustomerId());
		employeeFromPrevStep.setIsDeactivated(false);
		employeeFromPrevStep.getUrlParameters().clear();
		employeeFromPrevStep.putUrlParameters("customer", customer);
		employeeFromPrevStep.putUrlParameters("isDeactivated", false);
		if(employeeFromPrevStep.getUrlParameters().containsKey("device"))
			employeeFromPrevStep.getUrlParameters().remove("device");
		employeeFromPrevStep.updateEmployeesInAdminPanelResources();
		employeeFromPrevStep.updateInSharedContainer();
		log.debug("End employee update: Activate");
	}
}
