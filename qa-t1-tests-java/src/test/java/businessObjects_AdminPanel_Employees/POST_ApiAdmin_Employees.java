package businessObjects_AdminPanel_Employees;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Customer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_ApiAdmin_Employees extends AbstractClass {

	
	@Given("В Админке создать (\\d) [С,с]отрудника с минимальным набором полей для созданного клиента$")
	public void createSeveralEmployeesInAdminPanel(int totalEmpl){
		List customerFromPrevStepLst = (List) SharedContainer.getObjectsFromSharedContainer("any","customers");
		Customer customerFromPrevStep;
		if(customerFromPrevStepLst == null || customerFromPrevStepLst.isEmpty()){
			customerFromPrevStep = new Customer();
			JSONObject json_cust=	 (JSONObject) SharedContainer.getLastObjectFromSharedContainer("customer");
			try {//this is wrong way! but at least we get id and name
				customerFromPrevStep.setCustomerId(ReplyToJSON.extractPathFromJson(json_cust, "$._id.$oid"));
			} catch (Exception e) {
				log.error(ERROR,e);
			}
			customerFromPrevStep.setName(json_cust.get("Name").toString());
		} else {
			customerFromPrevStep = (Customer) customerFromPrevStepLst.get(0);
		}
		for(int i=0; i<totalEmpl; i++){
			Employee newEmpl = new Employee(false);
			newEmpl.getUrlParameters().remove("status");
			newEmpl.putUrlParameters("EmployeeStatus",newEmpl.getStatus());
			newEmpl.setCustomerId(customerFromPrevStep.getCustomerId());
			newEmpl.setCustomerName(customerFromPrevStep.getName());
			JSONObject json_customer = new JSONObject();
			json_customer.put("id",newEmpl.getCustomerId());
			json_customer.put("name",newEmpl.getCustomerName());
			newEmpl.putUrlParameters("customer",json_customer);
			newEmpl.createEmployeesInAdminPanelResources();
			SharedContainer.setContainer("employees", newEmpl);
		}
		
	}
	
	
	@Given("В Админке создать сотрудника с группой и с ТС$")
	public void createEmployeeInAdminPanelWithGroupAndVeh() {
		Vehicle vehicleFromPrevStep = null;
		Customer customerFromPrevStep = getCustomerFromContainer();
		Group group = (Group) SharedContainer.getObjectsFromSharedContainer("all", "groups").get(0);
		JSONObject groupInfo = new JSONObject();
		groupInfo.put("id", group.getGroupId());
		groupInfo.put("name", group.getName());
		List<Vehicle> listOfVehicles = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
		for (Vehicle vehicle : listOfVehicles) {
			if (vehicle.getEmployeeId() == null) {
				vehicleFromPrevStep = vehicle;
			}
		}

		Employee newEmpl = new Employee(false);
		newEmpl.setVehicle(vehicleFromPrevStep);
		newEmpl.setVehicleId(vehicleFromPrevStep.getVehicleId());
		JSONObject vehicleInfo = new JSONObject();
		vehicleInfo.put("id", vehicleFromPrevStep.getVehicleId());
		newEmpl.setGroupId(group.getGroupId());
		newEmpl.setGroupName(group.getName());

		newEmpl.getUrlParameters().remove("status");
		newEmpl.getUrlParameters().remove("type");
		// newEmpl.putUrlParameters("EmployeeStatus",newEmpl.status);
		newEmpl.setCustomerId(customerFromPrevStep.getCustomerId());
		newEmpl.setCustomerName(customerFromPrevStep.getName());
		JSONObject json_customer = new JSONObject();
		json_customer.put("id", newEmpl.getCustomerId());
		json_customer.put("name", newEmpl.getCustomerName());
		newEmpl.putUrlParameters("customer", json_customer);
		newEmpl.putUrlParameters("vehicle", vehicleInfo);
		newEmpl.putUrlParameters("group", groupInfo);
		newEmpl.createEmployeesInAdminPanelResources();
		newEmpl.addToSharedContainer();

		group.addObjectToGroup(newEmpl.getEmployeeId(), "Employee");
		group.updateInSharedContainer();
		
		vehicleFromPrevStep.setEmployeeId(newEmpl.getEmployeeId());
		vehicleFromPrevStep.updateInSharedContainer();
	}
	
	
	@Then("В Админке создать сотрудника с группой$")
	public void createEmployeeInAdminPanelWithGroup() {
		Customer customerFromPrevStep = getCustomerFromContainer();
		Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
		JSONObject groupInfo = new JSONObject();
		groupInfo.put("id", group.getGroupId());
		groupInfo.put("name", group.getName());

		Employee newEmpl = new Employee(false);

		newEmpl.getUrlParameters().remove("status");
		newEmpl.getUrlParameters().remove("type");
		newEmpl.setCustomerId(customerFromPrevStep.getCustomerId());
		newEmpl.setCustomerName(customerFromPrevStep.getName());
		newEmpl.setGroupId(group.getGroupId());
		newEmpl.setGroupName(group.getName());
		JSONObject json_customer = new JSONObject();
		json_customer.put("id", newEmpl.getCustomerId());
		json_customer.put("name", newEmpl.getCustomerName());
		newEmpl.putUrlParameters("customer", json_customer);
		newEmpl.putUrlParameters("group", groupInfo);
		newEmpl.createEmployeesInAdminPanelResources();

		newEmpl.addToSharedContainer();

		group.addObjectToGroup(newEmpl.getEmployeeId(), "Employee");
		group.updateInSharedContainer();

	}
	
	@Given("В Админке создать Сотрудника с ТС$")
	public void createEmployeeInAdminPanelWithVeh() {
		Vehicle vehicleFromPrevStep = null;
		Customer customerFromPrevStep = getCustomerFromContainer();
		List<Vehicle> listOfVehicles = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
		for (Vehicle vehicle : listOfVehicles) {
			if (vehicle.getEmployeeId() == null) {
				vehicleFromPrevStep = vehicle;
			}
		}

		Employee newEmpl = new Employee(false);
		newEmpl.setVehicle(vehicleFromPrevStep);
		newEmpl.setVehicleId(vehicleFromPrevStep.getVehicleId());
		JSONObject vehicleInfo = new JSONObject();
		vehicleInfo.put("id", vehicleFromPrevStep.getVehicleId());

		newEmpl.getUrlParameters().remove("status");
		newEmpl.getUrlParameters().remove("type");
		newEmpl.setStatus(null);
		newEmpl.setEmployeeType(null);
		// newEmpl.putUrlParameters("EmployeeStatus",newEmpl.status);
		newEmpl.setCustomerId(customerFromPrevStep.getCustomerId());
		newEmpl.setCustomerName(customerFromPrevStep.getName());
		JSONObject json_customer = new JSONObject();
		json_customer.put("id", newEmpl.getCustomerId());
		json_customer.put("name", newEmpl.getCustomerName());
		newEmpl.putUrlParameters("customer", json_customer);
		newEmpl.putUrlParameters("vehicle", vehicleInfo);
		newEmpl.createEmployeesInAdminPanelResources();
		newEmpl.addToSharedContainer();

		vehicleFromPrevStep.setEmployeeId(newEmpl.getEmployeeId());
		vehicleFromPrevStep.updateInSharedContainer();

	}
	
	@Given("В Админке создать Сотрудника с ТС (\\d)$")
	public void createEmployeeInAdminPanelWithVeh(int vehIndex) {
		Customer customerFromPrevStep = getCustomerFromContainer();
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(vehIndex-1);

		Employee newEmpl = new Employee(false);
		newEmpl.setVehicle(vehicleFromPrevStep);
		newEmpl.setVehicleId(vehicleFromPrevStep.getVehicleId());
		JSONObject vehicleInfo = new JSONObject();
		vehicleInfo.put("id", vehicleFromPrevStep.getVehicleId());

		newEmpl.getUrlParameters().remove("status");
		newEmpl.getUrlParameters().remove("type");
		newEmpl.setStatus(null);
		newEmpl.setEmployeeType(null);
		// newEmpl.putUrlParameters("EmployeeStatus",newEmpl.status);
		newEmpl.setCustomerId(customerFromPrevStep.getCustomerId());
		newEmpl.setCustomerName(customerFromPrevStep.getName());
		JSONObject json_customer = new JSONObject();
		json_customer.put("id", newEmpl.getCustomerId());
		json_customer.put("name", newEmpl.getCustomerName());
		newEmpl.putUrlParameters("customer", json_customer);
		newEmpl.putUrlParameters("vehicle", vehicleInfo);
		newEmpl.createEmployeesInAdminPanelResources();
		newEmpl.addToSharedContainer();

		vehicleFromPrevStep.setEmployeeId(newEmpl.getEmployeeId());
		vehicleFromPrevStep.updateInSharedContainer();

	}
	
	@Given("В Админке создать (\\d) Сотрудника с ТМУ для созданного клиента$")
	public void createSeveralEmployeesWithTrackerInAdminPanel(int totalEmpl){
		Customer customerFromPrevStep = getCustomerFromContainer();
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		for(int i=0; i<totalEmpl; i++){
			Employee newEmpl = new Employee(false);
			newEmpl.getUrlParameters().remove("status");
			newEmpl.putUrlParameters("EmployeeStatus",newEmpl.getStatus());
			newEmpl.setCustomerId(customerFromPrevStep.getCustomerId());
			newEmpl.setCustomerName(customerFromPrevStep.getName());
			JSONObject json_customer = new JSONObject();
			json_customer.put("id",newEmpl.getCustomerId());
			json_customer.put("name",newEmpl.getCustomerName());
			newEmpl.putUrlParameters("customer",json_customer);
			
			newEmpl.setDeviceId(telematics_device.getTrackerId());
			JSONObject deviceObj =new JSONObject();
			deviceObj.put("id",telematics_device.getTrackerId());
			newEmpl.putUrlParameters("device", deviceObj);

			newEmpl.createEmployeesInAdminPanelResources();
			SharedContainer.setContainer("employees", newEmpl);
		}
		
	}
	
	//should we move it to sharedContainer class or just correct customer creation step??
	public static Customer getCustomerFromContainer(){
		List customerFromPrevStepLst = (List) SharedContainer.getObjectsFromSharedContainer("any","customers");
		Customer customerFromPrevStep;
		if(customerFromPrevStepLst == null || customerFromPrevStepLst.isEmpty()){
			customerFromPrevStep = new Customer();
			JSONObject json_cust=	 (JSONObject) SharedContainer.getLastObjectFromSharedContainer("customer");
			try {//this is wrong way! but at least we get id and name
				customerFromPrevStep.setCustomerId(ReplyToJSON.extractPathFromJson(json_cust, "$._id.$oid"));
			} catch (Exception e) {
				log.error(ERROR,e);
			}
			customerFromPrevStep.setName(json_cust.get("Name").toString());
		} else {
			customerFromPrevStep = (Customer) customerFromPrevStepLst.get(0);
		}
		return customerFromPrevStep;
	}
}
