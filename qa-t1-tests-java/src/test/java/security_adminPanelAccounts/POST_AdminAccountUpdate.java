package security_adminPanelAccounts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Department;

//import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Given;

public class POST_AdminAccountUpdate extends AbstractClass {
	public AccountRegistrationAuth account; 

	
	@Given("^изменить аккаунт в панели администратора: на полное количество полей$")
	public  void updateAccFromAdminPanelToAllFields() {
		try {
			AccountRegistrationAuth accountfromPrevStep = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			String token ="";
			if(SharedContainer.getContainers().containsKey("demoToken")){
				 token = getToken("demoToken");
			} else {
				 token = getToken("userToken");
			}
			log.info("Start 'Positive_test_with_Random_Data'");
			//why we do init here? must be made into AccountRegistrationAuth
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
			
			Department department = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
			initMap.put("departmentId", department.getDepartmentId());
			initMap.put("profile_avatarUri","custom");
			HashMap filesMap = new HashMap();
			filesMap.put("Avatar", 		  "src\\test\\resources\\Test_Data\\avatar\\avatar_01.jpg");
			
			account	= new AccountRegistrationAuth(initMap,true);
			JSONObject updatedAcc	= account.updateAccFromAdminPanel(initMap, token,filesMap, accountfromPrevStep.getAccountId(),"");
			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
		} catch (Exception e) {
			log.error(ERROR,e);
		}

	}
	
	@Given("^изменить аккаунт в панели администратора: на минимальное количество полей$")
	public  void updateAccFromAdminPanelToMinFields() {
		try {
			AccountRegistrationAuth accountfromPrevStep = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			String token ="";
			if(SharedContainer.getContainers().containsKey("demoToken")){
				 token = getToken("demoToken");
			} else {
				 token = getToken("userToken");
			}
			log.info("Start 'Positive_test_with_Random_Data'");
			//why we do init here? must be made into AccountRegistrationAuth
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
			//remove all except required
			String[] minimalfields = new String[] {"login","password","email","accountPhone","firstname","lastname","sex","profile_avatarUri","account_role","customerId"};
			Iterator<String> initFieldsIter = initMap.keySet().iterator();
			String field="";
			while (initFieldsIter.hasNext()) {
				field = initFieldsIter.next();
				if(! Arrays.asList(minimalfields).contains(field) ){
					initFieldsIter.remove();
				}
			}
			initMap.put("IgnoreCustomer", false);
			account	= new AccountRegistrationAuth(initMap,false);
			JSONObject updatedAcc	= account.updateAccFromAdminPanel(initMap, token,null, accountfromPrevStep.getAccountId(), "");
			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
		} catch (Exception e) {
			log.error(ERROR,e);
		}
		
	}
	
	@Given("^изменить аккаунт в панели администратора: полное количество полей, новые значения$")
	public  void updateAccFromAdminPanelAllFieldsNewValues() {
		try {
			AccountRegistrationAuth accountfromPrevStep = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			String token ="";
			if(SharedContainer.getContainers().containsKey("demoToken")){
				 token = getToken("demoToken");
			} else {
				 token = getToken("userToken");
			}
			log.info("Start 'Positive_test_with_Random_Data'");
			//why we do init here? must be made into AccountRegistrationAuth
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
			
			Department department = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");

			initMap.put("departmentId", department.getDepartmentId());
			initMap.put("profile_avatarUri","custom");
			HashMap filesMap = new HashMap();
			filesMap.put("Avatar", 		  "src\\test\\resources\\Test_Data\\avatar\\avatar_01.jpg");
			
			account	= new AccountRegistrationAuth(initMap,true);
			JSONObject updatedAcc	= account.updateAccFromAdminPanel(initMap, token,filesMap, accountfromPrevStep.getAccountId(), "");

			SharedContainer.setContainer("accounts", (new ArrayList(){{add(account);}}));
		} catch (Exception e) {
			log.error(ERROR,e);
		}
	}


}