package security_adminPanelAccounts;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class GET_AdminAccounts extends AccountRegistrationAuth{
	
	@Then("^Запросить список всех аккаунтов в системе$")
	public static void getAllAccounts(){

		log.debug("Start>> GET_AdminAccounts getAllAccounts");

		List<JSONObject> receivedVehicle = (List<JSONObject>) new AccountRegistrationAuth().getAllAccountsFromAdminPanel();
		log.debug("end of get all accounts");
	}



}