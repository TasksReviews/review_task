package security_adminPanelAccounts;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

//import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Given;

public class POST_AdminAccountRemove extends AbstractClass {
	public AccountRegistrationAuth account; 

	
	@Given("^удалить аккаунт из панели администратора$")
	public  void removeAccInAdminPanel() {
		try {

			log.info("Start 'acc remove'");
			account = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
			account.removeAccountInAdminPanel();
		} catch (Exception e) {
			log.error(ERROR,e);
		}

	}

}