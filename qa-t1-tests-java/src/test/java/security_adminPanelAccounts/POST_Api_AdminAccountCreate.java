package security_adminPanelAccounts;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.api.Department;
import com.t1.core.api.Employee;

//import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Given;

public class POST_Api_AdminAccountCreate extends AbstractClass {
	public AccountRegistrationAuth account; 

	
	@Given("^создать новый аккаунт, в панели администратора, с минимальным количеством полей$")
//	Only: 		Login,Phone,Email,Type,Firstname,Lastname,Sex,AvatarUri,Role, AvatarUri, 	CustomerId?
	public  void registerNewAccFromAdminPanelMinFields() {
		try {
			String token =getCurAuthAccToken("authorizedAccount_admPanel");

			log.info("Start 'Positive_test_with_Random_Data'");
			//why we do init here? must be made into AccountRegistrationAuth
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
			//remove all except required
			String[] minimalfields = new String[] {"login","password","email","accountPhone","firstname","lastname","sex","profile_avatarUri","account_role","customerId"};
			Iterator<String> initFieldsIter = initMap.keySet().iterator();
			String field="";
			while (initFieldsIter.hasNext()) {
				field = initFieldsIter.next();
				if(! Arrays.asList(minimalfields).contains(field) ){
					initFieldsIter.remove();
				}
			}
			initMap.put("IgnoreCustomer", false);
			account	= new AccountRegistrationAuth(initMap,false);
		JSONObject registeredNewAcc	= account.registerNewAccFromAdminPanel(initMap, token,null, "");
			account.setAccountId(registeredNewAcc.get("accountId").toString());
			SharedContainer.setContainer("accounts", account);
		} catch (Exception e) {
			log.error(ERROR,e);
		}

	}
	
	@Given("^создать новый аккаунт, в панели администратора, с полным количеством полей$")
	public  void registerNewAccFromAdminPanelAllFields() {
		try {
			String token =getCurAuthAccToken("authorizedAccount_admPanel");
//			if(SharedContainer.getContainers().containsKey("demoToken")){
//				 token = getToken("demoToken");
//			} else {
//				 token = getToken("userToken");
//			}
			log.info("Start 'Positive_test_with_Random_Data'");
			//why we do init here? must be made into AccountRegistrationAuth
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account",getProperty("stdPattern"));
			
			Department department = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
			Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
			initMap.put("employeeId", employee.getEmployeeId());
			initMap.put("departmentId", department.getDepartmentId());//ReplyToJSON.extractPathFromJson(department, "$._id.$oid"));
			initMap.put("profile_avatarUri","custom");
			HashMap filesMap = new HashMap();
			filesMap.put("Avatar", 		  "src\\test\\resources\\Test_Data\\avatar\\avatar_01.jpg");
			
			account	= new AccountRegistrationAuth(initMap,true);
			JSONObject registeredNewAcc	= account.registerNewAccFromAdminPanel(initMap, token,filesMap, "");
			account.setAccountId(registeredNewAcc.get("accountId").toString());
			SharedContainer.setContainer("accounts", account);
		} catch (Exception e) {
			log.error(ERROR,e);
		}

	}

	
	@Given("^Создать аккаунт в панели администратора с role = \"([^\"]*)\"$")
	// Only: Login,Phone,Email,Type,Firstname,Lastname,Sex,AvatarUri,Role,
	// AvatarUri, CustomerId?
	public void registerNewAccFromAdminPanel_role(String role) {
		try {
			log.info("Start 'registerNewAccFromAdminPanel_role'");
			String token =getCurAuthAccToken("authorizedAccount_admPanel");//getToken("devToken");
			String[] roles = new String[] { "Admin", "Demo", "Courier", "Driver", "Operator", "Dispatcher", "Analyst",
					"TransportExpert", "TechnicalExpert", "FinancialExpert", "Chief", "InsuranceAnalyst" };
			if (!Arrays.asList(roles).contains(role)) {
				role = rolesOnRussianMap.get(role).toString();
			}
			
			//why we do init here? must be made into AccountRegistrationAuth
			JSONObject initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account", getProperty("stdPattern"));
			// remove all except required
			String[] minimalfields = new String[] { "login", "password", "email", "accountPhone", "firstname",
					"lastname", "sex", "profile_avatarUri", "account_role", "customerId" };
			Iterator<String> initFieldsIter = initMap.keySet().iterator();
			String field = "";
			while (initFieldsIter.hasNext()) {
				field = initFieldsIter.next();
				if (!Arrays.asList(minimalfields).contains(field)) {
					initFieldsIter.remove();
				}
			}
			initMap.put("IgnoreCustomer", false);
			initMap.put("account_role", role);
			account = new AccountRegistrationAuth(initMap,false);
			JSONObject registeredNewAcc = account.registerNewAccFromAdminPanel(initMap, token, null, "");
			account.setAccountId(registeredNewAcc.get("accountId").toString());
			SharedContainer.setContainer("accounts", account);
		} catch (Exception e) {
			log.error(ERROR,e);
		}

	}
}