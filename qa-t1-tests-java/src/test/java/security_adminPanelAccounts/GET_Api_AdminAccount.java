package security_adminPanelAccounts;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.SharedContainer;
import com.t1.core.api.AccountRegistrationAuth;

import cucumber.api.java.en.Then;

public class GET_Api_AdminAccount extends AccountRegistrationAuth{
	
	@Then("^Запросить структуры всех аккаунтов в системе$")
	public static void getAllAccountsWithStructure(){

		log.debug("Start>> GET_ApiAdminAccount getAllAccountsWithStructure");

		List<JSONObject> receivedAcctListStructure = (List<JSONObject>) new AccountRegistrationAuth().getAllAccountsWithStructureFromAdminPanel();
		log.debug("end of get all accounts");
	}

	@Then("^Запросить структуру созданного в предыдущем шаге аккаунта$")
	public static void getCreatedAccountStructureInfo(){

		log.debug("Start>> GET_ApiAdminAccount getCreatedAccountStructureInfo");
		AccountRegistrationAuth accountfromPrevStep = (AccountRegistrationAuth) SharedContainer.getFromContainer("accounts").get(0);
	//	JSONObject receivedAcctStructure = (JSONObject)
				accountfromPrevStep.getAccountStructureFromAdminPanel();
		log.debug("end of get all accounts");
	}
	

}