package businessObjects_Resources;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;

import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ResourcesStepsWithChecks extends AbstractClass {

	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) событий$")
	public void проверить_что_вернулись_событий(int totalEventsExpected) {
		JSONObject receivedRoadEvents = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("receivedRoadEvents");
		assertTrue(receivedRoadEvents!=null, "не получен ответ API из предыдущего шага");
//		log.debug("eventsTotalCount====== "+receivedRoadEvents.get("eventsTotalCount"));
		assertTrue("API: check eventsTotalCount field", Integer.parseInt(receivedRoadEvents.get("eventsTotalCount").toString())== totalEventsExpected 
				&& Integer.parseInt(receivedRoadEvents.get("eventsTotalCount").toString())>=0, "wrong number of events received:"+receivedRoadEvents.get("eventsTotalCount")+",expected "+totalEventsExpected);
		assertEquals("API: check events size", ((ArrayList) receivedRoadEvents.get("events")).size(), totalEventsExpected, "wrong number of events received.");
		setToAllureChechkedFields();
	}
	
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) событий типа <(.*)>$")
	public void checkReturnedXeventsWithType(int totalEventsExpected, String eventsType) {
		if(eventsType.startsWith("\"") && eventsType.endsWith("\"")){  //just a regular value to compare
			eventsType = eventsType.substring(1, eventsType.length()-1);
		}
		JSONObject receivedRoadEvents = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("receivedRoadEvents");
		assertTrue(receivedRoadEvents!=null, "не получен ответ API из предыдущего шага");
		ArrayList<JSONObject> events = (ArrayList) receivedRoadEvents.get("events");
		int totalFoundEvents = 0;
		if (eventsType.equals("Превышение скорости по ПДД") ) {
			eventsType="TrafficRulesSpeedLimit";
		} else if (eventsType.contains("Превышение скорости")) {
			eventsType="UserSpeedlimit";
		} else if (eventsType.contains("Стоянка")) {
			eventsType="StopEvent";
		} else if (eventsType.contains("Холостой ход")) {
			eventsType="IdleEvent";
		} else if (eventsType.contains("Резкое торможение")) {
			eventsType="SharpBraking";
		} else if (eventsType.contains("Резкое ускорение")) {
			eventsType="SharpSpeedup";
		} else if (eventsType.contains("Нажатие тревожной кнопки")) {
			eventsType="PanicButton";
		} else if (eventsType.contains("Резкие повороты")) {
			eventsType="SharpTurn";
		}
		for(JSONObject event : events){
			if(event.containsKey("roadEventType") && event.get("roadEventType").equals(eventsType)){
				totalFoundEvents ++;
			}
		}
		assertEquals("API: check "+eventsType+" events total", totalFoundEvents, totalEventsExpected, "wrong number of "+eventsType+" events received.");
		setToAllureChechkedFields();
	}
	
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) событий типа \"([^\"]*)\"$")
	public void checkReturnedXeventsWithType2(int totalEventsExpected, String eventsType) {
		checkReturnedXeventsWithType(totalEventsExpected, eventsType);
	}
	
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что события типа <\"([^\"]*)\"> не вернулись$")
	public void checkNoReturnedEventsWithType(String eventsType) {
		JSONObject receivedRoadEvents = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("receivedRoadEvents");
		assertTrue(receivedRoadEvents!=null, "не получен ответ API из предыдущего шага");
		ArrayList<JSONObject> events = (ArrayList) receivedRoadEvents.get("events");
		for(JSONObject event : events){
			if(event.containsKey("roadEventType") && event.get("roadEventType").equals(eventsType)){
				assertTrue("check no event type != "+eventsType, false, "ответ не должен сожержать событий типа "+eventsType);
			}
		}
	}
	

	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для ивента (\\d+) для параметра <\"([^\"]*)\"> получено значение <(.*)>$")
	public void проверить_что_для_ивента_для_параметра_получено_значение(int eventIndex, String roadEventParam, String roadEventValue)  {
		JSONObject receivedRoadEvents = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("receivedRoadEvents");
		assertTrue(receivedRoadEvents!=null, "не получен ответ API из предыдущего шага");
		ArrayList<JSONObject> events = (ArrayList) receivedRoadEvents.get("events");
		assertTrue("eventList contains event №"+eventIndex, events.size()>=eventIndex, "список содержит "+events.size()+" событий, событие №"+eventIndex+" не может быть проверено");
		JSONObject event = events.get(eventIndex-1);
		
		if(roadEventValue.startsWith("\"") && roadEventValue.endsWith("\"")){  //just a regular value to compare
			roadEventValue = roadEventValue.substring(1, roadEventValue.length()-1);
		} else {  // values like: <число = (текуший день - 3); время = "21:54:00">
			SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
			if(roadEventValue.contains("число =") && roadEventValue.contains("время =")){
				Calendar calCur = Calendar.getInstance();
				  String[] arr = roadEventValue.split(";");
				  if(arr[0].contains("текуший день")){
					  
					  int daysToSubstract = 0;
					  Pattern pattern = Pattern.compile("(\\d+)");
					  Matcher matcher = pattern.matcher(arr[0]);
					  if (matcher.find())
					  {
						  daysToSubstract = Integer.parseInt(matcher.group(1));
					  }
//						log.debug("daysToSubstract=="+daysToSubstract);
						calCur.add(Calendar.DATE, - (daysToSubstract));
						calCur.get(Calendar.YEAR);
				  }
				  if(arr[1].contains("время")){
					  Pattern pattern = Pattern.compile("\"(.*)\"");
					  Matcher matcher = pattern.matcher(arr[1]);
					  if (matcher.find())
					  {
						  roadEventValue = dfDate.format(calCur.getTime()) +" "+ matcher.group(1);
						 
					  }
				  }
				  
			} if (roadEventValue.contains("Имя объекта")){
				roadEventValue = event.get("relatedResourceName").toString();  
			} if (roadEventValue.contains("id объекта")){
				roadEventValue = event.get("relatedResourceId").toString(); 
			}
		}
		if(event.keySet().contains(roadEventParam)){
			assertEquals("API: check event parameter "+roadEventParam+" value", event.get(roadEventParam).toString(), roadEventValue, "wrong value for parameter: "+roadEventParam);
		} else {
			assertTrue(false, "ответ API не содержит поле "+roadEventParam);
		}
		setToAllureChechkedFields();
	}
	
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что возвращенные события получены в хронологическом порядке от старых к новым$")
	public void проверить_что_возвращенные_события_получены_в_хронологическом_порядке_от_старых_к_новым(){
		try {
			JSONObject receivedRoadEvents = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("receivedRoadEvents");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
	
			Date prevEventDateTime= df.parse("2000-01-01 00:00:00");
			Date curEventDateTime=null;
			ArrayList<JSONObject> events = (ArrayList) receivedRoadEvents.get("events");
			for(JSONObject event : events){
				if(event.containsKey("eventTime") && event.get("eventTime")!="" && AbstractClass.isADateTime((String) event.get("eventTime"))){
	
						curEventDateTime = df.parse(event.get("eventTime").toString());
						assertTrue("API: check order is accurate", prevEventDateTime.before(curEventDateTime), "wrong order");
						prevEventDateTime= curEventDateTime;
				}
			}
			
			setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
	}
		
}
