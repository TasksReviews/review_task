package businessObjects_Resources;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.ReadFile;
import com.t1.core.SharedContainer;
import com.t1.core.TearDownExecutor;
import com.t1.core.WriteToFile;
import com.t1.core.api.Employee;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;
import com.t1.core.mongodb.DeleteDocumentFromCollection;
import com.t1.core.mongodb.GetDevices;
import com.t1.core.mongodb.InsertToDeviceHistory;
import com.t1.core.mongodb.InsertToVehicleEmployeeHistory;
import com.t1.core.mongodb.InsertTrackPointToDeviceFullRecords;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class PrepareRoadEventsTrackSteps extends AbstractClass {

    JSONObject objectsWhoseDeletionIsPostponed = new JSONObject();

    @Then("^Взять документ с точками тестового трека <\"([^\"]*)\">$")
    public void readInputXMLFile(String fileName) {
        log.debug("start read input file");
        String path = "src/test/resources/Test_Data/tracks/" + fileName;
        List<Document> trackPointsFromFile = ReadFile.readMongoJsonFile(path);
        if (trackPointsFromFile.get(0).containsKey("DeviceTime")) {
            for (Document point : trackPointsFromFile) {
                point.put("DeviceTime_orig", point.get("DeviceTime"));
                point.put("ReceivedTime_orig", point.get("ReceivedTime"));
            }
        }

        if (SharedContainer.getContainers().containsKey("trackPointsFromFile")) {
            SharedContainer.removeFromContainer("trackPointsFromFile");
        }
        SharedContainer.setContainer("trackPointsFromFile", trackPointsFromFile);
    }

    @Then("^Присвоить точкам тестового трека DeviceID созданного ТМУ$")
    public void setDeviceIDOfCreatedDeviceToPoints() {
        log.debug("start updating DeviceID into read trackPointsFromFile");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        List<Document> trackPointsFromFile = (List<Document>) SharedContainer.getLastObjectFromSharedContainer("trackPointsFromFile");
        ListIterator<Document> iter = trackPointsFromFile.listIterator();

        while (iter.hasNext()) {
            Document doc = iter.next();
            doc.put("DeviceId", new ObjectId(telematics_device.getTrackerId()));
        }
        SharedContainer.removeFromContainer("trackPointsFromFile");
        SharedContainer.setContainer("trackPointsFromFile", trackPointsFromFile);
    }

    @Then("^Присвоить точкам тестового трека DeviceID созданного ТМУ (\\d+)$")
    public void setDeviceIDOfCreatedDeviceXToPoints(int deviceIndex) {
        log.debug("start updating DeviceID into read trackPointsFromFile");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
        List<Document> trackPointsFromFile = (List<Document>) SharedContainer.getLastObjectFromSharedContainer("trackPointsFromFile");
        ListIterator<Document> iter = trackPointsFromFile.listIterator();

        while (iter.hasNext()) {
            Document doc = iter.next();
            doc.put("DeviceId", new ObjectId(telematics_device.getTrackerId()));
        }
        SharedContainer.removeFromContainer("trackPointsFromFile");
        SharedContainer.setContainer("trackPointsFromFile", trackPointsFromFile);
    }

    @Then("^Присвоить точкам тестового трека DeviceCode созданного ТМУ$")
    public void setDeviceCodeOfCreatedDeviceToPoints() {
        log.debug("start updating DeviceCode into read trackPointsFromFile");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        List<Document> trackPointsFromFile = (List<Document>) SharedContainer.getLastObjectFromSharedContainer("trackPointsFromFile");
        ListIterator<Document> iter = trackPointsFromFile.listIterator();

        while (iter.hasNext()) {
            Document doc = iter.next();
            doc.put("DeviceCode", telematics_device.getTrackerCode());
        }
        SharedContainer.removeFromContainer("trackPointsFromFile");
        SharedContainer.setContainer("trackPointsFromFile", trackPointsFromFile);
    }

    @Then("^Присвоить точкам тестового трека DeviceCode созданного ТМУ (\\d+)$")
    public void setDeviceCodeOfCreatedDeviceXToPoints(int deviceIndex) {
        log.debug("start updating DeviceCode into read trackPointsFromFile");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
        List<Document> trackPointsFromFile = (List<Document>) SharedContainer.getLastObjectFromSharedContainer("trackPointsFromFile");
        ListIterator<Document> iter = trackPointsFromFile.listIterator();

        while (iter.hasNext()) {
            Document doc = iter.next();
            doc.put("DeviceCode", telematics_device.getTrackerCode());
        }
        SharedContainer.removeFromContainer("trackPointsFromFile");
        SharedContainer.setContainer("trackPointsFromFile", trackPointsFromFile);
    }

    @Then("^Сдвинуть \"([^\"]*)\" и \"([^\"]*)\" всех точек тестового трека на (\\d+) дней назад, считая от текущей даты$")
    public void updateDeviceTimeAndReceivedTime_toAFewDaysAgoSinceNow(String deviceTime, String receivedTime, int daysToSubstract) {
        updateDeviceTimeAndReceivedTime_toAFewDaysHoursAgoSinceNow(deviceTime, receivedTime, daysToSubstract, 0);
    }

    @Then("^Сдвинуть \"([^\"]*)\" и \"([^\"]*)\" всех точек тестового трека на (\\d+)\\.(\\d+) дней назад, считая от текущей даты$")
    public void updateDeviceTimeAndReceivedTime_toAFewDaysHoursAgoSinceNow(String deviceTime, String receivedTime, int daysToSubstract, int partOfDayTosubst) {

        log.debug("start updating DeviceTime & ReceivedTime into read trackPointsFromFile: substract " + daysToSubstract + " days");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        List<Document> trackPointsFromFile = (List<Document>) SharedContainer.getLastObjectFromSharedContainer("trackPointsFromFile");
        ListIterator<Document> iter = trackPointsFromFile.listIterator();
        int hoursTosubst = 0;
        while (iter.hasNext()) {
            Document doc = iter.next();
//			log.debug(doc.get("DeviceTime"));
            Calendar calCur = Calendar.getInstance();
            calCur.add(Calendar.DATE, -daysToSubstract);
            calCur.get(Calendar.YEAR);
            Calendar calDev = Calendar.getInstance();
            calDev.setTime((Date) doc.get("DeviceTime_orig"));
            Calendar calRec = Calendar.getInstance();
            calRec.setTime((Date) doc.get("ReceivedTime_orig"));
            // set current date - XX days
            calDev.set(calCur.get(Calendar.YEAR), calCur.get(Calendar.MONTH), calCur.get(Calendar.DATE));
            calRec.set(calCur.get(Calendar.YEAR), calCur.get(Calendar.MONTH), calCur.get(Calendar.DATE));
            if (partOfDayTosubst > 0) {
                hoursTosubst = (int) (24 * (Double.parseDouble("0." + partOfDayTosubst)));
//				log.debug("hoursTosubst="+hoursTosubst);
                //substract part of day
                calDev.add(Calendar.HOUR_OF_DAY, hoursTosubst * -1);
                calRec.add(Calendar.HOUR_OF_DAY, hoursTosubst * -1);
            } else {

            }

//			log.debug("point time="+calDev.getTime());
            doc.put("DeviceTime", calDev.getTime());
            doc.put("ReceivedTime", calRec.getTime());
        }
        SharedContainer.removeFromContainer("trackPointsFromFile");
        SharedContainer.setContainer("trackPointsFromFile", trackPointsFromFile);
        log.debug("end updating DeviceTime & ReceivedTime");
    }

    @Then("^Для всех точек тестового ТМУ (\\d+) обновить адреса$")
    public void updatePointsAddress(int deviceIndex) {
        log.debug("start updating Address into read trackPointsFromFile");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        List<Document> trackPointsFromFile = (List<Document>) SharedContainer.getLastObjectFromSharedContainer("trackPointsFromFile");
        Document lastPoint = trackPointsFromFile.get(trackPointsFromFile.size() - 1);
        int i = 1;
        if (lastPoint.keySet().contains("xgeo_Address")) {
            String address = lastPoint.get("xgeo_Address").toString();
            i = i + Integer.parseInt(address.substring(address.lastIndexOf('_') + 1, address.length()));
        }
        ListIterator<Document> iter = trackPointsFromFile.listIterator();

        while (iter.hasNext()) {
            Document doc = iter.next();
            doc.put("xgeo_Address", "ТМУ_" + deviceIndex + "_" + i);
            i++;
        }
        SharedContainer.removeFromContainer("trackPointsFromFile");
        SharedContainer.setContainer("trackPointsFromFile", trackPointsFromFile);
        log.debug("end updating Address");
    }

    @SuppressWarnings("unused")
    @Then("^Сохранить точки тестового трека в базу$")
    public void сохранить_точки_тестового_трека_в_базу() {
        log.debug("start save track points to MongoDB");
        List<Document> trackPointsFromFile = (List<Document>) SharedContainer.getLastObjectFromSharedContainer("trackPointsFromFile");
        ListIterator<Document> iter = trackPointsFromFile.listIterator();
        com.mongodb.client.MongoCollection<Document> collection = com.t1.core.mongodb.MongoConnection.getMongoCollection("DeviceFullRecords");
        while (iter.hasNext()) {
            Document doc = iter.next();
            Document docToInsert = new Document(doc);
            if (docToInsert.containsKey("_id")) {
                docToInsert.remove("_id");
            }
            if (docToInsert.containsKey("DeviceTime_orig")) {
                docToInsert.remove("DeviceTime_orig");
            }
            if (docToInsert.containsKey("ReceivedTime_orig")) {
                docToInsert.remove("ReceivedTime_orig");
            }
            org.json.simple.JSONObject result = InsertTrackPointToDeviceFullRecords.insertDocument(docToInsert, false);
//			log.debug(result);
//			break;
        }
        log.debug("end, saved " + trackPointsFromFile.size() + " track points to MongoDB");
    }


    //TODO: move following methods to appropriate class ??
    @Then("^Добавить в DeviceHistory для тестового ТМУ и тестового ТС запись с BoundToResourceTime = <([^>]*)>, UnboundFromResourceTime = <([^>]*)>$")
    public void addToDeviceHistoryForDeviceAndVehicleBoundToResourceTime_UnboundFromResourceTime(List boundArr, List unboundArr) {
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        addToDeviceHistoryBoundUnboundToResourceTime(telematics_device.getTrackerId(), "Vehicle", vehicleFromPrevStep.getVehicleId(), boundArr, unboundArr);
    }

    @Then("^Добавить в DeviceHistory для тестового ТМУ (\\d+) и тестового ТС запись с BoundToResourceTime = <([^>]*)>, UnboundFromResourceTime = <([^>]*)>$")
    public void addToDeviceHistoryForDeviceAndVehicleBoundToResourceTime_UnboundFromResourceTime(int deviceIndex, List boundArr, List unboundArr) {
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
        addToDeviceHistoryBoundUnboundToResourceTime(telematics_device.getTrackerId(), "Vehicle", vehicleFromPrevStep.getVehicleId(), boundArr, unboundArr);
    }

    @Then("^Добавить в DeviceHistory для тестового ТМУ и тестового Сотрудника запись с BoundToResourceTime = <([^>]*)>, UnboundFromResourceTime = <([^>]*)>$")
    public void добавить_в_DeviceHistory_для_тестового_ТМУ_и_тестового_Сотрудника_запись_с_BoundToResourceTime_UnboundFromResourceTime_null(List boundArr, List unboundArr) {
        Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        addToDeviceHistoryBoundUnboundToResourceTime(telematics_device.getTrackerId(), "Employee", employeeFromPrevStep.getEmployeeId(), boundArr, unboundArr);
    }

    @Then("^Добавить в DeviceHistory для тестового ТМУ (\\d+) и тестового Сотрудника запись с BoundToResourceTime = <([^>]*)>, UnboundFromResourceTime = <([^>]*)>$")
    public void добавить_в_DeviceHistory_для_тестового_ТМУ_и_тестового_Сотрудника_запись_с_BoundToResourceTime_UnboundFromResourceTime_null(int deviceIndex, List boundArr, List unboundArr) {
        Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
        addToDeviceHistoryBoundUnboundToResourceTime(telematics_device.getTrackerId(), "Employee", employeeFromPrevStep.getEmployeeId(), boundArr, unboundArr);
    }

    public void addToDeviceHistoryBoundUnboundToResourceTime(String deviceId, String resourceType, String resourceId, List boundArr, List unboundArr) {
        Document deviceHistDoc = new Document();
        deviceHistDoc.put("DeviceId", new ObjectId(deviceId));
        deviceHistDoc.put("ResourceId", new ObjectId(resourceId));
        deviceHistDoc.put("BoundToResourceTime", dateTimeCorrectionFromInputArr(boundArr));
        if (!unboundArr.isEmpty() && !unboundArr.get(0).equals("null")) {
            deviceHistDoc.put("UnboundFromResourceTime", dateTimeCorrectionFromInputArr(unboundArr));
        }
        deviceHistDoc.put("RelatedResourceType", resourceType);
        InsertToDeviceHistory.insertDocument(deviceHistDoc, true);
    }


    @Then("^Добавить в VehicleEmployeeHistory для тестового ТС и тестового Сотрудника запись с BoundTime = <([^>]*)>, UnboundTime = <([^>]*)>$")
    public void добавить_в_VehicleEmployeeHistory_для_тестового_ТС_и_тестового_Сотрудника_запись_с_BoundTime_UnboundTime(List boundArr, List unboundArr) {

//		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");


//	    "VehicleId" : ObjectId("58f9d2695532f3c1fc73ced1"),
//	    "EmployeeId" : ObjectId("58f9d2695532f3c1fc73cece"),
//	    "IsCurrent" : true,
//	    "OwnerType" : "Vehicle",
//	    "IsDeleted" : false,
//	    "BoundTime" : ISODate("2017-04-21T09:35:37.820Z")

        Document vehEmplHistDoc = new Document();
        vehEmplHistDoc.put("EmployeeId", new ObjectId(employeeFromPrevStep.getEmployeeId()));
        vehEmplHistDoc.put("VehicleId", new ObjectId(vehicleFromPrevStep.getVehicleId()));
        vehEmplHistDoc.put("OwnerType", "Vehicle");
        vehEmplHistDoc.put("IsDeleted", false);
        vehEmplHistDoc.put("BoundTime", dateTimeCorrectionFromInputArr(boundArr));
        if (!unboundArr.isEmpty() && !unboundArr.get(0).equals("null")) {
            vehEmplHistDoc.put("IsCurrent", false);
            vehEmplHistDoc.put("UnboundTime", dateTimeCorrectionFromInputArr(unboundArr));
        } else {
            vehEmplHistDoc.put("IsCurrent", true);
        }
//		vehEmplHistDoc.put("RelatedResourceType","Vehicle");

        //org.json.simple.JSONObject result =
        InsertToVehicleEmployeeHistory.insertDocument(vehEmplHistDoc, true);
        vehEmplHistDoc.put("OwnerType", "Employee");
        vehEmplHistDoc.remove("_id");
        InsertToVehicleEmployeeHistory.insertDocument(vehEmplHistDoc, true);
//		 log.debug("--fff--"+vehEmplHistDoc);


    }


    @Then("^Отложить удаление из БД созданного ТМУ и точек тестового трека по завершению теста, сохранив id в файл <\"([^\"]*)\">$")
    public void moveDeviceAndTrackPointsFromTeardownToFileStep(String fileName) {
        objectsWhoseDeletionIsPostponed.clear();
        moveDeviceAndTrackPointsFromTeardown();
        WriteToFile.writeJsonToFile(getProperty("defResourcesPath") + fileName, objectsWhoseDeletionIsPostponed);
    }

    @Then("^Отложить удаление из БД всех созданных ТМУ и точек тестового трека по завершению теста, сохранив id в файл <\"([^\"]*)\">$")
    public void moveDevicesAndTrackPointsFromTeardownToFileStep(String fileName) {
        objectsWhoseDeletionIsPostponed.clear();
        JSONObject jsonobj = moveDeviceAndTrackPointsFromTeardown();
        WriteToFile.writeJsonToFile(getProperty("defResourcesPath") + fileName, jsonobj);
    }

    @Then("^Отложить удаление из БД созданного ТС, ТМУ и точек тестового трека по завершению теста, сохранив id в файл <\"([^\"]*)\">$")
    public void moveVehicleDeviceAndTrackPointsFromTeardownToFileStep(String fileName) {
        log.warn("remove from teardown records about Devices and track points!\r\n Make sure test set contains test that removes temporary Devices and track points from DB");
        objectsWhoseDeletionIsPostponed.clear();
        moveDeviceAndTrackPointsFromTeardown();
        moveVehicleFromTeardown();
        WriteToFile.writeJsonToFile(getProperty("defResourcesPath") + fileName, objectsWhoseDeletionIsPostponed);
    }

    @Then("^Отложить удаление из БД созданного ТС, ТМУ,связь ТМУ с объектом и точек тестового трека по завершению теста, сохранив id в файл <\"([^\"]*)\">$")
    public void moveVehicleDeviceDeviceHistoryAndTrackPointsFromTeardownToFileStep(String fileName) {
        log.warn("remove from teardown records about Devices and track points!\r\n Make sure test set contains test that removes temporary Devices and track points from DB");
        objectsWhoseDeletionIsPostponed.clear();
        moveDeviceAndTrackPointsFromTeardown();
        moveVehicleFromTeardown();
        moveDeviceHistoryFromTeardown();
        WriteToFile.writeJsonToFile(getProperty("defResourcesPath") + fileName, objectsWhoseDeletionIsPostponed);
    }

    @SuppressWarnings("unchecked")
    public JSONObject moveDeviceAndTrackPointsFromTeardown() {
        log.warn("remove from teardown records about Devices and track points!\r\n Make sure test set contains test that removes temporary Devices and track points from DB");

        if (!(teardownData == null || teardownData.isEmpty())) {
            if (teardownData.keySet().contains("virtualTrackers")) {
                objectsWhoseDeletionIsPostponed.put("virtualTrackers", teardownData.get("virtualTrackers"));
                teardownData.remove("virtualTrackers");
            }
            if (teardownData.keySet().contains("insertedTrackPoints")) {
                objectsWhoseDeletionIsPostponed.put("insertedTrackPoints", teardownData.get("insertedTrackPoints"));
                teardownData.remove("insertedTrackPoints");
            }

            //WriteToFile.writeJsonToFile(getProperty("defResourcesPath")+fileName, jsonobj);
//			log.debug(jsonobj);
        }
        return objectsWhoseDeletionIsPostponed;
    }

    @SuppressWarnings("unchecked")
    public JSONObject moveVehicleFromTeardown() {
        log.warn("remove from teardown records about Vehicles");

        if (!(teardownData == null || teardownData.isEmpty())) {
            if (teardownData.keySet().contains("vehicles")) {
                objectsWhoseDeletionIsPostponed.put("vehicles", teardownData.get("vehicles"));
                teardownData.remove("vehicles");
            }
        }
        return objectsWhoseDeletionIsPostponed;
    }

    @SuppressWarnings("unchecked")
    public JSONObject moveDeviceHistoryFromTeardown() {
        log.warn("remove from teardown records about Vehicles");

        if (!(teardownData == null || teardownData.isEmpty())) {
            if (teardownData.keySet().contains("insertedDeviceHistoryRecords")) {
                objectsWhoseDeletionIsPostponed.put("insertedDeviceHistoryRecords", teardownData.get("insertedDeviceHistoryRecords"));
                teardownData.remove("insertedDeviceHistoryRecords");
            }
        }
        return objectsWhoseDeletionIsPostponed;
    }

    @Given("^получить данные о созданном ТМУ и точках тестового трека из файла <\"([^\"]*)\">$")
    public void getDataFromFileToContainer1Device(String fileName) {
        List<Document> fileData = ReadFile.readMongoJsonFile(getProperty("defResourcesPath") + fileName);
        Document doc = (Document) fileData.get(0);
        if (doc.keySet().contains("insertedTrackPoints")) {
            SharedContainer.setContainer("insertedTrackPointsIds", (ArrayList) doc.get("insertedTrackPoints"));
            //	TearDownExecutor.addEntityToTeardown((ArrayList)doc.get("insertedTrackPoints"),"insertedTrackPoints");
        }
        if (doc.keySet().contains("virtualTrackers")) {
            String deviceId = ((ArrayList) fileData.get(0).get("virtualTrackers")).get(0).toString();
            //		TearDownExecutor.addEntityToTeardown((ArrayList)fileData.get(0).get("virtualTrackers"),"virtualTrackers");
            TelematicVirtualTracker tracker = new TelematicVirtualTracker(true);
            tracker.getUrlParameters().clear();
            tracker.getInitJson().clear();
            tracker.setTrackerId(deviceId);
            JSONObject deviceInfo = (JSONObject) GetDevices.getDeviceById(deviceId, "").get(0);
            tracker.setTrackerCode(deviceInfo.get("Code").toString());
            tracker.addToSharedContainer();
        }
        //	log.debug("end");
    }

    @Given("^получить данные о созданном ТС из файла <\"([^\"]*)\">$")
    public void getDataFromFileToContainer1Vehicle(String fileName) {
        List<Document> fileData = ReadFile.readMongoJsonFile(getProperty("defResourcesPath") + fileName);
        Document doc = (Document) fileData.get(0);
        if (doc.keySet().contains("vehicles")) {
            String vehicleId = ((ArrayList) fileData.get(0).get("vehicles")).get(0).toString();
            Vehicle vehicle = new Vehicle(false);
            vehicle.setVehicleId(vehicleId);
            vehicle.addToSharedContainer();
        }
    }

    @Given("^получить данные о связи ТС и ТМУ из файла <\"([^\"]*)\">$")
    public void getDataFromFileToContainerDeviceHistory(String fileName) {
        List<Document> fileData = ReadFile.readMongoJsonFile(getProperty("defResourcesPath") + fileName);
        Document doc = (Document) fileData.get(0);
        if (doc.keySet().contains("insertedTrackPointsIds")) {
            String deviceHistRecId = ((ArrayList) fileData.get(0).get("insertedDeviceHistoryRecords")).get(0).toString();
            TearDownExecutor.addinsertedDevHistRecordsToTeardown(deviceHistRecId);
//			Vehicle vehicle = new Vehicle(false);
//			vehicle.setVehicleId(vehicleId);
//			vehicle.addToSharedContainer();
        }
    }

    @Given("^получить данные о созданных ранее ТМУ и точках тестового трека из файла <\"([^\"]*)\">$")
    public void getDataFromFileToContainerAllDevices(String fileName) {
        List<Document> fileData = ReadFile.readMongoJsonFile(getProperty("defResourcesPath") + fileName);
        Document doc = (Document) fileData.get(0);
        if (doc.keySet().contains("insertedTrackPoints")) {
            SharedContainer.setContainer("insertedTrackPointsIds", (ArrayList) doc.get("insertedTrackPoints"));
            //	TearDownExecutor.addEntityToTeardown((ArrayList)doc.get("insertedTrackPoints"),"insertedTrackPoints");
        }
        if (doc.keySet().contains("virtualTrackers")) {
            ArrayList<String> deviceIdArr = (ArrayList) fileData.get(0).get("virtualTrackers");
            //		TearDownExecutor.addEntityToTeardown((ArrayList)fileData.get(0).get("virtualTrackers"),"virtualTrackers");
            for (String deviceId : deviceIdArr) {
                TelematicVirtualTracker tracker = new TelematicVirtualTracker(true);
                tracker.getUrlParameters().clear();
                tracker.getInitJson().clear();
                tracker.setTrackerId(deviceId);
                JSONObject deviceInfo = (JSONObject) GetDevices.getDeviceById(deviceId, "").get(0);
                tracker.setTrackerCode(deviceInfo.get("Code").toString());
                tracker.addToSharedContainer();
            }
        }
        //	log.debug("end");
    }

    @Then("^В коллекцию \"Vehicles\" в запись тестового ТС добавить DeviceId тестового ТМУ$")
    public void addDeviceIdToVehicle() {
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        Bson filter = Filters.and(eq("_id", new ObjectId(vehicleFromPrevStep.getVehicleId())));

        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("DeviceId", new ObjectId(telematics_device.getTrackerId()));
        com.t1.core.mongodb.MongoConnection.updateCollection("Vehicles", filter, newObj);
    }

    @Then("^В коллекцию \"Vehicles\" в запись тестового ТС добавить DeviceId тестового ТМУ (\\d+)$")
    public void addDeviceIdToVehicle(int deviceIndex) {
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
        Bson filter = Filters.and(eq("_id", new ObjectId(vehicleFromPrevStep.getVehicleId())));

        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("DeviceId", new ObjectId(telematics_device.getTrackerId()));
        com.t1.core.mongodb.MongoConnection.updateCollection("Vehicles", filter, newObj);
    }

    @Then("^В коллекцию \"Vehicles\" добавить в запись тестового ТС EmployeeId тестового Сотрудника$")
    public void addEmployeeIdToVehicle() {
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        Bson filter = Filters.and(eq("_id", new ObjectId(vehicleFromPrevStep.getVehicleId())));
        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("EmployeeId", new ObjectId(employeeFromPrevStep.getEmployeeId()));
        com.t1.core.mongodb.MongoConnection.updateCollection("Vehicles", filter, newObj);
    }

    @Then("^В коллекцию \"Employees\" в запись тестового Сотрудника добавить DeviceId тестового ТМУ$")
    public void addDeviceIdToEmployee() {
        Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        Bson filter = Filters.and(eq("_id", new ObjectId(employeeFromPrevStep.getEmployeeId())));

        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("DeviceId", new ObjectId(telematics_device.getTrackerId()));
        com.t1.core.mongodb.MongoConnection.updateCollection("Employees", filter, newObj);
    }

    @Then("^В коллекцию \"Employees\" в запись тестового Сотрудника добавить DeviceId тестового ТМУ (\\d+)$")
    public void addDeviceIdToEmployee(int deviceIndex) {
        Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
        Bson filter = Filters.and(eq("_id", new ObjectId(employeeFromPrevStep.getEmployeeId())));

        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("DeviceId", new ObjectId(telematics_device.getTrackerId()));
        com.t1.core.mongodb.MongoConnection.updateCollection("Employees", filter, newObj);
    }

    @Then("^В коллекцию \"Employees\" добавить в запись тестового Сотрудника VehicleId тестового ТС$")
    public void addVehicleIdToEmployee() {
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        Bson filter = Filters.and(eq("_id", new ObjectId(employeeFromPrevStep.getEmployeeId())));
        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("VehicleId", new ObjectId(vehicleFromPrevStep.getVehicleId()));
        com.t1.core.mongodb.MongoConnection.updateCollection("Employees", filter, newObj);
    }

    @Then("^В коллекцию \"Devices\" в запись тестового ТМУ добавить ResourceId тестового ТС$")
    public void addVehicleResourceIdToDevice() {
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        Bson filter = Filters.and(eq("_id", new ObjectId(telematics_device.getTrackerId())));

        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("ResourceId", new ObjectId(vehicleFromPrevStep.getVehicleId()));
        newObj.put("ResourceType", "Vehicle");
        com.t1.core.mongodb.MongoConnection.updateCollection("Devices", filter, newObj);
    }

    @Then("^В коллекцию \"Devices\" в запись тестового ТМУ (\\d+) добавить ResourceId тестового ТС$")
    public void addVehicleResourceIdToDevice(int deviceIndex) {
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
        Bson filter = Filters.and(eq("_id", new ObjectId(telematics_device.getTrackerId())));

        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("ResourceId", new ObjectId(vehicleFromPrevStep.getVehicleId()));
        newObj.put("ResourceType", "Vehicle");
        com.t1.core.mongodb.MongoConnection.updateCollection("Devices", filter, newObj);
    }

    @Then("^В коллекцию \"Devices\" в запись тестового ТМУ добавить ResourceId тестового Сотрудника$")
    public void addEmployeeResourceIdToDevice() {
        Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        Bson filter = Filters.and(eq("_id", new ObjectId(telematics_device.getTrackerId())));

        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("ResourceId", new ObjectId(employeeFromPrevStep.getEmployeeId()));
        newObj.put("ResourceType", "Employee");
        com.t1.core.mongodb.MongoConnection.updateCollection("Devices", filter, newObj);
    }

    @Then("^В коллекцию \"Devices\" в запись тестового ТМУ (\\d+) добавить ResourceId тестового Сотрудника$")
    public void addEmployeeResourceIdToDevice(int deviceIndex) {
        Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
        Bson filter = Filters.and(eq("_id", new ObjectId(telematics_device.getTrackerId())));

        HashMap<String, Object> newObj = new HashMap<String, Object>();
        newObj.put("ResourceId", new ObjectId(employeeFromPrevStep.getEmployeeId()));
        newObj.put("ResourceType", "Employee");
        com.t1.core.mongodb.MongoConnection.updateCollection("Devices", filter, newObj);
    }

    @Then("^Удалить из базы тестовое ТС$")
    public void deleteInDBVehicle() {
        Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        Bson filter = Filters.and(eq("_id", new ObjectId(vehicleFromPrevStep.getVehicleId())));
        DeleteDocumentFromCollection.deleteDocument("Vehicles", filter, 1);
        TearDownExecutor.removeEntityFromTeardown(vehicleFromPrevStep.getVehicleId(), "vehicles");
    }

    @Then("^Удалить из базы тестовое ТМУ$")
    public void deleteInDBDevice() {
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        Bson filter = Filters.and(eq("_id", new ObjectId(telematics_device.getTrackerId())));
        DeleteDocumentFromCollection.deleteDocument("Devices", filter, 1);
        TearDownExecutor.removeEntityFromTeardown(telematics_device.getTrackerId(), "virtualTrackers");
    }

    @Then("^Удалить из базы все тестовые ТМУ, созданные в пререквизите$")
    public void deleteInDBAllTestDevices() {
        List<TelematicVirtualTracker> telematics_devices = (List<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers");
        for (TelematicVirtualTracker telematics_device : telematics_devices) {
            Bson filter = Filters.and(eq("_id", new ObjectId(telematics_device.getTrackerId())));
            DeleteDocumentFromCollection.deleteDocument("Devices", filter, 1);
            TearDownExecutor.removeEntityFromTeardown(telematics_device.getTrackerId(), "virtualTrackers");
        }
    }

    @Then("^Удалить из базы аналитические интервалы для тестового трека$")
    public void deleteInDBAnalyticIntervalResultsByDevice() {
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        Bson filter = Filters.and(eq("DeviceId", new ObjectId(telematics_device.getTrackerId())));
        DeleteDocumentFromCollection.deleteDocument("AnalyticIntervalResults", filter, -1);
        //	TearDownExecutor.removeEntityFromTeardown(telematics_device.trackerId, "analyticIntervalResults");
    }

    @Then("^Удалить из базы аналитические интервалы для тестовых треков, созданных в пререквизите$")
    public void deleteInDBAnalyticIntervalResultsAllDevices() {
        List<TelematicVirtualTracker> telematics_devices = (List<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers");
        for (TelematicVirtualTracker telematics_device : telematics_devices) {
            Bson filter = Filters.and(eq("DeviceId", new ObjectId(telematics_device.getTrackerId())));
            DeleteDocumentFromCollection.deleteDocument("AnalyticIntervalResults", filter, -1);
        }
    }

    @Then("^Удалить из базы точки тестового трека$")
    public void deleteInDBTrackPoints() {
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        ArrayList<String> insertedTrackPointsIds = SharedContainer.getObjectsFromSharedContainer("all", "insertedTrackPointsIds");

        Bson filter = Filters.and(eq("DeviceId", new ObjectId(telematics_device.getTrackerId())));
        DeleteDocumentFromCollection.deleteDocument("DeviceFullRecords", filter, insertedTrackPointsIds.size());
//		for(String id : insertedTrackPointsIds){
//			TearDownExecutor.removeEntityFromTeardown(id, "insertedTrackPoints");
//		}

    }

    @Then("^Удалить из базы точки тестовых треков, созданных в пререквизите$")
    public void deleteInDBPointsOfAllTracks() {
        List<TelematicVirtualTracker> telematics_devices = (List<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers");
        ArrayList<String> insertedTrackPointsIds = SharedContainer.getObjectsFromSharedContainer("all", "insertedTrackPointsIds");
        List<ObjectId> devicesIds = new ArrayList<ObjectId>();
        for (TelematicVirtualTracker telematics_device : telematics_devices) {
            devicesIds.add(new ObjectId(telematics_device.getTrackerId()));
        }
        Bson filter = Filters.and(Filters.in("DeviceId", devicesIds));
        DeleteDocumentFromCollection.deleteDocument("DeviceFullRecords", filter, insertedTrackPointsIds.size());
//		for(String id : insertedTrackPointsIds){
//			TearDownExecutor.removeEntityFromTeardown(id, "insertedTrackPoints");
//		}

    }

    @Then("^Удалить из базы записи о связи тестового ТМУ с объектами$")
    public void deleteInDBDeviceRelations() {
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");

        Bson filter = Filters.and(eq("DeviceId", new ObjectId(telematics_device.getTrackerId())));
        DeleteDocumentFromCollection.deleteDocument("DeviceHistory", filter, 1);

        if (teardownData.keySet().contains("insertedDeviceHistoryRecords")) {
            teardownData.remove("insertedDeviceHistoryRecords");
        }

    }

    @Then("^Удалить из базы записи о связи тестовых ТМУ, созданных в пререквизите, с объектами$")
    public void deleteInDBAllTestDeviceRelations() {
        List<TelematicVirtualTracker> telematics_devices = (List<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers");
        for (TelematicVirtualTracker telematics_device : telematics_devices) {
            Bson filter = Filters.and(eq("DeviceId", new ObjectId(telematics_device.getTrackerId())));
            DeleteDocumentFromCollection.deleteDocument("DeviceHistory", filter, 1);
            if (teardownData.keySet().contains("insertedDeviceHistoryRecords")) {
                teardownData.remove("insertedDeviceHistoryRecords");
            }
        }
    }

//	
}
