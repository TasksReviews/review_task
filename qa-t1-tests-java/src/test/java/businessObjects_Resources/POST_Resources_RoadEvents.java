package businessObjects_Resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Event;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Then;

public class POST_Resources_RoadEvents extends AbstractClass{
	public Event event;


	@Then("^запросить информацию о привязанном к ТМУ ресурсе с типом события \"([^\"]*)\" с периодом \"([^\"]*)\", пропустить \"([^\"]*)\", получить \"([^\"]*)\"$")
	public void requestRoadEventsByTelematicDevice(String event, String datePeriod, int skip, int limit) {
		log.debug("Start>> getRoadEventsByTelematicDevice");
		Event events = new Event();
		events.setEventType(event);
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		events.setDeviceId(telematics_device.getTrackerId());//"5926f69623a3532bc4d780e0";

		Map<String, String> resDatePeriod = getDateTimePeriodInUTC(datePeriod);
		events.setDateStart(resDatePeriod.get("dateTimeFrom").toString());
		events.setDateEnd(resDatePeriod.get("dateTimeTo").toString());
		events.setEventFilter(new ArrayList<>(Arrays.asList(event))) ;
		events.setSkip(skip);
		events.setLimit(limit);
		events.setResourceFilter(vehicleFromPrevStep.getVehicleId());
		events.setResourceName(vehicleFromPrevStep.getName());
		
		
	//	insertTrackPointsForEventsToFDR
		List<JSONObject> insertedTrackPoints =   SharedContainer.getObjectsFromSharedContainer("all", "insertedTrackPoints");
		 JSONObject receivedApiReply = events.requestRoadEvents();
		 events.checkResourceEvents(receivedApiReply,insertedTrackPoints);
		 
//		ArrayList<JSONObject> receivedEventsLst = new ArrayList(){{add(receivedEvents);}};
//			List<JSONObject> mongoDeviceFullRecords = GetDeviceFullRecordsFromMongo.getByDeviceIdAndDates(events.deviceId,events.dateStart,events.dateEnd);
			
		//	List<JSONObject> jsonMongoReply, ArrayList<JSONObject> jsonApiReply, JSONObject initJson
		 
//		 assertEquals("API:Resources.RoadEvents.deviceId ", 
//					ReplyToJSON.extractPathFromJson(receivedApiReply, "$.deviceId"),events.deviceId, "Resources.RoadEvents.deviceId не верен");	
//	
//		 
//		 List<JSONObject> receivedEvents = (List)receivedApiReply.get("events");
		
		//	new Event().checkBaseFieldsOfReceivedEvent(mongoDeviceFullRecords,new ArrayList(){{add(receivedEvents);}} , events.urlParameters);
//		JSONObject createdDepartment = department.createNewDepartment().get(0);
//		SharedContainer.setContainer("departments", (new ArrayList(){{add(department);}}));
	}
	
	@Then("^Отправить запрос получения списка дорожных событий с DateStart = \"([^\"]*)\", DateEnd = \"([^\"]*)\", Skip = \"([^\"]*)\", Limit = \"([^\"]*)\"$")
	public void requestRoadEvents_DateStart_DateEnd_Skip_Limit(String dateStart, String dateEnd, String skip, String limit) {
		List<JSONObject> insertedTrackPoints = SharedContainer.getObjectsFromSharedContainer("all", "insertedTrackPoints");
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		List trackEvents = SharedContainer.getObjectsFromSharedContainer("all", "roadEvents");
		Map<String, String> dateTimePeriod = getDateTimePeriod(dateStart, dateEnd);
		Event events = new Event();
		events.setDeviceId(telematics_device.getTrackerId());
		events.setDateStart(dateTimePeriod.get("dateTimeFrom").toString());
		events.setDateEnd(dateTimePeriod.get("dateTimeTo").toString());
	//	List eventsFilter = new ArrayList();
	//	eventsFilter.addAll(Arrays.asList("","",""));
	//	events.eventFilter =eventsFilter ;
	
		int totalEvents = trackEvents.size();
		if (skip.equals("0")) {
			events.setSkip(0);
		} else if (skip.equals("S(0;X)")) {
			events.setSkip((0 + (int) (Math.random() * totalEvents)));
		} else if (skip.equals("S = X")) {
			events.setSkip(trackEvents.size());
		} else if (skip.equals("S > X")) {
			events.setSkip(trackEvents.size() + 100);
		}
		if (limit.equals("L(0;X)")) {
			events.setLimit((0 + (int) (Math.random() * totalEvents)));
		} else if (limit.equals("L = X")) {
			events.setLimit(trackEvents.size());
		} else if (limit.equals("L > X")) {
			events.setLimit(trackEvents.size() + 100);
		}
		JSONObject receivedApiReply = events.requestRoadEvents();
	
//	 events.checkResourceEvents(receivedApiReply,insertedTrackPoints);
	 //asserts
		try {
			assertEquals("API:Resources.RoadEvents.deviceId ",
					ReplyToJSON.extractPathFromJson(receivedApiReply, "$.deviceId"), telematics_device.getTrackerId(),
					"Resources.RoadEvents.deviceId не верен");
			int expectedNoOfEvents=totalEvents-events.getSkip() ;
			
			if (expectedNoOfEvents > 0) {
				if (expectedNoOfEvents >= events.getLimit()) {

					expectedNoOfEvents = events.getLimit();
				}
			} else {
				expectedNoOfEvents = 0;
			}
			
			assertEquals("API:Resources.RoadEvents.eventsTotalCount ",
					Integer.parseInt(ReplyToJSON.extractPathFromJson(receivedApiReply, "$.eventsTotalCount")),totalEvents,
					"Resources.RoadEvents.eventsTotalCount не верен");
			JSONArray allReceivedEvents = ReplyToJSON.extractListPathFromJson(receivedApiReply, "$.events");
			assertEquals("API:Resources.RoadEvents.eventsTotal", allReceivedEvents.size(),expectedNoOfEvents,
					"Resources.RoadEvents.eventsTotalCount не верен");
		
			for(int i=0;i<allReceivedEvents.size();i++){
			assertDateIsBetween("API:Resources.RoadEvents.["+i+"]eventTime", ((JSONObject) allReceivedEvents.get(i)).get("eventTime").toString(), events.getDateStart(), events.getDateEnd(), 
					"Resources.RoadEvents.eventTime не верен");
			}
			setToAllureChechkedFields();
		} catch (Exception e) {
			log.error(ERROR,e);
			setToAllureChechkedFields();
		}
	//	getAnalyticIntervalbeRecalculted(telematics_device.trackerId, dateTimePeriod.get("dateTimeFrom"), dateTimePeriod.get("dateTimeTo"));
	}

	@ContinueNextStepsFor({AssertionError.class})
	@Then("^Отправить запрос получения списка событий за период = <\"([^\"]*)\">, с фильтром событий = <\"([^\"]*)\">, skip = (\\d+), limit = (\\d+)$")
	public void analyticIntervalResults_Step(String timePeriod, String eventsTypes, int skip, int limit){
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		analyticIntervalResults(timePeriod, eventsTypes, skip, limit, telematics_device.getTrackerId(),telematics_device.getResourceId(),
									telematics_device.getResourceType(),telematics_device.getResourceName() );
	}
	public void analyticIntervalResults(String timePeriod, String eventsTypes, int skip, int limit, String deviceId,String resourceId,String resourceType,String resourceName){
		 
		 Map<String, String> dateTimePeriod = getDateTimePeriod(timePeriod, TimeZone.getTimeZone("UTC"),"",true);
		log.debug("Start>> requestRoadEvents test");
		Event events = new Event();
				
		events.setDateStart(dateTimePeriod.get("dateTimeFrom").toString()) ;
		events.setDateEnd(dateTimePeriod.get("dateTimeTo").toString());
		if(deviceId!= null && ! deviceId.equals("")){
			events.setDeviceId(deviceId);
		}
		events.setResourceId(resourceId);
		events.setResourceType(resourceType);
		events.setResourceName(resourceName);
		List eventsFilter = new ArrayList();
		if(eventsTypes.contains("Все события")){
		eventsFilter.addAll(Arrays.asList("TrafficRulesSpeedLimit", "UserSpeedlimit", "StopEvent",
		                            	"IdleEvent", "SharpTurn", "SharpSpeedup", "SharpBraking", "PanicButton"));
		} else if (eventsTypes.equals("Превышение скорости по ПДД")) {
			eventsFilter.addAll(Arrays.asList("TrafficRulesSpeedLimit"));
		} else if (eventsTypes.equals("Превышение скорости")) {
			eventsFilter.addAll(Arrays.asList("UserSpeedlimit"));	
		} else if (eventsTypes.contains("Стоянка")) {
			eventsFilter.addAll(Arrays.asList("StopEvent"));
		} else if (eventsTypes.contains("Холостой ход")) {
			eventsFilter.addAll(Arrays.asList("IdleEvent"));
		} else if (eventsTypes.contains("Резкое торможение")) {
			eventsFilter.addAll(Arrays.asList("SharpBraking"));
		} else if (eventsTypes.contains("Резкое ускорение")) {
			eventsFilter.addAll(Arrays.asList("SharpSpeedup"));
		} else if (eventsTypes.contains("Нажатие тревожной кнопки")) {
			eventsFilter.addAll(Arrays.asList("PanicButton"));
		} else if (eventsTypes.contains("Резкие повороты")) {
			eventsFilter.addAll(Arrays.asList("SharpTurn"));
		}
		else {
			assertTrue(false, "not implemented " + eventsTypes);
		}
		events.setEventFilter(eventsFilter) ;

		events.setSkip(skip);
		events.setLimit(limit);
		JSONObject receivedRoadEvents = events.requestRoadEvents();
		SharedContainer.setContainer("receivedRoadEvents", receivedRoadEvents);
		log.debug("end of requestRoadEvents");
		setToAllureChechkedFields();
	}

	@ContinueNextStepsFor({AssertionError.class})
	@Then("^Отправить запрос получения списка событий за период = \"([^\"]*)\", с фильтром событий = \"([^\"]*)\", skip = (\\d+), limit = (\\d+)$")
	public void analyticIntervalResults2(String timePeriod, String eventsTypes, int skip, int limit){
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		analyticIntervalResults(timePeriod, eventsTypes, skip, limit, telematics_device.getTrackerId(),telematics_device.getResourceId(),telematics_device.getResourceType(),telematics_device.getResourceName() );
	}
	
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^Отправить запрос получения списка событий за период = \"([^\"]*)\", с фильтром событий = <\"([^\"]*)\">, skip = (\\d+), limit = (\\d+)$")
	public void analyticIntervalResults3(String timePeriod, String eventsTypes, int skip, int limit){
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		analyticIntervalResults(timePeriod, eventsTypes, skip, limit, telematics_device.getTrackerId(),telematics_device.getResourceId(),telematics_device.getResourceType(),telematics_device.getResourceName() );
	}
	
//	Then    Отправить запрос получения списка событий для ТС за период = <"Вчера">, фильтр событий = <"Все события">, skip = 0, limit = 30
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^Отправить запрос получения списка событий для ТС за период = <\"([^\"]*)\">, фильтр событий = <\"([^\"]*)\">, skip = (\\d+), limit = (\\d+)$")
	public void analyticIntervalResultsVehicle(String timePeriod, String eventsTypes, int skip, int limit){
	//	TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		analyticIntervalResults(timePeriod, eventsTypes, skip, limit, null,vehicleFromPrevStep.getVehicleId(),"Vehicle",vehicleFromPrevStep.getName() );
	}
	
	
//	Then    Отправить запрос получения списка событий для Сотрудника за период = <"Вчера">, фильтр событий = <"Все события">, skip = 0, limit = 30
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^Отправить запрос получения списка событий для Сотрудника за период = <\"([^\"]*)\">, фильтр событий = <\"([^\"]*)\">, skip = (\\d+), limit = (\\d+)$")
	public void analyticIntervalResultsEmployee(String timePeriod, String eventsTypes, int skip, int limit){
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		analyticIntervalResults(timePeriod, eventsTypes, skip, limit, null,employeeFromPrevStep.getEmployeeId(),"Employee",employeeFromPrevStep.getFirstname() );
	}		
			

	

}