package notifications_roadEventGenerator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.JsonFileParse;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.mongodb.InsertTrackPointToDeviceFullRecords;

import cucumber.api.java.en.Given;

public class POST_Notifications_RoadEventsGenerator extends AbstractClass{

	
	@SuppressWarnings("unchecked")
	@Given("^Взять точки из файла \"([^\"]*)\" и скопировать их в коллекцию \"([^\"]*)\" \"([^\"]*)\" раз, по \"([^\"]*)\" разу в \"([^\"]*)\" дней$")
	public void insertPointsFromFile(String fileName, String collection, String numberOfTimes,String numberOfTimes2, int numberOfDays){
		assertTrue(numberOfDays==1,"not implemented DaysCount= "+numberOfDays);
		TelematicVirtualTracker deviceFromPrevStep = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		
		
		List eventsFromFile = JsonFileParse.getTrackPointsFromFile(fileName);
		//update deviceId,DeviceCode, DeviceTime and ReceivedTime
		DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd'T'", Locale.ENGLISH);
		DateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
		formatDateTime.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
		 long inMs24hrs = 24* 60*60 * 1000L;
		 long currTimeInMs = System.currentTimeMillis();
		 long todayMidnight = currTimeInMs -(currTimeInMs % inMs24hrs);
		long yesterday = todayMidnight - inMs24hrs;
		int i=0;
		for (List event : (List<List>) eventsFromFile){
			for(JSONObject point : (List<JSONObject>)event){
				i++;
				log.debug(i+" point="+point);
				point.put("DeviceCode", deviceFromPrevStep.getTrackerCode());
				point.put("DeviceId", new ObjectId(deviceFromPrevStep.getTrackerId()));
				
				String newDeviceTime = formatDate.format(yesterday) + point.get("DeviceTime").toString().split("T")[1];
				String newReceivedTime = formatDate.format(yesterday) + point.get("ReceivedTime").toString().split("T")[1]; 
				try {
					point.put("DeviceTime",formatDateTime.parse(newDeviceTime));
					point.put("ReceivedTime",formatDateTime.parse(newReceivedTime));
					
					//since parser recognizes  ints as long:
					point.put("xgeo_Speedlimit",Integer.parseInt(point.get("xgeo_Speedlimit").toString()));
					point.put("Course",Integer.parseInt(point.get("Course").toString()));
					point.put("Mileage",Integer.parseInt(point.get("Mileage").toString()));
					point.put("Satellites",Integer.parseInt(point.get("Satellites").toString()));
					point.put("Speed",Integer.parseInt(point.get("Speed").toString()));
				} catch (ParseException e) {
					log.error(ERROR,e);
				}
				Document doc = new Document();
				doc.putAll(point);
				JSONObject insertedPoint = InsertTrackPointToDeviceFullRecords.insertPoint(collection, doc,deviceFromPrevStep.getTrackerId());
				SharedContainer.setContainer("insertedTrackPoints",insertedPoint);
			}
			SharedContainer.setContainer("roadEvents",event);
		}
		log.debug("all points were inserted");
	}
}
