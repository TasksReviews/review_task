package businessObjects_AdminPanel_Groups;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class PUT_ApiAdmin_Groups extends AbstractClass{

	

	
	@Then("^В Админке отредактировать текстовые поля группы$")
	public void editTextFields()  {
    	log.debug("Start group_update");
        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
        group.setName("JFW_groupName"+RandomStringUtils.random(rndFldLen(), getProperty("stdPattern")));
        group.updateGroupInAdminPanel();
        log.debug("End group_update");
	}
	@Then("^В Админке привязать к группе с (\\d+) сотрудника и (\\d+) ТС$")
	public void addEmplAndVehRelation(int numberOfEmpl, int numberOfVeh) {
    	log.debug("Start group_update");
        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
        ArrayList<Employee> employeeLst =  SharedContainer.getObjectsFromSharedContainer("all", "employees");
        ArrayList<Vehicle> vehicleLst =  SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
        ArrayList resourcesLst = (ArrayList) group.getUrlParameters().get("resources");
		for (Employee employee : employeeLst) {
			if (! group.getObjects().contains(employee.getEmployeeId())) {
				group.addObjectToGroup(employee.getEmployeeId(), "Employee");
				numberOfEmpl--;
			}
			if (numberOfEmpl <= 0)
				break;
		}
       for(Vehicle vehicle: vehicleLst){
    	   if (! group.getObjects().contains(vehicle.getVehicleId())) {
    	   group.addObjectToGroup(vehicle.getVehicleId(),"Vehicle");
    	   numberOfVeh--;
    	   }
    	   if (numberOfVeh <= 0)
				break;
       }
       JSONObject jsonReply = group.updateGroupInAdminPanel();
        
        HashMap  fields= new HashMap();
		fields.put("groupId", group.getGroupId());
		for(HashMap resouce : group.getResources()){
			if(resouce.get("resourceType").equals("Employee"))
			SharedContainer.updateContainer("employees", resouce.get("id").toString(), fields);
			if(resouce.get("resourceType").equals("Vehicle"))
				SharedContainer.updateContainer("vehicles", resouce.get("id").toString(), fields);
		}
		group.updateGroupInfoInResourcesInSharedContainer(group, jsonReply);
		
        log.debug("End group_update");	
	}
	
	@Then("^В Админке отвязать от группы с (\\d+) сотрудника и (\\d+) ТС$")
    public void updateGroupRemoveEmplVeh(int numberOfEmpl, int numberOfVeh)  {
    	log.debug("Start group_update");
        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
        ArrayList<HashMap<String,String>> resourcesLst = (ArrayList) group.getUrlParameters().get("resources");
		for (int i=0; i< resourcesLst.size(); i++) {
			HashMap<String,String> resource = resourcesLst.get(i);
			if (numberOfEmpl>0 && resource.get("resourceType").equals("Employee")   &&  group.getObjects().size()>0) {
				group.getResources().remove(resource);
				group.getObjects().remove(resource.get("id"));
				numberOfEmpl--;
			}
		}
		
		for (int i=0; i< resourcesLst.size(); i++) {
			HashMap<String,String> resource = resourcesLst.get(i);
			if (numberOfVeh>0 && resource.get("resourceType").equals("Vehicle")   &&  group.getObjects().size()>0) {
				group.getResources().remove(resource);
				group.getObjects().remove(resource.get("id"));
				numberOfVeh--;
			}
		}
		
		group.putUrlParameters("objects", group.getObjects());
		group.putUrlParameters("resources", group.getResources());
        group.updateGroupInAdminPanel();
        log.debug("End group_update");
    }
	
	
//	
	
	
//	@Then("^В Админке изменить группу убрать Сотрудника$")
//    public void updateGroupRemoveEmpl()  {
//    	log.debug("Start group_update");
//    	boolean emplRemoved = false;
//        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
//        ArrayList<HashMap<String,String>> resourcesLst = (ArrayList) group.urlParameters.get("resources");
//		for (int i=0; i< resourcesLst.size(); i++) {
//			HashMap<String,String> resource = resourcesLst.get(i);
//			if ( ! emplRemoved && resource.get("resourceType").equals("Employee")   &&  group.objects.size()>0) {
//				group.resources.remove(resource);
//				group.objects.remove(resource.get("id"));
//				emplRemoved = true;
//			}
//		}
//		
//		group.putUrlParameters("objects", group.objects);
//		group.putUrlParameters("resources", group.resources);
//        group.updateGroupInAdminPanel();
//        log.debug("End group_update");
//    }
//	
//	@Then("^В Админке изменить группу убрать ТС$")
//    public void updateGroupRemoveVeh()  {
//    	log.debug("Start group_update");
//    	boolean vehRemoved = false;
//        Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
//        ArrayList<HashMap<String,String>> resourcesLst = (ArrayList) group.urlParameters.get("resources");
//		
//		for (int i=0; i< resourcesLst.size(); i++) {
//			HashMap<String,String> resource = resourcesLst.get(i);
//			if (! vehRemoved && resource.get("resourceType").equals("Vehicle")   &&  group.objects.size()>0) {
//				group.resources.remove(resource);
//				group.objects.remove(resource.get("id"));
//				vehRemoved = true;
//			}
//		}
//		
//		group.putUrlParameters("objects", group.objects);
//		group.putUrlParameters("resources", group.resources);
//        group.updateGroupInAdminPanel();
//        log.debug("End group_update");
//    }
}
