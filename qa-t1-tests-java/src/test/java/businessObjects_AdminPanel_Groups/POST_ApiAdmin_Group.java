package businessObjects_AdminPanel_Groups;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Customer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Given;

public class POST_ApiAdmin_Group extends AbstractClass{

	
	@Given("^В Админке создать (\\d+) [Г,г]рупп с (\\d+) [С,с]отрудниками и (\\d+) ТС$")
	public void createGroupInAdminPanel(int noOfGroups, int noOfEmpl, int noOfVeh) {
		Customer customerFromPrevStep = Group.getCustomerFromContainer();
		for (int groupIndex = 0; groupIndex < noOfGroups; groupIndex++) {
			ArrayList<HashMap<String, String>> objects = new ArrayList<HashMap<String, String>>();
			// each group shold contain noOfEmpl and noOfVeh requested.
			if (noOfEmpl > 0) {
				ArrayList<Employee> listOfEmployees = SharedContainer.getObjectsFromSharedContainer("all", "employees");
				assertTrue(noOfGroups * noOfEmpl <= listOfEmployees.size(),
						"было создано меньше сотрудников чем необходимо для " + noOfGroups + " групп!");
				int emplForCurGr = noOfEmpl;

				for (Employee employee : listOfEmployees) {
					if (employee.getGroupId() == null && emplForCurGr > 0) {
						HashMap<String, String> object = new HashMap<String, String>();
						object.put("id", employee.getEmployeeId());
						object.put("resourceType", "Employee");
						objects.add(object);
						employee.setGroupId("000000000000000000000000");
						employee.updateInSharedContainer();
						emplForCurGr--;
					}
				}

			}
			if (noOfVeh > 0) {
				ArrayList<Vehicle> listOfVehicles = SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
				assertTrue(noOfGroups * noOfEmpl <= listOfVehicles.size(),
						"было создано меньше ТС чем необходимо для " + noOfGroups + " групп!");
				int vehForCurGr = noOfVeh;
				for (Vehicle vehicle : listOfVehicles) {
					if (vehicle.getGroupId() == null && vehForCurGr > 0) {
						HashMap<String, String> object = new HashMap<String, String>();
						object.put("id", vehicle.getVehicleId());
						object.put("resourceType", "Vehicle");
						objects.add(object);
						vehicle.setGroupId("000000000000000000000000");
						vehicle.updateInSharedContainer();
						vehForCurGr--;
					}
				}
			}
			Group group = new Group(objects);

			group.setCustomerId(customerFromPrevStep.getCustomerId());
			group.putUrlParameters("customerId", customerFromPrevStep.getCustomerId());

			JSONObject jsonReply = group.addGroupInAdminPanelResources();
			//end of group creation, updating group and related resources in container
			HashMap fields = new HashMap();
			fields.put("groupId", group.getGroupId());
			for (HashMap resouce : group.getResources()) {
				if (resouce.get("resourceType").equals("Employee"))
					SharedContainer.updateContainer("employees", resouce.get("id").toString(), fields);
				if (resouce.get("resourceType").equals("Vehicle"))
					SharedContainer.updateContainer("vehicles", resouce.get("id").toString(), fields);
			}
			group.updateGroupInfoInResourcesInSharedContainer(group, jsonReply);
			group.addToSharedContainer();

		}
	}
}
