package businessObjects_AdminPanel_Groups;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Group;

import cucumber.api.java.en.Then;

public class DELETE_ApiAdmin_Groups extends AbstractClass {
    @Then("^В Админке удалить группу$")
    public void deleteGroupById() {
        log.debug("Start group_delete");
        Group group = (Group)SharedContainer.getLastObjectFromSharedContainer("groups");
        group.deleteGroupInAdminPanel();
        log.debug("End group_delete");
    }
}
