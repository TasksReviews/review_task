package businessObjects_RelationChecks;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Vehicle;
import com.t1.core.mongodb.GetInfoFromMongo;

import cucumber.api.java.en.Then;

public class ObjectsRelations extends AbstractClass{
//	
										//коллекция Employees
	@Then("^Проверить, что запись о ТС присутствует в коллекции Employees для используемого [С,с]отрудника$")
	public void checkVehicleInfoInEmployeeCollectionStep()  {
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		checkVehicleInfoInEmployeeCollection(employee.getEmployeeId(), vehicle.getVehicleId(), true);
	}
	
	@Then("^Проверить, что запись о ТС (\\d+) присутствует в коллекции Employees для используемого [С,с]отрудника$")
	public void checkVehicleX_InfoInEmployeeCollectionStep(int indexVehicle)  {
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Vehicle vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(indexVehicle-1);
		checkVehicleInfoInEmployeeCollection(employee.getEmployeeId(), vehicle.getVehicleId(), true);
	}
	
	@Then("^Проверить, что запись о ТС присутствует в коллекции Employees для используемого [С,с]отрудника (\\d)$")
	public void checkVehicleInfoInEmployeeCollectionStep(int emplIndex)  {
		ArrayList<Employee> listOfEmployees = SharedContainer.getObjectsFromSharedContainer("all", "employees");
		Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		checkVehicleInfoInEmployeeCollection(listOfEmployees.get(emplIndex-1).getEmployeeId(), vehicle.getVehicleId(), true);
	}
	
	@Then("^Проверить, что запись о ТС отсутствует в коллекции Employees для используемого [С,с]отрудника$")
	public void checkNoVehicleInfoInEmployeeCollectionStep()  {
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		
		String deletedVehicleId = SharedContainer.getLastObjectFromSharedContainer("vehicles") ==null?
				SharedContainer.getLastObjectFromSharedContainer("deletedVehiclesIds").toString()
				: ((Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles")).getVehicleId();
				
		checkVehicleInfoInEmployeeCollection(employee.getEmployeeId(), deletedVehicleId, false);
	}
	
	@Then("^Проверить, что запись о ТС (\\d+) отсутствует в коллекции Employees для используемого [С,с]отрудника$")
	public void checkNoVehicle_X_InfoInEmployeeCollectionStep(int indexVehicle)  {
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		
		String deletedVehicleId = SharedContainer.getLastObjectFromSharedContainer("vehicles") ==null?
				SharedContainer.getObjectsFromSharedContainer("all","deletedVehiclesIds").get(indexVehicle-1).toString()//what  if is requested 2nd deleted veh but 1st wasn't deleted?
				: ((Vehicle) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(indexVehicle-1)).getVehicleId(); 
//				log.debug("empl="+employee.employeeId);
//				log.debug("veh="+deletedVehicleId);
		checkVehicleInfoInEmployeeCollection(employee.getEmployeeId(), deletedVehicleId, false);
	}
	
	@Then("^Проверить, что запись о ТС отсутствует в коллекции Employees для используемого [С,с]отрудника (\\d+)$")
	public void checkNoVehicleInfoInEmployeeCollectionStep(int indexEmpl)  {
		Employee employee = (Employee) SharedContainer.getObjectsFromSharedContainer("all","employees").get(indexEmpl-1);
		
		String deletedVehicleId = SharedContainer.getLastObjectFromSharedContainer("vehicles") ==null?
				SharedContainer.getLastObjectFromSharedContainer("deletedVehiclesIds").toString()
				: ((Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles")).getVehicleId();
				
		checkVehicleInfoInEmployeeCollection(employee.getEmployeeId(), deletedVehicleId, false);
	}
									//коллекция Vehicles
	@Then("^Проверить, что запись о [С,с]отруднике присутствует в коллекции Vehicles для используемого ТС$")
	public void checkEmployeeInfoInVehiclesCollectionStep()  {
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		checkEmployeeInfoInVehiclesCollection(employee.getEmployeeId(), vehicle.getVehicleId(), true);
	}
	
	@Then("^Проверить, что запись о [С,с]отруднике присутствует в коллекции Vehicles для используемого ТС (\\d+)$")
	public void checkEmployeeInfoInVehiclesCollection_VehicleX_Step(int indexVehicle)  {
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		Vehicle vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(indexVehicle-1);
		checkEmployeeInfoInVehiclesCollection(employee.getEmployeeId(), vehicle.getVehicleId(), true);
	}
	
	@Then("^Проверить, что запись о [С,с]отруднике (\\d) присутствует в коллекции Vehicles для используемого ТС$")
	public void checkEmployeeInfoInVehiclesCollectionStep(int emplIndex)  {
		ArrayList<Employee> listOfEmployees = SharedContainer.getObjectsFromSharedContainer("all", "employees");
		Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		checkEmployeeInfoInVehiclesCollection(listOfEmployees.get(emplIndex-1).getEmployeeId(), vehicle.getVehicleId(), true);
	}
	
	@Then("^Проверить, что запись о [С,с]отруднике отсутствует в коллекции Vehicles для используемого ТС$")
	public void checkNoEmployeeInfoInVehiclesCollectionStep()  {
//		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
//		SharedContainer.setContainer("deletedEmployeesIds", employeeId);uyiuy
		Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		
		String deletedEmployeeId = SharedContainer.getLastObjectFromSharedContainer("employees") ==null?
				SharedContainer.getLastObjectFromSharedContainer("deletedEmployeesIds").toString()
				: ((Employee) SharedContainer.getLastObjectFromSharedContainer("employees")).getEmployeeId();
		
		checkEmployeeInfoInVehiclesCollection(deletedEmployeeId, vehicle.getVehicleId(), false);
	}
	
	@Then("^Проверить, что запись о [С,с]отруднике (\\d) отсутствует в коллекции Vehicles для используемого ТС$")
	public void checkNoEmployee_X_InfoInVehiclesCollectionStep(int emplIndex)  {
		Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		
		String deletedEmployeeId = SharedContainer.getLastObjectFromSharedContainer("employees") ==null?
				SharedContainer.getObjectsFromSharedContainer("all","deletedEmployeesIds").get(emplIndex-1).toString()
				: ((Employee) SharedContainer.getObjectsFromSharedContainer("all","employees").get(emplIndex-1)).getEmployeeId();
		
		checkEmployeeInfoInVehiclesCollection(deletedEmployeeId, vehicle.getVehicleId(), false);
	}
	
	@Then("^Проверить, что запись о [С,с]отруднике отсутствует в коллекции Vehicles для используемого ТС (\\d+)$")
	public void checkNoEmployeeInfoInVehiclesCollectionStep(int indexVehicle)  {
		Vehicle vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(indexVehicle-1);
		
		String deletedEmployeeId = SharedContainer.getLastObjectFromSharedContainer("employees") ==null?
				SharedContainer.getLastObjectFromSharedContainer("deletedEmployeesIds").toString()
				: ((Employee) SharedContainer.getLastObjectFromSharedContainer("employees")).getEmployeeId();
		
		checkEmployeeInfoInVehiclesCollection(deletedEmployeeId, vehicle.getVehicleId(), false);
	}
	
	public void checkVehicleInfoInEmployeeCollection(String emplId, String vehId, boolean infoExists)  {

		    	JSONObject employee = GetInfoFromMongo.getInfoById("Employees", emplId,  "").get(0);
		    	try {
		    		if (infoExists){
		    		assertTrue("DB:Employees collection:"+emplId+",check contains vhicle_Id",employee.containsKey("VehicleId"),
		    				"объект "+emplId+" в коллекции Employees должен содержать связь с ТС");
					assertEquals("DB:Employees collection:"+emplId+",check contains vhicle_Id",ReplyToJSON.extractPathFromJson(employee,"$.VehicleId.$oid"), vehId,
							"объект "+emplId+" в коллекции Employees должен содержать корректный id ТС");
		    		} else {
		    			if(employee.containsKey("VehicleId")){
		    				log.debug("employee=="+employee);
		    				assertTrue("DB:Employees collection:"+emplId+",check does not contain vhicle_Id",
		    						! ReplyToJSON.extractPathFromJson(employee,"$.VehicleId.$oid").contains(vehId),
									"объект "+emplId+" в коллекции Employees не должен содержать id ТС "+vehId);
		    			} else {
			    		assertTrue("DB:Employees collection:"+emplId+",check does not contain vhicle_Id", ! employee.containsKey("VehicleId"),
			    				"объект "+emplId+" в коллекции Employees не должен содержать связь с ТС");
		    			}
		    		}
				} catch (Exception e) {
					setToAllureChechkedFields();
					log.error("Test failed: " + e.toString());
					assertTrue(false, e.toString());
				}
		    	
		 setToAllureChechkedFields();
	}
	
	public void checkEmployeeInfoInVehiclesCollection(String emplId, String vehId, boolean infoExists)  {

    	JSONObject vehicle = GetInfoFromMongo.getInfoById("Vehicles", vehId,  "").get(0);
    	try {
    		if (infoExists){
    		assertTrue("DB:Vehicles collection:"+vehId+",check contains employee_Id",vehicle.containsKey("EmployeeId"),
    				"объект "+vehId+" в коллекции Vehicles должен содержать связь с сотрудником");
			assertEquals("DB:Vehicles collection:"+vehId+",check contains employee_Id",ReplyToJSON.extractPathFromJson(vehicle,"$.EmployeeId.$oid"), emplId,
					"объект "+vehId+" в коллекции Vehicles должен содержать корректный id сотрудника");
    		} else {
    			if(vehicle.containsKey("EmployeeId")){
    				assertTrue("DB:Vehicles collection:"+vehId+",check does not contain employee_Id",
    						! ReplyToJSON.extractPathFromJson(vehicle,"$.EmployeeId.$oid").contains(emplId),
    						"объект "+vehId+" в коллекции Vehicles не должен содержать id сотрудника");
    			} else {
    			assertTrue("DB:Vehicles collection:"+vehId+",check does not contain employee_Id",! vehicle.containsKey("EmployeeId"),
        				"объект "+vehId+" в коллекции Vehicles не должен содержать связь с сотрудником");	
    			}
    		}
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
    	
 setToAllureChechkedFields();
}
	
								//коллекция VehicleEmployeeHistory
	@Then("^Проверить, что запись о связи Сотрудника и ТС присутствует в коллекции VehicleEmployeeHistory с OwnerType = \"([^\"]*)\", IsCurrent = (true|false), без UnboundTime$")
	public void checkRelationEmplVehInCollection_VehicleEmployeeHistory_noUnbound_Step(String ownerType,boolean isCurrent) {
		checkRelationEmplVehInCollection_VehicleEmployeeHistory_noUnbound(-1,-1, ownerType, isCurrent);
	}
	
	@Then("^Проверить, что запись о связи Сотрудника и ТС (\\d+) присутствует в коллекции VehicleEmployeeHistory с OwnerType = \"([^\"]*)\", IsCurrent = (true|false), без UnboundTime$")
	public void checkRelationEmplVehInCollection_VehicleEmployeeHistory_noUnbound_Step(int indexVehicle, String ownerType,boolean isCurrent) {
		checkRelationEmplVehInCollection_VehicleEmployeeHistory_noUnbound(indexVehicle-1,-1, ownerType, isCurrent);
	}

	@Then("^Проверить, что запись о связи Сотрудника (\\d+) и ТС присутствует в коллекции VehicleEmployeeHistory с OwnerType = \"([^\"]*)\", IsCurrent = (true|false), без UnboundTime$")
	public void checkRelationEmplVehInCollection_VehicleEmployeeHistory(int emplIndex, String ownerType,boolean isCurrent) {
		checkRelationEmplVehInCollection_VehicleEmployeeHistory_noUnbound(-1,emplIndex-1, ownerType, isCurrent);
	}
	
	public void checkRelationEmplVehInCollection_VehicleEmployeeHistory_noUnbound(int indexVehicle, int emplIndex, String ownerType,boolean isCurrent){
		String vehicleId =null;
		String employeeId =null;
	
		if(emplIndex == -1){
			Employee employees = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
			employeeId = employees.getEmployeeId();
		} else {
			Employee employee = (Employee) SharedContainer.getObjectsFromSharedContainer("all","employees").get(emplIndex);
			employeeId = employee.getEmployeeId();
		}
		if(indexVehicle == -1){
			Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			vehicleId = vehicle.getVehicleId();
		} else {
			Vehicle vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(indexVehicle);
			vehicleId = vehicle.getVehicleId();
		}
		
		 Bson filter = Filters.and (eq ("EmployeeId",new ObjectId(employeeId)), eq ("VehicleId",new ObjectId(vehicleId)), eq("IsCurrent",isCurrent) , eq("OwnerType",ownerType) );
		 List<JSONObject> reply = GetInfoFromMongo.getInfoByCustomFilters("VehicleEmployeeHistory", filter, new ArrayList<String>());

		for(JSONObject doc : reply){
			assertTrue("DB:VehicleEmployeeHistory collection, employee:"+employeeId+"\\vehicle"+vehicleId+",check has no UnboundTime", ! doc.containsKey("UnboundTime"),
		 				"Cотрудник "+employeeId+" в коллекции VehicleEmployeeHistory должен содержать актуальную связь с ТС "+vehicleId);
		}

		setToAllureChechkedFields();
	}
	
	@Then("^Проверить, что запись о связи Сотрудника и ТС (\\d+) присутствует в коллекции VehicleEmployeeHistory с OwnerType = \"([^\"]*)\", IsCurrent = (true|false), UnboundTime присутствует$")
	public void checkRelationEmplVeh_X_InCollection_VehicleEmployeeHistory(int indexVehicle, String ownerType,boolean isCurrent) {
//		String deletedEmployeeId = SharedContainer.getLastObjectFromSharedContainer("employees") ==null?
//				SharedContainer.getLastObjectFromSharedContainer("deletedEmployeesIds").toString()
//				: ((Employee) SharedContainer.getLastObjectFromSharedContainer("employees")).employeeId;
	
	String vehicleId = ((Vehicle) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(indexVehicle-1)).getVehicleId();
	 Bson filter = Filters.and (eq ("VehicleId",new ObjectId(vehicleId)), eq("IsCurrent",isCurrent), Filters.exists("UnboundTime") );
	 List<JSONObject> reply = GetInfoFromMongo.getInfoByCustomFilters("VehicleEmployeeHistory", filter, new ArrayList<String>());
	 assertTrue("DB:VehicleEmployeeHistory collection, employee:"+vehicleId+",check has UnboundTime",  reply.size()>0,
				"В коллекции VehicleEmployeeHistory должен содержаться документ о не актуальной связи ТС "+vehicleId+" с Сотрудником");
		 boolean unboundRecExists = false;
		for(JSONObject doc : reply){
			if(doc.containsKey("UnboundTime"))
				unboundRecExists  = true;
		 }

		 assertTrue("DB:VehicleEmployeeHistory collection, vehicle:"+vehicleId+",check has UnboundTime",  unboundRecExists,
	 				"ТС "+vehicleId+" в коллекции VehicleEmployeeHistory должено содержать время отвязывания Сотрудника");
		 
	 setToAllureChechkedFields();
	}
	
	@Then("^Проверить, что запись о связи Сотрудника и ТС присутствует в коллекции VehicleEmployeeHistory с OwnerType = \"([^\"]*)\", IsCurrent = (true|false), UnboundTime присутствует$")
	public void checkRelationEmplVehInCollection_VehicleEmployeeHistory(String ownerType,boolean isCurrent) {
		String deletedEmployeeId = SharedContainer.getLastObjectFromSharedContainer("employees") ==null?
				SharedContainer.getLastObjectFromSharedContainer("deletedEmployeesIds").toString()
				: ((Employee) SharedContainer.getLastObjectFromSharedContainer("employees")).getEmployeeId();
	
	 Bson filter = Filters.and (eq ("EmployeeId",new ObjectId(deletedEmployeeId)), eq("IsCurrent",isCurrent), Filters.exists("UnboundTime") );
	 List<JSONObject> reply = GetInfoFromMongo.getInfoByCustomFilters("VehicleEmployeeHistory", filter, new ArrayList<String>());
	 assertTrue("DB:VehicleEmployeeHistory collection, employee:"+deletedEmployeeId+",check has UnboundTime",  reply.size()>0,
				"В коллекции VehicleEmployeeHistory должен содержаться документ о не актуальной связи Ссотрудника "+deletedEmployeeId+" с ТС");
		 boolean unboundRecExists = false;
		for(JSONObject doc : reply){
			if(doc.containsKey("UnboundTime"))
				unboundRecExists  = true;
		 }

		 assertTrue("DB:VehicleEmployeeHistory collection, employee:"+deletedEmployeeId+",check has UnboundTime",  unboundRecExists,
	 				"сотрудник "+deletedEmployeeId+" в коллекции VehicleEmployeeHistory должен содержать время отвязывания ТС");
		 
	 setToAllureChechkedFields();
	}
	
	@Then("^Проверить, что запись о связи Сотрудника (\\d+) и ТС присутствует в коллекции VehicleEmployeeHistory с OwnerType = \"([^\"]*)\", IsCurrent = (true|false), UnboundTime присутствует$")
	public void checkRelationEmpl_X_VehInCollection_VehicleEmployeeHistory(int emplIndex, String ownerType,boolean isCurrent) {
		String deletedEmployeeId = SharedContainer.getLastObjectFromSharedContainer("employees") ==null?
				SharedContainer.getObjectsFromSharedContainer("all","deletedEmployeesIds").get(emplIndex-1).toString()
				: ((Employee) SharedContainer.getObjectsFromSharedContainer("all","employees").get(emplIndex-1)).getEmployeeId();
	
	 Bson filter = Filters.and (eq ("EmployeeId",new ObjectId(deletedEmployeeId)), eq("IsCurrent",isCurrent), Filters.exists("UnboundTime") );
	 List<JSONObject> reply = GetInfoFromMongo.getInfoByCustomFilters("VehicleEmployeeHistory", filter, new ArrayList<String>());
	 assertTrue("DB:VehicleEmployeeHistory collection, employee:"+deletedEmployeeId+",check has UnboundTime",  reply.size()>0,
				"В коллекции VehicleEmployeeHistory должен содержаться документ о не актуальной связи Ссотрудника "+deletedEmployeeId+" с ТС");
		 boolean unboundRecExists = false;
		for(JSONObject doc : reply){
			if(doc.containsKey("UnboundTime"))
				unboundRecExists  = true;
		 }

		 assertTrue("DB:VehicleEmployeeHistory collection, employee:"+deletedEmployeeId+",check has UnboundTime",  unboundRecExists,
	 				"сотрудник "+deletedEmployeeId+" в коллекции VehicleEmployeeHistory должен содержать время отвязывания ТС");
		 
	 setToAllureChechkedFields();
	}
										//коллекция Devices
	

}
