package businessObjects_RelationChecks;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Group;
import com.t1.core.api.Vehicle;
import com.t1.core.mongodb.GetInfoFromMongo;

import cucumber.api.java.en.Then;

public class GroupRelations extends AbstractClass{
				// ПРИСУТСТВУЕТ В Omgroups
	@Then("^Проверить, что запись о [С,с]отруднике присутствует в коллекции Omgroups для используемой [Г,г]руппы$")
	public void checkEmployeeInfoInOmgroupsCollectionStep()  {
		checkObjectInfoInOmgroupsCollection("employee",-1,-1);	    	
	}
	
	@Then("^Проверить, что запись о [С,с]отруднике присутствует в коллекции Omgroups для используемой группы (\\d+)$")
	public void checkEmployeeInfoInOmgroupsCollectionStep(int groupIndex)  {
		checkObjectInfoInOmgroupsCollection("employee",-1,groupIndex-1);
	}
	
	@Then("^Проверить, что запись о [С,с]отруднике (\\d) присутствует в коллекции Omgroups для используемой [Г,г]руппы$")
	public void checkEmployee_x_InfoInOmgroupsCollectionStep(int emplIndex)  {
		checkObjectInfoInOmgroupsCollection("employee",emplIndex-1,-1);
	}
	
	@Then("^Проверить, что запись о [С,с]отруднике (\\d) присутствует в коллекции Omgroups для используемой [Г,г]руппы (\\d)$")
	public void checkEmployeeInfoInOmgroupsCollectionStep(int emplIndex,int groupIndex)  {
		checkObjectInfoInOmgroupsCollection("employee",emplIndex-1,groupIndex-1);
	}

		
	public void checkObjectInfoInOmgroupsCollection(String typeOfResource, int objIndex, int groupIndex)  {
		List<String> expectedObjIds = new ArrayList<String>();
		ArrayList listOfObjects = new ArrayList();
		String typeOfResource_ru =null;
		Group group=null;
			if(groupIndex==-1){
				group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
			} else {
				group = (Group) SharedContainer.getObjectsFromSharedContainer("all","groups").get(groupIndex);
			}
			if(typeOfResource.equals("employee")){
				listOfObjects = SharedContainer.getObjectsFromSharedContainer("all", "employees");
				typeOfResource_ru = "Сотрудников";
			} else {
				listOfObjects = SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
				typeOfResource_ru = "ТС";
			}
//		ArrayList<Vehicle> listOfVehicles = SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
		assertTrue(group.getResources().size()>0,"expected group would contain at least an "+typeOfResource);
		assertTrue(group.getObjects().size()>0,"expected group would contain at least an "+typeOfResource);
		assertTrue(group.getObjects().size()==group.getResources().size(),"expected group would contain same number of objects into 'Objects' and 'Resources' section");
		
		JSONObject mongoGroupInfo = GetInfoFromMongo.getInfoById("OmGroups", group.getGroupId(),  "").get(0);
    	try {
    		assertArrayEqualsAnyOrder("check all expected "+typeOfResource+" are in objects",
				ReplyToJSON.extractListPathFromJson(mongoGroupInfo,"$.Objects..$oid").toArray(), group.getObjects().toArray(),"список "+typeOfResource_ru+" в objects не верен");
		
		if(objIndex==-1){
			for(Object obj : listOfObjects){
				String objGroupId="",objId="" ;
				if(typeOfResource.equals("employee")){
					objGroupId=	((Employee) obj).getGroupId();
					objId 	  = ((Employee) obj).getEmployeeId();
				} else {
					objGroupId=	((Vehicle) obj).getGroupId();
					objId 	  = ((Vehicle) obj).getVehicleId();
				}
				if(objGroupId.equals(group.getGroupId())){
					assertTrue("check "+typeOfResource+" "+objId+" is in resources", ReplyToJSON.extractListPathFromJson(mongoGroupInfo,"$.Resources..$oid").contains(objId),
							"список "+typeOfResource_ru+" в resources не верен");
					assertTrue("check "+typeOfResource+" "+objId+" is in objects", ReplyToJSON.extractListPathFromJson(mongoGroupInfo,"$.Objects..$oid").contains(objId),
							typeOfResource+" "+objId+" не содержится в objects");
					expectedObjIds.add(objId);
				}
			}
		} else {
			
			String objGroupId="",objId="" ;
			if(typeOfResource.equals("employee")){
				objGroupId=	((Employee)listOfObjects.get(objIndex)).getGroupId();
				objId 	  = ((Employee)listOfObjects.get(objIndex)).getEmployeeId();
			} else {
				objGroupId=	((Vehicle)listOfObjects.get(objIndex)).getGroupId();
				objId 	  = ((Vehicle)listOfObjects.get(objIndex)).getVehicleId();
			}
			
			if(objGroupId.equals(group.getGroupId())){
				assertTrue("check "+typeOfResource+" "+objId+" is in resources", ReplyToJSON.extractListPathFromJson(mongoGroupInfo,"$.Resources..$oid").contains(objId),
						"список "+typeOfResource_ru+" в resources не верен");
				assertTrue("check "+typeOfResource+" "+objId+" is in objects", ReplyToJSON.extractListPathFromJson(mongoGroupInfo,"$.Objects..$oid").contains(objId),
						typeOfResource+" "+objId+" не содержится в objects");
				expectedObjIds.add(objId);
			}
		}
		
		List<JSONObject> mongoResources = (List<JSONObject>) mongoGroupInfo.get("Resources");
		for(JSONObject resource : mongoResources){
			String id = ReplyToJSON.extractPathFromJson(resource,"_id.$oid");
			if(expectedObjIds.contains(id)){
				if(typeOfResource.equals("employee")){
					assertEquals("check employee "+id+" type", resource.get("ResourceType"), "Employee","ResourceType для Сотрудника "+id+" не верен");
				} else {
					assertEquals("check vehicle "+id+ " type", resource.get("ResourceType"), "Vehicle", "ResourceType для ТС "+id+" не верен");
				}
			}
		}
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
		    	
    	setToAllureChechkedFields();	    	
	}
		

	@Then("^Проверить, что запись о ТС присутствует в коллекции Omgroups для используемой [Г,г]руппы$")
	public void checkVehicleInfoInOmgroupsCollectionStep()  {
		checkObjectInfoInOmgroupsCollection("vehicle",-1,-1);
	}
	
	
	@Then("^Проверить, что запись о ТС присутствует в коллекции Omgroups для используемой [Г,г]руппы (\\d)$")
	public void checkVehicleInfoInOmgroupsCollection_groupIndexStep(int groupIndex)  {
		checkObjectInfoInOmgroupsCollection("vehicle",-1,groupIndex-1);
	}
	
	@Then("^Проверить, что запись о ТС (\\d+) присутствует в коллекции Omgroups для используемой группы$")
	public void checkVehicleInfoInOmgroupsCollection_vehicleIndexStep(int indexVehicle) {
		checkObjectInfoInOmgroupsCollection("vehicle",indexVehicle-1, -1);
	}

	@Then("^Проверить, что запись о ТС (\\d+) присутствует в коллекции Omgroups для используемой группы (\\d+)$")
	public void checkVehicleInfoInOmgroupsCollection_vehicleIndexStep(int indexVehicle, int groupIndex) {
		checkObjectInfoInOmgroupsCollection("vehicle",indexVehicle-1,groupIndex-1);
	}
	

	
									// ОТСУТСТВУЕТ В Omgroups
	@Then("^Проверить, что запись о Сотруднике отсутствует в коллекции Omgroups для используемой группы$")
	public void checkGroupInfoNoEmpl()  {
		checkGroupNoObjectInfo("employee", -1, -1);
	}
	
	@Then("^Проверить, что запись о Сотруднике (\\d+) отсутствует в коллекции Omgroups для используемой группы$")
	public void checkGroupInfoNoEmpl(int emplIndex)  {
		checkGroupNoObjectInfo("employee", emplIndex-1, -1);
	}
	
	@Then("^Проверить, что запись о Сотруднике (\\d+) отсутствует в коллекции Omgroups для используемой группы (\\d+)$")
	public void checkGroupInfoNoEmpl(int emplIndex, int groupIndex)  {
		checkGroupNoObjectInfo("employee", emplIndex-1, groupIndex-1);
	}

	public void checkGroupNoObjectInfo(String typeOfResource, int objIndex, int groupIndex)  {
		String objId=null;
		String typeOfResource_ru =null;
		Group group=null;
			if(groupIndex==-1){
				group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
			} else {
				group = (Group) SharedContainer.getObjectsFromSharedContainer("all","groups").get(groupIndex);
			}
			if(typeOfResource.equals("employee")){ 
				if(objIndex==-1){//get last deleted empl id, if the only one we had was deleted during the test (in employees as well). 
					//hope we will not have tests with 3rd deleted object from 5
					objId = SharedContainer.getLastObjectFromSharedContainer("employees") == null ? 
							SharedContainer.getLastObjectFromSharedContainer("deletedEmployeesIds").toString() 
							: ((Employee) SharedContainer.getLastObjectFromSharedContainer("employees")).getEmployeeId();
				} else {
					objId = SharedContainer.getObjectsFromSharedContainer("all", "employees").size() <= objIndex ?
							SharedContainer.getLastObjectFromSharedContainer("deletedEmployeesIds").toString() 
							: ((Employee) SharedContainer.getObjectsFromSharedContainer("all", "employees").get(objIndex)).getEmployeeId();
				}	
				typeOfResource_ru = "Сотрудника";
			} else {
				if(objIndex==-1){
					objId = SharedContainer.getLastObjectFromSharedContainer("vehicles") ==null?
							SharedContainer.getLastObjectFromSharedContainer("deletedVehiclesIds").toString()
							: ((Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles")).getVehicleId();
				} else {
					objId = SharedContainer.getObjectsFromSharedContainer("all", "vehicles").size() <= objIndex ?
							SharedContainer.getLastObjectFromSharedContainer("deletedVehiclesIds").toString()
							: ((Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(objIndex)).getVehicleId();
				}
				typeOfResource_ru = "ТС";
			}
		JSONObject mongoGroup = GetInfoFromMongo.getInfoById("OmGroups", group.getGroupId(),  "").get(0); 
		assertTrue("DB:group "+group.getGroupId()+" Resources",!  mongoGroup.get("Resources").toString().contains(objId),"id "+typeOfResource_ru+" не должно содержаться в group.Resources");
		assertTrue("DB:group "+group.getGroupId()+" Objects", ! mongoGroup.get("Objects").toString().contains(objId),"id "+typeOfResource_ru+" не должно содержаться в group.Objects");
		setToAllureChechkedFields();
	}
	
	@Then("^Проверить, что запись о ТС отсутствует в коллекции Omgroups для используемой [Г,г]руппы$")
	public void checkGroupInfoNoVeh()  {
		checkGroupNoObjectInfo("vehicle", -1, -1);
	}
	
	@Then("^Проверить, что запись о ТС (\\d+) отсутствует в коллекции Omgroups для используемой [Г,г]руппы$")
	public void checkGroupInfoNoVeh(int indexVehicle)  {
		checkGroupNoObjectInfo("vehicle", indexVehicle-1, -1);
		setToAllureChechkedFields();
	}
	
	@Then("^Проверить, что запись о ТС (\\d+) отсутствует в коллекции Omgroups для используемой [Г,г]руппы (\\d+)$")
	public void checkGroupInfoNoVeh(int indexVehicle, int groupIndex)  {
		checkGroupNoObjectInfo("vehicle", indexVehicle-1, groupIndex-1);
	}
								// ПРИСУТСТВУЕТ В Employees
	
	@Then("^Проверить, что запись о [Г,г]руппе (\\d+) присутствует в коллекции Employees для используемого [С,с]отрудника$")
	public void checkGroup_x_InfoInEmployeeCollection(int groupIndex)  {
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		checkGroupInfoInEmployeeOrVehicleCollection(groupIndex-1,true,"Employee",employee.getEmployeeId());
	}

	@Then("^Проверить, что запись о [Г,г]руппе присутствует в коллекции Employees для используемого [С,с]отрудника$")
	public void checkGroupInfoInEmployeeCollection()  {
		Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		checkGroupInfoInEmployeeOrVehicleCollection(true,"Employee",employee.getEmployeeId());
	}
	
	@Then("^Проверить, что запись о [Г,г]руппе присутствует в коллекции Employees для используемого [С,с]отрудника (\\d)$")
	public void checkGroupInfoInEmployeeCollection(int emplIndex)  {
		ArrayList<Employee> listOfEmployees = SharedContainer.getObjectsFromSharedContainer("all", "employees");
		checkGroupInfoInEmployeeOrVehicleCollection(true,"Employee",listOfEmployees.get(emplIndex-1).getEmployeeId());
	}

	@Then("^Проверить, что запись о [Г,г]руппе (\\d+) присутствует в коллекции Employees для используемого Сотрудника (\\d+)$")
	public void checkGroupInfoInCollectionEmployees_ForEmployee(int groupIndex, int emplIndex)  {
		Employee employee = (Employee) SharedContainer.getObjectsFromSharedContainer("all", "employees").get(emplIndex-1);
		checkGroupInfoInEmployeeOrVehicleCollection(groupIndex-1,true,"Employee",employee.getEmployeeId());
	}
	
						// ОТСУТСТВУЕТ В Employees

	@Then("^Проверить, что запись о [Г,г]руппе отсутствует в коллекции Employees для используемого [С,с]отрудника$")
	public void checkNoGroupInfoInEmployeeCollection()  {
		
		ArrayList<Employee> listOfEmployees = SharedContainer.getObjectsFromSharedContainer("all", "employees");
		for(Employee empl : listOfEmployees){
			checkGroupInfoInEmployeeOrVehicleCollection(-1,false,"Employee",empl.getEmployeeId());
			}
		}


	@Then("^Проверить, что запись о [Г,г]руппе отсутствует в коллекции Employees для используемого Сотрудника (\\d+)$")
	public void checkNoGroupInfoInEmployeeCollection(int emplIndex)  {
		ArrayList<Employee> listOfEmployees = SharedContainer.getObjectsFromSharedContainer("all", "employees");
		checkGroupInfoInEmployeeOrVehicleCollection(-1,false,"Employee",listOfEmployees.get(emplIndex-1).getEmployeeId());
		}
	
	@Then("^Проверить, что запись о [Г,г]руппе (\\d+) отсутствует в коллекции Employees для используемого Сотрудника (\\d+)$")
	public void checkNoGroupInfoInEmployeeCollection(int groupIndex, int emplIndex)  {
		Employee empl = (Employee)SharedContainer.getObjectsFromSharedContainer("all", "employees").get(emplIndex-1);
		checkGroupInfoInEmployeeOrVehicleCollection(groupIndex-1,false,"Employee",empl.getEmployeeId());
		}
	
	
	

	
										// ОТСУТСТВУЕТ В Vehicles
	
	@Then("^Проверить, что запись о [Г,г]руппе отсутствует в коллекции Vehicles для используемого ТС$")
	public void checkNoGroupInfoInVehiclesCollection()  {

	ArrayList<Vehicle> listOfVehicles = SharedContainer.getObjectsFromSharedContainer("all", "vehicles");
	for(Vehicle veh : listOfVehicles){
		checkGroupInfoInEmployeeOrVehicleCollection(-1,false,"Vehicle",veh.getVehicleId());
		}
	}

	@Then("^Проверить, что запись о [Г,г]руппе отсутствует в коллекции Vehicles для используемого ТС (\\d+)$")
	public void checkNoGroupInfoInVehiclesCollection(int indexVehicle)  {
		Vehicle veh = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(indexVehicle-1);
		checkGroupInfoInEmployeeOrVehicleCollection(-1,false,"Vehicle",veh.getVehicleId());
	}
	
	@Then("^Проверить, что запись о [Г,г]руппе (\\d+) отсутствует в коллекции Vehicles для используемого ТС (\\d+)$")
	public void checkNoGroupInfoInVehiclesCollection(int groupIndex,int indexVehicle)  {
		Vehicle veh = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(indexVehicle-1);
		checkGroupInfoInEmployeeOrVehicleCollection(groupIndex-1,false,"Vehicle",veh.getVehicleId());
	}
	
									// ПРИСУТСТВУЕТ В Vehicles
	
	@Then("^Проверить, что запись о [Г,г]руппе присутствует в коллекции Vehicles для используемого ТС$")
	public void checkGroupInfoInVehicleCollection()  {
		Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		checkGroupInfoInEmployeeOrVehicleCollection(true,"Vehicle",vehicle.getVehicleId());
	}

	@Then("^Проверить, что запись о группе присутствует в коллекции Vehicles для используемого ТС (\\d+)$")
	public void checkGroupInfoInVehicleCollectionForVehicle(int indexVehicle)  {
		
		List<Vehicle> vehicleList = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all","vehicles");
		checkGroupInfoInEmployeeOrVehicleCollection(true,"Vehicle",vehicleList.get(indexVehicle-1).getVehicleId());

	}
	
	@Then("^Проверить, что запись о группе (\\d+) присутствует в коллекции Vehicles для используемого ТС$")
	public void checkGroupInfoInVehicleCollectionForVehicle_groupIndexStep(int groupIndex)  {
		Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer( "vehicles");
		checkGroupInfoInEmployeeOrVehicleCollection(groupIndex-1,true,"Vehicle",vehicle.getVehicleId());
	}
	
	@Then("^Проверить, что запись о группе (\\d+) присутствует в коллекции Vehicles для используемого ТС (\\d+)$")
	public void checkGroupInfoInVehicleCollectionForVehicle_groupIndexStep(int groupIndex, int vehIndex)  {
		Vehicle vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(vehIndex-1);
		checkGroupInfoInEmployeeOrVehicleCollection(groupIndex-1,true,"Vehicle",vehicle.getVehicleId());
	}
	
	public void checkGroupInfoInEmployeeOrVehicleCollection(boolean infoExists,String objType,String objId){
		checkGroupInfoInEmployeeOrVehicleCollection(-1, infoExists,objType, objId);
	}
	
	@SuppressWarnings("unchecked")
	public void checkGroupInfoInEmployeeOrVehicleCollection(int groupIndex, boolean infoExists, String objType,	String objId) {
		Group group = null;
		String collection = "";

		if (objType.equals("Employee")) {
			collection = "Employees";
		} else {
			collection = "Vehicles";
		}
		JSONObject obj = GetInfoFromMongo.getInfoById(collection, objId, "").get(0);

		if (groupIndex == -1) {
			List<Group> groupsList = (List<Group>) SharedContainer.getObjectsFromSharedContainer("any","groups");
			if (groupsList!= null && groupsList.size()>0)
				group = groupsList.get(groupsList.size()-1);
		} else {
			List<Group> groupList = (List<Group>) SharedContainer.getObjectsFromSharedContainer("all", "groups");
			group = groupList.get(groupIndex);
		}

		try {
			if (infoExists) {
				assertTrue("object exists in group", group.getResources().toString().contains(objId),
						"объект " + objType + " " + objId + " не содержится в контейнере группы");

				assertEquals("DB:" + collection + " collection:" + objId + ",check contains group_Id",
						ReplyToJSON.extractPathFromJson(obj, "$.GroupId.$oid"), group.getGroupId(),
						"объект " + objId + " типа " + collection + " должен содержать корректный id группы");
			} else {
				if (obj.containsKey("GroupId")) {
					assertTrue("DB:" + collection + " collection:" + objId + ",check does not contain group_Id",
							!ReplyToJSON.extractPathFromJson(obj, "$.GroupId.$oid").contains(group.getGroupId()),
							"объект " + objId + " типа " + collection + " не должен содержать id группы");
				} else {
					assertTrue("DB:" + collection + " collection:" + objId + ",check does not contain group_Id",
							!obj.containsKey("GroupId"),
							"объект " + objId + " типа " + collection + " не должен содержать id группы");
				}
			}
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
		setToAllureChechkedFields();
	}
	        
	@Then("^Проверить, что запись в коллекции Omgroups для используемой группы содержит пустой массив \"Objects\" и пустой массив \"Resources\"$")
	public void checkGroupInfoEmpty()  {
		Group group = (Group) SharedContainer.getLastObjectFromSharedContainer("groups");
		JSONObject mongoGroup = GetInfoFromMongo.getInfoById("OmGroups", group.getGroupId(),  "").get(0); 
		assertTrue("DB:group "+group.getGroupId()+" Resources", ((JSONArray) mongoGroup.get("Resources")).isEmpty(),"ожидался пустой массив Resources");
		assertTrue("DB:group "+group.getGroupId()+" Objects",   ((JSONArray) mongoGroup.get("Objects")).isEmpty(),"ожидался пустой массив Objects");
		setToAllureChechkedFields();
	}
	
	@Then("^Проверить, что запись в коллекции Omgroups для используемой группы (\\d+) содержит пустой массив \"Objects\" и пустой массив \"Resources\"$")
	public void checkGroupInfoEmpty(int groupIndex)  {
		Group group = (Group) SharedContainer.getObjectsFromSharedContainer("all","groups").get(groupIndex-1);
		JSONObject mongoGroup = GetInfoFromMongo.getInfoById("OmGroups", group.getGroupId(),  "").get(0); 
		assertTrue("DB:group "+group.getGroupId()+" Resources", ((JSONArray) mongoGroup.get("Resources")).isEmpty(),"ожидался пустой массив Resources");
		assertTrue("DB:group "+group.getGroupId()+" Objects", ((JSONArray) mongoGroup.get("Objects")).isEmpty(),"ожидался пустой массив Objects");
		setToAllureChechkedFields();
	}

}
