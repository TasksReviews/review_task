package businessObjects_RelationChecks;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;
import com.t1.core.mongodb.GetInfoFromMongo;

import cucumber.api.java.en.Then;

public class ObjectDeviceRelations extends AbstractClass {


    //коллекция Employees
    @Then("^Проверить, что запись о ТМУ (\\d+) отсутствует в коллекции Employees для используемого Сотрудника$")
    public void checkEmployeeDeviceRelationExistsInEmployeesCollection(int deviceIndex) {
        try {
            TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
            Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
            JSONObject mongoEmployeeInfo = GetInfoFromMongo.getInfoById("Employees", employee.getEmployeeId(), "").get(0);

            if (mongoEmployeeInfo.containsKey("DeviceId")) {
                assertTrue("check employee " + employee.getEmployeeId() + " has relation to device " + telematics_device.getTrackerId(),
                        !ReplyToJSON.extractPathFromJson(mongoEmployeeInfo, "$.DeviceId.$oid").equals(telematics_device.getTrackerId()),
                        "Сотрудник " + employee.getEmployeeId() + " в коллекции Employees не должен содержать актуальную связь с ТМУ");
            } else {
                assertTrue("check employee " + employee.getEmployeeId() + " has relation to device " + telematics_device.getTrackerId(),
                        !mongoEmployeeInfo.containsKey("DeviceId"),
                        "Сотрудник " + employee.getEmployeeId() + " в коллекции Employees не должен содержать актуальную связь с ТМУ");

            }
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();
    }

    @Then("^Проверить, что запись о ТМУ (\\d+) присутствует в коллекции Employees для используемого Сотрудника$")
    public void проверить_что_запись_о_ТМУ_присутствует_в_коллекции_Employees_для_используемого_Сотрудника(int deviceIndex) {
        try {
            TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
            Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
            JSONObject mongoEmployeeInfo = GetInfoFromMongo.getInfoById("Employees", employee.getEmployeeId(), "").get(0);

            assertEquals("check employee " + employee.getEmployeeId() + " has relation to device " + telematics_device.getTrackerId(),
                    ReplyToJSON.extractPathFromJson(mongoEmployeeInfo, "$.DeviceId.$oid"), telematics_device.getTrackerId(),
                    "Сотрудник " + employee.getEmployeeId() + " в коллекции Employees должен содержать актуальную связь с ТМУ");

        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();
    }

    @Then("^Проверить, что запись о ТМУ отсутствует в коллекции Employees для используемого Сотрудника$")
    public void checkNoDeviceInfoInEmployeeCollection() {
        ArrayList<Employee> listOfEmployees = SharedContainer.getObjectsFromSharedContainer("all", "employees");
        for (Employee empl : listOfEmployees) {
            JSONObject obj = GetInfoFromMongo.getInfoById("Employees", empl.getEmployeeId(), "").get(0);
            assertTrue("check employee " + empl.getEmployeeId() + " has no device", !obj.containsKey("DeviceId"), "ожидалось что сотрудник не содержит связь с ТМУ");

        }
    }

    @Then("^Проверить, что запись о ТМУ отсутствует в коллекции Employees для используемого Сотрудника (\\d+)$")
    public void checkNoDeviceInfoInEmployeeCollection(int emplIndex) {
        Employee employee = (Employee) SharedContainer.getObjectsFromSharedContainer("all", "employees").get(emplIndex - 1);
        JSONObject obj = GetInfoFromMongo.getInfoById("Employees", employee.getEmployeeId(), "").get(0);
        assertTrue("check employee " + employee.getEmployeeId() + " has no device", !obj.containsKey("DeviceId"), "ожидалось что сотрудник не содержит связь с ТМУ");
    }

    @Then("^Проверить, что запись о ТМУ (\\d+) отсутствует в коллекции Employees для используемого Сотрудника (\\d+)$")
    public void checkNoDeviceX_InfoInEmployeeCollection(int deviceIndex, int emplIndex) {

        checkNoDeviceInfoInCollecionEmployee(emplIndex - 1, deviceIndex - 1);
    }


    public void checkNoDeviceInfoInCollecionEmployee(int emplIndex, int deviceIndex) {
        try {
            Employee employee = null;
            TelematicVirtualTracker telematics_device = null;
            if (deviceIndex != -1) {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex);
            } else {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
            }
            if (emplIndex != -1) {
                employee = (Employee) SharedContainer.getObjectsFromSharedContainer("all", "employees").get(emplIndex);
            } else {
                employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
            }

            JSONObject mongoEmployeeInfo = GetInfoFromMongo.getInfoById("Employees", employee.getEmployeeId(), "").get(0);

            if (mongoEmployeeInfo.containsKey("DeviceId")) {
                assertTrue("check employee " + employee.getEmployeeId() + " has relation to device " + telematics_device.getTrackerId(),
                        !ReplyToJSON.extractPathFromJson(mongoEmployeeInfo, "$.DeviceId.$oid").equals(telematics_device.getTrackerId()),
                        "Сотрудник " + employee.getEmployeeId() + " в коллекции Employees не должен содержать актуальную связь с ТМУ");
            } else {
                assertTrue("check employee " + employee.getEmployeeId() + " has relation to device " + telematics_device.getTrackerId(),
                        !mongoEmployeeInfo.containsKey("DeviceId"),
                        "Сотрудник " + employee.getEmployeeId() + " в коллекции Employees не должен содержать актуальную связь с ТМУ");
            }

        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();
    }

    @Then("^Проверить, что запись о ТМУ присутствует в коллекции Employees для используемого Сотрудника$")
    public void checkDeviceInfoInEmployeeCollection() {
        Employee empl = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
        checkDeviceInfoInEmployeeOrVehicleCollection(-1, "Employee", empl.getEmployeeId());
    }

    @Then("^Проверить, что запись о ТМУ присутствует в коллекции Employees для используемого Сотрудника (\\d+)$")
    public void checkDeviceInfoInEmployeeCollection(int emplIndex) {
        Employee empl = (Employee) SharedContainer.getObjectsFromSharedContainer("all", "employees").get(emplIndex - 1);
        checkDeviceInfoInEmployeeOrVehicleCollection(-1, "Employee", empl.getEmployeeId());
    }

    @Then("^Проверить, что запись о ТМУ (\\d+) присутствует в коллекции Employees для используемого Сотрудника (\\d+)$")
    public void checkDeviceInfoInEmployeeCollection(int deviceIndex, int emplIndex) {
        Employee empl = (Employee) SharedContainer.getObjectsFromSharedContainer("all", "employees").get(emplIndex - 1);
        checkDeviceInfoInEmployeeOrVehicleCollection(deviceIndex - 1, "Employee", empl.getEmployeeId());
    }
    //коллекция Devices


    @Then("^Проверить, что запись о Сотруднике присутствует в коллекции Devices для используемого ТМУ (\\d+)$")
    public void проверить_что_запись_о_Сотруднике_присутствует_в_коллекции_Devices_для_используемого_ТМУ(int deviceIndex) {
        try {
            TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
            Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
            JSONObject mongoDeviceInfo = GetInfoFromMongo.getInfoById("Devices", telematics_device.getTrackerId(), "").get(0);
            assertEquals("check employee " + employee.getEmployeeId() + " is in ResourceId of device " + telematics_device.getTrackerId(),
                    ReplyToJSON.extractPathFromJson(mongoDeviceInfo, "$.ResourceId.$oid"), employee.getEmployeeId(),
                    "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices должно содержать актуальную связь с Сотрудником");
            assertEquals("check resorce type of device " + telematics_device.getTrackerId(),
                    ReplyToJSON.extractPathFromJson(mongoDeviceInfo, "$.ResourceType"), "Employee",
                    "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices должно содержать актуальную связь с Сотрудником");
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();

    }

    @Then("^Проверить, что запись о Сотруднике присутствует в коллекции Devices для используемого ТМУ$")
    public void checkInfoAboutEmployeeExistsInDevicesStep() {
        checkInfoAboutEmployeeExistsInDevices(-1, -1);
    }


    @Then("^Проверить, что запись о Сотруднике (\\d+) присутствует в коллекции Devices для используемого ТМУ$")
    public void checkInfoAboutEmployee_X_ExistsInDevicesStep(int emplIndex) {
        checkInfoAboutEmployeeExistsInDevices(emplIndex - 1, -1);
    }

    @Then("^Проверить, что запись о Сотруднике (\\d+) присутствует в коллекции Devices для используемого ТМУ (\\d+)$")
    public void checkInfoAboutEmployee_X_ExistsInDevicesStep(int emplIndex, int deviceIndex) {
        checkInfoAboutEmployeeExistsInDevices(emplIndex - 1, deviceIndex - 1);
    }

    public void checkInfoAboutEmployeeExistsInDevices(int emplIndex, int deviceIndex) {
        try {
            Employee employee = null;
            TelematicVirtualTracker telematics_device = null;
            if (deviceIndex != -1) {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex);
            } else {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
            }
            if (emplIndex != -1) {
                employee = (Employee) SharedContainer.getObjectsFromSharedContainer("all", "employees").get(emplIndex);
            } else {
                employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
            }
            JSONObject mongoDeviceInfo = GetInfoFromMongo.getInfoById("Devices", telematics_device.getTrackerId(), "").get(0);
            assertEquals("check employee " + employee.getEmployeeId() + " is in ResourceId of device " + telematics_device.getTrackerId(),
                    ReplyToJSON.extractPathFromJson(mongoDeviceInfo, "$.ResourceId.$oid"), employee.getEmployeeId(),
                    "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices должно содержать актуальную связь с Сотрудником");
            assertEquals("check resorce type of device " + telematics_device.getTrackerId(),
                    ReplyToJSON.extractPathFromJson(mongoDeviceInfo, "$.ResourceType"), "Employee",
                    "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices должно содержать актуальную связь с Сотрудником");
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();
    }


    @Then("^Проверить, что запись о ТС присутствует в коллекции Devices для используемого ТМУ$")
    public void checkVehicleInfoInDevicesCollectionStep() {
        checkVehicleInfoInDevices(-1, -1);
    }

    @Then("^Проверить, что запись о ТС присутствует в коллекции Devices для используемого ТМУ (\\d+)$")
    public void checkVehicleInfoInDevicesForDevice_X_CollectionStep(int deviceIndex) {
        checkVehicleInfoInDevices(deviceIndex - 1, -1);
    }

    @Then("^Проверить, что запись о ТС (\\d+) присутствует в коллекции Devices для используемого ТМУ$")
    public void checkVehicleInfoInDevicesCollectionStep(int vehIndex) {
        checkVehicleInfoInDevices(-1, vehIndex - 1);
    }

    @Then("^Проверить, что запись о ТС (\\d+) присутствует в коллекции Devices для используемого ТМУ (\\d+)$")
    public void checkVehicle_X_InfoInDevicesForDevice_X_CollectionStep(int vehIndex, int deviceIndex) {
        checkVehicleInfoInDevices(deviceIndex - 1, vehIndex - 1);
    }

    public void checkVehicleInfoInDevices(int deviceIndex, int vehIndex) {
        try {
            TelematicVirtualTracker telematics_device = null;
            Vehicle vehicle = null;
            if (deviceIndex != -1) {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex);
            } else {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
            }
            if (vehIndex != -1) {
                vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(vehIndex);
            } else {
                vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
            }
            JSONObject mongoDeviceInfo = GetInfoFromMongo.getInfoById("Devices", telematics_device.getTrackerId(), "").get(0);
            assertEquals("check vehicle " + vehicle.getVehicleId() + " is in ResourceId of device " + telematics_device.getTrackerId(),
                    ReplyToJSON.extractPathFromJson(mongoDeviceInfo, "$.ResourceId.$oid"), vehicle.getVehicleId(),
                    "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices должно содержать актуальную связь с ТС");
            assertEquals("check resorce type of object " + telematics_device.getTrackerId(),
                    ReplyToJSON.extractPathFromJson(mongoDeviceInfo, "$.ResourceType"), "Vehicle",
                    "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices должно содержать актуальную связь с ТС");
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();
    }
    //////////////////////////

    @Then("^Проверить, что запись о Сотруднике отсутствует в коллекции Devices для используемого ТМУ$")
    public void checkNoEmployeeInfoInCollecionDevices_forDeviceStep() {
        checkNoEmployeeInfoInCollecionDevices_forDevice(-1, -1);
    }

    @Then("^Проверить, что запись о Сотруднике (\\d+) отсутствует в коллекции Devices для используемого ТМУ$")
    public void checkNoEmployee_X_InfoInCollecionDevices_forDeviceStep(int emplIndex) {
        checkNoEmployeeInfoInCollecionDevices_forDevice(emplIndex - 1, -1);
    }

    @Then("^Проверить, что запись о Сотруднике (\\d+) отсутствует в коллекции Devices для используемого ТМУ (\\d+)$")
    public void checkNoEmployee_X_InfoInCollecionDevices_forDeviceStep(int emplIndex, int deviceIndex) {
        checkNoEmployeeInfoInCollecionDevices_forDevice(emplIndex - 1, deviceIndex - 1);
    }

    public void checkNoEmployeeInfoInCollecionDevices_forDevice(int emplIndex, int deviceIndex) {
        try {
            // TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
            Employee employee = null;
            TelematicVirtualTracker telematics_device = null;
            if (deviceIndex != -1) {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex);
            } else {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
            }
            if (emplIndex != -1) {
                employee = (Employee) SharedContainer.getObjectsFromSharedContainer("all", "employees").get(emplIndex);
            } else {
                employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
            }

            //	Employee employee = (Employee)SharedContainer.getLastObjectFromSharedContainer("employees");
            JSONObject mongoDeviceInfo = GetInfoFromMongo.getInfoById("Devices", telematics_device.getTrackerId(), "").get(0);

            if (mongoDeviceInfo.containsKey("ResourceId")) {
                assertTrue("check no employee relation in device " + telematics_device.getTrackerId(),
                        !ReplyToJSON.extractPathFromJson(mongoDeviceInfo, "$.ResourceId.$oid").equals(employee.getEmployeeId()),
                        "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices не должно содержать актуальную связь с Сотрудником " + employee.getEmployeeId());
            } else {
                assertTrue("check no employee relation in device " + telematics_device.getTrackerId(),
                        !mongoDeviceInfo.containsKey("ResourceId"),
                        "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices не должно содержать актуальную связь с Сотрудником");
                assertTrue("check no resorce type in device " + telematics_device.getTrackerId(),
                        !mongoDeviceInfo.containsKey("ResourceType"),
                        "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices не должно содержать актуальную связь с Сотрудником");
            }

        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();
    }

    @Then("^Проверить, что запись о Сотруднике отсутствует в коллекции Devices для используемого ТМУ (\\d+)$")
    public void проверить_что_запись_о_Сотруднике_отсутствует_в_коллекции_Devices_для_используемого_ТМУ(int deviceIndex) {
        try {
            TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex - 1);
            Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
            JSONObject mongoDeviceInfo = GetInfoFromMongo.getInfoById("Devices", telematics_device.getTrackerId(), "").get(0);


            assertTrue("check no employee relation in device " + telematics_device.getTrackerId(),
                    !mongoDeviceInfo.containsKey("ResourceId"),
                    "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices не должно содержать актуальную связь с Сотрудником");
            assertTrue("check no resorce type in device " + telematics_device.getTrackerId(),
                    !mongoDeviceInfo.containsKey("ResourceType"),
                    "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices не должно содержать актуальную связь с Сотрудником");


        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();

    }

    @Then("^Проверить, что запись о ТС отсутствует в коллекции Devices для используемого ТМУ$")
    public void checkNoVehicleInfoInCollecionDevices_forDeviceStep() {
        checkNoVehicleInfoInCollecionDevices_forDevice(-1, -1);
    }

    @Then("^Проверить, что запись о ТС отсутствует в коллекции Devices для используемого ТМУ (\\d+)$")
    public void checkNoVehicleInfoInCollecionDevices_forDeviceStep(int deviceIndex) {
        checkNoVehicleInfoInCollecionDevices_forDevice(deviceIndex - 1, -1);
    }

    @Then("^Проверить, что запись о ТС (\\d+) отсутствует в коллекции Devices для используемого ТМУ$")
    public void checkNoVehicle_X_InfoInCollecionDevices_forDeviceStep(int vehIndex) {
        checkNoVehicleInfoInCollecionDevices_forDevice(-1, vehIndex - 1);
    }

    @Then("^Проверить, что запись о ТС (\\d+) отсутствует в коллекции Devices для используемого ТМУ (\\d+)$")
    public void checkNoVehicle_X_InfoInCollecionDevices_forDevice_X_Step(int vehIndex, int deviceIndex) {
        checkNoVehicleInfoInCollecionDevices_forDevice(deviceIndex - 1, vehIndex - 1);
    }

    public void checkNoVehicleInfoInCollecionDevices_forDevice(int deviceIndex, int vehIndex) {
        try {
            TelematicVirtualTracker telematics_device = null;
            Vehicle vehicle = null;
            if (deviceIndex != -1) {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex);
            } else {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
            }
            if (vehIndex != -1) {
                vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(vehIndex);
            } else {
                vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
            }
            JSONObject mongoDeviceInfo = GetInfoFromMongo.getInfoById("Devices", telematics_device.getTrackerId(), "").get(0);

            if (mongoDeviceInfo.containsKey("ResourceId")) {
                assertTrue("check no employee relation in device " + telematics_device.getTrackerId(),
                        !ReplyToJSON.extractPathFromJson(mongoDeviceInfo, "$.ResourceId.$oid").equals(vehicle.getVehicleId()),
                        "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices не должно содержать актуальную связь с ТС");
            } else {
                assertTrue("check no employee relation in device " + telematics_device.getTrackerId(),
                        !mongoDeviceInfo.containsKey("ResourceId"),
                        "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices не должно содержать актуальную связь с ТС");
                assertTrue("check no resorce type in device " + telematics_device.getTrackerId(),
                        !mongoDeviceInfo.containsKey("ResourceType"),
                        "ТМУ " + telematics_device.getTrackerId() + " в коллекции Devices не должно содержать актуальную связь с ТС");
            }

        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();
    }


    //коллекция Vehicles

    @Then("^Проверить, что запись о ТМУ присутствует в коллекции Vehicles для используемого ТС$")
    public void checkDeviceInfoInVehiclesCollection() {
        Vehicle veh = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        checkDeviceInfoInEmployeeOrVehicleCollection(-1, "Vehicle", veh.getVehicleId());
    }

    @Then("^Проверить, что запись о ТМУ присутствует в коллекции Vehicles для используемого ТС (\\d+)$")
    public void checkDeviceInfoInVehiclesCollection(int vehIndex) {
        Vehicle veh = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(vehIndex - 1);
        checkDeviceInfoInEmployeeOrVehicleCollection(-1, "Vehicle", veh.getVehicleId());
    }

    @Then("^Проверить, что запись о ТМУ (\\d+) присутствует в коллекции Vehicle для используемого ТС$")
    public void checkDevice_X_InfoInVehicleCollection(int trackerIndex) {
        Vehicle veh = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        checkDeviceInfoInEmployeeOrVehicleCollection(trackerIndex - 1, "Vehicle", veh.getVehicleId());
    }

    @Then("^Проверить, что запись о ТМУ (\\d+) присутствует в коллекции Vehicles для используемого ТС$")
    public void checkDevice_X_InfoInVehiclesCollection(int trackerIndex) {
        Vehicle veh = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
        checkDeviceInfoInEmployeeOrVehicleCollection(trackerIndex - 1, "Vehicle", veh.getVehicleId());
    }

    @Then("^Проверить, что запись о ТМУ (\\d+) присутствует в коллекции Vehicles для используемого ТС (\\d+)$")
    public void checkDeviceInfoInVehiclesCollection(int trackerIndex, int vehIndex) {
        Vehicle veh = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(vehIndex - 1);
        checkDeviceInfoInEmployeeOrVehicleCollection(trackerIndex - 1, "Vehicle", veh.getVehicleId());
    }

    public void checkDeviceInfoInEmployeeOrVehicleCollection(int trackerIndex, String resourceType, String obectId) {
        TelematicVirtualTracker device = null;
        if (trackerIndex != -1) {
            device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(trackerIndex);
        } else {
            device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        }
        String collection = "";
        if (resourceType.equals("Employee")) {
            collection = "Employees";
        } else {
            collection = "Vehicles";
        }
        JSONObject obj = GetInfoFromMongo.getInfoById(collection, obectId, "").get(0);
        try {
            assertEquals("DB:" + collection + " collection:" + obectId + ",check contains device_Id", ReplyToJSON.extractPathFromJson(obj, "$.DeviceId.$oid"), device.getTrackerId(),
                    "объект " + obectId + " типа " + collection + " должен содержать корректный id ТМУ");
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }

        setToAllureChechkedFields();
    }


    @Then("^Проверить, что запись о ТМУ отсутствует в коллекции Vehicles для используемого ТС$")
    public void checkNoDeviceInfoInVehiclesCollection() {
        checkNoDeviceInfoInCollecionVehicles_forVehicle(-1, -1);
    }

    @Then("^Проверить, что запись о ТМУ (\\d+) отсутствует в коллекции Vehicles для используемого ТС$")
    public void checkNoDevice_X_InfoInCollecionVehicles_forDeviceStep(int deviceIndex) {
        checkNoDeviceInfoInCollecionVehicles_forVehicle(deviceIndex - 1, -1);
    }

    @Then("^Проверить, что запись о ТМУ отсутствует в коллекции Vehicles для используемого ТС (\\d+)$")
    public void checkNoDeviceInfoInCollecionVehicles_forDeviceStep(int vehIndex) {
        checkNoDeviceInfoInCollecionVehicles_forVehicle(-1, vehIndex - 1);
    }

    @Then("^Проверить, что запись о ТМУ (\\d+) отсутствует в коллекции Vehicles для используемого ТС (\\d+)$")
    public void checkNoDevice_X_InfoInCollecionVehicles_forDeviceStep(int deviceIndex, int vehIndex) {
        checkNoDeviceInfoInCollecionVehicles_forVehicle(deviceIndex - 1, vehIndex - 1);
    }

    public void checkNoDeviceInfoInCollecionVehicles_forVehicle(int deviceIndex, int vehIndex) {
        try {
            TelematicVirtualTracker telematics_device = null;
            Vehicle vehicle = null;
            if (deviceIndex != -1) {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex);
            } else {
                telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
            }

            if (vehIndex != -1) {
                vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(vehIndex);
            } else {
                vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
            }
            JSONObject mongoDeviceInfo = GetInfoFromMongo.getInfoById("Vehicles", vehicle.getVehicleId(), "").get(0);

            if (mongoDeviceInfo.containsKey("DeviceId")) {
                assertTrue("check no device relation for vehicle " + vehicle.getVehicleId(),
                        !ReplyToJSON.extractPathFromJson(mongoDeviceInfo, "$.DeviceId.$oid").equals(telematics_device.getTrackerId()),
                        "ТС " + vehicle.getVehicleId() + " в коллекции Devices не должно содержать актуальную связь с ТМУ");
            } else {
                assertTrue("check no device relation for vehicle " + vehicle.getVehicleId(),
                        !mongoDeviceInfo.containsKey("DeviceId"),
                        "ТС " + vehicle.getVehicleId() + " в коллекции Devices не должно содержать актуальную связь с ТМУ");
            }


        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error("Test failed: " + e.toString());
            assertTrue(false, e.toString());
        }
        setToAllureChechkedFields();
    }

    //коллекция DeviceHistory

    @Then("^Проверить, что запись о связи Сотрудника и ТМУ (\\d+) присутствует в коллекции DeviceHistory с UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void проверить_что_запись_о_связи_Сотрудника_и_ТМУ_присутствует_в_коллекции_DeviceHistory_с_UnboundFromResourceTime_с_RelatedResourceType(int deviceIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(-1, deviceIndex - 1, relResType, true);
    }

    @Then("^Проверить, что запись о связи Сотрудника и ТМУ (\\d+) присутствует в коллекции DeviceHistory без UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void проверить_что_запись_о_связи_Сотрудника_и_ТМУ_присутствует_в_коллекции_DeviceHistory_без_UnboundFromResourceTime_с_RelatedResourceType(int deviceIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(-1, deviceIndex - 1, relResType, false);
    }


    // коллекция DeviceHistory
    @Then("^Проверить, что запись о связи Сотрудника и ТМУ присутствует в коллекции DeviceHistory без UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkEmployeeDeviceRelationExistsInDeviceHistory_without_UnboundFromResourceTime_with_RelatedResourceType_Step(String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(-1, -1, relResType, false);
    }

    @Then("^Проверить, что запись о связи Сотрудника (\\d+) и ТМУ присутствует в коллекции DeviceHistory без UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkEmployee_X_DeviceRelationExistsInDeviceHistory_without_UnboundFromResourceTime_with_RelatedResourceType_Step(int emplIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(emplIndex - 1, -1, relResType, false);
    }

    @Then("^Проверить, что запись о связи Сотрудника (\\d+) и ТМУ (\\d+) присутствует в коллекции DeviceHistory без UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkEmployee_X_DeviceRelationExistsInDeviceHistory_without_UnboundFromResourceTime_with_RelatedResourceType_Step(int emplIndex, int deviceIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(emplIndex - 1, deviceIndex - 1, relResType, false);
    }

    @Then("^Проверить, что запись о связи Сотрудника и ТМУ присутствует в коллекции DeviceHistory с UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkEmployeeDeviceRelationExistsInDeviceHistory_with_UnboundFromResourceTime_with_RelatedResourceType_Step(String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(-1, -1, relResType, true);
    }

    @Then("^Проверить, что запись о связи Сотрудника (\\d+) и ТМУ присутствует в коллекции DeviceHistory с UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkEmployee_X_DeviceRelationExistsInDeviceHistory_with_UnboundFromResourceTime_with_RelatedResourceType_Step(int emplIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(emplIndex - 1, -1, relResType, true);
    }

    @Then("^Проверить, что запись о связи Сотрудника (\\d+) и ТМУ (\\d+) присутствует в коллекции DeviceHistory с UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkEmployee_X_DeviceRelationExistsInDeviceHistory_with_UnboundFromResourceTime_with_RelatedResourceType_Step(int emplIndex, int deviceIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(emplIndex - 1, deviceIndex - 1, relResType, true);
    }

    public void checkObjectToDeviceRelationExistsInDeviceHistory(int objectIndex, int deviceIndex, String relResType, boolean unboundTime) {
        TelematicVirtualTracker telematics_device;
        if (deviceIndex == -1) {
            telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        } else {
            telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all", "virtualTrackers").get(deviceIndex);
        }
        String resourceId = null;
        if (relResType.equals("Vehicle")) {
            if (objectIndex != -1) {
                Vehicle vehicle = (Vehicle) SharedContainer.getObjectsFromSharedContainer("all", "vehicles").get(objectIndex);
                if (vehicle != null)
                    resourceId = vehicle.getVehicleId();
            } else {
                Vehicle vehicle = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
                if (vehicle != null)
                    resourceId = vehicle.getVehicleId();
            }
            if (resourceId == null) {
                resourceId = SharedContainer.getLastObjectFromSharedContainer("deletedVehiclesIds").toString();
            }
        } else {
            if (objectIndex != -1) {
                Employee employee = (Employee) SharedContainer.getObjectsFromSharedContainer("all", "employees").get(objectIndex);
                resourceId = employee.getEmployeeId();
            } else {
                Employee employee = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
                if (employee != null)
                    resourceId = employee.getEmployeeId();
            }
            if (resourceId == null) {
                resourceId = SharedContainer.getLastObjectFromSharedContainer("deletedEmployeesIds").toString();
            }
        }
        Bson filter = Filters.and(eq("DeviceId", new ObjectId(telematics_device.getTrackerId())),
                eq("RelatedResourceType", relResType), eq("ResourceId", new ObjectId(resourceId)),
                Filters.exists("UnboundFromResourceTime", unboundTime));
        try {
            JSONObject reply = (JSONObject) GetInfoFromMongo.getInfoByCustomFilters("DeviceHistory", filter, new ArrayList<String>()).get(0);
            if (unboundTime) {
                assertTrue("no actual relation between device " + telematics_device.getTrackerId() + " and " + relResType + " " + resourceId,
                        !reply.isEmpty(), "коллекция DeviceHistory не должна содержать актуальную связь между ТМУ "
                                + telematics_device.getTrackerId() + " и " + relResType + " " + resourceId);
            } else {
                assertTrue(
                        "exists actual relation between device " + telematics_device.getTrackerId() + " and " + relResType + " " + resourceId,
                        !reply.isEmpty(), "коллекция DeviceHistory должна содержать актуальную связь между ТМУ "
                                + telematics_device.getTrackerId() + " и " + relResType + " " + resourceId);
            }
        } catch (IndexOutOfBoundsException e) {
            setToAllureChechkedFields();
            log.error(e.getMessage());
        }
        setToAllureChechkedFields();

    }

    @Then("^Проверить, что запись о связи ТС и ТМУ присутствует в коллекции DeviceHistory без UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkVehicleDeviceRelationExistsInDeviceHistory_without_UnboundFromResourceTime_with_RelatedResourceType(String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(-1, -1, relResType, false);
    }

    @Then("^Проверить, что запись о связи ТС (\\d+) и ТМУ присутствует в коллекции DeviceHistory без UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkVehicle_X_DeviceRelationExistsInDeviceHistory_without_UnboundFromResourceTime_with_RelatedResourceType(int vehIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(vehIndex - 1, -1, relResType, false);
    }

    @Then("^Проверить, что запись о связи ТС и ТМУ (\\d+) присутствует в коллекции DeviceHistory без UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkVehicleDevice_X_RelationExistsInDeviceHistory_without_UnboundFromResourceTime_with_RelatedResourceType(int deviceIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(-1, deviceIndex - 1, relResType, false);
    }

    @Then("^Проверить, что запись о связи ТС (\\d+) и ТМУ (\\d+) присутствует в коллекции DeviceHistory без UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkVehicleDevice_X_RelationExistsInDeviceHistory_without_UnboundFromResourceTime_with_RelatedResourceType(int vehIndex, int deviceIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(vehIndex - 1, deviceIndex - 1, relResType, false);
    }

    @Then("^Проверить, что запись о связи ТС и ТМУ присутствует в коллекции DeviceHistory с UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkVehicleDeviceRelationExistsInDeviceHistory_with_UnboundFromResourceTime_with_RelatedResourceType(String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(-1, -1, relResType, true);
    }

    @Then("^Проверить, что запись о связи ТС и ТМУ (\\d+) присутствует в коллекции DeviceHistory с UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkVehicleDevice_X_RelationExistsInDeviceHistory_with_UnboundFromResourceTime_with_RelatedResourceType(int deviceIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(-1, deviceIndex - 1, relResType, true);
    }

    @Then("^Проверить, что запись о связи ТС (\\d+) и ТМУ присутствует в коллекции DeviceHistory с UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkVehicle_X_DeviceRelationExistsInDeviceHistory_with_UnboundFromResourceTime_with_RelatedResourceType(int vehIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(vehIndex - 1, -1, relResType, true);
    }

    @Then("^Проверить, что запись о связи ТС (\\d+) и ТМУ (\\d+) присутствует в коллекции DeviceHistory с UnboundFromResourceTime, с RelatedResourceType = \"([^\"]*)\"$")
    public void checkVehicle_X_DeviceRelationExistsInDeviceHistory_with_UnboundFromResourceTime_with_RelatedResourceType(int vehIndex, int deviceIndex, String relResType) {
        checkObjectToDeviceRelationExistsInDeviceHistory(vehIndex - 1, deviceIndex - 1, relResType, true);
    }
}
