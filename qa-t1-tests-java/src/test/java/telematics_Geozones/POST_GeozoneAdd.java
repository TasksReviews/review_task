package telematics_Geozones;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Geozone;
import com.t1.core.api.TelematicsTrack;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_GeozoneAdd extends AbstractClass{
	public TelematicsTrack device;
	ArrayList<JSONObject> pointsArr;
	@Then("^создать новую геозону типа Linear с 2 точками$")
	public void addNewGeozoneLinear2pointsMinFields(){
		final Geozone geozone = new Geozone();
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.66"); put("latitude", "55.71"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.58"); put("latitude", "55.73"); }});
		
		geozone.addGeozone("Linear", pointsArr); 
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Then("^создать новую геозону типа Linear с 3 точками$")
	public void addNewGeozoneLinear3pointsMinFields(){
		final Geozone geozone = new Geozone();
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.66"); put("latitude", "55.71"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.58"); put("latitude", "55.73"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.61"); put("latitude", "55.64"); }});
		
		geozone.addGeozone("Linear", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Then("^создать новую геозону типа Linear с 3 точками и полным списком полей$")
	public void addNewGeozoneLinear3pointsAllFields(){
		final Geozone geozone = new Geozone("Linear", true,"stdPattern");
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.66"); put("latitude", "55.71"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.58"); put("latitude", "55.73"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.61"); put("latitude", "55.64"); }});
		
		geozone.addGeozone("Linear", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Then("^создать новую геозону типа Round$")
	public void addNewGeozoneRoundMinFields(){
		final Geozone geozone = new Geozone("Round",false,"stdPattern");
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.66"); put("latitude", "55.71"); }});
		
		geozone.addGeozone("Round", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Then("^создать новую геозону типа Round и полным списком полей$")
	public void addNewGeozoneRoundAllFields(){
		final Geozone geozone = new Geozone("Round",true,"stdPattern");
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.66"); put("latitude", "55.71"); }});
		
		geozone.addGeozone("Round", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Given("^создать новую геозону типа Round с центром \"([^\"]*)\"; \"([^\"]*)\", радиусом \"([^\"]*)\" и буфером \"([^\"]*)\"$")
	public void addNewGeozoneRoundWithParams(final String lat, final String lon, String radius, String bufferThickness)  {
		final Geozone geozone = new Geozone(true,"stdPattern", "Round", radius, bufferThickness);
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", lon); put("latitude", lat); }});
		
		geozone.addGeozone("Round", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	
	@Then("^создать новую геозону типа Polygon с 4 точками$")
	public void addNewGeozonePolygon4pointsMinFields(){
		final Geozone geozone = new Geozone();
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.69"); put("latitude", "55.71"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.58"); put("latitude", "55.73"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.61"); put("latitude", "55.64"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.69"); put("latitude", "55.71"); }});
		
		geozone.addGeozone("Polygon", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Then("^создать новую геозону типа Polygon с 5 точками$")
	public void addNewGeozonePolygon5pointsMinFields(){
		final Geozone geozone = new Geozone();
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.66"); put("latitude", "55.71"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.58"); put("latitude", "55.73"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.61"); put("latitude", "55.64"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.72"); put("latitude", "55.74"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.66"); put("latitude", "55.71"); }});
		
		geozone.addGeozone("Polygon", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Then("^создать новую геозону типа Polygon с 5 точками и полным списком полей$")
	public void addNewGeozonePolygon5pointsAllFields(){
		final Geozone geozone = new Geozone("Polygon",true,"stdPattern");
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.66"); put("latitude", "55.71"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.58"); put("latitude", "55.73"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.61"); put("latitude", "55.64"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.72"); put("latitude", "55.74"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.66"); put("latitude", "55.71"); }});
		
		geozone.addGeozone("Polygon", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Then("^создать новую геозону типа Polygon с 4 точками и типом доступа SharedToCompany$")
	public void addNewGeozonePolygon4pointsMinFieldsAccessSharedToCompany(){
		final Geozone geozone = new Geozone();
		geozone.setAccess("SharedToCompany");
		geozone.putUrlParameters("access", geozone.getAccess());
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.69"); put("latitude", "55.71"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.58"); put("latitude", "55.73"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.61"); put("latitude", "55.64"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.69"); put("latitude", "55.71"); }});
		
		geozone.addGeozone("Polygon", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Then("^создать новую геозону типа Polygon с 4 точками и типом доступа PrivateToUser$")
	public void addNewGeozonePolygon4pointsMinFieldsAccessPrivateToUser(){
		final Geozone geozone = new Geozone();
		geozone.setAccess("PrivateToUser");
		geozone.putUrlParameters("access", geozone.getAccess());
		
		 pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.69"); put("latitude", "55.71"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.58"); put("latitude", "55.73"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.61"); put("latitude", "55.64"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.69"); put("latitude", "55.71"); }});
		
		geozone.addGeozone("Polygon", pointsArr);
		SharedContainer.setContainer("geozones", geozone);
	}
	
	
	@Then("^создать новую геозону типа Polygon для правил связанных с геозонами: \"([^\"]*)\"$")
	public void addNewGeozonePolygonForRule(String rule){
		if(rule.contains("Geozone")){
		final Geozone geozone = new Geozone();
		geozone.setBufferThickness("1");
		geozone.putUrlParameters("bufferThickness",  geozone.getBufferThickness());

		 ArrayList pointsArr = new ArrayList();
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.69"); put("latitude", "55.71"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.58"); put("latitude", "55.73"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.61"); put("latitude", "55.64"); }});
		 pointsArr.add(new JSONObject() {{  put("longitude", "37.69"); put("latitude", "55.71"); }});
		 geozone.addGeozone("Polygon", pointsArr);
		
		SharedContainer.setContainer("geozonesForNotificationRules", geozone);
		}
	}
	
	@Then("^создать новую геозону$")
	public void addNewGeozone(){
		final Geozone geozone = new Geozone();
		geozone.addGeozone();
		SharedContainer.setContainer("geozones", geozone);
	}
	
	@Then("^создать (\\d) новых геозоны$")
	public void addNewGeozones(int numberOfGeozones){
		for(int i=0;i<numberOfGeozones; i++){
		addNewGeozone();
		}
	}
}
