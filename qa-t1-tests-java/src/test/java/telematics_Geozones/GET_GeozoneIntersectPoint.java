package telematics_Geozones;

import com.t1.core.SharedContainer;
import com.t1.core.api.Geozone;

import cucumber.api.java.en.Then;

public class GET_GeozoneIntersectPoint {

	//@Step("^точка входит в геозону$")
	@Then("^проверить входит ли точка \"([^\"]*)\", \"([^\"]*)\" созданную геозону$")
	public boolean checkIfPointrelatesToGeozone(String pointLat, String pointLon){
		Geozone geozone = new Geozone();
		
		geozone =  (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozones");

		
		geozone.checkIfPoinRelatesToGeozone(geozone.getGeozoneId(),pointLon,pointLat);
		
		
		return false;
	}
	
}
