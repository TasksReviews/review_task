package telematics_Geozones;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Geozone;
import com.t1.core.api.TelematicsTrack;

import cucumber.api.java.en.Then;

public class POST_GeozoneDelete extends AbstractClass{
	public TelematicsTrack device;
	@Then("^удалить созданную геозону$")
	public void deleteGeozone(){
		Geozone geozone = new Geozone();
		
		geozone =  (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozones");

		geozone.deleteGeozone(geozone.getGeozoneId());
		
		

	}
}
