package telematics_Geozones;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicsTrack;
import com.t1.core.mongodb.GetTelematicsGeozonesInfoFromMongo;

import cucumber.api.java.en.Then;

public class GET_TelematicsGeozones extends AbstractClass{
	public TelematicsTrack device;
	@Then("^получить случайную геозону из БД$")
	public void getRandomTelematicsDevice(){
		 final JSONObject randomGeozone = (JSONObject) GetTelematicsGeozonesInfoFromMongo.getRandomTelematicsGeozoneFromMongo().get(0);
//		log.debug("Start>> employee_create");
//		employee = new Employee();
//		JSONObject createdemployee = employee.createNewEmployee().get(0);
		SharedContainer.setContainer("json_telematics_geozone", (new ArrayList(){{add(randomGeozone);}}));
//		//employee.deleteEmployee(employee.employeeId);
	}
}
