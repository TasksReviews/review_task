package telematics_Geozones;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Geozone;
import com.t1.core.api.TelematicsTrack;

import cucumber.api.java.en.Then;

public class GET_Geozone_id extends AbstractClass{
	public TelematicsTrack device;
	@Then("^получить информацию о геозоне по id$")
	public void getRandomTelematicsDevice(){
		Geozone geozone =  (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozones");
		geozone.requestGeozoneInfoById(geozone.getGeozoneId());

	}
}
