package telematics_Geozones;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Geozone;
import com.t1.core.api.TelematicsTrack;

import cucumber.api.java.en.Then;

public class POST_GeozoneUpdate extends AbstractClass{
	public TelematicsTrack device;
	ArrayList<JSONObject> pointsArr;
	@Then("^изменить созданную геозону$")
	public void addNewGeozoneLinear2pointsMinFields(){
		Geozone geozoneFromPrevStep = (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozones");
		final Geozone geozone = new Geozone();
		
		geozone.setGeozoneId(geozoneFromPrevStep.getGeozoneId());
		
		geozone.updateGeozone(geozone.getGeozoneId(),geozone.getType(),new ArrayList()); 
		SharedContainer.setContainer("geozones", (new ArrayList(){{add(geozone);}}));
	}
	

}
