package telematics_Geozones;

import com.t1.core.api.Geozone;

import cucumber.api.java.en.Then;

public class GET_GeozoneFindByPoint {

	//@Step("^точка входит в геозону$")
	@Then("^запрос списка геозон включающих в себя точку с координатами lat\"([^\"]*)\", lon\"([^\"]*)\"$")
	public void getListOfGeozonesByCoordinates(String lat, String lon){
	//	Geozone geozone = (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozones");

		new Geozone().getGeozonesRelatesToPoint(lat,lon);
		
	}
	
}
