package telematics_Geozones;

import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Geozone;
import com.t1.core.api.TelematicsTrack;

import cucumber.api.java.en.Then;

public class GET_GeozoneGetAll_accessType extends AbstractClass{
	public TelematicsTrack device;
	@Then("^получить информацию о всех геозонах с доступом SharedToCompany$")
	public void getGeozonesSharedToCompany(){
		Geozone geozone =  new Geozone();
		final JSONArray apiReplyGeozonesSharedToCompany = geozone.requestAllGeozones("SharedToCompany","");
		SharedContainer.setContainer("apiReplyGeozoneGetAll", (new ArrayList(){{add(apiReplyGeozonesSharedToCompany);}}));
	}
	
	@Then("^получить информацию о всех геозонах с доступом PrivateToUser$")
	public void getGeozonesPrivateToUser(){
		Geozone geozone =  new Geozone();
		final JSONArray apiReplyGeozonesSharedToCompany = geozone.requestAllGeozones("PrivateToUser","");
		SharedContainer.setContainer("apiReplyGeozoneGetAll", (new ArrayList(){{add(apiReplyGeozonesSharedToCompany);}}));
	}
	
	@Then("^убедиться что, созданная в предыдущем шаге, геозона с типом доступа SharedToCompany получена$")
	public void confirmJustCreatedGeozoneSharedToCompanyReceived(){
		confirmJustCreatedGeozoneReceived("SharedToCompany");
	}
	
	@Then("^убедиться что, созданная в предыдущем шаге, геозона с типом доступа PrivateToUser получена$")
	public void confirmJustCreatedPrivateToUserReceived(){

		confirmJustCreatedGeozoneReceived("PrivateToUser");
	}
	
	@Then("^получить информацию о геозоне из предыдущего шага с по типу доступа и имени$")
	public void getGeozonesByAccessAndName(){
		//Geozone geozone =  new Geozone();
		ArrayList<Geozone>  geozonesList = SharedContainer.getContainers().get("geozones");
		for (Geozone geozone : geozonesList){
			final JSONArray apiReplyGeozonesByAccessAndName = geozone.requestAllGeozones(geozone.getAccess(),geozone.getName());
			SharedContainer.setContainer("apiReplyGeozoneGetAll", (new ArrayList(){{add(apiReplyGeozonesByAccessAndName);}}));
		}

	}
	
	
	public void confirmJustCreatedGeozoneReceived(String accessType){
		ArrayList<Geozone> geozones =  (ArrayList<Geozone>) SharedContainer.getContainers().get("geozones");
		JSONArray apiReplyGeozones = (JSONArray) SharedContainer.getContainers().get("apiReplyGeozoneGetAll").get(0);
		ArrayList<String> searchingIds = new ArrayList<String>() ;
		ArrayList<String> receivedIds = new ArrayList<String>() ;
		for(Geozone geozone:geozones){
			if(geozone.getAccess().equals(accessType)){
				searchingIds.add(geozone.getGeozoneId());
			}
		}
				for(int i=0;i<apiReplyGeozones.size(); i++){
					String apiReplyGeozoneId =((JSONObject) apiReplyGeozones.get(i)).get("id").toString();
					if (searchingIds.contains(apiReplyGeozoneId)){
						receivedIds.add(apiReplyGeozoneId);
					}
				}
			
		assertEquals("API:geozone."+accessType+" returned all ids", receivedIds, searchingIds,
						"API:geozone созданная геозона с access="+accessType+" не была получена в ответе");
		setToAllureChechkedFields();
	}
}
