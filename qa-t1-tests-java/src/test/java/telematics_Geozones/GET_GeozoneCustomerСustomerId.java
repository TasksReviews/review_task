package telematics_Geozones;

import java.util.ArrayList;

import org.json.simple.JSONArray;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Geozone;
import com.t1.core.api.TelematicsTrack;

import cucumber.api.java.en.Then;

public class GET_GeozoneCustomerСustomerId extends AbstractClass{
	public TelematicsTrack device;
	@Then("^получить информацию о всех геозонах для используемого клиента$")
	public void getGeozonesSharedToCompany(){
		Geozone geozone =  new Geozone();
		final JSONArray apiReplyGeozonesSharedToCompany = geozone.requestAllGeozonesByCustomer( );
		SharedContainer.setContainer("apiReplyGeozoneGetAll", (new ArrayList(){{add(apiReplyGeozonesSharedToCompany);}}));
	}
	
}
