package reports;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Vehicle;

import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Then;

public class ReportsStepsWithChecks extends AbstractClass {
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) Ресурсов$")
	public void checkNumOfResourcesReceived(int totalResourcesExpected) {
		try {
		JSONObject reportResult = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportResult");
		JSONObject repliedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
		List<JSONObject> resources=null;
		if(reportResult!=null){
			resources = ReplyToJSON.extractListPathFromJson(reportResult, "$.deepDetailedReportModel.groups[0].resources");
		} else if(repliedTrack!=null){
				resources = ReplyToJSON.extractListPathFromJson(repliedTrack, "$.resources");
		} else {
			assertTrue(false, "no replies found in container to count resources");
		}
		assertEquals("API: check resources size", resources.size(), totalResourcesExpected, "wrong number of resources received.");
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
	}
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) Сабресурсов$")
	public void checkNumOfSubresourcesReceived(int totalSubResourcesExpected)  {
		try {
	
		JSONObject reportResult = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportResult");
		List<JSONObject> subResources = ReplyToJSON.extractListPathFromJson(reportResult, "$.deepDetailedReportModel.groups[0].resources[0].subResources");
		assertEquals("API: check subResources size", subResources.size(), totalSubResourcesExpected, "wrong number of subResources received.");
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
	}
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для Ресурса (\\d+) для параметра <([^>]*)> получено значение <([^>]*)>$")
	public void checkForResourceParam_X_ReceivedVal_Y(int resourceIndex, String param, String value) {
		try {
	
		JSONObject reportResult = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportResult");
		JSONObject repliedTrack = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("repliedTrack");
		List<JSONObject> resources=null;
		String actualValue="";

		if(reportResult!=null){
			resources = ReplyToJSON.extractListPathFromJson(reportResult, "$.deepDetailedReportModel.groups[0].resources");
			actualValue = ReplyToJSON.extractPathFromJson(resources.get(resourceIndex-1), "$.resourceCard."+param);
		} else if(repliedTrack!=null){
			resources = ReplyToJSON.extractListPathFromJson(repliedTrack, "$.resources");
			actualValue = ReplyToJSON.extractPathFromJson(resources.get(resourceIndex-1), "$."+param);
		} else {
		assertTrue(false, "no replies found in container to check resources");
		}

		if(value.equals("id тестового ТС")){
			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			value = vehicleFromPrevStep.getVehicleId();
		} 
		if(value.equals("id тестового Сотрудника")){
			Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
			value = employeeFromPrevStep.getEmployeeId();
		}
		assertEquals("API:resource["+resourceIndex+"]."+param,  actualValue ,value, "resource."+param+" не верен");
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
	}
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для Сабресурса (\\d+) для параметра <([^>]*)> получено значение <([^>]*)>$")
	public void checkForSubResourceParam_X_ReceivedVal_Y(int subresourceIndex, String param, String value) {
		try {
			JSONObject reportResult = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportResult");
		
		List<JSONObject> subresources = ReplyToJSON.extractListPathFromJson(reportResult, "$.deepDetailedReportModel.groups[0].resources[0].subResources");
		if(value.equals("id тестового ТС")){
			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			value = vehicleFromPrevStep.getVehicleId();
		} 
		if(value.equals("id тестового Сотрудника")){
			Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
			value = employeeFromPrevStep.getEmployeeId();
		}
		assertEquals("API:resource["+subresourceIndex+"]."+param, 
				ReplyToJSON.extractPathFromJson(subresources.get(subresourceIndex-1), "$.resourceCard."+param), value,
			//	resources.get(subresourceIndex-1).get(param),value, 
				"resource."+param+" не верен");
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
	}
	@ContinueNextStepsFor({AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) записей о событиях для Сабресурса (\\d+)$")
	public void проверить_что_вернулись_записей_о_событиях_для_Сабресурса(int totalExpectedEvents, int subResourceIndex)   {
		try {
		JSONObject reportResult = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportResult");
		List<JSONObject> extendedEvents = ReplyToJSON.extractListPathFromJson(reportResult, "$.deepDetailedReportModel.groups[0].resources[0].subResources["+(subResourceIndex-1)+"].extended");
		assertEquals("API: check subResource["+subResourceIndex+"].extended.events size", extendedEvents.size(), totalExpectedEvents, "wrong number of events in subResource "+subResourceIndex+" received.");
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
	}
	
	@ContinueNextStepsFor({AssertionError.class, NullPointerException.class})
	@Then("^Проверить, что для extended (\\d+) Сабресурса (\\d+) для параметра <\"([^\"]*)\"> получено значение <(.*)>$")
	public void checkParamValueforExtendedXSubresourceYStep(int extendedIndex, int subresourceIndex, String param, String roadEventValue)  {
		checkParamValueforExtendedXSubresourceY(extendedIndex,subresourceIndex,param,roadEventValue);
	}

	@ContinueNextStepsFor({AssertionError.class, NullPointerException.class})
	@Then("^Проверить, что для extended (\\d+) для параметра <\"([^\"]*)\"> получено значение <(.*)>$")
	public void checkParamValueforExtendedXSubresource1Step(int extendedIndex, String param, String roadEventValue)  {
		checkParamValueforExtendedXSubresourceY(extendedIndex,1,param,roadEventValue);
	}
	
	@ContinueNextStepsFor({AssertionError.class, NullPointerException.class})
	@Then("^Проверить, что для extended (\\d+) для параметра <([^\"]*)> получено значение <(.*)>$")
	public void checkParamValueforExtendedXSubresource1Step2(int extendedIndex, String param, String roadEventValue)  {
		checkParamValueforExtendedXSubresourceY(extendedIndex,1,param,roadEventValue);
	}
	
	public void checkParamValueforExtendedXSubresourceY(int extendedIndex, int subresourceIndex, String param, String roadEventValue)  {
		try {
		JSONObject reportResult = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportResult");
		List<JSONObject> extendedEvents = ReplyToJSON.extractListPathFromJson(reportResult, 
										"$.deepDetailedReportModel.groups[0].resources[0].subResources["+(subresourceIndex-1)+"].extended");
		assertTrue("extendedEventsList contains event №"+extendedIndex, extendedEvents.size()>=extendedIndex, "список содержит "+extendedEvents.size()+" extended, extended №"+extendedIndex+" не может быть проверено");
		JSONObject event = extendedEvents.get(extendedIndex-1);
		if(roadEventValue.startsWith("\"") && roadEventValue.endsWith("\"")){  //just a regular value to compare
			roadEventValue = roadEventValue.substring(1, roadEventValue.length()-1);
		} else {  // values like: <число = (текуший день - 3); время = "21:54:00">
			SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
			if(roadEventValue.contains("число =") && roadEventValue.contains("время =")){
				Calendar calCur = Calendar.getInstance();
				  String[] arr = roadEventValue.split(";");
				  if(arr[0].contains("текуший день")){
					  
					  int daysToSubstract = 0;
					  Pattern pattern = Pattern.compile("(\\d+)");
					  Matcher matcher = pattern.matcher(arr[0]);
					  if (matcher.find())
					  {
						  daysToSubstract = Integer.parseInt(matcher.group(1));
					  }
//						log.debug("daysToSubstract=="+daysToSubstract);
						calCur.add(Calendar.DATE, - (daysToSubstract));
						calCur.get(Calendar.YEAR);
				  }
				  if(arr[1].contains("время")){
					  Pattern pattern = Pattern.compile("\"(.*)\"");
					  Matcher matcher = pattern.matcher(arr[1]);
					  if (matcher.find())
					  {
						  roadEventValue = dfDate.format(calCur.getTime()) +" "+ matcher.group(1);
						 
					  }
				  }
				  
			} if (roadEventValue.contains("Имя объекта")){
				roadEventValue = event.get("relatedResourceName").toString();  
			} if (roadEventValue.contains("id объекта")){
				roadEventValue = event.get("relatedResourceId").toString(); 
			}
		}
		if(event.keySet().contains(param)){
		assertEquals("API: check extended["+extendedIndex+"].parameter "+param+" value", 
				event.get(param).toString(), roadEventValue, "wrong value for extended["+extendedIndex+"].parameter: "+param);
		} else {
			assertTrue(false, "ответ API не содержит поле "+param);
		}
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
	}
	
	
	@Then("^Проверить, что вернулись (\\d+) Групп объектов$")
	public void проверить_что_вернулись_Групп_объектов(int numOfObjects) {
		JSONObject reportResult = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportResult");
		try {
			assertEquals("API:report_resourcesFilter.include",
					ReplyToJSON.extractListPathFromJson(reportResult, "$.deepDetailedReportModel.groups").size(), numOfObjects,	
								"ожидалось "+numOfObjects+" групп объектов");
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error("Test failed: " + e.toString());
			assertTrue(false, e.toString());
		}
		setToAllureChechkedFields();
	}
}
