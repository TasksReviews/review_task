package reports;

import java.util.Map;

import org.json.simple.JSONArray;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Report;
import com.t1.core.api.TelematicVirtualTracker;

import cucumber.api.java.en.Then;

public class GET_Reports extends AbstractClass{
	public Report report; 
	//why these methods in get_report?
	@Then("^Получить информацию проанализированных интервалов с параметрами из таблицы: \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void analyticIntervalResults(String deviceid,String startTime, String endTime){
		log.debug("Start>> report test");
		report = new Report();
		report.setDateFrom(startTime);
		report.setDateTo(endTime);
		report.setDeviceId(deviceid);
		JSONArray createdReport = (JSONArray) report.generateAnalyticIntervalResults( deviceid, startTime, endTime);

		log.debug("end of report");

	}

	@Then("^Получить информацию проанализированных интервалов для созданного ТМУ за период с \"([^\"]*)\" по \"([^\"]*)\"$")
	public void analyticIntervalResults(String startTime, String endTime){
		 TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		 Map<String, String> dateTimePeriod = getDateTimePeriod(startTime, endTime);
		log.debug("Start>> report test");
		report = new Report();
		report.setDateFrom(dateTimePeriod.get("dateTimeFrom").toString()) ;
		report.setDateTo(dateTimePeriod.get("dateTimeTo").toString());
		report.setDeviceId(telematics_device.getTrackerId());
		JSONArray createdReport = (JSONArray) report.generateAnalyticIntervalResults( telematics_device.getTrackerId(), report.getDateFrom(), report.getDateTo());

		log.debug("end of report");

	}

}