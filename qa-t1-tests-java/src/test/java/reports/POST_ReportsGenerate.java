package reports;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.simple.JSONObject;

import com.google.common.annotations.Beta;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.Report;
import com.t1.core.api.Vehicle;

import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Then;
import security_settings.GET_Timezones;

public class POST_ReportsGenerate extends AbstractClass{
	public Report report; 
	
	@Then("^запросить отчет с параметрами из таблицы: \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void generate(String reportType, String reportCriteriaParams, String resourceType, final String resourcesFilterInclude, String dateFrom, String dateTo, 
						  String graphTimeUnit, String dataTimeUnit, final String geozoneIds,  Boolean isDeepDetailed){
		log.debug("Start>> report test");
		String detalizationType ="";
		if(isDeepDetailed){
			detalizationType ="DeepDetailed";
		} else {
			detalizationType ="Consolidated";
		}
		String timezone =" "+ GET_Timezones.getAccountTimezone();
		report = new Report( reportType,null,null, reportCriteriaParams, resourceType, resourcesFilterInclude, dateFrom+timezone, dateTo+timezone, "Manually", 
				geozoneIds,  detalizationType);
		
		JSONObject createdReport = (JSONObject) report.requestReportGeneration();
		SharedContainer.setContainer("reports", report);
		log.debug("end of report");
		//vehicle.deleteVehicle(vehicle.vehicleId);
	}
	@ContinueNextStepsFor({ AssertionError.class})
	//Then Отправить запрос построения отчета для ТС с категорией = <"По поездкам">, с типом = <"Детализированный">, за период с = <1, 0>, по = <0, 0>
	@Then("^Отправить запрос построения отчета для ТС с категорией = <\"([^\"]*)\">, с типом = <\"([^\"]*)\">, за период с = <([^>]*)>, по = <([^>]*)>$")
	public void requestReportGenerationForVehicleByCategoryByTypeForPeriod(String reportCategory, String reportType, List fromDate, List tillDate){
		requestReportGenerationForObjectByCategoryByTypeForPeriod("Vehicle", reportCategory, reportType, fromDate, tillDate,"Manually");
	}
	
	@Then("^Отправить запрос построения отчета для ТС с категорией = <([^\"]*)>, с типом = <([^\"]*)>, за период с = <([^>]*)>, по = <([^>]*)>$")
	public void requestReportGenerationForVehicleByCategoryByTypeForPeriod2(String reportCategory, String reportType, List fromDate, List tillDate){
		requestReportGenerationForObjectByCategoryByTypeForPeriod("Vehicle", reportCategory, reportType, fromDate, tillDate,"Manually");
	}
	
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Отправить запрос построения отчета для Сотрудника с категорией = <\"([^\"]*)\">, с типом = <\"([^\"]*)\">, за период с = <([^>]*)>, по = <([^>]*)>$")
	public void requestReportGenerationForEmployeeByCategoryByTypeForPeriod(String reportCategory, String reportType, List fromDate, List tillDate){
		requestReportGenerationForObjectByCategoryByTypeForPeriod("Employee", reportCategory, reportType, fromDate, tillDate,"Manually");
	}
	
	@Then("^Отправить запрос получения тоталов для ТС для отчета с id = <(.*)>, с детализацией = <(.*)>, за период с = <(.*)>, по = <(.*)>$")
	public void отправить_запрос_получения_тоталов_для_ТС_для_отчета_с_id_reportId_с_детализацией_День_за_период_с_по(String id, String detail, List fromDate, List tillDate)  {
		
		report = (Report) SharedContainer.getLastObjectFromSharedContainer("reports");
		
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		String vehicleId = vehicleFromPrevStep.getVehicleId();
		String timezone =" "+ GET_Timezones.getAccountTimezone();
		DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		report.getUrlParameters().put("reportId",report.getCacheReportId());
		report.getUrlParameters().put("dateFrom",df.format(dateTimeCorrectionFromInputArr(fromDate))+timezone);
		report.getUrlParameters().put("dateTo",df.format(dateTimeCorrectionFromInputArr(tillDate))+timezone);
		report.getUrlParameters().put("timeUnit","day");
		ArrayList arr = new ArrayList();
		arr.add(vehicleId);
		report.getUrlParameters().put("resources",arr);
		
		ArrayList<JSONObject> totalValuesBySubresources = (ArrayList<JSONObject>) report.requestTotalValuesBySubresources();
		JSONObject createdReport = new JSONObject();
		createdReport.put("report", totalValuesBySubresources);
		SharedContainer.setContainer("reportTotalValuesBySubresources", createdReport);
	}
	
	@Then("^Отправить запрос получения графиков для ТС для отчета с id = <(.*)>, с детализацией = <(.*)>, за период с = <(.*)>, по = <(.*)>$")
	public void отправить_запрос_получения_графиков_для_ТС_для_отчета_с_id_reportId_с_детализацией_День_за_период_с_по(String id, String detail, List fromDate, List tillDate)  {
		
		report = (Report) SharedContainer.getLastObjectFromSharedContainer("reports");
		
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		String vehicleId = vehicleFromPrevStep.getVehicleId();
		String timezone =" "+ GET_Timezones.getAccountTimezone();
		DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		report.getUrlParameters().put("reportId",report.getCacheReportId());
		report.getUrlParameters().put("dateFrom",df.format(dateTimeCorrectionFromInputArr(fromDate))+timezone);
		report.getUrlParameters().put("dateTo",df.format(dateTimeCorrectionFromInputArr(tillDate))+timezone);
		report.getUrlParameters().put("timeUnit","day");
		ArrayList arr = new ArrayList();
		arr.add(vehicleId);
		report.getUrlParameters().put("resources",arr);
		
		ArrayList<JSONObject> chartPointsBySubresources = (ArrayList<JSONObject>) report.requestGetChartPointsBySubresources();
		JSONObject createdReport = new JSONObject();
		createdReport.put("report", chartPointsBySubresources);
		SharedContainer.setContainer("reportGetChartPointsBySubresources", createdReport);
	}
	
	
	@Then("^Отправить запрос получения точек для графика в истории перемещений для ТС за период с = <(.*)>, по = <(.*)>$")
	public void отправить_запрос_получения_точек_для_графика_в_истории_перемещений_для_ТС_за_период_с_по(List fromDate, List tillDate)  {
		
		Report report = new Report();
		
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		String vehicleId = vehicleFromPrevStep.getVehicleId();
		String timezone =" "+ GET_Timezones.getAccountTimezone();
		DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		report.getUrlParameters().put("dateFrom",df.format(dateTimeCorrectionFromInputArr(fromDate))+timezone);
		report.getUrlParameters().put("dateTo",df.format(dateTimeCorrectionFromInputArr(tillDate))+timezone);
		ArrayList arr = new ArrayList();
		arr.add(vehicleId);
		report.getUrlParameters().put("resources",arr);
		
		ArrayList<JSONObject> chartPointsBySubresources = (ArrayList<JSONObject>) report.requestGetAllSensorsChartPoints();
		JSONObject createdReport = new JSONObject();
		createdReport.put("report", chartPointsBySubresources);
		SharedContainer.setContainer("reportGetAllSensorsChartPoints", createdReport);
	}
	
	@Then("^Отправить запрос получения точек для графика в истории перемещений для Сотрудника за период с = <(.*)>, по = <(.*)>$")
	public void отправить_запрос_получения_точек_для_графика_в_истории_перемещений_для_Сотрудника_за_период_с_по(List fromDate, List tillDate)  {
		
		Report report = new Report();
		
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		String employeeId = employeeFromPrevStep.getEmployeeId();
		String timezone =" "+ GET_Timezones.getAccountTimezone();
		DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		report.getUrlParameters().put("dateFrom",df.format(dateTimeCorrectionFromInputArr(fromDate))+timezone);
		report.getUrlParameters().put("dateTo",df.format(dateTimeCorrectionFromInputArr(tillDate))+timezone);
		ArrayList arr = new ArrayList();
		arr.add(employeeId);
		report.getUrlParameters().put("resources",arr);
		
		ArrayList<JSONObject> chartPointsBySubresources = (ArrayList<JSONObject>) report.requestGetAllSensorsChartPoints();
		JSONObject createdReport = new JSONObject();
		createdReport.put("report", chartPointsBySubresources);
		SharedContainer.setContainer("reportGetAllSensorsChartPoints", createdReport);
	}
	
	public void requestReportGenerationForObjectByCategoryByTypeForPeriod(String objectType, String reportCategory, String detalizationType, List fromDate, List tillDate,String periodicity) {
		log.debug("Start>> report test");
		String reportCriteria = "";
		String objectId ="";
		String resourceType="";
		if (objectType.equals("Vehicle")) {
			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			objectId=vehicleFromPrevStep.getVehicleId();
			resourceType = "Vehicles";
		} else if (objectType.equals("Employee")) {
			Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
			objectId=employeeFromPrevStep.getEmployeeId();
			resourceType = "Employees";
		} else {
			assertTrue(false, "unsupported object type " + objectType);
		}
			
		if(reportCategory.equals("По поездкам")){
			reportCategory= "TravelReport";
			reportCriteria = "1,2,3,4,5,6,7,8,9,10,11,12,13";
		 } else if (reportCategory.equals("По нарушениям")){
			 reportCategory= "ReportOnViolations";
			 reportCriteria = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23";
		 } else if (reportCategory.equals("По температуре")){
			 reportCategory= "ReportOnSensor";
			 reportCriteria = "1,2,3,5,6,7";
		 }else {
		 	 assertTrue(false, "not implemented report category: "+reportCategory);
		 }
		
		 HashMap reportVals = new InitDefValues().initializeReportInputWithDefaultOrRandomValues(reportCategory,"stdPattern");
		String timezone =" "+ GET_Timezones.getAccountTimezone();
		DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		report = new Report( reportCategory,null,null, reportCriteria,resourceType ,objectId , df.format(dateTimeCorrectionFromInputArr(fromDate))+timezone, 
				df.format(dateTimeCorrectionFromInputArr(tillDate))+timezone, periodicity, null,  detalizationType);
		
		JSONObject createdReport = (JSONObject) report.requestReportGeneration();
		SharedContainer.setContainer("reports", report);
		SharedContainer.setContainer("reportResult", createdReport);
		log.debug("end of report");
	}
	

	
	@Then("^запросить отчет сгенерированый в предыдущем шаге используя cacheReportId$")
	public void requestCachedReport(){
		log.debug("Start>> request cached report");
//		String timezone =" "+ GET_Timezones.getAccountTimezone();
		report = (Report) SharedContainer.getLastObjectFromSharedContainer("reports");
		JSONObject createdReport = (JSONObject) report.requestCachedReportGeneration();
		log.debug("end of report");
	}
	@Beta
	@Then("^запросить отчет сгенерированый в предыдущем шаге используя некорректный cacheReportId$")
	public void requestCachedReportWrongId(){
		log.debug("Start>> request cached report");
//		String timezone =" "+ GET_Timezones.getAccountTimezone();
		report = (Report) SharedContainer.getLastObjectFromSharedContainer("reports");
		report.setCacheReportId(report.getCacheReportId().substring(0, 24)+"ffffffffffff");
		JSONObject createdReport = (JSONObject) report.requestCachedReportGeneration();
		assertTrue(false,"was used wrong guid but received OK response");
		log.debug("end of report");
	}
	

	    

}