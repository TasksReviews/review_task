package reports;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.Vehicle;

import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Then;

public class ReportsTotalsAndGraphsStepsWithChecks extends AbstractClass {
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) Ресурсов в запросе TotalValuesBySubresources$")
	public void checkXresourcesReturnedForTotalValuesBySubresources(int totalResources) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportTotalValuesBySubresources");
		ArrayList<JSONObject> totalValsReport = (ArrayList<JSONObject>) report.get("report");
		log.debug(totalValsReport);
		assertEquals("API TotalValuesBySubresources_total_resources", totalValsReport.size(),totalResources, "total resources не верен");
		setToAllureChechkedFields();
	}

	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для Ресурса (\\d+) для параметра <id> получено значение <id тестового ТС> в запросе TotalValuesBySubresources$")
	public void checkResourceXInParamYReturnedValueForTotalValuesBySubresources(int resourceIndex) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportTotalValuesBySubresources");
		ArrayList<JSONObject> totalValsReport = (ArrayList<JSONObject>) report.get("report");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		assertEquals("API TotalValuesBySubresources["+resourceIndex+"]_resourceID", 
				ReplyToJSON.extractPathFromJson(totalValsReport.get(resourceIndex-1), "$.id"),vehicleFromPrevStep.getVehicleId(),
				"["+resourceIndex+"]_resourceID не верен");
		setToAllureChechkedFields();
	}

	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) Сабресурсов в запросе TotalValuesBySubresources$")
	public void checkXSubresourcesReturnedForTotalValuesBySubresources(int totalSubResources) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportTotalValuesBySubresources");
		ArrayList<JSONObject> totalValsReport = (ArrayList<JSONObject>) report.get("report");
		ArrayList<JSONObject> subresources = (ArrayList<JSONObject>)totalValsReport.get(0).get("subresources");
		assertEquals("API TotalValuesBySubresources ", subresources.size(),totalSubResources, "total subresources не верен");
		setToAllureChechkedFields();
	}

	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для Сабресурса (\\d+) для параметра <id> получено значение <id тестового ТС> в запросе TotalValuesBySubresources$")
	public void checkSubresourceXInParamYReturnedValueForTotalValuesBySubresources(int subResourceIndex) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportTotalValuesBySubresources");
		ArrayList<JSONObject> totalValsReport = (ArrayList<JSONObject>) report.get("report");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		ArrayList<JSONObject> subresources = (ArrayList<JSONObject>)totalValsReport.get(0).get("subresources");
		assertEquals("API TotalValuesBySubresources ", 
				ReplyToJSON.extractPathFromJson(subresources.get(subResourceIndex-1), "$.resourceId"),vehicleFromPrevStep.getVehicleId(), 
				"subresourceId не верен");
		setToAllureChechkedFields();
	}

	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) периодов для Сабресурса (\\d+) в запросе TotalValuesBySubresources$")
	public void checkReturnedXPeriodsForSubresourceYForTotalValuesBySubresources(int totalPeriods, int subResourceIndex) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportTotalValuesBySubresources");
		ArrayList<JSONObject> totalValsReport = (ArrayList<JSONObject>) report.get("report");
		ArrayList<JSONObject> subresources = (ArrayList<JSONObject>)totalValsReport.get(0).get("subresources");
		ArrayList<JSONObject> periods = (ArrayList<JSONObject>) subresources.get(subResourceIndex-1).get("periods");
		assertEquals("API TotalValuesBySubresources ", periods.size(),totalPeriods, "total periods не верен");
		setToAllureChechkedFields();
	}

	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для периода (\\d+) для параметра <([^>]*)> получено значение <([^>]*)> в запросе TotalValuesBySubresources$")
	public void checkPeriodXForParameterYReturnedValueInTotalValuesBySubresources(int periodIndex, String paramName, String paramValue) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportTotalValuesBySubresources");
		ArrayList<JSONObject> totalValsReport = (ArrayList<JSONObject>) report.get("report");
		ArrayList<JSONObject> subresources = (ArrayList<JSONObject>)totalValsReport.get(0).get("subresources");
		ArrayList<JSONObject> periods = (ArrayList<JSONObject>) subresources.get(0).get("periods");
		if(paramValue.contains("число =") && paramValue.contains("время =")){
			paramValue = convertStrDateTimeRequested(paramValue, false);
		}
		assertEquals("API TotalValuesBySubresources ", periods.get(periodIndex-1).get(paramName),paramValue, "periods["+periodIndex+"]"+paramName+" не верен");
		setToAllureChechkedFields();
	}

	private String convertStrDateTimeRequested(String paramValue, boolean isUnixTime) {
		SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
		if(paramValue.contains("число =") && paramValue.contains("время =")){
			Calendar calCur = Calendar.getInstance();
			  String[] arr = paramValue.split(";");
			  if(arr[0].contains("текуший день") || arr[0].contains("текущий день")){
				  int daysToSubstract = 0;
				  Pattern pattern = Pattern.compile("(\\d+)");
				  Matcher matcher = pattern.matcher(arr[0]);
				  if (matcher.find())
				  {
					  daysToSubstract = Integer.parseInt(matcher.group(1));
				  }
					calCur.add(Calendar.DATE, - (daysToSubstract));
					calCur.get(Calendar.YEAR);
			  }
			  if(arr[1].contains("время")){
				  Pattern pattern = Pattern.compile("\"(.*)\"");
				  Matcher matcher = pattern.matcher(arr[1]);
				  if (matcher.find())
				  {
					  paramValue = dfDate.format(calCur.getTime()) +" "+ matcher.group(1);
				  }
			  }
			  if(isUnixTime){
				  //2018-05-21 08:00:00 +03:00
				  dfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss X");
				  Long unixDateTime =0L;
				  try {
					unixDateTime = dfDate.parse(paramValue).getTime();
					if (paramValue.contains("+03:00")) {
						long inMs1hr = 60 * 60 * 1000L;
						unixDateTime = (unixDateTime + (6 * inMs1hr))/1000;
					}
					paramValue = unixDateTime.toString();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			  }
		}
		return paramValue;
	}

	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для периода (\\d+) Сабресурса (\\d+) получено (\\d+) тоталов в запросе TotalValuesBySubresources$")
	public void checkPeriodXForSubresourceYReturnedZTotalsInTotalValuesBySubresources(
			int periodIndex, int subresourceIndex, int numOfTotals) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportTotalValuesBySubresources");
		ArrayList<JSONObject> totalValsReport = (ArrayList<JSONObject>) report.get("report");
//		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		ArrayList<JSONObject> subresources = (ArrayList<JSONObject>)totalValsReport.get(0).get("subresources");
		ArrayList<JSONObject> periods = (ArrayList<JSONObject>) subresources.get(subresourceIndex-1).get("periods");
		ArrayList<String> totals = (ArrayList<String>) periods.get(periodIndex-1).get("total");
		assertEquals("API TotalValuesBySubresources ", totals.size(),numOfTotals, "количество total не верено");
		setToAllureChechkedFields();
	}

	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для тотала (\\d+) периода (\\d+) Сабресурса (\\d+) для параметра <(\\d+)> получено значение <([^>]*)> в запросе TotalValuesBySubresources$")
	public void checkTotalWPeriodXForSubresourceYParameterZReturnedValueInTotalValuesBySubresources(
			int totalIndex, int periodIndex, int subresourceIndex, int paramIndex, String totalParamVal) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportTotalValuesBySubresources");
		ArrayList<JSONObject> totalValsReport = (ArrayList<JSONObject>) report.get("report");
		ArrayList<JSONObject> subresources = (ArrayList<JSONObject>)totalValsReport.get(0).get("subresources");
		ArrayList<JSONObject> periods = (ArrayList<JSONObject>) subresources.get(0).get("periods");
		ArrayList<String> totals = (ArrayList<String>) periods.get(periodIndex-1).get("total");
		assertEquals("API TotalValuesBySubresources ", String.valueOf(totals.get(paramIndex-1)),totalParamVal, "значение для параметра "+paramIndex+" в total не верено");
		setToAllureChechkedFields();
	}
	
////////////////////////////////////////   GetChartPointsBySubresources   //////////////////////////////////////////////////////////////
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) графиков в запросе GetChartPointsBySubresources$")
	public void checkReturnedXGraphsForGetChartPointsBySubresources(int totalGraphs) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportGetChartPointsBySubresources");
		ArrayList<JSONObject> chartPointsReport = (ArrayList<JSONObject>) report.get("report");
		assertEquals("API GetChartPointsBySubresources_total_resources", chartPointsReport.size(),totalGraphs, "колиество графиков не верено");
		setToAllureChechkedFields();
	}
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для графика (\\d+) для параметра <([^>]*)> получено значение <([^>]*)> в запросе GetChartPointsBySubresources$")
	public void checkGraphXParameterYReturnedValueInGetChartPointsBySubresources(
			int graphIndex, String paramName, String paramValue)  {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportGetChartPointsBySubresources");
		ArrayList<JSONObject> chartPointsReport = (ArrayList<JSONObject>) report.get("report");
		if(paramValue.equals("id тестового ТС")){
			Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			paramValue= vehicleFromPrevStep.getVehicleId();
		}
		assertEquals("API GetChartPointsBySubresources["+graphIndex+"]_"+paramName, 
				ReplyToJSON.extractPathFromJson(chartPointsReport.get(graphIndex-1), "$."+paramName),paramValue, 
				"значение для параметра "+paramName+" в графике "+graphIndex+" не верено");
		setToAllureChechkedFields();
	}


	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для графика (\\d+) в массиве (\\w+) вернулось (\\d+) точек$")
	public void checkForGraphXInArrayYReturnedZOfPoints(int graphIndex, String paramName, int totalPoints) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportGetChartPointsBySubresources");
		ArrayList<JSONObject> chartPointsReport = (ArrayList<JSONObject>) report.get("report");
		JSONObject graph = chartPointsReport.get(graphIndex-1);
		ArrayList<JSONObject> arr = (ArrayList<JSONObject>) graph.get(paramName);
		assertEquals("API GetChartPointsBySubresources["+graphIndex+"]_"+paramName, arr.size(),totalPoints, "количество точек в массиве "+paramName+" не верено");
		setToAllureChechkedFields();
	}
	

	@ContinueNextStepsFor({ AssertionError.class, IndexOutOfBoundsException.class})
	@Then("^Проверить, что для точки (\\d+) в массиве (\\w+) графика (\\d+) для параметра <([^>]*)> получено значение <(.*)>$")
	public void checkForPointWInArrayXGrpaphYParameterZValue(
			int pointIndex, String arrName, int graphIndex, String paramName, String paramValue) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportGetChartPointsBySubresources");
		ArrayList<JSONObject> chartPointsReport = (ArrayList<JSONObject>) report.get("report");
		JSONObject graph = chartPointsReport.get(graphIndex-1);
		ArrayList<JSONObject> arr = (ArrayList<JSONObject>) graph.get(arrName);
		if(paramValue.contains("число =") && paramValue.contains("время =")){
			paramValue = convertStrDateTimeRequested(paramValue, true);
		}
		assertEquals("API GetChartPointsBySubresources["+graphIndex+"]_"+paramName, String.valueOf(arr.get(pointIndex-1).get(paramName)),paramValue, 
				"значение для точки "+pointIndex+" в массиве "+arrName+" не верено");
		setToAllureChechkedFields();
	}


	//Проверить, что для графика Y в массиве yDomain вернулись <5> и <5>
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для графика (\\d+) в массиве yDomain вернулись <(\\d+)> и <(\\d+)>$")
	public void checkForGrpaphYInArrayyDomainValues(int graphIndex, String val1, String val2) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportGetChartPointsBySubresources");
		ArrayList<JSONObject> chartPointsReport = (ArrayList<JSONObject>) report.get("report");
		JSONObject graph = chartPointsReport.get(graphIndex-1);
		ArrayList<JSONObject> yDomainarr = (ArrayList<JSONObject>) graph.get("yDomain");
		assertEquals("API GetChartPointsBySubresources[0]_yDomain[1]", String.valueOf(yDomainarr.get(0)),val1, "значение для точки 1 в массиве yDomain не верено");
		assertEquals("API GetChartPointsBySubresources[0]_yDomain[2]", String.valueOf(yDomainarr.get(1)),val2, "значение для точки 2 в массиве yDomain не верено");
		setToAllureChechkedFields();
	}
//////////////////////////////////////Reports/GetAllSensorsChartPoints///////////
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что вернулись (\\d+) графиков$")
	public void checkReturnedXOfGrpaphs(int totalGraphs) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportGetAllSensorsChartPoints");
		ArrayList<JSONObject> allSensorsChartPointsReport = (ArrayList<JSONObject>) report.get("report");
		assertEquals("API GetAllSensorsChartPoints_total_graphs", allSensorsChartPointsReport.size(),totalGraphs, "колиество графиков не верено");
		setToAllureChechkedFields();
	}
	
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для графика (\\d+) для параметра <([^>]*)> получено значение <([^>]*)>$")
	public void checkValueXInGraphY(int graphIndex, String paramName, String expectedParamValue) {
		if (!(expectedParamValue.matches("-?\\d+") 
				|| expectedParamValue.equals("false")
				|| expectedParamValue.equals("true") ) ) {
			if (expectedParamValue.equals("id тестового ТС")) {
				Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
				expectedParamValue = vehicleFromPrevStep.getVehicleId();
			} else if (expectedParamValue.startsWith("\"") && expectedParamValue.endsWith("\"")){
				expectedParamValue = expectedParamValue.substring(1, expectedParamValue.length()-1);
			}
				else {
				assertTrue(false, "unimplemented type of expected value " + expectedParamValue);
			}
		}
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportGetAllSensorsChartPoints");
		ArrayList<JSONObject> chartPointsReport = (ArrayList<JSONObject>) report.get("report");
		JSONObject graph = chartPointsReport.get(graphIndex-1);
		String actualParamValue = graph.get(paramName).toString();
		assertEquals("API GetAllSensorsChartPoints["+graphIndex+"]_"+paramName,actualParamValue,expectedParamValue, "значение для параметра "+paramName+" не верено");
		setToAllureChechkedFields();
	}
	
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что для графика (\\d+) в массиве (\\w+) вернулись (\\d+) точек$")  // quite similar method but for other report
	public void checkForGraphXInArrayYReturnedZOfPoints2(int graphIndex, String paramName, int totalPoints) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportGetAllSensorsChartPoints");
		ArrayList<JSONObject> chartPointsReport = (ArrayList<JSONObject>) report.get("report");
		JSONObject graph = chartPointsReport.get(graphIndex-1);
		ArrayList<JSONObject> arr = (ArrayList<JSONObject>) graph.get(paramName);
		assertEquals("API GetChartPointsBySubresources["+graphIndex+"]_"+paramName, arr.size(),totalPoints, "количество точек в массиве "+paramName+" не верено");
		setToAllureChechkedFields();
	}
	
	@ContinueNextStepsFor({ AssertionError.class})
	@Then("^Проверить, что в запросе Reports/GetAllSensorsChartPoints, для точки (\\d+) в массиве (\\w+) графика (\\d+) для параметра <([^>]*)> получено значение <([^>]*)>$")
	public void checkRequestReportsGetAllSensorsChartPointsForPointInArrayXGraphYParameterZValue
	(int pointIndex, String arrName, int graphIndex, String paramName, String expectedValue) {
		JSONObject report = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("reportGetAllSensorsChartPoints");
		ArrayList<JSONObject> chartPointsReport = (ArrayList<JSONObject>) report.get("report");
		JSONObject graph = chartPointsReport.get(graphIndex-1);
		ArrayList<JSONObject> arr = (ArrayList<JSONObject>) graph.get(arrName);
		assertTrue(! arr.isEmpty(),"массив "+arrName+" не содержит элементов");
		JSONObject point = arr.get(pointIndex - 1);

		if(expectedValue.contains("число =") && expectedValue.contains("время =")){
			expectedValue = convertStrDateTimeRequested(expectedValue, false);
			SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				long unixtime = dfDate.parse(expectedValue).getTime() /1000;
				long inMs3hr = 3 * 60 * 60;
				expectedValue = Long.toString(unixtime+inMs3hr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		assertEquals("API GetChartPointsBySubresources["+graphIndex+"]_"+paramName, point.get(paramName).toString(),expectedValue.toString(), "значение точки "+pointIndex+" параметра "+paramName+" в массиве "+arrName+" не верено");
		setToAllureChechkedFields() ;
	}
	
}
