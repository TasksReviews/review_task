package imitation_virtualTrackers;

import static com.mongodb.client.model.Filters.eq;

import java.sql.Date;
import java.util.HashMap;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicVirtualTracker;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_VirtualTelematicsTracker_Create extends AbstractClass {
    public TelematicVirtualTracker tracker;

    @Then("^создать новое ТМУ$")
    public void create() {
        log.debug("Start>> VirtualTelematicsTracker_create");
        tracker = new TelematicVirtualTracker(true);
        JSONObject createdTracker = (JSONObject) tracker.createNewVirtualTracker().get(0);
        SharedContainer.setContainer("virtualTrackers", tracker );
        log.debug("end of tracker");
    }

    @Then("^создать новое ТМУ с полями содержащими кирилицу: \"([^\"]*)\"$")
    public void create_cyrilicValues(String inputPattern) {
        log.debug("Start>> VirtualTelematicsTracker_create_cyrilicValues");
        log.info("Got pattern:=" + inputPattern);
        tracker = new TelematicVirtualTracker(inputPattern);
        JSONObject createdtracker = (JSONObject) tracker.createNewVirtualTracker().get(0);
        SharedContainer.setContainer("virtualTrackers", tracker);

    }

    @Then("^создать новое ТМУ с полями содержащими цифры: \"([^\"]*)\"$")
    public void create_numbersInValues(String inputPattern) {
        log.debug("Start>> VirtualTelematicsTracker_create_numbersInValues");
        log.info("Got pattern:=" + inputPattern);
        tracker = new TelematicVirtualTracker(inputPattern);
        JSONObject createdtracker = (JSONObject) tracker.createNewVirtualTracker().get(0);
        SharedContainer.setContainer("virtualTrackers", tracker);
    }

    @Then("^создать новое ТМУ с полями содержащими спец символы: (.*)$")
    public void create_SpecialCharachtersInFields(String inputPattern) {
        log.debug("Start>> VirtualTelematicsTracker_create_SpecialCharachtersInFields");
        log.info("Got pattern:=" + inputPattern);
        tracker = new TelematicVirtualTracker(inputPattern);
        JSONObject createdtracker = (JSONObject) tracker.createNewVirtualTracker().get(0);
        SharedContainer.setContainer("virtualTrackers", tracker);
    }
	
//	steps that dont call web API methods
    
    @Given("^Отредактировать время привязки ТМУ к ТС в коллекции \"([^\"]*)\" на \"([^\"]*)\"$")
    public void updateBoundToResourceTimeDevicetoVehicle(String collection, String newCreationTime) {
    	TelematicVirtualTracker deviceFromPrevStep = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		Bson filter = Filters.and(eq("DeviceId",new ObjectId(deviceFromPrevStep.getTrackerId()) ) ); 
		long yesterdayMidnight=0;
		if(newCreationTime.equals("вчера 00:00:00")){
			long currTimeInMs = System.currentTimeMillis();
			long inMs24hrs = 24* 60*60 * 1000L;
			yesterdayMidnight = currTimeInMs -(currTimeInMs % inMs24hrs)-inMs24hrs;
		}
		HashMap<String,Object> newCreationDate = new HashMap<String,Object>();
		newCreationDate.put( "BoundToResourceTime",new Date(yesterdayMidnight));
		com.t1.core.mongodb.MongoConnection.updateCollection("DeviceHistory",filter,newCreationDate);
    }



}