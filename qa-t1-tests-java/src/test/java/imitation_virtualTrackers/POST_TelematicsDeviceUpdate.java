package imitation_virtualTrackers;

import static com.mongodb.client.model.Filters.eq;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.SharedContainer;
import com.t1.core.api.Employee;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import cucumber.api.java.en.Then;

public class POST_TelematicsDeviceUpdate extends AbstractClass{

	
	@Then("^Привязать [С,с]отрудника к ТМУ$")
	public void addEmplToDevice() {
		Employee employeeFromPrevStep = (Employee) SharedContainer.getLastObjectFromSharedContainer("employees");
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		telematics_device.setResourceId(employeeFromPrevStep.getEmployeeId());
		telematics_device.setResourceType("Employee");
        telematics_device.putUrlParameters("resourceId", telematics_device.getResourceId());
        telematics_device.putUrlParameters("resourceType", telematics_device.getResourceType());
		
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
	}
	
	@Then("^Привязать [С,с]отрудника без ТМУ к ТМУ$")
	public void addEmplWithOutDeviceToDevice() {
		Employee employeeFromPrevStep = null;
		List<Employee> employeeList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
		for(Employee employee : employeeList ){
			if(employee.getDeviceId()==null){
				employeeFromPrevStep = employee;
				break;
			}
		}
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		telematics_device.setResourceId(employeeFromPrevStep.getEmployeeId());
		telematics_device.setResourceType("Employee");
        telematics_device.putUrlParameters("resourceId", telematics_device.getResourceId());
        telematics_device.putUrlParameters("resourceType", telematics_device.getResourceType());
		
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
	}

		
	@Then("^Отредактировать текстовые поля ТМУ$")
	public void editDeviceTextFields() {
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		JSONObject initJson = new InitDefValues().initVirtualTracker(getProperty("stdPattern"));  //stdPattern

		telematics_device.setType(initJson.get("type").toString());
		telematics_device.setModel(initJson.get("model").toString());
		telematics_device.setManufacturer(initJson.get("manufacturer").toString());
		telematics_device.setSimCard(initJson.get("simCard").toString());

        telematics_device.putUrlParameters("type", telematics_device.getType());
        telematics_device.putUrlParameters("model", telematics_device.getModel());
        telematics_device.putUrlParameters("manufacturer", telematics_device.getManufacturer());
        telematics_device.putUrlParameters("simCard", telematics_device.getSimCard());
		if (telematics_device.getResourceId() != null){
	        telematics_device.putUrlParameters("resourceId", telematics_device.getResourceId());
	        telematics_device.putUrlParameters("resourceType", telematics_device.getResourceType());
		}
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
	}
	
	@Then("^Привязать ТС к ТМУ$")
	public void addVehToDevice() {
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		telematics_device.setResourceId(vehicleFromPrevStep.getVehicleId());
		telematics_device.setResourceType("Vehicle");
        telematics_device.putUrlParameters("resourceId", telematics_device.getResourceId());
        telematics_device.putUrlParameters("resourceType", telematics_device.getResourceType());
		
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
	}
	
	@Then("^Привязать ТС без ТМУ к ТМУ$")
	public void addVehicleWithOutDeviceToDevice() {
		Vehicle vehicleFromPrevStep = null;
		List<Vehicle> vehicleList = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all","vehicles");
		for(Vehicle vehicle : vehicleList ){
			if(vehicle.getDeviceId()==null){
				vehicleFromPrevStep = vehicle;
				break;
			}
		}
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		telematics_device.setResourceId(vehicleFromPrevStep.getVehicleId());
		telematics_device.setResourceType("Vehicle");
        telematics_device.putUrlParameters("resourceId", telematics_device.getResourceId());
        telematics_device.putUrlParameters("resourceType", telematics_device.getResourceType());
		
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
	}
	
	@Then("^Отвязать ТС от ТМУ$")
	public void removeVehFromDevice() {
		removeResourceFromDevice();
	}
	//через редактирование ТМУ1 привязать к ТМУ1 ТС
	@Then("^Привязать ТС с ТМУ к ТМУ$")
	public void updateDevice_AddRelationToVehicleThatAlreadyHasDevice()  {
		Vehicle vehicleFromPrevStep = null;
		List<Vehicle> vehicleList = (List<Vehicle>) SharedContainer.getObjectsFromSharedContainer("all","vehicles");
		for(int i = vehicleList.size()-1; i>=0;  i-- ){
			if(vehicleList.get(i).getDeviceId()!=null && vehicleList.get(i).getDeviceId()!="000000000000000000000000"){ //looking for vehicle already assigned to device! 
				vehicleFromPrevStep = vehicleList.get(i);
				break;
			}
		}
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers").get(0);
		telematics_device.setResourceId(vehicleFromPrevStep.getEmployeeId());
		telematics_device.setResourceType("Vehicle");
        telematics_device.putUrlParameters("resourceId", telematics_device.getResourceId());
        telematics_device.putUrlParameters("resourceType", telematics_device.getResourceType());
		
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
	}
	
	//через редактирование ТМУ1 привязать к ТМУ1 Сотрудника2
	@Then("^Привязать Сотрудника с ТМУ к ТМУ$")
	public void updateDevice_AddRelationToEmplThatAlreadyHasDevice()  {
		Employee employeeFromPrevStep = null;
		List<Employee> employeeList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all","employees");
		for(int i = employeeList.size()-1; i>=0;  i-- ){
			if(employeeList.get(i).getDeviceId()!=null && employeeList.get(i).getDeviceId()!="000000000000000000000000"){ //looking for empl already assigned to device! 
				employeeFromPrevStep = employeeList.get(i);
				break;
			}
		}
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers").get(0);
		telematics_device.setResourceId(employeeFromPrevStep.getEmployeeId());
		telematics_device.setResourceType("Employee");
        telematics_device.putUrlParameters("resourceId", telematics_device.getResourceId());
        telematics_device.putUrlParameters("resourceType", telematics_device.getResourceType());
		
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
	}
	

	//через редактирование ТМУ1 привязать к ТМУ1 ТС
	@Then("^Привязать ТС (\\d+) с ТМУ (\\d+) к ТМУ (\\d+)$")
	public void updateDevice_AddRelationToVehicleThatAlreadyHasDevice(int vehicleIndex, int oldDeviceIndex, int newDeviceIndex)  {
		Vehicle vehicleFromPrevStep = ( Vehicle ) SharedContainer.getObjectsFromSharedContainer("all","vehicles").get(vehicleIndex-1);
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers").get(newDeviceIndex-1);
		telematics_device.setResourceId(vehicleFromPrevStep.getVehicleId());
		telematics_device.setResourceType("Vehicle");
        telematics_device.putUrlParameters("resourceId", telematics_device.getResourceId());
        telematics_device.putUrlParameters("resourceType", telematics_device.getResourceType());
		
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
		vehicleFromPrevStep.setDeviceId(telematics_device.getTrackerId());
		vehicleFromPrevStep.updateInSharedContainer();
	}
	
//	//через редактирование ТМУ1 привязать к ТМУ1 Сотрудника2
	@Then("^Привязать Сотрудника (\\d+) с ТМУ (\\d+) к ТМУ (\\d+)$")
	public void updateDevice_AddRelationToEmplThatAlreadyHasDevice(int emplIndex, int oldDeviceIndex, int newDeviceIndex)  {
		Employee employeeFromPrevStep = (Employee) SharedContainer.getObjectsFromSharedContainer("all","employees").get(emplIndex-1);
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getObjectsFromSharedContainer("all","virtualTrackers").get(newDeviceIndex-1);
		telematics_device.setResourceId(employeeFromPrevStep.getEmployeeId());
		telematics_device.setResourceType("Employee");
        telematics_device.putUrlParameters("resourceId", telematics_device.getResourceId());
        telematics_device.putUrlParameters("resourceType", telematics_device.getResourceType());
		
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
		
		employeeFromPrevStep.setDeviceId(telematics_device.getTrackerId());
		employeeFromPrevStep.updateInSharedContainer();
	}

	@Then("^Отвязать Сотрудника от ТМУ$")
	public void removeEmplFromDevice() {
		removeResourceFromDevice();
	}
	
	public void removeResourceFromDevice() {
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		telematics_device.setResourceId(null);
		telematics_device.setResourceType(null);
        telematics_device.getUrlParameters().remove("resourceId");
        telematics_device.getUrlParameters().remove("resourceType");
		
		telematics_device.updateVirtualTracker();
		telematics_device.updateInSharedContainer();
		
		//TODO: need to add here vehicle\Employee update in container
	}
	
    public void updateBoundToResourceTimeDevicetoObject( int numOfDays) {
    	TelematicVirtualTracker deviceFromPrevStep = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		Bson filter = Filters.and(eq("DeviceId",new ObjectId(deviceFromPrevStep.getTrackerId()) ) ); 
		long currTimeInMs = System.currentTimeMillis();
		long inMs24hrs = 24 * 60 * 60 * 1000L;
		long newCreationDate = currTimeInMs - (numOfDays * inMs24hrs);
		HashMap<String,Object> newCreationDateObj = new HashMap<String,Object>();
		newCreationDateObj.put( "BoundToResourceTime",new Date(newCreationDate));
		com.t1.core.mongodb.MongoConnection.updateCollection("DeviceHistory",filter,newCreationDateObj);
		deviceFromPrevStep.setBoundTime(newCreationDate);
		deviceFromPrevStep.updateInSharedContainer();
    }
	
    @Then("^Сдвинуть дату создания связи ТС и ТМУ на (\\d+) дней назад, считая от текущей даты$")
    public void updateBoundToResourceTimeDevicetoVehicle( int numOfDays) {
    	updateBoundToResourceTimeDevicetoObject(numOfDays);
    }
    
    @Then("^Сдвинуть дату создания связи Сотрудника и ТМУ на (\\d+) дней назад, считая от текущей даты$")
    public void updateBoundToResourceTimeDevicetoEmployee( int numOfDays) {
    	updateBoundToResourceTimeDevicetoObject(numOfDays);
    }
}
