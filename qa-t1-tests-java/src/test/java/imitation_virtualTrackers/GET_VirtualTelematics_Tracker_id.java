package imitation_virtualTrackers;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.api.TelematicVirtualTracker;

import cucumber.api.java.en.Then;

public class GET_VirtualTelematics_Tracker_id extends AbstractClass {
	public TelematicVirtualTracker tracker;

	@Then("^получить информацию о случайном ТМУ$")
	public void getRandomTrackerInfoById() {
		log.debug("Start>> getRandomTrackerInfoById");

		try {
			JSONObject telematics_device = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("json_telematics_device");
			// String telematics_deviceId =
			tracker = new TelematicVirtualTracker(true);
			tracker.setTrackerId(ReplyToJSON.extractPathFromJson(telematics_device, "$._id.$oid"));
			tracker.setTrackerCode(ReplyToJSON.extractPathFromJson(telematics_device, "$.Code"));

			tracker.getVirtualTrackerById(tracker.getTrackerId());
		} catch (Exception e) {
			log.error(ERROR,e);
		}


	}


    @Then("^получить информацию о созданном в предыдущем шаге ТМУ$")
    public void getLastCreatedTrackerInfoById() {
        log.debug("Start>> getLastCreatedTrackerInfoById");
        TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
        telematics_device.getVirtualTrackerById(telematics_device.getTrackerId());
        log.debug("End of Test");
    }
}