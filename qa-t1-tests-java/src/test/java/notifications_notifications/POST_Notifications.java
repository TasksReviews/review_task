package notifications_notifications;

import org.bson.Document;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SharedContainer;
import com.t1.core.api.Notification;
import com.t1.core.api.NotificationEvent;
import com.t1.core.api.NotificationRule;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;
import com.t1.core.mongodb.InsertNotification;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class POST_Notifications extends AbstractClass{
	public Notification notification; 
	public NotificationEvent event; 
	
//	public void generate(String reportType, String reportCriteriaParams, String resourceType, final String resourcesFilterInclude, String dateFrom, String dateTo, 
//			  String graphTimeUnit, String dataTimeUnit, final String geozoneIds,  Boolean isDeepDetailed){
//log.debug("Start>> report test");
//report = new Report( reportType, reportCriteriaParams, resourceType, resourcesFilterInclude, dateFrom, dateTo, graphTimeUnit, dataTimeUnit, geozoneIds,  isDeepDetailed);
//
//JSONObject createdvehicle = (JSONObject) report.requestReportGeneration();
////	SharedContainer.setContainer("vehicles", (new ArrayList(){{add(vehicle);}}));
//log.debug("end of report");
////vehicle.deleteVehicle(vehicle.vehicleId);
//}"<timeFrom>","<timeTo>" по заданному устройству "<Deviceid>"

//TODO: this step should not be here! 
	@Then("^запросить список событий и сгенерированных по ним оповещений за выбранный период \"([^\"]*)\",\"([^\"]*)\" по объектам \"([^\"]*)\"$")
	public void roadEventsGenerator(String dateFrom, String dateTo, String includeObjects) {
		
		log.debug("Start>> roadEventsGenerator test from="+dateFrom+", till="+dateTo+"=="+includeObjects);
		
//		event = new Event( deviceid, timeFrom, timeTo);
		
		TelematicVirtualTracker telematics_device = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers");
		
		NotificationEvent events = new NotificationEvent();
		NotificationRule ruleFromPrevStep = (NotificationRule) SharedContainer.getLastObjectFromSharedContainer("notificationRules");
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		String include = vehicleFromPrevStep.getVehicleId();
		JSONObject generatorReply = events.requestRoadEventsGenerator (telematics_device.getTrackerId(),include, ruleFromPrevStep, dateFrom,dateTo);
		
		
		
	//	ArrayList<JSONObject> createdEvents =  event.requestRoadEventsGenerator();

	}
	
	@Then("^запросить список событий используя фильтр \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void getByFilter(String skip, String limit, String dateFrom, String dateTo, String events, String groups, String include, String exclude){

		log.debug("Start>> notif test by filter="+dateFrom+"=="+dateTo+"=="+events);
		Vehicle vehicleFromPrevStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
		include = vehicleFromPrevStep.getVehicleId();
		notification = new Notification( skip, limit, dateFrom, dateTo, events, groups, include, exclude);
		
		log.debug("requestNotificationsByFilter");
		JSONObject creatednotif = (JSONObject) notification.requestNotificationsByFilter();

	}
	
	@Given("^добавить \"([^\"]*)\" оповещений в БД \"([^\"]*)\"$")
	public static Document pre (String noOfNotifToInsert, String notificationEventType){
		int numberOfNotificationsToInsert = Integer.parseInt(noOfNotifToInsert);
		for (int i = 0 ;i<numberOfNotificationsToInsert;i++){
		InsertNotification.insertDocument(notificationEventType);//, null, null, null, null);//.prepareDoc (input);
		}
		return null;
	}
	
//	/Notifications/Details/{id}?

}