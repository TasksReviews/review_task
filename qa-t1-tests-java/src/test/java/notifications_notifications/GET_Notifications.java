package notifications_notifications;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.simple.JSONObject;

import com.google.common.annotations.Beta;
import com.t1.core.AbstractClass;
import com.t1.core.api.Notification;
import com.t1.core.mongodb.GetNotifications;

import cucumber.api.java.en.Then;

public class GET_Notifications extends AbstractClass{
	public Notification notification; 
	
	@Then("^запросить список оповещений старше чем \"([^\"]*)\"$")
	public void lastDate(String date) {
		
		log.debug("Start>> notif test="+date);
		
		notification = new Notification();
		notification.setDateFrom(date);
		notification.setDateTo(getCurrentUTC3DateTime());
//		for(int i =0; i<50;i++){
		JSONObject receivedNotifications = (JSONObject) notification.getNotificationsLaterThanDate();
//		}


	}
	
	@Then("^запросить список оповещений за последнюю неделю$")
	public void lastWeek() {
		
		 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
		 Date date = new Date(System.currentTimeMillis() - 7*24*3600 * 1000); //cur-7days
		
		log.debug("Start>> NotificationsLaterThanDate="+df.format(date));
		notification = new Notification();
		notification.setDateFrom(df.format(date));
		notification.setDateTo(getCurrentUTC3DateTime());
		JSONObject receivedNotifications = (JSONObject) notification.getNotificationsLaterThanDate();

	}

	@Then("^запросить подробную информацию о оповещении \"([^\"]*)\"$")
	public void details_id(String notificationId) {
		
		log.debug("Start>> notif test="+notificationId);
		
		notification = new Notification();
		JSONObject receivedNotifications = (JSONObject) notification.getNotificationsDetailsId(notificationId);

	}

	
	//Details/{id}
@Beta
	@Then("^запросить подробную информацию о последнем оповещении$")
	public void getLastNotif_details() {
		//TODO: Get if from DB:
		String authAccId = getCurAuthAcc("authorizedAccount_t1client").get("id").toString();
		
		JSONObject mongoNotification = GetNotifications.getLastNotificationInfo(authAccId);
		log.debug("Start>> notif test= last notif in db");
		
		notification = new Notification();
		JSONObject receivedNotifications = (JSONObject) notification.getNotificationsDetailsId(mongoNotification.get("_id.$oid").toString());

	}
	
}
