Feature: POST_NotificationRulesTurn_Off_On

	@QA-2119 @tone
	Scenario Outline: POST_NotificationRulesTurn_Off_On
					Given авторизоваться используя стандартный аккаунт
					Then создать новое правило трижды, согласно параметрам из таблицы: "<roadEventType>","<inputResFilterGroups>","<inputResFilterInclude>","<inputResFilterExclude>","<daysOfWeek>","<timeFrom>","<timeTo>","<title>","<isImportant>","<showOnDisplay>","<roadEventArgumentsType>","<limitationName>","<limitationValue>","<limitationName2>","<limitationValue2>"
					Then запросить выключение созданных правил оповещений				
					Then запросить включение созданных правил оповещений																									
		
			Examples:
			|roadEventType	|inputResFilterGroups|inputResFilterInclude	  |inputResFilterExclude|daysOfWeek 		|timeFrom |timeTo  | title				|isImportant| showOnDisplay|roadEventArgumentsType							   			|limitationName|limitationValue			  |limitationName2|limitationValue2|
			|GeozoneEntrance|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_GeozoneEntrance	|false      | false        |Tone.Core.Data.GeozonesEntranceArguments, Tone.Core			|geozones	   |[59311a785532f43284d805eb]|				  |				   |
			|GeozoneExit	|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_GeozoneExit		|false      | false        |Tone.Core.Data.GeozonesEntranceArguments, Tone.Core			|geozones	   |[59311a785532f43284d805eb]|				  |				   |
			|GeozoneStanding|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_GeozoneStanding	|false      | false        |Tone.Core.Data.GeozonesStandingArguments, Tone.Core			|geozones	   |[59311a785532f43284d805eb]|duration		  |00:25:00		|
			|UserSpeedlimit |    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_UserSpeedLimit	|false      | false        |Tone.Core.Data.UserSpeedlimitArguments, Tone.Core 			|speedlimit	   |100						  |				  |				|
			|StopEvent		|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_StopEvent		|false      | false        |Tone.Core.Data.StopEventArguments, Tone.Core	   			|duration  	   |00:25:00		          |				  |				|
			|IdleEvent		|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_IdleEvent		|false      | false        |Tone.Core.Data.IdleEventArguments, Tone.Core	   			|duration  	   |00:01:00				  |				  |				|
			|TrafficRulesSpeedLimit|			 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_TrafficRulesSpeedLimit|false| false        |Tone.Core.Data.TrafficRulesSpeedLimitArguments, Tone.Core	|overspeed	   |100						  |				  |				|
			|SharpTurn		|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_SharpTurn		|false      | false        |Tone.Core.Data.SharpMoveArguments, Tone.Core	   			|acceleration  |2						  |				  |				|
			|SharpSpeedup	|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_SharpSpeedup	|false      | false        |Tone.Core.Data.SharpMoveArguments, Tone.Core	  		   	|acceleration  |2						  |				  |				|
			|SharpBraking	|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_SharpBraking	|false      | false        |Tone.Core.Data.SharpMoveArguments, Tone.Core	  		   	|acceleration  |2						  |				  |				|
			|SharpTurnSpeedLimit|				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_SharpTurnSpeedLimit|false   | false        |Tone.Core.Data.SharpTurnSpeedLimitArguments, Tone.Core	   	|speedlimit	   |80						  |				  |				|
			|Refueling		|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_Refueling		|false      | false        |Tone.Core.Data.RefuelingArguments, Tone.Core	   			|amount  	   |3						  |				  |				|
			|Discharge		|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_Discharge		|false      | false        |Tone.Core.Data.DischargeArguments, Tone.Core	   			|amount  	   |3						  |				  |				|
			|FuelConsumption|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_FuelConsumption	|false      | false        |Tone.Core.Data.FuelConsumptionExceedanceArguments, Tone.Core|amount  	   |15						  |				  |				|
			|PanicButton	|    				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_PanicButton		|false      | false        |   		   													|  			   |						  |				  |				|
			|TelematicDeviceMechanismOn|		 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_MileageExceedance|false     | false        |Tone.Core.Data.TelematicDeviceMechanismTriggeringArguments, Tone.Core|telematicDeviceMechanism|Hood	  |				  |				|
			|TelematicDeviceMechanismOff|		 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_MileageExceedance|false     | false        |Tone.Core.Data.TelematicDeviceMechanismTriggeringArguments, Tone.Core|telematicDeviceMechanism|Trunk  |				  |				|
			|MileageExceedance|  				 |593829b15532f71548deb095|						|Monday,Friday	    |01:02:03 |23:59:59|JFW_MileageExceedance|false     | false        |Tone.Core.Data.MileageExceedanceArguments, Tone.Core		|mileage  	   |500000					  |		  		  |				|
			