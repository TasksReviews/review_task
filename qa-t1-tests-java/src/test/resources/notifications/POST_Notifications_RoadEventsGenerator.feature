Feature: POST_Notifications_RoadEventsGenerator

	@QA-2129 @tone
	Scenario Outline: POST_Notifications_RoadEventsGenerator
		Given авторизоваться используя стандартный аккаунт
		Given создать новую геозону типа Polygon для правил связанных с геозонами: "<roadEventType>"
		Given авторизоваться используя аккаунт с правами разработчика
		Given cоздать новое ТМУ	
		Then cоздать новое ТС с минимальным количеством полей и привязать к созданному ТМУ
		Then создать новое правило согласно параметрам из таблицы: "<roadEventType>","<inputResFilterGroups>","<inputResFilterExclude>","<daysOfWeek>","<timeFrom>","<timeTo>","<title>","<isImportant>","<showOnDisplay>","<roadEventArgumentsType>","<limitationName>","<limitationValue>","<limitationName2>","<limitationValue2>"
		Given подготовить тестовый трек для созданного ТМУ для срабатывания созданного правила, критерий "<limitationName>" значение="<limitationValue>"
		Then запросить список событий и сгенерированных по ним оповещений за выбранный период "<dateFrom>","<dateTo>" по объектам "<inputResFilterInclude>"
				Examples:
	|roadEventType	|inputResFilterGroups|inputResFilterInclude	  |inputResFilterExclude|daysOfWeek 							 |timeFrom |timeTo  | title			   |isImportant| showOnDisplay|roadEventArgumentsType							   					|limitationName			|limitationValue|limitationName2  |limitationValue2|skip|limit|dateFrom			  |dateTo			  |
#	|UserSpeedlimit |    				 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_UserSpeedLimit|false      | false        |Tone.Core.Data.UserSpeedlimitArguments, Tone.Core 					|speedlimit	   			|28				|				  |				   |0	|10	  |за неделю		  |за неделю		  |
#	|TelematicDeviceMechanismOn|		 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_MechanismOn   |false      | false        |Tone.Core.Data.TelematicDeviceMechanismTriggeringArguments, Tone.Core|telematicDeviceMechanism|Hood	  		|				  |				   |0	|10	  |за сутки			  |за сутки			  |
#	|TelematicDeviceMechanismOff|		 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_MechanismOff  |false      | false        |Tone.Core.Data.TelematicDeviceMechanismTriggeringArguments, Tone.Core|telematicDeviceMechanism|Ignition  	|				  |				   |0	|10	  |за сутки			  |за сутки			  |
#	|SharpSpeedup	|    				 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_SharpSpeedup  |false      | false        |Tone.Core.Data.SharpMoveArguments, Tone.Core	  		   	            |acceleration           |0.2			|				  |				   |0	|10	  |за сутки			  |за сутки			  |
#	|SharpBraking	|    				 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_SharpBraking  |false      | false        |Tone.Core.Data.SharpMoveArguments, Tone.Core	  		   				|acceleration  			|-0.3			|				  |				   |0	|10	  |за сутки			  |за сутки			  |
#	|TrafficRulesSpeedLimit|			 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_TrafficRulesSpeedLimit|false| false      |Tone.Core.Data.TrafficRulesSpeedLimitArguments, Tone.Core			|overspeed	   			|100			|				  |					|0	|10	  |за сутки			  |за сутки			  |
#	|SharpTurn		|    				 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_SharpTurn		|false      | false       |Tone.Core.Data.SharpMoveArguments, Tone.Core	   						|acceleration  			|2				|				  |					|0	|10	  |за сутки			  |за сутки			  |
#	|PanicButton	|    				 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_PanicButton		|false      | false   |   		   															|  			   			|				|				  |					|0	|10	  |за сутки			  |за сутки			  |
#	|GeozoneEntrance|    				 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_GeozoneEntrance	|false      | false   |Tone.Core.Data.GeozonesEntranceArguments, Tone.Core					|geozones	   			|newGeozone		|				  |				   |0	|10	  |за сутки			  |за сутки			  |
#	|GeozoneExit	|    				 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_GeozoneExit		|false      | false   |Tone.Core.Data.GeozonesEntranceArguments, Tone.Core					|geozones	   			|newGeozone		|				  |				   |0	|10	  |за сутки			  |за сутки			  |
#	|GeozoneStanding|    				 |593829b15532f71548deb095|						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_GeozoneStanding	|false      | false   |Tone.Core.Data.GeozonesStandingArguments, Tone.Core					|geozones	   			|newGeozone		|duration		  |00:08:00			|0	|10	  |за сутки			  |за сутки			  |
#	|StopEvent		|    				 |						  |						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_StopEvent		|false      | false       |Tone.Core.Data.StopEventArguments, Tone.Core	   						|duration  	   			|00:25:00		|					|		  		|0   |10	 |за сутки 			|за сутки|
	|IdleEvent		|    				 |						  |						|Monday,Tuesday,Wednesday,Thursday,Friday|01:02:03 |23:59:59|JFW_IdleEvent		|false      | false       |Tone.Core.Data.IdleEventArguments, Tone.Core	   						|duration  	   			|00:01:00		|					|		  		|0   |10	 |за сутки 			|за сутки|
			