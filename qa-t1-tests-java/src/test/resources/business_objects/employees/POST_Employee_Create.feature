Feature: POST_Employee_Create

	
	@QA-1785 @QA-444 @tone
	Scenario: POST_Employee_Create_minFields
			 Given авторизоваться используя аккаунт с правами администратора
			 Then создать нового сотрудника с минимальным количеством полей и случайными значениями	

	
	@QA-1786 @QA-444 @tone
	Scenario: POST_Employee_Create_withEmptyFields
			 Given авторизоваться используя аккаунт с правами администратора
			 Then создать нового сотрудника с полями (Middlename, Description, GeoZoneIds, DeactivationReason) содержащими пустые значения	

	
	@QA-1787 @QA-444 @tone
	Scenario: POST_Employee_Create_CyrilicOnlyInFields
			 Given авторизоваться используя аккаунт с правами администратора
			 Then создать нового сотрудника с полями содержащими кирилицу: шаблон
				|шаблон                 |
				|ЙцукёФяжщчиэтьбюъ-_().,|		

	
	@QA-1788 @QA-444 @tone
	Scenario: POST_Employee_Create_NumbersOnlyInFields
			 Given авторизоваться используя аккаунт с правами администратора
			 Then создать нового сотрудника с полями содержащими цифры: шаблон
				|шаблон    |
				|1234567890|	

	
	@QA-1789 @QA-444 @tone
	Scenario: POST_Employee_Create_SpecialCharachtersInFields
			 Given авторизоваться используя аккаунт с правами администратора
			 Then создать нового сотрудника с полями содержащими спец символы: шаблон
				|шаблон    						       |
				|\~!@#$%^&*()_+}{\":?><\`-[];\'\/.,\№\||		

	
	@QA-1790 @QA-444 @tone
	Scenario: POST_Employee_Create_type_eq_Сourier_and_Status_eq_Contract
			 Given авторизоваться используя аккаунт с правами администратора
			 Then создать нового сотрудника с полями Type = Сourier и Status = Contract	
			 
	@QA-1949 @QA-444 @tone
	Scenario: POST_Employee_Create_allFields
			 Given авторизоваться используя стандартный аккаунт
			 Given cоздать новое ТМУ
			 Given получить случайную геозону из БД
			 Given получить случайную группу из БД
			 Given cоздать новое ТС с минимальным количеством полей и случайными значениями
			 Given созсоздать новое подразделение с минимальным количеством полей, без сотрудников
			 Then создать нового сотрудника с полным количеством полей и случайными значениями			 
			 