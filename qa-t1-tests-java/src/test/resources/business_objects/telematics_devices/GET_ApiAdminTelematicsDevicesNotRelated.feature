Feature: GET_ApiAdminTelematicsDevices[relationStatus=NotRelated]

  @QA-2375
  Scenario: GET_ApiAdminTelematicsDevices[relationStatus=NotRelated]
    Given Залогиниться в панели администратора под пользователем с ролью девелопера
#Создать нового Customer и запомнить customerId
    And Добавить нового клиента
    And Создать 3 новых ТМУ без привязки к клиенту
    And Создать 2 новых ТМУ с привязкой к клиенту
#Вызвать метод /Api/Admin/Telematics/Devices?relationStatus=NotRelated
    When Запросить список всех устройств, непривязанных к клиенту
#Проверить, что метод вернул все 3 созданных ТМУ без привязки к клиенту
#Проверить, что метод не вернул 2 созданных ТМУ с привязкой к клиенту, созданному ранее
#Проверить все поля для каждого из 3 ТМУ - сравнить с MongoDb
#Проверить, что у всех полученных ТМУ isCustomerBound = false, CustomerId = "000000000000000000000000"
    Then Получены только устройства, непривязанные к клиенту