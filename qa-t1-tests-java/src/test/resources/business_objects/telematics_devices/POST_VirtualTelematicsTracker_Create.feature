Feature: POST_VirtualTelematicsTracker_Create

	@QA-1973 @tone
	Scenario: POST_VirtualTelematicsTracker_create
			 Given авторизоваться используя стандартный аккаунт
			 Then cоздать новое ТМУ	

	
	@QA-1974 @tone
	Scenario: POST_VirtualTelematicsTracker_create_cyrilicValues
			 Given авторизоваться используя стандартный аккаунт
			 Then создать новое ТМУ с полями содержащими кирилицу: шаблон
						|шаблон                 |
						|ЙцукёФяжщчиэтьбюъ-_().,| 	

	
	@QA-1975 @tone
	Scenario: POST_VirtualTelematicsTracker_create_numbersInValues
			 Given авторизоваться используя стандартный аккаунт
			 Then создать новое ТМУ с полями содержащими цифры: шаблон
						|шаблон    |
						|1234567890|			

	
	@QA-1976 @tone
	Scenario: POST_VirtualTelematicsTracker_create_SpecialCharachtersInFields
			 Given авторизоваться используя стандартный аккаунт
			 Then создать новое ТМУ с полями содержащими спец символы: шаблон 
			 	|шаблон    						       |
				|\~!@#$%^&*()_+}{\":?><\`-[];\'\/.,\№\||					 