Feature: GET_ApiAdminTelematicsDevices[customerId]

  @QA-2653
  Scenario: GET_ApiAdminTelematicsDevices[customerId]
    Given Залогиниться в панели администратора под пользователем с ролью девелопера
#Создать нового Customer и запомнить customerId
    And Добавить нового клиента
    And Создать 3 новых ТМУ с привязкой к клиенту
    And Создать 2 новых ТМУ без привязки к клиенту
#Вызвать метод /Api/Admin/Telematics/Devices?customerId=<customerId>
    When Запросить список всех устройств, привязанных к клиенту
#Проверить, что метод вернул все 3 созданных ТМУ с привязкой к клиенту
#Проверить, что метод не вернул 2 созданных ТМУ без привязки к клиенту
#Проверить все поля для каждого из 3 ТМУ - сравнить с MongoDb
#Проверить, что у всех полученных ТМУ isCustomerBound = true, customerId == <customerId>, totalItem == 3
    Then Получены только устройства, привязанные к клиенту
