Feature: GET_ApiAdminTelematicsDevices
  @QA-2373
  Scenario: GET_ApiAdminTelematicsDevices_ThreeMore
    Given Залогиниться в панели администратора под пользователем с ролью девелопера
    And Создать 3 новых ТМУ
#Вызвать метод /Api/Admin/Telematics/Devices без параметров
    When Запросить информацию по всем устройствам
#Проверить, что метод вернул все 3 созданных ТМУ
#Проверить все поля для каждого из полученных ТМУ - сравнить с MongoDb
#Проверить, что pageNumber = 1, pageSize = 25 (значения по умолчанию)
    Then Получены все созданные в предыдущих шагах ТМУ