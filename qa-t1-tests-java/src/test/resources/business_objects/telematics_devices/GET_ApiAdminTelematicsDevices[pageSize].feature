Feature: GET_ApiAdminTelematicsDevices[pageSize]

  @QA-2594
  Scenario Outline: GET_ApiAdminTelematicsDevices[pageSize]
Given Залогиниться в панели администратора под пользователем с ролью девелопера
#Создать нового Customer и запомнить customerId
And Добавить нового клиента
And Создать 6 новых ТМУ
#Выполнить запрос о кол-ве ТМУ в коллекции Devices
When Запросить информацию о кол-ве ТМУ в MongoDb
#Вызвать метод /Api/Admin/Telematics/Devices?pageSize=<pageSize> с разными значениями параметра pageSize
And Запросить информацию по "<pageSize>" устройствам на странице
#Проверить, что метод возвращает правильное значение totalPage
#int totalPages = device_count%pageSize !== 0 ? device_count/pageSize + 1 : device_count/pageSize
#Проверить, что items.length == <pageSize> (pageNumber = 1 по умолчанию)
Then Проверить, что параметр totalPage зависит от pageSize

Examples:
|pageSize|
|    1   |
|    2   |
|    99  |
|    100 |