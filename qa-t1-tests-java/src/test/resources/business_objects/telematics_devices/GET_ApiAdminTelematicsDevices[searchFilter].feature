Feature: GET_ApiAdminTelematicsDevices[searchFilter]

  @QA-2644
  Scenario Outline: GET_ApiAdminTelematicsDevices[searchFilter]
    Given Залогиниться в панели администратора под пользователем с ролью девелопера
    Given Создать 6 новых ТМУ с параметрами
      | code   | type  | imei            | model  | manufacturer | simcard      |
      | 111111 | type1 | 345948373298579 | model1 | spaceteam    | +74952324444 |
      | 222222 | type2 | 346547657673423 | model2 | galileo      | +73465657657 |
      | 333333 | type1 | 436945767878342 | model3 | galileo      | +74365565324 |
      | 444444 | type2 | 576587682332343 | model1 | spaceteam    | +73485857645 |
      | 555555 | type1 | 876587980093452 | model2 | galileo      | +72322433542 |
      | 666666 | type2 | 547687987094332 | model2 | spaceteam    | +74365465632 |
     #Можно создать виртуальные трекеры со всеми указанными полями, кроме поля type. Оно всегда будет VirtualTracker.
     #После создания у всех ТМУ необходимо изменить поле type
     #Вызвать метод /Api/Admin/Telematics/Devices?searchFilter=<searchFilter> с разными значениями параметра searchFilter
    When Найти ТМУ, у которых поле "<field>" = "<searchFilter>"
     #Проверить, что в результате выполнения запроса ids созданных в предыдущем шаге ТМУ, удовлетворяющих условиям поиска, содержатся в ответе метода API
     #Проверить, что одно из полей (code, type, imei, model, manufacturer, simcard) всех найденных ТМУ содержит строку <searchFilter>
     #Проверить, что поле <field> = <searchFilter> для всех созданных ТМУ, удовлетворяющим условиям поиска
     #Проверить, что регистр не влияет на результаты поиска
    Then Получен список устройств, удовлетворяющих критерию поиска

    Examples:
      | searchFilter    | field        |
      | 111111          | code         |
      | type2           | type         |
      | TYPE2           | type         |
      | 576587682332343 | imei         |
      | model2          | model        |
      | MODEL2          | model        |
      | galileo         | manufacturer |
      | GALILEO         | manufacturer |
      | +72322433542    | simcard      |
      | 72322433542     | simcard      |