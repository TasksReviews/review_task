Feature: POST_Vehicle_Add

  @QA-1957
  Scenario: POST_Vehicle_Add_minFields
    Given авторизоваться используя стандартный аккаунт
    Then cоздать новое ТС с минимальным количеством полей и случайными значениями


  @QA-1958
  Scenario: POST_Vehicle_Add_minFields_cyrilicValues
    Given авторизоваться используя стандартный аккаунт
    Then создать новое ТС с минимальным количеством полей содержащими кирилицу: шаблон
      | шаблон                  |
      | ЙцукёФяжщчиэтьбюъ-_()., |


  @QA-1959
  Scenario: POST_Vehicle_Add_minFields_numbersInValues
    Given авторизоваться используя стандартный аккаунт
    Then создать новое ТС с минимальным количеством полей содержащими цифры: шаблон
      | шаблон     |
      | 1234567890 |


  @QA-1960
  Scenario: POST_Vehicle_Add_minFields_SpecialCharachtersInFields
    Given авторизоваться используя стандартный аккаунт
    Then создать новое ТС с минимальным количеством полей содержащими спец символы: шаблон
      | шаблон                                 |
      | \~!@#$%^&*()_+}{\":?><\`-[];\'\/.,\№\| |


  @QA-1961
  Scenario: POST_Vehicle_Add_minFields_typeEqCar
    Given авторизоваться используя стандартный аккаунт
    Then cоздать новое ТС с минимальным количеством полей и типом Car


  @QA-1962
  Scenario: POST_Vehicle_Add_allFields
    Given авторизоваться используя стандартный аккаунт
    Given получить случайную геозону из БД
    Given получить случайное подразделение из БД
    Given cоздать новое ТМУ
    Given получить случайную группу из БД
    Given создать нового сотрудника с минимальным количеством полей и случайными значениями
    Then cоздать новое ТС с полным количеством полей и случайными значениями

  @QA-
  Scenario: POST_Vehicle_Add_Device(not in testSet)
    Given авторизоваться используя стандартный аккаунт
    Given cоздать новое ТМУ
    Given cоздать новое ТС с минимальным количеством полей и привязать к созданному ТМУ

  @QA-2004 @tone
  Scenario: POST_Vehicle_Add_minFields_vehicleTypes
    Given авторизоваться используя стандартный аккаунт
    Then cоздать новое ТС с минимальным количеством полей с заданным типом: тип
      | тип            |
      | Bus            |
      | Sedan          |
      | Hatchback      |
      | Coupe          |
      | StationWagon   |
      | Van            |
      | SoftRoader     |
      | SportUtility   |
      | Cabriolet      |
      | Pickup         |
      | AutoTruck      |
      | Electrobus     |
      | Tramway        |
      | Microbus       |
      | Bulldozer      |
      | FieldEngine    |
      | TrailerCar     |
      | Ambulance      |
      | FireTruck      |
      | VehicleCarrier |
      | Stepthru       |
      | Scooter        |
      | Bike           |
      | Tricycle       |
      | QuadBike       |
      | Locomotive     |