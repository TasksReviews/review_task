Feature: GET_GeozoneGetAll

	@QA-2112 @QA-2101
	Scenario: GET_GeozoneGetAll?accessType_SharedToCompany
			Given авторизоваться используя стандартный аккаунт
			Given создать новую геозону типа Polygon с 4 точками и типом доступа SharedToCompany
			Given создать новую геозону типа Polygon с 4 точками и типом доступа PrivateToUser
			Then получить информацию о всех геозонах с доступом SharedToCompany
			Then убедиться что, созданная в предыдущем шаге, геозона с типом доступа SharedToCompany получена	

	
	@QA-2113 @QA-2101
	Scenario: GET_GeozoneGetAll?accessType_PrivateToUser
			Given авторизоваться используя стандартный аккаунт
			Given создать новую геозону типа Polygon с 4 точками и типом доступа SharedToCompany
			Given создать новую геозону типа Polygon с 4 точками и типом доступа PrivateToUser
			Then получить информацию о всех геозонах с доступом PrivateToUser
			Then убедиться что, созданная в предыдущем шаге, геозона с типом доступа PrivateToUser получена	

	
	@QA-2114 @QA-2101
	Scenario: GET_GeozoneGetAll?accessType&name
			Given авторизоваться используя стандартный аккаунт
			Given создать новую геозону типа Polygon с 4 точками и типом доступа SharedToCompany
			Given создать новую геозону типа Polygon с 4 точками и типом доступа PrivateToUser
			Then получить информацию о геозоне из предыдущего шага с по типу доступа и имени