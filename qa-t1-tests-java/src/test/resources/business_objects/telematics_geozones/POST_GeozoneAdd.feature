Feature: POST_GeozoneAdd

	@QA-2102 @QA-2101
	Scenario: POST_GeozoneAdd_Linear2pointsMinFields
		Given авторизоваться используя demo аккаунт
		Then создать новую геозону типа Linear с 2 точками	

	
	@QA-2103 @QA-2101
	Scenario: POST_GeozoneAdd_Linear3pointsMinFields
		Given авторизоваться используя demo аккаунт
		Then создать новую геозону типа Linear с 3 точками	

	
	@QA-2104 @QA-2101
	Scenario: POST_GeozoneAdd_Linear3pointsAllFields
		Given авторизоваться используя demo аккаунт
		Then создать новую геозону типа Linear с 3 точками и полным списком полей	

	
	@QA-2105 @QA-2101
	Scenario: POST_GeozoneAdd_RoundMinFields
		Given авторизоваться используя demo аккаунт
		Then создать новую геозону типа Round	

	
	@QA-2106 @QA-2101
	Scenario: POST_GeozoneAdd_RoundAllFields
		Given авторизоваться используя demo аккаунт
		Then создать новую геозону типа Round и полным списком полей	

	
	@QA-2107 @QA-2101
	Scenario: POST_GeozoneAdd_Polygon4pointsMinFields
		Given авторизоваться используя demo аккаунт
		Then создать новую геозону типа Polygon с 4 точками	

	
	@QA-2108 @QA-2101
	Scenario: POST_GeozoneAdd_Polygon5pointsMinFields
		Given авторизоваться используя demo аккаунт
		Then создать новую геозону типа Polygon с 5 точками	

	
	@QA-2109 @QA-2101
	Scenario: POST_GeozoneAdd_Polygon5pointsAllFields
		Given авторизоваться используя demo аккаунт
		Then создать новую геозону типа Polygon с 5 точками и полным списком полей	