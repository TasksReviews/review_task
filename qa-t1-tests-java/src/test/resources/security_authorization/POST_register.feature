Feature: POST_register

	@QA-1355 @QA-178 @QA-2089 @tone
	Scenario: POST_register_Positive_test_without_Login
		Then зарегистрировать аккаунт без поля login	

	
	@QA-1356 @QA-178 @QA-2089 @tone
	Scenario: POST_register_Positive_test_with_Random_Data
		Then зарегистрировать аккаунт со случайныйми значениями полей	

	
	@QA-1443 @QA-178 @QA-2089 @tone
	Scenario: POST_register_Positive_test_with_empty_Login
		Then зарегистрировать аккаунт c пустым значением поля login	

	
	@QA-1444 @QA-178 @QA-2089 @tone
	Scenario: POST_register_Phone_NecessaryOnRegistration
		Given установить в БД поле phone = NecessaryOnRegistration
		Then зарегистрировать аккаунт без поля email	

	
	@QA-1445 @QA-178 @QA-2089 @tone
	Scenario: POST_register_Email_NecessaryOnRegistration
		Given установить в БД поле email = NecessaryOnRegistration
		Then зарегистрировать аккаунт со случайныйми значениями полей, без phone	

	
	@QA-1491 @QA-178 @QA-2089 @tone
	Scenario Outline: POST_register_field_values_input_from_table
		Then зарегистрировать аккаунт c различными значениями полей из таблицы: "<login>", "<firstname>","<lastname>", "<email>","<password>"
			
		Examples:
		|login				|firstname				|lastname				|email							|password			|
		|d					|b						|b						|d@m.ru							|w4w4e2				|
		|0123456789			|1234567890				|1234567890				|rdr3421hgh@mail.ru				|638742427287		|
		|логинн				|имя					|фамилия				|testt@hohoh.com					|QAZXSWEDC			|
		|!#$&*-+/=?^_`{\|}~	|!#$&'*+-/=?^_`{\|}~	|!#$&'*+-/=?^_`{\|}~	|neww_email.test-qa@email.com	|~!@#$%^&-=_?		|
		|loginA				|firstname				|lastname				|emaila@e-mail.com				|edcxswqaz			|
		|loginB				|firstname				|lastname				|emailb@e-ma.il.com				|edcxswqaz			|
		|loginC				|firstname				|lastname				|emailc@e.mail.com				|edcxswqaz			|
		|LoginVeryLongUserNameVeryVeryLONGUSERNAMETESTyessS	|FirstnameVeryLongUserNameVeryVeryLONGUSERNAMETESTt		|LastnameVeryLongUserNameVeryVeryLONGUSERNAMETESTst	|emailverylongusernameveryverylongusernameteststemailverylongusernameveryverylongusernameteststemailverylongusernameveryverylongusernameteststemailverylongusernameveryverylongusernameteststemailverylongusernameveryverylonjfwgusernameteststtralalalala@mail.ru		|PasswordVeryLongUserNameVeryVeryLONGUSERNAMETESTst |
			

	
	@QA-1501 @QA-178 @QA-2089 @tone
	Scenario: POST_register_positive_test_with_unicode_Login
		Then зарегистрировать аккаунт c Unicode значением поля login
		|login                          |
		|%u043B%u043E%u0433%u0438%u043D |	

	
	@QA-1502 @QA-178 @QA-2089 @tone
	Scenario: POST_register_positive_test_with_unicode_Firstname
		Then зарегистрировать аккаунт c Unicode значением поля firstname
		|firstname          |
		|%u0438%u043C%u044F |	

	
	@QA-1503 @QA-178 @QA-2089 @tone
	Scenario: POST_register_positive_test_with_unicode_Lastname
		Then зарегистрировать аккаунт c Unicode значением поля lastname
		|lastname                                   |
		|%u0444%u0430%u043C%u0438%u043B%u0438%u044F |	

	
	@QA-1504 @QA-178 @QA-2089 @tone
	Scenario: POST_register_positive_test_with_unicode_Password
		Then зарегистрировать аккаунт c Unicode значением поля password
		|password                                                                                  |
		|%u0441%u0435%u043A%u0440%u0435%u0442%u043D%u044B%u0439%u043F%u0430%u0440%u043E%u043B%u044C|	