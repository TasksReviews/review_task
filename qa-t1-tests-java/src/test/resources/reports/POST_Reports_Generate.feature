Feature: POST_Reports_Generate

	
	@QA-2038 @tone
	Scenario Outline: POST_Reports_generate
			 Given авторизоваться используя demo аккаунт
			 Then запросить отчет с параметрами из таблицы: "<reportType>","<reportCriteriaParams>", "<resourceType>","<resourcesFilterInclude>", "<dateFrom>","<dateTo>","<graphTimeUnit>","<dataTimeUnit>","<geozoneIds>","<isDeepDetailed>"
		
			Examples:
			|reportType					|reportCriteriaParams							|resourceType	|resourcesFilterInclude		|dateFrom					|dateTo					  |graphTimeUnit	|dataTimeUnit	|geozoneIds					|isDeepDetailed	|
			|SummaryReport				|1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19|Vehicles		|593fb8f65532f61ad80c6269	|2017-07-14 00:00:00|2017-07-21 23:59:00|Month			|Month			|							|false			|
			|TravelReport				|1,2,3,4,5,6,7,8,9,10,11,12,13					|Vehicles		|593ef1305532f61ad80c5f06	|2017-07-01 00:00:00|2017-07-21 23:59:00|Day				|Day			|							|true			|							
			|ReportOnViolations			|1,2,3											|Vehicles		|593fb8f65532f61ad80c6269	|2017-07-14 00:00:00|2017-07-21 23:59:00|Month			|Month			|							|false			|							
			|ReportOnDriveStyle			|1,2,3											|Employees		|593fb8f65532f61ad80c6269	|2017-07-14 00:00:00|2017-07-21 23:59:00|Month			|Month			|							|false			|							
			|ReportOnFuel				|1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16			|Vehicles		|593fb8f65532f61ad80c6269	|2017-07-14 00:00:00|2017-07-21 23:59:00|Month			|Month			|							|false			|							
			|ReportOnVisitsToGeozones	|1,2,3											|Vehicles		|593fb8f65532f61ad80c6269	|2017-07-14 00:00:00|2017-07-21 23:59:00|Month			|Month			|5936d01f5532f317b021aef8	|false			|		
			|ReportOnLossOfCommunication|1,2,3											|Vehicles		|593fb8f65532f61ad80c6269	|2017-07-14 00:00:00|2017-07-21 23:59:00|Month			|Month			|							|false			|							
			 
			 
	@QA-2038 @tone
	Scenario Outline: POST_reports_generate[deepDetailed_timeZones]
Given авторизация в учетку с правами (Диспетчер, Транспортный специалист или Руководитель/владелец)
Given создание виртуального ТМУ
Given создание ТС с привязанным ТМУ
Given добавление точек в DeviceFullRecords, содержащих поездку
# период больше суток, в каждом часе есть поездка, время начала и конца которой (минуты) уникально.
Given пересчет интервалов для ТМУ
When установлен часовой пояс из таблицы: "<timezone>", "<timezoneOffset>"
Then отправить запрос отчета с глубокой детализацией за период dateFrom:"2017-08-24T06:00:00<пояс>" dateTo:"2017-08-24T22:59:59<пояс>"
# проверить, что в ответе пришли аналогичные dateFrom и dateFrom
# проверить, что в ответе пришли данные поездок за правильный период 

Examples:
|timezone						|timezoneOffset	|пояс|
|Dateline Standard Time			|-720			|-12:00|
|UTC-11							|-660			|-11:00|
|Aleutian Standard Time			|-600			|-10:00|
|Hawaiian Standard Time			|-600			|-10:00|
|Marquesas Standard Time		|-570			|-09:30|
|Alaskan Standard Time			|-540			|-09:00|
|UTC-09							|-540			|-09:00|
|Pacific Standard Time (Mexico)	|-480			|-08:00|
|UTC-08							|-480			|-08:00|
|Pacific Standard Time			|-480			|-08:00|
|US Mountain Standard Time		|-420			|-07:00|
|Mountain Standard Time (Mexico)|-420			|-07:00|
|Mountain Standard Time			|-420			|-07:00|
|Central America Standard Time	|-360			|-06:00|
|Central Standard Time			|-360			|-06:00|
|Easter Island Standard Time	|-360			|-06:00|
|Central Standard Time (Mexico)	|-360			|-06:00|
|Canada Central Standard Time	|-360			|-06:00|
|SA Pacific Standard Time		|-300			|-05:00|
|Eastern Standard Time (Mexico)	|-300			|-05:00|
|Eastern Standard Time			|-300			|-05:00|
|Haiti Standard Time			|-300			|-05:00|
|Cuba Standard Time				|-300			|-05:00|
|US Eastern Standard Time		|-300			|-05:00|
|Paraguay Standard Time			|-240			|-04:00|
|Atlantic Standard Time			|-240			|-04:00|
|Venezuela Standard Time		|-240			|-04:00|
|Central Brazilian Standard Time|-240			|-04:00|
|SA Western Standard Time		|-240			|-04:00|
|Pacific SA Standard Time		|-240			|-04:00|
|Turks And Caicos Standard Time	|-240			|-04:00|
|Newfoundland Standard Time		|-210			|-03:30|
|Tocantins Standard Time		|-180			|-03:00|
|E. South America Standard Time	|-180			|-03:00|
|SA Eastern Standard Time		|-180			|-03:00|
|Argentina Standard Time		|-180			|-03:00|
|Greenland Standard Time		|-180			|-03:00|
|Montevideo Standard Time		|-180			|-03:00|
|Saint Pierre Standard Time		|-180			|-03:00|
|Bahia Standard Time			|-180			|-03:00|
|UTC-02							|-120			|-02:00|
|Mid-Atlantic Standard Time		|-120			|-02:00|
|Azores Standard Time			|-60			|-01:00|
|Cape Verde Standard Time		|-60			|-01:00|
|UTC							|0				|UTC|
|Morocco Standard Time			|0				|+00:00|
|GMT Standard Time				|0				|+00:00|
|Greenwich Standard Time		|0				|+00:00|
|W. Europe Standard Time		|60				|+01:00|
|Central Europe Standard Time	|60				|+01:00|
|Romance Standard Time			|60				|+01:00|
|Central European Standard Time	|60				|+01:00|
|W. Central Africa Standard Time|60				|+01:00|
|Namibia Standard Time			|60				|+01:00|
|Jordan Standard Time			|120			|+02:00|
|GTB Standard Time				|120			|+02:00|
|Middle East Standard Time		|120			|+02:00|
|Egypt Standard Time			|120			|+02:00|
|E. Europe Standard Time		|120			|+02:00|
|Syria Standard Time			|120			|+02:00|
|West Bank Standard Time		|120			|+02:00|
|South Africa Standard Time		|120			|+02:00|
|FLE Standard Time				|120			|+02:00|
|Turkey Standard Time			|120			|+02:00|
|Israel Standard Time			|120			|+02:00|
|Kaliningrad Standard Time		|120			|+02:00|
|Libya Standard Time			|120			|+02:00|
|Arabic Standard Time			|180			|+03:00|
|Arab Standard Time				|180			|+03:00|
|Belarus Standard Time			|180			|+03:00|
|Russian Standard Time			|180			|+03:00|
|E. Africa Standard Time		|180			|+03:00|
|Iran Standard Time				|210			|+03:30|
|Arabian Standard Time			|240			|+04:00|
|Astrakhan Standard Time		|240			|+04:00|
|Azerbaijan Standard Time		|240			|+04:00|
|Russia Time Zone 3				|240			|+04:00|
|Mauritius Standard Time		|240			|+04:00|
|Georgian Standard Time			|240			|+04:00|
|Caucasus Standard Time			|240			|+04:00|
|Afghanistan Standard Time		|270			|+04:30|
|West Asia Standard Time		|300			|+05:00|
|Ekaterinburg Standard Time		|300			|+05:00|
|Pakistan Standard Time			|300			|+05:00|
|India Standard Time			|330			|+05:30|
|Sri Lanka Standard Time		|330			|+05:30|
|Nepal Standard Time			|345			|+05:45|
|Central Asia Standard Time		|360			|+06:00|
|Bangladesh Standard Time		|360			|+06:00|
|N. Central Asia Standard Time	|360			|+06:00|
|Myanmar Standard Time			|390			|+06:30|
|SE Asia Standard Time			|420			|+07:00|
|Altai Standard Time			|420			|+07:00|
|W. Mongolia Standard Time		|420			|+07:00|
|North Asia Standard Time		|420			|+07:00|
|Tomsk Standard Time			|420			|+07:00|
|China Standard Time			|480			|+08:00|
|North Asia East Standard Time	|480			|+08:00|
|Singapore Standard Time		|480			|+08:00|
|W. Australia Standard Time		|480			|+08:00|
|Taipei Standard Time			|480			|+08:00|
|Ulaanbaatar Standard Time		|480			|+08:00|
|North Korea Standard Time		|510			|+08:30|
|Aus Central W. Standard Time	|525			|+08:45|
|Transbaikal Standard Time		|540			|+09:00|
|Tokyo Standard Time			|540			|+09:00|
|Korea Standard Time			|540			|+09:00|
|Yakutsk Standard Time			|540			|+09:00|
|Cen. Australia Standard Time	|570			|+09:30|
|AUS Central Standard Time		|570			|+09:30|
|E. Australia Standard Time		|600			|+10:00|
|AUS Eastern Standard Time		|600			|+10:00|
|West Pacific Standard Time		|600			|+10:00|
|Tasmania Standard Time			|600			|+10:00|
|Vladivostok Standard Time		|600			|+10:00|
|Lord Howe Standard Time		|630			|+10:30|
|Bougainville Standard Time		|660			|+11:00|
|Russia Time Zone 10			|660			|+11:00|
|Magadan Standard Time			|660			|+11:00|
|Norfolk Standard Time			|660			|+11:00|
|Sakhalin Standard Time			|660			|+11:00|
|Central Pacific Standard Time	|660			|+11:00|
|Russia Time Zone 11			|720			|+12:00|
|New Zealand Standard Time		|720			|+12:00|
|UTC+12							|720			|+12:00|
|Fiji Standard Time				|720			|+12:00|
|Kamchatka Standard Time		|720			|+12:00|
|Chatham Islands Standard Time	|765			|+12:45|
|Tonga Standard Time			|780			|+13:00|
|Samoa Standard Time			|780			|+13:00|
|Line Islands Standard Time		|840			|+14:00|	 