Feature: GET_Reports_AnalyticIntervalResults

	@QA-2044 @tone
	Scenario Outline: GET_Reports_analyticIntervalResults
			 Given авторизоваться используя demo аккаунт
			 Then Получить информацию проанализированных интервалов с параметрами из таблицы: "<Deviceid>","<StartTime>","<EndTime>"
		
			Examples:
			|Deviceid					|StartTime			|EndTime			|
			|592f428c36214a623c8689a5	|2017-07-07			|2017-07-29			|
			|592f428c36214a623c8689a5	|2017-07-07 00:00:00|2017-07-28 23:59:00|	