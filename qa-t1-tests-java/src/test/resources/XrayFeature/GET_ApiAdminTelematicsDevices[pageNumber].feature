Feature: GET_ApiAdminTelematicsDevices[pageNumber]
  @QA-2499
  Scenario Outline: GET_ApiAdminTelematicsDevices[pageNumber]

Given Залогиниться в панели администратора под пользователем с ролью девелопера
And Создать 3 новых ТМУ
#Вызвать метод /Api/Admin/Telematics/Devices?pageSize=1 с разными значениями параметра pageNumber
When Запросить информацию по устройствам на странице "<pageNumber>"
#Проверить, что метод возвращает pageSize == 1, items.length == 1, pageNumber = <pageNumber>
Then Получена запрошенная страница

Examples:
|pageNumber|
|    1     |
|    2     |
|    3     |