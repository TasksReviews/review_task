package neo4j;

import org.neo4j.driver.v1.*;
import static org.neo4j.driver.v1.Values.parameters;

import java.sql.Timestamp;

//import org.neo4j.driver.v1.Config;

public class neo4jHello{
	public static void main(String[] args) {
		 long timestamp = System.currentTimeMillis();
		neo4jHello();
		System.out.println((System.currentTimeMillis() - timestamp ));
	}
	public static void neo4jHello() {
		Config noSSL = Config.build().withEncryptionLevel(Config.EncryptionLevel.NONE).toConfig();
		Driver driver = GraphDatabase.driver("bolt://localhost", AuthTokens.basic("neo4j", "qwerty"), noSSL);// <password>
		try {
			Session session = driver.session();
			StatementResult createresult = session.run("CREATE (a:TestNode3 {position:1,name:'SomePerson name',movie:'Cloud Atlas'}) return a");
			if (createresult.hasNext())
			System.out.println(createresult.next().get("a").asMap().toString());

			StatementResult result = session.run("MATCH (a:Person) return a");
			while (result.hasNext()) {
				Record record = result.next();
				System.out.println(record.get("a").asMap().toString());
			}

			session.close();
			driver.close();

		} catch (Exception e) {
			System.out.println("faled to execute query");
		}

	}
}
