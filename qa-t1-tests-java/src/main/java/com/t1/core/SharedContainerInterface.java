package com.t1.core;

import java.util.ArrayList;
import java.util.Map;

public interface SharedContainerInterface {

	
	public static Map<String, ArrayList> getContainers() {
		return AbstractClass.container;
	}
	
	public void addToSharedContainer() ;
	public void addToSharedContainer(int index) ;
	public void updateInSharedContainer() ;
	
	int removeFromSharedContainer();
}
