package com.t1.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

import org.json.XML;

public class XmlParse {
	protected static final String ERROR = "Error";
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("XmlParse");
	public String readXml (String fileName){
		String  fileContent = null;
		URL url = getClass().getResource("/src/test/resources/Test_Data/tracks/"+fileName);
		File file = new File(url.getPath());
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line.trim());
			}
			fileContent = XML.toJSONObject(sb.toString()).toString(1);
		} catch (IOException e) {
			log.error(ERROR, e);
		}
		return fileContent;
	}
}
