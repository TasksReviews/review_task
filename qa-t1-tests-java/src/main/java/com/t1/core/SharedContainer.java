package com.t1.core;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.t1.core.api.Customer;
import com.t1.core.api.Department;
import com.t1.core.api.Employee;
import com.t1.core.api.Geozone;
import com.t1.core.api.Group;
import com.t1.core.api.NotificationRule;
import com.t1.core.api.Report;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

public class SharedContainer extends AbstractClass {
	
	protected static String checkedFields = "checkedFields";
	protected static String usedObjectsStr = "usedObjects";
	
	public static void clearContainer() {
		Set<String> listOfContainers = container.keySet();
			
			Iterator<String> iterator = listOfContainers.iterator();
		    while(iterator.hasNext()) {
		    	String containerName = iterator.next();
			if(!(containerName.contains("token")||containerName.contains("Token"))){
				iterator.remove();
			}
		}
	}
	
	
	public static void removeAllContainers() {
		container.clear();
		if (! container.isEmpty()){
			assertTrue(false,"shared container wasn't cleared");	
			}
	}
	
	public static int removeFromContainer(String key, String id) {
		int indexOfRemovedEntity=-1;
		if (!container.isEmpty()) {
			if (container.containsKey(key)) {
				ArrayList listOfIds = container.get(key);
				if (! listOfIds.isEmpty()){
                    //удаление группы из контейнера
					if(listOfIds.get(0) instanceof com.t1.core.api.Group){
						for (int i = 0; i < listOfIds.size(); i++){
							if( ((Group) listOfIds.get(i)).getGroupId().equals(id)){
								listOfIds.remove(i);
								indexOfRemovedEntity=i;
							}
						}
					}

					else if(listOfIds.get(0) instanceof com.t1.core.api.Employee){
						for (int i =0; i<listOfIds.size(); i++){
							if( ((Employee) listOfIds.get(i)).getEmployeeId().equals(id)  ){
								listOfIds.remove(i);
								indexOfRemovedEntity=i;
							}
						} 
					}
					  
					else if(listOfIds.get(0) instanceof com.t1.core.api.Vehicle){
						for (int i =0; i<listOfIds.size(); i++){
							if( ((Vehicle) listOfIds.get(i)).getVehicleId().equals(id)  ){
								listOfIds.remove(i);
								indexOfRemovedEntity=i;
							}
						}
					}
					  
					else if(listOfIds.get(0) instanceof com.t1.core.api.Department){
						for (int i =0; i<listOfIds.size(); i++){
							if( ((Department) listOfIds.get(i)).getDepartmentId().equals(id)  ){
								listOfIds.remove(i);
								indexOfRemovedEntity=i;
							}
						}
					}
					else if(listOfIds.get(0) instanceof com.t1.core.api.Customer){
						for (int i =0; i<listOfIds.size(); i++){
							if( ((Customer) listOfIds.get(i)).getCustomerId().equals(id)  ){
								listOfIds.remove(i);
								indexOfRemovedEntity=i;
							}
						}
					}
					else if(listOfIds.get(0) instanceof com.t1.core.api.Geozone){
						for (int i =0; i<listOfIds.size(); i++){
							if( ((Geozone) listOfIds.get(i)).getGeozoneId().equals(id)  ){
								listOfIds.remove(i);
								indexOfRemovedEntity=i;
							}
						}
					}
					else if(listOfIds.get(0) instanceof com.t1.core.api.TelematicVirtualTracker){
						for (int i =0; i<listOfIds.size(); i++){
							if( ((TelematicVirtualTracker) listOfIds.get(i)).getTrackerId().equals(id)  ){
								listOfIds.remove(i);
								indexOfRemovedEntity=i;
							}
						}
					}
					else if(listOfIds.get(0) instanceof com.t1.core.api.Report){
						for (int i =0; i<listOfIds.size(); i++){
							if( ((Report) listOfIds.get(i)).getReportTemplateId().equals(id)  ){
								listOfIds.remove(i);
								indexOfRemovedEntity=i;
							}
						}
					}
					else if(listOfIds.get(0) instanceof com.t1.core.api.NotificationRule){
					for (int i =0; i<listOfIds.size(); i++){
						if( ((NotificationRule) listOfIds.get(i)).getRuleId().equals(id)  ){
							listOfIds.remove(i);
							indexOfRemovedEntity=i;
						}
					}
				}
					else if (listOfIds.get(0) instanceof java.util.ArrayList) {
						// do we really need to iterate thru this list or first instance is enough??
						for (int s = 0; s < listOfIds.size(); s++) {
							ArrayList subList = (ArrayList) listOfIds.get(s);
							if (subList.get(0) instanceof org.bson.Document) {
								for (int i = 0; i < subList.size(); i++) {
									if (((org.bson.Document) subList.get(i)).containsKey("_id")
											&& ((org.bson.Document) subList.get(i)).get("_id").toString().equals(id)) {
										subList.remove(i);
										indexOfRemovedEntity = i;
									}
								}
							}
						}
					}
				else if (listOfIds.contains(id)) {
					indexOfRemovedEntity=listOfIds.indexOf(id);
					listOfIds.remove(id);
					log.debug("id removed"+id);
					container.put(key, listOfIds);
				} else {
					log.debug("shared container "+key+" id="+id+" no such id!");
				}
			}
			} else {
				log.warn("can't remove from shared container "+key+" id="+id+" no such container");
			}

		}
		return indexOfRemovedEntity;
	}
	public static void removeFromContainer(String key) {
		if (!container.isEmpty()) {
			if (container.containsKey(key)) {
					container.remove(key);
			}
			else {
				assertTrue(false, "невозможно очистить список "+key);
			}
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
	public static void setContainer(String key, final Object entity) {
		if (getContainers().containsKey(key) && ! getContainers().get(key).isEmpty()) {
			ArrayList existingArray = getContainers().get(key);
			existingArray.add(entity);
			container.put(key, existingArray);
		} else {
			ArrayList newArray = new ArrayList();
			newArray.add(entity);
			container.put(key, newArray);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
	public static void setContainer(String key, int index, final Object entity) {
		if (getContainers().containsKey(key) && ! getContainers().get(key).isEmpty()) {
			ArrayList existingArray = getContainers().get(key);
			existingArray.add(index,entity);
			container.put(key, existingArray);
		} else {
			ArrayList newArray = new ArrayList();
			newArray.add(entity);
			container.put(key, newArray);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
	public static void setContainer(String key, ArrayList list) {
		if (!getContainers().isEmpty() && getContainers().get(key)!=null) {
			if (! getContainers().get(key).isEmpty()) {
				ArrayList existingArray = getContainers().get(key);
				existingArray.addAll(list);
				container.put(key, existingArray);
			}
		} else {
			container.put(key, list);
		}
	}
	

	@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
	public static void updateContainer(String key, final Object entity) {
		String id="";
		if (entity instanceof com.t1.core.api.Employee)
			id = ((Employee) entity).getEmployeeId();
		else if (entity instanceof com.t1.core.api.Vehicle)
			id = ((Vehicle) entity).getVehicleId();
		else if (entity instanceof com.t1.core.api.Group)
			id = ((Group) entity).getGroupId();
		else if (entity instanceof com.t1.core.api.Customer)
			id = ((Customer) entity).getCustomerId();
		else if (entity instanceof com.t1.core.api.Department)
			id = ((Department) entity).getDepartmentId();
		else if (entity instanceof com.t1.core.api.Geozone)
			id = ((Geozone) entity).getGeozoneId();
		else if (entity instanceof com.t1.core.api.TelematicVirtualTracker)
			id = ((TelematicVirtualTracker) entity).getTrackerId();
		else if (entity instanceof com.t1.core.api.NotificationRule)
			id = ((NotificationRule) entity).getRuleId();	
		else id = ((org.json.simple.JSONObject)entity).get("id").toString();
		int indexOfUpdatingEntity = removeFromContainer(key,  id);	
		setContainer(key,indexOfUpdatingEntity, entity);
	}
	/**
	 * <b>you must be really sure the entity contains each updating field.</b>
	 * method updates existing fields with requested values.
	 * @param key
	 * @param id
	 * @param fields
	 */
	@SuppressWarnings({ "rawtypes" })
	public static void updateContainer(String key, String id, HashMap fields) {
		List objects = getObjectsFromSharedContainer("any",key);
		int indexOfUpdatingEntity = removeFromContainer(key,  id);
		String entityId="";
		for(Object entity : objects){
			if (entity instanceof com.t1.core.api.Employee)
				entityId = ((Employee) entity).getEmployeeId();
			else if (entity instanceof com.t1.core.api.Vehicle)
				entityId = ((Vehicle) entity).getVehicleId();
			else if (entity instanceof com.t1.core.api.Group)
				entityId = ((Group) entity).getGroupId();
			else if (entity instanceof com.t1.core.api.Customer)
				entityId = ((Customer) entity).getCustomerId();
			else if (entity instanceof com.t1.core.api.Department)
				entityId = ((Department) entity).getDepartmentId();
			else if (entity instanceof com.t1.core.api.Geozone)
				entityId = ((Geozone) entity).getGeozoneId();
			else if (entity instanceof com.t1.core.api.TelematicVirtualTracker)
				entityId = ((TelematicVirtualTracker) entity).getTrackerId();
			else if (entity instanceof com.t1.core.api.NotificationRule)
				entityId = ((NotificationRule) entity).getRuleId();	
			else entityId = ((org.json.simple.JSONObject)entity).get("id").toString();

			if(entityId.equals(id)){
				Class<?> clazz = entity.getClass();
				for (Object fieldName : fields.keySet()){
					try {
					Field field = clazz.getDeclaredField(fieldName.toString());
		            field.setAccessible(true);
		            field.set(entity, fields.get(fieldName));
					} catch (NoSuchFieldException e) {
			            clazz = clazz.getSuperclass();
			        } catch (Exception e) {
			            throw new IllegalStateException(e);
			        }
				}
				setContainer(key,indexOfUpdatingEntity, entity);
			}
		}
			
		
		
	}
	
	public static Map<String, ArrayList> getContainers() {
		return container;
	}
	
	public static ArrayList getFromContainer(String objectName) {
		ArrayList result = null;
		try{
			result = SharedContainer.getContainers().get(objectName);
			if(result.size()==0){
				result.get(0);
			}
		}catch (Exception e) {
			AbstractClass.setToAllureChechkedFields();
			assertTrue(false,"Ошибка получения значения из предыдущего шага: контейнер не содержит объект "+objectName+"\r\n"+e);
			throw e;
		}
		return result;
	}
	
	
	public static Object getLastObjectFromSharedContainer(String container) {
		Object object = null;
		if ((SharedContainer.getContainers().containsKey(container))
				&& ! SharedContainer.getContainers().get(container).isEmpty()
				&&   SharedContainer.getContainers().get(container).get(0) != "") {
			int totalObj = SharedContainer.getContainers().get(container).size();
			 object =  SharedContainer.getContainers().get(container).get(totalObj-1);
		}

		return object;
	}
	
	 /**
	  * may be requested :
	  * <br>* <b>\d+</b> number of requested objects
	  * <br>* <b>'all'</b> - objects in container
	  * <br>* <b>'any'</b> - in case container may be empty or does not exists: will return all or none objects
	  * @param numberOfObjects
	  * @param container
	  * @return ArrayList of objects
	  */
	public static ArrayList getObjectsFromSharedContainer(String numberOfObjects, String container) {
		ArrayList arr = new ArrayList();

		if (numberOfObjects. equalsIgnoreCase("all")) {
			int totalObj = SharedContainer.getContainers().get(container).size();
			return getObjectsFromSharedContainer(totalObj, container);
			
		} else if (numberOfObjects. equalsIgnoreCase("any")) {
			if (SharedContainer.getContainers().containsKey(container) 
					&& ! SharedContainer.getContainers().get(container).isEmpty() 
					&&   SharedContainer.getContainers().get(container).get(0) != "") {
				int totalObj = SharedContainer.getContainers().get(container).size();
				return getObjectsFromSharedContainer(totalObj, container);
			} else {
				return arr;
			}
		}

		else if (Pattern.compile("\\d+").matcher(numberOfObjects).matches()) {
			if (Integer.parseInt(numberOfObjects) == 0) {
				return arr;
			} else {
				return getObjectsFromSharedContainer(Integer.parseInt(numberOfObjects), container);
			}
		}
		return arr;
	}
 /**
  * will return number of requested objects, except requested <b><0</b> then going to be returned <b>all</b> placed in container
  * @param numberOfObjects
  * @param container
  * @return ArrayList of objects
  */
 
	public static ArrayList getObjectsFromSharedContainer(int numberOfObjects, String container) {
		Object object = null;
		ArrayList arr = new ArrayList();
		if(numberOfObjects==0){
			return arr;
		}
		else if (SharedContainer.getContainers().containsKey(container)) {
			if(numberOfObjects <0) {
				numberOfObjects =	SharedContainer.getContainers().get(container).size();
			}
			for (int i = 1; i <= numberOfObjects; i++) {
				if (SharedContainer.getContainers().get(container).get(i-1) != "") {
					object = SharedContainer.getContainers().get(container).get(i-1);
					arr.add(object);
				} else {
					assertTrue(false, "не удалось получить " + numberOfObjects + " объектов");
				}
			}
		} else{
			assertTrue(false, "не удалось получить " + container + " объекты");
		}

		return arr;
	}
	
	
	public static void addCheckedFieldToContainer(String key, String value){
		final HashMap<String,String> field = new HashMap<>();
		if(value!=null){
		field.put(key, value );
		} else {
			field.put(key, null);
		}
		SharedContainer.setContainer(checkedFields, field);
	}
	public static void addCheckedFieldToContainer(String key, String[] value){
		final HashMap field = new HashMap<>();
		field.put(key, Arrays.toString(value));
		SharedContainer.setContainer(checkedFields, field);
	}
	public static void addCheckedFieldToContainer(String key, Object[] value){
		final HashMap field = new HashMap<>();
		field.put(key, Arrays.toString(value));
		SharedContainer.setContainer(checkedFields, field);
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addToUsedObjects(String objectName, final String objId) {
		HashMap usedObjects = new HashMap<String, ArrayList>();
		ArrayList listIds = new ArrayList();
		if (SharedContainer.container.containsKey(usedObjectsStr)) {
			ArrayList<HashMap> usedObjectsList = SharedContainer.getFromContainer(usedObjectsStr);
			// clean it, otherwise we will get duplicates
			SharedContainer.removeFromContainer(usedObjectsStr);
			
			usedObjects = usedObjectsList.get(0);
			if (usedObjectsList.get(0).containsKey(objectName)) {
				ArrayList<String> ids = (ArrayList) usedObjectsList.get(0).get(objectName);
				listIds = ids;
			}
		}
		listIds.add(objId);

		usedObjects.put(objectName, listIds);
		// need to change this container instead of adding new list to it!
		SharedContainer.setContainer(usedObjectsStr, usedObjects);
	}

	public static boolean isObjectUsed(String objId) {
		Boolean used = false;
		if (SharedContainer.container.containsKey(usedObjectsStr)) {
			ArrayList<HashMap> usedObjectsList = SharedContainer.getFromContainer(usedObjectsStr);
			for (Object objName : usedObjectsList) {
				if (objName.toString().contains(objId)) {
					used = true;
					break;
				}
			}
		}
		return used;
	}
}
