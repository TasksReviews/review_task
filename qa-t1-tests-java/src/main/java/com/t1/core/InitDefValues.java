package com.t1.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONObject;

import com.t1.core.mongodb.GetInfoFromMongo;

public class InitDefValues extends AbstractClass {
	static JSONObject initDefValsMap ;
	
	protected static String description = "description";
	protected static String phone = "phone";
	protected static String stdNumbPattern = "stdNumbPattern";
	protected static String courier = "Courier";
	protected static String driver = "Driver";
	protected static String address= "address";
	protected static String color = "color";
	protected static String fuelConsumption= "fuelConsumption";
	protected static String tankCapacity = "tankCapacity";
	protected static String date20180823mid = "2018-08-23 00:00:00";
	protected static String date20170823mid = "2017-08-23 00:00:00";
	protected static String date20170112time = "2017-01-12 13:14:25";
	protected static String entityStatus = "entityStatus"; 
	protected static String prefAddress = "JFW_address_" ;
	protected static String prefComment = "JFW_comment_";
	protected static String comment = "comment";
	protected static String technicalStateStr ="technicalState";
	protected static String latitude = "latitude";
	protected static String stdLatVal= "55.71";
	protected static String longitude = "longitude";
	protected static String reportOnShifts = "ReportOnShifts";
	
	
	
	@SuppressWarnings("unchecked")
	public static JSONObject initCommon (String pattern){
		initDefValsMap = new JSONObject();
		initDefValsMap.put("firstname",			"JFW_firstname"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put("lastname",				"JFW_lastname"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put("middleName",			"JFW_middleName"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put(description,			"JFW_description"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put(phone,				"+70"+RandomStringUtils.random(9, getProperty(stdNumbPattern)));
		initDefValsMap.put("email",				RandomStringUtils.random(10, "qwertyuiop-ASDFG0123456789_")+"@email"+RandomStringUtils.random(10, "qwertyuiop-ASDFG0123456789_")+".com");  
		initDefValsMap.put("status",				pickRandomFromList(	new String[]{"Regular","Contract"}));
		initDefValsMap.put("deactivationReason",	"JFW_DeactivationReason"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put("role",					pickRandomFromList(	new String[]{"General","Admin","Demo",courier,driver,"Operator","Dispatcher",
				"Analyst","TransportExpert","TechnicalExpert","FinancialExpert","Chief","InsuranceAnalyst","Developer"}));

		return initDefValsMap;
		
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject initAccount (String pattern){
		//Account
		initDefValsMap.put("login",				"JFW_login"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put("accountPhone",			"+70"+RandomStringUtils.random(9, getProperty(stdNumbPattern)));//previously account phonenumber had format "8*" should be replaced to phone field		
												//pattern for future: qwertyuiop-ASDFG0123456789_ЙЦУКЕНёшщзхячьъю
		initDefValsMap.put("password",				"JFW_"+RandomStringUtils.random(rndFldLen(), "AzQWERTYuiop~!@#$%^&_-=1230"));
		initDefValsMap.put("account_type",			pickRandomFromList(	new String[]{"Person"}));
		initDefValsMap.put("sex",			pickRandomFromList(	new String[]{"Male","Female"}));
		initDefValsMap.put("account_role",					pickRandomFromList(	new String[]{"Admin","Demo",courier,driver,"Operator","Dispatcher",
				"Analyst","TransportExpert","TechnicalExpert","FinancialExpert","Chief","InsuranceAnalyst"}));
		initDefValsMap.put("profile_avatarUri",	"system/profile-1.png");
		return initDefValsMap;
	}
	
	public static JSONObject initCustomer (String pattern){
		//customer fields
		initDefValsMap.put("customer_name",				"JFW_custName"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put(address,				"JFW_custAdress"+RandomStringUtils.random(rndFldLen(), pattern));
		return initDefValsMap;
	}
	
	public static JSONObject initDepartment (String pattern){
		//department fields
		initDefValsMap.put("department_name",				"JFW_depName"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put(address,				"JFW_depAdress"+RandomStringUtils.random(rndFldLen(), pattern));
		
		return initDefValsMap;
		
	}
	
	
	public static JSONObject initEmployee (String pattern){
		log.warn("no fields to aply pattern: "+pattern);
		initDefValsMap.put("employee_type",					pickRandomFromList(	new String[]{driver,courier}));
		initDefValsMap.put("profile_avatarUri",	"system/profile-1.png");
		//Employee driverlicense
		initDefValsMap.put("driverLicense_number",				RandomStringUtils.random(10, getProperty(stdNumbPattern)));
		initDefValsMap.put("driverLicense_category",			pickRandomFromList(	new String[]{"A,A1,B,B1,C,C1,D,D1,BE,CE,C1E,DE,D1E,M,Tm,Tb","A,B,C"}));
		initDefValsMap.put("driverLicense_expires",				pickRandomFromList(	new String[]{"2020-01-28","2020-01-28 00:00:00","2025-01-28","2025-01-28 00:00:00"})); //TODO: dates should be random.
		JSONObject driverLicense = new JSONObject();
		driverLicense.put("number", initDefValsMap.get("driverLicense_number"));
		driverLicense.put("category", initDefValsMap.get("driverLicense_category"));
		driverLicense.put("expires", initDefValsMap.get("driverLicense_expires"));
		initDefValsMap.put("driverLicense",driverLicense);

		//employee rating
		initDefValsMap.put("rating_Value",				RandomStringUtils.random(2, "123456789"));
		initDefValsMap.put("rating_Direction",			RandomStringUtils.random(1, "123456789"));
		JSONObject rating = new JSONObject();
		rating.put("Value", Integer.valueOf((String)initDefValsMap.get("rating_Value")));
		rating.put("Direction", Integer.valueOf((String)initDefValsMap.get("rating_Direction")));
		initDefValsMap.put("rating",rating);
		
		return initDefValsMap;
		
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject initVehicle (String pattern){
		java.util.Random random = new java.util.Random();
		//vehicle fields:
		initDefValsMap.put("name",					"JFW_name"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put("vehicle_type",			pickRandomFromList(	new String[]{"Bus","Sedan","Hatchback","Coupe","StationWagon","Van","SoftRoader",
						"SportUtility","Cabriolet","Pickup","AutoTruck","Electrobus","Tramway","Microbus","Bulldozer","FieldEngine","TrailerCar",
						"Ambulance","FireTruck","VehicleCarrier","Stepthru","Scooter","Bike","Tricycle","QuadBike"}));
		initDefValsMap.put("vehicle_number",		RandomStringUtils.random(10, getProperty(stdNumbPattern)));
		initDefValsMap.put("model",				"JFW_Model_"+RandomStringUtils.random(10, pattern));
		initDefValsMap.put("mark",					"JFW_Mark_"+RandomStringUtils.random(10, pattern));
		initDefValsMap.put("vinCode",				"JFWVINCODE"+RandomStringUtils.random(10, getProperty("stdLatPattern")));
		initDefValsMap.put("vehicle_avatarUri",	"system/vehicle-default.png");
		initDefValsMap.put("vehicle_custom",		"custom");
		initDefValsMap.put("capacity",				pickRandomFromList(	new String[]{"EspeciallySmall","Small","Average","Large","ParticularlyLarge"}));
		initDefValsMap.put(color,				pickRandomFromList(	new String[]{"red","green","blue","white","black"}));
		initDefValsMap.put("garageNumber",			RandomStringUtils.random(10, getProperty(stdNumbPattern)));
		initDefValsMap.put("maxSpeed",				(random.nextInt(400+1-10)+ 10)+"");
		initDefValsMap.put("vehicle_registered",	pickRandomFromList(	new String[]{"2002-01-28 00:00:00","2005-02-28 00:00:00","2016-01-13 00:00:00"}));

		//Vehicle fuel
		initDefValsMap.put("fuel_source",			pickRandomFromList(	new String[]{"gas station","tanker","bar"}));
		initDefValsMap.put("fuel_type",			pickRandomFromList(	new String[]{"petrol","diesel","gas","whiskey"}));
		initDefValsMap.put(fuelConsumption,		RandomStringUtils.random(1, getProperty(stdNumbPattern))+"."+(random.nextInt(9)+ 1));
		initDefValsMap.put(tankCapacity,			(random.nextInt(100+1-10)+ 10)+"");
		
		JSONObject fuel = new JSONObject();
		fuel.put("source", initDefValsMap.get("fuel_source"));
		fuel.put("type", initDefValsMap.get("fuel_type"));
		fuel.put(fuelConsumption, initDefValsMap.get(fuelConsumption));
		fuel.put(tankCapacity, initDefValsMap.get(tankCapacity));
		initDefValsMap.put("fuel",fuel);
		
		//Vehicle insurancePolicy
		initDefValsMap.put("insurancePolicy_type",				pickRandomFromList(	new String[]{"Kasko","Osago"}));
		initDefValsMap.put("insurancePolicy_number",			"1");
		initDefValsMap.put("insurancePolicy_expires",			pickRandomFromList(	new String[]{date20170823mid,date20180823mid}));
		initDefValsMap.put("insurancePolicy_documentFileName",	pickRandomFromList(	new String[]{"Blank2034543","Strahovka scan 01"}));
		initDefValsMap.put("insurancePolicy_DocumentUri",		"new");
		initDefValsMap.put("insurancePolicy_DocumentIndex",	"1");
		
		final JSONObject insurancePolicy = new JSONObject();
		insurancePolicy.put("type", initDefValsMap.get("insurancePolicy_type"));
		insurancePolicy.put("number", initDefValsMap.get("insurancePolicy_number"));
		insurancePolicy.put("expires", initDefValsMap.get("insurancePolicy_expires"));
		insurancePolicy.put("documentFileName", initDefValsMap.get("insurancePolicy_documentFileName"));
		insurancePolicy.put("DocumentUri", initDefValsMap.get("insurancePolicy_DocumentUri"));
		insurancePolicy.put("DocumentIndex", initDefValsMap.get("insurancePolicy_DocumentIndex"));
		insurancePolicy.put(entityStatus, "new");
		ArrayList insurances = new ArrayList();
		insurances.add(insurancePolicy);
		initDefValsMap.put("insurancePolicy",insurances);
		
		
		//Vehicle technical state start
		final JSONObject technicalState = new JSONObject();

		
		//Vehicle maintenances
		setRepairMaintenance("maintenances", pattern);
//		initDefValsMap.put("maintenances_date",			pickRandomFromList(	new String[]{date20170112time,date20170823mid,date20180823mid})); 
//		initDefValsMap.put("maintenances_state",			"JFW_state_"+RandomStringUtils.random(10, pattern));
//		initDefValsMap.put("maintenances_mileage",			(random.nextInt(10000+1-10)+ 10)+"");
//		initDefValsMap.put("maintenances_organization",	"JFW_organization_"+RandomStringUtils.random(10, pattern));
//		initDefValsMap.put("maintenances_phone",			"+70"+RandomStringUtils.random(9, getProperty(stdNumbPattern)));
//		initDefValsMap.put("maintenances_address",			prefAddress+RandomStringUtils.random(10, pattern));
//		initDefValsMap.put("maintenances_comment",			prefComment+RandomStringUtils.random(10, pattern));
//		initDefValsMap.put("maintenances_entityStatus",	"new");
//		initDefValsMap.put("maintenances_cost",			(random.nextInt(9000+1-10)+ 10)+"");
		
		final JSONObject maintenances = new JSONObject();
		maintenances.put("date", initDefValsMap.get("maintenances_date"));
		maintenances.put("state", initDefValsMap.get("maintenances_state"));
		maintenances.put("mileage", initDefValsMap.get("maintenances_mileage"));
		maintenances.put("organization", initDefValsMap.get("maintenances_organization"));
		maintenances.put(phone, initDefValsMap.get("maintenances_phone"));
		maintenances.put(address, initDefValsMap.get("maintenances_address"));
		maintenances.put(comment, initDefValsMap.get("maintenances_comment"));
		maintenances.put(entityStatus, initDefValsMap.get("maintenances_entityStatus"));
		maintenances.put("cost", initDefValsMap.get("maintenances_cost"));
		
		technicalState.put("maintenances",Arrays.asList(maintenances));
		initDefValsMap.put(technicalStateStr,technicalState  );
	
		
		//Vehicle repairs
		initDefValsMap.put("repairs_name",			"JFW_name_"+RandomStringUtils.random(10, pattern));
		setRepairMaintenance("repairs", pattern);
//		initDefValsMap.put("repairs_date",			pickRandomFromList(	new String[]{date20170112time,date20170823mid,date20180823mid})); 
//		initDefValsMap.put("repairs_state",			"JFW_state_"+RandomStringUtils.random(10, pattern));
//		initDefValsMap.put("repairs_mileage",			(random.nextInt(100000+1-10)+ 10)+"");
//		initDefValsMap.put("repairs_organization",	"JFW_organization_"+RandomStringUtils.random(10, pattern));
//		initDefValsMap.put("repairs_phone",			"+70"+RandomStringUtils.random(9, getProperty(stdNumbPattern)));
//		initDefValsMap.put("repairs_address",			prefAddress+RandomStringUtils.random(10, pattern));
//		initDefValsMap.put("repairs_comment",			prefComment+RandomStringUtils.random(10, pattern));
//		initDefValsMap.put("repairs_entityStatus",	"new");
//		initDefValsMap.put("repairs_cost",			(random.nextInt(9000+1-10)+ 10)+"");
		
		final JSONObject repairs = new JSONObject();
		repairs.put("name", initDefValsMap.get("repairs_name"));
		repairs.put("date", initDefValsMap.get("repairs_date"));
		repairs.put("state", initDefValsMap.get("repairs_state"));
		repairs.put("mileage", initDefValsMap.get("repairs_mileage"));
		repairs.put("organization", initDefValsMap.get("repairs_organization"));
		repairs.put(phone, initDefValsMap.get("repairs_phone"));
		repairs.put(address, initDefValsMap.get("repairs_address"));
		repairs.put(comment, initDefValsMap.get("repairs_comment"));
		repairs.put(entityStatus, initDefValsMap.get("repairs_entityStatus"));
		repairs.put("cost", initDefValsMap.get("repairs_cost"));
		
		technicalState.put("repairs",Arrays.asList(repairs) );
		initDefValsMap.put(technicalStateStr,technicalState  );

		
		
		//Vehicle accidents
		initDefValsMap.put("accidents_name",				"JFW_name_"+RandomStringUtils.random(10, pattern));
		initDefValsMap.put("accidents_date",				pickRandomFromList(	new String[]{date20170112time,date20170823mid,date20180823mid})); 
		initDefValsMap.put("accidents_document",			"JFW_document_"+RandomStringUtils.random(10, pattern));
		initDefValsMap.put("accidents_documentFileName",	"JFW_document_Accident_1");
		initDefValsMap.put("accidents_damage",				"JFW_damage_"+RandomStringUtils.random(10, pattern));
		initDefValsMap.put("accidents_phone",				"+70"+RandomStringUtils.random(9, getProperty(stdNumbPattern)));
		initDefValsMap.put("accidents_address",			prefAddress+RandomStringUtils.random(10, pattern));
		initDefValsMap.put("accidents_comment",			prefComment+RandomStringUtils.random(10, pattern));
		initDefValsMap.put("accidents_entityStatus",		"new");
		initDefValsMap.put("accidents_documentUri",		"new");
		final JSONObject employee = new JSONObject();
		//TODO: it is not a best way for performance to call mongodb for each constructor!
		try {
			JSONObject lastCreatedEmployee =	GetInfoFromMongo.getLastDocumentFromCollection("Employees",getProperty("testCustomerId"),null );
			initDefValsMap.put("accidents_employee",lastCreatedEmployee);
			employee.put("id",		ReplyToJSON.extractPathFromJson(lastCreatedEmployee, "$._id.$oid"));
		} catch (Exception e) {
			log.error(ERROR,e);
		}
		
		final JSONObject accidents = new JSONObject();
		accidents.put("name", initDefValsMap.get("accidents_name"));
		accidents.put("date", initDefValsMap.get("accidents_date"));
		accidents.put("document", initDefValsMap.get("accidents_document"));
		accidents.put("documentFileName", initDefValsMap.get("accidents_documentFileName"));
		accidents.put("damage", initDefValsMap.get("accidents_damage"));
		
		accidents.put(phone, initDefValsMap.get("accidents_phone"));
		accidents.put(address, initDefValsMap.get("accidents_address"));
		accidents.put(comment, initDefValsMap.get("accidents_comment"));
		accidents.put(entityStatus, initDefValsMap.get("accidents_entityStatus"));
		accidents.put("documentUri", initDefValsMap.get("accidents_documentUri"));
		accidents.put("documentIndex", initDefValsMap.get("accidents_documentIndex"));
		accidents.put("employee", employee);
		
		technicalState.put("accidents",Arrays.asList(accidents));
		initDefValsMap.put(technicalStateStr,technicalState  );


		//Vehicle technical state end

		return initDefValsMap;
		
	}
	private static void setRepairMaintenance(String type, String pattern){
		java.util.Random random = new java.util.Random();
		initDefValsMap.put(type+"_date",			pickRandomFromList(	new String[]{date20170112time,date20170823mid,date20180823mid})); 
		initDefValsMap.put(type+"_state",			"JFW_state_"+RandomStringUtils.random(10, pattern));
		initDefValsMap.put(type+"_mileage",			(random.nextInt(100000+1-10)+ 10)+"");
		initDefValsMap.put(type+"_organization",	"JFW_organization_"+RandomStringUtils.random(10, pattern));
		initDefValsMap.put(type+"_phone",			"+70"+RandomStringUtils.random(9, getProperty(stdNumbPattern)));
		initDefValsMap.put(type+"_address",			prefAddress+RandomStringUtils.random(10, pattern));
		initDefValsMap.put(type+"_comment",			prefComment+RandomStringUtils.random(10, pattern));
		initDefValsMap.put(type+"_entityStatus",	"new");
		initDefValsMap.put(type+"_cost",			(random.nextInt(9000+1-10)+ 10)+"");

	}
	
	public static JSONObject initVirtualTracker  (String pattern){
	//virtual tracker 
	initDefValsMap.put("code",			"JFW_Code"+RandomStringUtils.random(rndFldLen(), pattern));
	initDefValsMap.put("type",			"VirtualTracker_"+RandomStringUtils.random(5, pattern));
	initDefValsMap.put("model",			"TrackerModel_"+RandomStringUtils.random(5, pattern));
	initDefValsMap.put("manufacturer",			"TrackerManuf_"+RandomStringUtils.random(5, pattern));
	initDefValsMap.put("simCard",			"+70"+RandomStringUtils.random(9, getProperty(stdNumbPattern)));
	return initDefValsMap;
	
	}
	
	public static JSONObject initGroup  (String pattern){
	//Group 
		initDefValsMap.put("name",			"JFW_groupName"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put(color,				pickRandomFromList(	new String[]{
				"#7b6c95","#ddfa48","#b864c6","#ff26b7","#f7987a","#63ffc8","#c9e200","#5f71a1","#ffdd26","#e169a9","#ff1a96","#ff8a57","#92edcc","#68e06c",
				"#9288e8","#f4ff87","#f495f3","#ff583e","#ffd9ae","#85fffc","#b0ec88","#6d4291","#d1dd53","#fc9cde","#ff3346","#c25f01","#b1f7ec","#93f88e"}));
		initDefValsMap.put("objects", new ArrayList());
		initDefValsMap.put("resources", new ArrayList());
	return initDefValsMap;
	
	}
	
	public static JSONObject initGeozone  (String pattern){
		java.util.Random random = new java.util.Random();
		initDefValsMap.put("name",			"JFW_geoname"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put(color,					pickRandomFromList(	new String[]{
							"#7b6c95", "#ddfa48", "#b864c6", "#ff26b7", "#f7987a", "#63ffc8", "#c9e200", "#5f71a1", "#ffdd26", "#e169a9", "#ff1a96", "#ff8a57", "#92edcc", "#68e06c", 
							"#9288e8", "#f4ff87", "#f495f3", "#ff583e", "#ffd9ae", "#85fffc", "#b0ec88", "#6d4291", "#d1dd53", "#fc9cde", "#ff3346", "#c25f01", "#b1f7ec", "#93f88e"}));
		initDefValsMap.put("type",					pickRandomFromList(	new String[]{"Linear","Round","Polygon"}));
		initDefValsMap.put("access",					pickRandomFromList(	new String[]{"SharedToCompany","PrivateToUser"}));
		
		initDefValsMap.put("bufferThickness",				(random.nextInt(20)+ 1)+"");
		initDefValsMap.put("radius",				((double) random.nextInt(2000))+"");
		initDefValsMap.put(description,			"JFW_description"+RandomStringUtils.random(rndFldLen(), pattern));
		initDefValsMap.put("square",			"32470090.2131577");
	 JSONObject pointsRound =new JSONObject(); 
    //default round points
	 pointsRound.put(longitude, "37.66"); pointsRound.put(latitude, stdLatVal);
		 initDefValsMap.put("defaultRoundPoints", Arrays.asList(pointsRound));
		 
		//default polygon points
		 JSONObject pointsPolygon1 =new JSONObject();
		 JSONObject pointsPolygon2 =new JSONObject();
		 JSONObject pointsPolygon3 =new JSONObject();
		 JSONObject pointsPolygon4 =new JSONObject();
		 pointsPolygon1.put(longitude, "37.69"); pointsPolygon1.put(latitude, stdLatVal);
		 pointsPolygon2.put(longitude, "37.58"); pointsPolygon2.put(latitude, "55.73");
		 pointsPolygon3.put(longitude, "37.61"); pointsPolygon3.put(latitude, "55.64");
		 pointsPolygon4.put(longitude, "37.69"); pointsPolygon4.put(latitude, stdLatVal);
		 initDefValsMap.put("defaultPolygonPoints", Arrays.asList(pointsPolygon1,pointsPolygon2,pointsPolygon3,pointsPolygon4));
		 

	      //default linear points
			 JSONObject pointsLinear1 =new JSONObject();
			 JSONObject pointsLinear2 =new JSONObject();
			 JSONObject pointsLinear3 =new JSONObject();
			 pointsLinear1.put(longitude, "37.66"); pointsLinear1.put(latitude, stdLatVal);
			 pointsLinear2.put(longitude, "37.58"); pointsLinear2.put(latitude, "55.73");
			 pointsLinear3.put(longitude, "37.61"); pointsLinear3.put(latitude, "55.64");
			 initDefValsMap.put("defaultLinearPoints", Arrays.asList(pointsLinear1,pointsLinear2,pointsLinear3));
		
	return initDefValsMap;
	
	}

	@SuppressWarnings("unchecked")
	public static JSONObject initReport(String reportType,String pattern) {
		java.util.Random random = new java.util.Random();
		JSONObject initDefReportValsMap = new JSONObject();
		int totalParams=0;
		int maxNoOfParam=0;
		switch(reportType)
		{
		case "SummaryReport" : maxNoOfParam=19; break;
		case "TravelReport" : maxNoOfParam=13; break;
		case "ReportOnViolations" : maxNoOfParam=23; break;
		case "ReportOnDriveStyle" : maxNoOfParam=38; break;
		case "ReportOnFuel" : maxNoOfParam=16; break;
		case "ReportOnVisitsToGeozones" : maxNoOfParam=3; break;
		case "ReportOnLossOfCommunication" : maxNoOfParam=3; break;
		case "ReportOnShifts" : maxNoOfParam=17; break;
		case "ReportOnSensor" : maxNoOfParam=7; break;
		default:
			throw new IllegalArgumentException("Invalid\\Unsupported reportType: " + reportType);
		}
		
		//start for random report criteries
		totalParams = (random.nextInt(maxNoOfParam)+ 1);//we going to select this number of params as result
		ArrayList<String> reportCriteria = new ArrayList();
		ArrayList<String> selectedReportCriteria = new ArrayList();
		for( int i=1;i<=maxNoOfParam;i++){
			reportCriteria.add(reportType+"_Param"+i);
		}
		for( int i=1;i<=totalParams;i++){
			int j = (random.nextInt((reportCriteria.size())));//  (0 + (int) (random.nextInt() * reportCriteria.size()-1));//current id selected
			selectedReportCriteria.add(reportCriteria.get(j));
			reportCriteria.remove(j);
		}
		
		//ugly workaround for non-used params and diff paramName		
		if(reportType.equals(reportOnShifts)){
			for (int i=0; i< selectedReportCriteria.size(); i++){
				selectedReportCriteria.set(i, selectedReportCriteria.get(i).replace("ReportOnShifts_Param","ShiftReport_Param"));
				
				if (selectedReportCriteria.get(i).contains("Param1") || selectedReportCriteria.get(i).contains("Param4") || selectedReportCriteria.get(i).contains("Param6")){
					selectedReportCriteria.remove(i); i--;
				}
			}
			if(selectedReportCriteria.isEmpty()){
				selectedReportCriteria.add("ShiftReport_Param2");
			}
		}
		if(reportType.equals("ReportOnSensor")){
			for (int i=0; i< selectedReportCriteria.size(); i++){
				selectedReportCriteria.set(i, selectedReportCriteria.get(i).replace("ReportOnSensor_Param", "SensorReport_Param"));
				
				if (selectedReportCriteria.get(i).contains("Param4") ){
					selectedReportCriteria.remove(i); i--;
				}
			}
			if(selectedReportCriteria.isEmpty()){
				selectedReportCriteria.add("SensorReport_Param1");
			}
		}
		
		initDefReportValsMap.put("reportCriteria", selectedReportCriteria);
		//end
		initDefReportValsMap.put("name","JFW_report"+RandomStringUtils.random(rndFldLen(), getProperty(pattern)));
		initDefReportValsMap.put(description,"JFW_reportDescription"+RandomStringUtils.random(rndFldLen(), getProperty(pattern)));
		
		initDefReportValsMap.put("resourceType", pickRandomFromList(	new String[]{"Vehicles","Employees"}));

		Map<String, String> resDatePeriod = getDateTimePeriod (pickRandomFromList(	new String[]{"за неделю","сутки","вчера","2 недели"}), TimeZone.getTimeZone("UTC"),"",false);
		initDefReportValsMap.put("dateFrom",resDatePeriod.get("dateTimeFrom"));
		initDefReportValsMap.put("dateTo",resDatePeriod.get("dateTimeTo"));
		
		initDefReportValsMap.put("graphTimeUnit",pickRandomFromList(	new String[]{"Day","Week","TwoWeeks","Month"}));
		initDefReportValsMap.put("dataTimeUnit",pickRandomFromList(	new String[]{"Day","Week","TwoWeeks","Month"}));
		initDefReportValsMap.put("isDeepDetailed",Boolean.valueOf(pickRandomFromList(	new String[]{"true","false"})));
		initDefReportValsMap.put("detalization",pickRandomFromList(	new String[]{"Consolidated","Detailed","DeepDetailed"}));
		//info about shifts
		JSONObject shiftsSettings = new JSONObject();
		shiftsSettings.put("onlyFullShifts", false);
		List shiftList = new ArrayList();
		
		if(reportType.equals(reportOnShifts)){
			JSONObject shiftFields = new JSONObject();
			shiftFields.put("timeFrom", "9:00");
			shiftFields.put("timeTo", "18:00");
			shiftFields.put("name", "Смена 1");
			shiftList.add(shiftFields);
		} 			
		shiftsSettings.put("shiftList", shiftList);
		initDefReportValsMap.put("shiftsSettings",shiftsSettings);

		return initDefReportValsMap;
	}

	public JSONObject initializeInputWithDefaultOrRandomValues(){
		return  initCommon (getProperty("stdPattern"));
	}
	
	public JSONObject initializeInputWithDefaultOrRandomValues(String entity){
	//	stdPattern=	   QWERTYuiop\~!@#$%^&*()_+}{|\":?><|\`-[];\'\/.,\№ЙцукёФяжщ1230
		return initializeInputWithDefaultOrRandomValues(entity, getProperty("stdPattern"));
	}
	
	public static JSONObject initializeInputWithDefaultOrRandomValues(String entity, String pattern) {

		initCommon(pattern);
		
		switch (entity){
		case "account": 	return initAccount(pattern);
		case "department": 	return initDepartment(pattern);
		case "employee": 	return initEmployee(pattern);
		case "vehicle": 	return initVehicle(pattern);
		case "tracker": 	return initVirtualTracker(pattern);
		case "group": 		return initGroup(pattern);
		case "customer": 	return initCustomer(pattern);
		case "geozone": 	return initGeozone(pattern);

		default:
			throw new IllegalArgumentException("Invalid\\not implemented entity type " + entity+" no initial default values applied");
		
		}
	}

public static JSONObject initializeReportInputWithDefaultOrRandomValues(String reportType, String pattern){
		return initReport(reportType,pattern);

	}

}
