package com.t1.core;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import org.bson.BSONDecoder;
import org.bson.BSONObject;
import org.bson.BasicBSONDecoder;
import org.bson.Document;
import org.bson.UuidRepresentation;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.UuidCodec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.json.JsonWriterSettings;
import org.bson.types.ObjectId;
import org.json.XML;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class ReadFile {

	private ReadFile(){
	}
	protected static final String ERROR = "Error";
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("ReadFile");
	public static JSONObject readJsonFile(String path) {
		JSONParser parser = new JSONParser();
		File file = new File(path);
		try (BufferedReader br = new BufferedReader(new FileReader(file))){
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line.trim());
			}
			String jsonStringInput = sb.substring(sb.indexOf("{"), sb.lastIndexOf("}") + 1);
			return (JSONObject) parser.parse(jsonStringInput);
		} catch (Exception e) {
			log.error(ERROR,e);
			return null;
		}
	}
	
	
	public static List<Document> readMongoJsonFile(String path) {
		File file = new File(path);
		List<Document> jsonObjReadFromFile = null;
		try (BufferedReader br = new BufferedReader(new FileReader(file))){
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				String oneLine = line.trim();
				if (oneLine.contains("LUUID") || oneLine.contains("ISODate") || oneLine.contains("ObjectId")) {
					Document tmpDoc = new Document();
					String[] arr = oneLine.split("\"");
					if (oneLine.contains("ObjectId")) {
						tmpDoc.append(arr[1], new ObjectId(arr[3]));
					}
					else if (oneLine.contains("ISODate")) {
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
						df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
						tmpDoc.append(arr[1], new Date(df.parse(arr[3]).getTime()));
					}
					else if (oneLine.contains("LUUID")) {
						tmpDoc.append(arr[1], UUID.fromString(arr[3]));
					}
					CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
							CodecRegistries.fromCodecs(new UuidCodec(
									UuidRepresentation.STANDARD)), MongoClient
									.getDefaultCodecRegistry());
					//actually here must be type 4, but according to current mongodb values it is "$type":"03"  
					oneLine = tmpDoc.toJson(new JsonWriterSettings(), new DocumentCodec(codecRegistry));
					oneLine = oneLine.substring(2, oneLine.length() - 2);
				} 
				sb.append(oneLine);
			}
			String jsonStringInput = "{ \"file data\":"+sb.toString()+" }";
			 jsonObjReadFromFile = toListOfDocuments(jsonStringInput);
			 //could be also returned JSONArray
//			 			JSONObject jsonObjReadFromFile123 = (JSONObject) parser.parse(jsonStringInput);
		} catch (Exception e) {
			log.error(ERROR,e);
		}
		return jsonObjReadFromFile;
	}
	
	public static List<Document> toListOfDocuments(String json){
		String fileData ="file data";
	    Document doc = Document.parse(json);
	    Object list = doc.get(fileData);
	    if(list instanceof List<?>) {
	        return (List<Document>) doc.get(fileData);
	    } else {
	    	List<Document> resList= new ArrayList<>();
	    	resList.add((Document)doc.get(fileData));
	    	return resList;
	    }

	}
	
	public static List<DBObject> readBsonFile(String path) {
		Path filePath = Paths.get(path);
	    List<DBObject> dataset = new ArrayList<>();
	    try (ByteArrayInputStream fileBytes = new ByteArrayInputStream(Files.readAllBytes(filePath))){
	        BSONDecoder decoder = new BasicBSONDecoder();
	        BSONObject obj;
	        while ((obj = decoder.readObject(fileBytes)) != null) {
	            final DBObject mongoDocument = new BasicDBObject(obj.toMap());
	            dataset.add(mongoDocument);
	        }
	    } catch (FileNotFoundException e) {
	    	log.error(ERROR,e);
	        throw new RuntimeException("Can not open BSON input file.", e);
	    } catch (JsonProcessingException e) {
	    	log.error(ERROR,e);
	        throw new RuntimeException("Can not parse BSON data.", e);
	    } catch (IOException e) {
	    	log.error(ERROR,e);
	    }
	    return dataset;
	}
	
	public static org.json.JSONObject readXmlFile(String path) {
		File file = new File(path);
		try (BufferedReader br = new BufferedReader(new FileReader(file))){
			String line;
			StringBuilder sb = new StringBuilder();

			while ((line = br.readLine()) != null) {
				sb.append(line.trim());
			}
			return XML.toJSONObject(sb.toString());

		} catch (Exception e) {
			log.error(ERROR,e);
			return null;
		}
	}	
}
