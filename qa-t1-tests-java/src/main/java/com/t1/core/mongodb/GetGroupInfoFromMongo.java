package com.t1.core.mongodb;

import java.util.List;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetGroupInfoFromMongo extends AbstractClass{
	protected static String omGroups = "OmGroups";
	
	@Step("MongoDB:получение информации о Группе {0}")
	public static List getGroupsInfoById(String groupId, String showOnly){
	return	GetInfoFromMongo.getInfoById(omGroups, groupId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Группе {0}")
	public static List getGroupsInfoById(String groupId){
	return	GetInfoFromMongo.getInfoById(omGroups, groupId,  "");
	}
	
	@Step("MongoDB:получение случайноой Группы из БД")
	public static List getRandomGroupFromMongo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(omGroups, getProperty("testCustomerId"), "");
	}
}
