package com.t1.core.mongodb;

import java.util.List;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetInsurancePolicyInfoFromMongo extends AbstractClass{
	protected static String employeeInsurancePolicies = "EmployeeInsurancePolicies";
	
	@Step("MongoDB:получение информации о Страховом полисе {0}")
	public static List getInsurancePolicyInfoById(String insPolicyId, String showOnly){
	return	GetInfoFromMongo.getInfoById(employeeInsurancePolicies, insPolicyId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Страховом полисе по Id ТС {0}")
	public static List getInsurancePolicyInfoByVehicleId(String vehicleId, String showOnly){
	return	GetInfoFromMongo.getInfoByFieldAndIdValue(employeeInsurancePolicies, "VehicleId",vehicleId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Страховом полисе {0}")
	public static List getInsurancePolicyInfoById(String insPolicyId){
	return	GetInfoFromMongo.getInfoById(employeeInsurancePolicies, insPolicyId,  "");
	}
	

}
