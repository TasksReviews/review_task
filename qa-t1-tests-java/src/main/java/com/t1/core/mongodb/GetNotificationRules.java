package com.t1.core.mongodb;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetNotificationRules extends AbstractClass{
	protected static String notificationRules = "NotificationRules";
	
	@Step("MongoDB:получение информации о Правиле {0}")
	public static List getNotificationRuleInfoById(String notificationRuleId, String showOnly){
	return	GetInfoFromMongo.getInfoById(notificationRules, notificationRuleId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Правиле {0}")
	public static List getNotificationRuleInfoById(String notificationRuleId){
	return	GetInfoFromMongo.getInfoById(notificationRules, notificationRuleId,  "");
	}
	
	@Step("MongoDB:получение случайного Правила из БД  {0}")
	public static List getRandomnotificationRuleInfo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(notificationRules,getProperty("testCustomerId"), "");
	}
	
	@Step("MongoDB:получение Правила из БД по фильтру {0}")
	public static List getNotificationRuleInfoByCustomFilters(Bson inputFilter){
	return	GetInfoFromMongo.getInfoByCustomFilters(notificationRules, inputFilter,new ArrayList<String>());
	}
	

}
