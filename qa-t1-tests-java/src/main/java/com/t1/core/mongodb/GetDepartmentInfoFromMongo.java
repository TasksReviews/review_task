package com.t1.core.mongodb;

import java.util.List;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetDepartmentInfoFromMongo extends AbstractClass{

	private  static final String DEPARTMENTS = "Departments";
	
	@Step("MongoDB:получение информации о Подразделении {0}")
	public static List getDepartmentInfoById(String departmentId, String showOnly){
	return	GetInfoFromMongo.getInfoById(DEPARTMENTS, departmentId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Подразделении {0}")
	public static List getDepartmentInfoById(String departmentId){
	return	GetInfoFromMongo.getInfoById(DEPARTMENTS, departmentId,  "");
	}
	
	@Step("MongoDB:получение случайного Подразделения из БД  {0}")
	public static List getRandomDepartmentInfo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(DEPARTMENTS,getProperty("testCustomerId"), "");
	}
	
}
