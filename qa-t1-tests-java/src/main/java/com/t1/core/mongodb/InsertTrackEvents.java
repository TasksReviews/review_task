package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.Date;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.model.Filters;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.TearDownExecutor;
import com.t1.core.api.Geozone;
import com.t1.core.api.NotificationRule;

import ru.yandex.qatools.allure.annotations.Step;

public class InsertTrackEvents extends MongoConnection {

	
	//NOT RELATED TO NOTIFICATIONS AT ALL

//	@Given("^добавить событие в БД \"([^\"]*)\"$")
	public static Document prepareDoc(String notificationEventType){
		String address = "Address";
		String message = "Message";
		String eventName ="EventName";
		String roadEventType ="RoadEventType";
		String geozoneId ="GeozoneId";
		String limitation ="Limitation";
		String recorded ="Recorded";
		String eventId ="EventId";
	 try {
		 Date notificationTime = new Date(System.currentTimeMillis() );
//			Vehicle vehicleFromPreviouesStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
//			TelematicVirtualTracker deviceFromPreviouesStep = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers"); 
//			List<JSONObject> trackPointsFromPreviouesStep =  SharedContainer.getObjectsFromSharedContainer("all","insertedTrackPoints"); 
//			JSONObject lastTrackPoint = trackPointsFromPreviouesStep.get(trackPointsFromPreviouesStep.size()-1);
//			String latitude =lastTrackPoint.get("Latitude").toString();
//			String longitude=lastTrackPoint.get("Longitude").toString();
			NotificationRule ruleFromPreviouesStep = (NotificationRule) SharedContainer.getLastObjectFromSharedContainer("notificationRules");
		Geozone geozoneFromPreviouesStep = (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozonesForNotificationRules");
		Document doc = new Document(address, "г. Москва, пр-кт Андропова, д.599");
				doc.append("Border", "Single");
			switch (notificationEventType) {
			case "GeozoneEntrance": {
				doc.append(message, "JFW_Вход в геозону");
				doc.append(eventName, "Вход в геозону");
				doc.append(roadEventType, "GeozoneEntrance");
				doc.append(eventId, "5996b84cbf5efd44d81c3ca6");
				doc.append(geozoneId, geozoneFromPreviouesStep.getGeozoneId());
				doc.append(address, "г. Москва, ул. Москворечье, д.3");

			}
				break;

			case "GeozoneStanding": {
				doc.append(roadEventType, "GeozoneStanding");
				doc.append(message, "JFW_Нахождение в геозоне");
				doc.append(eventName, "Нахождение в геозоне");
				doc.append("EventTime", notificationTime);
				doc.append(eventId, "59956677bf5efd395ccd1167");
				doc.append(geozoneId, geozoneFromPreviouesStep.getGeozoneId());
				doc.append(limitation, ruleFromPreviouesStep.getLimitationValue()+" мин");
				doc.append(recorded, "183 мин");
				doc.append(address, "г. Москва, пр-кт Андропова, д.599");
			}
				break;
			case "GeozoneExit": {
				doc.append(roadEventType, "GeozoneExit");
				doc.append(message, "JFW_Выход из геозоны");
				doc.append(eventName, "Выход из геозоны");
				doc.append(eventId, "5996b77abf5efd44d81c2b80");
				doc.append(geozoneId, geozoneFromPreviouesStep.getGeozoneId());
				doc.append(address, "г. Москва, ул. Москворечье, д.3");
			}
				break;

			case "UserSpeedlimit": {
				doc.append(roadEventType, "UserSpeedlimit");
				doc.append(message, "JFW_Превышение установленного порога скорости");
				doc.append(eventName, "Превышение установленного порога скорости");
				doc.append(eventId, "5996bc8bbf5efd44d81c42ef");
				doc.append(limitation, "65 км/ч");
				doc.append(recorded, "66 км/ч");
				doc.append(address, "г. Москва, ул. Перовская, д.1А, стр.17");
			}
				break;
			case "StopEvent": {
				doc.append(roadEventType, "StopEvent");
				doc.append(message, "JFW_Стоянка");
				doc.append(eventName, "Стоянка");
				doc.append(eventId, "5996b1eebf5efd44d81c054e");
				doc.append(address, "г. Москва, ул. Алексея Дикого, д.18");
			}
				break;
			case "IdleEvent": {
				doc.append("_t", "IdleEvent");
				doc.append(message, "JFW_Холостой ход - проверка иконки");
				doc.append(eventName, "Холостой ход - проверка иконки");
				doc.append(eventId, "5996b678bf5efd44d81c19f0");
				doc.append(address, "г. Москва, ул. Ленинская Слобода, д.19, стр.4");
			}
				break;
			case "TrafficRulesSpeedLimit": {
				doc.append(roadEventType, "TrafficRulesSpeedLimit");
				doc.append(message, "JFW_Превышение скорости по ПДД");
				doc.append(eventName, "Превышение скорости по ПДД");
				doc.append(eventId, "5996b689bf5efd44d81c1a21");
				doc.append(limitation, "60 км/ч");
				doc.append(recorded, "80 км/ч");
				doc.append(address, "д. Дубинино, ш. Ленинградское");

			}
				break;
			default:
				throw new IllegalArgumentException("Invalid\\Unsupported notification type: " + notificationEventType);
			}
	 return doc;
	 } catch (Exception e) {
		 log.error(ERROR,e);
			return null;
		}
 }

	@Step("MongoDB: вставка документа в TrackEvents")
	public static org.json.simple.JSONObject insertDocument(String notificationEventType) {
		try {
			log.debug(">> insertion to TrackEvents");
			org.json.simple.JSONObject insertedNotification =null;
			Document doc =  prepareDoc(notificationEventType);
			if(doc != null){
			 Bson inputFilter =  Filters.and (eq ("ResourceId",doc.get("ResourceId")), eq ("Created", doc.get("Created") ) );
			 insertedNotification = (org.json.simple.JSONObject) GetInfoFromMongo.getInfoByCustomFilters("TrackEvents", inputFilter,new ArrayList<String>()).get(0);
			 TearDownExecutor.addInsertedNotificationToTeardown(ReplyToJSON.extractPathFromJson(insertedNotification, "$._id.$oid") );
			} else {
				log.error("nothing to insert, doc is empty");
			}
			 return insertedNotification;
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
			return null;
		}
	}
}
