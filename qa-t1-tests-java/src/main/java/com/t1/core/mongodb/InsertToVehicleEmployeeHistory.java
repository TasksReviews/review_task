package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.t1.core.ReplyToJSON;
import com.t1.core.TearDownExecutor;

import ru.yandex.qatools.allure.annotations.Step;

public class InsertToVehicleEmployeeHistory extends MongoConnection {

	



	@Step("MongoDB: вставка документа в VehicleEmployeeHistory")
	public static org.json.simple.JSONObject insertDocument(Document doc, boolean showLogs) {
		org.json.simple.JSONObject insertedVehEmplHistRec =null;
		try {
			FindIterable<Document> response;
			if(doc != null){
//			log.debug(doc.toString());
			com.mongodb.client.MongoCollection<Document> collection = getMongoCollection("VehicleEmployeeHistory");

			 collection.insertOne(doc);
//			 mongoClient.close();
			    
			 ObjectId vehId = new ObjectId(doc.get("VehicleId").toString());
			 ObjectId emplId = new ObjectId(doc.get("EmployeeId").toString());
			 Bson inputFilter =  Filters.and (eq ("VehicleId",vehId),  eq ("EmployeeId",emplId) );
			 insertedVehEmplHistRec = (org.json.simple.JSONObject) GetInfoFromMongo.getInfoByCustomFilters("VehicleEmployeeHistory", inputFilter,new ArrayList<String>(),showLogs).get(0);
			 
//			 log.debug("insertedTrackPoint="+insertedTrackPoint.toString());
			 TearDownExecutor.addinsertedVehEmplHistRecordsToTeardown(ReplyToJSON.extractPathFromJson(insertedVehEmplHistRec, "$._id.$oid") );
//			 mongoClient.close();
			} else {
				log.error("nothing to insert, doc is empty");
			}
			 return insertedVehEmplHistRec;
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
			return null;
		}
	}
	
}
