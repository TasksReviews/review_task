package com.t1.core.mongodb;



import static com.mongodb.client.model.Filters.eq;
import static java.lang.Math.toIntExact;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.apache.commons.lang3.StringEscapeUtils;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.t1.core.ReplyToJSON;

import ru.yandex.qatools.allure.annotations.Step;

public class GetInfoFromMongo extends MongoConnection{

	protected static String deviceTime = "DeviceTime";
	protected static String customerIdStr = "CustomerId";
	protected static String responseFromCollection = "response from mongo received from collection ";
	protected static String getInfoByIdInCollection = "getInfoById in collection ";
	protected static String resultlistStr = ", resultlist ==" ;
	protected static String infoById = "информация о найденном ID ";
	protected static String noInfoById = "невозможно получить информацию о искомом ID ";
	protected static String noInfoInCollection = "Nothing was found in mongo! collection: ";
	protected static String noInfoByFilter = "невозможно получить информацию по искомому фильтру в коллекции ";
	
/*	aggregation:
	Block<Document> block = getDocumentBlock();
	AggregateIterable aggregate = toneMainTest.aggregate(Arrays.asList(
            new BasicDBObject("$lookup", new BasicDBObject("from", "Employees").append("localField", "EmployeeId").append("foreignField", "_id").append("as", "Employees"))
));
aggregate.forEach(block);
	выполняет запрос db."collection".aggregate([{
		$lookup:{from:"anotherCollection",localField:"localField",foreignField:"foreignField",as:"as"}
		}
		])
*/		
	/**
	 * 
	 * @param accountId
	 * @param showOnly
	 * @return
	 */
	@Step("MongoDB:получение информации из коллекции {0} по id {1}")
	public static List<org.json.simple.JSONObject> getInfoById(String collection, String searchingId, String showOnly){
		List<org.json.simple.JSONObject> resultlist = new ArrayList();
		FindIterable<Document> response;
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
			   ObjectId objectId = new ObjectId(searchingId);
			   if(showOnly== null||showOnly.equals("")){
				   response = coll.find(eq ("_id",objectId));
			   }else{
				   response = coll.find(eq ("_id",objectId)).projection(new Document(showOnly, 1));  
				  }
			   log.debug(responseFromCollection+collection+", id="+searchingId);
			   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);

			   if(! documentList.isEmpty()){
				   resultlist.add(ReplyToJSON.getJsonObjectFromDocument(documentList.get(0)));
			   } else {
				   resultlist = ReplyToJSON.getJsonObjectFromDocumentList(documentList);
			   }
			   log.debug(getInfoByIdInCollection+collection+resultlistStr+StringEscapeUtils.unescapeJson(resultlist.toString()));
			   textToAllureRep(infoById+searchingId ,StringEscapeUtils.unescapeJson(resultlist.toString()));
			   return  resultlist; 
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep(noInfoById+searchingId ,e.toString());
            assertTrue(false,e.toString());
    		return null;
        }

	 }
	
	
	@Step("MongoDB:получение информации из коллекции {0} по списку id {1}")
	public static List<org.json.simple.JSONObject> getInfoByListOfIds(String collection, List<String> requestedIds, String customerId, String showOnly){
		List<org.json.simple.JSONObject> resultlist = new ArrayList();
		FindIterable<Document> response;
		Bson filter = null;
		try {
			List<ObjectId> searchingIds = new ArrayList(); 
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
			
			for(String id :requestedIds ){
				searchingIds.add(new ObjectId(id));
			}
			if (customerId!= null && customerId!=""){
			 filter = Filters.and( Filters.in("_id",searchingIds),Filters.eq (customerIdStr,new ObjectId(customerId)));
			} else {
				 filter =  Filters.in("_id",searchingIds);
			}
				   if(showOnly==null||showOnly.equals("")){
				   response = coll.find(filter);
			   }else{
				   response = coll.find(filter).projection(new Document(showOnly, 1));  
				  }
			   log.debug(responseFromCollection+collection+", id="+requestedIds);
			   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);
			   resultlist = ReplyToJSON.getJsonObjectFromDocumentList(documentList);
			   log.debug(getInfoByIdInCollection+collection+resultlistStr+StringEscapeUtils.unescapeJson(resultlist.toString()));
			   textToAllureRep("информация о найденных ID "+requestedIds ,StringEscapeUtils.unescapeJson(resultlist.toString()));
			   return  resultlist; 
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep("невозможно получить информацию о искомых ID "+requestedIds ,e.toString());
            assertTrue(false,e.toString());
    		return null;
        }

	 }
	
	/**
	 * 
	 * @param accountId
	 * @param showOnly
	 * @return
	 */
	@Step("MongoDB:получение информации из коллекции {0} по id {1}")
	public static List getInfoByFieldAndIdValue(String collection, String searchingField,String searchingIdValue, String showOnly){
		List<org.json.simple.JSONObject> resultlist = new ArrayList();
		FindIterable<Document> response;
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
			   ObjectId objectId = new ObjectId(searchingIdValue);
			   if(showOnly==null || showOnly.equals("")){
				   response = coll.find(eq (searchingField,objectId));
			   }else{
				   response = coll.find(eq (searchingField,objectId)).projection(new Document(showOnly, 1));  
				  }
			   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);

			   if(! documentList.isEmpty()){
				   resultlist.add(ReplyToJSON.getJsonObjectFromDocument(documentList.get(0)));
			   } 
			   else if(documentList.isEmpty()){
				   log.warn(noInfoInCollection+collection);
			   	}else {
				   resultlist = ReplyToJSON.getJsonObjectFromDocumentList(documentList);
			   }
			   log.debug(getInfoByIdInCollection+collection+resultlistStr+resultlist.toString());
			   textToAllureRep("информация о документе найденном по полю "+searchingField+", ID "+searchingIdValue ,StringEscapeUtils.unescapeJson(resultlist.toString()));
			   return  resultlist; 
		}
		catch(Exception e) {
			mongoClient.close();
			log.error(ERROR,e);
            textToAllureRep("невозможно получить информацию о документе найденном по полю "+searchingField+", ID "+searchingIdValue ,e.toString());
    		return null;
        }

	 }
	public static List getInfoByCustomFilters(String collection, Bson inputFilters, List<String> showOnlyList){
		return getInfoByCustomFilters(collection, inputFilters, showOnlyList, true);
	}
	/**
	 * 
	 * @param collection
	 * @param inputFilters
	 * @param showOnlyList
	 * @return
	 */
	public static List<org.json.simple.JSONObject> getInfoByCustomFilters(String collection, Bson inputFilters, List<String> showOnlyList, boolean showLogs){

		List<org.json.simple.JSONObject> resultlist = new ArrayList<>();
		FindIterable<Document> response;
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
			  	
			if (inputFilters == null) {
				if (showOnlyList.isEmpty() || showOnlyList.get(0).equals("")) {
					response = coll.find();
				} else {
					response = coll.find().projection(com.mongodb.client.model.Projections
										  .fields(com.mongodb.client.model.Projections.include(showOnlyList)));
				}
			} else {

				if (showOnlyList.isEmpty() || showOnlyList.get(0).equals("")) {
					response = coll.find(inputFilters);
				} else {
					response = coll.find(inputFilters).projection(com.mongodb.client.model.Projections
								   .fields(com.mongodb.client.model.Projections.include(showOnlyList)));
				}
			}
			if(showLogs){
			   log.debug(responseFromCollection+collection+", customFilter:"+inputFilters);
			}
			   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);

			   if(! documentList.isEmpty()){
				   resultlist.addAll(ReplyToJSON.getJsonObjectFromDocumentList(documentList));
			   } else {
				   resultlist = ReplyToJSON.getJsonObjectFromDocumentList(documentList);
			   }
			   if(showLogs){
			   log.debug(getInfoByIdInCollection+collection+resultlistStr+StringEscapeUtils.unescapeJson(resultlist.toString()));
			   textToAllureRep("найденная информация в коллекции "+collection ,StringEscapeUtils.unescapeJson(resultlist.toString()));
			   }
			   return  resultlist; 
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep(noInfoByFilter+collection ,e.toString());
            assertTrue(false,e.toString());
    		return null;
        }

	}
	

	public static List getInfoByCustomFilters(String collection, Bson inputFilters, HashMap<String,Integer> inputSortOrderSet, List<String> showOnlyList){

		List<org.json.simple.JSONObject> resultlist = new ArrayList();
		BasicDBObject sortByObj = new BasicDBObject();
		if(inputSortOrderSet!=null && ! inputSortOrderSet.keySet().isEmpty()){
			for(String inputSortOrderKey : inputSortOrderSet.keySet()){
				sortByObj.append(inputSortOrderKey, inputSortOrderSet.get(inputSortOrderKey));
			}
		}
		FindIterable<Document> response;
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
			  	
			if (inputFilters == null) {
				if (showOnlyList.isEmpty() || showOnlyList.get(0).equals("")) {
					
					response = coll.find().sort(sortByObj);
				} else {
					response = coll.find().projection(com.mongodb.client.model.Projections
										  .fields(com.mongodb.client.model.Projections.include(showOnlyList))).sort(sortByObj);
				}
			} else {

				if (showOnlyList.isEmpty() || showOnlyList.get(0).equals("")) {
					response = coll.find(inputFilters).sort(sortByObj);
				} else {
					response = coll.find(inputFilters).projection(com.mongodb.client.model.Projections
								   .fields(com.mongodb.client.model.Projections.include(showOnlyList))).sort(sortByObj);
				}
			}
			   log.debug(responseFromCollection+collection+", customFilter:"+inputFilters);
			   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);

			   if(! documentList.isEmpty()){
				   resultlist.addAll(ReplyToJSON.getJsonObjectFromDocumentList(documentList));
			   } else {
				   resultlist = ReplyToJSON.getJsonObjectFromDocumentList(documentList);
			   }
			   log.debug(getInfoByIdInCollection+collection+resultlistStr+StringEscapeUtils.unescapeJson(resultlist.toString()));
			   textToAllureRep("найденная информация в коллекции "+collection ,StringEscapeUtils.unescapeJson(resultlist.toString()));
			   return  resultlist; 
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep(noInfoByFilter+collection ,e.toString());
            assertTrue(false,e.toString());
    		return null;
        }

	}
	
	public static long countFilteredByCustomFilters(String collection, Bson inputFilters){
		long response;
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
			if (inputFilters == null) {
				log.debug("count objects in collection "+collection+" no filters");
				response = coll.count();
			} else {
				log.debug("count objects in collection "+collection+" by filter:"+inputFilters.toString());
				response = coll.count(inputFilters);
			}
			textToAllureRep("количество документов в коллекции, фильтр: "+String.valueOf((Object) inputFilters),String.valueOf((Object) response));
			return response;
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep(noInfoByFilter+collection ,e.toString());
            assertTrue(false,e.toString());
    		return -1;
        }

	}
	
	@Step("MongoDB:получение информации из коллекции {0} по id {1} за период времени")
	public static List getInfoByDeviceIdAndDates(String collection, String  deviceId, String dateTimeFrom, String dateTimeTill, String showOnly){

/*		db.getCollection('DeviceFullRecords').find({"DeviceId" : ObjectId("5940ff14bf5efd572883a27f"), 
			deviceTime : {
			        $gte: ISODate("2017-06-21T08:45:00.000Z"),
			        $lt:  ISODate("2017-06-21T08:45:10.000Z")
			    }})
*/		DateFormat format = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		format.setTimeZone(utc3TimeZone);
		List<org.json.simple.JSONObject> resultlist = new ArrayList();
		FindIterable<Document> response;
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
			   ObjectId objectdeviceId = new ObjectId(deviceId);
			   Bson filter = 
					   Filters.and (eq ("DeviceId",objectdeviceId), 
					   Filters.gte(deviceTime,format.parse(dateTimeFrom)),
					   Filters.lte(deviceTime,dateTimeTill));
			  			  
			   if(showOnly==null || showOnly.equals("")){
				   response = coll.find(filter).sort(new BasicDBObject(deviceTime, -1));
			   }else{
				   response = coll.find(filter).sort(new BasicDBObject(deviceTime, -1)).projection(new Document(showOnly, 1));  
				  }
			   log.debug(responseFromCollection+collection+", id="+deviceId+",from="+dateTimeFrom+",till="+dateTimeTill);
			   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);

			   if(documentList.size()>0){
				   resultlist.addAll(ReplyToJSON.getJsonObjectFromDocumentList(documentList));
			   } else {
				   resultlist = ReplyToJSON.getJsonObjectFromDocumentList(documentList);
			   }
			   log.debug(getInfoByIdInCollection+collection+resultlistStr+StringEscapeUtils.unescapeJson(resultlist.toString()));
			   textToAllureRep(infoById+deviceId ,StringEscapeUtils.unescapeJson(resultlist.toString()));
			   return  resultlist; 
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep(noInfoById+deviceId ,e.toString());
            assertTrue(false,e.toString());
    		return null;
        }

	 }
	
	
	@SuppressWarnings("unused")
	@Step("MongoDB:получение информации из коллекции {0} по id {1} за период времени")
	public static List getInfoByDeviceIdAndDates(String collection, String  deviceId, String dateTimeFrom, String showOnly){
		
/*		db.getCollection('DeviceFullRecords').find({"DeviceId" : ObjectId("5940ff14bf5efd572883a27f"), 
			deviceTime : {
			        $gte: ISODate("2017-06-21T08:45:00.000Z"),
			        $lt:  ISODate("2017-06-21T08:45:10.000Z")
			    }})
*/
		DateFormat format = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		format.setTimeZone(utc3TimeZone);
		List<org.json.simple.JSONObject> resultlist = new ArrayList();
		FindIterable<Document> response;
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
			   ObjectId objectdeviceId = new ObjectId(deviceId);
			   Bson filter = 
					   Filters.and (eq ("DeviceId",objectdeviceId), 
					   Filters.gte(deviceTime,format.parse(dateTimeFrom))  );
			   if(showOnly==null || showOnly.equals("")){
				   response = coll.find(filter).sort(new BasicDBObject(deviceTime, -1));
			   }else{
				   response = coll.find(filter).sort(new BasicDBObject(deviceTime, -1)).projection(new Document(showOnly, 1));  
				  }
			   log.debug(responseFromCollection+collection+", id="+deviceId);
			   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);

			   if(documentList.size()>0){
				   resultlist.addAll(ReplyToJSON.getJsonObjectFromDocumentList(documentList));
			   } else {
				   resultlist = ReplyToJSON.getJsonObjectFromDocumentList(documentList);
			   }

			   log.debug(getInfoByIdInCollection+collection+resultlistStr+StringEscapeUtils.unescapeJson(resultlist.toString()));
			   textToAllureRep(infoById+deviceId ,StringEscapeUtils.unescapeJson(resultlist.toString()));
			   return  resultlist; 
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep(noInfoById+deviceId ,e.toString());
            assertTrue(false,e.toString());
    		return null;
        }

	 }
	
	@Step("MongoDB:получение случайного документа из коллекции {0}")
	public static List getRandomDocumentFromCollection(String collection, String customerId, String showOnly){
		List<org.json.simple.JSONObject> resultlist = new ArrayList();
		FindIterable<Document> response;
		Bson filter = null ;
		if (customerId!= null && customerId!=""){
		ObjectId objectId = new ObjectId(customerId);
		 filter = Filters.eq (customerIdStr,objectId);
		
		}
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
			//get total docs in collection
			Bson finalFilter = (filter == null) ? new BsonDocument() : filter;
			log.debug("rand collection="+collection+", filter="+finalFilter );
			long totalDocs = coll.count(finalFilter);
			 log.debug("total="+totalDocs);
			 Random random = new Random();
			 int rand = random.nextInt(toIntExact(totalDocs) ) ;
			
			   if(showOnly==null || showOnly.equals("")){
				   response = coll.find(finalFilter).limit(1).skip(rand);
			   }else{
				   response = coll.find(finalFilter).limit(1).skip(rand).projection(new Document(showOnly, 1));  
				  }
			   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);//ReplytoHashMap.findIterableDocumentToHashMap(response);

			   if(!documentList.isEmpty()){
				   resultlist.add(ReplyToJSON.getJsonObjectFromDocument(documentList.get(0)));
			   } else if(documentList.isEmpty()){
				   log.warn(noInfoInCollection+collection);
			   	}
			   else  {
				   resultlist = ReplyToJSON.getJsonObjectFromDocumentList(documentList);
			   }
			   log.debug("get random document from collection "+collection+resultlistStr+StringEscapeUtils.unescapeJson(resultlist.toString()));
			   textToAllureRep("информация о найденном случайном документе из коллекции "+collection,StringEscapeUtils.unescapeJson(resultlist.toString()));
			   return  resultlist; 
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
    		return null;
        }

	 }
	
	@Step("MongoDB:получение последнего документа из коллекции {0}")
	public static org.json.simple.JSONObject getLastDocumentFromCollection(String collection, String customerId, String accountId){
		org.json.simple.JSONObject result = new org.json.simple.JSONObject();
		FindIterable<Document> response;
		Bson filter = null ;
		if (customerId!= null && accountId== null){
		ObjectId objectId = new ObjectId(customerId);
		 filter = Filters.eq (customerIdStr,objectId);
		}
		else if (accountId!= null && customerId== null){
		ObjectId objectId = new ObjectId(accountId);
		 filter = Filters.eq ("AccountId",objectId);
		}
		else if(customerId!= null && accountId!= null){
			ObjectId objectId = new ObjectId(customerId);
			ObjectId objectId2 = new ObjectId(accountId);
			 filter = Filters.and ( eq (customerIdStr,objectId),eq ("AccountId",objectId2));
		}
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection(collection);
				   response = coll.find(filter).limit(1).sort(new BasicDBObject("_id",-1));  
			   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);//ReplytoHashMap.findIterableDocumentToHashMap(response);

			   if(documentList.isEmpty()){
				   log.warn(noInfoInCollection+collection+" filters: "+filter);
			   	}
			   else  {
				   result = (JSONObject) ReplyToJSON.getJsonObjectFromDocumentList(documentList).get(0);
			   }
			   log.debug("get last document from collection " +collection+resultlistStr+StringEscapeUtils.unescapeJson(result.toString()));
			   textToAllureRep("информация о найденном последнем документе из коллекции "+collection,StringEscapeUtils.unescapeJson(result.toString()));
			   return  result; 
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
    		return null;
        }

	 }


	public static List findGeozonesListByPointCoordinates(double lat, double lon, String showOnly) {
		List<JSONObject> result = new ArrayList<JSONObject>();
		FindIterable<Document> response;
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection("Geozones");
			log.debug("looking for geozones located by lon="+lon+" lat="+lat);
				
				// Build query 
				Document query = new Document("GeoInfo", 
				                      new Document("$near", 
				                         new Document("$geometry", 
				                             new Document("type", "Point")
				                                 // Note that the order is "longitude, latitude"
				                                 .append("coordinates", Arrays.asList( lon, lat) )
				                         )
				                         // These distances are in meters.
				                       //  .append("$minDistance", 0)
				                         .append("$maxDistance", 0)//how to  get geozones whose buffers are placed on point's coordinates? 
				        )
				);
				// By default the results are sorted from nearest to farthest.
				if(showOnly==null || showOnly.equals("")){
					response =  coll.find(query);
				}else {
					response =  coll.find(query).projection(new Document(showOnly, 1));
				}
				   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);

				   if(documentList.isEmpty()){
					   log.warn("Nothing was found in mongo! collection: Geozones");
				   	}
				   else  {
					   result = (List<JSONObject>) ReplyToJSON.getJsonObjectFromDocumentList(documentList);
				   }
				   log.debug("geozones found =="+StringEscapeUtils.unescapeJson(result.toString()));
				   textToAllureRep("информация о найденных геозонах расположенных по запрошенной координате ",StringEscapeUtils.unescapeJson(result.toString()));
			return result;
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep("невозможно получить информацию по искомому фильтру в коллекции Geozones" ,e.toString());
            assertTrue(false,e.toString());
    		return null;
        }
		
		
/*		db.Geozones.find(
		        { GeoInfo:
		            {$near:
		                {$geometry:
		                    {type: "Point", coordinates: [37.471703,55.644426]},
		                   $maxDistance: 300
		                }    
		            }
		        },{_id:1,"Name":1}
		        );
*/		

	}
	
	
	public static List selectGeozonesFromListPlacedOnPointCoordinates(List<ObjectId> geozonesList, double lon, double lat, String showOnly) {
		List<JSONObject> result = new ArrayList<JSONObject>();
		FindIterable<Document> response;
		try {
			MongoCollection<Document> coll = MongoConnection.getMongoCollection("Geozones");
			log.debug("selecting geozones from list "+geozonesList+" those are located on coordinate lon="+lon+" lat="+lat);
				// Build query 
				Document query = new Document("GeoInfo", 
				                      new Document("$near", 
				                         new Document("$geometry", 
				                             new Document("type", "Point")
				                                 // Note that the order is "longitude, latitude"
				                                 .append("coordinates", Arrays.asList( lon, lat) )
				                         )
				                         // These distances are in meters.
				                       //  .append("$minDistance", 0)
				                         .append("$maxDistance", 0)//how to  get geozones whose buffers are placed on point's coordinates? 
				        )
				)
				.append("_id", 
						new Document("$in",geozonesList));
				// By default the results are sorted from nearest to farthest.
				if(showOnly==null || showOnly.equals("")){
					response =  coll.find(query);
				}else {
					response =  coll.find(query).projection(new Document(showOnly, 1));
				}
				   List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);

				   if(documentList.isEmpty()){
					   log.warn("Nothing was found in mongo! collection: Geozones");
				   	}
				   else  {
					   result = (List<JSONObject>) ReplyToJSON.getJsonObjectFromDocumentList(documentList);
				   }
				   log.debug("geozones found =="+StringEscapeUtils.unescapeJson(result.toString()));
				   textToAllureRep("информация о найденных геозонах расположенных по запрошенной координате ",StringEscapeUtils.unescapeJson(result.toString()));
				   
			return result;
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep("невозможно получить информацию по искомому фильтру в коллекции Geozones" ,e.toString());
            assertTrue(false,e.toString());
    		return null;
        }
		
/*		
		db.Geozones.find(
		        {_id: {$in: [ObjectId("5978513f5532f42ce4d7cf25"),ObjectId("597851235532f42ce4d7cf23"),ObjectId("597851725532f42ce4d7cf27")]},
		            GeoInfo:
		            {$near:
		                {$geometry:
		                    {type: "Point", coordinates: [37.471703,55.644426]},
		                   $maxDistance: 300
		                }    
		            }
		        },{_id:1,"Name":1}
		        );
*/		
		
		
	}
}
