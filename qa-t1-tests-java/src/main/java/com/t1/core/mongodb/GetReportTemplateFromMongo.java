package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetReportTemplateFromMongo extends AbstractClass{
	protected static String reportTemplates = "ReportTemplates";
	/**
	 * 
	 * @param notificationRuleId
	 * @param showOnly
	 * @return
	 */
	@Step("MongoDB:получение информации о Шаблоне отчета {0}")
	public static List getReportTemplateInfoById(String notificationRuleId, String showOnly){
	return	GetInfoFromMongo.getInfoById(reportTemplates, notificationRuleId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Шаблоне отчета {0}")
	public static List getReportTemplateInfoById(String notificationRuleId){
	return	GetInfoFromMongo.getInfoById(reportTemplates, notificationRuleId,  "");
	}
	
	@Step("MongoDB:получение случайного Шаблона отчета из БД  {0}")
	public static List getRandomReportTemplateInfo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(reportTemplates,getProperty("testCustomerId"), "");
	}
	/**
	 * 
	 * @param inputFilter
	 * @return
	 */
	@Step("MongoDB:получение Шаблона отчета из БД по фильтру {0}")
	public static List getReportTemplateInfoByCustomFilters(Bson inputFilter){
	return	GetInfoFromMongo.getInfoByCustomFilters(reportTemplates, inputFilter,new ArrayList<String>());
	}
	/**
	 * 
	 * @param customerId
	 * @param showOnlyList
	 * @return
	 */
	@Step("MongoDB:получение списка шаблонов отчетов по id клиента {0}")
	public static List getReportTemplateInfoByCustomerId(String customerId, List<String> showOnlyList){
		Bson filter = Filters.and ( eq("CustomerId",new ObjectId(customerId)  )) ;
		return	GetInfoFromMongo.getInfoByCustomFilters(reportTemplates,filter,showOnlyList);
	}
	/**
	 * 
	 * @param customerId
	 * @param showOnlyList
	 * @return
	 */
	
	@Step("MongoDB:получение списка шаблонов отчетов по id клиента {0}, сортировка по времени создания\\изменения")
	public static List getReportTemplateInfoByCustomerIdSortedByDates(String customerId, List<String> showOnlyList){
		Bson filter = Filters.and ( eq("CustomerId",new ObjectId(customerId)  )) ;
		HashMap<String,Integer> sortOrder = new HashMap<String,Integer>();
		sortOrder.put("Updated", -1);
		sortOrder.put("Created", -1);
		return	GetInfoFromMongo.getInfoByCustomFilters(reportTemplates,filter,sortOrder,showOnlyList);
	}
}
