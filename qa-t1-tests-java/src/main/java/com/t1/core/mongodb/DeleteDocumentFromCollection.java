package com.t1.core.mongodb;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.result.DeleteResult;

import ru.yandex.qatools.allure.annotations.Step;

public class DeleteDocumentFromCollection extends MongoConnection {

	



	@Step("MongoDB: удаление документа в {0}")
	public static DeleteResult deleteDocument(String collectionName, Bson filter, long numberOfDocsToDelete) {
		DeleteResult deletedDevHistRec =null;
		try {
			FindIterable<Document> response;
			if(filter != null){
//			log.debug(doc.toString());
				
			long numFilteredDocs = GetInfoFromMongo.countFilteredByCustomFilters(collectionName,filter);
			if (numberOfDocsToDelete>1){
				assertTrue(numFilteredDocs == numberOfDocsToDelete, "wrong number of docs tried to delete!");
			}
			if(numFilteredDocs>0){
			com.mongodb.client.MongoCollection<Document> collection = getMongoCollection(collectionName);

			deletedDevHistRec = collection.deleteMany(filter);
//			log.debug("deletedDevHistRec=="+deletedDevHistRec);
			assertTrue(deletedDevHistRec.getDeletedCount() == numFilteredDocs, "wrong number of docs was deleted!");
			}else {
				log.error("nothing to delete, document "+filter.toString()+" in collection "+collectionName+" wasn't found");
			}
//			 mongoClient.close();
			} else {
				log.error("nothing to delete, filter is empty");
			}
			 return deletedDevHistRec;
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
			return null;
		}
	}
	
}
