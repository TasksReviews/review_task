package com.t1.core.mongodb;

import java.util.List;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class FindGeozones extends AbstractClass{

	
	@Step("MongoDB:получение информации о геозонах по координатам lat{0},  lon{1}")
	public static List getGeozonesInfoBycoordinates(double lat, double lon){
	return	GetInfoFromMongo.findGeozonesListByPointCoordinates(lat, lon,  "");
	}
	
	@Step("MongoDB:получение информации о геозонах по координатам lat{0},  lon{1}")
	public static List getGeozonesInfoBycoordinates(double lat, double lon,String showOnly){
	return	GetInfoFromMongo.findGeozonesListByPointCoordinates(lat, lon,  showOnly);
	}


}
