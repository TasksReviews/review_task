package com.t1.core.mongodb;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetDeviceFullRecordsFromMongo extends AbstractClass{
	private static  String DeviceFullRecords = "DeviceFullRecords";
	
	@Step("MongoDB:получение информации о точке {0}")
	public static List getByDeviceIdAndDates(String deviceId, String dateTimeFrom,String dateTimeTill, String showOnly){
	return	GetInfoFromMongo.getInfoByDeviceIdAndDates(DeviceFullRecords, deviceId, dateTimeFrom, dateTimeTill, showOnly);
	}
	
	@Step("MongoDB:получение информации о точке {0}")
	public static List getByDeviceIdAndDates(String deviceId, String dateTimeFrom,String dateTimeTill){
	return	GetInfoFromMongo.getInfoByDeviceIdAndDates(DeviceFullRecords, deviceId, dateTimeFrom, dateTimeTill, "");
	}
	
	@Step("MongoDB:получение информации о точке по фильтру  {0}")
	public static List getDeviceFullRecordsByCustomFilters(Bson inputFilter){
	return	GetInfoFromMongo.getInfoByCustomFilters(DeviceFullRecords, inputFilter,new ArrayList<String>());
	}
}
