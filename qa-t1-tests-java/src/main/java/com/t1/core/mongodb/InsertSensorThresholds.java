package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.t1.core.ReplyToJSON;
import com.t1.core.TearDownExecutor;

import ru.yandex.qatools.allure.annotations.Step;

public class InsertSensorThresholds extends MongoConnection {

	@Step("MongoDB: вставка документа в SensorThresholds")
	public static org.json.simple.JSONObject insertDocument(Document doc, boolean showLogs) {
		org.json.simple.JSONObject insertedSensorThresholdsRec =null;
		try {
			FindIterable<Document> response;
			if(doc != null){
//			log.debug(doc.toString());
			com.mongodb.client.MongoCollection<Document> collection = getMongoCollection("SensorThresholds");

			 collection.insertOne(doc);
//			 mongoClient.close();
			 ObjectId objectId = new ObjectId(doc.get("DeviceId").toString());
			 Bson inputFilter =  Filters.and (eq ("DeviceId",objectId), eq ("StartDate", doc.get("StartDate") ) );
			 insertedSensorThresholdsRec = (org.json.simple.JSONObject) GetInfoFromMongo.getInfoByCustomFilters("SensorThresholds", inputFilter,new ArrayList<String>(),showLogs).get(0);
			 
//			 log.debug("insertedTrackPoint="+insertedTrackPoint.toString());
			 TearDownExecutor.addinsertedSensorThresholdsRecordsToTeardown(ReplyToJSON.extractPathFromJson(insertedSensorThresholdsRec, "$._id.$oid") );
//			 mongoClient.close();
			} else {
				log.error("nothing to insert, doc is empty");
			}
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
		}
		return insertedSensorThresholdsRec;
	}
}
