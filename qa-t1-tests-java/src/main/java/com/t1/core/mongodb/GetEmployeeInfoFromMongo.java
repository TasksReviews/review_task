package com.t1.core.mongodb;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;

import ru.yandex.qatools.allure.annotations.Step;

public class GetEmployeeInfoFromMongo extends AbstractClass{
	protected static String employees = "Employees";
	
	@Step("MongoDB:получение информации о сотруднике {0}")
	public static List getEmployeeInfoById(String employeeId, String showOnly){
	return	GetInfoFromMongo.getInfoById(employees, employeeId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о сотруднике {0}")
	public static List getEmployeeInfoById(String employeeId){
	return	GetInfoFromMongo.getInfoById(employees, employeeId,  "");
	}
	@Step("MongoDB:получение id последнем созданном сотруднике")
	public static String getLastEmployeeId(){
		
		try {
			JSONObject employee =	GetInfoFromMongo.getLastDocumentFromCollection(employees,getProperty("testCustomerId"),null );
			return ReplyToJSON.extractPathFromJson(employee, "$._id.$oid").toString();
		} catch (Exception e) {
			log.error(ERROR,e);
			return null;
		}
	}
	
	@Step("MongoDB:получение информации о сотрудниках {0}")
	public static List<org.json.simple.JSONObject> getInfoByEmployeeIdList(List<String> employeeIds, String customerId){
		try {
			
			List<org.json.simple.JSONObject> mongoEmployeeInfo = GetInfoFromMongo.getInfoByListOfIds(employees, employeeIds, customerId, null); 
			return mongoEmployeeInfo;

		} catch (Exception e) {
			log.error(ERROR,e);
		}
		return null;
	 }
}
