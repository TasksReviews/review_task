package com.t1.core.mongodb;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetNotifications extends AbstractClass{
	protected static String notifications = "Notifications";
	
	@Step("MongoDB:получение информации о оповещении {0}")
	public static List getNotificationInfoById(String notificationId, String showOnly){
	return	GetInfoFromMongo.getInfoById(notifications, notificationId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о оповещении {0}")
	public static List getNotificationInfoById(String notificationId){
	return	GetInfoFromMongo.getInfoById(notifications, notificationId,  "");
	}
	
	@Step("MongoDB:получение случайного оповещения из БД  {0}")
	public static List getRandomnotificationInfo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(notifications,getProperty("testCustomerId"), "");
	}
	
	@Step("MongoDB:получение случайного оповещения из БД  {0}")
	public static List getNotificationInfoByCustomFilters(Bson inputFilter){
	return	GetInfoFromMongo.getInfoByCustomFilters(notifications, inputFilter,new ArrayList<String>());
	}
	
	@Step("MongoDB:получение последнего оповещения из БД ")
	public static JSONObject getLastNotificationInfo(String accountId){
	return	GetInfoFromMongo.getLastDocumentFromCollection(notifications,null, accountId);
	}

}
