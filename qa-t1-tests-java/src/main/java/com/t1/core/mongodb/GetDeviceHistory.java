package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetDeviceHistory extends AbstractClass{
	protected static String deviceHistory  ="DeviceHistory" ;
	protected static String deviceId  ="DeviceId" ;
	protected static String boundToResourceTime= "BoundToResourceTime";
	
	@Step("MongoDB:получение информации из DeviceHistory по id объекта {0}")
	public static List getDeviceHistoryById(String id, String showOnly){
	return	GetInfoFromMongo.getInfoById(deviceHistory, id,  showOnly);
	}
	
	@Step("MongoDB:получение информации из DeviceHistory по id объекта {0}")
	public static List getDeviceHistoryByObjectId(String id){
	return	GetInfoFromMongo.getInfoById(deviceHistory, id,  "");
	}
	
	@Step("MongoDB:получение информации из DeviceHistory по id ТМУ {0}")
	public static List getDeviceHistoryByDeviceId(String id){
		ObjectId objectId = new ObjectId(id);
		 Bson filter = Filters.and (eq (deviceId,objectId)  );
	return	GetInfoFromMongo.getInfoByCustomFilters(deviceHistory, filter, new ArrayList<String>());
	}
	
	@Step("MongoDB:получение информации из DeviceHistory по ТМУ {0} и датам ")
	public static List getDeviceHistoryByDeviceIdAndDates(String id, String dateFrom, String dateTo ){
	//return	GetInfoFromMongo.getInfoByDeviceIdAndDates(deviceHistory, id,dateFrom,  dateTo,  "");
		try {
			DateFormat format = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
			format.setTimeZone(utc3TimeZone);
			 ObjectId objectId = new ObjectId(id);
			 Bson filter = Filters.and (eq (deviceId,objectId), Filters.gte(boundToResourceTime,format.parse(dateFrom)),Filters.lte(boundToResourceTime,format.parse(dateTo)) );
			 
			 return	GetInfoFromMongo.getInfoByCustomFilters(deviceHistory, filter, new ArrayList<String>());
			} catch (Exception e) {
				log.error(ERROR,e);
			}
			return null;
	}
	
	@Step("MongoDB:получение информации из DeviceHistory по ТМУ {0} и датам ")
	public static List getDeviceHistoryByDeviceIdAndDateTill(String id, String dateTo ){
	//return	GetInfoFromMongo.getInfoByDeviceIdAndDates(deviceHistory, id,dateFrom,  dateTo,  "");
		try {
			DateFormat format = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
			format.setTimeZone(utc3TimeZone);
			 ObjectId objectId = new ObjectId(id);
			 Bson filter = Filters.and (eq (deviceId,objectId), Filters.lte(boundToResourceTime,format.parse(dateTo)) );
			 
			 return	GetInfoFromMongo.getInfoByCustomFilters(deviceHistory, filter, new ArrayList<String>());
			} catch (Exception e) {
				log.error(ERROR,e);
			}
			return null;
	}
	
	@Step("MongoDB:получение информации из DeviceHistory по id Объекта {0} и датам ")
	public static List getDeviceHistoryByObjectIdAndDateTill(String id, String dateTo ){
		try {
		DateFormat format = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		format.setTimeZone(utc3TimeZone);
		 ObjectId objectId = new ObjectId(id);
		 Bson filter = Filters.and (eq ("ResourceId",objectId), Filters.lte(boundToResourceTime,format.parse(dateTo)) );
		 return	GetInfoFromMongo.getInfoByCustomFilters(deviceHistory, filter, new ArrayList<String>());
		} catch (Exception e) {
			log.error(ERROR,e);
		}
		return null;

	}

}
