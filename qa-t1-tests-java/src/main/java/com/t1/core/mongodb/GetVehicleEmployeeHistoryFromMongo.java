package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetVehicleEmployeeHistoryFromMongo extends AbstractClass{

	
	@Step("MongoDB:получение информации о Предыдущих водителях {0}")
	public static List getInfoById(String id, String showOnly){
	return	GetInfoFromMongo.getInfoById("VehicleEmployeeHistory", id,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Предыдущих водителях {0}")
	public static List getInfoByVehicleId(String vehicleId){
		ObjectId objectId = new ObjectId(vehicleId);
		Bson filter = Filters.and(eq("VehicleId", objectId));
		return GetInfoFromMongo.getInfoByCustomFilters("VehicleEmployeeHistory", filter, new ArrayList<String>());

	}
}
