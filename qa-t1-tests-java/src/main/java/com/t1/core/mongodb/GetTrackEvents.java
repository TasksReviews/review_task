package com.t1.core.mongodb;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetTrackEvents extends AbstractClass{

	protected static String trackEvents = "TrackEvents";
	
	@Step("MongoDB:получение информации о событии {0}")
	public static List getNotificationInfoById(String eventId, String showOnly){
	return	GetInfoFromMongo.getInfoById(trackEvents, eventId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о событии {0}")
	public static List getNotificationInfoById(String eventId){
	return	GetInfoFromMongo.getInfoById(trackEvents, eventId,  "");
	}
	
	@Step("MongoDB:получение случайного событии из БД  {0}")
	public static List getRandomnotificationInfo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(trackEvents,getProperty("testCustomerId"), "");
	}
	
	@Step("MongoDB:получение случайного события из БД  {0}")
	public static List getNotificationInfoByCustomFilters(Bson inputFilter){
	return	GetInfoFromMongo.getInfoByCustomFilters(trackEvents, inputFilter,new ArrayList<String>());
	}
	

}
