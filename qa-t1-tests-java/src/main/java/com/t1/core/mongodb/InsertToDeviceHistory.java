package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.t1.core.ReplyToJSON;
import com.t1.core.TearDownExecutor;

import ru.yandex.qatools.allure.annotations.Step;

public class InsertToDeviceHistory extends MongoConnection {

	



	@Step("MongoDB: вставка документа в DeviceHistory")
	public static org.json.simple.JSONObject insertDocument(Document doc, boolean showLogs) {
		try {
			FindIterable<Document> response;
			org.json.simple.JSONObject insertedDevHistRec =null;

			if(doc != null){
//			log.debug(doc.toString());
			com.mongodb.client.MongoCollection<Document> collection = getMongoCollection("DeviceHistory");

			 collection.insertOne(doc);
//			 mongoClient.close();
			 ObjectId objectId = new ObjectId(doc.get("DeviceId").toString());
			 Bson inputFilter =  Filters.and (eq ("DeviceId",objectId), eq ("DeviceTime", doc.get("DeviceTime") ) );
			 insertedDevHistRec = (org.json.simple.JSONObject) GetInfoFromMongo.getInfoByCustomFilters("DeviceHistory", inputFilter,new ArrayList<String>(),showLogs).get(0);
			 
//			 log.debug("insertedTrackPoint="+insertedTrackPoint.toString());
			 TearDownExecutor.addinsertedDevHistRecordsToTeardown(ReplyToJSON.extractPathFromJson(insertedDevHistRec, "$._id.$oid") );
//			 mongoClient.close();
			} else {
				log.error("nothing to insert, doc is empty");
			}
			 return insertedDevHistRec;
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
			return null;
		}
	}
	
}
