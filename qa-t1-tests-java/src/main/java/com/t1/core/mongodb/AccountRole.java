package com.t1.core.mongodb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import com.t1.core.ReplyToJSON;

import ru.yandex.qatools.allure.annotations.Step;

public class AccountRole extends MongoConnection{
	
	@Step("MongoDB:применение роли администратора для аккаунта {0}")
	public static List setAdminRoleToAccountId(String accountId){
	return	setRoleToAccountId("Admin", accountId);
	}
	
	@Step("MongoDB:применение роли разработчика для аккаунта {0}")
	public static List setDeveloperRoleToAccountId(String accountId){
	return	setRoleToAccountId("Developer", accountId);
	}
	/**
	 * 
	 * @param role
	 * @param accountId
	 * @return
	 */
	@Step("MongoDB: изменение роли аккаунта с id {1} на роль {0} ")
	public static List setRoleToAccountId(String role, String accountId){
		ObjectId valueToSet = null;
			Bson filter = Filters.eq ("Name",role );
			JSONObject returnedValue = (JSONObject) GetInfoFromMongo.getInfoByCustomFilters("AccountRoles", filter, Arrays.asList("_id")).get(0);
			try {
				valueToSet= new ObjectId(ReplyToJSON.extractPathFromJson(returnedValue,"_id.$oid").toString());
			} catch (Exception e) {
				log.error(ERROR,e);
		         textToAllureRep("невозможно получить информацию о искомом ID "+accountId ,e.toString());
		         assertTrue(false,e.toString());
			}

		try {
			FindIterable<Document> response;
			String showOnly = "Roles";
			com.mongodb.client.MongoCollection<Document> collection = MongoConnection.getMongoCollection("Accounts");
			filter = Filters.eq("_id", new ObjectId(accountId));// in this  collection  we may change only this  document
			Bson updateField = new Document("$set", new Document("Roles.0", valueToSet));
			UpdateOptions options = new UpdateOptions().upsert(true);
			UpdateResult updateResult = collection.updateOne(filter, updateField, options);
			long matchedCount = updateResult.getMatchedCount();
			long modifiedCount = updateResult.getModifiedCount();
			textToAllureRep("ответ MongoDb", "подходящих документов для обновления=" + matchedCount + ", обновлено полей согласно запрошенным параметрам=" + modifiedCount);
			assertEquals("DB: check 1 document updated",matchedCount, 1, "запросу должно соотвествовать одно поля документа");
			assertTrue(modifiedCount == 1 || modifiedCount == 0, "должно было обновиться не более одного поля документа");
			log.debug("updated " + modifiedCount + " fileds");

			response = collection.find(filter).projection(new Document(showOnly, 1));
			List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);
			textToAllureRep("результат обновления SubsystemsVarsCollection:", ReplyToJSON.getJsonObjectFromDocument(documentList.get(0)).toJSONString());
			return ReplyToJSON.getJsonObjectFromDocumentList(documentList);
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
            textToAllureRep("невозможно получить информацию о искомом ID "+accountId ,e.toString());
            assertTrue(false,e.toString());
    		return null;
        }

	 }
	
	
	@Step("MongoDB:получить все существующие роли аккаунтов")
	public static List<String> getAllExistingRoles(){
			Bson filter = null;
			return  (List<String>) GetInfoFromMongo.getInfoByCustomFilters("AccountRoles", filter, Arrays.asList("_id", "Name"));
	 }

}
