package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;
//import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.t1.core.ReplyToJSON;

import ru.yandex.qatools.allure.annotations.Step;

public class GetAccountInfoFromMongo extends MongoConnection {

	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("GetAccountInfoFromMongo");
	private  static final String ACCOUNTS = "Accounts";
	/**
	 * 
	 * @param accountId
	 * @param showOnly
	 * @return
	 */
	@Step("MongoDB:получение информации для подтверждения аккаунта {0}")
	public static org.json.simple.JSONObject  getConfirmationInfoByAccountId(String accountId, String showOnly){
		log.info("return only last confirmation");
		List<org.json.simple.JSONObject>  resultLst =  getAllConfirmationInfoByAccountId(accountId, showOnly);
		JSONObject result = null;
		if (resultLst != null)
			result= resultLst.get(0);
		return result;
	}
	
	
	@Step("MongoDB:получение информации для подтверждения аккаунта {0}")
	public static List<org.json.simple.JSONObject>  getAllConfirmationInfoByAccountId(String accountId, String showOnly){
		try {
			com.mongodb.client.MongoCollection<Document> coll =MongoConnection.getMongoCollection("Confirmations");
			  ObjectId objectId = new ObjectId(accountId);
			  BasicDBObject fields = new BasicDBObject();
			  FindIterable<Document> response;
			 // fields.put("Code", 1);
			  if(showOnly==null ||showOnly.equals("")){
				  response = coll.find(eq ("AccountId",objectId)).sort(new BasicDBObject("_id", -1));
			  }else{
				  response = coll.find(eq ("AccountId",objectId)).projection(new Document(showOnly, 1)).sort(new BasicDBObject("_id", -1));  
			  }
			  List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);
			  textToAllureRep("информация для подтверждения "+accountId ,ReplyToJSON.getJsonObjectFromDocument(documentList.get(0)).toJSONString());
		//	  mongoClient.close();
			  return ReplyToJSON.getJsonObjectFromDocumentList(documentList);
			    
  		
		}
		catch(Exception e) {
			log.error(ERROR,e);
          mongoClient.close();
  		return null;
      }
	}
	
	
	
	/**
	 * 
	 * @param accountId
	 * @param showOnly
	 * @return
	 */
	@Step("MongoDB:получение информации о последней доставке кода подтверждения для {1} посредством {0}.")
	public static org.json.simple.JSONObject getLastDeliveryNotificationByReceiverContact(String channelType,String receiverContact, String showOnly) {
		org.json.simple.JSONObject reply = null;
		BasicDBObject query = new BasicDBObject();
		query.put("ReceiverContact", new BasicDBObject("$eq", receiverContact));
		
		 List<JSONObject> deliverNotif = getDeliveryNotificationByReceiverContact(query, channelType,receiverContact, showOnly);
		 if(deliverNotif!=null){
			reply = deliverNotif.get(0);
			assertEquals("DB: check ChannelType",reply.get("ChannelType"), channelType, "использован другой способ связи");
		 } else {
			 assertTrue(false, "информация о доставке кода подтверждения не найдена");
		 }
		
		return reply;
	}
	
	
	
	/**
	 * 
	 * @param accountId
	 * @param showOnly
	 * @return
	 */
	@Step("MongoDB:получение информации о всех доставках кодов подтверждения для {1} посредством {0}.")
	public static List<org.json.simple.JSONObject> getAllDeliveryNotificationByReceiverContactAndChannelType(String channelType,String receiverContact, String showOnly) {
		
		BasicDBObject query = new BasicDBObject();
		List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("ReceiverContact", receiverContact));
		obj.add(new BasicDBObject("ChannelType", channelType));
		query.put("$and", obj);
	
		List<org.json.simple.JSONObject> reply = getDeliveryNotificationByReceiverContact(query, channelType,receiverContact, showOnly);
		return reply;
	}
	
	
	
	public static List<org.json.simple.JSONObject> getDeliveryNotificationByReceiverContact(BasicDBObject query, String channelType,String receiverContact, String showOnly) {
		List<org.json.simple.JSONObject> reply = null;
		try {
			log.debug("get NotificationDeliveries info from mongo by " + channelType + ":" + receiverContact);
			String codeDeliveredInfo = "информация о доставке кода подтверждения ";

			FindIterable<Document> response;

			for (int i = 0; i < 10; i++) {
				com.mongodb.client.MongoCollection<Document> coll = MongoConnection.getMongoCollection("NotificationDeliveries");
				if (showOnly ==null || showOnly.equals("")) {
					response = coll.find(query).sort(new BasicDBObject("_id", -1));
				} else {
					response = coll.find(query).projection(new Document(showOnly, 1))
									.sort(new BasicDBObject("_id", -1));
				}

				List<Document> documentList = MongoConnection.findIterableDocumentToDocumnentsList(response);// ReplytoHashMap.findIterableDocumentToHashMap(response);
				if (!documentList.isEmpty()) {
					reply = ReplyToJSON.getJsonObjectFromDocumentList(documentList);

					log.debug(reply);
					textToAllureRep(codeDeliveredInfo, reply.toString());
				} else {
					String tmpTxt= " не найдена, channelType: " + channelType + " receiverContact: " + receiverContact;
					textToAllureRep("информация о доставке ",tmpTxt	);
					log.error(codeDeliveredInfo +tmpTxt);
					assertTrue(false, codeDeliveredInfo +tmpTxt);
					break;
				}

				if (reply.get(0).get("State").equals("DeliveryDoneOk")) {
					break;
				} else {
					log.debug("delivery state!=DeliveryDoneOk, check it in db one more time in 500ms");
					Thread.sleep(500);
				}

			}
			return reply;

		} catch (Exception e) {
			mongoClient.close();
			log.error(ERROR,e);
			return null;
		}
	}
	/**
	 * 
	 * @param accountId
	 * @param showOnly
	 * @return
	 */
	@Step("MongoDB:получение информации об аккаунте {0}")
	public static List<org.json.simple.JSONObject> getAccountInfoByAccountId(String accountId, String showOnly){
	return	GetInfoFromMongo.getInfoById(ACCOUNTS, accountId,  showOnly);
	 }

	public List<org.json.simple.JSONObject> getAccountInfoFromMongo(String accountId) {
		try {
			List<org.json.simple.JSONObject> mongoAccountInfo = getAccountInfoByAccountId(accountId, "");// "Contacts");
			return mongoAccountInfo;

		} catch (Exception e) {
			log.error(ERROR,e);
		}
		return null;
	}

	@Step("MongoDB:получение информации об аккаунтах {0}")
	public static List<org.json.simple.JSONObject> getAccountInfoByAccountIdList(List<String> accountIds, String customerId){
		try {
			
			List<org.json.simple.JSONObject> mongoAccountInfo = GetInfoFromMongo.getInfoByListOfIds(ACCOUNTS, accountIds, customerId, null); 
			return mongoAccountInfo;

		} catch (Exception e) {
			log.error(ERROR,e);
		}
		return null;
	 }


	
	@Step("получение информации из коллекции Confirmations")
	public static JSONObject getAccountConfirmationInfoFromMongo(String accountId, String phone, String email) {
		List<JSONObject> mongoConfirmationList = new ArrayList();
		JSONObject mongoConfirmation = null;
		try {

			mongoConfirmationList = getAllConfirmationInfoByAccountId(accountId, "");
			if (mongoConfirmationList != null) {
				mongoConfirmation = mongoConfirmationList.get(0);
				mongoConfirmation.put("NumberOfConfirmations", mongoConfirmationList.size());
				// log.debug("MongoCollection:'Confirmations':" +  mongoConfirmation.toString());
				String confimationCode = ReplyToJSON.extractPathFromJson(mongoConfirmation, "$.Code");// "$.Contacts[0].Value");
				if (ReplyToJSON.extractPathFromJson(mongoConfirmation, "$.Operation").equals("confirmContact")) {
					String contactType = ReplyToJSON.extractPathFromJson(mongoConfirmation, "$.Params.Type");
					if (contactType.equals("Phone")) {
						assertEquals("check phone",ReplyToJSON.extractPathFromJson(mongoConfirmation, "$.Params.Value"), phone, "phone не верен");
					} else if (contactType.equals("Email")) {
						assertEquals("check email", ReplyToJSON.extractPathFromJson(mongoConfirmation, "$.Params.Value"), email, "email не верен");
					} else {
						assertTrue(false, "unexpected contact type:" + contactType);
					}

				} else {
					assertTrue(ReplyToJSON.extractPathFromJson(mongoConfirmation, "$.Operation").equals("passwordChange"),
							"операция проверки подтверждения не реализована!");
				}
			} else {
				assertTrue(false, "unexpected zero NumberOfConfirmations");
			}

		} catch (Exception e) {
			log.error(ERROR,e);
		}
		return mongoConfirmation;
	}

	@Step("MongoDB:получение случайного аккаунта из БД  {0}")
	public static List getRandomAccountInfo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(ACCOUNTS,null, "");
	}

	@Step("MongoDB:получение случайного аккаунта принадлежаего тестовому клиенту из БД")
	public static List getRandomAccountInfoTestCustomer(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(ACCOUNTS,getProperty("testCustomerId"), "");
	}

	@Step("MongoDB:получение случайного аккаунта принадлежаего клиенту {0} из БД")
	public static List getRandomAccountInfoByCustomer(String customerId){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(ACCOUNTS,customerId, "");
	}
	
	@Step("MongoDB:получение списка аккаунтов принадлежащих клиенту {0} из БД")
	public static List getAccountsListInfoByCustomer(String customerId){
		Bson filter = Filters.and ( eq("CustomerId",new ObjectId(customerId)  )) ;
	return	GetInfoFromMongo.getInfoByCustomFilters(ACCOUNTS,filter, new ArrayList<String>());
	}
	
	@Step("MongoDB:получение количества аккаунтов принадлежащих клиенту {0} из БД")
	public static Long getAccountsCountInfoByCustomer(String customerId){
		Bson filter = Filters.and ( eq("CustomerId",new ObjectId(customerId)  )) ;
	return	GetInfoFromMongo.countFilteredByCustomFilters(ACCOUNTS,filter);
	}
	
	@Step("MongoDB:получение общего количества аккаунтов из БД")
	public static Long getAccountsCountInfo(){
	return	GetInfoFromMongo.countFilteredByCustomFilters(ACCOUNTS, null);
	}

	@Step("MongoDB:получение всех аккаунтов из БД")
	public static List<JSONObject> getAllAccounts() {
		return	GetInfoFromMongo.getInfoByCustomFilters(ACCOUNTS,null, new ArrayList<String>());
	}

	@Step("MongoDB:получение всех ролей аккаунтов из БД")
	public static List<JSONObject> getAllRoles() {
		return	GetInfoFromMongo.getInfoByCustomFilters("AccountRoles",null, new ArrayList<String>());
	}
	@Step("MongoDB:получение информации о роли из БД")
	public static JSONObject getRole(String roleId) {
		Bson filter = Filters.and ( eq("_id",new ObjectId(roleId)  )) ;
		return	(JSONObject)GetInfoFromMongo.getInfoByCustomFilters("AccountRoles",filter, new ArrayList<String>()).get(0);
	}
}
