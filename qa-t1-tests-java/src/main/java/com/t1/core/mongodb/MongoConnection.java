package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.exists;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bson.Document;
//import org.bson.codecs.configuration.CodecRegistryHelper.fromCodec;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.google.common.annotations.Beta;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.MongoSocketReadException;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;

import ru.yandex.qatools.allure.annotations.Step;

 
//db.serverStatus()
// qa : TIcfHm8U9cuJwzaScexk   >>> tail -f /var/log/mongo/mongod.log | ccze -A
public class MongoConnection extends AbstractClass{
	
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("MongoConnection");
	static String dbhost =getProperty("dbhost");
	static Integer dbport =Integer.parseInt(getProperty("dbport"));
	static String databaseName =getProperty("dbname");
	static String dbuserName =getProperty("dbuser");
	final static	char[] dbpassword = getProperty("dbpass").toCharArray();
	static String dbuserAdmin =getProperty("dbuserAdmin");
	final static	char[] dbpasswordAdmin = getProperty("dbpasswordAdmin").toCharArray();
	static MongoClient mongoClient = null;  // static may cause issues on parallel executions?
	static int dbmaxConnectionIdleTime =Integer.parseInt(getProperty("dbmaxConnectionIdleTime"));
	static int dbmaxTime =Integer.parseInt(getProperty("dbmaxTime"));
	
	public static List<Document> findIterableDocumentToDocumnentsList(FindIterable<Document> response) throws Exception{
		List results =  new ArrayList();
		response.maxTime(50, TimeUnit.SECONDS);
		response.maxAwaitTime(50, TimeUnit.SECONDS);
		try{
			//coll.find(eq ("_id",objectId)).forEach((Block<Document>) document -> {	  results.add(document);	});
			//List<Document> response = coll.find(eq ("_id",objectId)).into(new ArrayList<Document>());
		MongoCursor<Document> cursor = response.iterator();
		while(cursor.hasNext()){
        	results.add(cursor.next());
	        }

		}catch (MongoSocketReadException e) {
			
			log.error(ERROR,e);
			response.toString();
			assertTrue(false,"невозможно получить корректный документ из MongDB");
		}
		return results;
	}
	

	
	/**
	 * 
	 * @param field
	 * @param valueToSet
	 * @param showOnly
	 * @return
	 */
	@Step("MongoDB:изменение SubsystemsVarsCollection,поле:{0},на:{1}")
	public static org.json.simple.JSONObject updateSubsystemsVarsCollection(String field, String valueToSet, String showOnly) {
		try {
			FindIterable<Document> response;
			if(showOnly==null || showOnly.equals("")){
				showOnly = "AuthConfig";
			}
			org.json.simple.JSONObject result = null;
			com.mongodb.client.MongoCollection<Document> collection = getMongoCollection("SubsystemsVarsCollection");
			if(collection !=null){
//			db.getCollection('SubsystemsVarsCollection').find({  $and: [  { "_t" : "SubsystemVars`1" }  ,{"AuthConfig.AuthFields": { $exists: true }}
			Bson filter = Filters.and(eq("_t","SubsystemVars`1"), exists("AuthConfig.AuthFields")  ); //shold return only 1 document, "58f0d66244d3900ea89f47f3" on QA
//			Bson filter = Filters.eq("_id", new ObjectId("58f0d66244d3900ea89f47f3"));// in this  collection  we may change only this  document
			Bson updateField1 = new Document("$set", new Document(field, valueToSet));
			UpdateOptions options = new UpdateOptions().upsert(true);
			UpdateResult updateResult = collection.updateOne(filter, updateField1, options);
			long matchedCount = updateResult.getMatchedCount();
			long modifiedCount = updateResult.getModifiedCount();
			textToAllureRep("ответ MongoDb", "подходящих полей для обновления=" + matchedCount + ", обновлено полей согласно запрошенным параметрам=" + modifiedCount);
			assertEquals("DB: check 1 document updated",matchedCount, 1, "запросу должно соотвествовать одно поля документа");
			assertTrue(modifiedCount == 1 || modifiedCount == 0, "должно было обновиться не более одного поля документа");
			log.debug("updated " + modifiedCount + " fileds");

			response = collection.find(filter).projection(new Document(showOnly, 1));

			List<Document> documentList = findIterableDocumentToDocumnentsList(response);
			if(documentList != null){
				result = ReplyToJSON.getJsonObjectFromDocument(documentList.get(0));
				textToAllureRep("результат обновления SubsystemsVarsCollection:", result.toJSONString());
			}
			}else {
				log.error("wasn't able to connect to collection.");
			}
			
			return result;
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
			return null;
		}
	}
	
	
	@Step("MongoDB:изменение документа:Коллекция {0} документ:{0},новые поля:{2}")
	public static org.json.simple.JSONObject updateCollection(String colletction, Bson filter, HashMap<String,Object> fieldsToUpdate) {
		try {
			FindIterable<Document> response;

			org.json.simple.JSONObject result = null;
			com.mongodb.client.MongoCollection<Document> collection = getMongoCollection(colletction);
			if(collection !=null && filter != null && fieldsToUpdate!= null){

				response = collection.find(filter);
				List<Document> documentList = findIterableDocumentToDocumnentsList(response);
				if(documentList != null && ! documentList.isEmpty()){
					result = ReplyToJSON.getJsonObjectFromDocument(documentList.get(0));
					log.debug("before update:"+result);
				}
	
				BasicDBObject updateFields = new BasicDBObject();
				for(String key : fieldsToUpdate.keySet()){
					updateFields.append(key, fieldsToUpdate.get(key));
				}
				BasicDBObject setQuery = new BasicDBObject();
				setQuery.append("$set", updateFields);

			UpdateOptions options = new UpdateOptions().upsert(false);
			UpdateResult updateResult = collection.updateOne(filter, setQuery, options);

			assertTrue(updateResult.getMatchedCount()==1,"подходящих для обновления "+updateResult.getMatchedCount()+" документов, ожидалось 1");
			assertTrue(updateResult.getModifiedCount()==1,"обновлено "+updateResult.getModifiedCount()+" документов, ожидалось 1");
			response = collection.find(filter);
			documentList = findIterableDocumentToDocumnentsList(response);
			if(documentList != null){
				result = ReplyToJSON.getJsonObjectFromDocument(documentList.get(0));
				log.debug("after update:" +result);
				textToAllureRep("результат обновления SubsystemsVarsCollection:", result.toJSONString());
			}

			}else {
				log.error("wasn't able to update collection "+colletction+", for doc: "+filter);
			}
			setToAllureChechkedFields();
			return result;
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
			setToAllureChechkedFields();
			return null;
		}
	}
	
	public static com.mongodb.client.MongoCollection<Document> getMongoCollection(String collection){
		try {
			//too much connections causes "errno:11 Resource temporarily unavailable", so only 1 connection has to be opened for entire build.
			if(mongoClient==null || mongoClient.getAddress().toString()==""){	
			 MongoCredential credential = MongoCredential.createScramSha1Credential(dbuserAdmin, databaseName, dbpasswordAdmin);
			  mongoClient = new MongoClient(new ServerAddress(dbhost , dbport),  
			        Arrays.asList(credential),setOpts()); 
			}
			  MongoDatabase db = mongoClient.getDatabase(databaseName);
			  return db.getCollection(collection);
		}
		catch(java.lang.IllegalStateException e){
			log.warn("Unexpectedly lost connection to DB, connect again.");
			 MongoCredential credential = MongoCredential.createScramSha1Credential(dbuserAdmin, databaseName, dbpasswordAdmin); 
			  mongoClient = new MongoClient(new ServerAddress(dbhost , dbport),  
			        Arrays.asList(credential),setOpts());
			  MongoDatabase db = mongoClient.getDatabase(databaseName);
			  return db.getCollection(collection);
		}
		catch(Exception e) {
            log.error("Not able to establish connection!",e);
            if(mongoClient!=null)
            	mongoClient.close();
    		return null;
		}
		
	}
/*	public static com.mongodb.client.MongoCollection<Document> getMongoCollectionAsAdmin(String collection){
		try {
			 MongoCredential credential = MongoCredential.createScramSha1Credential(dbuserAdmin, databaseName, dbpasswordAdmin); 
			  mongoClient = new MongoClient(new ServerAddress(dbhost , dbport),  
			        Arrays.asList(credential),setOpts()); 
			  MongoDatabase db = mongoClient.getDatabase(databaseName);
			  return db.getCollection(collection);
		}
		catch(Exception e) {
            e.printStackTrace();
            mongoClient.close();
    		return null;
		}
	}
*/
	
	public static String connectAndSelectFromMongo() throws UnknownHostException {
		MongoCredential credential = MongoCredential.createScramSha1Credential(dbuserName, databaseName, dbpassword);
		mongoClient = new MongoClient(new ServerAddress(dbhost, dbport), Arrays.asList(credential), setOpts());
		MongoDatabase db = mongoClient.getDatabase(databaseName);
		com.mongodb.client.MongoCollection<Document> coll = db.getCollection("record");

		FindIterable<Document> response = coll.find(eq("Properties.Value", 44)).limit(2).skip(8);
		String stringResponse = com.mongodb.util.JSON.serialize(response);
		return stringResponse;
	}
	 
	 public static String deleteDocument( String collectionName, String id ) {
		return deleteDocument( collectionName, id ,false);
	 }

	@Step("MongoDB: удаление документа")
	public static String deleteDocument(String collectionName, String id, Boolean removeFromTeardownIf1attemptFiled) {
		com.mongodb.client.MongoCollection<Document> collection = getMongoCollection(collectionName);
		DeleteResult deleteOneResult = null;
		if (collection != null) {
			ObjectId objectId = new ObjectId(id);
			Bson filter = Filters.and(eq("_id", objectId));
			deleteOneResult = collection.deleteOne(filter);

			if (deleteOneResult.getDeletedCount() != 1) // remove from teradown first attempt failed or id has been deleted
			{
				log.error("wasn't able to delete " + id + " from collection " + collectionName + ". total deleted:"
						+ deleteOneResult.getDeletedCount());
			} else {
				log.debug(id + " in " + collectionName + " deleted");
			}
			assertEquals("DB:trackpoints deleted", deleteOneResult.getDeletedCount(), 1,
					"1 документ должен был быть удален!");
		}
		return (deleteOneResult != null) ? deleteOneResult.toString() : null;
	}

/*	 
	 @Beta
	 public static List<HashMap> connectMongoAndExecuteJS(String js){
		HashMap<String, Object> resultMap=null;
		// example for js syntax: 
		  //TODO: check this if works correct for us
		 //using: http://jongo.org/
		
		HashMap mongoRes = new HashMap();
		 List results =  new ArrayList();
		 Integer limit=null;
	   final Document sort=null; 
		 
		try {
			 MongoCredential credential = MongoCredential.createScramSha1Credential(dbuserName, databaseName, dbpassword); 
			  mongoClient = new MongoClient(new ServerAddress(dbhost , dbport),  
			        Arrays.asList(credential),setOpts()); 

			  
			  MongoDatabase db = mongoClient.getDatabase(databaseName); 
			  //db.runCommand(command)
//			  JongoApi jongo =  Jongo.with(database, mapper);
//			  com.mongodb.client.MongoCollection<Friend> friends = jongo.getCollection("friends", Friend.class);
//			  Friend robert = collection.find(jongo.query("{name:'#'}", "Robert")).first();
//			  
//			  Jongo jongo = new Jongo(db3);
//			 org.jongo.MongoCollection friends = jongo.getCollection("accounts");
//			 // MongoCursor<Friend> 
			//  MongoCursor<String> all = friends.find("{\"Login\": \"lOIYgkTlOj\"}").as(String.class);
			  
			  
			  //all.next().get
		//	  log.info("all==="+all);
			  
			  
//			  Code functionCode = new Code (js) ; 
//			  DBObject doc = new BasicDBObject()//(functionCode);
			  
			  
			  
		/*	   ListCollectionsIterable<Document> list = db.listCollections(); 
			   //  "db.getCollection('accounts').find({\"_id\": ObjectId(\"58b40bc65532f309fc6c92d5\")});";
			   MongoCollection<Document> coll = db.getCollection("confirmation");
		//	   log.info(coll.count());
			   
			   BasicDBObject query = new BasicDBObject(); 
			  // BasicDBObject fields = new BasicDBObject("Properties.Value",44);
			    //ObjectId("58b432df5532f30af405b77d")
			   ObjectId config = new ObjectId("58b432df5532f30af405b77d");
			   
			  FindIterable<Document> response = coll.find(eq ("AccountId",config));//.limit(2).skip(8);
			 // FindIterable<Document> response = coll.find(eq ("Params.Value","75345555322")).limit(2).skip(8);
			 // FindIterable<Document> response2 = coll.find(gte ("Properties.Value",44));
			  if (sort != null) {
				  response.sort(sort);
				  }
				  if (limit != null) {
					  response.limit(limit);
				  }

//				  if (LOG.isDebugEnabled()) {
//				    LOG.debug("Fetch from '{}.{}' query {}", dbName, collectionName, execQuery);
//				  }
			  
			  
			  
			  
			  
			  log.info("keyset=="+ response.first().keySet());
			  log.info("total=="+ coll.count());
			  log.info("first device=="+ response.first().get("Code"));
			  int tmp =0;
		        for (Document document : response) {
		        	//for (String key : document.keySet()) {
		        	
		        	//	mongoRes.put(key, document.get(key));
		       // }
//		        		 JsonFactory factory = new JsonFactory();
//
//		 		        ObjectMapper mapper = new ObjectMapper(factory);
//		 		        JsonNode rootNode = mapper.readTree(JSON.serialize(document));  
//		 		        resultMap = mapper.convertValue(rootNode, HashMap.class);
		        	
		        	

		        	
					results.add(ReplytoHashMap.documentToHashMap(document));
			        }
		        
			  
			  /*for(Document current: response){
				  
				  log.info(current.get("_id"));
				  log.info(current.get("DeviceId"));
				  log.info(current.get("Time"));
				  log.info(current.get("Properties"));
				  ++tmp;
				  if (tmp>10){continue;}
			  }
			//  Array keyset = response.first().keySet();
			   
			String response ="";

			 
			  
			  log.info("==================\r\n"+com.mongodb.util.JSON.serialize(response));
			//   String stringResponse = JSON.serialize(response);

			 
			  
//			   mongoClient.close(); 
			  // return stringResponse;
			  return results; 
		        

		       
		        
			   
		  
//		final BasicDBObject command = new BasicDBObject();
//        command.put("eval", String.format(js, ""));
//        Document result = db.runCommand(command);
//		 
		 
		
		
		
		}
		catch(Exception e) {
          //  log.error(ExceptionUtils.getStackTrace(e));
			log.error(ERROR,e);
        }
		
		return null;
	 }
*/	 
	
	 
	 
	 /**
	  *  can process only filters like {\"Login\": \"lOIYgkTlOj\"}  parse errors for ISODate, ObjectId etc.
	  * @param query
	  * @return
	  */
	@Beta
	public static String connectAndExecuteSimpleFilter(String query){
		
		// example for js syntax: 
		  //TODO: check this if works correct for us
	
		String stringResponse="";
		try {

		MongoCredential credential = MongoCredential.createScramSha1Credential(dbuserName, databaseName, dbpassword); 
		  mongoClient = new MongoClient(new ServerAddress(dbhost , dbport),  
		        Arrays.asList(credential),setOpts()); 
		  MongoDatabase db = mongoClient.getDatabase("ToneMainTest"); 
		 // String query = "{\"Login\": \"lOIYgkTlOj\"},{\"Contacts\":1}";
				  //"{\"Created\" : {$gt : ISODate(\"2017-02-28T13:17:59.046Z\")}}";//"{\"Login\": \"lOIYgkTlOj\"},{\"Contacts\":1}";
		  com.mongodb.client.MongoCollection<Document> coll = db.getCollection("accounts");
		  FindIterable<Document> response = coll.find((Bson) com.mongodb.util.JSON.parse(query));
		  

		  for(Document current: response){
		  
		  log.info(">>"+current.get("_id"));
		  log.info(">>"+current.get("Login"));

	  }

	 
	  
	  log.info("==================\r\n"+com.mongodb.util.JSON.serialize(response));
	   stringResponse = com.mongodb.util.JSON.serialize(response);
		
		}
		catch(Exception e) {
			log.error(ERROR,e);
        }
		return stringResponse;
	 }

	@Beta
	//not tested, probably does not work
	public static org.json.simple.JSONObject executeMongoSavedScript(String inputCommand){
		try {
			 MongoCredential credential = MongoCredential.createScramSha1Credential( dbuserName, databaseName, dbpassword); 
			  mongoClient = new MongoClient(new ServerAddress(dbhost , dbport),  
			        Arrays.asList(credential),setOpts()); 
			  MongoDatabase db = mongoClient.getDatabase(databaseName);
			  
			  final BasicDBObject command = new BasicDBObject();
			  //command.put("eval", String.format("function() { %s return;}}, {entity_id : 1, value : 1, type : 1}).forEach(someFun); }"));
	            command.put("eval", String.format(inputCommand));
	            Document docResult = db.runCommand(command);
	            org.json.simple.JSONObject result = ReplyToJSON.getJsonObjectFromDocument(docResult);
			  return result;
		}
		catch(Exception e) {
			log.error(ERROR,e);
            mongoClient.close();
    		return null;
		}
	}
	
	
	@Beta
	public static void execMongo()  {

		Runtime rt = Runtime.getRuntime();
        try {
        	
        	 MongoCredential credential = MongoCredential.createScramSha1Credential(dbuserName, databaseName, dbpassword); 
			  mongoClient = new MongoClient(new ServerAddress(dbhost , dbport),  
			        Arrays.asList(credential),setOpts()); 

			  MongoDatabase db = mongoClient.getDatabase(databaseName); 

            // the query you want to run in mongo, you can get it 
            // from a file using a FileReader
            String query = "db.getCollection('accounts').find({\"Login\": \"lOIYgkTlOj\"});";

            // the database name you need to use
          //  String db = "database";

            // run a command from terminal. this line is equivalent to 
            // mongo database --eval "db.col.find()" 
            // it calls the mongo binary and execute the javascript you 
            // passed in eval parameter. It should work for both unix and 
            // windows 
            Process pr = rt.exec(new String[]{"mongo", databaseName, "--eval", query});
            // read the output of the command
            InputStream in = pr.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }

            // print the command and close outputstream reader
            log.info(">>>>>>"+out.toString());   
            reader.close();
        } catch (IOException ex) {
        	log.debug(ex);
        }
	}
	
	
	public static MongoClientOptions setOpts(){
		MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
        //build the connection options  
        builder.maxConnectionIdleTime(dbmaxConnectionIdleTime);//set the max wait time in (ms)
        builder.connectTimeout(dbmaxTime);//ms
        builder.maxWaitTime(dbmaxTime);

//        builder.codecRegistry( CodecRegistries.fromRegistries(
//        		CodecRegistries.fromCodecs(new UuidCodec(UuidRepresentation.STANDARD)), MongoClient.getDefaultCodecRegistry()));
        
        MongoClientOptions opts = builder.build();
        return opts;
	}
}

