package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetDevices extends AbstractClass{
	protected static String devices = "Devices";
	
	@Step("MongoDB:получение информации о ТМУ по id объекта {0}")
	public static List getDeviceById(String id, String showOnly){
	return	GetInfoFromMongo.getInfoById(devices, id,  showOnly);
	}
	
	@Step("MongoDB:получение информации о последнем ТМУ по id объекта {0}")
	public static List getLastDeviceById(String id, String showOnly){
	return	GetInfoFromMongo.getInfoById(devices, id,  showOnly);
	}

	@Step("MongoDB:получение количества ТМУ принадлежащих клиенту {0} из БД")
	public static Long getDevicesCountInfoByCustomer(String customerId){
		Bson filter = Filters.and ( eq("CustomerId",new ObjectId(customerId)  )) ;
	return	GetInfoFromMongo.countFilteredByCustomFilters(devices,filter);
	}
	
	@Step("MongoDB:получение общего количества ТМУ из БД")
	public static Long getDevicesCountInfo(){
	return	GetInfoFromMongo.countFilteredByCustomFilters(devices,null);
	}

}
