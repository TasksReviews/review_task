package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.ReplyToJSON;
import com.t1.core.TearDownExecutor;

import ru.yandex.qatools.allure.annotations.Step;

public class InsertTrackPointToDeviceFullRecords extends MongoConnection {

	private  static String deviceTime = "DeviceTime";
	private  static String deviceId = "DeviceId";
	private  static String deviceFullRecords = "DeviceFullRecords";
	private  static final String PATH_ID = "$._id.$oid";
	
 public static Document prepareDoc(org.json.simple.JSONObject fieldsToUpdate, String stringDateTime, String objectIdValue, String deviceCode){
	 try {
	 ObjectId objectId = new ObjectId(objectIdValue);
	 Date trackPointTime = null;
	  DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		format.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));

		if (stringDateTime == null || stringDateTime.equals("")){
			 trackPointTime =new Date(System.currentTimeMillis() + 60 * 1000); //+1 min (due to bamboo vs BE time asynch >1.5sec, that fails tests)
		}else {
		 trackPointTime = new Date(format.parse(stringDateTime).getTime() + (100 * 1 ));//+100 m.sec 
		}
	 Document doc = new Document(deviceTime , trackPointTime)
				.append("ReceivedTime" , trackPointTime) 
				.append("DeviceCode" , deviceCode) 
				.append(deviceId , objectId) 
				.append("xgeo_Speedlimit" , 0) 
				.append("xgeo_Goezones" , Arrays.asList()) 
				.append("xgeo_SafetyObjects" , Arrays.asList()) 
				.append("Longitude" , 37.999210357666016) 
				.append("Longitude_upd" , true) 
				.append("Latitude" , 55.9251594543457) 
				.append("Latitude_upd" , true) 
				.append("Ignition" , false) 
				.append("Ignition_upd" , false) 
				.append("Course" , 0) 
				.append("Course_upd" , false) 
				.append("Mileage_upd" , false) 
				.append("Satellites" , 3) 
				.append("Satellites_upd" , true) 
				.append("Speed" , 0) 
				.append("Speed_upd" , false) 
				.append("AccelerationX_upd" , false) 
				.append("AccelerationY_upd" , false) 
				.append("AccelerationDurationX_upd" , false) 
				.append("AccelerationDurationY_upd" , false) 
				.append("FuelLevel_upd" , false) 
				.append("FuelCAN_upd" , false) 
				.append("FuelSpent_upd" , false) 
				.append("Fuel" , "100") 
				.append("Fuel_upd" , false) 
				.append("FuelLevel2_upd" , false) 
				.append("FuelLevel3_upd" , false) 
				.append("GeoLocationSignalLevel_upd" , false) 
				.append("GsmSignalLevel_upd" , false) 
				.append("Voltage_upd" , false) 
				.append("VoltageAlarm_upd" , false) 
				.append("BatteryLevel_upd" , false) 
				.append("BatteryLevelAlarm_upd" , false) 
				.append("DoorsSignal_upd" , false) 
				.append("HoodSignal_upd" , false) 
				.append("TrunkSignal_upd" , false) 
				.append("Rpm_upd" , false) 
				.append("Temperature1_upd" , false) 
				.append("Temperature2_upd" , false) 
				.append("Temperature3_upd" , false) 
				.append("Odometr_upd" , false) 
				.append("PanicButton_upd" , false) 
				.append("Button1_upd" , false) 
				.append("Button2_upd" , false) 
				.append("Button3_upd" , false) 
				.append("Button4_upd" , false) 
				.append("HeaterSignal_upd" , false) 
				.append("LeftBackDoor_upd" , false) 
				.append("LeftFrontDoor_upd" , false) 
				.append("RightBackDoor_upd" , false) 
				.append("RightFrontDoor_upd" , false);
	 
	 
	 
	 if(!fieldsToUpdate.isEmpty()){
		for(Object  keytoUpdate: fieldsToUpdate.keySet()){
		        doc.put(keytoUpdate.toString(), fieldsToUpdate.get(keytoUpdate.toString()));
		}
	 }
	 
	 return doc;
	 } catch (Exception e) {
		 log.error(ERROR,e);
			return null;
		}
 }
 
 //And Взять точки из файла <шаблон поездок с событиями> и скопировать их в коллекцию deviceFullRecords <TracksCount> раз, по <TraksCount> разу в <DaysCount> дней
 public static JSONObject insertPoint(String collectionName, Document  doc, String deviceIdValue) {
	 org.json.simple.JSONObject insertedTrackPoint =null;
	 com.mongodb.client.MongoCollection<Document> collection = getMongoCollection(collectionName);
	 collection.insertOne(doc);
	 Bson inputFilter =  Filters.and (eq (deviceId,new ObjectId(deviceIdValue)), eq (deviceTime, doc.get(deviceTime) ) );
	 insertedTrackPoint = (org.json.simple.JSONObject) GetInfoFromMongo.getInfoByCustomFilters(deviceFullRecords, inputFilter,new ArrayList<String>()).get(0);
	 try {
		TearDownExecutor.addInsertedTrackPointToTeardown(ReplyToJSON.extractPathFromJson(insertedTrackPoint, PATH_ID) );
	} catch (Exception e) {
		log.error(ERROR,e);
	}
	 return insertedTrackPoint;
 }
 

	@Step("MongoDB: вставка документа в DeviceFullRecords")
	public static org.json.simple.JSONObject insertDocument(org.json.simple.JSONObject fieldsToUpdate, String collectionName,String stringDateTime, String objectIdValue, String deviceCode ) {
		try {
			org.json.simple.JSONObject insertedTrackPoint =null;
			Document doc = prepareDoc(fieldsToUpdate, stringDateTime,  objectIdValue,  deviceCode);
			if(doc != null){
			com.mongodb.client.MongoCollection<Document> collection = getMongoCollection(collectionName);
			 collection.insertOne(doc);
			 ObjectId objectId = new ObjectId(objectIdValue);
			 Bson inputFilter =  Filters.and (eq (deviceId,objectId), eq (deviceTime, doc.get(deviceTime) ) );
			 insertedTrackPoint = (org.json.simple.JSONObject) GetInfoFromMongo.getInfoByCustomFilters(deviceFullRecords, inputFilter,new ArrayList<String>()).get(0);
			 TearDownExecutor.addInsertedTrackPointToTeardown(ReplyToJSON.extractPathFromJson(insertedTrackPoint, PATH_ID) );
			} else {
				log.error("nothing to insert, doc is empty");
			}
			 return insertedTrackPoint;
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
			return null;
		}
	}
	
	@Step("MongoDB: вставка документа в DeviceFullRecords")
	public static org.json.simple.JSONObject insertDocument(Document doc, boolean showLogs) {
		try {
			org.json.simple.JSONObject insertedTrackPoint =null;
			if(doc != null){
			com.mongodb.client.MongoCollection<Document> collection = getMongoCollection(deviceFullRecords);
			 collection.insertOne(doc);
			 ObjectId objectId = new ObjectId(doc.get(deviceId).toString());
			 Bson inputFilter =  Filters.and (eq (deviceId,objectId), eq (deviceTime, doc.get(deviceTime) ) );
			 insertedTrackPoint = GetInfoFromMongo.getInfoByCustomFilters(deviceFullRecords, inputFilter,new ArrayList<String>(),showLogs).get(0);
			 TearDownExecutor.addInsertedTrackPointToTeardown(ReplyToJSON.extractPathFromJson(insertedTrackPoint, PATH_ID) );
			} else {
				log.error("nothing to insert, doc is empty");
			}
			 return insertedTrackPoint;
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
			return null;
		}
	}
	
}
