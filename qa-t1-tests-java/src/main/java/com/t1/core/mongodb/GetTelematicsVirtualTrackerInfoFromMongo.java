package com.t1.core.mongodb;

import java.util.List;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetTelematicsVirtualTrackerInfoFromMongo extends AbstractClass{

	protected static String devices = "Devices";
	
	@Step("MongoDB:получение информации о ТМУ {0}")
	public static List getTrackerInfoById(String deviceId, String showOnly){
	return	GetInfoFromMongo.getInfoById(devices, deviceId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о ТМУ {0}")
	public static List getTrackerInfoById(String deviceId){
	return	GetInfoFromMongo.getInfoById(devices, deviceId,  "");
	}
	//it can return already assigned device!
	@Step("MongoDB:получение случайного ТМУ из БД")
	public static List getRandomTelematicsTrackerFromMongo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(devices, null, "");
	}
	
	@Step("MongoDB:получение случайного ТМУ из БД не связаного с ТС")
	public static List getRandomFreeTelematicsTrackerFromMongo(){
//		intersection with vahicles and employees? too much resources needed
		assertTrue(false,"not implemented!");
	return	null;//GetInfoFromMongo.getRandomDocumentFromCollection(devices, null, "");
	}
}
