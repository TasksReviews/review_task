package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetVehicleEmployeeInsurancePoliciesInfoFromMongo extends AbstractClass{
	
	protected static String employeeInsurancePolicies = "EmployeeInsurancePolicies";
	@Step("MongoDB:получение информации о Страховках {0}")
	public static List getInfoById(String id, String showOnly){
	return	GetInfoFromMongo.getInfoById(employeeInsurancePolicies, id,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Страховках {0}")
	public static List getInfoById(String id){
	return	GetInfoFromMongo.getInfoById(employeeInsurancePolicies, id,  "");
	}
	
	@Step("MongoDB:получение информации о Страховках {0}")
	public static List getInfoByVehicleId(String vehicleId) {
		ObjectId objectId = new ObjectId(vehicleId);
		Bson filter = Filters.and(eq("VehicleId", objectId));
		return GetInfoFromMongo.getInfoByCustomFilters(employeeInsurancePolicies, filter, new ArrayList<String>());
	}
}
