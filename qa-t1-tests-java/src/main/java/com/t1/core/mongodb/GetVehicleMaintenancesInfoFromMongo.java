package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetVehicleMaintenancesInfoFromMongo extends AbstractClass{

	protected static String vehicleMaintenances = "VehicleMaintenances";
	
	@Step("MongoDB:получение информации о ТС {0}")
	public static List getInfoById(String id, String showOnly){
	return	GetInfoFromMongo.getInfoById(vehicleMaintenances, id,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Техобслуживание {0}")
	public static List getInfoById(String id){
	return	GetInfoFromMongo.getInfoById(vehicleMaintenances, id,  "");
	}
	
	@Step("MongoDB:получение информации о Техобслуживании {0}")
	public static List getInfoByVehicleId(String vehicleId) {
		ObjectId objectId = new ObjectId(vehicleId);
		Bson filter = Filters.and(eq("VehicleId", objectId));
		return GetInfoFromMongo.getInfoByCustomFilters(vehicleMaintenances, filter, new ArrayList<String>());
	}
}
