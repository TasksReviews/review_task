package com.t1.core.mongodb;

import java.util.List;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetVehicleInfoFromMongo extends AbstractClass{

	protected static String vehicles = "Vehicles";
	@Step("MongoDB:получение информации о ТС {0}")
	public static List getVehicleInfoById(String vehicleId, String showOnly){
	return	GetInfoFromMongo.getInfoById(vehicles, vehicleId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о ТС {0}")
	public static List getVehicleInfoById(String vehicleId){
	return	GetInfoFromMongo.getInfoById(vehicles, vehicleId,  "");
	}
	
	@Step("MongoDB:получение информации о нескольких ТС {0}")
	public static List<org.json.simple.JSONObject> getInfoByVehicleIdList(List<String> VehicleIds, String customerId){
		try {
			
			List<org.json.simple.JSONObject> mongoVehicleInfo = GetInfoFromMongo.getInfoByListOfIds(vehicles, VehicleIds, customerId, null); 
			return mongoVehicleInfo;

		} catch (Exception e) {
			log.error(ERROR,e);
		}
		return null;
	 }
}
