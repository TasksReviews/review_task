package com.t1.core.mongodb;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.ReplyToJSON;
import com.t1.core.SharedContainer;
import com.t1.core.TearDownExecutor;
import com.t1.core.api.Geozone;
import com.t1.core.api.NotificationRule;
import com.t1.core.api.TelematicVirtualTracker;
import com.t1.core.api.Vehicle;

import ru.yandex.qatools.allure.annotations.Step;

public class InsertNotification extends MongoConnection {


//	@Given("^добавить оповещение в БД \"([^\"]*)\"$")
 public static Document prepareDoc(String notificationEventType){
		String resourceId = "ResourceId";
		String created = "Created";
		String message = "Message";
		String eventName ="EventName";
		String roadEventType ="RoadEventType";
		String geozoneId ="GeozoneId";
		String limitation ="Limitation";
		String recorded ="Recorded";
		String kmh =" км/ч";
		String recMgDec =",999996304512 mg.";
	 try {
	 Date notificationTime = null;
			notificationTime = new Date(System.currentTimeMillis() );
			Vehicle vehicleFromPreviouesStep = (Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles");
			TelematicVirtualTracker deviceFromPreviouesStep = (TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers"); 
			List<JSONObject> trackPointsFromPreviouesStep =  SharedContainer.getObjectsFromSharedContainer("all","insertedTrackPoints"); 
			JSONObject lastTrackPoint = trackPointsFromPreviouesStep.get(trackPointsFromPreviouesStep.size()-1);
			String latitude =lastTrackPoint.get("Latitude").toString();
			String longitude=lastTrackPoint.get("Longitude").toString();
			NotificationRule ruleFromPreviouesStep = (NotificationRule) SharedContainer.getLastObjectFromSharedContainer("notificationRules");
			Geozone geozoneFromPreviouesStep = null;
			if (SharedContainer.getContainers().containsKey("geozonesForNotificationRules")){
			geozoneFromPreviouesStep = (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozonesForNotificationRules");
			}
			JSONObject authorizedAccount = (JSONObject) SharedContainer.getObjectsFromSharedContainer("all","authorizedAccount_t1client").get(0);
	 Document doc = 
		   new Document("AccountId" , new ObjectId(authorizedAccount.get("id").toString()))
				.append("EventTime", notificationTime)
				.append(resourceId, vehicleFromPreviouesStep.getVehicleId())
				.append("DeviceId", deviceFromPreviouesStep.getTrackerId())
				.append("NotificationRuleId", ruleFromPreviouesStep.getRuleId())
				.append("Latitude", latitude)
				.append("Longitude", longitude)
				.append("IsImportant" , false)
				.append("IsRead" , false)
				.append(created , notificationTime)
				.append("EventId", "5996b689bf5efd0000000000")
	 			.append("Address", "г. Москва, пр-кт Тест-Андропова, д.599");
			switch (notificationEventType) {
			case "GeozoneEntrance": {
				doc.append(message, "JFW_Вход в геозону");
				doc.append(eventName, "Вход в геозону");
				doc.append(roadEventType, "GeozoneEntrance");
				doc.append(geozoneId, (geozoneFromPreviouesStep == null)? null: geozoneFromPreviouesStep.getGeozoneId());

			}
				break;

			case "GeozoneStanding": {
				doc.append(roadEventType, "GeozoneStanding");
				doc.append(message, "JFW_Нахождение в геозоне");
				doc.append(eventName, "Нахождение в геозоне");
				doc.append("EventTime", notificationTime);
				doc.append(geozoneId, (geozoneFromPreviouesStep == null)? null: geozoneFromPreviouesStep.getGeozoneId());
				
				
				String[] hourMin = ruleFromPreviouesStep.getLimitationValue2().split(":");
			    int hour = Integer.parseInt(hourMin[0]);
			    int mins = Integer.parseInt(hourMin[1]);
			    int durationInMins = (hour * 60)+mins;
			    
			    
				doc.append(limitation, durationInMins+" мин");
				
				doc.append(recorded, (durationInMins+2)+" мин");

			}
				break;
			case "GeozoneExit": {
				doc.append(roadEventType, "GeozoneExit");
				doc.append(message, "JFW_Выход из геозоны");
				doc.append(eventName, "Выход из геозоны");
				doc.append(geozoneId, (geozoneFromPreviouesStep == null)? null: geozoneFromPreviouesStep.getGeozoneId());

			}
				break;

			case "TelematicDeviceMechanismOff": {
				doc.append(message, "JFW_Выключение исполнительного механизма");
				doc.append(eventName, "Выключение исполнительного механизма");
				doc.append(roadEventType, "TelematicDeviceMechanismOff");

			}
				break;
				
			case "TelematicDeviceMechanismOn": {
				doc.append(message, "JFW_Включение исполнительного механизма");
				doc.append(eventName, "Включение исполнительного механизма");
				doc.append(roadEventType, "TelematicDeviceMechanismOn");
			}
				break;	
				
			case "PanicButton": {
				doc.append(message, "JFW_PanicButton");
				doc.append(eventName, "Нажатие тревожной кнопки");
				doc.append(roadEventType, "PanicButton");
			}
				break;	
				
			case "UserSpeedlimit": {
				doc.append(roadEventType, "UserSpeedlimit");
				doc.append(message, "JFW_Превышение установленного порога скорости");
				doc.append(eventName, "Превышение установленного порога скорости");
				doc.append(limitation, ruleFromPreviouesStep.getLimitationValue()+kmh);
				doc.append(recorded, (Integer.parseInt(ruleFromPreviouesStep.getLimitationValue())+100)+kmh);

			}
				break;
			case "StopEvent": {
				doc.append(roadEventType, "StopEvent");
				doc.append(message, "JFW_Стоянка");
				doc.append(eventName, "Стоянка");
			}
				break;
			case "IdleEvent": {
				doc.append(roadEventType, "IdleEvent");
				doc.append(message, "JFW_Холостой ход");
				doc.append(eventName, "Холостой ход");


			}
				break;
			case "TrafficRulesSpeedLimit": {
				doc.append(roadEventType, "TrafficRulesSpeedLimit");
				doc.append(message, "JFW_Превышение скорости по ПДД");
				doc.append(eventName, "Превышение скорости по ПДД");
				doc.append(limitation, ruleFromPreviouesStep.getLimitationValue() +kmh);
				doc.append(recorded, (Integer.parseInt(ruleFromPreviouesStep.getLimitationValue())+20)+kmh);


			}
				break;
				
				
			case "SharpTurnSpeedLimit": {
				doc.append(roadEventType, "SharpTurnSpeedLimit");
				doc.append(message, "JFW_Резкий поворот");
				doc.append(eventName, "JFW_SharpTurnSpeedLimit");
				doc.append(limitation, ruleFromPreviouesStep.getLimitationValue() +kmh);
				doc.append(recorded,   (Integer.parseInt(ruleFromPreviouesStep.getLimitationValue())+14)+kmh);
			}
				break;	
				
			case "MileageExceedance": {
				doc.append(roadEventType, "MileageExceedance");
				doc.append(message, "JFW_Превышение порога пробега");
				doc.append(eventName, "JFW_MileageExceedance");
				doc.append(limitation,  ruleFromPreviouesStep.getLimitationValue() +" км");
				doc.append(recorded,   (Integer.parseInt(ruleFromPreviouesStep.getLimitationValue())+1)+" км");

			}
				break;
				
			case "SharpBraking": {
				doc.append(roadEventType, "SharpBraking");
				doc.append(message, "JFW_Резкое торможение");
				doc.append(eventName, "JFW_SharpBraking");
				doc.append(limitation, ruleFromPreviouesStep.getLimitationValue()+" mg.");
				doc.append(recorded,   (Integer.parseInt(ruleFromPreviouesStep.getLimitationValue())+2)+recMgDec);

			}
				break;	
				
			case "SharpSpeedup": {
				doc.append(roadEventType, "SharpSpeedup");
				doc.append(message, "JFW_Резкое ускорение");
				doc.append(eventName, "JFW_SharpSpeedup");
				doc.append(limitation, ruleFromPreviouesStep.getLimitationValue()+" mg.");
				doc.append(recorded,   (Integer.parseInt(ruleFromPreviouesStep.getLimitationValue())+2)+recMgDec);

			}
				break;	
				
			case "SharpTurn": {
				doc.append(roadEventType, "SharpTurn");
				doc.append(message, "JFW_Резкий поворот");
				doc.append(eventName, "JFW_SharpTurn");
				doc.append(limitation, ruleFromPreviouesStep.getLimitationValue()+" mg.");
				doc.append(recorded,   (Integer.parseInt(ruleFromPreviouesStep.getLimitationValue())+2)+recMgDec);

			}
			break;
	
				
			default:
				throw new IllegalArgumentException("Invalid\\Unsupported notification type: " + notificationEventType);
			}

	 return doc;
	 } catch (Exception e) {
		 log.error(ERROR,e);
			return null;
		}
 }

	@Step("MongoDB: вставка документа в Notifications")
	public static org.json.simple.JSONObject insertDocument(String notificationEventType) {
		String resourceId = "ResourceId";
		String created = "Created";
		try {
			log.debug(">> insertion to Notifications");
			Document doc =  prepareDoc(notificationEventType);
			org.json.simple.JSONObject insertedNotification = null;
			if(doc != null){
				com.mongodb.client.MongoCollection<Document> collection = getMongoCollection("Notifications");
	
				 collection.insertOne(doc);
				 Bson inputFilter =  Filters.and (eq (resourceId,doc.get(resourceId)), eq (created, doc.get(created) ) );
				 insertedNotification = (org.json.simple.JSONObject) GetInfoFromMongo.getInfoByCustomFilters("Notifications", inputFilter,new ArrayList<String>()).get(0);
	
				 TearDownExecutor.addInsertedNotificationToTeardown(ReplyToJSON.extractPathFromJson(insertedNotification, "$._id.$oid") );
				 final JSONObject sharedInsertedNotification = insertedNotification;
				 SharedContainer.setContainer("insertedNotifications", sharedInsertedNotification);
	
			} else {
				log.error("nothing to insert, doc is empty");
			}
			 return insertedNotification;
		} catch (Exception e) {
			log.error(ERROR,e);
			mongoClient.close();
			return null;
		}
	}
}
