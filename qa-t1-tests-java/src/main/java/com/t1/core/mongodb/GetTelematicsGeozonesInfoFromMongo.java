package com.t1.core.mongodb;

import java.util.List;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetTelematicsGeozonesInfoFromMongo extends AbstractClass{

	protected static String geozones = "Geozones";
	
	@Step("MongoDB:получение информации о Геозоне {0}")
	public static List getTelematicsGeozonesInfoById(String geozoneId, String showOnly){
	return	GetInfoFromMongo.getInfoById(geozones, geozoneId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Геозоне {0}")
	public static List getTelematicsGeozonesInfoById(String geozoneId){
	return	GetInfoFromMongo.getInfoById(geozones, geozoneId,  "");
	}
	
	@Step("MongoDB:получение случайной Геозоны из БД")
	public static List getRandomTelematicsGeozoneFromMongo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(geozones,getProperty("testCustomerId"), "");
	}
}
