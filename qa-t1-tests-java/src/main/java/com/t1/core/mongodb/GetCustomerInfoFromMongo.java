package com.t1.core.mongodb;

import java.util.ArrayList;
import java.util.List;

import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetCustomerInfoFromMongo extends AbstractClass{

	private  static final String CUSTOMERS = "Customers";
	
	@Step("MongoDB:получение информации о Клиенте {0}")
	public static List getCustomerInfoById(String customerId, String showOnly){
	return	GetInfoFromMongo.getInfoById(CUSTOMERS, customerId,  showOnly);
	}
	
	@Step("MongoDB:получение информации о Клиенте {0}")
	public static List getCustomerInfoById(String customerId){
	return	GetInfoFromMongo.getInfoById(CUSTOMERS, customerId,  "");
	}
	
	@Step("MongoDB:получение информации о всех Клиентах {0}")
	public static List getAllCustomers(){
	return	GetInfoFromMongo.getInfoByCustomFilters(CUSTOMERS, null,new ArrayList<String>());//GetInfoFromMongo.getInfoById(CUSTOMERS, customerId,  "");
	}
	
	@Step("MongoDB:получение случайного Клиента из БД  {0}")
	public static List getRandomCustomerInfo(){
	return	GetInfoFromMongo.getRandomDocumentFromCollection(CUSTOMERS,"", "");
	}
	
}
