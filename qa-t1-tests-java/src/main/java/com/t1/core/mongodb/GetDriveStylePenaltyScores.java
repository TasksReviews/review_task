package com.t1.core.mongodb;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;

import ru.yandex.qatools.allure.annotations.Step;

public class GetDriveStylePenaltyScores extends AbstractClass{

	
	@Step("MongoDB:получение информации о настройках {0}")
	public static List getDriveStyleSettings(){
		
		Bson filter = Filters.eq ("CustomerId",new ObjectId(getProperty("testCustomerId")) ); 
		     
	return	GetInfoFromMongo.getInfoByCustomFilters("DriveStylePenaltyScores", filter,  new ArrayList<String>());
	}
	
	@Step("MongoDB:получение информации о настройках {0}")
	public static List getDriveStyleSettings(String customerId){
		
		Bson filter = Filters.eq ("CustomerId",new ObjectId(customerId) ); 
		     
	return	GetInfoFromMongo.getInfoByCustomFilters("DriveStylePenaltyScores", filter,  new ArrayList<String>());
	}
	
//	@Step("MongoDB:получение информации о ТМУ {0}")
//	public static List getTelematicsDevicesInfoById(String deviceId){
//	return	GetInfoFromMongo.getInfoById("DriveStylePenaltyScores", deviceId,  "");
//	}
//	//it can return already assigned device!
//	@Step("MongoDB:получение случайного ТМУ из БД")
//	public static List getRandomTelematicsDeviceFromMongo(){
//	return	GetInfoFromMongo.getRandomDocumentFromCollection("DriveStylePenaltyScores", null, "");
//	}
	

}
