package com.t1.core.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class T1LoginPage {
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("Selenium");
    WebDriver driver;

    /**
     * Логин
     */
    @FindBy(id = "username")
    private WebElement username;

    /**
     * Пароль
     */
    @FindBy(id = "password")
    private WebElement userPassword;
    /**
     * Кнопка Войти
     */
    @FindBy(xpath = "//div[contains(@class, 'form-group remember')]/button")
    private WebElement bSubmitLogin;

    @FindBy(xpath = "//span[contains(@class, 't-one-text-bold text-red-light uppercase')]")
    WebElement titleText;

    /**
     * Сообщение об ошибке
     */
    @FindBy(id = "error-message")
    private WebElement registerError;

    public T1LoginPage(WebDriver driver) {
        this.driver = driver;
        // This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }

    // Set user name in textbox
    public void setUserName(String strUserName) {
        username.sendKeys(strUserName);

    }

    // Set password in password textbox
    public void setPassword(String strPassword) {
        userPassword.sendKeys(strPassword);
    }

    // Click on login button
    public void clickLogin() {
        bSubmitLogin.click();
    }

    // Get the title of Login Page
    public String getLoginTitle() {
        return titleText.getText();
    }

    public void loginToT1(String strUserName, String strPasword) {
        // Fill user name
        this.setUserName(strUserName);
        // Fill password
        this.setPassword(strPasword);
        // Click Login button
        this.clickLogin();

    }

    // Create a new instance of the Firefox driver
    // Notice that the remainder of the code relies on the interface,
    // not the implementation.
    public void seleniumStart() throws InterruptedException {
        //FirefoxDriver
        //ChromeDriver
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();

        // And now use this to visit page
        driver.get("http://map-tone-test.qa.t1-group.ru");


        // Find the text input element by its name
        WebElement element = driver.findElement(By.id("username"));
        element.sendKeys("lIkffUWslY");
        element = driver.findElement(By.id("password"));
        element.sendKeys("qazxsw11");

        // Now submit the form. WebDriver will find the form for us from the element
        element = driver.findElement(By.xpath("//div[contains(@class, 'form-group remember')]/button"));
        element.submit();

        // Check the title of the page


        log.debug("Page title is: " + driver.getTitle());

        // is rendered dynamically with JavaScript.
        // Wait for the page to load, timeout after 10 seconds
        (new WebDriverWait(driver, 10)).until((ExpectedCondition<Boolean>) d -> {
            assert d != null;
            return d.getTitle().startsWith("TOne");
        });

        // Should see: "cheese! - Google Search"
        log.debug("Page title is: " + driver.getTitle());

        synchronized (driver) {
            while (driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS) != null) {
                driver.wait(10000);
            }

            //Close the browser
            driver.quit();
        }
    }
}
