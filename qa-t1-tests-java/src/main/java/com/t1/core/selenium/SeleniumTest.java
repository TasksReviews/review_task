package com.t1.core.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

//@Test(singleThreaded = true, invocationTimeOut = 1000000, groups = { "GUI" })
public class SeleniumTest {
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("Selenium");
    // Create a new instance of the Firefox driver
    // Notice that the remainder of the code relies on the interface, 
    // not the implementation.
    public void seleniumStart() throws InterruptedException {
        //FirefoxDriver
        //ChromeDriver
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        // And now use this to visit page
        driver.get("http://map-tone-test.qa.t1-group.ru");

        // Find the text input element by its name
        WebElement element = driver.findElement(By.id("username"));
        element.sendKeys("lIkffUWslY");
        element = driver.findElement(By.id("password"));
        element.sendKeys("qazxsw11");

        // Now submit the form. WebDriver will find the form for us from the element
        element = driver.findElement(By.xpath("//div[contains(@class, 'form-group remember')]/button"));
        element.submit();

        // Check the title of the page
        log.debug("Page title is: " + driver.getTitle());

        // Google's search is rendered dynamically with JavaScript.
        // Wait for the page to load, timeout after 10 seconds
        (new WebDriverWait(driver, 10)).until((ExpectedCondition<Boolean>) d -> d.getTitle().startsWith("TOne"));

        // Should see: "cheese! - Google Search"
        log.debug("Page title is: " + driver.getTitle());

        synchronized (driver) {
            while (driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS) != null) {
                driver.wait(10000);
            }
            //Close the browser
            driver.quit();
        }
    }
}
