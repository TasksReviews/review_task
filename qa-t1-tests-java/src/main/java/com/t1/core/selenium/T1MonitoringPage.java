package com.t1.core.selenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by IntelliJ IDEA.
 * User: ab83625
 * Date: 10.11.2010
 * To change this template use File | Settings | File Templates.
 */
public class T1MonitoringPage {

	WebDriver driver;
	@FindBy(xpath = "//a[contains(@href, 'monitoring')]/div[2]")
	public
	WebElement monitoringPageLeftBar;

	@FindBy(xpath = "//i[contains(@class, 'expand-collapse-switch t-one t-one-list')]")
	WebElement openMonitoringObjectListBar;

	@FindBy(xpath = "//div[contains(@class, 'icon-open-container expand-collapse-switch clockwise')]")
	WebElement closeMonitoringObjectListBar;

	@FindBy(xpath = "//input[contains(@placeholder, 'Поиск')]")
	public
	WebElement monitoringObjectListBarSearch;

	@FindBy(xpath = "//label[contains(@class, 't-one-checkbox-label-text') and contains(@for,'group-checkbox-58943c725532f31be80d1946')]/span[2]/span[2]")
	WebElement monitoringObjectFoundDemoBus;

	@FindBy(xpath = "//label[contains(@class, 't-one-checkbox-label-text') and contains(@for,'group-checkbox-58943c725532f31be80d1946')]/span[1]")
	WebElement monitoringObjectFoundDemoGroup;

	@FindBy(xpath = "//label[contains(@class, 't-one-checkbox-label third-state') and contains(@for,'group-checkbox-58943c725532f31be80d1946')]")
	WebElement monitoringObjectCheckBoxFoundDemoGroup;


	@FindBy(xpath = "//i[contains(@class, 'ng-class:zoomOutIcon t-one t-one-minus')]")
	WebElement monitoringZoomOut;

	@FindBy(xpath = "//div[contains(@class, 't-one-slider')]")
	WebElement monitoringObjectsSwithcer;

	public T1MonitoringPage(WebDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);
	}

	// Get the User name from Home Page
	public String getMonitoringPageLeftBarText() {
		return monitoringPageLeftBar.getText();
	}

	// Click on object list to open
	public void clickOnObjectListBarToOpen() {
//		WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(OpenMonitoringObjectListBar));
		openMonitoringObjectListBar.click();
	}

	// Click on object list to close
	public void clickOnObjectListBarToClose() {
		closeMonitoringObjectListBar.click();
	}

	// Click on demo group

	public void clickOnMonitoringObjectFoundDemoGroup() {
//		JavascriptExecutor jse = (JavascriptExecutor) driver;
//		WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(MonitoringObjectCheckBoxFoundDemoGroup));

	/*	 WebElement element = driver.findElement(By.xpath(
		 "//label[contains(@class, 't-one-checkbox-label third-state') and
		 contains(@for,'group-checkbox-58943c725532f31be80d1946')]"));
		 jse.executeScript("arguments[0].scrollIntoView(true);", element);
		 // driver.
		 MonitoringObjectCheckBoxFoundDemoGroup.sendKeys("");
		 */
		monitoringObjectCheckBoxFoundDemoGroup.click();
	}

	// Click on ZoomOut
	public void clickOnMonitoringZoomOut() {
		monitoringZoomOut.click();
	}

	// Click on ObjectsSwithcer
	public void clickOnMonitoringObjectsSwithcer() {
		monitoringObjectsSwithcer.click();
	}

	// Set user name in textbox
	public void setSearchingObject(String searchingObject) {
		monitoringObjectListBarSearch.sendKeys(searchingObject);
	}

	public String getObjectFoundDemoBusText() {
		return monitoringObjectFoundDemoBus.getText();
	}
}
