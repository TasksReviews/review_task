package com.t1.core.selenium;

import cucumber.api.java.en.Given;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.t1.core.AbstractClass;

/**
 * Created by IntelliJ IDEA.
 * User: ab83625
 * Date: 10.11.2010
 * To change this template use File | Settings | File Templates.
 */
public class TestLogin extends AbstractClass {
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("Selenium");
	WebDriver driver;
	T1LoginPage objLogin;
	T1MonitoringPage objMonitoringPage;
	String setBrowser = "firefox";

	@Given("fsdfsdfsdf")
	public void setUp() {
		// Инициализация контекста.
		if (setBrowser.toLowerCase().contains("firefox")) {
		System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
			this.driver = new FirefoxDriver();
		} else if (setBrowser.toLowerCase().contains("chrome")) {
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
			this.driver = new ChromeDriver();
		} else {
			log.error("unsupported browser selected.");
		}

		driver.get("http://map-tone-test.qa.t1-group.ru");
    }

	@Test(timeout = 1000)
	public void testMonitoringPageDisplayed() throws InterruptedException {
		long timeoutInSeconds = 10;
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);

		// Create Login Page object
		objLogin = new T1LoginPage(driver);
		// Verify login page title
		String loginPageTitle = objLogin.getLoginTitle();
		log.debug("loginPageTitle>>>>>>>>>>>>>>>>>>" + loginPageTitle);
		assertTrue("check loginPageTitle contains авторизация",loginPageTitle.toLowerCase().contains("авторизация"));
		// login to application
		objLogin.loginToT1("alex", "alex");
		// go the next page
		objMonitoringPage = new T1MonitoringPage(driver);
		// Verify home page

		wait.until(ExpectedConditions.elementToBeClickable(objMonitoringPage.monitoringPageLeftBar));

		log.debug("============monitoring? = "
				+ objMonitoringPage.getMonitoringPageLeftBarText().toLowerCase());
		assertTrue("check loginPageTitle contains мониторинг",objMonitoringPage.getMonitoringPageLeftBarText().toLowerCase().contains("мониторинг"));


		objMonitoringPage.clickOnObjectListBarToOpen();
		objMonitoringPage.clickOnMonitoringObjectsSwithcer();
		wait.until(ExpectedConditions.visibilityOf(objMonitoringPage.monitoringObjectListBarSearch));
		objMonitoringPage.setSearchingObject("demonstration bus");

		Thread.sleep(2000);
		assertTrue("страница мониторинг содержит DemoBus",objMonitoringPage.getObjectFoundDemoBusText().contains("1"));

		objMonitoringPage.clickOnMonitoringObjectFoundDemoGroup();
		objMonitoringPage.clickOnMonitoringZoomOut();
		objMonitoringPage.clickOnMonitoringZoomOut();
		Thread.sleep(200);
		objMonitoringPage.clickOnMonitoringZoomOut();
		objMonitoringPage.clickOnMonitoringZoomOut();
		objMonitoringPage.clickOnMonitoringZoomOut();
		objMonitoringPage.clickOnObjectListBarToClose();

		Thread.sleep(14000);
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@After
	public void closeBrowser() throws InterruptedException {
		if (driver != null) {
			Thread.sleep(2000);
			driver.close();
			Thread.sleep(2000);
			driver.quit();
		} else {
			log.error("driver empty!");
		}
	}
}