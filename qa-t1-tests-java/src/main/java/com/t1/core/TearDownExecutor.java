package com.t1.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.MongoConnection;

import ru.yandex.qatools.allure.annotations.Step;

public class TearDownExecutor extends AbstractClass {
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("TearDown");
	
	protected static String accountsStr = "accounts";
	protected static String employeesStr = "employees";
	protected static String virtualTrackersStr = "virtualTrackers";
	protected static String customersStr = "customers";
	protected static String vehiclesStr = "vehicles";
	protected static String departmentsStr = "departments";
	protected static String notificationRulesStr = "notificationRules";
	protected static String geozonesStr = "geozones";
	protected static String groupsStr = "groups";
	protected static String insertedTrackPointsStr = "insertedTrackPoints";
	protected static String insertedDeviceHistoryRecordsStr = "insertedDeviceHistoryRecords";
	protected static String insertedNotificationsStr = "insertedNotifications";
	protected static String insertedSensorThresholdsRecordsStr = "insertedSensorThresholdsRecords";
	protected static String reportTemplatesStr = "reportTemplates";
	protected static String employeeStr = "employee";
	protected static String departmentStr = "department";
	protected static String groupStr = "group";
	protected static String vehicleStr = "vehicle";
	protected static String reportTemplateStr = "reportTemplate";
	protected static String authorizedAccountT1clientStr = "authorizedAccount_t1client";
	protected static String authorizedAccountAdmPanelStr = "authorizedAccount_admPanel";
	protected static String adminPanelHostStr = "adminPanelHost";
			
	
	public void addAccountToTeardown(List<String> accountIds){
		addEntityToTeardown (accountIds,accountsStr);
	}
	
	public void addEmployeeToTeardown(List<String> employeeIds){
		
		addEntityToTeardown (employeeIds,employeesStr);
	}
	
	public static void addVirtualTrackerToTeardown(String trackerId) {
		addEntityToTeardown (trackerId,virtualTrackersStr);
	}
	
	public static void addAccountToTeardown(String accountId){
		addEntityToTeardown(accountId, accountsStr);
	}

	public static void addEmployeeToTeardown(String employeeId){
		addEntityToTeardown(employeeId, employeesStr);
	}
	
	public static void addCustomerToTeardown(String customerId){
		addEntityToTeardown(customerId, customersStr);
	}
	
	public static void addVehicleToTeardown(String vehicleId) {
		addEntityToTeardown(vehicleId, vehiclesStr);
	}
	
	public static void addDepartmentToTeardown(String departmentId) {
		addEntityToTeardown(departmentId, departmentsStr);
	}
	public static void addRuleToTeardown(String ruleId) {
		addEntityToTeardown(ruleId, notificationRulesStr);
	}
	public static void addGeozoneToTeardown(String geozoneId) {
		addEntityToTeardown(geozoneId, geozonesStr);
	}
	public static void addGroupToTeardown(String groupId) {
		addEntityToTeardown(groupId, groupsStr);
	}
	
	public static void addInsertedTrackPointToTeardown(String trackPoint) {
		addEntityToTeardown(trackPoint, insertedTrackPointsStr);
	}
	
	public static void addinsertedDevHistRecordsToTeardown(String histRec) {
		addEntityToTeardown(histRec, insertedDeviceHistoryRecordsStr);
	}
	
	public static void addinsertedVehEmplHistRecordsToTeardown(String histRec) {
		addEntityToTeardown(histRec, "insertedVehicleEmployeeHistoryRecords");
	}
	
	public static void addInsertedNotificationToTeardown(String notification) {
		addEntityToTeardown(notification, insertedNotificationsStr);
	}
	
	public static void addinsertedSensorThresholdsRecordsToTeardown(String sensorRec) {
		addEntityToTeardown(sensorRec, insertedSensorThresholdsRecordsStr);
	}
	
	public static void addReportTemplateToTeardown(String reportTemplate) {
		addEntityToTeardown(reportTemplate, reportTemplatesStr);
	}
	
	public static void addEntityToTeardown(List<String> entityIds, String entity){
		
		ArrayList<String> listOfentityIds = new ArrayList<>();
		if(teardownData.get(entity)!=null){
			listOfentityIds =	teardownData.get(entity);
		} 
		listOfentityIds.addAll(entityIds);
		teardownData.put(entity, listOfentityIds);
	}
	
	
	
	public static void addEntityToTeardown(String entityId, String entity){
		ArrayList<String> listOfentityIds = new ArrayList<>();
		if(teardownData.get(entity)!=null){
			listOfentityIds =	teardownData.get(entity);
		} 
		if(! listOfentityIds.contains(entityId)){
		listOfentityIds.add(entityId);
		teardownData.put(entity, listOfentityIds);
		}
	}
	
	public static void removeEntityFromTeardown(String entityId, String entity){
		ArrayList<String> listOfentityIds = new ArrayList<>();
		if(teardownData.get(entity)!=null){
			listOfentityIds =	teardownData.get(entity);
			listOfentityIds.remove(entityId);
		} 
		teardownData.put(entity, listOfentityIds);
	}
	
	

	 @Step("Удаление временно созданных сущностей")
	public static boolean teardownDeleteEntities(Map<String, ArrayList<String>> teardownData) {
		String id = "";
		try {
			if (!(teardownData == null || teardownData.isEmpty())) {
				log.debug("teardownData contains"+teardownData.keySet().toString() );

				if ((! SharedContainer.getContainers().containsKey(authorizedAccountT1clientStr))
						|| SharedContainer.getFromContainer(authorizedAccountT1clientStr).get(0) == ""){
					new AccountRegistrationAuth().loginAsT1Developer();
				}
				
//				NotificationRules
				//notification rule is related to specific account! must be used token of account that created rule we trying to delete.
				if (teardownData.keySet().contains(notificationRulesStr)  && teardownData.get(notificationRulesStr) != null) {
					Iterator<String> notificationRulesIds = teardownData.get(notificationRulesStr).iterator();
					while (notificationRulesIds.hasNext()) {
						id = notificationRulesIds.next();
						if(id != null && id != ""){
						log.info("notificationRules found for removing:" + id);
						sendRequestToApiToDeleteEntity("notificationRule",id);
						SharedContainer.removeFromContainer(notificationRulesStr,id);
						}
					}
					teardownData.remove(notificationRulesStr);
					if(SharedContainer.getContainers().containsKey(notificationRulesStr))
						SharedContainer.removeFromContainer(notificationRulesStr);
				} 
				
				if (teardownData.keySet().contains(employeesStr)  &&  teardownData.get(employeesStr) != null) {
					Iterator<String> employeeIds = teardownData.get(employeesStr).iterator();
					while (employeeIds.hasNext()) {
						id = employeeIds.next();
						if(id != null && id != ""){
						log.info("employee found for removing:" + id);
						sendRequestToApiToDeleteEntity(employeeStr,id);
						 SharedContainer.removeFromContainer(employeesStr,id);
						}
					}
					teardownData.remove(employeesStr);
					if(SharedContainer.getContainers().containsKey(employeesStr))
						SharedContainer.removeFromContainer(employeesStr);
				}
				
				if (teardownData.keySet().contains(departmentsStr)  && teardownData.get(departmentsStr) != null) {
					Iterator<String> departmentIds = teardownData.get(departmentsStr).iterator();
					while (departmentIds.hasNext()) {
						id = departmentIds.next();
						if(id != null && id != ""){
						log.info("department found for removing:" + id);
						sendRequestToApiToDeleteEntity(departmentStr,id);
						SharedContainer.removeFromContainer(departmentsStr,id);
						}
					}
					teardownData.remove(departmentsStr);
					if(SharedContainer.getContainers().containsKey(departmentsStr))
						SharedContainer.removeFromContainer(departmentsStr);

				} 
//				virtualTrackers
				if (teardownData.keySet().contains(virtualTrackersStr)  && teardownData.get(virtualTrackersStr) != null) {
					Iterator<String> virtualTrackersIds = teardownData.get(virtualTrackersStr).iterator();
					while (virtualTrackersIds.hasNext()) {
						id = virtualTrackersIds.next();
						if(id != null && id != ""){
						log.info("virtual tracker found for removing:" + id);
						sendRequestToApiToDeleteEntity("virtualTracker",id);
						SharedContainer.removeFromContainer(virtualTrackersStr,id);
						}
					}
					teardownData.remove(virtualTrackersStr);
					if(SharedContainer.getContainers().containsKey(virtualTrackersStr))
						SharedContainer.removeFromContainer(virtualTrackersStr);
				}
				
				if (teardownData.keySet().contains(geozonesStr)  && teardownData.get(geozonesStr) != null) {
					Iterator<String> geozonesIds = teardownData.get(geozonesStr).iterator();
					while (geozonesIds.hasNext()) {
						id = geozonesIds.next();
						if(id != null && id != ""){
						log.info("geozones found for removing:" + id);
						sendRequestToApiToDeleteEntity("geozone",id);
						SharedContainer.removeFromContainer(geozonesStr,id);
						}
					}
					teardownData.remove(geozonesStr);
					if(SharedContainer.getContainers().containsKey(geozonesStr))
						SharedContainer.removeFromContainer(geozonesStr);
				} 
				
				if (teardownData.keySet().contains(groupsStr)  && teardownData.get(groupsStr) != null) {
					Iterator<String> groupsIds = teardownData.get(groupsStr).iterator();
					while (groupsIds.hasNext()) {
						id = groupsIds.next();
						if(id != null && id != ""){
						log.info("groups found for removing:" + id);
						sendRequestToApiToDeleteEntity(groupStr,id);
						SharedContainer.removeFromContainer(groupsStr,id);
						}
					}
					teardownData.remove(groupsStr);
					if(SharedContainer.getContainers().containsKey(groupsStr))
						SharedContainer.removeFromContainer(groupsStr);
				} 
				
				String trackPoint ="";
				if (teardownData.keySet().contains(insertedTrackPointsStr)  && teardownData.get(insertedTrackPointsStr) != null) {
					Iterator<String> insertedTrackPoints = teardownData.get(insertedTrackPointsStr).iterator();
					while (insertedTrackPoints.hasNext()) {
						trackPoint = insertedTrackPoints.next();
						if (trackPoint != null && trackPoint != "") {
							MongoConnection.deleteDocument("DeviceFullRecords", trackPoint, false);
							if (container.containsKey(insertedTrackPointsStr)) {
								SharedContainer.removeFromContainer(insertedTrackPointsStr, trackPoint);
							} else if (container.containsKey("trackPointsFromFile")) {
								SharedContainer.removeFromContainer("trackPointsFromFile", trackPoint);
							}
						}
					}
					teardownData.remove(insertedTrackPointsStr);
					if(SharedContainer.getContainers().containsKey(insertedTrackPointsStr))
						SharedContainer.removeFromContainer(insertedTrackPointsStr);
				} 
				
				String devHistRec ="";
				if (teardownData.keySet().contains(insertedDeviceHistoryRecordsStr)  && teardownData.get(insertedDeviceHistoryRecordsStr) != null) {
					Iterator<String> insertedDevHistRec = teardownData.get(insertedDeviceHistoryRecordsStr).iterator();
					while (insertedDevHistRec.hasNext()) {
						devHistRec = insertedDevHistRec.next();
						if (devHistRec != null && devHistRec != "") {
							MongoConnection.deleteDocument("DeviceHistory", devHistRec, false);
							if (container.containsKey(insertedDeviceHistoryRecordsStr)) {
								SharedContainer.removeFromContainer(insertedDeviceHistoryRecordsStr, devHistRec);
							} 
						}
					}
					teardownData.remove(insertedDeviceHistoryRecordsStr);
					if(SharedContainer.getContainers().containsKey(insertedDeviceHistoryRecordsStr))
						SharedContainer.removeFromContainer(insertedDeviceHistoryRecordsStr);
				} 
		
				String sensorRec ="";
				if (teardownData.keySet().contains(insertedSensorThresholdsRecordsStr)  && teardownData.get(insertedSensorThresholdsRecordsStr) != null) {
					Iterator<String> insertedSensorThresholdsRec = teardownData.get(insertedSensorThresholdsRecordsStr).iterator();
					while (insertedSensorThresholdsRec.hasNext()) {
						sensorRec = insertedSensorThresholdsRec.next();
						if (sensorRec != null && sensorRec != "") {
							MongoConnection.deleteDocument("SensorThresholds", sensorRec, false);
							if (container.containsKey(insertedSensorThresholdsRecordsStr)) {
								SharedContainer.removeFromContainer(insertedSensorThresholdsRecordsStr, sensorRec);
							} 
						}
					}
					teardownData.remove(insertedSensorThresholdsRecordsStr);
					if(SharedContainer.getContainers().containsKey(insertedSensorThresholdsRecordsStr))
						SharedContainer.removeFromContainer(insertedSensorThresholdsRecordsStr);
				} 

				String notification ="";
				if (teardownData.keySet().contains(insertedNotificationsStr)  && teardownData.get(insertedNotificationsStr) != null) {
					Iterator<String> insertedTrackPoints = teardownData.get(insertedNotificationsStr).iterator();
					while (insertedTrackPoints.hasNext()) {
						notification = insertedTrackPoints.next();
						if(notification != null && notification != ""){
						log.info("insertedNotifications found for removing:" + notification);
						MongoConnection.deleteDocument("Notifications",notification,false);

						SharedContainer.removeFromContainer(insertedNotificationsStr,notification);
						}
					}
					teardownData.remove(insertedNotificationsStr);
					if(SharedContainer.getContainers().containsKey(insertedNotificationsStr))
						SharedContainer.removeFromContainer(insertedNotificationsStr);
				} 
				
				if (teardownData.keySet().contains(vehiclesStr)  && teardownData.get(vehiclesStr) != null) {
					Iterator<String> vehiclesIds = teardownData.get(vehiclesStr).iterator();
					while (vehiclesIds.hasNext()) {
						id = vehiclesIds.next();
						if(id != null && id != ""){
						log.info("vehicle found for removing:" + id);
						sendRequestToApiToDeleteEntity(vehicleStr,id);
						SharedContainer.removeFromContainer(vehiclesStr,id);
						}
					}
					teardownData.remove(vehiclesStr);
					if(SharedContainer.getContainers().containsKey(vehiclesStr))
						SharedContainer.removeFromContainer(vehiclesStr);
				}
				
				if (teardownData.keySet().contains(reportTemplatesStr)  && teardownData.get(reportTemplatesStr) != null) {
					Iterator<String> reportIds = teardownData.get(reportTemplatesStr).iterator();
					while (reportIds.hasNext()) {
						id = reportIds.next();
						if(id != null && id != ""){
						log.info("report templates found for removing:" + id);
						sendRequestToApiToDeleteEntity(reportTemplateStr,id);
						SharedContainer.removeFromContainer(reportTemplatesStr,id);
						}
					}
					teardownData.remove(reportTemplatesStr);
					if(SharedContainer.getContainers().containsKey(reportTemplatesStr))
						SharedContainer.removeFromContainer(reportTemplatesStr);
				}
				
				if (teardownData.keySet().contains(accountsStr)  && teardownData.get(accountsStr) != null) {
					Iterator<String> accountIds = teardownData.get(accountsStr).iterator();
					while (accountIds.hasNext()) {
						id = accountIds.next();
						if(id != null && id != ""){
						log.info("account found for removing:" + id);
						sendRequestToApiToDeleteEntity("account",id);
						}
					}
					teardownData.remove(accountsStr);
					if(SharedContainer.getContainers().containsKey(accountsStr))
						SharedContainer.removeFromContainer(accountsStr);
				} 
				
				if (teardownData.keySet().contains(customersStr)  &&  teardownData.get(customersStr) != null) {
					Iterator<String> customersIds = teardownData.get(customersStr).iterator();
					while (customersIds.hasNext()) {
						id = customersIds.next();
						if(id != null && id != ""){
						log.info("customer found for removing:" + id);
						sendRequestToApiToDeleteEntity("customer",id);
						 SharedContainer.removeFromContainer(customersStr,id);
						}
					}
					teardownData.remove(customersStr);
					if(SharedContainer.getContainers().containsKey(customersStr))
						SharedContainer.removeFromContainer(customersStr);
				}
				
				SharedContainer.removeAllContainers();
			} else {
				log.debug("teardown is empty");
				SharedContainer.removeAllContainers();
			}
			return true;
		} catch (Exception e) {
			log.error("failed to delete existing entity: " + e);
			SharedContainer.clearContainer();
			return false;
		}

	}
//does it works w/o token?
	 public static void accountDelete(String accountId) {
			String url  = getProperty("host")+"/account/delete";
			try{
				 JSONObject registrationReply = SendRequestToAPI.sendPostRequest( url, accountId, "", false,true);
				log.info("registrationReply=="+registrationReply.toString());
		}catch(Exception e){
			log.error("Test failed: "+ e);
			assertTrue(false, e.toString());
			
		}
	 }

	 @Step("Удаление временно созданного: {0}, id = {1}")
	 public static void sendRequestToApiToDeleteEntity(String entityType, String entityId) {
		 Boolean multipart= false;
		 String url = getProperty("host");
		 String postParameters="";
		 String	token=null;
		org.json.simple.JSONObject requestReply=null;
		try {
			if ((Arrays.asList(reportTemplateStr,vehicleStr,employeeStr,groupStr,departmentStr,"virtualTracker").contains(entityType)) 
				&& (!SharedContainer.getContainers().containsKey(authorizedAccountAdmPanelStr))){
					token = new AccountRegistrationAuth().loginAsT1AdminInAdminPanel();
			}
			if (entityType.equals(vehicleStr)) {
				url = getProperty(adminPanelHostStr) + "/Api/Admin/Vehicles/" + entityId;
				token = getCurAuthAccToken(authorizedAccountAdmPanelStr);
				multipart = true;
			} else if (entityType.equals("account")) {
				if ((!SharedContainer.getContainers().containsKey(authorizedAccountT1clientStr)) 
						|| SharedContainer.getFromContainer(authorizedAccountT1clientStr).get(0) == "" 
						|| !((JSONObject)SharedContainer.getFromContainer(authorizedAccountT1clientStr).get(0)).get("role").equals("Developer")) {
					token = new AccountRegistrationAuth().loginAsT1Developer();
				} else {
						token = getCurAuthAccToken(authorizedAccountT1clientStr,"Developer");
				}
				url += "/account/delete";
				postParameters = "{ \"accountId\" : \"" + entityId + "\" }";
				multipart = false;
			}
			else if (entityType.equals(employeeStr)) {
				url = getProperty(adminPanelHostStr) +"/Api/Admin/Employees/" + entityId;
				token = getCurAuthAccToken(authorizedAccountAdmPanelStr);
				multipart = true;
			}
			else if (entityType.equals("customer")) {
				url += "/Api/Customer/Delete/" + entityId;
				token = getCurAuthAccTokeninT1Client();
				multipart = false;
			}
			else if (entityType.equals(departmentStr)) {
				url = getProperty(adminPanelHostStr) + "/Api/Admin/Departments/" + entityId;
				token = getCurAuthAccTokeninT1Client();
				multipart = true;
			} else if (entityType.equals("virtualTracker")) {
				url = getProperty(adminPanelHostStr) + "/Api/Admin/Telematics/Device/" + entityId; //by some reason we have this post method to delete device...
				if ((! SharedContainer.getContainers().containsKey(authorizedAccountAdmPanelStr))
						|| SharedContainer.getFromContainer(authorizedAccountAdmPanelStr).get(0) == "") {
				
					//new AccountRegistrationAuth().loginAsT1AdminInAdminPanel();
				}
				token = getCurAuthAccToken(authorizedAccountAdmPanelStr);
				multipart = true;
			}
			else if (entityType.equals("notificationRule")) {
				url += "/NotificationRules/Delete/" + entityId;
				token = getCurAuthAccTokeninT1Client();
				multipart = false;
			}
			else if (entityType.equals("geozone")) {
				url += "/Geozone/Delete/" + entityId;
					token = getCurAuthAccTokeninT1Client();
				multipart = false;
			}
			else if (entityType.equals(groupStr)) {
				url = getProperty(adminPanelHostStr) + "/Api/Admin/Groups/" + entityId;
				token = getCurAuthAccToken(authorizedAccountAdmPanelStr);
				multipart = false;
			}
			else if (entityType.equals(reportTemplateStr)) {
				url += "/ReportTemplates/" + entityId;
				token = getCurAuthAccTokeninT1Client();
				multipart = false;
			}
			assertTrue("token received",token != null, "для удаления объекта необходимо пройти авторизацию");
			if (Arrays.asList(reportTemplateStr,vehicleStr,employeeStr,groupStr,departmentStr).contains(entityType)){
				requestReply = SendRequestToAPI.sendDeleteRequest(url, token,  false);
			}else{
			requestReply = SendRequestToAPI.sendPostRequest(url, postParameters, token, multipart, false);
			}
			 String replyCode =null;
			 if(requestReply.containsKey("code")) replyCode = requestReply.get("code").toString();
			 if(requestReply.containsKey("messageBody")) replyCode = requestReply.get("messageBody").toString();
			 
			if(replyCode== null || !replyCode.equals("Ok") ){
				teardownData.remove(entityType,  entityId);
				log.warn("wasn't able to delete "+entityType+" with id "+entityId+", remove it from containers anyway!");
				SharedContainer.removeFromContainer(entityType,  entityId);
				AbstractClass.setToAllureChechkedFields();
			}
				

		} catch (Exception e) {
			log.error("failed to delete existing entity: " + e);
		}
	 }


}
