package com.t1.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;

import org.apache.log4j.LogManager;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.t1.core.api.AccountRegistrationAuth;
import com.t1.core.mongodb.GetAccountInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Attachment;

public abstract class AbstractClass extends Asserts{
	protected static Map<String, ArrayList> container = new HashMap();
	private static String token;

	protected Map permissionsMap = new HashMap<Integer, String>();
	protected Map rolesOnRussianMap = new HashMap<String, String>();
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("Test");
	java.util.logging.Logger mongoLogger = java.util.logging.Logger.getLogger( "org.mongodb.driver" );

	protected static Map<String, ArrayList<String>> teardownData = new HashMap<>();
	protected static TimeZone utc3TimeZone = TimeZone.getTimeZone("UTC+3:00");
	protected static String standartTimeFormat = "yyyy-MM-dd HH:mm:ss";
	protected static String standartTimeFormatNoSpace = "yyyy-MM-dd'T'HH:mm:ss";
	protected static final String ERROR = "Error";
	protected static String authorizedAccountT1client = "authorizedAccount_t1client";
	protected static String dateTimeFrom = "dateTimeFrom";
	protected static String dateTimeTo = "dateTimeTo";
	protected static String week = "неделю";
	protected static String day = "сутки";
	protected static String yesterday = "вчера";
	protected static String tokenStr = "token";
	
	public AbstractClass() {
	    LogManager.getLogger("com.jayway.jsonpath.internal.path").setLevel(org.apache.log4j.Level.ERROR);  //otherwise will get "Evaluating path: {}"
		//mongoLogger.setLevel(java.util.logging.Level.WARNING);
	    LogManager.getLogger("org.mongodb.driver").setLevel(org.apache.log4j.Level.ERROR); //otherwise will get connection state,sended command etc.
//	    LogManager.getLogger("org.mongodb.driver.connection").setLevel(org.apache.log4j.Level.ERROR);
//	    LogManager.getLogger("org.mongodb.driver.management").setLevel(org.apache.log4j.Level.ERROR);
//	    LogManager.getLogger("org.mongodb.driver.cluster").setLevel(org.apache.log4j.Level.ERROR);
//	    LogManager.getLogger("org.mongodb.driver.protocol.insert").setLevel(org.apache.log4j.Level.ERROR);
//	    LogManager.getLogger("org.mongodb.driver.protocol.query").setLevel(org.apache.log4j.Level.ERROR);
//	    LogManager.getLogger("org.mongodb.driver.protocol.update").setLevel(org.apache.log4j.Level.ERROR);
		
//		permissionsMap.put(0,PermissionsEnum.System); permissionsMap.put(1,PermissionsEnum.ViewDevice); permissionsMap.put(2,PermissionsEnum.ShareDevice); 
//		permissionsMap.put(3,PermissionsEnum.ViewDeviceState); permissionsMap.put(4,PermissionsEnum.ViewDeviceHistory); permissionsMap.put(5,PermissionsEnum.ViewVirtualDevice); 
//		permissionsMap.put(6,PermissionsEnum.ViewVirtualDeviceState); permissionsMap.put(7,PermissionsEnum.ViewVirtualDeviceHistory); permissionsMap.put(8,PermissionsEnum.EditVirtualDevice); 
//		permissionsMap.put(9,PermissionsEnum.EditVirtualDeviceHistory); permissionsMap.put(10,PermissionsEnum.UserManagement); permissionsMap.put(11,PermissionsEnum.RoleManagement);
//		permissionsMap.put(12,PermissionsEnum.Admin); permissionsMap.put(13,PermissionsEnum.AccessToAllResources); permissionsMap.put(14,PermissionsEnum.Developer); 
	
		rolesOnRussianMap.put("Администратор","Admin"); rolesOnRussianMap.put("Демо","Demo"); rolesOnRussianMap.put("Курьер","Courier"); rolesOnRussianMap.put("Водитель","Driver");
		rolesOnRussianMap.put("Оператор","Operator"); rolesOnRussianMap.put("Диспетчер","Dispatcher"); rolesOnRussianMap.put("Аналитик","Analyst"); 
		rolesOnRussianMap.put("Транспортный специалист","TransportExpert"); rolesOnRussianMap.put("Технический специалист","TechnicalExpert"); 
		rolesOnRussianMap.put("Финансовый специалист","FinancialExpert"); rolesOnRussianMap.put("Руководитель/владелец","Chief"); 
		rolesOnRussianMap.put("Аналитик страховой компании","InsuranceAnalyst");  
	}

	
	protected static String getProperty(String property) {
		Properties prop = new Properties();
		InputStream input = null;
		String result =null;
		// InputStream input = new ByteArrayInputStream(null);

		try {

			input = AbstractClass.class.getClassLoader().getResourceAsStream("config.properties");

			// load a properties file
			prop.load(input);
			// get the property value and print it out
	//		System.out.println(prop.getProperty("database"));
	//		System.out.println(prop.getProperty("dbuser"));
	//		System.out.println(prop.getProperty("dbpassword"));
			result = prop.getProperty(property);
			if (result ==null || result.equals("")){ // TODO: sometimes we use send here pattern value instead of pattern name! this should be corrected.
				result = property;
			}
		} catch (IOException ex) {
			log.error(ERROR,ex);
		} finally {

			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					log.error(ERROR,e);
				}
			}
		}
		return result;
	}
	
	public String getAuthorizedUserCustomer(String accountId) {
		String customerId = "";
		try { // alexautotest_qa_transportExpert, autotest_qa_admin autotest_qa_chief autotest_qa_demo
			String[] autotestAccs = { "59aea8455532f3209c58a1db", "59312de95532f50eb0250cff", "59aea8875532f3209c58a1df", "59312de95532f50eb0250d03",
					// autotest_qa_developer, autotest_qa_dispatcher, autotest_qa_driver autotest_qa_operator
					"5982fbb95532f30c74b9e152", "59aea81a5532f3209c58a1d7", "59aea7925532f3209c58a1cf", "59aea7c85532f3209c58a1d2" };
			for (int j = 0; j < autotestAccs.length; j++) {
				if (autotestAccs[j].matches(accountId)) { // matches uses regex
					customerId = getProperty("testCustomerId");
					break;
				}
			}
			if (customerId == null || customerId.equals("")) {
				JSONObject accInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(accountId, "CustomerId").get(0);
				customerId = ReplyToJSON.extractPathFromJson(accInfo, "$.CustomerId.$oid");
			}
		} catch (Exception e) {
			log.error(ERROR, e);
			return null;
		}
		return customerId;
	}

	protected Iterator<Object[]> getInputDataDefault() {
		List<Object[]> testCases = new ArrayList<>();
		String[] data = null;
		String cvsSplitBy = ";";
		String csvFile = "src/main/resources/input.csv";
		// this loop is pseudo code
		String line="";
		try ( BufferedReader br = new BufferedReader(new FileReader(csvFile)) ){
			while ((line = br.readLine()) != null) {
				// use comma as separator
				data = line.split(cvsSplitBy);
				testCases.add(data);
			}
		} catch (IOException e) {
			log.error(ERROR,e);
		}
		return testCases.iterator();
	}

	public Object[][] getInputDataStringArrayTestData() {
		Object[][] testCases = null;
		ArrayList<String[]> list = new ArrayList<>();
		String[] data = null;
		String cvsSplitBy = ";";
		String csvFile = getProperty("testInputData");
		// this loop is pseudo code
		int countlines = 0;
		String line = "";
		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
			while ((line = br.readLine()) != null) {
				// use comma as separator
				countlines++;
				data = line.split(cvsSplitBy);
				list.add(data);
			}
			testCases = new Object[countlines][0];
			for (int i = 0; i < list.size(); i++) {
				testCases[i] = list.get(i);
			}
		} catch (IOException e) {
			log.error(ERROR, e);
		}
		return testCases;
	}

	/**
	 * @deprecated (should be used getCurAuthAccTokeninT1Client(); or getCurAuthAccToken(authorizedAccount_admPanel) instead)
	 */
	@Deprecated
	public static String getToken(String tokenType) {
		String adminToken = "adminToken";
		if ((SharedContainer.getContainers().containsKey(tokenType))
				&& SharedContainer.getFromContainer(tokenType).get(0) != "") {
			token = SharedContainer.getFromContainer(tokenType).get(0).toString();
			log.debug("using " + tokenType + " from container");
		} else {//method needs user token which was't found, probably in test we were authorized in as admin
			if (tokenType.equals("userToken")) {//is it ok to use admin role automatically instead of demo, when in prev step auhorized as admin? endToEnd scenario issues?
				log.warn("user token requested wasn't found, try to use adminToken and continue test");
				if ((SharedContainer.getContainers().containsKey(adminToken))
						&& SharedContainer.getFromContainer(adminToken).get(0) != "") {
					log.info("adminToken exists, continue test");
					token = SharedContainer.getFromContainer(adminToken).get(0).toString();
				}
			} else if (tokenType.equals("devToken")) {
				//it is fine to athorize as Dev automatically without cucumber step since dev access must be used only for specific methods that are not used in GUI
				log.debug("developer token wasn't found, login as developer to get token");
				token  = new AccountRegistrationAuth().loginAsT1Developer();
				SharedContainer.setContainer("devToken", Arrays.asList(token));
			}
			
			else {
				assertTrue(false, "token не найден");
			}
		}
		return token;
	}
	/**
	 * @deprecated (should be used getCurAuthAccTokeninT1Client(); instead)
	 */
	@Deprecated
	public static String getAnyToken() {
		Iterator<String> iter = SharedContainer.getContainers().keySet().iterator();
		List<String> keysWithTokens = new ArrayList<>();
		while (iter.hasNext()){
			String key = iter.next();
			if(key.contains("Token")||key.contains(tokenStr)){
				keysWithTokens.add(key);
			}
		}
		if (keysWithTokens.isEmpty()){
			assertTrue(false, "не найдено ключей авторизации");
		}
		token = SharedContainer.getFromContainer(keysWithTokens.get(0)).get(0).toString();
		
			return token;
		}	
	
	public static String getCurAuthAccToken(String authPlace){
		token = getCurAuthAcc(authPlace).get(tokenStr).toString();
		return token;
	}

	
	public static String getCurAuthAccToken(String authPlace, String role){
		assertTrue("check auth place", authPlace==authorizedAccountT1client || authPlace=="authorizedAccount_admPanel", "запрошено недопустимое место авторизации");

		if ((SharedContainer.getContainers().containsKey(authPlace))
				&& SharedContainer.getFromContainer(authPlace).get(0) != "") {
			
			JSONObject authorizedAccount = (JSONObject)SharedContainer.getFromContainer(authPlace).get(0);
			assertEquals("check role", authorizedAccount.get("role").toString(), role
								,"авторизован аккаунт с некорректной ролью: "+authorizedAccount.get("role")+", ожидался аккаунт с ролью"+role);
			token = authorizedAccount.get(tokenStr).toString();}
		else {
			assertTrue(false,"не найдены авторизованные аккаунты для "+authPlace +", с ролью "+role);
			throw new IllegalArgumentException("not found any autorized accounts during this test");
		}
		return token;
	}
	
	public static String getCurAuthAccTokeninT1Client(){
		return getCurAuthAccToken(authorizedAccountT1client);
	}
	
	public static JSONObject getCurAuthAcc(String authPlace){
		JSONObject authorizedAccount;
		assertTrue("check auth place", authPlace==authorizedAccountT1client || authPlace=="authorizedAccount_admPanel"
				, "запрошено недопустимое место авторизации, допустимые:authorizedAccount_t1client ||  authorizedAccount_admPanel");

		if ((SharedContainer.getContainers().containsKey(authPlace))
				&& SharedContainer.getFromContainer(authPlace).get(0) != "") {
			authorizedAccount = (JSONObject)SharedContainer.getFromContainer(authPlace).get(0);
		} else {
			assertTrue(false,"не найдены авторизованные аккаунты для "+authPlace);
			throw new IllegalArgumentException("not found any autorized accounts during this test");
		}
		return authorizedAccount;
	}
	

	
	public static String pickRandomFromList(String[] inputList){
		Random random = new Random();
		int index = random.nextInt(inputList.length);
		return inputList[index];
	}
	
	public static String pickRandomFromList(List<String> inputList){
		Random random = new Random();
		int index = random.nextInt(inputList.size());
		return inputList.get(index);
	}

	public static org.json.simple.JSONObject toJson (Object input){
		JSONParser parser = new JSONParser();
		try {
			if(input instanceof Boolean || input instanceof String ){
				String	jStr= "{\"messageBody\":\""+input+"\"}";
				return  (JSONObject) parser.parse(jStr);
			}
			else {
				return  (JSONObject) parser.parse(input.toString());
			}
		} catch (ParseException e) {
			log.error(ERROR,e);
		}
		return null;
	}
	
	public static org.json.simple.JSONObject toJson (String input){
		JSONParser parser = new JSONParser();
		try {
			return  (JSONObject) parser.parse(input);
		} catch (ParseException e) {
			log.error(ERROR,e);
		}
		return null;
	}
	
	
	public static org.json.simple.JSONObject toJson (StringBuilder input){
		JSONParser parser = new JSONParser();
		try {
			return  (JSONObject) parser.parse(input.toString());
		} catch (ParseException e) {
			log.error(ERROR,e);
		}
		return null;
	}
	
	public static List<org.json.simple.JSONObject> toJson (List<Document> input){
		List<org.json.simple.JSONObject> jsonList = new ArrayList<>();
		JSONParser parser = new JSONParser();
		try {
			for(int i=0; i<input.size(); i++){
				jsonList.add((JSONObject) parser.parse(input.get(i).toString()));
			}
		} catch (ParseException e) {
			log.error(ERROR,e);
		}
		return jsonList;
	}
	
	public static org.json.simple.JSONObject toJson (Document input){
		JSONParser parser = new JSONParser();
		try {
			return (JSONObject) parser.parse(input.toString());
			 
		} catch (ParseException e) {
			log.error(ERROR,e);
		}
		return null;
	}
	
	/**
	 * 
	 * @param attachName
	 * @param message
	 * @return
	 */
	 @Attachment(value = "{0}", type = "text/plain")
	 public static String textToAllureRep(String attachName, String message) {
	        return message;
	    }

//TODO: here should be screenshot or some real image!
	 @Attachment(value = "{0}", type = "image/png")
	 public static byte[] saveImageAttach(String attachName) {
	     try {
	         URL defaultImage = AbstractClass.class.getResource("/allure.png");
	         File imageFile = new File(defaultImage.toURI());
	         return Files.readAllBytes(Paths.get(imageFile.getPath()));
	     } catch (Exception e) {
	    	 log.error(ERROR,e);
	     }
	     return new byte[0];
	 }
	 
	//TODO: here should be some real page, this is only example!
	 @Attachment(value = "{0}", type = "text/html")
	 public static byte[] saveHtmlAttach(String attachName) {
	     try {
	         URL defaultImage = AbstractClass.class.getResource("/test.html");
	         File imageFile = new File(defaultImage.toURI());
	         return Files.readAllBytes(Paths.get(imageFile.getPath()));
	     } catch (Exception e) {
	    	 log.error(ERROR,e);
	     }
	     return new byte[0];
	 }

	 
	 public static String jsonParamsToString(JSONObject in){
		 return in.toJSONString(); 
	 }
	 public static String jsonParamsToString(JSONObject in, boolean escapingValue){
		 return in.toJSONString(escapingValue); 
	 }
	 
	 public static int rndFldLen(){
		 int min = Integer.parseInt(getProperty("minFieldSize")) ;
		 int max = Integer.parseInt(getProperty("maxFieldSize"));
		 return new java.util.Random().nextInt(max + 1 - min) + min;
	 }
	 
	 
	public static void setToAllureChechkedFields() {
		String checkedFieldsStr = "checkedFields";
		if (SharedContainer.getContainers().containsKey(checkedFieldsStr)) {
			ArrayList checkedFields = SharedContainer.getFromContainer(checkedFieldsStr);
			String strCheckedFields = checkedFields.toString();
			strCheckedFields = strCheckedFields.replace("}, {", "}, \r\n{");
			log.info("fields checked : " + strCheckedFields);
			textToAllureRep("Были проверены поля: ", strCheckedFields);
			SharedContainer.removeFromContainer(checkedFieldsStr);
		}
	}
	
	public static String dateToUnixTime(String time) {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		long unixtime = 0;
		dfm.setTimeZone(utc3TimeZone);// Specify your timezone
		try {
			if (time.contains("-")) {
				unixtime = dfm.parse(time).getTime();
			} else {
				unixtime = Long.parseLong(time);
			}
		} catch (java.text.ParseException e) {
			log.error(ERROR, e);
		}
		return Objects.toString(unixtime, null);
	}
	public static String dateTimeToUnixTime(String time){
		String dateTimeFormat ="";
		if(time.contains("T") && time.contains("+")){
			dateTimeFormat ="yyyy-MM-dd'T'HH:mm:ssX";
		} else {
			dateTimeFormat =standartTimeFormat;
		}
		return dateTimeToUnixTime( time,dateTimeFormat);
	}
	
	public static String dateTimeToUnixTime(String time, String dateTimeFormat) {
		DateFormat dfm = new SimpleDateFormat(dateTimeFormat );
		long unixtime = 0;
		dfm.setTimeZone(utc3TimeZone);// Specify your timezone
		try {
			unixtime = dfm.parse(time).getTime();
		} catch (java.text.ParseException e) {
			log.error(ERROR, e);
		}
		return Objects.toString(unixtime, null);
	}
	
	public static java.util.Date dateTimeToTypeDate(String time){
		DateFormat dfm = new SimpleDateFormat(standartTimeFormat);
		java.util.Date unixDateTime = null;
		dfm.setTimeZone(utc3TimeZone);// Specify your timezone
		try {
			unixDateTime = dfm.parse(time);
		} catch (java.text.ParseException e) {
			log.error(ERROR, e);
		}
		return unixDateTime;
	}
	
	public static boolean isADateTime(String inputVal) {
		boolean res= false;
		if (inputVal.contains("-") && inputVal.contains(":")) {
			DateFormat dfm = new SimpleDateFormat(standartTimeFormat);
			dfm.setTimeZone(utc3TimeZone);// Specify your timezone
			try {
					dfm.parse(inputVal); 
					res = true;
			} catch (java.text.ParseException e) {
				res = false;
			}
		} return res;
	}
	
	public static String getCurrentLocalDate(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.now();
		return dtf.format(localDate);
	}
	public static String getCurrentLocalDateTime(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(standartTimeFormat);
		LocalDateTime localDateTime = LocalDateTime.now();
		return dtf.format(localDateTime);
	}
	public static String getCurrentLocalDateTimePlus5Sec(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(standartTimeFormat);
		LocalDateTime localDateTime = LocalDateTime.now().plusSeconds(5);
		return dtf.format(localDateTime);
	}
	
	public static String getCurrentLocalDateTimePlus1Min(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(standartTimeFormat);
		LocalDateTime localDateTime = LocalDateTime.now().plusSeconds(60);
		return dtf.format(localDateTime);
	}
	
	public static String getCurrentUTC3DateTime(){
		Calendar calendar = Calendar.getInstance(utc3TimeZone);
		SimpleDateFormat simpleDateFormat = 
		       new SimpleDateFormat(standartTimeFormat, Locale.US);
		simpleDateFormat.setTimeZone(utc3TimeZone);
		log.debug("UTC:     " + simpleDateFormat.format(calendar.getTime()));
		return simpleDateFormat.format(calendar.getTime());
	}
	

	
	public List<String> addToArrParams(String input, String prefix) {
		List<String> paramsArray = new ArrayList<>();
		if (input.length() > 0) {
			if(input.contains("[")&& input.contains("]")){
				input = input.substring(1, input.length()-1);
			}
			String[] splittedInput = input.split(",");
			for (String criteria : splittedInput) {
				paramsArray.add(prefix + criteria);
			}
		}
		return paramsArray;
	}
	
	public static double roundDouble(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = BigDecimal.valueOf(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public static Map<String, String> getUTCTimezoneFromString(String strTimeZone) {
	    Map<String, String> result ;
	     
	     switch (strTimeZone) {
	     //TimeZones UTC
	     case  "Dateline Standard Time" :   result = setTomeZoneAndOffset("-12:00", -720); break;
	     case  "UTC-11" :  					result = setTomeZoneAndOffset("-11:00", -660);  break; 
	     case  "Aleutian Standard Time" : 
	     case  "Hawaiian Standard Time" :  result = setTomeZoneAndOffset("-10:00", -600);  break; 
	     case  "Marquesas Standard Time" : result = setTomeZoneAndOffset("-09:30", -570);  break; 
	     case  "Alaskan Standard Time" :  
	     case  "UTC-09" :  					result = setTomeZoneAndOffset("-09:00", -540);  break; 
	     case  "Pacific Standard Time (Mexico)" : 
	     case  "UTC-08" : 
	     case  "Pacific Standard Time" :  result = setTomeZoneAndOffset("-08:00", -480);  break; 
	     case  "US Mountain Standard Time" :  
	     case  "Mountain Standard Time (Mexico)" : 
	     case  "Mountain Standard Time" :  result = setTomeZoneAndOffset("-07:00", -420);  break; 
	     case  "Central America Standard Time" :  
	     case  "Central Standard Time" :  
	     case  "Easter Island Standard Time" :  
	     case  "Central Standard Time (Mexico)" : 
	     case  "Canada Central Standard Time" :  result = setTomeZoneAndOffset("-06:00", -360);  break; 
	     case  "SA Pacific Standard Time" : 
	     case  "Eastern Standard Time (Mexico)" :  
	     case  "Eastern Standard Time" : 
	     case  "Haiti Standard Time" : 
	     case  "Cuba Standard Time" : 
	     case  "US Eastern Standard Time" :  result = setTomeZoneAndOffset("-05:00", -300);  break; 
	     case  "Paraguay Standard Time" : 
	     case  "Atlantic Standard Time" : 
	     case  "Venezuela Standard Time" : 
	     case  "Central Brazilian Standard Time" :
	     case  "SA Western Standard Time" : 
	     case  "Pacific SA Standard Time" : 
	     case  "Turks And Caicos Standard Time" :  result = setTomeZoneAndOffset("-04:00", -240);  break; 
	     case  "Newfoundland Standard Time" :  result = setTomeZoneAndOffset("-03:30", -210);  break; 
	     case  "Tocantins Standard Time" :  
	     case  "E. South America Standard Time" : 
	     case  "SA Eastern Standard Time" :  
	     case  "Argentina Standard Time" : 
	     case  "Greenland Standard Time" : 
	     case  "Montevideo Standard Time" : 
	     case  "Saint Pierre Standard Time" : 
	     case  "Bahia Standard Time" :  result = setTomeZoneAndOffset("-03:00", -180);  break; 
	     case  "UTC-02" :  				
	     case  "Mid-Atlantic Standard Time" :  result = setTomeZoneAndOffset("-02:00", -120);  break; 
	     case  "Azores Standard Time" : 
	     case  "Cape Verde Standard Time" :  result = setTomeZoneAndOffset("-01:00", -60);  break; 
	     case  "UTC" :  				result = setTomeZoneAndOffset("UTC", 0);  break; 
	     case  "Morocco Standard Time" :  
	     case  "GMT Standard Time" : 
	     case  "Greenwich Standard Time" :  result = setTomeZoneAndOffset("+00:00", 0);  break; 
	     case  "W. Europe Standard Time" :  
	     case  "Central Europe Standard Time" : 
	     case  "Romance Standard Time" : 
	     case  "Central European Standard Time" : 
	     case  "W. Central Africa Standard Time" : 
	     case  "Namibia Standard Time" :  result = setTomeZoneAndOffset("+01:00", 60);  break; 
	     case  "Jordan Standard Time" :  
	     case  "GTB Standard Time" :  
	     case  "Middle East Standard Time" :  
	     case  "Egypt Standard Time" :  
	     case  "E. Europe Standard Time" :  
	     case  "Syria Standard Time" :  
	     case  "West Bank Standard Time" :  
	     case  "South Africa Standard Time" :  
	     case  "FLE Standard Time" :  
	     case  "Turkey Standard Time" :  
	     case  "Israel Standard Time" :  
	     case  "Kaliningrad Standard Time" : 
	     case  "Libya Standard Time" :  result = setTomeZoneAndOffset("+02:00", 120);  break; 
	     case  "Arabic Standard Time" :  
	     case  "Arab Standard Time" :  
	     case  "Belarus Standard Time" :  
	     case  "Russian Standard Time" :  
	     case  "E. Africa Standard Time" :  result = setTomeZoneAndOffset("+03:00", 180);  break; 
	     case  "Iran Standard Time" :  result = setTomeZoneAndOffset("+03:30", 210);  break; 
	     case  "Arabian Standard Time" :  
	     case  "Astrakhan Standard Time" :  
	     case  "Azerbaijan Standard Time" : 
	     case  "Russia Time Zone 3" :  
	     case  "Mauritius Standard Time" :  
	     case  "Georgian Standard Time" :  
	     case  "Caucasus Standard Time" :  result = setTomeZoneAndOffset("+04:00", 240);  break; 
	     case  "Afghanistan Standard Time" :  result = setTomeZoneAndOffset("+04:30", 270);  break; 
	     case  "West Asia Standard Time" :  
	     case  "Ekaterinburg Standard Time" :  
	     case  "Pakistan Standard Time" :  result = setTomeZoneAndOffset("+05:00", 300);  break; 
	     case  "India Standard Time" :  
	     case  "Sri Lanka Standard Time" :  result = setTomeZoneAndOffset("+05:30", 330);  break; 
	     case  "Nepal Standard Time" :  result = setTomeZoneAndOffset("+05:45", 345);  break; 
	     case  "Central Asia Standard Time" : 
	     case  "Bangladesh Standard Time" : 
	     case  "N. Central Asia Standard Time" :  result = setTomeZoneAndOffset("+06:00", 360);  break; 
	     case  "Myanmar Standard Time" :  result = setTomeZoneAndOffset("+06:30", 390);  break; 
	     case  "SE Asia Standard Time" :  
	     case  "Altai Standard Time" :  
	     case  "W. Mongolia Standard Time" :  
	     case  "North Asia Standard Time" :  
	     case  "Tomsk Standard Time" :  result = setTomeZoneAndOffset("+07:00", 420);  break; 
	     case  "China Standard Time" :  
	     case  "North Asia East Standard Time" : 
	     case  "Singapore Standard Time" :  
	     case  "W. Australia Standard Time" :  
	     case  "Taipei Standard Time" :  
	     case  "Ulaanbaatar Standard Time" :  result = setTomeZoneAndOffset("+08:00", 480);  break; 
	     case  "North Korea Standard Time" :  result = setTomeZoneAndOffset("+08:30", 510);  break; 
	     case  "Aus Central W. Standard Time" :  result = setTomeZoneAndOffset("+08:45", 525);  break; 
	     case  "Transbaikal Standard Time" :  
	     case  "Tokyo Standard Time" :  
	     case  "Korea Standard Time" :  
	     case  "Yakutsk Standard Time" :  result = setTomeZoneAndOffset("+09:00", 540);  break; 
	     case  "Cen. Australia Standard Time" :  
	     case  "AUS Central Standard Time" :  result = setTomeZoneAndOffset("+09:30", 570);  break; 
	     case  "E. Australia Standard Time" :  
	     case  "AUS Eastern Standard Time" :  
	     case  "West Pacific Standard Time" :  
	     case  "Tasmania Standard Time" :  
	     case  "Vladivostok Standard Time" :  result = setTomeZoneAndOffset("+10:00", 600);  break; 
	     case  "Lord Howe Standard Time" :  result = setTomeZoneAndOffset("+10:30", 630);  break; 
	     case  "Bougainville Standard Time" :  
	     case  "Russia Time Zone 10" :  
	     case  "Magadan Standard Time" :  
	     case  "Norfolk Standard Time" :  
	     case  "Sakhalin Standard Time" :  
	     case  "Central Pacific Standard Time" :  result = setTomeZoneAndOffset("+11:00", 660);  break; 
	     case  "Russia Time Zone 11" :  
	     case  "New Zealand Standard Time" :  
	     case  "UTC+12" :  				
	     case  "Fiji Standard Time" :  
	     case  "Kamchatka Standard Time" :  result = setTomeZoneAndOffset("+12:00", 720);  break; 
	     case  "Chatham Islands Standard Time" :  result = setTomeZoneAndOffset("+12:45", 765);  break; 
	     case  "Tonga Standard Time" :  
	     case  "Samoa Standard Time" :  result = setTomeZoneAndOffset("+13:00", 780);  break; 
	     case  "Line Islands Standard Time" :  result = setTomeZoneAndOffset("+14:00", 840);  break; 
	
	         default:
	             throw new IllegalArgumentException("Invalid time zone: " + strTimeZone);
	     }
	     return result;
		
	}
	 protected static Map<String, String> setTomeZoneAndOffset(String timeZone, int timeOffset ){
		 Map<String, String> result = new HashMap<>();
	     result.put("timeZone",timeZone); 
	     result.put("timeOffset",String.valueOf(timeOffset));
	     return result;
	 }
	/**
	 * @deprecated (use getDateTimePeriod())
	 * @param dateTimeFrom
	 * @param dateTimeTo
	 * @return
	 */
	@Deprecated
	//change dateTimeFrom -> since; dateTimeTo-> period
	public Map<String, String> getDateTimePeriod (String dateTimeFrom,String dateTimeTo){
		HashMap<String, String> result = new HashMap<>();
		long currTimeInMs = System.currentTimeMillis();
		long inMs24hrs = 24* 60*60 * 1000L;
		if(isADateTime(dateTimeFrom) && isADateTime(dateTimeTo)){
			result.put(dateTimeFrom, dateTimeFrom);
			result.put(dateTimeTo, dateTimeTo);
		}
		else {
			if(dateTimeFrom.contains(week) || dateTimeTo.contains(week)){
				 DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
				 df.setTimeZone(utc3TimeZone);
				 Date lastWeek = new Date(currTimeInMs - 7*inMs24hrs); 
				 result.put(dateTimeFrom, df.format(lastWeek));
				 result.put(dateTimeTo, getCurrentLocalDateTime());
					
				 
			}
			if(dateTimeFrom.contains(day) || dateTimeTo.contains(day)){
				 DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
				 df.setTimeZone(utc3TimeZone);
				 Date lastDay = new Date(currTimeInMs - inMs24hrs); 
				 result.put(dateTimeFrom, df.format(lastDay));
				 result.put(dateTimeTo, getCurrentLocalDateTime());
			}
			// TODO: this is wrong!
			if(dateTimeFrom.equals(yesterday) || dateTimeTo.equals(yesterday)){
				 DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
				 df.setTimeZone(utc3TimeZone);
				 Date twodaysAgo = new Date(currTimeInMs - 2*inMs24hrs); 
				 Date lastDay = new Date(currTimeInMs - 24* inMs24hrs); 
				 result.put(dateTimeFrom, df.format(twodaysAgo));
				 result.put(dateTimeTo, df.format(lastDay));
			}
		}
		DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		if(dateTimeFrom.equals("вчера 00:00:00") && dateTimeTo.equals("вчера 23:59:59")){
			 result.put(dateTimeFrom, df.format(currTimeInMs -(currTimeInMs % inMs24hrs)-inMs24hrs));
			result.put(dateTimeTo, df.format(currTimeInMs -(currTimeInMs % inMs24hrs)-1));
		}
		return result;
	}

	public Map<String, String> getDateTimePeriodInUTC (String dateTimePeriod){
		return	getDateTimePeriod (dateTimePeriod, TimeZone.getTimeZone("UTC"),"",false);
	}

	public Map<String, String> getDateTimePeriodInGMTPlus3 (String dateTimePeriod){
		return	getDateTimePeriod (dateTimePeriod, TimeZone.getTimeZone("GMT+3"),"",false);
	}
	
	public static Map<String, String> getDateTimePeriod (String dateTimePeriod, TimeZone timezone,String dateTimeFormat, boolean isRoadEventRequest){
		DateFormat df;
		HashMap<String, String> result = new HashMap<>();
		if(dateTimeFormat==null || dateTimeFormat.equals("")){
			df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
		} else {
			df = new SimpleDateFormat(dateTimeFormat, Locale.ENGLISH);	
		}
		 
		if(timezone!=null ){
			 df.setTimeZone(timezone); 
		 } else{
		 df.setTimeZone(TimeZone.getTimeZone("UTC"));
		 }
		 long currTimeInMs = System.currentTimeMillis();
		 long inMs24hrs = 24* 60*60 * 1000L;
		 long inMs1hr = 60*60 * 1000L;
		 long offset =0;
		 if(isRoadEventRequest)
			 offset = inMs1hr*3;
		 
		 long todayMidnight = currTimeInMs -(currTimeInMs % inMs24hrs);

			if(dateTimePeriod.contains(week) || dateTimePeriod.contains("Неделя")){
				 Date lastWeek = new Date(todayMidnight - 7*inMs24hrs- offset); 
				 result.put(dateTimeFrom, df.format(lastWeek));
				 if(isRoadEventRequest)
					 result.put(dateTimeTo, df.format(todayMidnight- offset-1));
				 else
					 result.put(dateTimeTo, df.format(currTimeInMs));
					
				 
			}
			if(dateTimePeriod.contains("2 недели") ){
				 Date twoWeeksAgo = new Date(todayMidnight - (14*inMs24hrs)- offset); 
				 result.put(dateTimeFrom, df.format(twoWeeksAgo));
				 if(isRoadEventRequest)
					 result.put(dateTimeTo, df.format(todayMidnight- offset-1));
				 else
					 result.put(dateTimeTo, df.format(currTimeInMs));
			}
			if(dateTimePeriod.contains("Месяц") ){
				java.util.Date oneMonthAgo = new Date(todayMidnight - offset); 
				 Calendar cal = Calendar.getInstance();
					cal.setTime(oneMonthAgo);
					cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)-1, cal.get(Calendar.DATE));

				 oneMonthAgo = cal.getTime();
				 result.put(dateTimeFrom, df.format(oneMonthAgo));
				 if(isRoadEventRequest)
					 result.put(dateTimeTo, df.format(todayMidnight- offset-1));
				 else
					 result.put(dateTimeTo, df.format(currTimeInMs));
			}
			
			if(dateTimePeriod.contains("календарные сутки") ){
				 long startOfDay = currTimeInMs - (currTimeInMs % inMs24hrs); 
				 long endOfDay = startOfDay+inMs24hrs-1; 
				 result.put(dateTimeFrom, df.format(startOfDay));
				 result.put(dateTimeTo, df.format(endOfDay));
			}
			
			if(dateTimePeriod.contains(day) ){
				 Date lastDay = new Date(currTimeInMs - inMs24hrs); 
				 result.put(dateTimeFrom, df.format(lastDay));
				 result.put(dateTimeTo, df.format(currTimeInMs));
			}
			
			if(dateTimePeriod.contains(yesterday) || dateTimePeriod.toLowerCase().contains("вчера")){
				Date twodaysAgo=null;
				Date lastDay=null;
				 
				 if(isRoadEventRequest){
					 twodaysAgo = new Date(todayMidnight - (inMs24hrs)- offset); 
					 lastDay = new Date(todayMidnight - offset); 
				 } else {
					 twodaysAgo = new Date(todayMidnight - (inMs24hrs)); 
					 lastDay = new Date(todayMidnight -1); 
				 }
					 
				 result.put(dateTimeFrom, df.format(twodaysAgo));
				 result.put(dateTimeTo, df.format(lastDay));
			}
			if(dateTimePeriod.contains("сегодня") ||  dateTimePeriod.contains("Сегодня")){
				 Date todayMid = new Date(todayMidnight - offset-1); 
				 result.put(dateTimeFrom, df.format(todayMid ));
				 result.put(dateTimeTo, df.format(currTimeInMs));
			}

			if(dateTimePeriod.contains("час") ){
				long startOfHour = currTimeInMs -(currTimeInMs % inMs1hr);
				long endOfHour = startOfHour + inMs1hr -1;
				 result.put(dateTimeFrom, df.format(startOfHour));
				 result.put(dateTimeTo, df.format(endOfHour));
			}
			
			if(dateTimePeriod.matches("\\d{1,2}[ ]{0,2}дня$")){
				int noOfDays = Integer.parseInt(dateTimePeriod.substring(0,dateTimePeriod.indexOf("дня")-1));
				long startFrom = currTimeInMs - (inMs24hrs*noOfDays);
				result.put(dateTimeFrom, df.format(startFrom));
				 result.put(dateTimeTo, df.format(currTimeInMs));
			}
			
			//TODO: add месяц
		return result;
	}
	
	
	/**
	 * substract days, and add hours (requested in array) since current midnight 
	 * @param arr
	 * @return current date - arr0; midnight+arr1
	 */
	
	public java.util.Date dateTimeCorrectionFromInputArr (List arr){
		if (arr.size()==2){
			Calendar calCur = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			
			calCur.add(Calendar.DATE, Integer.parseInt(arr.get(0).toString())*-1);
			
			int hoursToadd = Integer.parseInt(arr.get(1).toString());
			if(hoursToadd>0 && hoursToadd<24){				
				calCur.set(Calendar.HOUR_OF_DAY, hoursToadd);
			} else {
				calCur.set(Calendar.HOUR_OF_DAY, 0);
			}
			calCur.set(Calendar.MINUTE, 0);
			calCur.set(Calendar.SECOND, 0);
			calCur.set(Calendar.MILLISECOND, 0);
			return calCur.getTime();
		} else {
			return null;
		}
	}
	
	public static String getIfExists(JSONObject obj, String key) {
		String result = null;
		String[] keyArr = key.split("\\.");
		if (keyArr.length <= 1) {
			if (obj.containsKey(key)) {
				result = obj.get(key).toString();
			} 
		} else {
			
			try {
				if (keyArr[1].contains("["))
					keyArr[1] = keyArr[1].substring(0, keyArr[1].indexOf('['));
				if(obj.containsKey(keyArr[0]))
				result = ReplyToJSON.extractPathFromJson(obj, key);
			} catch (Exception e) {
				log.error(ERROR,e);
			}
		}
		return result;
	}

	//be aware result is approx.
	double distanceBetween2Coordinates(double lat1, double lon1, double lat2, double lon2){  // generally used geo measurement function
		double r = 6378.137; // Radius of earth in KM
		double dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
		double dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
		double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
	    Math.sin(dLon/2) * Math.sin(dLon/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double d = r * c;
	    return d * 1000; // meters
	}
	
}