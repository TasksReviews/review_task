package com.t1.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

public class JsonFileParse extends AbstractClass {

	public static List getTrackPointsFromFile(String filePath) {
		File file;
		if(filePath.equals("<шаблон поездок с событиями>")){
			file = new File("src\\test\\resources\\Test_Data\\tracks\\tripsWithEventsTepmlate.json");
		} else {
			file = new File("src\\test\\resources\\Test_Data\\"+filePath);
		}
		List listOfEvents = new ArrayList();
		try (BufferedReader br = new BufferedReader(new FileReader(file)))
		{

			String line;

			StringBuilder sb = new StringBuilder();

			while ((line = br.readLine()) != null) {
				sb.append(line.trim());
			}
			Pattern pattern = Pattern.compile("\\],\\[") ;
			String[] events = pattern.split(sb.toString().substring(1,sb.toString().length()-1));
			for (String event : events) {
				//an event may contain list of points
				List<JSONObject> listOfPoints = new ArrayList<>();
				event = event.replaceAll("(ISODate\\()\"(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d{3}Z)\"\\)", "\"$1$2)\"");
				event = event.replaceAll("(ObjectId\\()\"(\\d{24})\"\\)", "\"$1$2)\"");
				Pattern pattern2 = Pattern.compile("\\},\\{") ;
				String[] points = pattern2.split(event.substring(1,event.length()-1));
				for (String point : points) {
					listOfPoints.add(toJson("{"+point+"}"));
				}
				listOfEvents.add(listOfPoints);
			}
			
		} catch (IOException e) {
			log.error(ERROR,e);
		}
		return listOfEvents;
	}

}
