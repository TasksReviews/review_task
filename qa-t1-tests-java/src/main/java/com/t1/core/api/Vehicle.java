package com.t1.core.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.common.annotations.Beta;
import com.jayway.jsonpath.JsonPath;
import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetDeviceHistory;
import com.t1.core.mongodb.GetEmployeeInfoFromMongo;
import com.t1.core.mongodb.GetGroupInfoFromMongo;
import com.t1.core.mongodb.GetInfoFromMongo;
import com.t1.core.mongodb.GetInsurancePolicyInfoFromMongo;
import com.t1.core.mongodb.GetVehicleAccidentsFromMongo;
import com.t1.core.mongodb.GetVehicleEmployeeHistoryFromMongo;
import com.t1.core.mongodb.GetVehicleEmployeeInsurancePoliciesInfoFromMongo;
import com.t1.core.mongodb.GetVehicleInfoFromMongo;
import com.t1.core.mongodb.GetVehicleMaintenancesInfoFromMongo;
import com.t1.core.mongodb.GetVehicleRepairsInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Step;


@SuppressWarnings("rawtypes")
public class Vehicle  extends AbstractClass implements SharedContainerInterface{

	private static HashMap initMap ;
	private JSONObject urlParameters = new JSONObject();
	private JSONObject fuel = new JSONObject();
	protected JSONObject geoZone = new JSONObject();

	private ArrayList insurancePolicyReq = new ArrayList<JSONObject>();
	private ArrayList maintenancesReq = new ArrayList<JSONObject>();
	private ArrayList accidentsReq = new ArrayList<JSONObject>();
	private ArrayList repairsReq = new ArrayList<JSONObject>();
	private TelematicVirtualTracker telematicsDevice;
	protected JSONObject department = new JSONObject();
	private JSONObject group = new JSONObject();
	protected ArrayList maintenances = new ArrayList<JSONObject>();
	protected ArrayList repairs = new ArrayList<JSONObject>();
	protected ArrayList accidents = new ArrayList<JSONObject>();
	protected ArrayList<Employee> employees;
	private Employee employeeObj;
	private String token = null;
	private static JSONObject initJson;
	private String vehicleId;
	private String name;
	private String type;
	private String number;
	private String model;
	private String mark;
	private String vinCode;
	private String avatarUri;
	private String color;
	private String  isDeactivated;
	private String isActive;
	private String garageNumber;
	private String maxSpeed;
	private String capacity;
	private String description;
	private String employeeId;
	private String deviceId;
	private String geoZoneId;
	private String groupId;
	private String groupName;
	private String departmentId;
	private String customerId;
	private static  JSONArray insuranceIdsList = new JSONArray();
	private static JSONArray maintenancesIdsList = new JSONArray();
	private static JSONArray repairsIdsList = new JSONArray();
	private static JSONArray accidentsIdsList = new JSONArray();
	private long currentDateTime;
	private String registered;
	private String vehCreated;
	private String vehUpdated;
	private String customerName;

	private  static final String VEHICLE_STR = "vehicle";
	private  static final String MODEL_STR = "model";
	private  static final String NUMBER_STR = "number";
	private  static final String VINCODE_STR = "vinCode";
	private  static final String COLOR_STR = "color";
	private  static final String GARAGE_NUMBER = "garageNumber";
	private  static final String MAXSPEED_STR = "maxSpeed";
	private  static final String CAPACITY_STR = "capacity";
	private  static final String DESCRIPTION_STR = "description" ;
	private  static final String AVATARURI_STR = "avatarUri";
	private  static final String FALSE = "false";
	private  static final String SOURCE = "source" ;
	private  static final String FUEL_CONSUPTION = "fuelConsumption";
	private  static final String TANK_CAPACITY = "tankCapacity";
	private  static final String DOCUMENT_FILENAME = "documentFileName";
	private  static final String STATE = "state";
	private  static final String MILEAGE = "mileage";
	private  static final String ORGANIZATION = "organization";
	private  static final String PHONE = "phone";
	private  static final String ADDRESS = "address";
	private  static final String COMMENT = "comment";
	private  static final String TEST_CUTOMER_ID = "testCustomerId" ;
	private  static final String VEHICLES = "vehicles";
	private  static final String GROUPID_STR = "groupId";
	private  static final String DEPARTMENTID_STR = "departmentId";
	private  static final String EMPLOYEEID_STR = "employeeId";
	private  static final String DEVICEID_STR = "deviceId";
	private  static final String ISDEACTIVATED_STR = "isDeactivated" ;
	private  static final String REGISTERED_STR = "registered";
	private  static final String GROUP_STR = "group";
	private  static final String DEPARTMENT_STR = "department";
	private  static final String ISACTIVE_STR = "isActive";
	private  static final String FUEL_SOURCE_STR = "fuel.source";
	private  static final String FUEL_TYPE_STR = "fuel.type";
	private  static final String FUEL_CONSUMPTION_STR = "fuel.fuelConsumption";
	private  static final String FUEL_TANKCAPACITY_STR = "fuel.tankCapacity";
	private  static final String GEOZONEIDS_STR = "geoZoneIds";
	
	private  static final String  PATH_ID = "$._id.$oid";
	private  static final String  PATH_VEHICLE_ID = "$.vehicle.id";
	private  static final String  PATH_NUMBER = "$.Number";
	private  static final String  PATH_TYPE = "$.Type";
	private  static final String  PATH_REGISTERED_DATE = "$.Registered.$date";
	private  static final String  PATH_GROUP_ID = "$.group.id";
	private  static final String  PATH_GROUP_ID2 = "$.GroupId.$oid";
	private  static final String  PATH_DEPARTMENT_ID = "$.DepartmentId.$oid";
	
	private  static final String CREATE_AUTH_NEEDED = "для создания ТС необходимо пройти авторизацию";
	private  static final String UPDATE_AUTH_NEEDED = "для изменения ТС необходимо пройти авторизацию";
	private  static final String DELETE_AUTH_NEEDED = "для удаления ТС необходимо пройти авторизацию";
	private  static final String TOKEN_RECEIVED = "token received" ;
	private  static final String TEST_FAILED = "Test failed: ";
	private  static final String ADM_PANEL_HOST = "adminPanelHost";
	private  static final String AUTH_ADMPANEL = "authorizedAccount_admPanel";
	private  static final String MULTIPART = "multipart";
	private  static final String WRONG = " не верен";

	private  static final String API_VEHICLE_ID = "API:vehicle_Id" ;
	private  static final String API_VEHICLE_DEPARTMENT = "API:vehicle_departmentId";
	private  static final String API_VEHICLE_REGISTERED = "API:vehicle_registered" ;
	private  static final String API_VEHICLE_AVATARURI = "API:vehicle_avatarUri";
	private  static final String DB_VEHICLE_GROUP = "DB:vehicle_group";
	private  static final String PHONE_STR = "].phone";
	private  static final String DATE_STR= "].date";
	private  static final String ADDRESS_STR= "].address";
	private  static final String COMMENT_STR= "].comment";
	private  static final String STATE_STR = "].state";
	private  static final String MILEAGE_STR = "].mileage";
	private  static final String ORGANIZATION_STR = "].organization" ;
	private  static final String COST_STR = "].cost";
	private  static final String TECHNICAL_STATE_REPAIRS_STR = "technicalState.repairs";
	private  static final String NAME_STR = "].name";
	private  static final String TECHNICAL_STATE_ACCIDENTS_STR = "technicalState.accidents";
	private  static final String VEHICLE_VIEWCARD_SECONDARYTITLE_STR = "vehicleViewCard.secondaryTitle";
	private  static final String EMPLOYEES_GROUP_STR = "employees.groupId";
	
	private  static final String ZERO_ID = "000000000000000000000000";
	
	public String getVehicleId() {		return vehicleId;	}
	public void setVehicleId(String vehicleId) {		this.vehicleId = vehicleId;	}
	public String getName() {		return name;	}
	public void setName(String name) {		this.name = name;	}
	public String getModel() {		return model;	}
	public void setModel(String model) {		this.model = model;	}
	public String getMark() {		return mark;	}
	public void setMark(String mark) {		this.mark = mark;	}
	public String getVinCode() {		return vinCode;	}
	public void setVinCode(String vinCode) {		this.vinCode = vinCode;	}
	public String getNumber() {		return number;	}
	public void setNumber(String number) {		this.number = number;	}
	public String getType() {		return type;	}
	public void setType(String type) {		this.type = type;	}
	public String getCapacity() {		return capacity;	}
	public void setCapacity(String capacity) {		this.capacity = capacity;	}
	public String getRegistered() {		return registered;	}
	public void setRegistered(String registered) {		this.registered = registered;	}
	public String getGroupId() {		return groupId;	}
	public void setGroupId(String groupId) {		this.groupId = groupId;	}
	public String getEmployeeId() {		return employeeId;	}
	public void setEmployeeId(String employeeId) {		this.employeeId = employeeId;	}
	public String getCustomerId() {		return customerId;	}
	public void setCustomerId(String customerId) {		this.customerId = customerId;	}
	public String getCustomerName() {		return customerName;	}
	public void setCustomerName(String customerName) {		this.customerName = customerName;	}
	public String getDeviceId() {		return deviceId;	}
	public void setDeviceId(String deviceId) {		this.deviceId = deviceId;	}
	public String getDescription() {		return description;	}
	public void setDescription(String description) {		this.description = description;	}
	public String getGarageNumber() {		return garageNumber;	}
	public void setGarageNumber(String garageNumber) {		this.garageNumber = garageNumber;	}
	public String getMaxSpeed() {		return maxSpeed;	}
	public void setMaxSpeed(String maxSpeed) {		this.maxSpeed = maxSpeed;	}
	public String getGroupName() {		return groupName;	}
	public void setGroupName(String groupName) {		this.groupName = groupName;	}
	public String getIsDeactivated() {		return isDeactivated;	}
	public void setIsDeactivated(String isDeactivated) {		this.isDeactivated = isDeactivated;	}
	public TelematicVirtualTracker getTelematicsDevice() {		return telematicsDevice;	}
	public void setTelematicsDevice(TelematicVirtualTracker telematicsDevice) {		this.telematicsDevice = telematicsDevice;	}
	public JSONObject getUrlParameters() {		return urlParameters;	}
	public void setUrlParameters(JSONObject urlParameters) {		this.urlParameters = urlParameters;	}
	public void putUrlParameters(Object key, Object value) {		this.urlParameters.put(key, value);	}
	public JSONObject getFuel() {		return fuel;	}
	public void setFuel(JSONObject fuel) {		this.fuel = fuel;	}
	public ArrayList getMaintenancesReq() {		return maintenancesReq;	}
	public void setMaintenancesReq(ArrayList maintenancesReq) {		this.maintenancesReq = maintenancesReq;	}
	public ArrayList getRepairsReq() {		return repairsReq;	}
	public void setRepairsReq(ArrayList repairsReq) {		this.repairsReq = repairsReq;	}
	public ArrayList getAccidentsReq() {		return accidentsReq;	}
	public void setAccidentsReq(ArrayList accidentsReq) {		this.accidentsReq = accidentsReq;	}
	public ArrayList getInsurancePolicyReq() {		return insurancePolicyReq;	}
	public void setInsurancePolicyReq(ArrayList insurancePolicyReq) {		this.insurancePolicyReq = insurancePolicyReq;	}
	public static JSONArray getAccidentsIdsList() {		return accidentsIdsList;	}
	public static void setAccidentsIdsList(JSONArray accidentsIdsList) {		Vehicle.accidentsIdsList = accidentsIdsList;	}
	public JSONObject getGroup() {		return group;	}
	public void setGroup(JSONObject group) {		this.group = group;	}
	public static JSONArray getInsuranceIdsList() {		return insuranceIdsList;	}
	public static void setInsuranceIdsList(JSONArray insuranceIdsList) {		Vehicle.insuranceIdsList = insuranceIdsList;	}
	public static JSONArray getMaintenancesIdsList() {		return maintenancesIdsList;	}
	public static void setMaintenancesIdsList(JSONArray maintenancesIdsList) {		Vehicle.maintenancesIdsList = maintenancesIdsList;	}
	public static JSONArray getRepairsIdsList() {		return repairsIdsList;	}
	public static void setRepairsIdsList(JSONArray repairsIdsList) {		Vehicle.repairsIdsList = repairsIdsList;	}
	
	
	public Vehicle(boolean initializeObject) {
		if (initializeObject) {
			setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues(VEHICLE_STR, getProperty("stdPattern")));
			setMinimalFields(getInitJson());
		}
	}
	
/*	public Vehicle(InitLevels init) {
		if (init.equals(InitLevels.min)) {
			initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues(VEHICLE, getProperty("stdPattern"));
			setMinimalFields(initJson);
		}
		if (init.equals(InitLevels.all)) {
			initJson = new InitDefValues().initializeInputWithDefaultOrRandomValues(VEHICLE, getProperty("stdPattern"));
			setMinimalFields(initJson);
			setAllVehicleFields(initJson);
		}
	}
*/	
	
	public Vehicle(Boolean allFields, String pattern) {
		setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues(VEHICLE_STR,getProperty(pattern)));
		setMinimalFields(getInitJson());

		if (allFields) {
			setAllVehicleFields(getInitJson());
		}

	}
	
	@SuppressWarnings("unchecked")
	public Vehicle(Boolean allFields, List<Employee> employeesObjects, JSONObject group,
			TelematicVirtualTracker telematicsDevice, JSONObject telematicsGeozone, JSONObject department, String pattern) {
		employeeObj = employeesObjects.get(0);
		setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues(VEHICLE_STR,getProperty(pattern)));

		setMinimalFields(getInitJson());

		this.setGroup(group);
		this.geoZone = telematicsGeozone;
		this.setTelematicsDevice(telematicsDevice);
		this.department = department;

		this.setDeviceId(telematicsDevice.getTrackerId() );
		this.geoZoneId = JsonPath.read(telematicsGeozone, PATH_ID).toString();
		this.setGroupId(JsonPath.read(group, PATH_ID).toString());
		this.departmentId = JsonPath.read(department, PATH_ID).toString();
		this.setEmployeeId(employeeObj.getEmployeeId());

		this.urlParameters.put(GROUPID_STR, this.getGroupId());
		this.urlParameters.put(DEPARTMENTID_STR, this.departmentId);
		this.urlParameters.put("geozoneIds", Arrays.asList(geoZoneId));
		this.urlParameters.put(DEVICEID_STR, this.getDeviceId());
		this.urlParameters.put(EMPLOYEEID_STR, employeeObj.getEmployeeId());

		if (allFields) {
			setAllVehicleFields(getInitJson());
			
		}

	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Step("Создание нового ТС с минимальным количеством полей и случайными значениями")
	public List<org.json.simple.JSONObject> createNewVehicle() {
		List<org.json.simple.JSONObject> mongoVehicleInfo = null;
		try {
			token = getCurAuthAccToken(authorizedAccountT1client);
			assertTrue(TOKEN_RECEIVED,token != null,CREATE_AUTH_NEEDED);
			String url = getProperty("host") + "/Vehicle/Add";
			org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,true,true);
			
			setVehicleId(ReplyToJSON.extractPathFromJson(registrationReply, PATH_VEHICLE_ID));
			TearDownExecutor.addVehicleToTeardown(this.getVehicleId());
			getInsAndTechnicalSatateIds(registrationReply);
			vehCreated=ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.created");
			// get account info from mongo
			mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(getVehicleId(),"");
			// compare mongo with input
			checkRegistredVehicle(mongoVehicleInfo.get(0), registrationReply, false,Vehicle.initMap);
		} catch (Exception e) {
			log.error(TEST_FAILED + e);
			assertTrue(false, e.toString());
		}
		return mongoVehicleInfo;
	}
	
	@SuppressWarnings("unchecked")
	@Step("Создание нового ТС в панели администратора, с минимальным количеством полей и случайными значениями")
	public List<org.json.simple.JSONObject> createInAdminPanelNewVehicle(){
		String url = getProperty(ADM_PANEL_HOST) +"/Api/Admin/Vehicles";
		List<org.json.simple.JSONObject> mongoVehicleInfo = null;
		try {
			token = getCurAuthAccToken(AUTH_ADMPANEL);
			assertTrue(TOKEN_RECEIVED,token != null,CREATE_AUTH_NEEDED);
			
			org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,true,true);
			
			setVehicleId(ReplyToJSON.extractPathFromJson(registrationReply, "$.id"));
			TearDownExecutor.addVehicleToTeardown(this.getVehicleId());
//			getInsAndTechnicalSatateIds(registrationReply);
//			veh_created=ReplyToJSON.extractPathFromJson(registrationReply, "$.created");
			// get account info from mongo
			mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(getVehicleId(),"");
//			// compare mongo with input
//			checkRegistredVehicle(mongoVehicleInfo.get(0), registrationReply, false,Vehicle.initMap);
		} catch (Exception e) {
			log.error(TEST_FAILED + e);
			assertTrue(false, e.toString());
		}
		return mongoVehicleInfo;
	}

	@SuppressWarnings("unchecked")
	@Step("Создание нового ТС с полым количеством полей")
	public List<org.json.simple.JSONObject> createNewVehicleWithFiles(HashMap<String,String> filesMap) {
		List<org.json.simple.JSONObject> mongoVehicleInfo = null;
		try {
			token = getCurAuthAccToken(authorizedAccountT1client);
			assertTrue(TOKEN_RECEIVED,token != null,CREATE_AUTH_NEEDED);
			String url = getProperty("host") + "/Vehicle/Add";
			org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,MULTIPART,filesMap,true).get(0);
			
			setVehicleId(ReplyToJSON.extractPathFromJson(registrationReply, PATH_VEHICLE_ID));
			TearDownExecutor.addVehicleToTeardown(this.getVehicleId());
			getInsAndTechnicalSatateIds(registrationReply);
			vehCreated=ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.created");
			// get account info from mongo
			mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(getVehicleId(),"");
//			// compare mongo with input
			checkRegistredVehicle(mongoVehicleInfo.get(0), registrationReply,false, Vehicle.initMap);
		} catch (Exception e) {
			log.error(TEST_FAILED + e);
			assertTrue(false, e.toString());
		}
		return mongoVehicleInfo;
	}
	
	@SuppressWarnings("unchecked")
	@Step("получение ТС по id: {0}")
	public List<org.json.simple.JSONObject> getVehiclebyId(String reqVehicleId) {
		String url = getProperty("host") + "/Vehicle/"+reqVehicleId;
		
		List<org.json.simple.JSONObject> mongoVehicleInfo = null;
		try {
			token = getCurAuthAccToken(authorizedAccountT1client);
			assertTrue(TOKEN_RECEIVED,token != null,"для получения ТС необходимо пройти авторизацию");
			org.json.simple.JSONObject getReply = (JSONObject) SendRequestToAPI.sendGetRequest( url,"GET", "","", token).get(0);
		
			assertEquals(API_VEHICLE_ID,reqVehicleId, ReplyToJSON.extractPathFromJson(getReply, PATH_VEHICLE_ID),"получен некорректный id ТС");
		//	vehicleId = ReplyToJSON.extractPathFromJson(getReply, PATH_VEHICLE_ID);
			getInsAndTechnicalSatateIds(getReply);
			// get account info from mongo
			mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(reqVehicleId,"");
//			// compare mongo with input
			checkRegistredVehicle(mongoVehicleInfo.get(0), getReply,false, Vehicle.initMap);
		} catch (Exception e) {
			log.error(TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return mongoVehicleInfo;
	}
	
	@Beta  //not completed!
	@Step("Валидация: полученный JSON содержит данные с которыми ТС был запрошено")
	public void checkRegistredVehicleAdminReply(JSONObject jsonMongoReply, JSONObject jsonApiRegistrationReply,boolean allVehList, Map initMap) {
		try {
		assertContainsMngId("DB:vehicle_Id",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_ID), "id" + WRONG);
		assertContainsMngId(API_VEHICLE_ID,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.id"), "id" + WRONG);
		
		if (this.getName() != null) {
			assertEquals("DB:vehicle_name",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Name"), this.getName(), "name" + WRONG);
			assertEquals("API:vehicle_name",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.name"), this.getName(),"name" + WRONG);
		}
		if (this.getModel() != null) {
			assertEquals("DB:vehicle_model",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Model"), this.getModel(), MODEL_STR + WRONG);
			assertEquals("API:vehicle_model",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.model"), this.getModel(),	MODEL_STR + WRONG);
		}

		if (this.getMark() != null) {
			assertEquals("DB:vehicle_mark",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Mark"), this.getMark(), "mark" + WRONG);
			assertEquals("API:vehicle_mark",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.mark"), this.getMark(),"mark" + WRONG);
		}

		if (this.getNumber() != null) {
			assertEquals("DB:vehicle_number",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_NUMBER), this.getNumber(),	NUMBER_STR + WRONG);
			assertEquals("API:vehicle_number",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.number"), this.getNumber(),NUMBER_STR + WRONG);
		}
		
		if (this.getType() != null) {
			assertEquals("DB:vehicle_type",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_TYPE), this.getType(), "type" + WRONG);
			assertEquals("API:vehicle_type",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.type"), this.getType(),"type" + WRONG);
		}

		if (this.color != null) {
			assertEquals("DB:vehicle_color",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Color"), this.color, COLOR_STR + WRONG);
			assertEquals("API:vehicle_color",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.color"), this.color,	COLOR_STR + WRONG);
		}

		if (this.getIsDeactivated() != null) {
			assertEquals("DB:vehicle_isDeactivated",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.IsDeactivated"), this.getIsDeactivated(),ISDEACTIVATED_STR + WRONG);
			assertEquals("API:vehicle_isDeactivated",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.isDeactivated"),	this.getIsDeactivated(), ISDEACTIVATED_STR + WRONG);
			if(this.getIsDeactivated().equals("true")){
				assertEquals("API:vehicle_status",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.status.value"),	"Deactivated", ISDEACTIVATED_STR + WRONG);
			}
		}

		if (this.getGarageNumber() != null) {
			assertEquals("DB:vehicle_garageNumber",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.GarageNumber"), this.getGarageNumber(),	GARAGE_NUMBER + WRONG);
			assertEquals("API:vehicle_garageNumber",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.garageNumber"),	this.getGarageNumber(), GARAGE_NUMBER + WRONG);
		}
		if (this.getMaxSpeed() != null) {
			assertEquals("DB:vehicle_maxSpeed",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.MaxSpeed"), this.getMaxSpeed(),	MAXSPEED_STR + WRONG);
			assertEquals("API:vehicle_maxSpeed",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.maxSpeed"),this.getMaxSpeed(), MAXSPEED_STR + WRONG);
		}

		if (this.getCapacity() != null) {
			assertEquals("DB:vehicle_capacity",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Capacity"), this.getCapacity(),	CAPACITY_STR + WRONG);
			assertEquals("API:vehicle_capacity",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.capacity"),this.getCapacity(), CAPACITY_STR + WRONG);
		}
		
		
		if (this.getRegistered() != null) {
			assertEquals("DB:vehicle_registered",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_REGISTERED_DATE), dateToUnixTime(this.getRegistered()),	REGISTERED_STR + WRONG);
			assertEquals(API_VEHICLE_REGISTERED,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.registered"),this.getRegistered()+" 00:00:00", REGISTERED_STR + WRONG);
		}

		if (this.getDescription() != null) {
			assertEquals("DB:vehicle_description",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Description"), 					this.getDescription(),DESCRIPTION_STR + WRONG);
			assertEquals("API:vehicle_description",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.description"),	this.getDescription(), DESCRIPTION_STR + WRONG);
		}
		if (this.avatarUri != null) {
			assertContains("DB:vehicle_avatarUri",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.AvatarUri"), this.avatarUri,AVATARURI_STR + WRONG);
			assertContains(API_VEHICLE_AVATARURI,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.avatarUri"),	this.avatarUri, AVATARURI_STR + WRONG);
		}

		if (this.getGroupId() != null) {
			assertEquals("API:vehicle_group",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_GROUP_ID),	this.getGroupId(), GROUP_STR + WRONG);
		}
		if(this.getGroupId() == null || this.getGroupId().equals("") || this.getGroupId().equals(ZERO_ID)  ){
			assertTrue(DB_VEHICLE_GROUP,! jsonMongoReply.containsKey("GroupId"),	"ожидалось что к ТС не привязана группа");
		} else {
			assertEquals(DB_VEHICLE_GROUP,ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_GROUP_ID2), this.getGroupId(),	GROUP_STR + WRONG);
		}

		if (this.departmentId != null) {
			assertEquals("DB:vehicle_departmentId",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_DEPARTMENT_ID), this.departmentId,	DEPARTMENT_STR + WRONG);
			assertEquals(API_VEHICLE_DEPARTMENT,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.departmentId"),	this.departmentId, DEPARTMENT_STR + WRONG);
		}
		
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные новогого ТС:" + e.toString());

		}
	}

	@SuppressWarnings("unchecked")
	@Step("Валидация: полученный JSON содержит данные с которыми ТС был запрошено")
	public void checkRegistredVehicle(JSONObject jsonMongoReply, JSONObject jsonApiRegistrationReply,boolean allVehList, Map initMap) {
		String pipe =" | ";
		try {
			String pref ="";
			if(! allVehList){
				pref = "vehicle.";
			}

			assertContainsMngId("DB:vehicle_Id",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_ID), pref+"id" + WRONG);
			assertContainsMngId(API_VEHICLE_ID,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"id"), pref+"id" + WRONG);
			assertContainsMngId("API:vehicle_vehicleViewCard.Id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"vehicleViewCard.id"), pref+"vehicleViewCard.id" + WRONG);
				


			if (this.getName() != null) {
				assertEquals("DB:vehicle_name",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Name"), this.getName(), "name" + WRONG);
				assertEquals("API:vehicle_name",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"name"), this.getName(),"name" + WRONG);
				assertEquals("API:vehicle_vehicleViewCard.name",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"vehicleViewCard.primaryTitle"), this.getName(),"vehicleViewCard.primaryTitle" + WRONG);
				
			}
			if (this.getModel() != null) {
				assertEquals("DB:vehicle_model",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Model"), this.getModel(), MODEL_STR + WRONG);
				assertEquals("API:vehicle_model",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+MODEL_STR), this.getModel(),	MODEL_STR + WRONG);
			}

			if (this.getMark() != null) {
				assertEquals("DB:vehicle_mark",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Mark"), this.getMark(), "mark" + WRONG);
				assertEquals("API:vehicle_mark",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"mark"), this.getMark(),"mark" + WRONG);
			}

			if (this.getNumber() != null) {
				assertEquals("DB:vehicle_number",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_NUMBER), this.getNumber(),	NUMBER_STR + WRONG);
				assertEquals("API:vehicle_number",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+NUMBER_STR), this.getNumber(),NUMBER_STR + WRONG);
			}
			if (this.getVinCode() != null) {
				assertEquals("DB:vehicle_vinCode",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.VinCode"), this.getVinCode(),VINCODE_STR + WRONG);
				assertEquals("API:vehicle_vinCode",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+VINCODE_STR),this.getVinCode(), VINCODE_STR + WRONG);
			}
			if (this.getType() != null) {
				assertEquals("DB:vehicle_type",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_TYPE), this.getType(), "type" + WRONG);
				assertEquals("API:vehicle_type",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"type"), this.getType(),"type" + WRONG);
			}

			if (this.color != null) {
				assertEquals("DB:vehicle_color",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Color"), this.color, COLOR_STR + WRONG);
				assertEquals("API:vehicle_color",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+COLOR_STR), this.color,	COLOR_STR + WRONG);
			}

			compareDateTimeUpTo1min("Created",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Created.$date"), 
											  ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"created"),this.currentDateTime);
			
			//TODO: correct for vehicle update
			if(vehUpdated!=null){
			compareDateTimeUpTo1min("Updated",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Updated.$date"), 
											  ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"updated"),this.currentDateTime);
			
//			assertTrue(! jsonMongoReply.containsKey("Updated"),"поле updated заполняется только после изменения ТС");
			}
			
			if (this.getIsDeactivated() != null) {
				assertEquals("DB:vehicle_isDeactivated",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.IsDeactivated"), this.getIsDeactivated(),ISDEACTIVATED_STR + WRONG);
				assertEquals("API:vehicle_isDeactivated",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+ISDEACTIVATED_STR),	this.getIsDeactivated(), ISDEACTIVATED_STR + WRONG);
			}

			if (this.getGarageNumber() != null) {
				assertEquals("DB:vehicle_garageNumber",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.GarageNumber"), this.getGarageNumber(),	GARAGE_NUMBER + WRONG);
				assertEquals("API:vehicle_garageNumber",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+GARAGE_NUMBER),	this.getGarageNumber(), GARAGE_NUMBER + WRONG);
			}
			if (this.getMaxSpeed() != null) {
				assertEquals("DB:vehicle_maxSpeed",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.MaxSpeed"), this.getMaxSpeed(),	MAXSPEED_STR + WRONG);
				assertEquals("API:vehicle_maxSpeed",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+MAXSPEED_STR),this.getMaxSpeed(), MAXSPEED_STR + WRONG);
			}

			if (this.getCapacity() != null) {
				assertEquals("DB:vehicle_capacity",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Capacity"), this.getCapacity(),	CAPACITY_STR + WRONG);
				assertEquals("API:vehicle_capacity",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+CAPACITY_STR),this.getCapacity(), CAPACITY_STR + WRONG);
			}
			
			
			if (this.getRegistered() != null) {
				String unixDate ="";
				String strDate ="";
				if(isADateTime(this.getRegistered())){
					unixDate = dateToUnixTime(this.getRegistered());
					strDate = this.getRegistered();
				} else {
					unixDate = this.getRegistered();
					DateFormat dfm = new SimpleDateFormat(standartTimeFormat);
					dfm.setTimeZone(utc3TimeZone);
					strDate =dfm.format(new Date(Long.parseLong(this.getRegistered() )));
				}
				assertEquals("DB:vehicle_registered",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_REGISTERED_DATE), unixDate,	REGISTERED_STR + WRONG);
				assertEquals(API_VEHICLE_REGISTERED,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+REGISTERED_STR),strDate, REGISTERED_STR + WRONG);
			}

			
			if (this.getDescription() != null) {
				assertEquals("DB:vehicle_description",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Description"), 					this.getDescription(),DESCRIPTION_STR + WRONG);
				assertEquals("API:vehicle_description",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+DESCRIPTION_STR),	this.getDescription(), DESCRIPTION_STR + WRONG);
			}
			
//			assertEquals("DB:vehicle_isActive", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.isActive"), 					 "true", ISACTIVE_STR + WRONG);

			
			if (this.isActive != null) {
			assertEquals("API:vehicle_isActive",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+ISACTIVE_STR), "true", ISACTIVE_STR + WRONG);
			}
			
			if (this.avatarUri != null) {
				assertContains("DB:vehicle_avatarUri",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.AvatarUri"), this.avatarUri,AVATARURI_STR + WRONG);
				assertContains(API_VEHICLE_AVATARURI,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+AVATARURI_STR),	this.avatarUri, AVATARURI_STR + WRONG);
				assertContains(API_VEHICLE_AVATARURI,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"vehicleViewCard.avatarUri"),	this.avatarUri, "vehicleViewCard.avatarUri" + WRONG);
				
			}

			if (this.getGroupId() != null) {
				assertEquals(DB_VEHICLE_GROUP,ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_GROUP_ID2), this.getGroupId(),	GROUP_STR + WRONG);
				assertEquals("API:vehicle_groupId",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+GROUPID_STR),	this.getGroupId(), GROUPID_STR + WRONG);
				assertEquals("API:vehicle_group",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_GROUP_ID),	this.getGroupId(), GROUP_STR + WRONG);

			}

			if (this.departmentId != null) {
				assertEquals("DB:vehicle_departmentId",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_DEPARTMENT_ID), this.departmentId,	DEPARTMENT_STR + WRONG);
				assertEquals(API_VEHICLE_DEPARTMENT,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+DEPARTMENTID_STR),	this.departmentId, DEPARTMENT_STR + WRONG);
			}
			if((this.getDeviceId() != null) && (this.getIsDeactivated() == null || this.getIsDeactivated() == FALSE)){ //if object contains info about device and it is not in deactivated state
				assertEquals("DB:vehicle_deviceId",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DeviceId.$oid"), this.getDeviceId(),	DEVICEID_STR + WRONG);
				assertEquals("API:vehicle_deviceId",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+DEVICEID_STR),	this.getDeviceId(), DEVICEID_STR + WRONG);
			} else {
				assertTrue("API:vehicle_device",!jsonApiRegistrationReply.containsKey("device"), "ожидалось что ТМУ не привязано к ТС");
				assertTrue("DB:vehicle_device",!jsonMongoReply.containsKey("DeviceId"), "ожидалось что ТМУ не привязано к ТС");
			}
			if (this.getEmployeeId() != null && (this.getIsDeactivated() == null || this.getIsDeactivated() == FALSE)) {
				assertEquals("DB:vehicle_employeeId",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.EmployeeId.$oid"), this.getEmployeeId(),	EMPLOYEEID_STR + WRONG);
				assertEquals("API:vehicle_employeeId",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+EMPLOYEEID_STR),	this.getEmployeeId(), EMPLOYEEID_STR + WRONG);

			}

			if (this.getFuel() != null && ! this.getFuel().keySet().isEmpty()) {
				assertEquals("DB:vehicle_fuel.source",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Fuel.Source"), 				this.getFuel().get(SOURCE),FUEL_SOURCE_STR + WRONG);
				assertEquals("DB:vehicle_fuel.type",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Fuel.Type"), 					this.getFuel().get("type"),FUEL_TYPE_STR + WRONG);
				assertEquals("DB:vehicle_fuel.fuelConsumption",ReplyToJSON.extractPathFromJson(jsonMongoReply,"$.Fuel.FuelConsumption"),this.getFuel().get(FUEL_CONSUPTION).toString(),FUEL_CONSUMPTION_STR + WRONG);
				assertEquals("DB:vehicle_fuel.tankCapacity",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Fuel.TankCapacity"), 	this.getFuel().get(TANK_CAPACITY)+".0",FUEL_TANKCAPACITY_STR + WRONG);
				
				
				assertEquals("API:vehicle_fuel.source",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+FUEL_SOURCE_STR), 					this.getFuel().get(SOURCE),FUEL_SOURCE_STR + WRONG);
				assertEquals("API:vehicle_fuel.type",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+FUEL_TYPE_STR), 						this.getFuel().get("type"),FUEL_TYPE_STR + WRONG);
				assertEquals("API:vehicle_fuel.fuelConsumption",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+FUEL_CONSUMPTION_STR),this.getFuel().get(FUEL_CONSUPTION).toString(),FUEL_CONSUMPTION_STR + WRONG);
				assertEquals("API:vehicle_fuel.tankCapacity",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+FUEL_TANKCAPACITY_STR), 		this.getFuel().get(TANK_CAPACITY),FUEL_TANKCAPACITY_STR + WRONG);

			}
		
		if (this.geoZoneId != null) {
			assertEquals("DB:vehicle_geoZoneIds",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.GeoZoneIds[0].$oid"), this.geoZoneId,	GEOZONEIDS_STR + WRONG);
			assertEquals("API:vehicle_geoZoneIds",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"geoZoneIds[0]"),	this.geoZoneId, GEOZONEIDS_STR + WRONG);
		}
		
		if (! this.getInsurancePolicyReq().isEmpty() &&  this.getInsurancePolicyReq().get(0) != null) {
			JSONObject insurancePolicyJson = (JSONObject) this.getInsurancePolicyReq().get(0);

			 JSONObject mongoInsurancePolicyInfo = (JSONObject) GetInsurancePolicyInfoFromMongo.getInsurancePolicyInfoByVehicleId(getVehicleId(),"").get(0);
			
			 assertContainsMngId("DB:vehicle_insurancePolicy.id",ReplyToJSON.extractPathFromJson(mongoInsurancePolicyInfo, PATH_ID), 								"insurancePolicy.id" + WRONG);
			assertEquals("DB:vehicle_insurancePolicy.type",ReplyToJSON.extractPathFromJson(mongoInsurancePolicyInfo, PATH_TYPE), 						insurancePolicyJson.get("type"),				"insurancePolicy.type" + WRONG);
			assertEquals("DB:vehicle_insurancePolicy.number", ReplyToJSON.extractPathFromJson(mongoInsurancePolicyInfo, PATH_NUMBER),					insurancePolicyJson.get(NUMBER_STR), 			"insurancePolicy.number" + WRONG);
			assertEquals("DB:vehicle_insurancePolicy.expires", 
						ReplyToJSON.extractPathFromJson(mongoInsurancePolicyInfo, "$.Expires.$date"),	dateTimeToUnixTime(insurancePolicyJson.get("expires").toString()),
						"insurancePolicy.expires" + WRONG);
			assertContainsMngId("DB:vehicle_insurancePolicy.documentUri",ReplyToJSON.extractPathFromJson(mongoInsurancePolicyInfo, "$.DocumentUri"), 	 										"insurancePolicy.documentUri" + WRONG);
			assertEquals("DB:vehicle_insurancePolicy.documentFileName",ReplyToJSON.extractPathFromJson(mongoInsurancePolicyInfo,"$.DocumentFileName"),insurancePolicyJson.get(DOCUMENT_FILENAME),"insurancePolicy.documentFileName" + WRONG);

			assertContainsMngId("API:vehicle_insurancePolicy.id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"insurancePolicy[0].id"), 						"insurancePolicy.id" + WRONG);
			assertEquals("API:vehicle_insurancePolicy.type",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"insurancePolicy[0].type"), 						insurancePolicyJson.get("type"),				"insurancePolicy.type" + WRONG);
			assertEquals("API:vehicle_insurancePolicy.number", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"insurancePolicy[0].number"),					insurancePolicyJson.get(NUMBER_STR), 			"insurancePolicy.number" + WRONG);
			assertEquals("API:vehicle_insurancePolicy.expires", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"insurancePolicy[0].expires"),				insurancePolicyJson.get("expires"), 			"insurancePolicy.expires" + WRONG);
			assertContainsMngId("API:vehicle_insurancePolicy.documentUri",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+"insurancePolicy[0].documentUri"), 	 										"insurancePolicy.documentUri" + WRONG);
			assertEquals("API:vehicle_insurancePolicy.documentFileName",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+"insurancePolicy[0].documentFileName"),insurancePolicyJson.get(DOCUMENT_FILENAME),"insurancePolicy.documentFileName" + WRONG);
		
		}
		if(! allVehList){
		//how to get here values to check?
		assertContains("API:vehicle_technicalState",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicle.technicalState"), "maintenances",				  "technicalState не содержит поля");
		assertContains("API:vehicle_technicalState.maintenances",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicle.technicalState.maintenances"), "[","technicalState.maintenances" + WRONG);
		}
		if (getMaintenancesReq() != null && ! getMaintenancesReq().isEmpty()){
			maintenances = ReplyToJSON.extractListPathFromJson(jsonApiRegistrationReply, "$."+pref+"technicalState.maintenances");
			String apiVehicleTechnicalStateMaintenances = "API:vehicle_technicalState.maintenances[";
			String technicalStateMaintenances= "technicalState.maintenances[";
			for (int i=0; i<getMaintenancesReq().size(); i++){
		 
				assertEquals(apiVehicleTechnicalStateMaintenances+i+DATE_STR,
						((JSONObject) maintenances.get(i)).get("date"), ((JSONObject)getMaintenancesReq().get(i)).get("date"),		  technicalStateMaintenances+i+DATE_STR + WRONG);
				
				assertEquals(apiVehicleTechnicalStateMaintenances+i+STATE_STR,
						((JSONObject) maintenances.get(i)).get(STATE), ((JSONObject)getMaintenancesReq().get(i)).get(STATE),		  technicalStateMaintenances+i+STATE_STR + WRONG);
				
				assertEquals(apiVehicleTechnicalStateMaintenances+i+MILEAGE_STR,
						((JSONObject) maintenances.get(i)).get(MILEAGE).toString(), ((JSONObject)getMaintenancesReq().get(i)).get(MILEAGE),		  technicalStateMaintenances+i+MILEAGE_STR + WRONG);
				
				assertEquals(apiVehicleTechnicalStateMaintenances+i+ORGANIZATION_STR,
						((JSONObject) maintenances.get(i)).get(ORGANIZATION), ((JSONObject)getMaintenancesReq().get(i)).get(ORGANIZATION),		  technicalStateMaintenances+i+ORGANIZATION_STR + WRONG);
				
				assertEquals(apiVehicleTechnicalStateMaintenances+i+PHONE_STR,
						((JSONObject) maintenances.get(i)).get(PHONE), ((JSONObject)getMaintenancesReq().get(i)).get(PHONE),		  technicalStateMaintenances+i+PHONE_STR + WRONG);
				
				assertEquals(apiVehicleTechnicalStateMaintenances+i+ADDRESS_STR,
						((JSONObject) maintenances.get(i)).get(ADDRESS), ((JSONObject)getMaintenancesReq().get(i)).get(ADDRESS),		  technicalStateMaintenances+i+ADDRESS_STR + WRONG);
				
				assertEquals(apiVehicleTechnicalStateMaintenances+i+COMMENT_STR,
						((JSONObject) maintenances.get(i)).get(COMMENT), ((JSONObject)getMaintenancesReq().get(i)).get(COMMENT),		  technicalStateMaintenances+i+COMMENT_STR + WRONG);
				
				assertEquals(apiVehicleTechnicalStateMaintenances+i+COST_STR,
						((JSONObject) maintenances.get(i)).get("cost").toString(), ((JSONObject)getMaintenancesReq().get(i)).get("cost"),		  technicalStateMaintenances+i+COST_STR + WRONG);

		        	
			        	
			}
		
		}
		if(! allVehList){
			assertContains("API:vehicle_technicalState.repairs",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+TECHNICAL_STATE_REPAIRS_STR), "[",		  TECHNICAL_STATE_REPAIRS_STR + WRONG);
		}
		if (getRepairsReq() != null && ! getRepairsReq().isEmpty()){
			String vehicleTechnicalStateRepairs = "API:vehicle_technicalState.repairs[";
			repairs = ReplyToJSON.extractListPathFromJson(jsonApiRegistrationReply, "$."+pref+TECHNICAL_STATE_REPAIRS_STR);
			String technicalStateRepairs = "technicalState.repairs[";
			for (int i=0; i<getRepairsReq().size(); i++){
				assertContainsMngId("API:vehicle_repairs.id",((JSONObject) repairs.get(i)).get("id").toString(), 								"repairs.id" + WRONG);
				assertEquals(vehicleTechnicalStateRepairs+i+NAME_STR,
						((JSONObject) repairs.get(i)).get("name").toString(), ((JSONObject)getRepairsReq().get(i)).get("name"),		  technicalStateRepairs+i+NAME_STR + WRONG);
				assertEquals(vehicleTechnicalStateRepairs+i+DATE_STR,
						((JSONObject) repairs.get(i)).get("date"), ((JSONObject)getRepairsReq().get(i)).get("date"),		  technicalStateRepairs+i+DATE_STR + WRONG);
				
				assertEquals(vehicleTechnicalStateRepairs+i+STATE_STR,
						((JSONObject) repairs.get(i)).get(STATE), ((JSONObject)getRepairsReq().get(i)).get(STATE),		  technicalStateRepairs+i+STATE_STR + WRONG);
				
				assertEquals(vehicleTechnicalStateRepairs+i+MILEAGE_STR,
						((JSONObject) repairs.get(i)).get(MILEAGE).toString(), ((JSONObject)getRepairsReq().get(i)).get(MILEAGE),		  technicalStateRepairs+i+MILEAGE_STR + WRONG);
				
				assertEquals(vehicleTechnicalStateRepairs+i+ORGANIZATION_STR,
						((JSONObject) repairs.get(i)).get(ORGANIZATION), ((JSONObject)getRepairsReq().get(i)).get(ORGANIZATION),		  technicalStateRepairs+i+ORGANIZATION_STR + WRONG);
				
				assertEquals(vehicleTechnicalStateRepairs+i+PHONE_STR,
						((JSONObject) repairs.get(i)).get(PHONE), ((JSONObject)getRepairsReq().get(i)).get(PHONE),		  technicalStateRepairs+i+PHONE_STR + WRONG);
				
				assertEquals(vehicleTechnicalStateRepairs+i+ADDRESS_STR,
						((JSONObject) repairs.get(i)).get(ADDRESS), ((JSONObject)getRepairsReq().get(i)).get(ADDRESS),		  technicalStateRepairs+i+ADDRESS_STR + WRONG);
				
				assertEquals(vehicleTechnicalStateRepairs+i+COMMENT_STR,
						((JSONObject) repairs.get(i)).get(COMMENT), ((JSONObject)getRepairsReq().get(i)).get(COMMENT),		  technicalStateRepairs+i+COMMENT_STR + WRONG);
				
				assertEquals(vehicleTechnicalStateRepairs+i+COST_STR,
						((JSONObject) repairs.get(i)).get("cost").toString(), ((JSONObject)getRepairsReq().get(i)).get("cost"),		  technicalStateRepairs+i+COST_STR + WRONG);
			}
		}
		if(! allVehList){
		assertContains("API:vehicle_technicalState.accidents",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$."+pref+TECHNICAL_STATE_ACCIDENTS_STR), "[",	  TECHNICAL_STATE_ACCIDENTS_STR + WRONG);
		}
		if (getAccidentsReq() != null && ! getAccidentsReq().isEmpty()){
			String apiVehicleTechnicalStateAccidents = "API:vehicle_technicalState.accidents[";
			String technicalStateAccidents = "technicalState.accidents[";
			accidents = ReplyToJSON.extractListPathFromJson(jsonApiRegistrationReply, "$."+pref+TECHNICAL_STATE_ACCIDENTS_STR);
			for (int i=0; i<getAccidentsReq().size(); i++){
				assertContainsMngId("API:vehicle_accidents.id",((JSONObject) accidents.get(i)).get("id").toString(), 								"accidents.id" + WRONG);
				assertEquals(apiVehicleTechnicalStateAccidents+i+NAME_STR,
						((JSONObject) accidents.get(i)).get("name").toString(), ((JSONObject)getAccidentsReq().get(i)).get("name"),		  technicalStateAccidents+i+NAME_STR + WRONG);
				assertEquals(apiVehicleTechnicalStateAccidents+i+DATE_STR,
						((JSONObject) accidents.get(i)).get("date"), ((JSONObject)getAccidentsReq().get(i)).get("date"),		  technicalStateAccidents+i+DATE_STR + WRONG);

				assertEquals(apiVehicleTechnicalStateAccidents+i+"].document",
						((JSONObject) accidents.get(i)).get("document"), ((JSONObject)getAccidentsReq().get(i)).get("document"),		  technicalStateAccidents+i+"].document" + WRONG);
				if(((JSONObject) accidents.get(i)).containsKey("documentUri")){
					assertContainsMngId(apiVehicleTechnicalStateAccidents+i+"].documentUri",((JSONObject) accidents.get(i)).get("documentUri").toString(),  technicalStateAccidents+i+"].documentUri" + WRONG);
				}
				assertEquals(apiVehicleTechnicalStateAccidents+i+"].documentFileName",
						((JSONObject) accidents.get(i)).get(DOCUMENT_FILENAME).toString(), ((JSONObject)getAccidentsReq().get(i)).get(DOCUMENT_FILENAME),
						technicalStateAccidents+i+"].documentFileName" + WRONG);
				
				assertEquals(apiVehicleTechnicalStateAccidents+i+"].damage",
						((JSONObject) accidents.get(i)).get("damage"), ((JSONObject)getAccidentsReq().get(i)).get("damage"),		  technicalStateAccidents+i+"].damage" + WRONG);
				
				assertEquals(apiVehicleTechnicalStateAccidents+i+PHONE_STR,
						((JSONObject) accidents.get(i)).get(PHONE), ((JSONObject)getAccidentsReq().get(i)).get(PHONE),		  technicalStateAccidents+i+PHONE_STR + WRONG);
				
				assertEquals(apiVehicleTechnicalStateAccidents+i+ADDRESS_STR,
						((JSONObject) accidents.get(i)).get(ADDRESS), ((JSONObject)getAccidentsReq().get(i)).get(ADDRESS),		  technicalStateAccidents+i+ADDRESS_STR + WRONG);
				
				assertEquals(apiVehicleTechnicalStateAccidents+i+COMMENT_STR,
						((JSONObject) accidents.get(i)).get(COMMENT), ((JSONObject)getAccidentsReq().get(i)).get(COMMENT),		  technicalStateAccidents+i+COMMENT_STR + WRONG);
/*				:   :   :   :   :   :   "employee":
				:   :   :   :   :   :   {
				:   :   :   :   :   :   :   "id":"59412e2a5532f31a9cc3cc58",
				:   :   :   :   :   :   :   "name":"Ivan Ivanov",
				:   :   :   :   :   :   :   ISACTIVE_STR:false,
				:   :   :   :   :   :   :   AVATARURI:"system/profile-1.png",
				:   :   :   :   :   :   :   GROUPID_STR:ZERO_ID
				:   :   :   :   :   :   }
				:   :   :   :   :   }	*/
				String accidEmplId="";
				String accidEmplName="";
				String accidEmplAvatarUri="";
				String accidEmplIsActive=null;
				String accidEmplGroupId="";
				
				JSONObject reqAccidEmpl;
				if(initMap!= null && initMap.containsKey("accidents_employee")){
				 reqAccidEmpl = (JSONObject) initMap.get("accidents_employee");
				 accidEmplId=ReplyToJSON.extractPathFromJson(reqAccidEmpl, PATH_ID);
				 accidEmplName=reqAccidEmpl.get("Firstname")+ " "+ reqAccidEmpl.get("Lastname");
				 accidEmplAvatarUri=reqAccidEmpl.get("AvatarUri").toString();
				 accidEmplIsActive=String.valueOf(!((Boolean) reqAccidEmpl.get("IsDeactivated")));
					 if(reqAccidEmpl.containsKey("GroupId")){	 
						 accidEmplGroupId=ReplyToJSON.extractPathFromJson( reqAccidEmpl,PATH_GROUP_ID2);
					 } else {
						 accidEmplGroupId =ZERO_ID;
					 }

				} else { //if we don't contain it in init map we have to get it from DB,no sense to compare object to itself, accident employee we receive during vehicle initialization.
					 reqAccidEmpl = (JSONObject) this.accidents.get(0);
					 reqAccidEmpl = (JSONObject) reqAccidEmpl.get("employee");
					 accidEmplId=ReplyToJSON.extractPathFromJson(reqAccidEmpl, "$.id");
					 accidEmplName=reqAccidEmpl.get("name").toString();
					 accidEmplAvatarUri=reqAccidEmpl.get(AVATARURI_STR).toString();
					 accidEmplIsActive= reqAccidEmpl.get(ISACTIVE_STR).toString();
					 accidEmplGroupId=ReplyToJSON.extractPathFromJson( reqAccidEmpl,"$.groupId");
				}

				assertEquals(apiVehicleTechnicalStateAccidents+i+"].employee.id",
						ReplyToJSON.extractPathFromJson( (JSONObject)accidents.get(i),"$.employee.id"), accidEmplId,
						technicalStateAccidents+i+"].employee.id" + WRONG);
				assertEquals(apiVehicleTechnicalStateAccidents+i+"].employee.name",
						ReplyToJSON.extractPathFromJson( (JSONObject)accidents.get(i),"$.employee.name"), accidEmplName,
						technicalStateAccidents+i+"].employee.name" + WRONG);
				assertEquals(apiVehicleTechnicalStateAccidents+i+"].employee.avatarUri",
						ReplyToJSON.extractPathFromJson( (JSONObject)accidents.get(i),"$.employee.avatarUri"), accidEmplAvatarUri,
						technicalStateAccidents+i+"].employee.avatarUri" + WRONG);

				assertEquals(apiVehicleTechnicalStateAccidents+i+"].employee.isActive",
						 ReplyToJSON.extractPathFromJson( (JSONObject)accidents.get(i),"$.employee.isActive"), accidEmplIsActive,
						 technicalStateAccidents+i+"].employee.isActive" + WRONG);
//				if (reqAccidEmpl.containsKey("GroupId")){
					assertEquals(apiVehicleTechnicalStateAccidents+i+"].employee.groupId",
									ReplyToJSON.extractPathFromJson( (JSONObject)accidents.get(i),"$.employee.groupId"),  accidEmplGroupId,
									technicalStateAccidents+i+"].employee.groupId" + WRONG);
/*				} else {
					assertEquals(api_vehicle_technicalState_accidents+i+"].employee.groupId",
								ReplyToJSON.extractPathFromJson( (JSONObject)accidents.get(i),"$.employee.groupId"), ZERO_ID,
							technicalState_accidents+i+"].employee.groupId" + WRONG);
				}
	*/			
			}
			
		}
		
		if(this.getModel() != null){
		assertEquals("API:vehicle_vehicleViewCard.secondaryTitle",
				ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+VEHICLE_VIEWCARD_SECONDARYTITLE_STR),this.getMark()+pipe+this.getModel()+pipe+this.getNumber(),VEHICLE_VIEWCARD_SECONDARYTITLE_STR + WRONG);
		} else{
			assertEquals("API:vehicle_vehicleViewCard.secondaryTitle",
					ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+VEHICLE_VIEWCARD_SECONDARYTITLE_STR),this.getMark()+pipe +this.getNumber(),VEHICLE_VIEWCARD_SECONDARYTITLE_STR + WRONG);
			}
		String vehicleViewCardStatusValue = "vehicleViewCard.status.value";
		assertNotNull("API:vehicle_vehicleViewCard.status.statusValue",
				ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+"vehicleViewCard.status.statusValue"), "vehicleViewCard.status.statusValue" + WRONG);
		assertNotNull("API:vehicle_vehicleViewCard.status.value",
				ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+vehicleViewCardStatusValue),	vehicleViewCardStatusValue + WRONG);

		if(!ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+vehicleViewCardStatusValue).equals("WithoutDevice")){
			assertNotNull("API:vehicle_vehicleViewCard.status.statusName",
					ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+"vehicleViewCard.status.statusName"),	 "vehicleViewCard.status.statusName" + WRONG);
			if((! allVehList)
				&& ( ! ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+vehicleViewCardStatusValue).equals("Offline") 
						&& getIfExists(jsonApiRegistrationReply,"$.device.type")!=null 
						&& ! ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.device.type").equals("VirtualTracker") )  ){
				assertNotNull("API:vehicle_vehicleViewCard.status.SettingDate",
						ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+"vehicleViewCard.status.settingDate"),	 "vehicleViewCard.status.SettingDate" + WRONG);
				if(! (ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+vehicleViewCardStatusValue).equals("Deactivated"))) {
					assertNotNull("API:vehicle_vehicleViewCard.status.Address",
							ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+"vehicleViewCard.status.address"),	 "vehicleViewCard.status.Address" + WRONG);
					assertNotNull("API:vehicle_vehicleViewCard.status.MovingStateTitle",
							ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$."+pref+"vehicleViewCard.status.movingStateTitle"),	 "vehicleViewCard.status.MovingStateTitle" + WRONG);
				}
				
			}
		}
		
		//TODO: add here asserts for  SettingDate, Address
		
		if(this.departmentId != null){
		assertEquals("API:department.id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.department.id"),this.departmentId,"department.id" + WRONG);
		assertEquals("API:department.name",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.department.name"),this.department.get("Name"),"department.Name" + WRONG);
		}
		if(this.getGroupId() != null){
		assertEquals("API:group.id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,PATH_GROUP_ID),this.getGroupId(),"group.id" + WRONG);
		String objGroupName="";
		if(!this.getGroup().isEmpty()){
		if(this.getGroup().containsKey("Name"))
			objGroupName= this.getGroup().get("Name").toString();
		else 
			objGroupName= this.getGroup().get("name").toString();
		assertEquals("API:group.name",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.group.name"),objGroupName,"group.name" + WRONG);
		}
		}
		if(this.geoZoneId != null){
		assertEquals("API:geozones.id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.geozones[0].id"),this.geoZoneId,"geozones.id" + WRONG);
		assertEquals("API:geozones.name",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.geozones[0].name"),this.geoZone.get("Name"),"geozones.name" + WRONG);
		}
		if((this.getDeviceId() != null) && (this.getIsDeactivated() == null || this.getIsDeactivated() == FALSE)){ //if object contains info about device and it is not in deactivated state
			if (jsonApiRegistrationReply.keySet().contains("device")){
		assertEquals("API:device.id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.device.id"), this.getTelematicsDevice().getTrackerId(),			"device.id" + WRONG);
		assertEquals("API:device.code",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.device.code"),			this.getTelematicsDevice().getTrackerCode(),			"device.code" + WRONG);
		assertEquals("API:device.isActive",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.device.isActive"), 	"true",		"device.isActive" + WRONG);
			//where to get signal level?
			if(this.getTelematicsDevice().getSignalLevel() != null){
			assertEquals("API:device.signalQuality",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.device.signalQuality"),
										 this.getTelematicsDevice().getSignalLevel(),"device.signalQuality" + WRONG);
			}
			} else {
				assertEquals("API:device.id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.deviceId"), this.getDeviceId(),			"device.id" + WRONG);
			}
		}
			if (this.employeeObj != null && this.employeeObj.getEmployeeId() != null){
			assertEquals("API:employees.id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].id"),this.employeeObj.getEmployeeId(),"employees.id" + WRONG);
			assertEquals("API:employees.name",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].name"),
											  this.employeeObj.getFirstname()+" "+this.employeeObj.getLastname(),										 "employees.name" + WRONG);
			assertEquals("API:employees.isActive",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].isActive"),"true","employees.isActive" + WRONG);
			assertEquals("API:employees.phone",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].phone"),this.employeeObj.getPhone(),"employees.phone" + WRONG);
			assertEquals("API:employees.email",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].email"),this.employeeObj.getEmail(),"employees.email" + WRONG);
			assertContains("API:employees.avatarUri",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].avatarUri"),this.employeeObj.getAvatarUri(),"employees.avatarUri" + WRONG);
			String apiEmployeesGroupId = "API:employees.groupId";
			if(this.employeeObj.getGroupId()!=null){
				assertEquals(apiEmployeesGroupId,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].groupId"),this.employeeObj.getGroupId(),EMPLOYEES_GROUP_STR + WRONG);
				assertEquals(apiEmployeesGroupId,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].group.id"),this.employeeObj.getGroupId(),EMPLOYEES_GROUP_STR + WRONG);
			} else{
				assertEquals(apiEmployeesGroupId,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].groupId"), ZERO_ID,EMPLOYEES_GROUP_STR + WRONG);
				assertEquals(apiEmployeesGroupId,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply,"$.employees[0].group.id"),ZERO_ID,EMPLOYEES_GROUP_STR + WRONG);
				
			}
		}
		assertEquals("DB:vehicle_CustomerId",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.CustomerId.$oid"), getProperty(TEST_CUTOMER_ID), "customer" + WRONG);
		//Not implemented yet?
//		assertEquals("API:vehicle_CustomerId",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.customerId"), getProperty(TEST_CUTOMER_ID),"customer" + WRONG);
		
/* 		/////////////////////// additional fields to check ?:
    "employees": [{
			DEPARTMENT_STR: {
			},
			"geoZones": []
		}]   */
			
			
			setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные новогого ТС:" + e.toString());

		}
	}
	

	@SuppressWarnings("unchecked")	
	public void setMinimalFields(JSONObject initMap){
		this.setName(initMap.get("name").toString());
		this.setType(initMap.get("vehicle_type").toString());
		this.setNumber(initMap.get("vehicle_number").toString());
		this.setMark(initMap.get("mark").toString());
		this.avatarUri=initMap.get("vehicle_avatarUri").toString();
		this.setGroupId(null);
		this.setIsDeactivated(FALSE);
		this.isActive= "true";
		this.urlParameters.clear();
		
		this.urlParameters.put("name", this.getName());
		this.urlParameters.put("type", this.getType());
		this.urlParameters.put("Number",  this.getNumber());
		this.urlParameters.put("mark",  this.getMark());

		this.urlParameters.put(AVATARURI_STR,  this.avatarUri);
		
		currentDateTime = System.currentTimeMillis();
	}
	@SuppressWarnings("unchecked")
	public void setAllVehicleFields(JSONObject initMap){


		this.setNumber(initMap.get("vehicle_number").toString());
		this.setVinCode(initMap.get(VINCODE_STR).toString());
		this.color=initMap.get(COLOR_STR).toString();
		this.setGarageNumber(initMap.get(GARAGE_NUMBER).toString());
		this.setMaxSpeed(initMap.get(MAXSPEED_STR).toString());
		this.setCapacity(initMap.get(CAPACITY_STR).toString());
		this.setRegistered(initMap.get("vehicle_registered").toString());
		this.avatarUri=initMap.get("vehicle_custom").toString();
		this.setDescription(initMap.get(DESCRIPTION_STR).toString());
		this.setModel(initMap.get(MODEL_STR).toString());
		this.setCapacity(initMap.get(CAPACITY_STR).toString());
		
		this.setFuel((JSONObject) initMap.get("fuel"));
		this.setInsurancePolicyReq((ArrayList) initMap.get("insurancePolicy"));
		
		this.urlParameters.put(NUMBER_STR, this.getNumber());
		this.urlParameters.put(MODEL_STR, this.getModel());
		this.urlParameters.put(VINCODE_STR,  this.getVinCode());
		this.urlParameters.put(CAPACITY_STR, this.getCapacity());
		this.urlParameters.put(COLOR_STR, this.color);
		this.urlParameters.put(GARAGE_NUMBER, this.getGarageNumber());
		this.urlParameters.put(MAXSPEED_STR, this.getMaxSpeed());
		this.urlParameters.put(CAPACITY_STR, this.getCapacity());
		this.urlParameters.put(REGISTERED_STR, this.getRegistered());
		this.urlParameters.put(AVATARURI_STR, this.avatarUri);
		this.urlParameters.put(DESCRIPTION_STR, this.getDescription());
		
		this.urlParameters.put("fuel", this.getFuel());
		this.urlParameters.put("insurancePolicy", this.getInsurancePolicyReq());
		
	}

	@SuppressWarnings("unchecked")
	@Step("Изменение ТС")
	public List<org.json.simple.JSONObject> updateVehicle() {
		List<org.json.simple.JSONObject> mongoVehicleInfo = null;
		String url = getProperty("host") + "/Vehicle/Update";
		
		assertTrue(this.getVehicleId()!=null, "необходимо указать id изменяемого ТС");
		urlParameters.put("id", this.getVehicleId());
		
		try {
			token = getCurAuthAccToken(authorizedAccountT1client);
			assertTrue(TOKEN_RECEIVED,token != null,UPDATE_AUTH_NEEDED);
			org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,true,true);
			
			setVehicleId(ReplyToJSON.extractPathFromJson(registrationReply, PATH_VEHICLE_ID));
			vehUpdated=ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.updated");
			// get account info from mongo
			mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(getVehicleId(),"");
			getInsAndTechnicalSatateIds(registrationReply);
//			// compare mongo with input
			checkRegistredVehicle(mongoVehicleInfo.get(0), registrationReply,false, Vehicle.getInitJson());
		} catch (Exception e) {
			log.error(TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return mongoVehicleInfo;
	}
	@SuppressWarnings("unchecked")
	@Step("Изменение ТС")
	public List<org.json.simple.JSONObject> updateVehicle(HashMap<String,String> filesMap) {
		List<org.json.simple.JSONObject> mongoVehicleInfo = null;
		String url = getProperty("host") + "/Vehicle/Update";
		
		assertTrue(this.getVehicleId()!=null, "необходимо указать id изменяемого ТС");
		urlParameters.put("id", this.getVehicleId());
		try {
			token = getCurAuthAccToken(authorizedAccountT1client);
			assertTrue(TOKEN_RECEIVED,token != null,UPDATE_AUTH_NEEDED);
			org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,MULTIPART,filesMap,true).get(0);
			
			setVehicleId(ReplyToJSON.extractPathFromJson(registrationReply, PATH_VEHICLE_ID));
			vehUpdated=ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.updated");
			getInsAndTechnicalSatateIds(registrationReply);
			// get account info from mongo
			mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(getVehicleId(),"");
//			// compare mongo with input
			checkRegistredVehicle(mongoVehicleInfo.get(0), registrationReply,false, Vehicle.getInitJson());
		} catch (Exception e) {
			log.error(TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return mongoVehicleInfo;
	}
	@Step("Изменение ТС в панели администратора")
	public List<JSONObject> updateVehiclesInAdminPanelResources() {
		HashMap<String,String> filesMap = new HashMap<>();
		List<org.json.simple.JSONObject> mongoVehicleInfo = null;
		String url = getProperty(ADM_PANEL_HOST) + "/Api/Admin/Vehicles/"+this.getVehicleId();
		urlParameters.put("id", this.getVehicleId());
		try {
			token = getCurAuthAccToken(AUTH_ADMPANEL);
			assertTrue(TOKEN_RECEIVED,token != null,UPDATE_AUTH_NEEDED);
			org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPutRequest( url, jsonParamsToString(urlParameters), token,MULTIPART,filesMap,true).get(0);
			
			setVehicleId(ReplyToJSON.extractPathFromJson(registrationReply, "$.id"));
//			veh_updated=ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.updated");
//			getInsAndTechnicalSatateIds(registrationReply);
			// get account info from mongo
			mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(getVehicleId(),"");
//			// compare mongo with input
			checkRegistredVehicleAdminReply(mongoVehicleInfo.get(0), registrationReply,false, Vehicle.getInitJson());
		} catch (Exception e) {
			log.error(TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return mongoVehicleInfo;
	}

	public static void getInsAndTechnicalSatateIds(JSONObject  registrationReply) throws Exception {
		getInsuranceIdsList().clear();
		getMaintenancesIdsList().clear();
		getRepairsIdsList().clear();
		getAccidentsIdsList().clear();
		if(! ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.insurancePolicy").equals("[]") ){
			setInsuranceIdsList(ReplyToJSON.extractListPathFromJson(registrationReply, "$.vehicle.insurancePolicy..id"));
		}
		if(! ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.technicalState.maintenances").equals("[]") ){
			setMaintenancesIdsList(ReplyToJSON.extractListPathFromJson(registrationReply, "$.vehicle.technicalState.maintenances..id"));
		//insurancePolicyId= ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.insurancePolicy[0].id");
		}
		if(! ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.technicalState.repairs").equals("[]") ){
			setRepairsIdsList(ReplyToJSON.extractListPathFromJson(registrationReply, "$.vehicle.technicalState.repairs..id"));
		}
		if(! ReplyToJSON.extractPathFromJson(registrationReply, "$.vehicle.technicalState.accidents").equals("[]") ){
			setAccidentsIdsList(ReplyToJSON.extractListPathFromJson(registrationReply, "$.vehicle.technicalState.accidents..id"));
		}
		
	}

	public JSONObject deleteVehicle() {
		String url = getProperty("host");
		String postParameters = "";
		org.json.simple.JSONObject requestReply = null;
		try {
			url += "/Vehicle/delete/" + this.getVehicleId();
			token = getCurAuthAccToken(authorizedAccountT1client);
			assertTrue(TOKEN_RECEIVED,token != null, DELETE_AUTH_NEEDED);
			requestReply = SendRequestToAPI.sendPostRequest(url, postParameters, token, true, true);
			checkVehicleDeleted(this.getVehicleId());
		} catch (Exception e) {
			log.error(TEST_FAILED + e);
			assertTrue(false, e.toString());
		}
		return requestReply;

	}
	
	@Step("Удаление ТС {0}")
	public static org.json.simple.JSONObject deleteVehicle(String vehicleId) {
		org.json.simple.JSONObject registrationReply=null;
		String url = getProperty("host") + "/Vehicle/delete/"+vehicleId;
		try {
			String	token = getCurAuthAccToken(authorizedAccountT1client);
			assertTrue(TOKEN_RECEIVED,token != null,DELETE_AUTH_NEEDED);
			
			 registrationReply = SendRequestToAPI.sendPostRequest( url, "", token,true,true);
		} catch (Exception e) {
		log.warn("wasn't able to delete vehicle, remove it from containers anyway!");
		SharedContainer.removeFromContainer(VEHICLES,vehicleId);
		TearDownExecutor.removeEntityFromTeardown(vehicleId,VEHICLES);
		log.error(TEST_FAILED + e);
		assertTrue(false, e.toString());
		
	}
	return registrationReply;
}
	
	public JSONObject deleteVehicleInAdminPanel() {
		// /Api/Admin/Vehicle/
		org.json.simple.JSONObject registrationReply=null;
		String url = getProperty(ADM_PANEL_HOST) + "/Api/Admin/Vehicles/"+this.getVehicleId();
		try {
			token = getCurAuthAccToken(AUTH_ADMPANEL);
			assertTrue(TOKEN_RECEIVED,token != null,DELETE_AUTH_NEEDED);
			
			 registrationReply = SendRequestToAPI.sendDeleteRequest( url, token,true);
			 checkVehicleDeleted(this.getVehicleId());
/*TODO: add validation vehicle deleted
				#Vehicles : запись отсутствует
				
				#Devices : запись обновлена, отсутсвуют значения:
				#           - "ResourceId"
				#           - "ResourceType" : "Vehicle"
				
				#DeviceHistory : запись обновлена, в записи присутствует:
				#               - "DeviceId"
				#               - "ResourceId"
				#               - "BoundToResourceTime" - время выполнения строки 5
				#               - "UnboundFromResourceTime" - время выполнения строки 6
				#               - "RelatedResourceType" :  "Vehicle"  
*/
		} catch (Exception e) {
		log.warn("wasn't able to delete vehicle, remove it from containers anyway!");
		SharedContainer.removeFromContainer(VEHICLES,getVehicleId());
		TearDownExecutor.removeEntityFromTeardown(getVehicleId(),VEHICLES);
		SharedContainer.setContainer("deletedVehiclesIds", getVehicleId());
		log.error(TEST_FAILED + e);
		assertTrue(false, e.toString());
		
	}
	return registrationReply;
	}
	
	@Step("получение списка всех ТС")
	public static List<org.json.simple.JSONObject> getAllVehicles1AndLastPages(String token) {

		try {
			String url = getProperty("host") + "/Vehicle/GetAll/?pageNumber=1";
			org.json.simple.JSONObject vehiclesReplied = (JSONObject) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token).get(0);
			List<JSONObject> vehicleListApi = (List<JSONObject>) vehiclesReplied.get("items");
//			"pageInfo":{"pageNumber":1,"pageSize":25,"totalPage":25,"totalItem":615}
			int apiPageSize = Integer.parseInt(ReplyToJSON.extractPathFromJson(vehiclesReplied, "$.pageInfo.pageSize"));
			int apiTotalPage = Integer.parseInt(ReplyToJSON.extractPathFromJson(vehiclesReplied, "$.pageInfo.totalPage"));
			int apiTotalItem = Integer.parseInt(ReplyToJSON.extractPathFromJson(vehiclesReplied, "$.pageInfo.totalItem"));
			
			
			Bson filter = Filters.eq("CustomerId",new ObjectId(getProperty(TEST_CUTOMER_ID)));
			Long totalVehicles = GetInfoFromMongo.countFilteredByCustomFilters("Vehicles",filter);
			long pages = (totalVehicles/apiPageSize);
			if(totalVehicles%apiPageSize > 0){
				pages+=1;
			}
			
			assertEquals("total items",Long.valueOf(apiTotalItem), totalVehicles,"получено некорректное количество страниц");
			assertEquals("total pages",apiTotalPage, pages,"получено некорректное количество страниц");
			
			url = getProperty("host") + "/Vehicle/GetAll/?pageNumber="+apiTotalPage;
			org.json.simple.JSONObject vehiclesRepliedLastPage = (JSONObject) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token).get(0);
			vehicleListApi.addAll( (List<JSONObject>) vehiclesRepliedLastPage.get("items"));
			checkVehicleList (vehicleListApi);
			return vehicleListApi;

		} catch (Exception e) {
			log.error(ERROR,e);
		}
		return null;

	}
	
private static void checkVehicleList(List<JSONObject> vehiclesReplied) {
	
	try {
		JSONArray vehiclesRepliedJsonArr = new JSONArray(vehiclesReplied);
	List<String> listOfIds = ReplyToJSON.extractListPathFromJson(vehiclesRepliedJsonArr, "$.[*].id");
	
	//List<JSONObject> vehicleListApi = (List<JSONObject>) vehiclesReplied.get("items");
	List<JSONObject> foundInMongoVehicles = GetInfoFromMongo.getInfoByListOfIds("Vehicles",listOfIds,getProperty(TEST_CUTOMER_ID),"");
	JSONObject mongoVehicle = new JSONObject();
	for (int v=0; v<vehiclesReplied.size();v++){
		log.debug("checking vehicle "+v);
		for (int j=0;j<foundInMongoVehicles.size();j++){
			if (ReplyToJSON.extractPathFromJson(foundInMongoVehicles.get(j), PATH_ID).equals(vehiclesReplied.get(v).get("id"))){
				mongoVehicle = foundInMongoVehicles.get(j);
				break;
			}
		}
	//	mongoVehicle = foundInMongoVehicles.get
			assertEquals("compare ids",vehiclesReplied.get(v).get("id"), ReplyToJSON.extractPathFromJson(mongoVehicle, PATH_ID),"wrong vehicle received from DB");
			
			Vehicle tmpvehicleObj = new Vehicle(false);
			tmpvehicleObj.setName(getIfExists(mongoVehicle,"Name"));
			tmpvehicleObj.setModel(getIfExists(mongoVehicle,"Model"));
			tmpvehicleObj.setMark(getIfExists(mongoVehicle,"Mark"));
			tmpvehicleObj.setNumber(getIfExists(mongoVehicle,"Number"));
			tmpvehicleObj.setVinCode(getIfExists(mongoVehicle,"VinCode"));
			tmpvehicleObj.setType(getIfExists(mongoVehicle,"Type"));
			tmpvehicleObj.color=	getIfExists(mongoVehicle,"Color");
			tmpvehicleObj.setIsDeactivated(getIfExists(mongoVehicle,"IsDeactivated"));
			tmpvehicleObj.isActive=null;
			tmpvehicleObj.setGarageNumber(getIfExists(mongoVehicle,"GarageNumber"));
			tmpvehicleObj.setMaxSpeed(getIfExists(mongoVehicle,"MaxSpeed"));
			tmpvehicleObj.setCapacity(getIfExists(mongoVehicle,"Capacity"));
			tmpvehicleObj.setRegistered(getIfExists(mongoVehicle,"Registered.$date"));
			tmpvehicleObj.setDescription(getIfExists(mongoVehicle,"Description"));
			tmpvehicleObj.avatarUri=getIfExists(mongoVehicle,"AvatarUri");
			tmpvehicleObj.setGroupId(null);
			tmpvehicleObj.departmentId=getIfExists(mongoVehicle,"DepartmentId.$oid");
			tmpvehicleObj.setDeviceId(getIfExists(mongoVehicle,"DeviceId.$oid"));
			tmpvehicleObj.setEmployeeId(getIfExists(mongoVehicle,"$.EmployeeId.$oid"));

			JSONObject fuel = (JSONObject) mongoVehicle.get("Fuel");
			if (fuel != null && ! fuel.keySet().isEmpty()){		
			tmpvehicleObj.getFuel().put("type", fuel.get("Type"));
			tmpvehicleObj.getFuel().put(SOURCE, fuel.get("Source"));
			tmpvehicleObj.getFuel().put(FUEL_CONSUPTION, fuel.get("FuelConsumption"));
			String tankCapacity =fuel.get("TankCapacity").toString();
			tmpvehicleObj.getFuel().put(TANK_CAPACITY, tankCapacity.substring(0, tankCapacity.lastIndexOf('.')));
			} else {
				tmpvehicleObj.setFuel(null);
			}
			tmpvehicleObj.geoZoneId = null;
			//compairing to DB here mostly has no sense but this method already have almmost all needed asserts
			tmpvehicleObj.checkRegistredVehicle(mongoVehicle, vehiclesReplied.get(v), true, null);
			if(getIfExists(mongoVehicle,PATH_REGISTERED_DATE)!=null)
				assertEquals(API_VEHICLE_REGISTERED,dateTimeToUnixTime(ReplyToJSON.extractPathFromJson(vehiclesReplied.get(v), "$.registered")),
													getIfExists(mongoVehicle,PATH_REGISTERED_DATE), REGISTERED_STR + WRONG);
			assertEquals(API_VEHICLE_DEPARTMENT,getIfExists(vehiclesReplied.get(v), "$.departmentId"), getIfExists(mongoVehicle,PATH_DEPARTMENT_ID), DEPARTMENTID_STR + WRONG);
			assertEquals("API:vehicle_deviceId",getIfExists(vehiclesReplied.get(v), "$.deviceId"), getIfExists(mongoVehicle,"$.DeviceId.$oid"), DEVICEID_STR + WRONG);
			//String groupId = (getIfExists(mongoVehicle,PATH_GROUP_ID2)!=null)? getIfExists(mongoVehicle,PATH_GROUP_ID2): ZERO_ID;
			assertEquals("API:vehicle_groupId",getIfExists(vehiclesReplied.get(v), PATH_GROUP_ID), getIfExists(mongoVehicle,PATH_GROUP_ID2), GROUPID_STR + WRONG);
			if(getIfExists(mongoVehicle,"$.GeoZoneIds[*].$oid")!=null)
				assertEquals("API:vehicle_geoZoneIds",getIfExists(vehiclesReplied.get(v), "$.geoZoneIds"),	
													getIfExists(mongoVehicle,"$.GeoZoneIds[*].$oid"), GEOZONEIDS_STR + WRONG);
			}
			setToAllureChechkedFields();
		} catch (Exception e) {
			log.error(ERROR,e);
			setToAllureChechkedFields();
		}
	}

	//	NOT REDAY
	public void checkVehicleDeleted(String vehicleId){
		try {
//		+	db.getCollection('Vehicles').find({})					(НЕ УДАЛЕНО/Удалено)
//		+	db.getCollection('DeviceHistory').find({})   			(отвязано)
//		+	db.getCollection('OmGroups').find({}) 					(удалено из группы)
//		+	db.getCollection('Employees').find({}) 					(удалена связь)
//		+	db.getCollection('VehicleAccidents').find({}) 			(удалено)
//		+	db.getCollection('VehicleEmployeeHistory').find({})		(отвязано от сотрудника)
//		+	db.getCollection('VehicleMaintenances').find({})		(удалено)
//		+	db.getCollection('VehicleRepairs').find({})				(удалено)
//		+	db.getCollection('EmployeeInsurancePolicies').find({})	(удалено)

//		-	db.getCollection('NotificationRules').find({})  		(правило активно, Тс НЕ удалено)			
//		-	db.getCollection('ReportTemplates').find({}) 			(НЕ удалено)			
			

			 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
			 Date currentTime = new Date(System.currentTimeMillis() );
			 
			List<org.json.simple.JSONObject> deletedVehiclejsonMongoReply =  GetVehicleInfoFromMongo.getVehicleInfoById(vehicleId);
			assertTrue("checkVehicleDeleted",deletedVehiclejsonMongoReply.isEmpty(), "была найдена информация о ТС в коллекции \"Vehicles\":"+deletedVehiclejsonMongoReply.toString());

			if(this.getDeviceId()!=null && this.getDeviceId()!=""){
				JSONObject deviceHist =	(JSONObject) GetDeviceHistory.getDeviceHistoryByDeviceId(this.getDeviceId()).get(0);

					assertEquals("DB:DeviceHistory.RelatedResourceType",
							ReplyToJSON.extractPathFromJson(deviceHist,"$.RelatedResourceType"),"Vehicle","DeviceHistory.RelatedResourceType" + WRONG);
					assertEquals("DB:DeviceHistory.ResourceId",
							ReplyToJSON.extractPathFromJson(deviceHist,"$.ResourceId.$oid"),vehicleId,"DeviceHistory.ResourceId" + WRONG);

					compareDateTimeUpTo1min("Created",ReplyToJSON.extractPathFromJson(deviceHist, "$.UnboundFromResourceTime.$date"), 
							  null,currentDateTime);
/*					Date uboundTime = new Date(
							 Long.parseLong(ReplyToJSON.extractPathFromJson(deviceHist,"$.UnboundFromResourceTime.$date").toString()));
					log.debug("=========="+df.format(uboundTime)+"<<<"+df.format(currentTime));
					assertDateIsBeforeOrSame("DB:DeviceHistory.UnboundFromResourceTime",df.format(uboundTime),df.format(currentTime),
										"DeviceHistory.UnboundFromResourceTime" + WRONG);
 */
			}
			if(this.getGroupId()!=null && this.getGroupId()!=""){
				JSONObject groupInfo =	(JSONObject) GetGroupInfoFromMongo.getGroupsInfoById(this.getGroupId()).get(0);
				
				List<String> objectsInGroup = ReplyToJSON.extractListPathFromJson(groupInfo,"$.Objects..$oid");
				
				assertTrue("DB:OmGroups.Objects", ! objectsInGroup.contains(vehicleId),
						   "была найдена информация о ТС в коллекции \"OmGroups\":"+groupInfo.toString());
				
				//remove vehicle from group container
			    removeVehicleFromGroupObj (this.getGroupId(), vehicleId);

			}
			if(this.getEmployeeId()!=null && this.getEmployeeId()!=""){
				JSONObject employeeInfo = (JSONObject) GetEmployeeInfoFromMongo.getEmployeeInfoById(this.getEmployeeId()).get(0);
				assertTrue("DB:Employees.vehicleId", ! employeeInfo.containsKey("VehicleId"),
						   "была найдена информация о ТС в коллекции \"Employees\":"+employeeInfo.toString());
			}
			
			//since there is no info what should happen with rule\report when we completely remove object, following asserts are commented

/* 		if(this.notifRuleRelated!=null && this.notifRuleRelated!=""){
				List foundNotificationRules = GetNotificationRules.getnotificationRuleInfoById(this.notifRuleRelated);
				if(foundNotificationRules.size()>0){
				JSONObject notifRuleInfo = (JSONObject) foundNotificationRules.get(0);
				List<String> objectsInRule = ReplyToJSON.extractListPathFromJson(notifRuleInfo,"$.ResourcesFilter.Include");
				
				assertTrue("DB:NotificationRules.ResourcesFilter.Include", ! objectsInRule.contains(vehicleId),
						   "была найдена информация о ТС в коллекции \"NotificationRules\":"+notifRuleInfo.toString());
			log.debug(notifRuleInfo);
				}else{
					log.warn("rule wasnt found, is it ok?");
				}
			}
*/
			
		List accidentsList = GetVehicleAccidentsFromMongo.getInfoByVehicleId(vehicleId);
		assertTrue("checkVehicleAccidentsDeleted",accidentsList.isEmpty(), "была найдена информация о ТС в коллекции \"VehicleAccidents\":"+accidents.toString());

		List<JSONObject> emplHist = GetVehicleEmployeeHistoryFromMongo.getInfoByVehicleId(vehicleId);

		for (int i=0; i<emplHist.size();i++){
		assertTrue("checkVehicleEmployeeHistory.IsCurrent",(Boolean) emplHist.get(i).get("IsCurrent") == false, 
					"была найдена информация о связи с удаленным ТС в коллекции \"VehicleEmployeeHistory\":"+emplHist.get(i).toString());

		assertTrue("checkVehicleEmployeeHistory.IsDeleted",(Boolean) emplHist.get(i).get("IsDeleted") == true, 
				"была найдена информация о связи с удаленным ТС в коллекции \"VehicleEmployeeHistory\":"+emplHist.get(i).toString());
	
		 Date uboundTime = new Date(
				 Long.parseLong(ReplyToJSON.extractPathFromJson(emplHist.get(i),"$.UnboundTime.$date")));
		assertDateIsBeforeOrSame("DB:DeviceHistory.UnboundFromResourceTime",df.format(uboundTime),df.format(currentTime),
							"DeviceHistory.UnboundFromResourceTime" + WRONG);
		}
		
		List maintenancesList = GetVehicleMaintenancesInfoFromMongo.getInfoByVehicleId(vehicleId);
		assertTrue("checVehicleMaintenancesDeleted",maintenancesList.isEmpty(), "была найдена информация о ТС в коллекции \"VehicleMaintenances\":"+maintenances.toString());

		List repairsList = GetVehicleRepairsInfoFromMongo.getInfoByVehicleId(vehicleId);
		assertTrue("checVehicleRepairsDeleted",repairsList.isEmpty(), "была найдена информация о ТС в коллекции \"VehicleRepairs\":"+repairs.toString());

		List insuranceList = GetVehicleEmployeeInsurancePoliciesInfoFromMongo.getInfoByVehicleId(vehicleId);
		assertTrue("checkEmployeeInsurancePoliciesDeleted",insuranceList.isEmpty(), "была найдена информация о ТС в коллекции \"EmployeeInsurancePolicies\":"+insuranceList.toString());
		
		
			teardownData.remove(VEHICLES,  Arrays.asList(vehicleId));
			SharedContainer.removeFromContainer(VEHICLES,  vehicleId);
			SharedContainer.setContainer("deletedVehiclesIds", vehicleId);
			setToAllureChechkedFields();
			
			
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(TEST_FAILED + e.toString());
			assertTrue(false, e.toString());
		}
	}



	@Override
	public int removeFromSharedContainer() {
		int indexOfRemovedEntity=-1;
		if (!container.isEmpty() && container.containsKey(VEHICLES) && ! container.get(VEHICLES).isEmpty()) {
			ArrayList listOfIds = container.get(VEHICLES);
				// удаление из контейнера
				if (listOfIds.get(0) instanceof com.t1.core.api.Vehicle) {
					for (int i = 0; i < listOfIds.size(); i++) {
						if (((Vehicle) listOfIds.get(i)).getVehicleId().equals(this.getVehicleId())) {
							listOfIds.remove(i);
							indexOfRemovedEntity=i;
						}
					}
				}
		}
		return indexOfRemovedEntity;
	}
	
	@Override
	public void addToSharedContainer() {
		addToSharedContainer(-1);
	}

	@Override
	@SuppressWarnings({ "unchecked", "serial" })
	public void addToSharedContainer(int index) {
		if (SharedContainerInterface.getContainers().containsKey(VEHICLES) && ! SharedContainerInterface.getContainers().get(VEHICLES).isEmpty()) {
			ArrayList<Vehicle> existingArray = (ArrayList<Vehicle>) SharedContainerInterface.getContainers().get(VEHICLES);
			if(index>=0) {
				existingArray.add(index,this);
			} else {
				existingArray.add(this);
			}
			container.put(VEHICLES, existingArray);
		} else {
			ArrayList newArray = new ArrayList();
			newArray.add(this);
			container.put(VEHICLES, newArray);
		}
	}

	

	@Override
	public void updateInSharedContainer() {
		int index = this.removeFromSharedContainer();
		this.addToSharedContainer(index);
	}
	
	public void removeVehicleFromGroupObj(String groupId, String vehicleId) {
		if(SharedContainer.getContainers().containsKey("groups")) {
			Group groupFromContainer = Group.getFromSharedContainer(groupId);
			groupFromContainer.getObjects().remove(vehicleId);
			Iterator<HashMap<String, String>> iterator = groupFromContainer.getResources().iterator();
			while (iterator.hasNext()) {
				HashMap resource = iterator.next();
				if (resource.get("id").equals(vehicleId)) {
					iterator.remove();
				}
			}
		}
	}
	public static JSONObject getInitJson() {
		return initJson;
	}
	public static void setInitJson(JSONObject initJson) {
		Vehicle.initJson = initJson;
	}
	
}
