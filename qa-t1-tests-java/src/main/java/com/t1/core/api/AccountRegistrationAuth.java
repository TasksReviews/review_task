package com.t1.core.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Logger;

import com.jayway.jsonpath.PathNotFoundException;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONObject;

import com.jayway.jsonpath.JsonPath;
import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetAccountInfoFromMongo;
import com.t1.core.mongodb.GetCustomerInfoFromMongo;
import com.t1.core.mongodb.GetInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Step;

public class AccountRegistrationAuth extends AbstractClass implements SharedContainerInterface {

    private String url;
    private String login;
    private String phone;
    private String oldPhone;
    private String confirmationCode;
    private String expectedApiCode = "";
    private String accountId;
    protected static JSONObject urlParameters = new JSONObject();
    protected JSONObject initMap = new JSONObject();
    private String firstname;
    private String lastname;
    private String middleName;
    private String email;
    private String sex;
    private String role;
    private String avatar;
    private String accountStatus;
    private String accountContacts;
    private String accountMailCofirmed;
    private String accountPhoneCofirmed;
    private String password;
    private String apiMethod;
    private String token;
    private String deliveryNotificationState;
    private String deliveryNotificationMessageContent;
    private String confirmationId;
    private static final String CONST_ACC_ROLE = "account_role";
    private static final String CONST_PROFILE_AVATAR_URI = "profile_avatarUri";
    private static final String CONST_AVATAR_URI = "AvatarUri";
    private static final String CONST_LOGIN = "login";
    private static final String CONST_FIRSTNAME = "firstname";
    private static final String CONST_LASTNAME = "lastname";
    private static final String CONST_MIDDLENAME = "middleName";
    private static final String CONST_ACCOUNTCONST_PHONE = "accountPhone";
    private static final String CONST_EMAIL = "email";
    private static final String CONST_PHONE = "phone";
    private static final String CONST_ACCOUNTID = "accountId";
    private static final String CONST_TOKEN = "token";
    private static final String PATH_ID = "$._id.$oid";
    private static final String PATH_STATUS = "$.Status";
    private static final String PATH_USED = "$.Used";
    private static final String PATH_MIDDLENAME = "$.Middlename";
    private static final String PATH_MESSAGECONTENT = "$.MessageContent";
    private static final String PATH_LASTNAME = "$.Lastname";
    private static final String PATH_SEX = "$.Sex";
    private static final String PATH_PROFILESEX = "$.profile.sex";
    private static final String PATH_DEPARTMENTID = "$.DepartmentId.$oid";
    private static final String PATH_FIRSTNAME = "$.Profile.Firstname";
    private static final String PATH_CUSTIMERID = "$.CustomerId.$oid";
    private static final String PATH_NAME = "$.name";
    private static final String PATH_PERMISSIONS = "$.permissions";
    private static final String PATH_CONTACTS = "$.Contacts[0].Value";
    private static final String CONST_IGNORECUSTOMER = "IgnoreCustomer";
    private static final String CONST_DEPATMENID = "DepartmentId";
    private static final String CONST_DEPATMENID2 = "departmentId";
    private static final String CONST_EMPLOYEEID = "EmployeeId";
    private static final String CONST_EMPLOYEEID2 = "employeeId";
    private static final String CONST_PASS = "password";
    private static final String CONST_ACCOUNTUSED = "account_Used";
    private static final String CONST_FALSE = "false";
    private static final String CONST_BIRTH = "Birth";
    private static final String CONST_ACCOUNTS = "accounts";
    private static final String CONST_ACCOUNT_CUSTOMERID = "account_CustomerId";
    private static final String CONST_ACCOUNT_CUSTOMERID_WRONG = "account_CustomerId не верен";
    private static final String CONST_ACCOUNTTYPE = "account_type";
    private static final String CONST_AUTHT1CLIENT = "authorizedAccount_t1client";
    private static final String CONST_ACCOUNTCODE = "account_code";
    private static final String CONST_ACTIVE = "Active";
    private static final String CONST_ACCOUNT_ACTIVE = "account_Active";
    private static final String CONST_PANEL_HOST = "adminPanelHost";
    private static final String CONST_T1PASS = "t1APIPassword";
    private static final String CONST_AUTHT1ADMIN = "authorizedAccount_admPanel";
    private static final String CONST_CONTACT = "Contact";
    private static final String CONST_CONFIRMATIONID = "confirmationId";
    private static final String CONST_DEVTOKEN = "devToken";
    private static final String CONST_CONFCODE_WRONG = "код подтверждения не должен был быть использован";
    private static final String CONST_REPLY_SHOULD_CONTAIN = "ответ должен содержать ";
    private static final String CONST_SHOUD_BE_ACTIVE = "после подтверждения регистрации статус должен быть Active";
    private static final String CONST_ACCOUNTID_WASNT_RECEIVED = "accountId не был получен ";
    private static final String CONST_NO_TOKEN = "not able to get token: ";
    private static final String CONST_REPLY_PASS_CHANGE = "reply on password change";
    private static final String CONST_TOKEN_RECEIVED = "token received";
    private static final String CONST_AUTH_NEEDED = "для получения ТС необходимо пройти авторизацию";
    private static final String CONST_CUSTOMERID_WRONG = "customerId не верен";
    private static final String CONST_ACCID_WRONG = "accountPhoneUpdate_accountId не верен";
    private static final String CONST_CONTACT_WRONG = "accountPhoneUpdate_contact.value не верен";
    private static final String CONST_API_ACC = "API: accounts[";
    private static final String CONST_API = "API: [";
    private static final String CONST_CUSTOMERID = "].customerId";
    private static final String CONST_ACCOUNTS_STR = "accounts[";
    private static final String CONST_PROFILE_SEX = "].profile.sex";
    private static final String CONST_BR_PROFILE_AVATAR_URI = "].profile.avatarUri";
    private static final String CONST_PROFILE_SEX_WRONG = "].profile.sex не корректен";
    private static final String CONST_ACCOUNTID_STR = "{ \"accountId\" : \"";
    private static final String CONST_DELETEREPLY = "deletedReply==";


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDeliveryNotificationState() {
        return deliveryNotificationState;
    }

    public void setDeliveryNotificationState(String deliveryNotificationState) {
        this.deliveryNotificationState = deliveryNotificationState;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public String getDeliveryNotificationMessageContent() {
        return deliveryNotificationMessageContent;
    }

    public void setDeliveryNotificationMessageContent(String deliveryNotificationMessageContent) {
        this.deliveryNotificationMessageContent = deliveryNotificationMessageContent;
    }

    public String getOldPhone() {
        return oldPhone;
    }

    public void setOldPhone(String oldPhone) {
        this.oldPhone = oldPhone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getConfirmationId() {
        return confirmationId;
    }

    public void setConfirmationId(String confirmationId) {
        this.confirmationId = confirmationId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public AccountRegistrationAuth() {
        this.initMap = new InitDefValues().initializeInputWithDefaultOrRandomValues("account", getProperty("stdPattern"));
        this.setDeliveryNotificationState("");
        this.setDeliveryNotificationMessageContent("");
        setCommonValues(initMap);
    }

    /**
     * @param initMap
     * @param allFields
     */

    @SuppressWarnings("unchecked")
    public AccountRegistrationAuth(JSONObject initMap, boolean allFields) {
        setCommonValues(initMap);
        this.initMap = initMap;
        if (allFields) {
            this.sex = (initMap.get("sex") == null) ? null : initMap.get("sex").toString();
            this.role = (initMap.get(CONST_ACC_ROLE) == null) ? null : initMap.get(CONST_ACC_ROLE).toString();
            this.avatar = (initMap.get(CONST_PROFILE_AVATAR_URI) == null) ? null
                    : initMap.get(CONST_PROFILE_AVATAR_URI).toString();
            AccountRegistrationAuth.urlParameters.put("Sex", initMap.get("sex"));
            AccountRegistrationAuth.urlParameters.put("Role", initMap.get(CONST_ACC_ROLE));
            AccountRegistrationAuth.urlParameters.put(CONST_AVATAR_URI, initMap.get(CONST_PROFILE_AVATAR_URI));
            AccountRegistrationAuth.urlParameters.put(CONST_IGNORECUSTOMER, false);
            AccountRegistrationAuth.urlParameters.put("CustomerId", getProperty("testCustomerId"));
            AccountRegistrationAuth.urlParameters.put(CONST_DEPATMENID, initMap.get(CONST_DEPATMENID2));
            AccountRegistrationAuth.urlParameters.put(CONST_EMPLOYEEID, initMap.get(CONST_EMPLOYEEID2));
            AccountRegistrationAuth.urlParameters.put("AccountType", "Person");
            AccountRegistrationAuth.urlParameters.put("AdminCreated", false);
        }
    }

    @SuppressWarnings("unchecked")
    public void setCommonValues(JSONObject initMap) {
        this.apiMethod = "/register";
        this.url = getProperty("host") + apiMethod;
        this.setConfirmationCode("");
        this.accountStatus = "";
        this.accountContacts = "";
        this.accountMailCofirmed = "";
        this.accountPhoneCofirmed = "";
        this.setAccountId("");
        this.setToken("");

        this.firstname = (initMap.get(CONST_FIRSTNAME) == null) ? null : initMap.get(CONST_FIRSTNAME).toString();
        this.lastname = (initMap.get(CONST_LASTNAME) == null) ? null : initMap.get(CONST_LASTNAME).toString();
        this.middleName = (initMap.get(CONST_MIDDLENAME) == null) ? null : initMap.get(CONST_MIDDLENAME).toString();
        this.setLogin((initMap.get(CONST_LOGIN) == null) ? null : initMap.get(CONST_LOGIN).toString());
        this.setPhone((initMap.get(CONST_ACCOUNTCONST_PHONE) == null) ? null : initMap.get(CONST_ACCOUNTCONST_PHONE).toString());
        this.setEmail((initMap.get(CONST_EMAIL) == null) ? null : initMap.get(CONST_EMAIL).toString());
        this.setPassword((initMap.get(CONST_PASS) == null) ? null : initMap.get(CONST_PASS).toString());
        this.avatar = (initMap.get(CONST_PROFILE_AVATAR_URI) == null) ? null : initMap.get(CONST_PROFILE_AVATAR_URI).toString();

        AccountRegistrationAuth.urlParameters.clear();
        AccountRegistrationAuth.urlParameters.put(CONST_LOGIN, this.getLogin());
        AccountRegistrationAuth.urlParameters.put(CONST_FIRSTNAME, this.firstname);
        AccountRegistrationAuth.urlParameters.put(CONST_LASTNAME, this.lastname);
        if (this.middleName != null)
            AccountRegistrationAuth.urlParameters.put(CONST_MIDDLENAME, this.middleName);
        if (this.getPhone() != null)
            AccountRegistrationAuth.urlParameters.put(CONST_PHONE, this.getPhone());
        if (this.getEmail() != null)
            AccountRegistrationAuth.urlParameters.put(CONST_EMAIL, this.getEmail());
        AccountRegistrationAuth.urlParameters.put(CONST_PASS, this.getPassword());
    }

    // @Test(singleThreaded = true, invocationTimeOut = 10000, groups = { "functest", "integration" } )
    @Step("Самостоятельная регистрация нового аккаунта")
    public List<org.json.simple.JSONObject> registerAccount() {
        List<org.json.simple.JSONObject> mongoAccountInfo = null;
        try {
            // send acount registration request
            org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, true);
            setAccountId(registrationReply.get(CONST_ACCOUNTID).toString());
            // get account info from mongo
            mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(getAccountId(), "");
            accountStatus = JsonPath.read(mongoAccountInfo.get(0), PATH_STATUS);
            // compare mongo with input
            checkRegistredAccount(mongoAccountInfo.get(0), registrationReply, this.initMap);
            return mongoAccountInfo;
        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
            assertTrue(false, "not able to complete self registration");
        }
        return mongoAccountInfo;
    }


    //self registration

    public AccountRegistrationAuth startCommonAccountRegistration(JSONObject initMap, String deliveryNotificationType,
                                                                  String deliveryNotificationValue) {
        AccountRegistrationAuth account = new AccountRegistrationAuth(initMap, false);
        try {
            initMap.remove("role");
            initMap.remove(CONST_PROFILE_AVATAR_URI);
            initMap.remove(CONST_MIDDLENAME);
            initMap.remove(CONST_ACC_ROLE);
            initMap.remove("sex");
            account.initMap = initMap;
            List<org.json.simple.JSONObject> mongoAccountInfo = account.registerAccount();

            this.setAccountId(ReplyToJSON.extractPathFromJson(mongoAccountInfo.get(0), PATH_ID));

            TearDownExecutor.addAccountToTeardown(this.getAccountId());
            this.accountStatus = ReplyToJSON.extractPathFromJson(mongoAccountInfo.get(0), PATH_STATUS);
            assertEquals("account_Status", this.accountStatus, "New", "некорректный статус");
            this.accountContacts = ReplyToJSON.extractPathFromJson(mongoAccountInfo.get(0), "$.Contacts[0].Confirmed");
            assertEquals("account_Contacts[0].Confirmed", this.accountContacts, null, "некорректный статус контактов");

            JSONObject accountConfirmationInfo = GetAccountInfoFromMongo.getAccountConfirmationInfoFromMongo(this.getAccountId(),
                    this.getPhone(), this.getEmail());
            this.setConfirmationCode(ReplyToJSON.extractPathFromJson(accountConfirmationInfo, "$.Code"));
            log.info("Got confirmation code: " + this.getConfirmationCode());

            assertTrue(CONST_ACCOUNTUSED, ReplyToJSON.extractPathFromJson(accountConfirmationInfo, PATH_USED).equals(CONST_FALSE),
                    CONST_CONFCODE_WRONG);
            log.info("Get Delivery Notification from DB");
            JSONObject deliveryNotification = GetAccountInfoFromMongo
                    .getLastDeliveryNotificationByReceiverContact(deliveryNotificationType, deliveryNotificationValue, "");
            // Validation
            this.setDeliveryNotificationState(ReplyToJSON.extractPathFromJson(deliveryNotification, "$.State"));
            this.setDeliveryNotificationMessageContent(ReplyToJSON.extractPathFromJson(deliveryNotification, PATH_MESSAGECONTENT));
            setToAllureChechkedFields();

        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
            assertTrue(false, e.toString());
        }
        return account;
    }

    /**
     * @return token
     * @throws Exception
     */
    @Step("подтверждение регистрации аккаунта {0}")
    public String confirmRegistration(String accountId) throws Exception {
        try {
            this.url = getProperty("host") + "/register/confirm";
            log.info("send GET confirmation with code");
            String getUrlParameters = "";
            if (this.getConfirmationCode() == "") {
                JSONObject accountConfirmationInfo = GetAccountInfoFromMongo.getAccountConfirmationInfoFromMongo(this.getAccountId(),
                        this.getPhone(), this.getEmail());
                this.setConfirmationCode(ReplyToJSON.extractPathFromJson(accountConfirmationInfo, "$.Code"));
            }
            if ((this.getPhone() != null) && (this.getPhone() != "")) {
                getUrlParameters = "?accountId=" + this.getAccountId() + "&code=" + this.getConfirmationCode() + "&contact=" + this.getPhone().replace("+", "%2B");
            } else if ((this.getEmail() != null) && (this.getEmail() != "")) {
                getUrlParameters = "?accountId=" + this.getAccountId() + "&code=" + this.getConfirmationCode() + "&contact=" + this.getEmail();
            } else {
                assertTrue(false, "должен быть хотябы один тип контакта для подтверждения регистрации.");
            }
            // send GET confirmation with confirmation code

            JSONObject confirmationReply = (JSONObject) SendRequestToAPI.sendGetRequest(url + getUrlParameters, "GET", "", "", "").get(0);
            checkRegistrationConfirmationToken(confirmationReply);
            this.setToken(confirmationReply.get(CONST_TOKEN).toString());
            checkRegistrationConfirmation();
        } catch (Exception e) {
            assertTrue(false, "невозможно подтвердить регистрацию аккаунта " + accountId + e.toString());

            log.error(ERROR, e);
        }

        return this.getToken();

    }

    @Step("Запрос регистрации нового аккаунта из профиля")
    public JSONObject registerNewAccFromProfile(JSONObject initMap, String token, String expectedCode) {
        JSONObject newAccRequestReply = null;
        AccountRegistrationAuth.urlParameters.clear();
        try {
            this.url = getProperty("host") + "/Account/Create";
            urlParameters.put("Sex", initMap.get("sex"));
            urlParameters.put(CONST_AVATAR_URI, initMap.get(CONST_PROFILE_AVATAR_URI));
            this.setEmail(initMap.get(CONST_EMAIL).toString());
            this.setPhone(initMap.get(CONST_PHONE).toString());
            this.lastname = initMap.get(CONST_LASTNAME).toString();
            this.firstname = initMap.get(CONST_FIRSTNAME).toString();
            this.setLogin(initMap.get(CONST_LOGIN).toString());
            this.setPassword(initMap.get(CONST_PASS).toString());
            this.role = initMap.get(CONST_ACC_ROLE).toString();
            initMap.remove(CONST_MIDDLENAME);
            urlParameters.put(CONST_EMAIL, this.getEmail());
            urlParameters.put(CONST_PHONE, this.getPhone());
            urlParameters.put("lastName", this.lastname);
            urlParameters.put("firstName", this.firstname);
            urlParameters.put(CONST_LOGIN, this.getLogin());
            urlParameters.put(CONST_PASS, this.getPassword());
            urlParameters.put("role", this.role);
            if (expectedCode == "") {
                if (token == null || token == "") {
                    token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
                }
                newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, true, true);
                log.debug("reply on new acc from profile req " + newAccRequestReply.toString());
            } else {
                newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, false);
                assertEquals(CONST_ACCOUNTCODE, newAccRequestReply.get("code"), expectedCode, CONST_REPLY_SHOULD_CONTAIN + expectedCode);
            }
            if (newAccRequestReply.containsKey(CONST_ACCOUNTID)) {
                setAccountId(newAccRequestReply.get(CONST_ACCOUNTID).toString());
                TearDownExecutor.addAccountToTeardown(getAccountId());

                List<org.json.simple.JSONObject> mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(getAccountId(), "");

                this.accountStatus = JsonPath.read(mongoAccountInfo.get(0), PATH_STATUS);
                assertEquals(CONST_ACCOUNT_ACTIVE, this.accountStatus, CONST_ACTIVE, CONST_SHOUD_BE_ACTIVE);
                // compare mongo with input
                checkRegistredAccount(mongoAccountInfo.get(0), newAccRequestReply, initMap);
            } else {
                assertTrue(false, CONST_ACCOUNTID_WASNT_RECEIVED + newAccRequestReply.toString());
            }

            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();

            log.error(ERROR, e);
        }

        return newAccRequestReply;

    }


    @Step("Запрос изменения существующего аккаунта из профиля")
    public JSONObject updateAccFromProfile(JSONObject initMap, String token, String expectedCode) {
        JSONObject newAccRequestReply = null;
        AccountRegistrationAuth.urlParameters.clear();
        try {
            this.url = getProperty("host") + "/Account/Update";
            initMap.remove(CONST_MIDDLENAME);
            urlParameters.put(CONST_ACCOUNTID, this.getAccountId());
            urlParameters.put("Sex", initMap.get("sex"));
            urlParameters.put(CONST_AVATAR_URI, initMap.get(CONST_PROFILE_AVATAR_URI));
            urlParameters.put("Role", initMap.get(CONST_ACC_ROLE));
            urlParameters.put("lastName", this.lastname);
            urlParameters.put("firstName", this.firstname);
            urlParameters.put(CONST_LOGIN, this.getLogin());
            urlParameters.put(CONST_PASS, this.getPassword());
            urlParameters.put(CONST_EMAIL, this.getEmail());
            urlParameters.put(CONST_PHONE, this.getPhone());

            if (expectedCode == "") {
                newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, true, true);
                log.debug("reply on new acc from profile req " + newAccRequestReply.toString());
            } else {
                newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, false);
                assertEquals(CONST_ACCOUNTCODE, newAccRequestReply.get("code"), expectedCode, CONST_REPLY_SHOULD_CONTAIN + expectedCode);
            }
            if (newAccRequestReply.containsKey(CONST_ACCOUNTID)) {
                setAccountId(newAccRequestReply.get(CONST_ACCOUNTID).toString());

                List<org.json.simple.JSONObject> mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(getAccountId(), "");

                this.accountStatus = JsonPath.read(mongoAccountInfo.get(0), PATH_STATUS);
                assertEquals(CONST_ACCOUNT_ACTIVE, this.accountStatus, CONST_ACTIVE, CONST_SHOUD_BE_ACTIVE);
                // compare mongo with input
                checkRegistredAccount(mongoAccountInfo.get(0), newAccRequestReply, initMap);
            } else {
                assertTrue(false, CONST_ACCOUNTID_WASNT_RECEIVED + newAccRequestReply.toString());
            }

            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();

            log.error(ERROR, e);
        }

        return newAccRequestReply;

    }

    @Step("Запрос регистрации нового аккаунта из панели администратора")
    public JSONObject registerNewAccFromAdminPanel(JSONObject initMap, String token, HashMap filesMap, String expectedCode) {
        JSONObject newAccRequestReply = null;
        try {
            this.url = getProperty(CONST_PANEL_HOST) + "/Api/Admin/Account/Create";
            urlParameters.put("Sex", initMap.get("sex"));
            urlParameters.put(CONST_AVATAR_URI, initMap.get(CONST_PROFILE_AVATAR_URI));
            urlParameters.put("Role", initMap.get(CONST_ACC_ROLE));
            urlParameters.put(CONST_IGNORECUSTOMER, false);

            if (expectedCode == "") {
                if (filesMap != null && !filesMap.keySet().isEmpty()) {
                    newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters, true), token, "multipart", filesMap, true).get(0);
                } else {
                    newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters, true), token, true, true);
                }
                log.debug("reply on new acc from admin panel req " + newAccRequestReply.toString());
            } else {
                newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters, true), token, true, false);
                assertEquals(CONST_ACCOUNTCODE, newAccRequestReply.get("code"), expectedCode, CONST_REPLY_SHOULD_CONTAIN + expectedCode);
            }
            if (newAccRequestReply.containsKey(CONST_ACCOUNTID)) {
                setAccountId(newAccRequestReply.get(CONST_ACCOUNTID).toString());
                TearDownExecutor.addAccountToTeardown(getAccountId());

                List<org.json.simple.JSONObject> mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(getAccountId(), "");

                this.accountStatus = JsonPath.read(mongoAccountInfo.get(0), PATH_STATUS);
                assertEquals(CONST_ACCOUNT_ACTIVE, this.accountStatus, CONST_ACTIVE, CONST_SHOUD_BE_ACTIVE);
                // compare mongo with input
                checkRegistredAccount(mongoAccountInfo.get(0), newAccRequestReply, initMap);
            } else {
                assertTrue(false, CONST_ACCOUNTID_WASNT_RECEIVED + newAccRequestReply.toString());
            }
            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();

            log.error(ERROR, e);
        }

        return newAccRequestReply;

    }

//	


    @Step("Авторизация:\r\nпользователь {0}")
    public String loginInClientPanel(String login, String pass) {
        this.url = getProperty("host") + "/login";
        JSONObject authorizedAccount = null;
        urlParameters.clear();
        urlParameters.put(CONST_LOGIN, login);
        urlParameters.put(CONST_PASS, pass);
        try {
            authorizedAccount = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, true);
            checkRegistrationConfirmation(authorizedAccount);
            final JSONObject authorizedAccInfo = new JSONObject();
            authorizedAccInfo.put("id", authorizedAccount.get(CONST_ACCOUNTID).toString());
            authorizedAccInfo.put(CONST_LOGIN, login);
            authorizedAccInfo.put("role", authorizedAccount.get("role").toString());
            authorizedAccInfo.put(CONST_TOKEN, authorizedAccount.get(CONST_TOKEN).toString());
            authorizedAccInfo.put("place", "t1client");
            if (SharedContainer.getContainers().containsKey(CONST_AUTHT1CLIENT)) {
                SharedContainer.removeFromContainer(CONST_AUTHT1CLIENT);
            }
            SharedContainer.setContainer(CONST_AUTHT1CLIENT, authorizedAccInfo);
            this.role = authorizedAccount.get("role").toString();
            token = authorizedAccount.get(CONST_TOKEN).toString();
        } catch (Exception e) {
            log.error(CONST_NO_TOKEN + e);
        }
        return token;

    }

    @Step("Авторизация:\r\nпользователь autotest_qa_admin с правами администратора")
    public String loginAsT1Admin() {
        return loginInClientPanel(getProperty("t1APIAdmin"), getProperty(CONST_T1PASS));
    }

    @Step("Авторизация:\r\nпользователь autotest_qa_demo с правами обычного пользователя")
    public String loginAsT1User() {
        return loginInClientPanel(getProperty("t1APIUser"), getProperty(CONST_T1PASS));
    }

    @Step("Авторизация:\r\nпользователь demo права Администратора")
    public String loginAsT1Demo() {
        return loginInClientPanel(getProperty("t1APIDemo"), getProperty("t1APIDemoPass"));
    }

    @Step("Авторизация:\r\nпользователь autotest_qa_developer права Разработчик")
    public String loginAsT1Developer() {
        return loginInClientPanel(getProperty("t1APIDeveloper"), getProperty(CONST_T1PASS));
    }


    @Step("Валидация:\r\nподтверждение регистрации: получен токен")
    public void checkRegistrationConfirmation(HashMap<String, Object> response) {
        assertNotNull("account_token", response.get(CONST_TOKEN));
        setToAllureChechkedFields();
    }


    /**
     * @param login
     * @param pass
     * @return token
     */
    @SuppressWarnings("unchecked")
    @Step("Авторизация в панели администратора:\r\nпользователь {0}")
    public String loginInAdminPanel(String login, String pass, Boolean requestIsPositive) {
        this.url = getProperty(CONST_PANEL_HOST) + "/Api/LoginAdmin";
        JSONObject authorizedAccount = null;
        String result = null;

        urlParameters.clear();
        urlParameters.put(CONST_LOGIN, login);
        urlParameters.put(CONST_PASS, pass);
        try {

            authorizedAccount = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, requestIsPositive);
            if (requestIsPositive) {
                checkRegistrationConfirmation(authorizedAccount);
                final JSONObject authorizedAccInfo = new JSONObject();
                if (authorizedAccount.containsKey(CONST_ACCOUNTID)) {
                    authorizedAccInfo.put("id", authorizedAccount.get(CONST_ACCOUNTID).toString());
                }
                authorizedAccInfo.put(CONST_LOGIN, login);
                authorizedAccInfo.put("role", authorizedAccount.get("role").toString());
                authorizedAccInfo.put(CONST_TOKEN, authorizedAccount.get(CONST_TOKEN).toString());
                SharedContainer.setContainer(CONST_AUTHT1ADMIN, authorizedAccInfo);
                result = authorizedAccount.get(CONST_TOKEN).toString();
            } else {
                result = authorizedAccount.get("code").toString();
            }
        } catch (Exception e) {
            log.error(CONST_NO_TOKEN + e);
        }
        return result;

    }

    @Step("Авторизация в панели администратора :\r\nпользователь с правами администратора")
    public String loginAsT1AdminInAdminPanel() {
        return loginInAdminPanel(getProperty("t1APIDeveloper"), getProperty(CONST_T1PASS), true);

    }

    @Step("Авторизация в панели администратора :\r\nпользователя {0} с правами администратора")
    public String loginAsT1AdminInAdminPanel(String login, String password) {
        return loginInAdminPanel(login, password, true);
    }


    @Step("Выход из аккаунта:\r\nпользователь {0}")
    public void logout(String login) {
        this.url = getProperty("host") + "/logout";
        JSONObject logoutReply = null;
        try {
            logoutReply = SendRequestToAPI.sendPostRequest(url, "", this.getToken(), false, true);
            log.debug("logout reply ==" + logoutReply);
            checkLoggedOut(logoutReply);
        } catch (Exception e) {
            log.error(CONST_NO_TOKEN + e);
        }

    }


    @Step("Продление токена:\r\nпользователь {0}")
    public void renewToken(String login) {
        this.url = getProperty("host") + "/renew";
        JSONObject renewReply = null;
        try {
            renewReply = SendRequestToAPI.sendPostRequest(url, "", this.getToken(), false, true);
            checkRegistrationConfirmation(renewReply);
            this.setToken(renewReply.get(CONST_TOKEN).toString());
        } catch (Exception e) {
            log.error(CONST_NO_TOKEN + e);
        }

    }

    @Step("Запрос сброса пароля:\r\nпользователь {0}")
    public String resetPasswordStart(String login, String contact, String expectedCode) {
        this.url = getProperty("host") + "/resetPassword/start";
        JSONObject resetPasswordReply = null;
        try {
            urlParameters.clear();
            urlParameters.put(CONST_CONTACT, contact);

            if (expectedCode == "") {
                resetPasswordReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, true);
                setConfirmationId(resetPasswordReply.get(CONST_CONFIRMATIONID).toString());
                log.debug("confirmationId=" + resetPasswordReply);
                assertNotNull("account_confirmationId", resetPasswordReply.get(CONST_CONFIRMATIONID), "ответ должен содержать confirmationId");
            } else {
                resetPasswordReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, false);
                assertEquals(CONST_ACCOUNTCODE, resetPasswordReply.get("code"), expectedCode, CONST_REPLY_SHOULD_CONTAIN + expectedCode);
            }
            log.info("Got confirmation code for password reset: " + getConfirmationId());
            setToAllureChechkedFields();
        } catch (Exception e) {
            log.error(CONST_NO_TOKEN + e);
            setToAllureChechkedFields();
        }
        return getConfirmationId();

    }

    /**
     * @param accountId
     * @param confirmationId
     * @param code
     * @param newPassword
     * @param expectedCode
     * @return
     * @throws Exception
     */
    @Step("Запрос установки нового пароля:\r\nпользователь {0}")
    public String resetPasswordPerform(String accountId, String confirmationId, String code, String newPassword, String expectedCode) throws Exception {
        try {
            JSONObject resetPasswordReply = null;
            this.url = getProperty("host") + "/resetPassword/perform";

            urlParameters.clear();
            urlParameters.put("ConfirmationId", confirmationId);
            urlParameters.put("code", code);
            urlParameters.put("newPassword", newPassword);
            if (expectedCode == "") {
                resetPasswordReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, true);
                log.debug(CONST_REPLY_PASS_CHANGE + resetPasswordReply.toString());
            } else {
                resetPasswordReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, false);
                assertEquals(CONST_ACCOUNTCODE, resetPasswordReply.get("code"), expectedCode, CONST_REPLY_SHOULD_CONTAIN + expectedCode);
            }
            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
        }

        return this.getToken();

    }


    @Step("Запрос повторной отправки кода подтверждения для сброса пароля:\r\nпользователь {0}")
    public String resetPasswordResendCode(String login, String contact, String expectedCode) {

        this.url = getProperty("host") + "/resetPassword/resendCode";
        JSONObject resendPasswordReply = null;
        try {
            urlParameters.clear();
            urlParameters.put(CONST_CONTACT, contact);
            if (expectedCode == "") {
                resendPasswordReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, true);
                if (resendPasswordReply != null) {
                    confirmationId = resendPasswordReply.get(CONST_CONFIRMATIONID).toString();
                    log.debug("confirmationId=" + resendPasswordReply);
                    assertNotNull("account_confirmationId", resendPasswordReply.get(CONST_CONFIRMATIONID), "ответ должен содержать confirmationId");
                } else {
                    assertTrue(false, "http ответ не содержит тела ответа");
                }
            } else {
                resendPasswordReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, false);
                assertEquals(CONST_ACCOUNTCODE, resendPasswordReply.get("code"), expectedCode, CONST_REPLY_SHOULD_CONTAIN + expectedCode);
            }
            log.info("Got confirmation code for password reset: " + confirmationId);
            setToAllureChechkedFields();
        } catch (Exception e) {
            log.error(CONST_NO_TOKEN + e);
            setToAllureChechkedFields();
        }
        return confirmationId;
    }

    @Step("Запрос повторной отправки кода подтверждения регистрации:\r\nпользователь {0}")
    public void registerResendCode(String login, String contact, String deliveryNotificationType, String expectedCode) {

        this.url = getProperty("host") + "/register/resendCode";
        JSONObject resendRegistrationCodeReply = null;
        try {
            urlParameters.clear();
            urlParameters.put(CONST_ACCOUNTID, getAccountId());
            urlParameters.put(CONST_CONTACT, contact);
            if (expectedCode != "") {
                resendRegistrationCodeReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, false);
                assertEquals(CONST_ACCOUNTCODE, resendRegistrationCodeReply.get("code"), expectedCode, CONST_REPLY_SHOULD_CONTAIN + expectedCode);

            } else {
                resendRegistrationCodeReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, true);
                if (resendRegistrationCodeReply != null) {
                    assertEquals("account_messageBody", "Ok", resendRegistrationCodeReply.get("messageBody").toString(), "некорректное содержимое сообщения ответа");
                }
                JSONObject accountConfirmationInfo = GetAccountInfoFromMongo.getAccountConfirmationInfoFromMongo(this.getAccountId(),
                        this.getPhone(), this.getEmail());
                org.json.simple.JSONArray notificationIds = JsonPath.read(accountConfirmationInfo, "$.NotificationIds");
                log.info("found " + notificationIds.size() + " NotificationIds in MongoDB");
                assertTrue("account_NotificationIds", notificationIds.size() > 1, "после запроса повторной отправки кода подтверждения регистрации в БД должно существовать не менее 2 NotificationIds");
                if (notificationIds.size() > 1) {
                    assertEquals("account_confirmationCode", this.getConfirmationCode(), accountConfirmationInfo.get("Code").toString(), "confirmation code не должен был изменитсья");
                }
                assertTrue(ReplyToJSON.extractPathFromJson(accountConfirmationInfo, PATH_USED).equals(CONST_FALSE), CONST_CONFCODE_WRONG);

                log.info("Get Delivery Notification from DB");
                List<JSONObject> deliveryNotificationList = GetAccountInfoFromMongo
                        .getAllDeliveryNotificationByReceiverContactAndChannelType(deliveryNotificationType, contact, "");

                assertEquals("account_confirmationCode", this.getConfirmationCode(), ReplyToJSON.extractPathFromJson(deliveryNotificationList.get(0), PATH_MESSAGECONTENT), "confirmation code не должен был изменитсья");
                // Validation
                this.setDeliveryNotificationState(ReplyToJSON.extractPathFromJson(deliveryNotificationList.get(0), "$.State"));
                this.setDeliveryNotificationMessageContent(ReplyToJSON.extractPathFromJson(deliveryNotificationList.get(0), PATH_MESSAGECONTENT));
            }
            setToAllureChechkedFields();
        } catch (Exception e) {
            log.error("not able to complete register_resendCode() : " + e);
            assertTrue(false, e.toString());
        }
    }

    @Step("Запрос проверки полученного кода подтверждения смены пароля:\r\nпользователь {0}")
    public void confirmationVerify(String accountId, String confirmationId, String confirmationCode, String expectedCode) {

        try {
            JSONObject confirmationVerifyReply = null;
            this.url = getProperty("host") + "/confirmation/verify";

            urlParameters.clear();
            urlParameters.put("ConfirmationId", confirmationId);
            urlParameters.put("code", confirmationCode);
            if (expectedCode == "") {
                confirmationVerifyReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, true);
                assertEquals(CONST_ACCOUNTCODE, ReplyToJSON.extractPathFromJson(confirmationVerifyReply, "$.code"), "Ok", "код подтверждения некорректен");
                log.debug("reply on confirmation verify" + confirmationVerifyReply.toString());
            } else {
                confirmationVerifyReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, false);
                assertEquals(CONST_ACCOUNTCODE, confirmationVerifyReply.get("code"), expectedCode, CONST_REPLY_SHOULD_CONTAIN + expectedCode);
            }

            JSONObject accountConfirmationInfo = GetAccountInfoFromMongo.getAccountConfirmationInfoFromMongo(this.getAccountId(),
                    this.getPhone(), this.getEmail());
            assertTrue(CONST_ACCOUNTUSED, ReplyToJSON.extractPathFromJson(accountConfirmationInfo, PATH_USED).equals(CONST_FALSE), CONST_CONFCODE_WRONG);
            assertTrue("account_Operation", ReplyToJSON.extractPathFromJson(accountConfirmationInfo, "$.Operation").equals("passwordChange"), "получен некорректный документ из БД");
            setToAllureChechkedFields();
        } catch (Exception e) {
            log.error(ERROR, e);
        }

    }


    @Step("Запрос изменения аккаунта из панели администратора")
    public JSONObject updateAccFromAdminPanel(JSONObject initMap, String token, HashMap filesMap, String accountId, String expectedCode) {
        JSONObject newAccRequestReply = null;
        try {
            this.url = getProperty(CONST_PANEL_HOST) + "/Api/Admin/Account/Update";
            urlParameters.put(CONST_ACCOUNTID, accountId);
            urlParameters.put("Sex", initMap.get("sex"));
            urlParameters.put(CONST_AVATAR_URI, initMap.get(CONST_PROFILE_AVATAR_URI));
            urlParameters.put("Role", initMap.get(CONST_ACC_ROLE));
            urlParameters.put(CONST_IGNORECUSTOMER, false);

            if (expectedCode == "") {
                if (filesMap != null && !filesMap.keySet().isEmpty()) {
                    newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters, true), token, "multipart", filesMap, true).get(0);
                } else {
                    newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters, true), token, true, true);
                }
                log.debug("reply on new acc from admin panel req " + newAccRequestReply.toString());
            } else {
                newAccRequestReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters, true), token, true, false);
                assertEquals(CONST_ACCOUNTCODE, newAccRequestReply.get("code"), expectedCode, CONST_REPLY_SHOULD_CONTAIN + expectedCode);
            }
            if (newAccRequestReply.containsKey(CONST_ACCOUNTID)) {
                accountId = newAccRequestReply.get(CONST_ACCOUNTID).toString();
                TearDownExecutor.addAccountToTeardown(accountId);

                List<org.json.simple.JSONObject> mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(accountId, "");

                this.accountStatus = JsonPath.read(mongoAccountInfo.get(0), PATH_STATUS);
                assertEquals(CONST_ACCOUNT_ACTIVE, this.accountStatus, CONST_ACTIVE, CONST_SHOUD_BE_ACTIVE);
                // compare mongo with input
                checkRegistredAccount(mongoAccountInfo.get(0), newAccRequestReply, initMap);
            } else {
                assertTrue(false, CONST_ACCOUNTID_WASNT_RECEIVED + newAccRequestReply.toString());
            }
            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
        }

        return newAccRequestReply;

    }

    public JSONObject accountChangePhoneStart() {
        JSONObject phoneUpdateReply = null;
        try {
            this.url = getProperty("host") + "/Api/Account/Contact/ChangePhone";
            urlParameters.clear();
            urlParameters.put(CONST_ACCOUNTID, this.getAccountId());
            urlParameters.put(CONST_PHONE, this.getPhone());
            if (this.expectedApiCode == "") {
                phoneUpdateReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), this.getToken(), false, true);
                log.debug(CONST_REPLY_PASS_CHANGE + phoneUpdateReply.toString());
            } else {
                phoneUpdateReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), this.getToken(), false, false);
                assertEquals("account_expectedApiCode", phoneUpdateReply.get("code"), this.expectedApiCode, CONST_REPLY_SHOULD_CONTAIN + this.expectedApiCode);
            }
            org.json.simple.JSONObject mongoAccountInfo = (JSONObject) GetAccountInfoFromMongo.getAccountInfoByAccountId(this.getAccountId(), "").get(0);
            checkphoneUpdateRequestReply(phoneUpdateReply, mongoAccountInfo);
            setToAllureChechkedFields();
        } catch (Exception e) {
            log.error(ERROR, e);
        }

        return phoneUpdateReply;

    }

    public JSONObject accountChangePhoneConfirmation() {
        JSONObject phoneUpdateReply = null;
        try {
            this.url = getProperty("host") + "/Api/Account/Contact/ChangePhone";
            urlParameters.clear();
            urlParameters.put(CONST_ACCOUNTID, this.getAccountId());
            urlParameters.put(CONST_PHONE, this.getPhone());
            urlParameters.put("code", this.getConfirmationCode());
            urlParameters.put(CONST_CONFIRMATIONID, this.getConfirmationId());
            if (this.expectedApiCode == "") {
                phoneUpdateReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), this.getToken(), false, true);
                log.debug(CONST_REPLY_PASS_CHANGE + phoneUpdateReply.toString());
            } else {
                phoneUpdateReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), "", false, false);
                assertEquals(CONST_ACCOUNTCODE, phoneUpdateReply.get("code"), this.expectedApiCode, CONST_REPLY_SHOULD_CONTAIN + this.expectedApiCode);
            }
            org.json.simple.JSONObject mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(this.getAccountId(), "").get(0);
            assertNotNull("API:accountPhoneUpdate_contact.confirmed",
                    ReplyToJSON.extractPathFromJson(phoneUpdateReply, "$.contact.confirmed"), "accountPhoneUpdate_contact.confirmed не верен");
            checkphoneUpdateRequestReply(phoneUpdateReply, mongoAccountInfo);
            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
        }

        return phoneUpdateReply;
    }


    @SuppressWarnings("unchecked")
    @Step("получение списка всех аккаунтов")
    public List<org.json.simple.JSONObject> getAllAccountsFromAdminPanel() {
        apiMethod = "/Api/Admin/Accounts";
        url = getProperty(CONST_PANEL_HOST) + apiMethod;

        List<org.json.simple.JSONObject> accListReply = null;
        List<org.json.simple.JSONObject> mongoAccountsInfo = null;
        List<org.json.simple.JSONObject> mongoDepartments = null;

        try {
            setToken(getCurAuthAccToken(CONST_AUTHT1ADMIN));
            assertTrue(CONST_TOKEN_RECEIVED, getToken() != null, CONST_AUTH_NEEDED);
            accListReply = (List<JSONObject>) SendRequestToAPI.sendGetRequest(url, "GET", "", "", getToken());

            mongoAccountsInfo = GetAccountInfoFromMongo.getAllAccounts();
            List<org.json.simple.JSONObject> mongoRolesInfo = GetAccountInfoFromMongo.getAllRoles();

            for (int i = 0; i < accListReply.size(); i++) {
                String accApiRoleId = "";
                String accApiRoleTitle = "";
                for (JSONObject mongoRole : mongoRolesInfo) {
                    if (((JSONObject) accListReply.get(i).get("account")).containsKey("role")) { // some accounts have no roles.
                        String apiAccRole = ReplyToJSON.extractPathFromJson(accListReply.get(i), "$.account.role");
                        if ((apiAccRole != null && apiAccRole != "")
                                && (ReplyToJSON.extractPathFromJson(mongoRole, "Name").matches(apiAccRole))) {
                            accApiRoleId = ReplyToJSON.extractPathFromJson(mongoRole, PATH_ID);
                            accApiRoleTitle = ReplyToJSON.extractPathFromJson(mongoRole, "$.Title");
                            break;

                        }
                    }
                }

                JSONObject mongoAccountInfo = new JSONObject();
                String apiAccountId = ReplyToJSON.extractPathFromJson(accListReply.get(i), "$.account.id");
                for (int j = 0; j < mongoAccountsInfo.size(); j++) {
                    if (ReplyToJSON.extractPathFromJson(mongoAccountsInfo.get(j), PATH_ID).matches(apiAccountId)) {
                        mongoAccountInfo = mongoAccountsInfo.get(j);
                        break;
                    }
                }

//			// compare mongo with input
                checkAccountProfileViewModel(mongoAccountInfo, accListReply.get(i), accApiRoleId, accApiRoleTitle);
                setToAllureChechkedFields();
            }
        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
            assertTrue(false, e.toString());
        }
        return accListReply;
    }


    private void checkAccountProfileViewModel(JSONObject mongoAccountInfo, JSONObject apiAccount, String accApiRoleId, String accApiRoleTitle) {
        try {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));


            setAccountId(ReplyToJSON.extractPathFromJson(apiAccount, "$.account.id"));
            //check account structure
            assertContainsMngId(CONST_API_ACC + getAccountId() + "].id", getAccountId(), "account id не верен");

            assertEquals(CONST_API + getAccountId() + "].account.type",
                    ReplyToJSON.extractPathFromJson(apiAccount, "$.account.type"), ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.Type"),
                    "[" + getAccountId() + "].account.type не корректен");
            if (accApiRoleId != "")
                assertEquals(CONST_API + getAccountId() + "].account.role", accApiRoleId, ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.Roles[0].$oid"),
                        "[" + getAccountId() + "].account.role не корректен");
            if (accApiRoleTitle != "")
                assertEquals(CONST_API + getAccountId() + "].account.roleTitle", ReplyToJSON.extractPathFromJson(apiAccount, "$.account.roleTitle"), accApiRoleTitle,
                        "[" + getAccountId() + "].account.roleTitle не корректен");

            assertEquals(CONST_API + getAccountId() + "].account.status",
                    ReplyToJSON.extractPathFromJson(apiAccount, "$.account.status"), ReplyToJSON.extractPathFromJson(mongoAccountInfo, PATH_STATUS),
                    "[" + getAccountId() + "].account.status не корректен");

            if (mongoAccountInfo.containsKey("StatusDate")) {
                Date mongoStatusDateTime = new Date(Long.parseLong(ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.StatusDate.$date")));
                assertEquals(CONST_API + getAccountId() + "].account.statusDate",
                        ReplyToJSON.extractPathFromJson(apiAccount, "$.account.statusDate"), df.format(mongoStatusDateTime),
                        "[" + getAccountId() + "].account.status не корректен");

                if (mongoAccountInfo.containsKey("StatusReason") && mongoAccountInfo.get("StatusReason") != null)
                    assertEquals(CONST_API + getAccountId() + "].account.statusReason",
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.account.statusReason"), ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.StatusReason"),
                            "[" + getAccountId() + "].account.statusReason не корректен");
            }
            Date mongoCreatedTime = new Date(Long.parseLong(ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.Created.$date")));
            assertEquals(CONST_API + getAccountId() + "].account.created",
                    ReplyToJSON.extractPathFromJson(apiAccount, "$.account.created"), df.format(mongoCreatedTime),
                    "[" + getAccountId() + "].account.created не корректен");

            assertContainsMngId(CONST_API_ACC + getAccountId() + CONST_CUSTOMERID, ReplyToJSON.extractPathFromJson(apiAccount, "$.account.customerId"), CONST_CUSTOMERID_WRONG);

//		"isOnline": false,
            String accountMongoStrTimeZone = ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.Settings.SystemSettings.Timezone");
            assertEquals(CONST_API + getAccountId() + "].account.timezoneOffset",
                    ReplyToJSON.extractPathFromJson(apiAccount, "$.account.timezoneOffset"), getUTCTimezoneFromString(accountMongoStrTimeZone).get("timeOffset"),
                    "[" + getAccountId() + "].account.timezoneOffset не корректен");

            //TODO: actually this is duplicate, should be moved to separate method/.
            //checking personal profile:
            JSONObject mongoAccountProfile = (JSONObject) mongoAccountInfo.get("Profile");
            if (mongoAccountProfile != null) {

                assertEquals(CONST_API_ACC + getAccountId() + "].profile.firstname",
                        ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.firstname"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, "$.Firstname"),
                        CONST_ACCOUNTS_STR + getAccountId() + "].profile.firstname не корректен");
                if (mongoAccountProfile.containsKey(CONST_MIDDLENAME) && ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_MIDDLENAME) != null) {
                    log.debug("==" + ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_MIDDLENAME));

                    assertEquals(CONST_API_ACC + getAccountId() + "].profile.middlename",
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.middlename"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_MIDDLENAME),
                            CONST_ACCOUNTS_STR + getAccountId() + "].profile.middlename не корректен");
                }
                assertEquals(CONST_API_ACC + getAccountId() + "].profile.lastname",
                        ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.lastname"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_LASTNAME),
                        CONST_ACCOUNTS_STR + getAccountId() + "].profile.lastname не корректен");
                if (ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_SEX).equals("0")) {
                    assertEquals(CONST_API_ACC + getAccountId() + CONST_PROFILE_SEX,
                            ReplyToJSON.extractPathFromJson(apiAccount, PATH_PROFILESEX), "Male",
                            CONST_ACCOUNTS_STR + getAccountId() + CONST_PROFILE_SEX_WRONG);
                } else {
                    assertEquals(CONST_API_ACC + getAccountId() + CONST_PROFILE_SEX,
                            ReplyToJSON.extractPathFromJson(apiAccount, PATH_PROFILESEX), ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_SEX),
                            CONST_ACCOUNTS_STR + getAccountId() + CONST_PROFILE_SEX_WRONG);
                }
                if (mongoAccountProfile.containsKey(CONST_AVATAR_URI) && ReplyToJSON.extractPathFromJson(mongoAccountProfile, "$.CONST_AVATAR_URI") != null)
                    assertEquals(CONST_API_ACC + getAccountId() + CONST_BR_PROFILE_AVATAR_URI,
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.avatarUri"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, "$.CONST_AVATAR_URI"),
                            CONST_ACCOUNTS_STR + getAccountId() + "].profile.avatarUri не корректен");
                if (mongoAccountProfile.containsKey(CONST_BIRTH) && mongoAccountProfile.get(CONST_BIRTH) != null)
                    assertEquals(CONST_API_ACC + getAccountId() + CONST_BR_PROFILE_AVATAR_URI,
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.birth"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, "$.Birth"),
                            CONST_ACCOUNTS_STR + getAccountId() + "].profile.birth не корректен");
                if (mongoAccountProfile.containsKey(CONST_DEPATMENID) && mongoAccountProfile.get(CONST_DEPATMENID) != null)
                    assertEquals(CONST_API_ACC + getAccountId() + "].profile.departmentId",
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.departmentId"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_DEPARTMENTID),
                            CONST_ACCOUNTS_STR + getAccountId() + "].profile.departmentId не корректен");
                if (mongoAccountProfile.containsKey(CONST_EMPLOYEEID) && mongoAccountProfile.get(CONST_EMPLOYEEID) != null)
                    assertEquals(CONST_API_ACC + getAccountId() + "].profile.employeeId",
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.employeeId"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, "$.EmployeeId.$oid"),
                            CONST_ACCOUNTS_STR + getAccountId() + "].profile.employeeId не корректен");

                assertEquals(CONST_API_ACC + getAccountId() + "].profile.name",
                        ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.name"),
                        ReplyToJSON.extractPathFromJson(mongoAccountInfo, PATH_FIRSTNAME) + " " + ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_LASTNAME),
                        CONST_ACCOUNTS_STR + getAccountId() + "].profile.name не корректен");

                if (mongoAccountProfile.containsKey(CONST_DEPATMENID) && mongoAccountProfile.get(CONST_DEPATMENID) != null) {

                    assertContainsMngId(CONST_API + getAccountId() + "].department.id", ReplyToJSON.extractPathFromJson(apiAccount, "$.department.id"), "department.id не верен");
                    assertEquals(CONST_API_ACC + getAccountId() + "].department.id",
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.department.id"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_DEPARTMENTID),
                            CONST_ACCOUNTS_STR + getAccountId() + "].department.id не корректен");
                    assertNotNull(CONST_API + getAccountId() + "].department.name", ReplyToJSON.extractPathFromJson(apiAccount, "$.department.name"), "поле [" + getAccountId() + "].department.name не должно быть пустым");
//				assertEquals(CONST_API_ACC+accountId+"].department.name", 
//						ReplyToJSON.extractPathFromJson(apiAccount, "$.department.name"), /*get here department name?*/"",
//						  CONST_ACCOUNTS_STR+accountId+"].department.name не корректен");
                }


            }
            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
            assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные аккаунта (AccountProfileViewModel):" + e.toString());
        }
    }

    @SuppressWarnings("unchecked")
    @Step("получение всех структур аккаунтов")
    public List<org.json.simple.JSONObject> getAllAccountsWithStructureFromAdminPanel() {
        apiMethod = "/Api/Admin/Account";
        url = getProperty(CONST_PANEL_HOST) + apiMethod;
        List<org.json.simple.JSONObject> apiAccounts = null;
        List<org.json.simple.JSONObject> mongoAccountsInfo = null;
        try {
            setToken(getCurAuthAccToken(CONST_AUTHT1ADMIN));
            assertTrue(CONST_TOKEN_RECEIVED, getToken() != null, CONST_AUTH_NEEDED);
            JSONObject apiReply = (JSONObject) SendRequestToAPI.sendGetRequest(url, "GET", "", "", getToken()).get(0);
            apiAccounts = (List<org.json.simple.JSONObject>) apiReply.get(CONST_ACCOUNTS);

            mongoAccountsInfo = GetAccountInfoFromMongo.getAllAccounts();
            List<org.json.simple.JSONObject> mongoRolesInfo = GetAccountInfoFromMongo.getAllRoles();
            List<org.json.simple.JSONObject> mongoCustomersInfo = GetCustomerInfoFromMongo.getAllCustomers();

            for (int i = 0; i < apiAccounts.size(); i++) {
                JSONObject mongoAccountInfo = new JSONObject();
                String apiAccountId = ReplyToJSON.extractPathFromJson(apiAccounts.get(i), "$.id");
                for (int j = 0; j < mongoAccountsInfo.size(); j++) {
                    if (ReplyToJSON.extractPathFromJson(mongoAccountsInfo.get(j), PATH_ID).matches(apiAccountId)) {
                        mongoAccountInfo = mongoAccountsInfo.get(j);
                        break;
                    }
                }
                List<String> mongoAccountRolesIds = ReplyToJSON.extractListPathFromJson(mongoAccountInfo, "$.Roles..$oid");
                List<JSONObject> rolesWithPermissions = new ArrayList<JSONObject>();
                for (String roleId : mongoAccountRolesIds) {
                    JSONObject roleWithPermissions = new JSONObject();
                    for (JSONObject mongoRole : mongoRolesInfo) {
                        if (ReplyToJSON.extractPathFromJson(mongoRole, PATH_ID).matches(roleId)) {
                            roleWithPermissions.put("id", roleId);
                            roleWithPermissions.put("name", mongoRole.get("Name"));
                            roleWithPermissions.put("permissions", mongoRole.get("Permissions"));
                        }
                    }
                    rolesWithPermissions.add(roleWithPermissions);
                }

                String mongoCustomerId = ReplyToJSON.extractPathFromJson(mongoAccountInfo, PATH_CUSTIMERID);
                JSONObject customerInfo = new JSONObject();
                for (JSONObject mongoCustomerInfo : mongoCustomersInfo) {
                    if (ReplyToJSON.extractPathFromJson(mongoCustomerInfo, PATH_ID).matches(mongoCustomerId)) {
                        customerInfo = mongoCustomerInfo;
                        break;
                    }
                }
                checkAccountStructureReturned(mongoAccountInfo, apiAccounts.get(i), customerInfo, rolesWithPermissions);
            }


        } catch (Exception e) {
            log.error(ERROR, e);
            assertTrue(false, e.toString());

        }

        return apiAccounts;
    }

    //TODO: not all assertions implemented since methods don't return all data!
    private void checkAccountStructureReturned(JSONObject mongoAccountInfo, JSONObject apiAccount, JSONObject customerInfo, List<JSONObject> mongoRolesWithPermissions) {
        log.debug("start check");
        JSONObject rolesWithPermissionsObj = new JSONObject();
        List<String> mongoRolePermissionsNames = new ArrayList();
        rolesWithPermissionsObj.put("roles", mongoRolesWithPermissions);
        try {
            accountId = ReplyToJSON.extractPathFromJson(apiAccount, "$.id");
            assertContainsMngId(CONST_API_ACC + accountId + "].id", accountId, "account id не верен");
            assertContainsMngId(CONST_API_ACC + accountId + CONST_CUSTOMERID, ReplyToJSON.extractPathFromJson(apiAccount, "$.customerId"), CONST_CUSTOMERID_WRONG);

//			List<JSONObject> mongoAccountsContacts = ReplyToJSON.extractListPathFromJson(mongoAccountInfo, "$.Contacts");
            //T1-3709 this field has been removed .
//			if(mongoAccountsContacts.size()>0){
//				if(mongoAccountsContacts.get(0).get("Confirmed")!= null||mongoAccountsContacts.get(1).get("Confirmed")!= null){
//			assertEquals(CONST_API_ACC+accountId+"].enabled", 
//					ReplyToJSON.extractPathFromJson(apiAccount, "$.enabled") , "true",
//					CONST_ACCOUNTS_STR+accountId+"].roles.enabled не корректен");
//				}
//			}

            //start checking profile
            // profile can be corporate! :			Корпоративный профиль (CompanyProfile)
            //checking personal profile:
            JSONObject mongoAccountProfile = (JSONObject) mongoAccountInfo.get("Profile");
            if (mongoAccountProfile != null) {
                assertEquals(CONST_API_ACC + accountId + "].profile.firstname",
                        ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.firstname"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, "$.Firstname"),
                        CONST_ACCOUNTS_STR + accountId + "].profile.firstname не корректен");
                if (mongoAccountProfile.containsKey(CONST_MIDDLENAME) && mongoAccountProfile.get(CONST_MIDDLENAME) != null)
                    assertEquals(CONST_API_ACC + accountId + "].profile.middlename",
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.middlename"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_MIDDLENAME),
                            CONST_ACCOUNTS_STR + accountId + "].profile.middlename не корректен");
                assertEquals(CONST_API_ACC + accountId + "].profile.lastname",
                        ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.lastname"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_LASTNAME),
                        CONST_ACCOUNTS_STR + accountId + "].profile.lastname не корректен");
                if (ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_SEX).equals("0")) {
                    assertEquals(CONST_API_ACC + accountId + CONST_PROFILE_SEX,
                            ReplyToJSON.extractPathFromJson(apiAccount, PATH_PROFILESEX), "Male",
                            CONST_ACCOUNTS_STR + accountId + CONST_PROFILE_SEX_WRONG);
                } else {
                    assertEquals(CONST_API_ACC + accountId + CONST_PROFILE_SEX,
                            ReplyToJSON.extractPathFromJson(apiAccount, PATH_PROFILESEX), ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_SEX),
                            CONST_ACCOUNTS_STR + accountId + CONST_PROFILE_SEX_WRONG);
                }
                if (mongoAccountProfile.containsKey(CONST_AVATAR_URI))
                    assertEquals(CONST_API_ACC + accountId + CONST_BR_PROFILE_AVATAR_URI,
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.avatarUri"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, "$.AvatarUri"),
                            CONST_ACCOUNTS_STR + accountId + "].profile.avatarUri не корректен");
                if (mongoAccountProfile.containsKey(CONST_BIRTH) && mongoAccountProfile.get(CONST_BIRTH) != null)
                    assertEquals(CONST_API_ACC + accountId + CONST_BR_PROFILE_AVATAR_URI,
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.birth"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, "$.Birth"),
                            CONST_ACCOUNTS_STR + accountId + "].profile.birth не корректен");
                if (mongoAccountProfile.containsKey(CONST_DEPATMENID) && mongoAccountProfile.get(CONST_DEPATMENID) != null)
                    assertEquals(CONST_API_ACC + accountId + "].profile.departmentId",
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.departmentId"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_DEPARTMENTID),
                            CONST_ACCOUNTS_STR + accountId + "].profile.departmentId не корректен");
                if (mongoAccountProfile.containsKey(CONST_EMPLOYEEID) && mongoAccountProfile.get(CONST_EMPLOYEEID) != null)
                    assertEquals(CONST_API_ACC + accountId + "].profile.employeeId",
                            ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.employeeId"), ReplyToJSON.extractPathFromJson(mongoAccountProfile, "$.EmployeeId.$oid"),
                            CONST_ACCOUNTS_STR + accountId + "].profile.employeeId не корректен");

                assertEquals(CONST_API_ACC + accountId + "].profile.name",
                        ReplyToJSON.extractPathFromJson(apiAccount, "$.profile.name"),
                        ReplyToJSON.extractPathFromJson(mongoAccountInfo, PATH_FIRSTNAME) + " " + ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_LASTNAME),
                        CONST_ACCOUNTS_STR + accountId + "].profile.name не корректен");

                assertEquals(CONST_API_ACC + accountId + "].name",
                        ReplyToJSON.extractPathFromJson(apiAccount, PATH_NAME),
                        ReplyToJSON.extractPathFromJson(mongoAccountInfo, PATH_FIRSTNAME) + " " + ReplyToJSON.extractPathFromJson(mongoAccountProfile, PATH_LASTNAME),
                        CONST_ACCOUNTS_STR + accountId + "].name не корректен");
            } else {
                assertEquals(CONST_API_ACC + accountId + "].name",
                        ReplyToJSON.extractPathFromJson(apiAccount, PATH_NAME), "", CONST_ACCOUNTS_STR + accountId + "].name не корректен");
            }
            //start checking customer
            if (customerInfo != null && !customerInfo.keySet().isEmpty()) {
                assertEquals(CONST_API_ACC + accountId + "].customer.address", ReplyToJSON.extractPathFromJson(apiAccount, "$.customer.address"), customerInfo.get("Address"),
                        CONST_ACCOUNTS_STR + accountId + "].customer.address не корректен");
                assertEquals(CONST_API_ACC + accountId + "].customer.phone", ReplyToJSON.extractPathFromJson(apiAccount, "$.customer.phone"), customerInfo.get("Phone"),
                        CONST_ACCOUNTS_STR + accountId + "].customer.phone не корректен");
                assertEquals(CONST_API_ACC + accountId + "].customer.name", ReplyToJSON.extractPathFromJson(apiAccount, "$.customer.name"), customerInfo.get("Name"),
                        CONST_ACCOUNTS_STR + accountId + "].customer.name не корректен");
                assertEquals(CONST_API_ACC + accountId + "].customer.description", ReplyToJSON.extractPathFromJson(apiAccount, "$.customer.description"), customerInfo.get("Description"),
                        CONST_ACCOUNTS_STR + accountId + "].customer.description не корректен");

                assertEquals(CONST_API_ACC + accountId + "].customer.id", ReplyToJSON.extractPathFromJson(apiAccount, "$.customer.id"), ReplyToJSON.extractPathFromJson(customerInfo, PATH_ID),
                        CONST_ACCOUNTS_STR + accountId + "].customer.id не корректен");

                assertEquals(CONST_API_ACC + accountId + "].customer.email", ReplyToJSON.extractPathFromJson(apiAccount, "$.customer.email"), customerInfo.get("Email"),
                        CONST_ACCOUNTS_STR + accountId + "].customer.email не корректен");
            } else {
                assertEquals(CONST_API_ACC + accountId + CONST_CUSTOMERID, ReplyToJSON.extractPathFromJson(apiAccount, "$.customerId"), "000000000000000000000000",
                        CONST_CUSTOMERID_WRONG);
            }

            assertArrayEquals(CONST_ACCOUNTS_STR + accountId + "].roles",
                    ReplyToJSON.extractListPathFromJson(apiAccount, "$.roles[*].id").toArray(), ReplyToJSON.extractListPathFromJson(rolesWithPermissionsObj, "$.roles[*].id").toArray(),
                    CONST_ACCOUNTS_STR + accountId + "].roles.ids не корректен");
            assertArrayEquals(CONST_ACCOUNTS_STR + accountId + "].roles.name",
                    ReplyToJSON.extractListPathFromJson(apiAccount, "$.roles[*].name").toArray(), ReplyToJSON.extractListPathFromJson(rolesWithPermissionsObj, "$.roles[*].name").toArray(),
                    CONST_ACCOUNTS_STR + accountId + "].roles.names не корректен");
//			check permissions for each role
            List<JSONObject> apiAccountRoles = ReplyToJSON.extractListPathFromJson(apiAccount, "$.roles");
            for (JSONObject mongoRole : mongoRolesWithPermissions) {
                log.debug("mongoRole==" + mongoRole);
                //find same role in api reply for checking account
                for (JSONObject apiAccountRole : apiAccountRoles) {
                    if (mongoRole.get("id").toString().matches(apiAccountRole.get("id").toString())) {
                        assertEquals(CONST_API_ACC + accountId + "].roles.id",
                                ReplyToJSON.extractPathFromJson(apiAccountRole, "$.id"), ReplyToJSON.extractPathFromJson(mongoRole, "$.id"),
                                CONST_ACCOUNTS_STR + accountId + "]..roles.id не корректен");
                        assertEquals(CONST_API_ACC + accountId + "].roles.name",
                                ReplyToJSON.extractPathFromJson(apiAccountRole, PATH_NAME), ReplyToJSON.extractPathFromJson(mongoRole, PATH_NAME),
                                CONST_ACCOUNTS_STR + accountId + "].roles.name не корректен");
//						convert mongo permission numbers to names
                        List<Long> mongoRolePermissions = ReplyToJSON.extractListPathFromJson(mongoRole, PATH_PERMISSIONS);

                        for (Long permission : mongoRolePermissions) {
                            mongoRolePermissionsNames.add(permissionsMap.get(permission.intValue()).toString());
                        }
                        //duplicated permissions field. can account hold 2 roles? by UI - not, but we have a list of roles. if account has 2 roles should permissions be joined or concatenated here?
                        assertArrayEquals(CONST_API_ACC + accountId + "].roles.permissions",
                                ReplyToJSON.extractListPathFromJson(apiAccountRole, PATH_PERMISSIONS).toArray(), mongoRolePermissionsNames.toArray(),
                                CONST_ACCOUNTS_STR + accountId + "].roles.permissions не корректено");
                    }
                }

            }
            assertArrayEquals(CONST_API_ACC + accountId + "].permissions",
                    ReplyToJSON.extractListPathFromJson(apiAccount, PATH_PERMISSIONS).toArray(), mongoRolePermissionsNames.toArray(),
                    CONST_ACCOUNTS_STR + accountId + "].permissions не корректено");

            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
            assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные структуры аккаунта:" + e.toString());
        }

    }

    @Step("получение структуры аккаунта")
    public org.json.simple.JSONObject getAccountStructureFromAdminPanel() {
        apiMethod = "/Api/Admin/Account/" + this.getAccountId();
        url = getProperty(CONST_PANEL_HOST) + apiMethod;
        JSONObject apiAccount = null;
        List<org.json.simple.JSONObject> mongoAccountInfo = null;
        try {
            setToken(getCurAuthAccToken(CONST_AUTHT1ADMIN));
            assertTrue(CONST_TOKEN_RECEIVED, getToken() != null, CONST_AUTH_NEEDED);
            JSONObject apiReply = (JSONObject) SendRequestToAPI.sendGetRequest(url, "GET", "", "", getToken()).get(0);
            apiAccount = (JSONObject) apiReply.get("result");

            mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(this.getAccountId(), "");
            List<org.json.simple.JSONObject> mongoRolesInfo = GetAccountInfoFromMongo.getAllRoles();

            String mongoCustomerId = ReplyToJSON.extractPathFromJson(mongoAccountInfo.get(0), PATH_CUSTIMERID);
            JSONObject customerInfo = (JSONObject) GetCustomerInfoFromMongo.getCustomerInfoById(mongoCustomerId).get(0);

            List<String> mongoAccountRolesIds = ReplyToJSON.extractListPathFromJson(mongoAccountInfo.get(0), "$.Roles..$oid");
            List<JSONObject> rolesWithPermissions = new ArrayList<JSONObject>();
            for (String roleId : mongoAccountRolesIds) {
                JSONObject roleWithPermissions = new JSONObject();
                for (JSONObject mongoRole : mongoRolesInfo) {
                    if (ReplyToJSON.extractPathFromJson(mongoRole, PATH_ID).matches(roleId)) {
                        roleWithPermissions.put("id", roleId);
                        roleWithPermissions.put("name", mongoRole.get("Name"));
                        roleWithPermissions.put("permissions", mongoRole.get("Permissions"));
                    }
                }
                rolesWithPermissions.add(roleWithPermissions);
            }

//			// compare mongo with input
            checkAccountStructureReturned(mongoAccountInfo.get(0), apiAccount, customerInfo, rolesWithPermissions);
        } catch (Exception e) {
            log.error(ERROR, e);
            assertTrue(false, e.toString());

        }

        return apiAccount;
    }

    @Step("Валидация:\r\nподтверждение регистрации: получен токен")
    public void checkRegistrationConfirmationToken(HashMap<String, Object> response) {
        assertNotNull("account_token", response.get(CONST_TOKEN));
    }

    @Step("Валидация: полученный JSON содержит данные с которыми аккаунт был запрошен")
    public void checkRegistredAccount(JSONObject jsonReplyMongo, JSONObject jsonReplyAPI, JSONObject initMap) {
        try {
            if (initMap.get(CONST_FIRSTNAME) != null) {
                assertEquals("account_firstname", ReplyToJSON.extractPathFromJson(jsonReplyMongo, PATH_FIRSTNAME), this.firstname, "firstname не верен");
            }
            if (initMap.get(CONST_LASTNAME) != null) {
                assertEquals("account_lastname", ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Profile.Lastname"), this.lastname, "lastname не верен");
            }
            if (initMap.get(CONST_LOGIN) != null && initMap.get(CONST_LOGIN) != "") {
                assertEquals("account_login", ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Login"), this.getLogin(), "login не верен");
            }
            if (initMap.get(CONST_ACCOUNTCONST_PHONE) != null && initMap.get(CONST_ACCOUNTCONST_PHONE) != "") {
                if (ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Contacts[0].Type").equals(CONST_PHONE)) {
                    assertEquals("account_phone", ReplyToJSON.extractPathFromJson(jsonReplyMongo, PATH_CONTACTS), this.getPhone(), "phone не верен");
                } else
                    assertEquals("account_phone", ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Contacts[1].Value"), this.getPhone(), "phone не верен");
            }
            if (initMap.get(CONST_EMAIL) != null) {
                assertEquals("account_email", ReplyToJSON.extractPathFromJson(jsonReplyMongo, PATH_CONTACTS), this.getEmail(), "email не верен");
            }

            if (jsonReplyAPI != null && jsonReplyAPI.containsKey("role")) {
                assertEquals(CONST_ACC_ROLE, ReplyToJSON.extractPathFromJson(jsonReplyAPI, "$.role"), "Demo", "role не верен");
            }
            String roleId = ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Roles[0].$oid");
            Bson filter = Filters.eq("_id", new ObjectId(roleId));
            JSONObject returnedValue = (JSONObject) GetInfoFromMongo.getInfoByCustomFilters("AccountRoles", filter, Arrays.asList("_id", "Name")).get(0);
            if (initMap.containsKey(CONST_ACC_ROLE)) {
                assertEquals("account_Role", returnedValue.get("Name"), initMap.get(CONST_ACC_ROLE), "account_Role не верен");
            } else {
                assertEquals("account_Role", returnedValue.get("Name"), "Demo", "account_Role не верен");
            }
            if (initMap.get(CONST_PROFILE_AVATAR_URI) != null) {
                assertContains("account_profileAvatarUri", ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Profile.AvatarUri"), initMap.get(CONST_PROFILE_AVATAR_URI).toString(),
                        "profileAvatarUri не верен");
            }
            if (initMap.get(CONST_MIDDLENAME) != null) {
                assertEquals("account_profile_middleName", ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Profile.Middlename"), initMap.get(CONST_MIDDLENAME), "profile_middleName не верен");
            }
            if (initMap.get("sex") != null) {
                assertEquals("account_Sex", ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Profile.Sex"), initMap.get("sex"), "account_Sex не верен");
            }

            String containers = SharedContainer.getContainers().keySet().toString();
            if (containers.contains("Token") || containers.contains(CONST_TOKEN)) {
                if (containers.contains("customer")) {
                    assertEquals(CONST_ACCOUNT_CUSTOMERID, ReplyToJSON.extractPathFromJson(jsonReplyMongo, PATH_CUSTIMERID),
                            SharedContainer.getFromContainer("customer").get(0), CONST_ACCOUNT_CUSTOMERID_WRONG);
                } else {
                    assertEquals(CONST_ACCOUNT_CUSTOMERID, ReplyToJSON.extractPathFromJson(jsonReplyMongo, PATH_CUSTIMERID),
                            getProperty("testCustomerId"), CONST_ACCOUNT_CUSTOMERID_WRONG);
                }
            } else {
                assertEquals(CONST_ACCOUNT_CUSTOMERID, ReplyToJSON.extractPathFromJson(jsonReplyMongo, PATH_CUSTIMERID),
                        "000000000000000000000000", CONST_ACCOUNT_CUSTOMERID_WRONG);
            }

            if (initMap.get(CONST_ACCOUNTTYPE) != null) {
                assertEquals(CONST_ACCOUNTTYPE, jsonReplyMongo.get("Type"), initMap.get(CONST_ACCOUNTTYPE).toString(),
                        "account_type не верен");
            }

            if (initMap.get(CONST_PASS) != null) {
                assertTrue("account_password", ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Password") != null,
                        "account_password не верен");
            }
            if (initMap.get(CONST_DEPATMENID2) != null) {
                try {
                    assertEquals("account_DepartmentId", ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Profile.DepartmentId.$oid"), initMap.get(CONST_DEPATMENID2).toString(),
                            "account_DepartmentId не верен");

                    if (initMap.get(CONST_EMPLOYEEID2) != null) {
                        assertEquals("account_EmployeeId", ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Profile.EmployeeId.$oid"), initMap.get(CONST_EMPLOYEEID2).toString(),
                                "account_EmployeeId не верен");
                    }
                } catch(PathNotFoundException | AssertionError e){
                    Logger.getLogger(">>>").info(e.getMessage());
                }
            }
//			not implemented?
//			assertEquals("account_AdminCreated",ReplyToJSON.extractPathFromJson(jsonReplyMongo, "$.Profile.AdminCreated"), false,
//							"account_AdminCreated не верен");

            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
            assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные новогого аккаунта:" + e.toString());
        }
    }


    @Step("Валидация:\r\nподтверждение регистрации: данные MongoDB обновились")
    public void checkRegistrationConfirmation() throws Exception {
        List<org.json.simple.JSONObject> mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(this.getAccountId(), "");
        this.accountStatus = JsonPath.read(mongoAccountInfo.get(0), PATH_STATUS);
        assertEquals(CONST_ACCOUNT_ACTIVE, accountStatus, CONST_ACTIVE, CONST_SHOUD_BE_ACTIVE);
        this.accountMailCofirmed = ReplyToJSON.extractPathFromJson(mongoAccountInfo.get(0), "$.Contacts[0].Confirmed");//mail
        if (this.getPhone() != null) {
            this.accountPhoneCofirmed = ReplyToJSON.extractPathFromJson(mongoAccountInfo.get(0), "$.Contacts[1].Confirmed");//phone
        }
        assertTrue("account_accountMailCofirmed", (this.accountMailCofirmed != null && this.accountMailCofirmed != "") || (this.accountPhoneCofirmed != null && this.accountPhoneCofirmed != "")
                , "после подтверждения регистрации phone или email должен содержать дату подтверждения регистрации");

        JSONObject accountConfirmationInfo = GetAccountInfoFromMongo.getAccountConfirmationInfoFromMongo(this.getAccountId(),
                this.getPhone(), this.getEmail());
        assertTrue(CONST_ACCOUNTUSED, ReplyToJSON.extractPathFromJson(accountConfirmationInfo, PATH_USED).equals("true"),
                "аккаунт не мог быть использован до подтверждения регистрации");
        // compare mongo with input
        checkRegistredAccount(mongoAccountInfo.get(0), null, this.initMap);
        setToAllureChechkedFields();
    }

    private void checkphoneUpdateRequestReply(JSONObject phoneUpdateReply, JSONObject mongoAccountInfo) {
        try {
            assertContainsMngId("API:accountPhoneUpdate_accountId", ReplyToJSON.extractPathFromJson(phoneUpdateReply, "$.accountId"), CONST_ACCID_WRONG);
            assertContainsMngId("API:accountPhoneUpdate_confirmationId", ReplyToJSON.extractPathFromJson(phoneUpdateReply, "$.confirmationId"), "accountPhoneUpdate_confirmationId не верен");
            assertEquals("API:accountPhoneUpdate_accountId", phoneUpdateReply.get(CONST_ACCOUNTID), this.getAccountId(), CONST_ACCID_WRONG);
            assertEquals("DB:accountPhoneUpdate_accountId", phoneUpdateReply.get(CONST_ACCOUNTID), ReplyToJSON.extractPathFromJson(mongoAccountInfo, PATH_ID), CONST_ACCID_WRONG);
            if (this.getConfirmationId() != null) {
                assertEquals("API:accountPhoneUpdate_confirmationId", phoneUpdateReply.get(CONST_CONFIRMATIONID), this.getConfirmationId(), "accountPhoneUpdate_confirmationId не верен");
            }
            assertEquals("API:accountPhoneUpdate_contact.type",
                    ReplyToJSON.extractPathFromJson(phoneUpdateReply, "$.contact.type").toLowerCase(), CONST_PHONE, "accountPhoneUpdate_contact.type не верен");
            assertEquals("API:accountPhoneUpdate_contact.value",
                    ReplyToJSON.extractPathFromJson(phoneUpdateReply, "$.contact.value"), this.getPhone(), CONST_CONTACT_WRONG);
            if (ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.Contacts[0].Type").equals(CONST_PHONE)) {
                assertEquals("DB:accountPhoneUpdate_contact.value", ReplyToJSON.extractPathFromJson(phoneUpdateReply, "$.contact.value"),
                        ReplyToJSON.extractPathFromJson(mongoAccountInfo, PATH_CONTACTS),
                        CONST_CONTACT_WRONG);
            } else {
                assertEquals("DB:accountPhoneUpdate_contact.value", ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.Contacts[1].Value"),
                        //ReplyToJSON.extractPathFromJson(phoneUpdateReply, "$.contact.value"),
                        this.getOldPhone(), CONST_CONTACT_WRONG);
            }

        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
            assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные при обновлении номера телефона аккаунта:" + e.toString());
        }
    }

    @Step("Валидация:\r\nВыход из аккаунта был произведен")
    public void checkLoggedOut(HashMap<String, Object> response) {
        assertTrue("account_messageBody", response.get("messageBody").equals("true"), "выход из аккаунта не был произведен");
        setToAllureChechkedFields();
    }

    ///////////////////////////////DELETE
    @Step("delete account  {0}")
    public HashMap accountDelete() {
        String devToken = "";
        try {
            if ((!SharedContainer.getContainers().containsKey(CONST_DEVTOKEN)) || SharedContainer.getFromContainer(CONST_DEVTOKEN).get(0) == "") {
                setToken(getCurAuthAccToken(CONST_AUTHT1ADMIN));
                SharedContainer.setContainer(CONST_DEVTOKEN, devToken);
            } else {
                devToken = SharedContainer.getFromContainer(CONST_DEVTOKEN).get(0).toString();
            }
            // delete account
            url = getProperty("host") + "/account/delete";
            String postParameters = CONST_ACCOUNTID_STR + this.getAccountId() + "\" }";

            JSONObject deletedReply = SendRequestToAPI.sendPostRequest(url, postParameters, devToken, false, true);
            checkAccountDeletedSuccessfullyMongoDB(this.getAccountId());
            log.info(CONST_DELETEREPLY + deletedReply);

            if ((deletedReply.containsKey("code")) && (deletedReply.get("code").equals("Ok"))) {
                TearDownExecutor.removeEntityFromTeardown(this.getAccountId(), CONST_ACCOUNTS);
                SharedContainer.removeFromContainer(CONST_ACCOUNTS, this.getAccountId());
            }
            setToAllureChechkedFields();
            return deletedReply;

        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
        }
        return null;
    }

    @Step("delete account  {0}")
    public HashMap removeAccountInAdminPanel() {
        try {

            setToken(getCurAuthAccToken(CONST_AUTHT1ADMIN, "Developer"));
            // delete account
            url = getProperty(CONST_PANEL_HOST) + "/Api/Admin/Account/Remove";
            String postParameters = CONST_ACCOUNTID_STR + this.getAccountId() + "\" }";

            JSONObject deletedReply = SendRequestToAPI.sendPostRequest(url, postParameters, getToken(), false, true);
            checkAccountHasStateDeletedMongoDB(this.getAccountId());
            log.info(CONST_DELETEREPLY + deletedReply);
            setToAllureChechkedFields();
            return deletedReply;
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
        }
        return null;
    }

    @Step("remove account  {0}")
    public HashMap removeAccountFromProfile() {
        try {
            setToken(getCurAuthAccToken(CONST_AUTHT1CLIENT));

            // delete account
            //Права доступа: обычный пользователь - для своего профиля; Администратор – для сотрудников, принадлежащих Клиенту, Developer –для любого аккаунта.
            url = getProperty("host") + "/Account/Remove";
            String postParameters = CONST_ACCOUNTID_STR + this.getAccountId() + "\" }";

            JSONObject deletedReply = SendRequestToAPI.sendPostRequest(url, postParameters, getToken(), false, true);
            checkAccountHasStateDeletedMongoDB(this.getAccountId());
            log.info(CONST_DELETEREPLY + deletedReply);
            setToAllureChechkedFields();
            return deletedReply;

        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
        }
        return null;
    }

    @Step("Проверка: аккаунт был успешно удален")
    private static void checkAccountDeletedSuccessfullyMongoDB(String accountId) {
        List<org.json.simple.JSONObject> deletedAccount = new GetAccountInfoFromMongo().getAccountInfoFromMongo(accountId);
        assertEquals("DB: check account deleted", deletedAccount.size(), 0, "была найдена информация об аккунте:" + deletedAccount.toString());
    }

    @SuppressWarnings("rawtypes")
    @Step("Проверка: аккаунт получил статус удален")
    private static void checkAccountHasStateDeletedMongoDB(String accountId) {
        org.json.simple.JSONObject deletedAccount = new GetAccountInfoFromMongo().getAccountInfoFromMongo(accountId).get(0);
        assertEquals("deleted_account.status", deletedAccount.get("Status").toString(), "Deleted", "некорректный статус аккаунта:" + deletedAccount.toString());
        setToAllureChechkedFields();
    }

    @Override
    public int removeFromSharedContainer() {
        int indexOfRemovedEntity = -1;
        if (!container.isEmpty() && container.containsKey(CONST_ACCOUNTS)) {
            ArrayList listOfIds = container.get(CONST_ACCOUNTS);
            if ((!listOfIds.isEmpty()) &&
                    (listOfIds.get(0) instanceof com.t1.core.api.AccountRegistrationAuth)) {
                for (int i = 0; i < listOfIds.size(); i++) {
                    if (((AccountRegistrationAuth) listOfIds.get(i)).getAccountId().equals(this.getAccountId())) {
                        listOfIds.remove(i);
                        indexOfRemovedEntity = i;
                    }
                }

            }
        }
        return indexOfRemovedEntity;
    }

    @Override
    public void addToSharedContainer() {
        addToSharedContainer(-1);
    }

    @Override
    public void addToSharedContainer(int index) {
        if (SharedContainerInterface.getContainers().containsKey(CONST_ACCOUNTS) && !SharedContainerInterface.getContainers().get(CONST_ACCOUNTS).isEmpty()) {
            ArrayList existingArray = SharedContainerInterface.getContainers().get(CONST_ACCOUNTS);
            if (index >= 0) {
                existingArray.add(index, this);
            } else {
                existingArray.add(this);
            }
            container.put(CONST_ACCOUNTS, existingArray);
        } else {
            ArrayList newArray = new ArrayList();
            newArray.add(this);
            container.put(CONST_ACCOUNTS, newArray);
        }
    }


    @Override
    public void updateInSharedContainer() {
        int index = this.removeFromSharedContainer();
        this.addToSharedContainer(index);
    }


}
