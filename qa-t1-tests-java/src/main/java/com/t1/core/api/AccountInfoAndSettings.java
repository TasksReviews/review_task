package com.t1.core.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.jayway.jsonpath.JsonPath;
import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.mongodb.GetAccountInfoFromMongo;

public class AccountInfoAndSettings extends AbstractClass {

	private String token;

	public List<JSONObject> getAllTimeZones() {
		String url = getProperty("host") +"/Timezones";
		
		List<org.json.simple.JSONObject> timeZonesList = null;
		try {
			token = getToken("userToken");
			assertTrue("token received",token != null,"для получения ТС необходимо пройти авторизацию");
			 timeZonesList = SendRequestToAPI.sendGetRequest( url,"GET", "","", token);
			 checkTimeZonesList(timeZonesList);
		} catch (Exception e) {
			log.error("Test failed: " + e);
			assertTrue(false, e.toString());

		}
		return timeZonesList;
	}

	private void checkTimeZonesList(List<JSONObject> timeZonesList) {
		try {

		for(JSONObject timezone : timeZonesList){
			String expectedOffset = "0";
			String timeZoneId=	timezone.get("id").toString();
			String timeZoneTitle=	timezone.get("title").toString();
			Integer timeZoneOffset=	Integer.parseInt(timezone.get("offset").toString());
			assertNotNull("id", timeZoneId, "поле timeZoneId не должно быть пустым");
			assertContains("API:title", timeZoneTitle, "UTC", "title не содержит часовой пояс");
			String strtime = timeZoneTitle.substring(timeZoneTitle.indexOf("UTC")+3, timeZoneTitle.indexOf(')'));
			if(strtime.length()>4){
			String strUTCTimezone =	getUTCTimezoneFromString(timeZoneId).get("timeZone");
			assertContains("check title", timeZoneTitle, "(UTC"+strUTCTimezone+")","title does not contain UTC timezone info");
			String timezoneTitleSign = strtime.substring(0,1);
			strtime = strtime.substring(1,strtime.length());

			 String[] hourMin = strtime.split(":");
			    int hour = Integer.parseInt(hourMin[0]);
			    int mins = Integer.parseInt(hourMin[1]);
			    int offsetInMins = (hour * 60)+mins;
			    expectedOffset = (offsetInMins != 0 && timezoneTitleSign.equals("-")) ? timezoneTitleSign+offsetInMins : String.valueOf(offsetInMins);

			} else {
				assertContains("check title", timeZoneTitle, "(UTC)","title does not contain UTC timezone info");
			}
			assertEquals("API: offset", timeZoneOffset.toString(), expectedOffset, "некорректное значение поля offset");

		}
		} catch (Exception e) {
			log.error(ERROR,e);
			setToAllureChechkedFields();
		}
		setToAllureChechkedFields();
	}

	public JSONObject getAccountProfileInfo(String accountId) {
		String url = getProperty("host") + "/Account/Profile/"+accountId;
		org.json.simple.JSONObject profileInfo = null;
		try {
			token = getAnyToken();
			assertTrue("token received",token != null,"для получения ТС необходимо пройти авторизацию");
			profileInfo = (JSONObject) SendRequestToAPI.sendGetRequest( url,"GET", "","", token).get(0);
			org.json.simple.JSONObject mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(accountId,"").get(0);
			checkReceivedAccountProfileInfo(profileInfo,mongoAccountInfo);
		} catch (Exception e) {
			log.error("Test failed: " + e);
			assertTrue(false, e.toString());

		}
		return profileInfo;
	}

	private void checkReceivedAccountProfileInfo(JSONObject apiReply,JSONObject mongoAccountInfo) {
		String timezone ="";
		try {

	
			
			
		JSONObject apiAccount = (JSONObject) apiReply.get("account");
		JSONObject apiProfile = (JSONObject) apiReply.get("profile");
		assertContainsMngId("API:account.Id",apiAccount.get("id").toString(), "account.Id не верен");
		assertEquals("API:account.type", apiAccount.get("type"), mongoAccountInfo.get("Type"), "account.type не верен");
		//TODO: add request to account roles.
//		assertEquals("API:account.role", apiAccount.get("role"), mongoAccountInfo.get(" "), "account.role не верен");
//		assertEquals("API:account.roleTitle", apiAccount.get("roleTitle"), mongoAccountInfo.get(" "), "account.roleTitle не верен");
		assertEquals("API:account.status", apiAccount.get("status"), mongoAccountInfo.get("Status"), "account.status не верен");

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
		 Date mongoCreatedTime = new Date(Long.parseLong(ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.Created.$date")) ); 
		 
		assertEquals("API:account.created",  apiAccount.get("created"), df.format(mongoCreatedTime), "account.created не верен");
		assertEquals("API:account.customerId", apiAccount.get("customerId"), ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.CustomerId.$oid"), "account.customerId не верен");
		
		if(mongoAccountInfo.containsKey("Settings")){
		String accountStrTimeZone = JsonPath.read(mongoAccountInfo, "$.Settings.SystemSettings.Timezone");
		timezone = getUTCTimezoneFromString(accountStrTimeZone).get("timeZone");
		} else {
			timezone="+03:00";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
			String offsetSign = timezone.substring(0,1);
			Date date = sdf.parse(timezone.substring(1));
			String offsetInMinutes = String.valueOf(date.getTime() / 60000);
			if(! offsetSign.equals("+")){
				offsetInMinutes=offsetSign+offsetInMinutes;
			}
		assertEquals("API:account.timezoneOffset", apiAccount.get("timezoneOffset").toString(), offsetInMinutes, "account.status не верен");
		JSONObject mongoAccProfile = (JSONObject) mongoAccountInfo.get("Profile");
		assertEquals("API:profile.name", apiProfile.get("name"), mongoAccProfile.get("Firstname")+" "+mongoAccProfile.get("Lastname"), "profile.name не верен");
		assertEquals("API:profile.firstname", apiProfile.get("firstname"), mongoAccProfile.get("Firstname"), "profile.firstname не верен");
		assertEquals("API:profile.lastname", apiProfile.get("lastname"), mongoAccProfile.get("Lastname"), "profile.lastname не верен");
		if (apiProfile.containsKey("middlename")){
			assertEquals("API:profile.middlename", apiProfile.get("middlename"), mongoAccProfile.get("Middlename"), "profile.middlename не верен");
		}
		assertEquals("API:profile.sex", apiProfile.get("sex"), mongoAccProfile.get("Sex"), "profile.sex не верен");
		
		if(((JSONArray) mongoAccountInfo.get("Contacts")).size()>1) 
		assertEquals("API:profile.email", apiProfile.get("email"),
				ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.Contacts[0].Value") , "profile.email не верен");
		assertEquals("API:profile.phone", apiProfile.get("phone"), 
				ReplyToJSON.extractPathFromJson(mongoAccountInfo, "$.Contacts[1].Value"), "profile.phone не верен");
		if (apiProfile.containsKey("departmentId"))
		assertEquals("API:profile.departmentId", apiProfile.get("departmentId"), 
				ReplyToJSON.extractPathFromJson(mongoAccProfile, "$.DepartmentId.$oid"), "profile.departmentId не верен");
		if (apiProfile.containsKey("avatarUri")|| mongoAccProfile.get("AvatarUri")!=null)
		assertEquals("API:profile.avatarUri", apiProfile.get("avatarUri"), mongoAccProfile.get("AvatarUri"), "profile.avatarUri не верен");
		
		if (apiProfile.containsKey("employeeId"))
			assertEquals("API:profile.employeeId", apiProfile.get("employeeId"), 
					ReplyToJSON.extractPathFromJson(mongoAccProfile, "$.EmployeeId.$oid"), "profile.employeeId не верен");
		


		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные полученных оповещений':" + e.toString());
		}
		setToAllureChechkedFields();
		
	}


	public static void checkCustomerAccountsSettings(JSONObject mongoAcct, JSONObject apiAccount) {
		
		log.debug("check account with id="+apiAccount.get("id"));
		try {
		assertEquals("API:CustomersAccount.customerId", apiAccount.get("customerId"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.CustomerId.$oid"),"CustomersAccount.customerId не верен");
		assertEquals("API:CustomersAccount.Type", apiAccount.get("type"),mongoAcct.get("Type"),"CustomersAccount.Type не верен");
		
			assertEquals("API:CustomersAccount.profile.firstname",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.profile.firstname"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Profile.Firstname"),
					"CustomersAccount.profile.firstname не верен");
			assertEquals("API:CustomersAccount.profile.lastname",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.profile.lastname"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Profile.Lastname"),
					"CustomersAccount.profile.lastname не верен");
			assertEquals("API:CustomersAccount.profile.sex",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.profile.sex"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Profile.Sex"),
					"CustomersAccount.profile.sex не верен");
			assertEquals("API:CustomersAccount.profile.name",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.profile.name"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Profile.Firstname")+" "+ ReplyToJSON.extractPathFromJson(mongoAcct,"$.Profile.Lastname"),
					"CustomersAccount.profile.name не верен");

			assertEquals("API:CustomersAccount.settings.viewSettings.parkings",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.parkings"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.Parkings"),
					"CustomersAccount.settings.viewSettings.parkings не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.overSpeedTurns",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.overSpeedTurns"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.OverSpeedTurns"),
					"CustomersAccount.settings.viewSettings.overSpeedTurns не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.idlings",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.idlings"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.Idlings"),
					"CustomersAccount.settings.viewSettings.idlings не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.sharpBrakings",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.sharpBrakings"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.SharpBrakings"),
					"CustomersAccount.settings.viewSettings.sharpBrakings не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.sharpSpeedups",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.sharpSpeedups"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.SharpSpeedups"),
					"CustomersAccount.settings.viewSettings.sharpSpeedups не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.sharpTurns",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.sharpTurns"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.SharpTurns"),
					"CustomersAccount.settings.viewSettings.sharpTurns не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.sharpSpeedupUserLimits",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.sharpSpeedupUserLimits"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.SharpSpeedupUserLimits"),
					"CustomersAccount.settings.viewSettings.sharpSpeedupUserLimits не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMinX",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMinX")+".0",
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.SharpSpeedupUserLimitsValues.SharpSpeedupMinX"),
					"CustomersAccount.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMinX не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMaxX",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMaxX")+".0",
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.SharpSpeedupUserLimitsValues.SharpSpeedupMaxX"),
					"CustomersAccount.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMaxX не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMinY",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMinY")+".0",
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.SharpSpeedupUserLimitsValues.SharpSpeedupMinY"),
					"CustomersAccount.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMinY не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMaxY",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMaxY")+".0",
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.SharpSpeedupUserLimitsValues.SharpSpeedupMaxY"),
					"CustomersAccount.settings.viewSettings.sharpSpeedupUserLimitsValues.sharpSpeedupMaxY не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.discharges",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.discharges"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.Discharges"),
					"CustomersAccount.settings.viewSettings.discharges не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.refuelings",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.refuelings"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.Refuelings"),
					"CustomersAccount.settings.viewSettings.refuelings не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.panicButtons",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.panicButtons"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.PanicButtons"),
					"CustomersAccount.settings.viewSettings.panicButtons не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.overSpeeds",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.overSpeeds"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.OverSpeeds"),
					"CustomersAccount.settings.viewSettings.overSpeeds не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.overSpeedUserLimits",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.overSpeedUserLimits"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.OverSpeedUserLimits"),
					"CustomersAccount.settings.viewSettings.overSpeedUserLimits не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.overSpeedLimit",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.overSpeedLimit"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.OverSpeedLimit"),
					"CustomersAccount.settings.viewSettings.overSpeedLimit не верен");
			assertEquals("API:CustomersAccount.settings.viewSettings.pinsSize",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.viewSettings.pinsSize"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ViewSettings.PinsSize"),
					"CustomersAccount.settings.viewSettings.pinsSize не верен");
			
			assertEquals("API:CustomersAccount.settings.systemSettings.timezone",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.systemSettings.timezone"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.SystemSettings.Timezone"),
					"CustomersAccount.settings.systemSettings.timezone не верен");
			assertEquals("API:CustomersAccount.settings.systemSettings.language",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.systemSettings.language"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.SystemSettings.Language"),
					"CustomersAccount.settings.systemSettings.timezone не верен");
			assertEquals("API:CustomersAccount.settings.systemSettings.timezone",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.systemSettings.updateFrequency"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.SystemSettings.UpdateFrequency"),
					"CustomersAccount.settings.systemSettings.updateFrequency не верен");
			
			assertEquals("API:CustomersAccount.settings.objectViewSettings.connectedObjectData",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.connectedObjectData"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.ConnectedObjectData"),
					"CustomersAccount.settings.objectViewSettings.connectedObjectData не верен");
			
			assertEquals("API:CustomersAccount.settings.objectViewSettings.employeeViewFields.photo",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.employeeViewFields.photo"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.EmployeeViewFields.Photo"),
					"CustomersAccount.settings.objectViewSettings.employeeViewFields.photo не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.employeeViewFields.fullName",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.employeeViewFields.fullName"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.EmployeeViewFields.FullName"),
					"CustomersAccount.settings.objectViewSettings.employeeViewFields.fullName не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.employeeViewFields.phone",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.employeeViewFields.phone"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.EmployeeViewFields.Phone"),
					"CustomersAccount.settings.objectViewSettings.employeeViewFields.phone не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.employeeViewFields.type",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.employeeViewFields.type"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.EmployeeViewFields.Type"),
					"CustomersAccount.settings.objectViewSettings.employeeViewFields.type не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.employeeViewFields.group",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.employeeViewFields.group"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.EmployeeViewFields.Group"),
					"CustomersAccount.settings.objectViewSettings.employeeViewFields.group не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.employeeViewFields.device",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.employeeViewFields.device"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.EmployeeViewFields.Device"),
					"CustomersAccount.settings.objectViewSettings.employeeViewFields.device не верен");
		
			assertEquals("API:CustomersAccount.settings.objectViewSettings.vehicleViewFields.photo",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.vehicleViewFields.photo"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.VehicleViewFields.Photo"),
					"CustomersAccount.settings.objectViewSettings.vehicleViewFields.photo не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.vehicleViewFields.name",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.vehicleViewFields.name"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.VehicleViewFields.Name"),
					"CustomersAccount.settings.objectViewSettings.vehicleViewFields.name не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.vehicleViewFields.markModelNumber",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.vehicleViewFields.markModelNumber"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.VehicleViewFields.MarkModelNumber"),
					"CustomersAccount.settings.objectViewSettings.vehicleViewFields.markModelNumber не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.vehicleViewFields.garageNumber",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.vehicleViewFields.garageNumber"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.VehicleViewFields.GarageNumber"),
					"CustomersAccount.settings.objectViewSettings.vehicleViewFields.garageNumber не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.vehicleViewFields.group",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.vehicleViewFields.group"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.VehicleViewFields.Group"),
					"CustomersAccount.settings.objectViewSettings.vehicleViewFields.group не верен");
			assertEquals("API:CustomersAccount.settings.objectViewSettings.vehicleViewFields.deviceModel",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.settings.objectViewSettings.vehicleViewFields.deviceModel"),
					ReplyToJSON.extractPathFromJson(mongoAcct,"$.Settings.ObjectViewSettings.VehicleViewFields.DeviceModel"),
					"CustomersAccount.settings.objectViewSettings.vehicleViewFields.deviceModel не верен");

			
			assertArrayEquals("API:CustomersAccount.roles",
					ReplyToJSON.extractListPathFromJson(apiAccount,"$.roles").toArray(),
					ReplyToJSON.extractListPathFromJson(mongoAcct,"$.Roles..$oid").toArray(),"CustomersAccount.roles не верен");
			compareDateTimeUpTo1min("API:CustomersAccount.Created",ReplyToJSON.extractPathFromJson(mongoAcct,"$.Created.$date"), 
					  ReplyToJSON.extractPathFromJson(apiAccount, "$.created"),System.currentTimeMillis());
			assertEquals("API:CustomersAccount.settings.status",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.status"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Status"),
					"CustomersAccount.status не верен");
			
			assertEquals("API:CustomersAccount.contacts[0].type",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.contacts[0].type"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[0].Type"),
					"CustomersAccount.contacts[0].type не верен");
			assertEquals("API:CustomersAccount.contacts[0].value",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.contacts[0].value"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[0].Value"),
					"CustomersAccount.contacts[0].value не верен");
			compareDateTimeUpTo1min("API:CustomersAccount.contacts[0].Created",ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[0].Created.$date"), 
					  ReplyToJSON.extractPathFromJson(apiAccount, "$.contacts[0].created"),System.currentTimeMillis());
			assertEquals("API:CustomersAccount.contacts[0].isPrimary",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.contacts[0].isPrimary"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[0].IsPrimary"),
					"CustomersAccount.contacts[0].isPrimary не верен");
			if(ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[0].IsPrimary").equals("true")){
				compareDateTimeUpTo1min("API:CustomersAccount.contacts[0].confirmed",ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[0].Confirmed.$date"), 
						  ReplyToJSON.extractPathFromJson(apiAccount, "$.contacts[0].confirmed"),System.currentTimeMillis());
			}
			
			assertEquals("API:CustomersAccount.contacts[1].type",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.contacts[1].type"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[1].Type"),
					"CustomersAccount.contacts[1].type не верен");
			assertEquals("API:CustomersAccount.contacts[1].value",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.contacts[1].value"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[1].Value"),
					"CustomersAccount.contacts[1].value не верен");
			
			
			compareDateTimeUpTo1min("API:CustomersAccount.contacts[1].Created",ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[1].Created.$date"), 
					  ReplyToJSON.extractPathFromJson(apiAccount, "$.contacts[1].created"),System.currentTimeMillis());
			
			assertEquals("API:CustomersAccount.contacts[1].isPrimary",
					ReplyToJSON.extractPathFromJson(apiAccount,"$.contacts[1].isPrimary"),ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[1].IsPrimary"),
					"CustomersAccount.contacts[1].isPrimary не верен");
			
			if(ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[1].IsPrimary").equals("true")){
				compareDateTimeUpTo1min("API:CustomersAccount.contacts[1].confirmed",ReplyToJSON.extractPathFromJson(mongoAcct,"$.Contacts[1].Confirmed.$date"), 
						  ReplyToJSON.extractPathFromJson(apiAccount, "$.contacts[1].confirmed"),System.currentTimeMillis());
			}

			setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false,"ошибка при попытке сравнить запрошенные и фактические даные аккаунта:" +e.toString());
		}
	}

}
