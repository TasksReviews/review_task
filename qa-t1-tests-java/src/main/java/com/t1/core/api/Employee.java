package com.t1.core.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.simple.JSONObject;

import com.jayway.jsonpath.JsonPath;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetDeviceHistory;
import com.t1.core.mongodb.GetDevices;
import com.t1.core.mongodb.GetEmployeeInfoFromMongo;
import com.t1.core.mongodb.GetGroupInfoFromMongo;
import com.t1.core.mongodb.GetVehicleInfoFromMongo;

import org.junit.ComparisonFailure;
import ru.yandex.qatools.allure.annotations.Step;

public class Employee extends AbstractClass implements SharedContainerInterface {

    private String employeeId;
    private TelematicVirtualTracker telematicsDevice;
    private Vehicle vehicle;
    private Department department;
    private JSONObject urlParameters = new JSONObject();
    private String firstname;
    private String lastname;
    private String middleName;
    private String phone;
    private String email;
    private String avatarUri;
    private String employeeType;
    private String status;
    private String description;
    private String vehicleId;
    private String deviceId;
    private String departmentId;
    private Boolean isDeactivated;
    private String deactivationReason;
    private String groupId;
    private String groupName;

    private List<String> geoZoneIds;
    private String driverLicenseUri;
    private JSONObject driverLicense;
    private JSONObject rating;
    private JSONObject groupJson = new JSONObject();
    private JSONObject geoZoneJson = new JSONObject();
    private String emplCreated;
    private String emplUpdated;
    private String token = null;
    private long currentDateTime;
    private String customerId;
    private String customerName;
	
	/*
	private static JSONObject initJson;
	private static String urlParametersString;
	private String notifRuleRelated;
	private String telematics_deviceId;
	*/

    //setters getters
    public void setEmail(String value) {
        this.email = value;
    }

    public String getEmail() {
        return this.email;
    }

    public void setMiddleName(String value) {
        this.middleName = value;
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public void setPhone(String value) {
        this.phone = value;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setAvatarUri(String value) {
        this.avatarUri = value;
    }

    public String getAvatarUri() {
        return this.avatarUri;
    }

    public void setCustomerId(String value) {
        this.customerId = value;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerName(String value) {
        this.customerName = value;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDeviceId(String value) {
        this.deviceId = value;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void setEmployeeId(String value) {
        this.employeeId = value;
    }

    public String getEmployeeId() {
        return this.employeeId;
    }

    public void setEmployeeType(String value) {
        this.employeeType = value;
    }

    public String getEmployeeType() {
        return this.employeeType;
    }

    //	public JSONObject 	getUrlParameters() {		return urlParameters;	}
//	public void 		setUrlParameters(JSONObject urlParameters) {		this.urlParameters = urlParameters;	}
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Boolean getIsDeactivated() {
        return isDeactivated;
    }

    public void setIsDeactivated(Boolean isDeactivated) {
        this.isDeactivated = isDeactivated;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public TelematicVirtualTracker getTelematicsDevice() {
        return telematicsDevice;
    }

    public void setTelematicsDevice(TelematicVirtualTracker telematicsDevice) {
        this.telematicsDevice = telematicsDevice;
    }

    public JSONObject getUrlParameters() {
        return urlParameters;
    }

    public void setUrlParameters(JSONObject urlParameters) {
        this.urlParameters = urlParameters;
    }

    public void putUrlParameters(Object key, Object value) {
        this.urlParameters.put(key, value);
    }

    private static final String CONST_MIDDLENAME = "middleName";
    private static final String CONST_DESCRIPTION = "description";
    private static final String CONST_DRIVER_LICENSE = "driverLicense";
    private static final String CONST_RATING = "rating";
    private static final String CONST_JSON_TELEMATICS_GEOZONE = "json_telematics_geozone";
    private static final String CONST_JSON_GROUP = "json_group";
    private static final String CONST_TEST_FAILED = "Test failed: ";
    private static final String CONST_AUTH_ADMPANEL = "authorizedAccount_admPanel";
    private static final String CONST_ADM_PANEL_HOST = "adminPanelHost";
    private static final String CONST_NUMBER = "number";
    private static final String CONST_CATEGORY = "category";
    private static final String CONST_EXPIRES = "expires";
    private static final String CONST_TOKEN_RECEIVED = "token received";
    private static final String CONST_EMPLOYEES = "employees";
    private static final String CONST_EMPLOYEEID_WRONG = "employee.id не верен";
    private static final String CONST_FIRSTNAME_WRONG = "firstname не верен";
    private static final String CONST_LASTNAME_WRONG = "lastname не верен";
    private static final String CONST_MIDDLENAME_WRONG = "middleName не верен";
    private static final String CONST_TYPE_WRONG = "type не верен";
    private static final String CONST_PHONE_WRONG = "phone не верен";
    private static final String CONST_EMAIL_WRONG = "email не верен";
    private static final String CONST_AVATAURI_WRONG = "avatarUri не верен";
    private static final String CONST_STATUS_WRONG = "status не верен";
    private static final String CONST_DEPARTMENTID_WRONG = "departmentId не верен";
    private static final String CONST_ISDEACTIVATED_WRONG = "isDeactivated не верен";
    private static final String CONST_GROUPID_WRONG = "groupId не верен";
    private static final String CONST_DEACTIVATEDDATE = "DeactivatedDate";
    private static final String CONST_EXP_DEVICE_NOT_REL_TO_EMPL = "ожидалось что ТМУ не привязано к Сотруднику";
    private static final String CONST_API_EMPL_STATUS = "API:employee_status";
    private static final String CONST_DB_EMPLOYEE_GROUP = "DB:employee_group";

    private static final String PATH_EMPLOYEEVIEWCARDID = "$.employeeViewCard.id";
    private static final String PATH_EMPLOYEEREATED = "$.employee.created";
    private static final String PATH_STATUS_VALUE = "$.status.value";
    private static final String PATH_ID = "$._id.$oid";
    private static final String PATH_EMPLOYEE_UDPATED = "$.employee.updated";


    //	public Employee (){
//		initJson = initializeInputWithDefaultOrRandomValues("employee",getProperty("stdPattern"));
//		setMinimalFields(initJson);
//	}

    public Employee(Boolean allFields) {
        JSONObject initMap = InitDefValues.initializeInputWithDefaultOrRandomValues("employee", getProperty("stdPattern"));
        setMinimalFields(initMap);
        if (allFields) {
            setAllFields(initMap);
//			if(this.vehicleId == null ||this.departmentId == null ||this.telematics_deviceId == null){
//				assertTrue(false,"некоторые из полей необходимых для создания сотрудника не содержат данные");
        }
//		}


    }

    public Employee(String pattern) {
/*	
	"firstname": "${text_cyr_01}",
	"lastname": "${text_cyr_01}",
	"phone": "${phone_01}",
	"email": "${email_01}",
	"type": "${EmployeeType_Driver}",
	"status": "${EmployeeStatus_Regular}",
	"avatarUri": "${avatarUri_sys}",
	CONST_MIDDLENAME: "${text_cyr_01}",
	CONST_DESCRIPTION: "${text_cyr_01}",
	"geoZoneIds": [],
	"DeactivationReason": "${text_cyr_01}"
*/

        JSONObject initMap = InitDefValues.initializeInputWithDefaultOrRandomValues("employee", pattern);
        setMinimalFields(initMap);

        this.middleName = initMap.get(CONST_MIDDLENAME).toString();
        this.description = initMap.get(CONST_DESCRIPTION).toString();
        this.deactivationReason = initMap.get("deactivationReason").toString();
        this.driverLicense = (JSONObject) initMap.get(CONST_DRIVER_LICENSE);
        this.rating = (JSONObject) initMap.get(CONST_RATING);


        this.urlParameters.put(CONST_MIDDLENAME, this.middleName);
        this.urlParameters.put(CONST_DESCRIPTION, this.description);
        this.urlParameters.put("IsDeactivated", false);
        this.urlParameters.put("DeactivationReason", this.deactivationReason);
        this.urlParameters.put(CONST_DRIVER_LICENSE, this.driverLicense);
        this.urlParameters.put(CONST_RATING, this.rating);

//		if(( SharedContainer.getContainers().containsKey("vehicles")) && SharedContainer.getFromContainer("vehicles").get(0)!=""){
//			JSONObject  vehicleJson = (JSONObject) SharedContainer.getFromContainer("vehicles").get(0);
//			this.vehicle = vehicleJson.get("id").toString();
//		}
//		if(( SharedContainer.getContainers().containsKey("departments")) && SharedContainer.getFromContainer("departments").get(0)!=""){
//			JSONObject departmentJson = (JSONObject) SharedContainer.getFromContainer("departments").get(0);
//			this.department = departmentJson.get("id").toString();
//		}
//		if(( SharedContainer.getContainers().containsKey("telematics_devices")) && SharedContainer.getFromContainer("telematics_devices").get(0)!=""){
//			JSONObject deviceJson = (JSONObject) SharedContainer.getFromContainer("telematics_devices").get(0);
//			this.telematics_device = device.id;
//					}
        if ((SharedContainer.getContainers().containsKey(CONST_JSON_TELEMATICS_GEOZONE)) && SharedContainer.getFromContainer(CONST_JSON_TELEMATICS_GEOZONE).get(0) != "") {
            geoZoneJson = (JSONObject) SharedContainer.getFromContainer(CONST_JSON_TELEMATICS_GEOZONE).get(0);
            this.geoZoneIds = Arrays.asList(geoZoneJson.get("id").toString());
        }
        if ((SharedContainer.getContainers().containsKey(CONST_JSON_GROUP)) && SharedContainer.getFromContainer(CONST_JSON_GROUP).get(0) != "") {
            groupJson = (JSONObject) SharedContainer.getFromContainer(CONST_JSON_GROUP).get(0);
            this.setGroupId(groupJson.get("id").toString());
        }


        if (getVehicle() != null) {
            this.urlParameters.put("vehicleId", this.getVehicleId());
        }
        if (department != null) {
            this.urlParameters.put("departmentId", this.departmentId);
        }
        if (getTelematicsDevice() != null) {
            this.urlParameters.put("deviceId", this.getTelematicsDevice());
        }

    }

    @Step("Создание нового сотрудника")
    public List<org.json.simple.JSONObject> createNewEmployee() {
        List<org.json.simple.JSONObject> mongoEmployeeInfo = null;
        try {
            String url = getProperty("host") + "/Employee/Create";
            token = getCurAuthAccTokeninT1Client();

            org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, true, true);
            // TODO: add here assertions reply is OK and data exists;

            employeeId = ReplyToJSON.extractPathFromJson(registrationReply, PATH_EMPLOYEEVIEWCARDID);
            TearDownExecutor.addEmployeeToTeardown(this.employeeId);
            emplCreated = ReplyToJSON.extractPathFromJson(registrationReply, PATH_EMPLOYEEREATED);
            // get account info from mongo
            mongoEmployeeInfo = GetEmployeeInfoFromMongo.getEmployeeInfoById(employeeId, "");
//			// compare mongo with input
            checkRegistredEmployee(mongoEmployeeInfo.get(0), registrationReply, false);

        } catch (Exception e) {
            log.error(CONST_TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
        return mongoEmployeeInfo;
    }
	

/*	@Step("Создание нового сотрудника с полным количеством полей и случайными/стандартными значениями")
	public List<org.json.simple.JSONObject> createNewEmployeeAllFields() {
		List<org.json.simple.JSONObject> mongoEmployeeInfo = null;
		try {
			token = getCurAuthAccTokeninT1Client();//getToken("userToken");

			org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,true,true);
			// TODO: add here assertions reply is OK and data exists;
			
			employeeId = ReplyToJSON.extractPathFromJson(registrationReply, PATH_EMPLOYEEVIEWCARDID);
			TearDownExecutor.addEmployeeToTeardown(this.employeeId);
			empl_created=ReplyToJSON.extractPathFromJson(registrationReply, PATH_EMPLOYEEREATED);
			// get account info from mongo
			mongoEmployeeInfo = GetEmployeeInfoFromMongo.getEmployeeInfoById(employeeId,"");
			// compare mongo with input
			checkRegistredEmployee(mongoEmployeeInfo.get(0), registrationReply,false);
		} catch (Exception e) {
			e.printStackTrace();	
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return mongoEmployeeInfo;
	}
*/

    @Step("Создание нового сотрудника с полным количеством полей и случайными/стандартными значениями")
    public List<org.json.simple.JSONObject> createNewEmployeeAllFieldsWithFiles(HashMap<String, String> filesMap) {
        List<org.json.simple.JSONObject> mongoEmployeeInfo = null;
        try {
            token = getCurAuthAccTokeninT1Client();
            String url = getProperty("host") + "/Employee/Create";
            this.avatarUri = "custom/";
            this.driverLicenseUri = filesMap.get("DriverLicense");
            org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, "multipart", filesMap, true).get(0);
            // TODO: add here assertions reply is OK and data exists;

            this.employeeId = ReplyToJSON.extractPathFromJson(registrationReply, PATH_EMPLOYEEVIEWCARDID);
            TearDownExecutor.addEmployeeToTeardown(this.employeeId);
            emplCreated = ReplyToJSON.extractPathFromJson(registrationReply, PATH_EMPLOYEEREATED);
            // get account info from mongo
            mongoEmployeeInfo = GetEmployeeInfoFromMongo.getEmployeeInfoById(employeeId, "");
//			// compare mongo with input
            checkRegistredEmployee(mongoEmployeeInfo.get(0), registrationReply, false);
        } catch (Exception e) {
            log.error(CONST_TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
        return mongoEmployeeInfo;
    }

    @Step("Создание нового сотрудника: панель администратора>ресурсы, с минимальным набором полей ")
    public List<JSONObject> createEmployeesInAdminPanelResources() {
        List<org.json.simple.JSONObject> mongoEmployeeInfo = null;
        try {
            String url = getProperty(CONST_ADM_PANEL_HOST) + "/Api/Admin/Employees";
            token = getCurAuthAccToken(CONST_AUTH_ADMPANEL);

            org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, true, true);
            // TODO: add here assertions reply is OK and data exists;

            employeeId = ReplyToJSON.extractPathFromJson(registrationReply, "$.id");
            TearDownExecutor.addEmployeeToTeardown(this.employeeId);
            // get account info from mongo
            mongoEmployeeInfo = GetEmployeeInfoFromMongo.getEmployeeInfoById(employeeId, "");
//			// compare mongo with input
            //TODO: add here check method for employee
//			checkRegistredEmployee(mongoEmployeeInfo.get(0), registrationReply,false);
        } catch (Exception e) {
            log.error(CONST_TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
        return mongoEmployeeInfo;

    }

    @Step("Удаление сотрудника {0}")
    public void deleteEmployee(String employeeId) {
        String url = getProperty("host") + "/Employee/delete/" + employeeId;
        try {
            token = getCurAuthAccTokeninT1Client();
            SendRequestToAPI.sendPostRequest(url, "", token, true, true);
            checkEmployeeDeleted(employeeId);
        } catch (Exception e) {
            log.error(CONST_TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
    }


    public void deleteEmployeeInAdminPanelResorces() {
        String url = getProperty(CONST_ADM_PANEL_HOST) + "/Api/Admin/Employees/" + this.employeeId;
        try {
            token = getCurAuthAccToken(CONST_AUTH_ADMPANEL);
            SendRequestToAPI.sendDeleteRequest(url, token, true);
            checkEmployeeDeleted(employeeId);
            SharedContainer.setContainer("deletedEmployeesIds", employeeId);
        } catch (Exception e) {
            log.error(CONST_TEST_FAILED + e);
            assertTrue(false, e.toString());

        }

    }

    @Step("Валидация: полученный JSON содержит данные с которыми сотрудник был запрошен")
    public void checkRegistredEmployeeAdminReply(JSONObject jsonMongoReply, JSONObject jsonApiRegistrationReply, boolean emplUpdated) {
        try {
            assertContainsMngId("DB:employee_Id", ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_ID), CONST_EMPLOYEEID_WRONG);
            assertContainsMngId("API:employee_Id", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.id"), CONST_EMPLOYEEID_WRONG);

            if (this.getFirstname() != null) {
                assertEquals("DB:employee_firstname", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Firstname"), this.getFirstname(), CONST_FIRSTNAME_WRONG);
                assertEquals("API:employee_firstname", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.firstname"), this.getFirstname(), CONST_FIRSTNAME_WRONG);
            }


            if (this.getLastname() != null) {
                assertEquals("DB:employee_lastname", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Lastname"), this.getLastname(), CONST_LASTNAME_WRONG);
                assertEquals("API:employee_lastname", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.lastname"), this.getLastname(), CONST_LASTNAME_WRONG);
            }

            if (this.middleName != null) {
                assertEquals("DB:employee_middleName", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Middlename"), this.middleName, CONST_MIDDLENAME_WRONG);
                assertEquals("API:employee_middleName", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.middlename"), this.middleName, CONST_MIDDLENAME_WRONG);
            }

            if (this.getEmployeeType() != null) {
                assertEquals("DB:employee_type", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Type"), this.getEmployeeType(), CONST_TYPE_WRONG);
                assertEquals("API:employee_type", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.type"), this.getEmployeeType(), CONST_TYPE_WRONG);
            }

            if (this.getPhone() != null) {
                assertEquals("DB:employee_phone", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Phone"), this.getPhone(), CONST_PHONE_WRONG);
                assertEquals("API:employee_phone", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.phone"), this.getPhone(), CONST_PHONE_WRONG);
            }
            if (this.getEmail() != null) {
                assertEquals("DB:employee_email", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Email"), this.getEmail(), CONST_EMAIL_WRONG);
                assertEquals("API:employee_email", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.email"), this.getEmail(), CONST_EMAIL_WRONG);
            }

            if (this.avatarUri != null) {
                assertContains("DB:employee_avatarUri", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.AvatarUri"), this.avatarUri, CONST_AVATAURI_WRONG);
                assertContains("API:employee_avatarUri", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.avatarUri"), this.avatarUri, CONST_AVATAURI_WRONG);
            }

            if (this.getStatus() != null) {
                assertEquals("DB:employee_status", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Status"), this.getStatus(), CONST_STATUS_WRONG);
                if (this.getIsDeactivated() != null && this.getIsDeactivated()) {
                    assertEquals(CONST_API_EMPL_STATUS, ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_STATUS_VALUE), "Deactivated", CONST_STATUS_WRONG);
                } else {
                    if (this.deviceId == null || this.deviceId.equals("")) {
                        assertEquals(CONST_API_EMPL_STATUS, ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_STATUS_VALUE), "WithoutDevice", CONST_STATUS_WRONG);
                    } else {
                        assertEquals(CONST_API_EMPL_STATUS, ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_STATUS_VALUE), "Offline", CONST_STATUS_WRONG);
                    }
                }

            }
            if (this.deviceId == null || this.deviceId.equals("")) {
                assertTrue("API:employee.device", !jsonApiRegistrationReply.containsKey("device"), CONST_EXP_DEVICE_NOT_REL_TO_EMPL);
                assertTrue("DB:employee.device", !jsonMongoReply.containsKey("DeviceId"), CONST_EXP_DEVICE_NOT_REL_TO_EMPL);
            }
            if (this.departmentId != null) {
                assertEquals("DB:employee_department", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DepartmentId.$oid"), this.departmentId, CONST_DEPARTMENTID_WRONG);
                assertEquals("API:employee_department", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.department.id"), this.departmentId, CONST_DEPARTMENTID_WRONG);
            }
            if (this.getIsDeactivated() != null) {
                assertEquals("DB:employee_isDeactivated", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.IsDeactivated"), String.valueOf(this.getIsDeactivated()), CONST_ISDEACTIVATED_WRONG);
                if (this.getIsDeactivated()) {
                    assertTrue("DB:employee.DeactivatedDate", jsonMongoReply.containsKey(CONST_DEACTIVATEDDATE) && jsonMongoReply.get(CONST_DEACTIVATEDDATE) != null, "DeactivatedDate отсутствует");
                } else {
                    assertTrue("DB:employee.DeactivatedDate", !jsonMongoReply.containsKey(CONST_DEACTIVATEDDATE), "DeactivatedDate отсутствует");
                }
                assertEquals("API:employee_isDeactivated", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.isDeactivated"), String.valueOf(this.getIsDeactivated()), CONST_ISDEACTIVATED_WRONG);
            }
//			if(this.deactivationReason != null){
//				assertEquals("DB:employee_deactivationReason",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DeactivationReason"), this.deactivationReason,"deactivationReason не верен");
//				assertEquals("API:employee_deactivationReason",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.deactivationReason"), this.deactivationReason,"deactivationReason не верен");
//			}


            if (this.getGroupId() != null) {
                assertEquals("API:employee_groupId", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.group.id"), this.getGroupId(), CONST_GROUPID_WRONG);
            }
            if (this.getGroupId() == null || this.getGroupId().equals("") || this.getGroupId().equals("000000000000000000000000")) {
                assertTrue(CONST_DB_EMPLOYEE_GROUP, !jsonMongoReply.containsKey("GroupId"), "ожидалось что к Сотруднику не привязана группа");
            } else {
                assertEquals(CONST_DB_EMPLOYEE_GROUP, ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.GroupId.$oid"), this.getGroupId(), CONST_GROUPID_WRONG);
            }

        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
            assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные новогого сотрудника:" + e.toString());

        }
    }

    @Step("Валидация: полученный JSON содержит данные с которыми сотрудник был запрошен")
    public synchronized void checkRegistredEmployee(JSONObject jsonMongoReply, JSONObject jsonApiRegistrationReply, boolean emplUpdated) {
        try {

            assertContainsMngId("DB:employee_Id", ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_ID), CONST_EMPLOYEEID_WRONG);
            assertContainsMngId("API:employee_Id", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.id"), CONST_EMPLOYEEID_WRONG);
            assertContainsMngId("API:employee_employeeViewCard.Id", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_EMPLOYEEVIEWCARDID), "employee.employeeViewCard.id не верен");


            if (this.getFirstname() != null) {
                assertEquals("DB:employee_firstname", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Firstname"), this.getFirstname(), CONST_FIRSTNAME_WRONG);
                assertEquals("API:employee_firstname", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.firstname"), this.getFirstname(), CONST_FIRSTNAME_WRONG);
            }


            if (this.getLastname() != null) {
                assertEquals("DB:employee_lastname", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Lastname"), this.getLastname(), CONST_LASTNAME_WRONG);
                assertEquals("API:employee_lastname", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.lastname"), this.getLastname(), CONST_LASTNAME_WRONG);
            }

            if (this.middleName != null) {
                assertEquals("DB:employee_middleName", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Middlename"), this.middleName, CONST_MIDDLENAME_WRONG);
                assertEquals("API:employee_middleName", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.middlename"), this.middleName, CONST_MIDDLENAME_WRONG);
            }

            if (this.getEmployeeType() != null) {
                assertEquals("DB:employee_type", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Type"), this.getEmployeeType(), CONST_TYPE_WRONG);
                assertEquals("API:employee_type", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.type"), this.getEmployeeType(), CONST_TYPE_WRONG);
            }
            if (this.getStatus() != null) {
                assertEquals("DB:employee_status", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Status"), this.getStatus(), CONST_STATUS_WRONG);
                assertEquals(CONST_API_EMPL_STATUS, ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.status"), this.getStatus(), CONST_STATUS_WRONG);
            }
            if (this.description != null) {
                assertEquals("DB:employee_description", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Description"), this.description, "description не верен");
                assertEquals("API:employee_description", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.description"), this.description, "description не верен");
            }
            if (this.getPhone() != null) {
                assertEquals("DB:employee_phone", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Phone"), this.getPhone(), CONST_PHONE_WRONG);
                assertEquals("API:employee_phone", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.phone"), this.getPhone(), CONST_PHONE_WRONG);
            }
            if (this.getEmail() != null) {
                assertEquals("DB:employee_email", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Email"), this.getEmail(), CONST_EMAIL_WRONG);
                assertEquals("API:employee_email", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.email"), this.getEmail(), CONST_EMAIL_WRONG);

            }

            compareDateTimeUpTo1min("Created", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Created.$date"),
                    ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_EMPLOYEEREATED), this.currentDateTime);
            //TODO: correct for employee update
//			if(jsonMongoReply.containsKey("Updated")){
//			compareDateTimeUpTo1min("Updated",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Updated.$date"), 
//					  ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_EMPLOYEE_UDPATED),this.currentDateTime);
//			}
            if (this.emplUpdated == null) {
                assertTrue(!jsonMongoReply.containsKey("Updated"), "поле updated заполняется только после изменения сотрудника");
            } else {
                assertTrue(jsonMongoReply.containsKey("Updated"), "поле updated не было заполнено после изменения сотрудника");
                compareDateTimeUpTo1min("employee.Updated", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Updated.$date"),
                        ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_EMPLOYEE_UDPATED), this.currentDateTime);


                assertDateIsBeforeOrSame("API:employee_updated",
                        ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_EMPLOYEEREATED), ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, PATH_EMPLOYEE_UDPATED),
                        "employee.updated не верен");
                assertTrue("DB:employee_updated > employee_created",
                        Long.parseLong(ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Updated.$date")) > Long.parseLong(ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Created.$date")),
                        "employee.updated не верен");
            }

            if (this.avatarUri != null) {
                assertContains("DB:employee_avatarUri", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.AvatarUri"), this.avatarUri, CONST_AVATAURI_WRONG);
                assertContains("API:employee_avatarUri", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.avatarUri"), this.avatarUri, CONST_AVATAURI_WRONG);
            }
            if ((this.getVehicleId() != null) && (this.getIsDeactivated() == null || this.getIsDeactivated() == false)) { //if object contains info about vehicle and it is not in deactivated state
                assertEquals("DB:employee_vehicleId", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.VehicleId.$oid"), this.getVehicleId(), "vehicles не верен");
                assertEquals("API:employee_vehicleId", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.vehicleId"), this.getVehicleId(), "vehicles не верен");
            }
            if (this.getIsDeactivated() != null) {
                assertEquals("DB:employee_isDeactivated", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.IsDeactivated"), this.getIsDeactivated().toString(), CONST_ISDEACTIVATED_WRONG);
                assertEquals("API:employee_isDeactivated", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.isDeactivated"), this.getIsDeactivated().toString(), CONST_ISDEACTIVATED_WRONG);
            }
            if ((this.deviceId != null) && (this.getIsDeactivated() == null || this.getIsDeactivated() == false)) { //if object contains info about device and it is not in deactivated state
                assertEquals("DB:employee_deviceId", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DeviceId.$oid"), this.deviceId, "deviceId не верен");
                assertEquals("API:employee_deviceId", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.deviceId"), this.deviceId, "deviceId не верен");
            } else {
                assertTrue("API:employee.device", !jsonApiRegistrationReply.containsKey("device"), CONST_EXP_DEVICE_NOT_REL_TO_EMPL);
                assertTrue("DB:employee.device", !jsonMongoReply.containsKey("DeviceId"), CONST_EXP_DEVICE_NOT_REL_TO_EMPL);
            }
            if (this.departmentId != null) {
                assertEquals("DB:employee_department", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DepartmentId.$oid"), this.departmentId, CONST_DEPARTMENTID_WRONG);
                assertEquals("API:employee_department", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.departmentId"), this.departmentId, CONST_DEPARTMENTID_WRONG);
            }

            if (this.deactivationReason != null) {
                assertEquals("DB:employee_deactivationReason", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DeactivationReason"), this.deactivationReason, "deactivationReason не верен");
                assertEquals("API:employee_deactivationReason", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.deactivationReason"), this.deactivationReason, "deactivationReason не верен");
            }
            if (this.getGroupId() != null) {
                assertEquals(CONST_DB_EMPLOYEE_GROUP, ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.GroupId.$oid"), this.getGroupId(), CONST_GROUPID_WRONG);
                assertEquals("API:employee_groupId", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.groupId"), this.getGroupId(), CONST_GROUPID_WRONG);
            }
            if (this.geoZoneIds != null) {
                assertArrayEquals("API:employee_geoZoneIds",
                        ReplyToJSON.extractListPathFromJson(jsonApiRegistrationReply, "$.employee.geoZoneIds").toArray(), this.geoZoneIds.toArray(), "geoZoneIds не верен");
            }
            //start checking driver license:
            if (this.driverLicense != null && this.driverLicense.containsKey(CONST_NUMBER)) {
                assertEquals("DB:employee_driverLicense_number",
                        ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DriverLicense.Number"), this.driverLicense.get(CONST_NUMBER),
                        "employee_driverLicense_number не верен");
                assertEquals("API:employee_driverLicense_number",
                        ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.driverLicense.number"), this.driverLicense.get(CONST_NUMBER),
                        "employee_driverLicense_number не верен");
            }
            if (this.driverLicense != null && this.driverLicense.containsKey(CONST_CATEGORY)) {
                assertEquals("DB:employee_driverLicense_category",
                        ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DriverLicense.Category"), this.driverLicense.get(CONST_CATEGORY),
                        "employee_driverLicense_category не верен");
                assertEquals("API:employee_driverLicense_category",
                        ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.driverLicense.category"), this.driverLicense.get(CONST_CATEGORY),
                        "employee_driverLicense_category не верен");
            }

            if (this.driverLicense != null && this.driverLicense.containsKey(CONST_EXPIRES)) {
                assertEquals("DB:employee_driverLicense_expires",
                        ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DriverLicense.Expires.$date"), dateToUnixTime(this.driverLicense.get(CONST_EXPIRES).toString()),
                        "employee_driverLicense_expires не верен");
                assertContains("API:employee_driverLicense_expires",
                        ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.driverLicense.expires"), this.driverLicense.get(CONST_EXPIRES).toString(),
                        "employee_driverLicense_expires не верен");
            }


            if (this.driverLicense != null && this.driverLicense.containsKey("DocumentUri")) {
                assertContainsMngId("DB:employee_driverLicense_DocumentUri",
                        ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.DriverLicense.DocumentUri"), "employee_driverLicense_DocumentUri не верен");
                assertContainsMngId("API:employee_driverLicense_DocumentUri",
                        ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.driverLicense.documentUri"), "employee_driverLicense_DocumentUri не верен");
            }

            assertEquals("DB:employee_CustomerId", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.CustomerId.$oid"), getProperty("testCustomerId"), "customer не верен");
            assertEquals("API:employee_CustomerId", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.customerId"), getProperty("testCustomerId"), "customer не верен");


            if (this.rating != null) {
                assertEquals("DB:employee_rating", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Rating.Value"), this.rating.get("Value").toString(), "rating не верен");
                assertEquals("API:employee_rating", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employee.rating.value"), this.rating.get("Value").toString(), "rating не верен");
            }
//////////////////////////////////////////////////////////////////			


            if (this.getGroupId() != null) {
                assertEquals("API:group.id", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.group.id"), this.getGroupId(), "group.id не верен");
                if (this.getGroupName() == null || this.getGroupName().equals("")) {
                    assertEquals("API:group.name", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.group.name"), this.groupJson.get("Name"), "group.name не верен");
                } else {
                    assertEquals("API:group.name", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.group.name"), this.getGroupName(), "group.name не верен");
                }
            }
            if (this.departmentId != null) {
                assertEquals("API:department.id", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.department.id"), this.departmentId, "department.id не верен");
                assertEquals("API:department.name", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.department.name"), this.department.getDepartmentName(), "department.name не верен");
            }


            if (this.geoZoneIds != null) {
                assertArrayEquals("API:geozones.id", ReplyToJSON.extractListPathFromJson(jsonApiRegistrationReply, "$.geozones..id").toArray(), this.geoZoneIds.toArray(), "geozones.id не верен");
                assertEquals("API:geozones.name", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.geozones[0].name"), this.geoZoneJson.get("Name"), "geozones.name не верен");
            }

            if ((this.deviceId != null) && (this.getIsDeactivated() == null || this.getIsDeactivated() == false)) { //if object contains info about device and it is not in deactivated state
                assertEquals("API:device.id", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.device.id"), this.getTelematicsDevice().getTrackerId(), "device.id не верен");
                assertEquals("API:device.isActive", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.device.isActive"), "true", "device.isActive не верен");

                if (this.getTelematicsDevice().getSignalLevel() != null) {
                    assertEquals("API:device.signalQuality", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.device.signalQuality"),
                            this.getTelematicsDevice().getSignalLevel(), "device.signalQuality не верен");
                }
            }

            if (this.getVehicleId() != null) {
                assertEquals("API:vehicles.id", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].id"), this.getVehicle().getVehicleId(), "vehicle.id не верен");
                assertEquals("API:vehicles.name", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].name"), this.getVehicle().getName(), "vehicle.name не верен");
                if (this.getVehicle().getModel() != null) {
                    assertEquals("API:vehicles.model", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].model"), this.getVehicle().getModel(), "vehicle.model не верен");
                }

                assertEquals("API:vehicles.mark", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].mark"), this.getVehicle().getMark(), "vehicle.mark не верен");
                if (this.getVehicle().getVinCode() != null) {
                    assertEquals("API:vehicles.vinCode", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].vinCode"), this.getVehicle().getVinCode(), "vehicle.vinCode не верен");
                }
                assertEquals("API:vehicles.number", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].number"), this.getVehicle().getNumber(), "vehicle.number не верен");
                try {
                    assertEquals("API:vehicles.isActive", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].isActive"), "true", "vehicle.isActive не верен");
                } catch (ComparisonFailure failure) {
                    assertEquals("API:vehicles.isActive", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].isActive"), "false", "vehicle.isActive не верен");
                }
                assertEquals("API:vehicles.type", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].type"), this.getVehicle().getType(), "vehicle.type не верен");
                if (this.getVehicle().getCapacity() != null) {
                    assertEquals("API:vehicles.capacity", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].capacity"), this.getVehicle().getCapacity(), "vehicle.capacity не верен");
                }
                if (this.getVehicle().getRegistered() != null) {
                    assertEquals("API:vehicles.registered", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].registered"), this.getVehicle().getRegistered(), "vehicle.registered не верен");
                }
                if (this.getVehicle().getGroupId() != null) {
                    assertEquals("API:vehicles.groupId", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].groupId"), this.getVehicle().getGroupId(), "vehicle.groupId не верен");
                }
                if (this.getVehicle().getEmployeeId() != null && ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].isActive") == "true") {
                    assertEquals("API:vehicles.employeeId", ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.vehicles[0].employeeId"), this.employeeId, "vehicle.employeeId не верен");
                }

            }

            //			assertEquals(ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.employeeViewCard.secondaryTitle"), this.phone, CONST_PHONE_WRONG);

//
//			if(this.geoZoneId != null){
////			assertEquals("DB:employee_geoZoneId",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.GeoZoneIds[0].$oid"), this.geoZoneId,"geoZoneIds не верен");
//			assertEquals("API:geozones.id",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.GeoZoneIds[0].$oid"), this.geoZoneId,"geozone.id не верен");
//			assertEquals("API:geozone.name",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.GeoZoneIds[0].name"), this.geoZoneId,"geozones.name не верен");
//			}


            setToAllureChechkedFields();

        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
            assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные новогого сотрудника:" + e.toString());

        }
    }

    public void setMinimalFields(JSONObject init) {
        this.setFirstname(init.get("firstname").toString());
        this.setLastname(init.get("lastname").toString());
        this.setPhone(init.get("phone").toString());
        this.setEmail(init.get("email").toString());
        this.avatarUri = init.get("profile_avatarUri").toString();
        this.setEmployeeType(init.get("employee_type").toString());
        this.setStatus(init.get("status").toString());
        this.setIsDeactivated(false);
        this.setGroupId(null);

        this.urlParameters.clear();
        this.urlParameters.put("firstname", this.getFirstname());
        this.urlParameters.put("lastname", this.getLastname());
        this.urlParameters.put("phone", this.getPhone());
        this.urlParameters.put("email", this.getEmail());
        this.urlParameters.put("avatarUri", this.avatarUri);
        this.urlParameters.put("type", this.getEmployeeType());
        this.urlParameters.put("status", this.getStatus());

        currentDateTime = System.currentTimeMillis();
    }

    public void setAllFields(JSONObject initJson) {
        //get needed objects from prev steps
        try {
            setVehicle((Vehicle) SharedContainer.getLastObjectFromSharedContainer("vehicles"));
            this.setVehicleId(getVehicle().getVehicleId());
            department = (Department) SharedContainer.getLastObjectFromSharedContainer("departments");
            this.departmentId = department.getDepartmentId();

            setTelematicsDevice((TelematicVirtualTracker) SharedContainer.getLastObjectFromSharedContainer("virtualTrackers"));
            this.deviceId = getTelematicsDevice().getTrackerId();

            geoZoneJson = (JSONObject) SharedContainer.getLastObjectFromSharedContainer(CONST_JSON_TELEMATICS_GEOZONE);
            this.geoZoneIds = Arrays.asList(JsonPath.read(geoZoneJson, PATH_ID).toString());

            groupJson = (JSONObject) SharedContainer.getLastObjectFromSharedContainer(CONST_JSON_GROUP);
            this.setGroupId(JsonPath.read(groupJson, PATH_ID));


//		if(( SharedContainer.getContainers().containsKey(CONST_JSON_TELEMATICS_GEOZONE)) && SharedContainer.getFromContainer(CONST_JSON_TELEMATICS_GEOZONE).get(0)!=""){
//			geoZoneJson = (JSONObject) SharedContainer.getFromContainer(CONST_JSON_TELEMATICS_GEOZONE).get(0);
//			this.geoZoneId = geoZoneJson.get("id").toString();
//					}
//		if(( SharedContainer.getContainers().containsKey(CONST_JSON_GROUP)) && SharedContainer.getFromContainer(CONST_JSON_GROUP).get(0)!=""){
//			groupJson = (JSONObject) SharedContainer.getFromContainer(CONST_JSON_GROUP).get(0);
//			this.groupId = groupJson.get("id").toString();
//					}


        } catch (Exception e) {
            log.error(ERROR, e);
            assertTrue(false, "failed to get object from prev step!");
        }
        //put got objects Ids to post params
        this.urlParameters.put("departmentId", this.departmentId);
        this.urlParameters.put("vehicleId", this.getVehicleId());
        this.urlParameters.put("deviceId", this.deviceId);
        this.urlParameters.put("geoZoneIds", this.geoZoneIds);
        this.urlParameters.put("groupId", this.getGroupId());


        this.middleName = initJson.get(CONST_MIDDLENAME).toString();
        this.description = initJson.get(CONST_DESCRIPTION).toString();
        this.deactivationReason = initJson.get("deactivationReason").toString();
        this.rating = (JSONObject) initJson.get(CONST_RATING);
        this.driverLicense = (JSONObject) initJson.get(CONST_DRIVER_LICENSE);

        this.urlParameters.put(CONST_MIDDLENAME, this.middleName);
        this.urlParameters.put(CONST_DESCRIPTION, this.description);
        this.urlParameters.put("IsDeactivated", false);
        this.urlParameters.put("DeactivationReason", this.deactivationReason);
        this.urlParameters.put(CONST_DRIVER_LICENSE, this.driverLicense);
        this.urlParameters.put(CONST_RATING, this.rating);


    }

    public List<org.json.simple.JSONObject> updateEmployeesInAdminPanelResources() {
        List<org.json.simple.JSONObject> mongoEmployeeInfo = null;
        String url = getProperty(CONST_ADM_PANEL_HOST) + "/Api/Admin/Employees/" + this.employeeId;
        //assertTrue(this.employeeId!=null, "необходимо указать id изменяемого сотрудника");
        //urlParameters.put("id", this.employeeId);
        try {
            token = getCurAuthAccToken(CONST_AUTH_ADMPANEL);
            assertTrue(CONST_TOKEN_RECEIVED, token != null, "для изменения сотрудника необходимо пройти авторизацию");
            org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPutRequest(url, jsonParamsToString(urlParameters), token, true, true);
            // TODO: add here assertions reply is OK and data exists;

//		employeeId = ReplyToJSON.extractPathFromJson(registrationReply, "$.id");
//		empl_updated=ReplyToJSON.extractPathFromJson(registrationReply, PATH_EMPLOYEE_UDPATED);
            // get account info from mongo
            mongoEmployeeInfo = GetEmployeeInfoFromMongo.getEmployeeInfoById(employeeId, "");
            //getInsAndTechnicalSatateIds(registrationReply);
//		// compare mongo with input
            checkRegistredEmployeeAdminReply(mongoEmployeeInfo.get(0), registrationReply, true);
        } catch (Exception e) {
            log.error(CONST_TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
        return mongoEmployeeInfo;
    }


    //TODO: method was not checked and probably is not completed!converted from Vehicle obj.
    @SuppressWarnings("unchecked")
    @Step("Изменение сотрудника")
    public List<org.json.simple.JSONObject> updateEmployee() {
        List<org.json.simple.JSONObject> mongoEmployeeInfo = null;
        String url = getProperty("host") + "/Employee/Update";

        assertTrue(this.employeeId != null, "необходимо указать id изменяемого сотрудника");
        urlParameters.put("id", this.employeeId);

        try {
            token = getCurAuthAccTokeninT1Client();
            assertTrue(CONST_TOKEN_RECEIVED, token != null, "для изменения сотрудника необходимо пройти авторизацию");
            org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, true, true);
            // TODO: add here assertions reply is OK and data exists;

            employeeId = ReplyToJSON.extractPathFromJson(registrationReply, "$.employee.id");
            emplUpdated = ReplyToJSON.extractPathFromJson(registrationReply, PATH_EMPLOYEE_UDPATED);
            // get account info from mongo
            mongoEmployeeInfo = GetEmployeeInfoFromMongo.getEmployeeInfoById(employeeId, "");
            //getInsAndTechnicalSatateIds(registrationReply);
            //		// compare mongo with input
            checkRegistredEmployee(mongoEmployeeInfo.get(0), registrationReply, true);
        } catch (Exception e) {
            log.error(CONST_TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
        return mongoEmployeeInfo;
    }

    public JSONObject deleteEmployee() {
        String url = getProperty("host");
        String postParameters = "";
        org.json.simple.JSONObject requestReply = null;
        try {
            url += "/Employee/delete/" + this.employeeId;
            token = getCurAuthAccTokeninT1Client();
            assertTrue(CONST_TOKEN_RECEIVED, token != null, "для удаления сотрудника необходимо пройти авторизацию");
            requestReply = SendRequestToAPI.sendPostRequest(url, postParameters, token, true, true);
            checkEmployeeDeleted(this.employeeId);
        } catch (Exception e) {
            log.error(CONST_TEST_FAILED + e);
            assertTrue(false, e.toString());
        }
        return requestReply;

    }

//	NOT REDAY

    public void checkEmployeeDeleted(String employeeId) {
        log.warn("this method \"checkEmployeeDeleted\" was not completed!");
        try {
            //Devices : запись обновлена, отсутсвуют значения: "ResourceId", "ResourceType"
//				db.getCollection('DeviceHistory').find({})   			(отвязано
//				db.getCollection('Employees').find({}) 					(удалено)
//				db.getCollection('Vehicles').find({}) 					(удалена связь)			
//				db.getCollection('NotificationRules').find({}) 			(НЕ удалено)
//				db.getCollection('OmGroups').find({}) 					(удалено из группы)
//				db.getCollection('ReportTemplates').find({})			(удалено)

            List deletedEmployeejsonMongoReply = GetEmployeeInfoFromMongo.getEmployeeInfoById(employeeId);
            assertTrue("checkEmployeeDeleted", deletedEmployeejsonMongoReply.isEmpty(),
                    "была найдена информация о сотруднике в коллекции \"Employees\":" + deletedEmployeejsonMongoReply.toString());

            if (this.deviceId != null && this.deviceId != "") {
                JSONObject device = (JSONObject) GetDevices.getDeviceById(this.deviceId, null).get(0);
                assertTrue("DB:Device.ResourceType",
                        !device.containsKey("ResourceType"), "Device.ResourceType не верен");
                assertTrue("DB:Device.ResourceId",
                        !device.containsKey("ResourceId"), "Device.ResourceId не верен");


                List<JSONObject> deviceHistList = (List<JSONObject>) GetDeviceHistory.getDeviceHistoryByDeviceId(this.deviceId);
                assertTrue("DB:DeviceHistory", !deviceHistList.isEmpty(), "коллекция DB:DeviceHistory не содержит записей о ТМУ " + deviceId);
                JSONObject deviceHist = deviceHistList.get(0);
                assertEquals("DB:DeviceHistory.RelatedResourceType",
                        ReplyToJSON.extractPathFromJson(deviceHist, "$.RelatedResourceType"), "Employee", "DeviceHistory.RelatedResourceType не верен");
                assertEquals("DB:DeviceHistory.ResourceId",
                        ReplyToJSON.extractPathFromJson(deviceHist, "$.ResourceId.$oid"), employeeId, "DeviceHistory.ResourceId не верен");


                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
                Date currentTime = new Date(System.currentTimeMillis());
                Date uboundTime = new Date(
                        Long.parseLong(ReplyToJSON.extractPathFromJson(deviceHist, "$.UnboundFromResourceTime.$date")));

                assertDateIsBeforeOrSame("DB:DeviceHistory.UnboundFromResourceTime", df.format(uboundTime), df.format(currentTime),
                        "DeviceHistory.UnboundFromResourceTime не верен");
            }
            if (this.getGroupId() != null && this.getGroupId() != "") {
                JSONObject groupInfo = (JSONObject) GetGroupInfoFromMongo.getGroupsInfoById(this.getGroupId()).get(0);

                List<String> objectsInGroup = ReplyToJSON.extractListPathFromJson(groupInfo, "$.Objects..$oid");

                assertTrue("DB:OmGroups.Objects", !objectsInGroup.contains(employeeId),
                        "была найдена информация о сотруднике в коллекции \"OmGroups\":" + groupInfo.toString());
                //remove Employee from group container

                removeEmplFromGroupObj(this.getGroupId(), employeeId);
            }
            if (this.getVehicleId() != null && this.getVehicleId() != "") {
                JSONObject vehicleInfo = (JSONObject) GetVehicleInfoFromMongo.getVehicleInfoById(this.getVehicleId()).get(0);
                assertTrue("DB:Vehicles.EmployeeId", !vehicleInfo.containsKey("EmployeeId"),
                        "была найдена информация о сотруднике в коллекции \"Vehicles\":" + vehicleInfo.toString());
            }
/* since there is no info what should happen with rule\report when we completely remove object, following asserts are commented

		if(this.notifRuleRelated!=null && this.notifRuleRelated!=""){
		List foundNotificationRules = GetNotificationRules.getnotificationRuleInfoById(this.notifRuleRelated);
		if(foundNotificationRules.size()>0){
		JSONObject notifRuleInfo = (JSONObject) foundNotificationRules.get(0);
		List<String> objectsInRule = ReplyToJSON.extractListPathFromJson(notifRuleInfo,"$.ResourcesFilter.Include");
		
		assertTrue("DB:NotificationRules.ResourcesFilter.Include", ! objectsInRule.contains(employeeId),
				   "была найдена информация о сотруднике в коллекции \"NotificationRules\":"+notifRuleInfo.toString());
		log.debug(notifRuleInfo);
		}else{
			log.warn("rule wasnt found, is it ok?");
		}
	}*/
            teardownData.remove(CONST_EMPLOYEES, Arrays.asList(employeeId));
            SharedContainer.removeFromContainer(CONST_EMPLOYEES, employeeId);

            setToAllureChechkedFields();


        } catch (Exception e) {
            log.error(CONST_TEST_FAILED + e);
            assertTrue(false, e.toString());
        }
    }


    //implementation for Shared container
    @Override
    public int removeFromSharedContainer() {
        int indexOfRemovedEntity = -1;
        if (!container.isEmpty() && container.containsKey(CONST_EMPLOYEES) && !container.get(CONST_EMPLOYEES).isEmpty()) {
            ArrayList listOfIds = container.get(CONST_EMPLOYEES);
            //удаление из контейнера
            if (listOfIds.get(0) instanceof com.t1.core.api.Employee) {
                for (int i = 0; i < listOfIds.size(); i++) {
                    if (((Employee) listOfIds.get(i)).employeeId.equals(this.employeeId)) {
                        listOfIds.remove(i);
                        indexOfRemovedEntity = i;
                    }
                }
            }
        }
        return indexOfRemovedEntity;
    }

    @Override
    public void addToSharedContainer() {
        addToSharedContainer(-1);
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked", "serial"})
    public void addToSharedContainer(int index) {
        if (SharedContainer.getContainers().containsKey(CONST_EMPLOYEES) && !SharedContainer.getContainers().get(CONST_EMPLOYEES).isEmpty()) {
            ArrayList existingArray = SharedContainer.getContainers().get(CONST_EMPLOYEES);
            if (index >= 0) {
                existingArray.add(index, this);
            } else {
                existingArray.add(this);
            }
            container.put(CONST_EMPLOYEES, existingArray);
        } else {
            ArrayList newArray = new ArrayList();
            newArray.add(this);
            container.put(CONST_EMPLOYEES, newArray);
        }
    }


    @Override
    public void updateInSharedContainer() {
        int index = this.removeFromSharedContainer();
        this.addToSharedContainer(index);
    }


    public void removeEmplFromGroupObj(String groupId, String employeeId) {
        if (SharedContainer.getContainers().containsKey("groups")) {
            Group group = Group.getFromSharedContainer(groupId);
            group.getObjects().remove(employeeId);
            Iterator<HashMap<String, String>> iterator = group.getResources().iterator();
            while (iterator.hasNext()) {
                HashMap resource = iterator.next();
                if (resource.get("id").equals(employeeId)) {
                    iterator.remove();
                }
            }
        }
    }


    public static Employee getFromSharedContainer(String oldEmployeeId) {
        List<Employee> employeeList = (List<Employee>) SharedContainer.getObjectsFromSharedContainer("all", CONST_EMPLOYEES);
        Employee foundEmpl = null;
        for (Employee empl : employeeList) {
            if (empl.employeeId.equals(oldEmployeeId)) {
                foundEmpl = empl;
                break;
            }
        }
        return foundEmpl;
    }

    //should we move it to sharedContainer class or just correct customer creation step??
    public static Customer getCustomerFromContainer() {
        List customerFromPrevStepLst = (List) SharedContainer.getObjectsFromSharedContainer("any", "customers");
        Customer customerFromPrevStep;
        if (customerFromPrevStepLst == null || customerFromPrevStepLst.isEmpty()) {
            customerFromPrevStep = new Customer();
            JSONObject jsonCust = (JSONObject) SharedContainer.getLastObjectFromSharedContainer("customer");
            try {//this is wrong way! but at least we get id and name
                customerFromPrevStep.setCustomerId(ReplyToJSON.extractPathFromJson(jsonCust, PATH_ID));
            } catch (Exception e) {
                log.error(ERROR, e);
            }
            customerFromPrevStep.setName(jsonCust.get("Name").toString());
        } else {
            customerFromPrevStep = (Customer) customerFromPrevStepLst.get(0);
        }
        return customerFromPrevStep;
    }

}
