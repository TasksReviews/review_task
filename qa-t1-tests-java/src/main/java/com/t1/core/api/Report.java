package com.t1.core.api;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetEmployeeInfoFromMongo;
import com.t1.core.mongodb.GetReportTemplateFromMongo;
import com.t1.core.mongodb.GetVehicleInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Step;

public class Report  extends AbstractClass implements SharedContainerInterface{
	private JSONObject urlParameters = new JSONObject();
	private JSONObject reportVals = new JSONObject();
	private String token = null;
	
	private String reportTemplateId;
	private String cacheReportId;
	private String reportTemplateName;
	private long currentDateTime;
	private String reportTemplateDescription;
	protected ArrayList<Employee> employees;
	private String deviceId;
	private ArrayList<String> geoZoneIds;
	protected JSONObject reportTemplateReply = new JSONObject();
	protected JSONObject reportTemplateUpdateReply = new JSONObject();
	protected JSONArray generatedReportArray = new JSONArray();

	protected Employee employeesObject;
	
	private String reportType;
	private ArrayList<String> reportCriteria;
	private String resourceType;
	private String dateFrom;
	private String dateTo;
	private String periodicity;
	private String detalization;

	private ArrayList<String> resourcesFilterInclude = new ArrayList<>();
	private ArrayList<String> resourcesFilterExclude = new ArrayList<>();
	private ArrayList<String> resourcesFilterGroups = new ArrayList<>();
	private JSONObject shiftsSettings;
	
	private  static final String CONST_DESCRIPTION = "description";
	private  static final String CONST_REPORTCRITERIA = "reportCriteria" ;
	private  static final String CONST_TIME_FROM = "timeFrom";
	private  static final String CONST_TIME_TO = "timeTo";
	private  static final String DATE_FROM = "dateFrom";
	private  static final String DATE_TO = "dateTo";


	private  static final String CONST_SHIFTS_SETTINGS = "shiftsSettings" ;
	private  static final String CONST_REPORT_TYPE = "reportType";
	private  static final String CONST_RESOURCE_TYPE = "ResourceType" ;
	private  static final String CONST_AUTHT1CLIENT = "authorizedAccount_t1client";
	private  static final String CONST_TOKEN_RECEIVED = "token received";
	private  static final String CONST_AUTH_NEEDED = "для получения Отчета необходимо пройти авторизацию";
	private  static final String CONST_CACHE_REPORT_ID = "cacheReportId";
	private  static final String CONST_TEST_FAILED = "Test failed: ";
	private  static final String CONST_REPORT_TEPMPLATES = "/ReportTemplates";
	private  static final String CONST_REPORTS_GENERATE = "/Reports/Generate";
	private  static final String CONST_API_REPORT_TEMPLATES = "API:ReportTemplate[";
	private  static final String CONST_REPORT_TEPMPLATES_STR = "reportTemplates";
	private  static final String CONST_DETALIZATION = "detalization";
	private  static final String CONST_MANUALLY = "Manually";
	
	public String getDateFrom() {		return dateFrom;	}
	public void setDateFrom(String dateFrom) {		this.dateFrom = dateFrom;	}
	public String getDateTo() {		return dateTo;	}
	public void setDateTo(String dateTo) {		this.dateTo = dateTo;	}
	public String getDeviceId() {		return deviceId;	}
	public void setDeviceId(String deviceId) {		this.deviceId = deviceId;	}
	public String getResourceType() {		return resourceType;	}
	public void setResourceType(String resourceType) {		this.resourceType = resourceType;	}
	public String getReportTemplateId() {		return reportTemplateId;	}
	public void setReportTemplateId(String reportTemplateId) {		this.reportTemplateId = reportTemplateId;	}
	public String getCacheReportId() {		return cacheReportId;	}
	public void setCacheReportId(String cacheReportId) {		this.cacheReportId = cacheReportId;	}
	public JSONObject getUrlParameters() {		return urlParameters;	}
	public void setUrlParameters(JSONObject urlParameters) {		this.urlParameters = urlParameters;	}
	public void putUrlParameters(Object key, Object value) {		this.urlParameters.put(key, value);	}
	public ArrayList<String> getReportCriteria() {		return reportCriteria;	}
	public void setReportCriteria(ArrayList<String> reportCriteria) {		this.reportCriteria = reportCriteria;	}
	public HashMap getReportVals() {		return reportVals;	}
	public void setReportVals(JSONObject reportVals) {		this.reportVals = reportVals;	}
	public ArrayList<String> getResourcesFilterInclude() {		return resourcesFilterInclude;	}
	public void setResourcesFilterInclude(ArrayList<String> resourcesFilterInclude) {		this.resourcesFilterInclude = resourcesFilterInclude;	}
	public ArrayList<String> getResourcesFilterGroups() {		return resourcesFilterGroups;	}
	public void setResourcesFilterGroups(ArrayList<String> resourcesFilterGroups) {		this.resourcesFilterGroups = resourcesFilterGroups;	}
	public ArrayList<String> getResourcesFilterExclude() {		return resourcesFilterExclude;	}
	public void setResourcesFilterExclude(ArrayList<String> resourcesFilterExclude) {		this.resourcesFilterExclude = resourcesFilterExclude;	}
	public JSONObject getShiftsSettings() {		return shiftsSettings;	}
	public void setShiftsSettings(JSONObject shiftsSettings) {		this.shiftsSettings = shiftsSettings;	}
	public ArrayList<String> getGeoZoneIds() {		return geoZoneIds;	}
	public void setGeoZoneIds(ArrayList<String> geoZoneIds) {		this.geoZoneIds = geoZoneIds;	}
	public String getPeriodicity() {		return periodicity;	}
	public void setPeriodicity(String periodicity) {		this.periodicity = periodicity;	}
	public String getDetalization() {		return detalization;	}
	public void setDetalization(String detalization) {		this.detalization = detalization;	}
	
	public Report (){
		this.urlParameters.clear();
		}

	public Report (String reportType){
		this.urlParameters.clear();
		
		setReportVals(InitDefValues.initializeReportInputWithDefaultOrRandomValues(reportType,"stdPattern"));
		
		this.reportTemplateName=getReportVals().get("name").toString();
		this.reportTemplateDescription=getReportVals().get(CONST_DESCRIPTION).toString();
		
		this.reportType=reportType;
		this.setReportCriteria((ArrayList<String>)getReportVals().get(CONST_REPORTCRITERIA));
		this.setResourceType(getReportVals().get("resourceType").toString());
		
		this.setDateFrom(getReportVals().get(DATE_FROM).toString());
		this.setDateTo(getReportVals().get(DATE_TO).toString());
		this.setDetalization(getReportVals().get(CONST_DETALIZATION).toString());
		this.urlParameters.put(CONST_DETALIZATION,this.getDetalization());
		this.setShiftsSettings((JSONObject) getReportVals().get(CONST_SHIFTS_SETTINGS));
		
		this.urlParameters.put("name", getReportVals().get("name").toString());
		this.urlParameters.put(CONST_DESCRIPTION, getReportVals().get(CONST_DESCRIPTION).toString());
		

		
//		if(geozoneIdsParam != null)
		this.urlParameters.put("geozoneIds", new ArrayList());

		if(this.getPeriodicity()== null){
			this.setPeriodicity(CONST_MANUALLY);
		}
		this.urlParameters.put("periodicity", this.getPeriodicity());
		if(! getPeriodicity().equals(CONST_MANUALLY)) {
			this.urlParameters.put(DATE_FROM, "");
			this.urlParameters.put(DATE_TO, "");
		}else {
			if(this.dateFrom != null){
				this.urlParameters.put(DATE_FROM, this.dateFrom);
			}
			if(this.dateTo != null){
				this.urlParameters.put(DATE_TO, this.dateTo);
			}
		}
		this.urlParameters.put(CONST_RESOURCE_TYPE, getResourceType());
		final JSONObject contentConfig = new JSONObject();
		contentConfig.put(CONST_REPORT_TYPE, reportType);
		
		ArrayList reqCriteriaParamsArray = new ArrayList();
	//	if (reportType.equals("ReportOnVisitsToGeozones")){
			contentConfig.put(CONST_REPORTCRITERIA, getReportCriteria());
	//	}
		
		this.urlParameters.put("contentConfig", contentConfig);
		
		final JSONObject resourcesFilter = new JSONObject();
		resourcesFilter.put("groups", new ArrayList());
		resourcesFilter.put("include", this.getResourcesFilterInclude());
		resourcesFilter.put("exclude", new ArrayList());
		this.urlParameters.put("resourcesFilter", resourcesFilter);
		this.urlParameters.put(CONST_SHIFTS_SETTINGS, getShiftsSettings());
	}
	

	/**
	 * 
	 * @param reportType
	 * @param name
	 * @param description
	 * @param reportCriteriaParams
	 * @param resourceType
	 * @param inputResourcesFilterInclude
	 * @param dateFrom
	 * @param dateTo
	 * @param periodicity
	 * @param geozoneIdsParam
	 * @param detalization
	 */

	public Report (String reportType, String name, String description, String reportCriteriaParams, String resourceType, final String inputResourcesFilterInclude, String dateFrom, String dateTo, 
			String periodicity,   final String geozoneIdsParam,  String  detalization){

		String url = getProperty("host") + CONST_REPORTS_GENERATE;
		this.urlParameters.clear();
		
		this.reportTemplateName=name;
		this.reportTemplateDescription=description;
		currentDateTime = System.currentTimeMillis();
		
		this.reportType=reportType;
		if(reportType.equals("ReportOnShifts")){
			this.setReportCriteria(addToArrParams(reportCriteriaParams,"ShiftReport_Param"));
		} else if(reportType.equals("ReportOnSensor")){
			this.setReportCriteria(addToArrParams(reportCriteriaParams,"SensorReport_Param"));
		} else {
			this.setReportCriteria(addToArrParams(reportCriteriaParams,reportType+"_Param"));
		}
		this.setResourceType(resourceType);
		this.setDateFrom(dateFrom);
		this.setDateTo(dateTo);

		if(geozoneIdsParam != null){
			this.setGeoZoneIds(addToArrParams(geozoneIdsParam,""));
		}
//		this.setIsDeepDetailed(isDeepDetailed);
		if(inputResourcesFilterInclude!=null){
			this.setResourcesFilterInclude(addToArrParams(inputResourcesFilterInclude,""));
		}
		if(name != null){
			this.urlParameters.put("name", name);
		}
		if(description != null){
			this.urlParameters.put(CONST_DESCRIPTION, description);
		}
		if(periodicity!= null){
			this.setPeriodicity(periodicity);
			this.urlParameters.put("periodicity",periodicity);
		}
		if(periodicity!= null && ! periodicity.equals(CONST_MANUALLY)) {
			this.urlParameters.put(DATE_FROM, "");
			this.urlParameters.put(DATE_TO, "");
		}else {
			if(dateFrom != null){
				this.urlParameters.put(DATE_FROM, dateFrom);
			}
			if(dateTo != null){
				this.urlParameters.put(DATE_TO, dateTo);
			}
		}
		if(geozoneIdsParam != null){
			this.urlParameters.put("geozoneIds", this.getGeoZoneIds());
		}
		this.detalization = detalizationType(detalization);
		this.urlParameters.put(CONST_DETALIZATION, this.getDetalization());

		if(resourceType!=null){
			this.urlParameters.put(CONST_RESOURCE_TYPE, resourceType);
		}
		final JSONObject contentConfig = new JSONObject();
		contentConfig.put(CONST_REPORT_TYPE, reportType);
		
		ArrayList reqCriteriaParamsArray = new ArrayList();
	//	if (reportType.equals("ReportOnVisitsToGeozones")){
			contentConfig.put(CONST_REPORTCRITERIA, getReportCriteria());
	//	}
		
		this.urlParameters.put("contentConfig", contentConfig);
		
		final JSONObject resourcesFilter = new JSONObject();
		resourcesFilter.put("groups", new ArrayList());
		resourcesFilter.put("include", this.getResourcesFilterInclude());
		resourcesFilter.put("exclude", new ArrayList());
		this.urlParameters.put("resourcesFilter", resourcesFilter);
		
		if (this.getShiftsSettings()==null){
			//info about shifts
			setShiftsSettings(new JSONObject());
			getShiftsSettings().put("onlyFullShifts", false);
			List shiftList = new ArrayList();
			
			if(reportType.equals("ReportOnShifts")){
				JSONObject shiftFields = new JSONObject();
				shiftFields.put(CONST_TIME_FROM, "9:00");
				shiftFields.put(CONST_TIME_TO, "18:00");
				shiftFields.put("name", "Смена 1");
				shiftList.add(shiftFields);
			} 			
			getShiftsSettings().put("shiftList", shiftList);
		}
		this.urlParameters.put(CONST_SHIFTS_SETTINGS, getShiftsSettings());
	}

 /*	public Report(Boolean allFields, ArrayList<Employee> employeesObjects) {
		ArrayList<String> employeesIds = new ArrayList<String>();
		this.apiMethod = CONST_REPORTS_GENERATE;
		this.host = getProperty("host");
		this.url = host + apiMethod;
		initJson = initializeInputWithDefaultOrRandomValues("department",getProperty("stdPattern"));
		this.urlParameters.clear();
		this.employees = employeesObjects;
		assertTrue(this.employees.get(0).employeeId != null,	"для создания Департамента необходимо создать\\получить не менее 1 сотрудника");
		this.departmentName = initJson.get("department_name").toString();
		this.urlParameters.put("name", this.departmentName);
		for(Employee employee: employeesObjects){
			employeesIds.add(
					employee.employeeId);
		}
		this.urlParameters.put("employees", employeesIds);

	}*/
	
	@Step("запрос создания отчета")
	public JSONObject requestReportGeneration(){
		String url = getProperty("host") + CONST_REPORTS_GENERATE;
		try {
			token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			reportTemplateReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			
			if(! reportTemplateReply.isEmpty() && reportTemplateReply.containsKey(CONST_CACHE_REPORT_ID)){
				this.setCacheReportId(reportTemplateReply.get(CONST_CACHE_REPORT_ID).toString());
			}
		//	mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(reqVehicleId,"");
//			checkBaseFieldsOfReceivedReport(/*mongoVehicleInfo.get(0),*/ reportTemplateReply, this.urlParameters);

		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return reportTemplateReply;
	}
	
	@Step("запрос создания отчета по cacheReportId")
	public JSONObject requestCachedReportGeneration(){
		String url = getProperty("host") + CONST_REPORTS_GENERATE;
		try {
			token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			this.urlParameters.put("id", this.getCacheReportId());
			reportTemplateReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
		//	mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(reqVehicleId,"");
			checkBaseFieldsOfReceivedReport(/*mongoVehicleInfo.get(0),*/ reportTemplateReply, this.urlParameters);
			this.setCacheReportId(reportTemplateReply.get(CONST_CACHE_REPORT_ID).toString());
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return reportTemplateReply;
	}
	
	public ArrayList<JSONObject> requestTotalValuesBySubresources() {
		String url = getProperty("host") + "/Reports/TotalValuesBySubresources";
		ArrayList<JSONObject> reportTotalValuesBySubresources = new ArrayList<>();
		try {
			token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			reportTotalValuesBySubresources = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, "", new HashMap(), true);
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());
		}
		return reportTotalValuesBySubresources;
	}
	
	public ArrayList<JSONObject> requestGetChartPointsBySubresources() {
		String url = getProperty("host") + "/Reports/GetChartPointsBySubresources";
		ArrayList<JSONObject> reportChartPointsBySubresources = new ArrayList<>();
		try {
			token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			reportChartPointsBySubresources = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, "", new HashMap(), true);
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());
		}
		return reportChartPointsBySubresources;
	}
	
	public ArrayList<JSONObject> requestGetAllSensorsChartPoints() {
		// Reports/GetAllSensorsChartPoints
		String url = getProperty("host") + "/Reports/GetAllSensorsChartPoints";
		ArrayList<JSONObject> reportAllSensorsChartPoints = new ArrayList<>();
		try {
			token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			reportAllSensorsChartPoints = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, "", new HashMap(), true);
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());
		}
		return reportAllSensorsChartPoints;
	}
	
	
	@Step("запрос создания шаблона отчета")
	public JSONObject requestReportTemplate(){
		List<JSONObject> mongoReportTemplateInfo = new ArrayList();
		String url = getProperty("host") + CONST_REPORT_TEPMPLATES;
		try {
			token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			reportTemplateReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			this.setReportTemplateId(reportTemplateReply.get("messageBody").toString());
			TearDownExecutor.addReportTemplateToTeardown(this.getReportTemplateId());
			assertContainsMngId("API: report template id", this.getReportTemplateId(), "report template id не корректен");
				mongoReportTemplateInfo = GetReportTemplateFromMongo.getReportTemplateInfoById(this.getReportTemplateId(),"");
				log.debug(">>>>"+mongoReportTemplateInfo);
			checkBaseFieldsOfReportTemplate(mongoReportTemplateInfo.get(0) );
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return reportTemplateReply;
		

	}
	
	@Step("запрос редактирования шаблона отчета")
	public JSONObject requestUpdateReportTemplate() {
		List<JSONObject> mongoReportTemplateInfo = new ArrayList();
		String url = getProperty("host") + CONST_REPORT_TEPMPLATES;
		try {
			token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			assertTrue(this.getReportTemplateId()!=null, "необходимо указать id изменяемого шаблона отчета");
			urlParameters.put("id", this.getReportTemplateId());
			
			reportTemplateUpdateReply = SendRequestToAPI.sendPutRequest( url, jsonParamsToString(urlParameters), token,false,true);
		//	this.reportTemplateId = reportTemplateReply.get("messageBody").toString();

		//	assertContainsId("API: report template id", this.reportTemplateId, "report template id не корректен");
				mongoReportTemplateInfo = GetReportTemplateFromMongo.getReportTemplateInfoById(this.getReportTemplateId(),"");
				log.debug(">>>>"+mongoReportTemplateInfo);
			checkBaseFieldsOfReportTemplate(mongoReportTemplateInfo.get(0) );
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return reportTemplateUpdateReply;
	}
	
	@Step("запрос получения списка шаблонов отчетов")
	public List<JSONObject> requestReportTemplatesList() {
		List<JSONObject> mongoReportTemplateInfo = new ArrayList();
		List<JSONObject> reportTemplatesListReply = new ArrayList();
		String url = getProperty("host") + CONST_REPORT_TEPMPLATES;
		try {
			token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			 reportTemplatesListReply = (List<JSONObject>) SendRequestToAPI.sendGetRequest( url,"GET", "","", token);
			 String authAccId = getCurAuthAcc(CONST_AUTHT1CLIENT).get("id").toString();
			mongoReportTemplateInfo = GetReportTemplateFromMongo.getReportTemplateInfoByCustomerId(getAuthorizedUserCustomer(authAccId),new ArrayList());
			assertEquals("total report templates",reportTemplatesListReply.size(), mongoReportTemplateInfo.size(), "MongoDB содержит иное количество шаблонов отчетов");
				log.debug(">>>>"+reportTemplatesListReply);
			checkReportTemplateListReceived(reportTemplatesListReply,  mongoReportTemplateInfo);
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return reportTemplatesListReply;
		
	}
	
	private void checkReportTemplateListReceived(List<JSONObject> reportTemplatesListReply,
			List<JSONObject> mongoReportTemplateInfo) {
		String authAccId = getCurAuthAcc(CONST_AUTHT1CLIENT).get("id").toString();
		try {
		for (JSONObject mongoTemplate : mongoReportTemplateInfo){
			JSONObject apiTemplate = new JSONObject();
			String id = ReplyToJSON.extractPathFromJson(mongoTemplate, "$._id.$oid");
			
			for (int j = 0; j < reportTemplatesListReply.size(); j++) {
				if (( reportTemplatesListReply.get(j)).get("id").toString().matches(id)) {
					apiTemplate = reportTemplatesListReply.get(j);
				}
			}
			log.debug("check "+id);
			assertEquals("API:ReportTemplate_id", apiTemplate.get("id"), id, "id не верен");
			assertEquals(CONST_API_REPORT_TEMPLATES+id+"]_name", apiTemplate.get("name"),mongoTemplate.get("Name"), "Name не верен");
			assertEquals(CONST_API_REPORT_TEMPLATES+id+"]_description", apiTemplate.get(CONST_DESCRIPTION),mongoTemplate.get("Description"), "Description не верен");
			assertEquals(CONST_API_REPORT_TEMPLATES+id+"]_reportType",apiTemplate.get(CONST_REPORT_TYPE),ReplyToJSON.extractPathFromJson(mongoTemplate, "$.ContentConfig.ReportType"),"reportType не верен");
			List mngResources = ReplyToJSON.extractListPathFromJson(mongoTemplate, "$.ResourcesFilter.Include..$oid");//.toArray()
			assertEquals(CONST_API_REPORT_TEMPLATES+id+"]_resourcesCount", Integer.parseInt(apiTemplate.get("resourcesCount").toString()),mngResources.size(), "resourcesCount не верен");
			
				if (! mngResources.isEmpty()) {
					ArrayList<String> objects = new ArrayList<>();
					List<JSONObject> accountsInfo = GetEmployeeInfoFromMongo.getInfoByEmployeeIdList(mngResources,getAuthorizedUserCustomer(authAccId));
					for (JSONObject account : accountsInfo) {
						objects.add(account.get("Firstname") + " " + account.get("Lastname"));
					}
					List<JSONObject> vehiclesInfo = GetVehicleInfoFromMongo.getInfoByVehicleIdList(mngResources,getAuthorizedUserCustomer(authAccId));
					for (JSONObject vehicle : vehiclesInfo) {
						objects.add(vehicle.get("Name").toString());
					}
					List<String> apiResources = (List<String>) apiTemplate.get("resources");
					List<String> apiResourcesTmp = new ArrayList<>();
					for (int i = 0; i < apiResources.size(); i++) {
						apiResourcesTmp.add(StringEscapeUtils.unescapeJson(apiResources.get(i)));
					}
					Collections.sort(objects);
					Collections.sort(apiResourcesTmp);
					assertArrayEquals(CONST_API_REPORT_TEMPLATES + id + "]_resources", apiResourcesTmp.toArray(),objects.toArray(), "resources не верен");
				}

			String mngDateTime = ReplyToJSON.extractPathFromJson(mongoTemplate, "$.Created.$date");
			String apiDateTime ="";
			if(apiTemplate.containsKey("created")){
				apiDateTime = dateTimeToUnixTime(apiTemplate.get("created").toString());
			} else {
				assertTrue(false, "'created' field wasn't found.");
			}
			
			assertEquals(CONST_API_REPORT_TEMPLATES+id+"]_Created",mngDateTime.substring(0,mngDateTime.length()-3), apiDateTime.substring(0,apiDateTime.length()-3),"время создания (created) не верно");	
			
			mngDateTime = ReplyToJSON.extractPathFromJson(mongoTemplate, "$.Updated.$date");
			apiDateTime = dateTimeToUnixTime(apiTemplate.get("updated").toString());
			assertEquals(CONST_API_REPORT_TEMPLATES+id+"]_Updated",mngDateTime.substring(0,mngDateTime.length()-3), apiDateTime.substring(0,apiDateTime.length()-3),"время изменения (updated) не верно");
			//what if locale on account is not ru-ru? get here account settings locale...
			if(mongoTemplate.containsKey("DateFrom")){
				DateFormat dfm = new SimpleDateFormat("dd MMMMM",new Locale("ru"));
			String mongoPeriod = "с "+dfm.format(new Date(Long.parseLong(ReplyToJSON.extractPathFromJson(mongoTemplate, "$.DateFrom.$date"))))
							  +" по "+dfm.format(new Date(Long.parseLong(ReplyToJSON.extractPathFromJson(mongoTemplate, "$.DateTo.$date"))));
			assertEquals(CONST_API_REPORT_TEMPLATES+id+"]_period",apiTemplate.get("period"), mongoPeriod,	"period не верен");
			}
		}
		//check sorted correctly
		ArrayList sortedReportTemplates = (ArrayList)GetReportTemplateFromMongo.getReportTemplateInfoByCustomerIdSortedByDates(getAuthorizedUserCustomer(authAccId),Arrays.asList("_id") );

		assertEquals("API:ReportTemplate_sort_order",
				ReplyToJSON.extractListPathFromJson((ArrayList)reportTemplatesListReply,"$..id"),
				ReplyToJSON.extractListPathFromJson(sortedReportTemplates,"$..$oid"), "порядок шаблонов отчетов не верен");
		
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e.toString());
			assertTrue(false, e.toString());
		}
	}

	private void checkBaseFieldsOfReportTemplate(JSONObject mongoReply) {
		try {
		assertEquals("DB:ReportTemplate_name", mongoReply.get("Name"), this.reportTemplateName, "Name не верен");
		assertEquals("DB:ReportTemplate_description", mongoReply.get("Description"), this.reportTemplateDescription, "Description не верен");
//		if(this.getIsDeepDetailed() !=null){
		assertEquals("DB:ReportTemplate_detalization", mongoReply.get("Detalization"), this.getDetalization(), "detalization не верен");
//		}else{
//			assertEquals("DB:ReportTemplate_IsDeepDetailed", mongoReply.get("IsDeepDetailed"), false, "IsDeepDetailed не верен");
//		}
		assertEquals("DB:ReportTemplate_ResourceType", mongoReply.get(CONST_RESOURCE_TYPE), this.getResourceType(), "ResourceType не верен");
		assertEquals("DB:ReportTemplate_Periodicity", mongoReply.get("Periodicity"), this.getPeriodicity(), "Periodicity не верен");
//		assertEquals("DB:ReportTemplate_DataTimeUnit", mongoReply.get("DataTimeUnit"), this.getDataTimeUnit(), "DataTimeUnit не верен");
		
		
		assertEquals("DB:ReportTemplate_ContentConfig.ReportCriteria", ReplyToJSON.extractListPathFromJson(mongoReply,"$.ContentConfig.ReportCriteria"), this.getReportCriteria(), "ContentConfig.ReportCriteria не верен");
		assertEquals("DB:ReportTemplate_ContentConfig.ReportType", ReplyToJSON.extractPathFromJson(mongoReply,"$.ContentConfig.ReportType"), this.reportType, "ReportType не верен");
		if(this.getDateFrom() !=null && ! this.getDateFrom().equals("")){
			assertEquals("DB:ReportTemplate_DateFrom", ReplyToJSON.extractPathFromJson(mongoReply,"$.DateFrom.$date"), dateTimeToUnixTime(this.getDateFrom()), "DateFrom не верен");
		}
		if(this.getDateTo() !=null && ! this.getDateTo().equals("")){
			assertEquals("DB:ReportTemplate_DateTo", ReplyToJSON.extractPathFromJson(mongoReply,"$.DateTo.$date"), dateTimeToUnixTime(this.getDateTo()), "DateTo не верен");
		}
		compareDateTimeUpTo1min("DB:ReportTemplate_Created",ReplyToJSON.extractPathFromJson(mongoReply, "$.Created.$date"), null,this.currentDateTime);	
		compareDateTimeUpTo1min("DB:ReportTemplate_Updated",ReplyToJSON.extractPathFromJson(mongoReply, "$.Updated.$date"), null,this.currentDateTime);
		if(this.getGeoZoneIds() !=null){
		assertEquals("DB:ReportTemplate_GeozoneIds", ReplyToJSON.extractListPathFromJson(mongoReply,"$.GeozoneIds[*].$oid"), this.getGeoZoneIds(), "GeozoneIds не верен");
		}else {
			assertEquals("DB:ReportTemplate_GeozoneIds", ReplyToJSON.extractPathFromJson(mongoReply,"$.GeozoneIds[*].$oid"),"[]", "GeozoneIds не верен");
		}
		
		if (this.getResourcesFilterExclude() != null && ! this.getResourcesFilterExclude().isEmpty()){
		assertEquals("DB:ReportTemplate_ResourcesFilter.Exclude", ReplyToJSON.extractListPathFromJson(mongoReply,"$.ResourcesFilter.Exclude[*].$oid"),this.getResourcesFilterExclude(), "ResourcesFilter.Exclude не верен");
		}else{
			assertEquals("DB:ReportTemplate_ResourcesFilter.Exclude", ReplyToJSON.extractPathFromJson(mongoReply,"$.ResourcesFilter.Exclude"),"[]", "ResourcesFilter.Exclude не верен");

		}
		if (this.getResourcesFilterInclude() != null && ! this.getResourcesFilterInclude().isEmpty()){
		assertEquals("DB:ReportTemplate_ResourcesFilter.Include", ReplyToJSON.extractListPathFromJson(mongoReply,"$.ResourcesFilter.Include[*].$oid"),this.getResourcesFilterInclude(), "ResourcesFilter.Include не верен");
		}else{
			assertEquals("DB:ReportTemplate_ResourcesFilter.Include", ReplyToJSON.extractPathFromJson(mongoReply,"$.ResourcesFilter.Include"),"[]", "ResourcesFilter.Include не верен");
		}
		if (this.getResourcesFilterGroups() != null && ! this.getResourcesFilterGroups().isEmpty()){
		assertEquals("DB:ReportTemplate_ResourcesFilter.Groups", ReplyToJSON.extractListPathFromJson(mongoReply,"$.ResourcesFilter.Groups[*].$oid"),this.getResourcesFilterGroups(), "ResourcesFilter.Groups не верен");
		}else{
			assertEquals("DB:ReportTemplate_ResourcesFilter.Groups", ReplyToJSON.extractPathFromJson(mongoReply,"$.ResourcesFilter.Groups"),"[]", "ResourcesFilter.Groups не верен");
	}
		
		assertEquals("DB:vehicle_CustomerId",ReplyToJSON.extractPathFromJson(mongoReply, "$.CustomerId.$oid"), getProperty("testCustomerId"), "customer не верен");

		
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные 'шаблона отчета':" + e.toString());
		}
		
	}

	@Step("запрос информации проанализированных интервалов")
	public JSONArray generateAnalyticIntervalResults(String deviceid,String startTime, String endTime){
		//apiMethod = "/Reports/AnalyticIntervalResults?deviceid=592f428c36214a623c8689a5&startTime=2017-06-01&endTime=2017-06-09";
		startTime=startTime.replace(" ", "+");
		endTime=endTime.replace(" ", "+");
		//apiMethod ="/Reports/AnalyticIntervalResults?deviceid=592f428c36214a623c8689a5&startTime=2017-06-08+02:00:00&endTime=2017-06-08+03:00:00";
		String url = getProperty("host") + "/Reports/AnalyticIntervalResults?deviceid="+deviceid+"&startTime="+startTime+"&endTime="+endTime;
		try {
			token = getCurAuthAccToken(CONST_AUTHT1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			generatedReportArray = SendRequestToAPI.sendGetRequest( url, "GET","", "",token);
			
			
		//	mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(reqVehicleId,"");
			Event.checkBaseFieldsOfAnalyticIntervalResults(/*mongoVehicleInfo.get(0),*/ generatedReportArray, this.urlParameters,startTime,endTime,deviceid );
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return generatedReportArray;
		

	}

	@Step("Валидация: полученный JSON содержит данные с которыми отчет был запрошен")
	public void checkBaseFieldsOfReceivedReport(/*JSONObject jsonMongoReply,*/ JSONObject inputJsonApiReply, JSONObject initJson) {
		String apiReportReportType = "API:report_reportType";
		try {
			JSONObject jsonApiReply=null;
//			if(this.getIsDeepDetailed()){
//				assertEquals(apiReportReportType,ReplyToJSON.extractPathFromJson(inputJsonApiReply, "$.modelType"), "DeepDetailed",	"modelType не верен");
//				jsonApiReply = (JSONObject)inputJsonApiReply.get("deepDetailedReportModel");
//			}else {
//				assertEquals(apiReportReportType,ReplyToJSON.extractPathFromJson(inputJsonApiReply, "$.modelType"), "Consolidated",	"modelType не верен");
//				jsonApiReply = (JSONObject)inputJsonApiReply.get("consolidatedReportModel");
//			}
			assertEquals("DB:ReportTemplate_detalization", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.detalization"), this.getDetalization(), "detalization не верен");
			
			assertEquals(apiReportReportType,ReplyToJSON.extractPathFromJson(jsonApiReply, "$.contentConfig.reportType"), this.reportType,	"reportType не верен");
			assertArrayEquals("API:report_reportCriteria",ReplyToJSON.extractListPathFromJson(jsonApiReply, "$.contentConfig.reportCriteria").toArray(), this.getReportCriteria().toArray(),	CONST_REPORTCRITERIA);
			assertEquals("API:report_resourceType",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.resourceType"), this.getResourceType(),	"resourceType не верен");
			assertEquals("API:report_dateFrom",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.dateFrom"), this.getDateFrom(),	"dateFrom не верен");
			assertEquals("API:report_dateTo",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.dateTo"), this.getDateTo(),	"dateTo не верен");
			if (this.getGeoZoneIds() != null) {
			assertEquals("API:report_geozoneIds",ReplyToJSON.extractListPathFromJson(jsonApiReply, "$.geozoneIds"), this.getGeoZoneIds(),	"geozoneIds не верен");
			}
			
			assertEquals("DB:ReportTemplate_Periodicity", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.periodicity"), this.getPeriodicity(), "Periodicity не верен");
//			assertEquals("API:report_isDeepDetailed",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.isDeepDetailed"), this.getIsDeepDetailed().toString(),	"isDeepDetailed не верен");
/*		assertArrayEquals("API:report_resourcesFilter.include",
					ReplyToJSON.extractListPathFromJson(jsonApiReply, "$.groups[*].resources[*].resourceCard.id").toArray(), this.resourcesFilterInclude.toArray(),	
								"resourcesFilter.include не верен");
*/			
			assertTrue("API:report_cacheReportId[exists]", inputJsonApiReply.containsKey(CONST_CACHE_REPORT_ID) ,"report_cacheReportId отсутствует");
			assertContainsGUID("API:report_cacheReportId",ReplyToJSON.extractPathFromJson(inputJsonApiReply, "$.cacheReportId"), "report_cacheReportId содержит некорректный GUID");
 

				setToAllureChechkedFields();
			}
		 catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные отчета:" + e.toString());
		}
	}
	
	public String detalizationType(String detalizationType) {
		if (!detalizationType.equals("DeepDetailed") && !detalizationType.equals("Detailed")
				&& !detalizationType.equals("Consolidated")) {
			if (detalizationType.equals("Детализированный")) {
				detalizationType = "DeepDetailed";
			} else if (detalizationType.equals("По дню")) {
				detalizationType = "Detailed";
			} else if (detalizationType.equals("Консолидированный")) {
				detalizationType = "Consolidated";
			} else {
				assertTrue(false, "wrong detalization: " + detalizationType);
			}
		}
		return detalizationType;
	}

	@Override	
	public ArrayList<String> addToArrParams(String input, String prefix) {
		ArrayList<String> paramsArray = new ArrayList<>();
		if (input.length() > 0) {
			String[] splittedInput = input.split(",");
			for (String criteria : splittedInput) {
				paramsArray.add(prefix + criteria);
			}
		}
		return paramsArray;
	}


	@Override
	public int removeFromSharedContainer() {
		int indexOfRemovedEntity=-1;
		if (!container.isEmpty() && container.containsKey(CONST_REPORT_TEPMPLATES_STR) && ! container.get(CONST_REPORT_TEPMPLATES_STR).isEmpty()) {
			ArrayList listOfIds = container.get(CONST_REPORT_TEPMPLATES_STR);
				// удаление из контейнера
				if (listOfIds.get(0) instanceof com.t1.core.api.Report) {
					for (int i = 0; i < listOfIds.size(); i++) {
						if (((Report) listOfIds.get(i)).getReportTemplateId().equals(this.getReportTemplateId())) {
							listOfIds.remove(i);
							indexOfRemovedEntity=i;
						}
					}
				}
		}
		return indexOfRemovedEntity;
	}

	@Override
	public void addToSharedContainer() {
		addToSharedContainer(-1);
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public void addToSharedContainer(int index) {
		if (SharedContainerInterface.getContainers().containsKey(CONST_REPORT_TEPMPLATES_STR) && ! SharedContainerInterface.getContainers().get(CONST_REPORT_TEPMPLATES_STR).isEmpty()) {
			ArrayList<Report> existingArray = (ArrayList<Report>) SharedContainerInterface.getContainers().get(CONST_REPORT_TEPMPLATES_STR);
			if(index>=0) {
				existingArray.add(index,this);
			} else {
				existingArray.add(this);
			}
			container.put(CONST_REPORT_TEPMPLATES_STR, existingArray);
		} else {
			ArrayList newArray = new ArrayList();
			newArray.add(this);
			container.put(CONST_REPORT_TEPMPLATES_STR, newArray);
		}
	}

	@Override
	public void updateInSharedContainer() {
		int index = this.removeFromSharedContainer();
		this.addToSharedContainer(index);
	}


}
	
