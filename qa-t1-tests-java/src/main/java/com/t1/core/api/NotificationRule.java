package com.t1.core.api;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetNotificationRules;

import ru.yandex.qatools.allure.annotations.Step;

public class NotificationRule  extends AbstractClass implements SharedContainerInterface{
	private String host = "";
	private String url;
	private String apiMethod;
	JSONObject ruleAPIReply = new JSONObject();
	protected JSONObject urlParameters = new JSONObject();
	
	private String ruleId;
	protected List<String> groups = new ArrayList<>();
	protected List<String> include = new ArrayList<>();
	protected List<String> exclude = new ArrayList<>();
	
	protected List<String> daysOfWeek = new ArrayList<>();
	protected ArrayList<String> geozones = new ArrayList<>();
	protected List<String> limitationValueArr= new ArrayList<>();
	private String limitationValue= "";
	private String limitationValue2= "";
	private String timeFrom;
	private String timeTo;
	private String title;
	private String accountId;
	private boolean isImportant ;
	private boolean showOnDisplay;
	private String roadEventArgumentsType;
	private String roadEventType;
	protected final JSONObject resourcesFilter = new JSONObject();
	protected Map roadEventArguments = new LinkedHashMap();
	
	protected JSONObject initJson;
	private String token = null;
	private String authClientStr = "authorizedAccount_t1client";
	public String getLimitationValue() {		return limitationValue;	}
	public void setLimitationValue(String limitationValue) {		this.limitationValue = limitationValue;	}
	public String getRoadEventType() {		return roadEventType;	}
	public void setRoadEventType(String roadEventType) {		this.roadEventType = roadEventType;	}
	public String getRuleId() {		return ruleId;	}
	public void setRuleId(String ruleId) {		this.ruleId = ruleId;	}
	public String getAccountId() {		return accountId;	}
	public void setAccountId(String accountId) {		this.accountId = accountId;	}
	public String getTitle() {		return title;	}
	public void setTitle(String title) {		this.title = title;	}
	public boolean isImportant() {		return isImportant;	}
	public void setImportant(boolean isImportant) {		this.isImportant = isImportant;	}
	public String getLimitationValue2() {		return limitationValue2;	}
	public void setLimitationValue2(String limitationValue2) {		this.limitationValue2 = limitationValue2;	}
	
	private  static final String CONST_PANICBUTTON = "PanicButton";
	private  static final String CONST_TOKEN_RECEIVED = "token received";
	private  static final String CONST_TEST_FAILED = "Test failed: ";
	private  static final String CONST_AUTH_NEEDED = "для удаления правила необходимо пройти авторизацию";
	private  static final String CONST_NOTIFICATIONRULES = "notificationRules";
	private  static final String CONST_ROADARGUMENTS_DURATION_WRONG = "roadEventArguments.duration не верен";
	
	
	public NotificationRule (){
		this.host = getProperty("host");
		this.url = host + apiMethod;
		this.urlParameters.clear();
		}
	
	@SuppressWarnings("unchecked")	
	public NotificationRule (String roadEventType, String groups, String include, String exclude,String daysOfWeek,String timeFrom,
			String timeTo, String title, String isImportant,String showOnDisplay, String roadEventArgumentsType,
			String limitationName,String limitationValue,String limitationName2,String limitationValue2){
		this.host = getProperty("host");
		this.url = host + apiMethod;
		this.urlParameters.clear();
		
		this.setRoadEventType(roadEventType);
		this.groups = addToArrParams(groups,"");
		this.include = addToArrParams(include,"");
		this.exclude = addToArrParams(exclude,"");
		this.daysOfWeek = addToArrParams(daysOfWeek,"");
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
		this.setTitle(title);
		this.setImportant(Boolean.parseBoolean(isImportant));
		this.showOnDisplay = Boolean.parseBoolean(showOnDisplay);
		this.roadEventArgumentsType  = roadEventArgumentsType;
		
		if (roadEventType.equals(CONST_PANICBUTTON)) {
			this.roadEventArguments.put("", "");
		} else {
			// roadEventArguments has type LinkedHashMap to keep fields order,  otherway method brakes if order of fields changes!
			this.roadEventArguments.put("__type", this.roadEventArgumentsType);
			if (limitationValue.contains("[")) {
				this.limitationValueArr = addToArrParams(limitationValue, "");
				this.roadEventArguments.put(limitationName, this.limitationValueArr);

			} else {
				this.setLimitationValue(limitationValue);
				this.roadEventArguments.put(limitationName, this.getLimitationValue());
			}
			if (roadEventType.equals("GeozoneStanding")) {
				this.setLimitationValue2(limitationValue2);
				this.roadEventArguments.put(limitationName2, this.getLimitationValue2());
			}
		}
		this.resourcesFilter.put("groups", this.groups);
		this.resourcesFilter.put("include", this.include);
		this.resourcesFilter.put("exclude", this.exclude);
		
		this.urlParameters.put("resourcesFilter", this.resourcesFilter);
		this.urlParameters.put("daysOfWeek", this.daysOfWeek);
		this.urlParameters.put("timeFrom", this.timeFrom);
		this.urlParameters.put("timeTo", this.timeTo);
		this.urlParameters.put("title", this.getTitle());
		this.urlParameters.put("isImportant", this.isImportant());
		this.urlParameters.put("showOnDisplay", this.showOnDisplay);
		this.urlParameters.put("roadEventType", this.getRoadEventType());
		this.urlParameters.put("roadEventArguments", this.roadEventArguments);
		
	}
	
	@SuppressWarnings("unchecked")
	@Step("запрос создания правила оповещения")
	public JSONObject createRule(){
		List<org.json.simple.JSONObject> mongoNotificationRuleInfo = null;
		apiMethod = "/NotificationRules/Create";
		url = getProperty("host") + apiMethod;
		try {
			token = getCurAuthAccToken(authClientStr);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для создания правила  необходимо пройти авторизацию");
			ruleAPIReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			this.setRuleId(ruleAPIReply.get("id").toString());
			this.setAccountId(ruleAPIReply.get("accountId").toString());
			
			TearDownExecutor.addRuleToTeardown(this.getRuleId());
						
			mongoNotificationRuleInfo = GetNotificationRules.getNotificationRuleInfoById(this.getRuleId());
			checkBaseFieldsOfReceivedRule(mongoNotificationRuleInfo.get(0), ruleAPIReply, this.urlParameters);
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return ruleAPIReply;
		
	}
	
	public JSONObject turnOffRule(List<String> rulesIds){
		return ruleSwitch(rulesIds, "TurnOff", false);
	}
	
	public JSONObject turnOnRule(List<String> rulesIds){
		return ruleSwitch(rulesIds, "TurnOn",true);
	}
	
	private JSONObject ruleSwitch(List<String> rulesIds, String state,boolean isEnabled) {
		if (state.equals("TurnOn") || state.equals("TurnOff")) {
			url = getProperty("host") + "/NotificationRules/" + state;
			urlParameters.clear();
			urlParameters.put("ids", rulesIds);
			try {
				token = getCurAuthAccToken(authClientStr);
				assertTrue(CONST_TOKEN_RECEIVED, token != null, CONST_AUTH_NEEDED);
				ruleAPIReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, false,
						true);
				for (String rule : rulesIds) {
					List<JSONObject> mongoNotificationRulesList = (List<JSONObject>) GetNotificationRules.getNotificationRuleInfoById(rule);
					assertEquals("DB:notificationsRule_enabled", mongoNotificationRulesList.get(0).get("Enabled"),
							isEnabled, "notificationsRule_enabled не верен");
				}
				setToAllureChechkedFields();
			} catch (Exception e) {
				log.error(CONST_TEST_FAILED + e);
				assertTrue(false, e.toString());

			}
		} else {
			assertTrue(false, "wrong syaye for rule: " + state);
		}
		return ruleAPIReply;
	}
	
	@SuppressWarnings("unchecked")
	@Step("запрос удаления правила оповещения")
	public JSONObject deleteRule(){
		List<org.json.simple.JSONObject> mongoNotificationRuleInfo = null;
		apiMethod = "/NotificationRules/Delete/"+this.getRuleId();
		url = getProperty("host") + apiMethod;
		try {
			token =getCurAuthAccToken(authClientStr);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			ruleAPIReply = SendRequestToAPI.sendPostRequest( url, "", token,false,true);

			mongoNotificationRuleInfo = GetNotificationRules.getNotificationRuleInfoById(this.getRuleId());
			assertTrue(mongoNotificationRuleInfo.isEmpty(), "правило не было удалено из БД");
			TearDownExecutor.removeEntityFromTeardown(this.getRuleId(), CONST_NOTIFICATIONRULES);
			setToAllureChechkedFields();
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return ruleAPIReply;
		
	}
	public static JSONObject deleteRule(String id){
		NotificationRule newNotifRule = new NotificationRule ();
		newNotifRule.setRuleId(id);
	return newNotifRule.deleteRule();
	}
	
	@Step("Валидация: полученный JSON содержит данные с которыми правило было запрошено")
	public void checkBaseFieldsOfReceivedRule (JSONObject jsonMongoReply, JSONObject  createdRule, JSONObject  urlParameters){
		String resourcesFilterGroupsWrong = "resourcesFilter.groups не верен";
		try {
			assertContainsMngId("API:notificationsRule_Id",ReplyToJSON.extractPathFromJson(createdRule, "$.id"), "notifications.id не верен");
			assertEquals("DB:notificationsRule_Id",ReplyToJSON.extractPathFromJson(createdRule, "$.id"), ReplyToJSON.extractPathFromJson(jsonMongoReply, "$._id.$oid"),
					resourcesFilterGroupsWrong);
			assertTrue(createdRule.containsKey("resourcesFilter"),"ответ не содержит поле resourcesFilter");
			assertArrayEquals("API:notificationsRule_resourcesFilter.groups", 
								ReplyToJSON.extractListPathFromJson(createdRule, "$.resourcesFilter.groups").toArray(), this.groups.toArray(), 
								resourcesFilterGroupsWrong);
			assertArrayEquals("DB:notificationsRule_resourcesFilter.groups", 
								ReplyToJSON.extractListPathFromJson(jsonMongoReply, "$.ResourcesFilter.Groups").toArray(), this.groups.toArray(), 
								resourcesFilterGroupsWrong);
			assertArrayEquals("API:notificationsRule_resourcesFilter.include", 
								ReplyToJSON.extractListPathFromJson(createdRule, "$.resourcesFilter.include").toArray(), this.include.toArray(), 
								"resourcesFilter.include не верен");
			assertArrayEquals("DB:notificationsRule_resourcesFilter.include", 
								ReplyToJSON.extractListPathFromJson(jsonMongoReply, "$.ResourcesFilter.Include..$oid").toArray(), this.include.toArray(), 
								"resourcesFilter.include не верен");
			assertArrayEquals("API:notificationsRule_resourcesFilter.exclude", 
								ReplyToJSON.extractListPathFromJson(createdRule, "$.resourcesFilter.exclude").toArray(), this.exclude.toArray(), 
								"resourcesFilter.exclude не верен");
			assertArrayEquals("DB:notificationsRule_resourcesFilter.exclude", 
								ReplyToJSON.extractListPathFromJson(jsonMongoReply, "$.ResourcesFilter.Exclude").toArray(), this.exclude.toArray(), 
								"resourcesFilter.exclude не верен");
			assertNotNull("notificationsRule_resourcesFilter.resourcesCount",
					ReplyToJSON.extractPathFromJson(createdRule,"$.resourcesFilter.resourcesCount"), "resourcesFilter.resourcesCount отсутствует");
			
			assertEquals("API:notificationsRule_daysOfWeek", createdRule.get("daysOfWeek"), this.daysOfWeek, "daysOfWeek не верен");
			assertEquals("DB:notificationsRule_daysOfWeek", jsonMongoReply.get("DaysOfWeek"), this.daysOfWeek, "daysOfWeek не верен");
			assertEquals("API:notificationsRule_timeFrom", createdRule.get("timeFrom"), "0."+this.timeFrom, "timeFrom не верен");
			assertEquals("DB:notificationsRule_timeFrom", jsonMongoReply.get("TimeFrom"), this.timeFrom, "timeFrom не верен");
			assertEquals("API:notificationsRule_timeTo", createdRule.get("timeTo"), "0."+this.timeTo, "title не верен");
			assertEquals("DB:notificationsRule_timeTo", jsonMongoReply.get("TimeTo"), this.timeTo, "title не верен");
			assertEquals("API:notificationsRule_title", createdRule.get("title"), this.getTitle(), "deviceId не верен");
			assertEquals("DB:notificationsRule_title", jsonMongoReply.get("Title"), this.getTitle(), "deviceId не верен");
			assertEquals("API:notificationsRule_isImportant", createdRule.get("isImportant"), this.isImportant(), "isImportant не верен");
			assertEquals("DB:notificationsRule_isImportant", jsonMongoReply.get("IsImportant"), this.isImportant(), "isImportant не верен");
			assertEquals("API:notificationsRule_showOnDisplay", createdRule.get("showOnDisplay"), this.showOnDisplay, "showOnDisplay не верен");
			assertEquals("DB:notificationsRule_showOnDisplay", jsonMongoReply.get("ShowOnDisplay"), this.showOnDisplay, "showOnDisplay не верен");
			assertEquals("API:notificationsRule_roadEventType", createdRule.get("roadEventType"), this.getRoadEventType(), "roadEventType не верен");
			assertEquals("DB:notificationsRule_roadEventType", jsonMongoReply.get("RoadEventType"), this.getRoadEventType(), "roadEventType не верен");
		
		
		if(! this.getRoadEventType().equals(CONST_PANICBUTTON)){
		assertTrue(createdRule.containsKey("roadEventArguments"),"ответ не содержит поле roadEventArguments");
		assertEquals("API:notificationsRule_roadEventArguments.__type", ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.__type"), this.roadEventArgumentsType, "roadEventArguments.__type не верен");
		assertContains("DB:notificationsRule_roadEventArguments.__type", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments._t"), this.roadEventArgumentsType.substring(15,33), "roadEventArguments.__type не верен");
		}
		if(this.getRoadEventType().equals("GeozoneEntrance")||this.getRoadEventType().equals("GeozoneExit")){
		assertArrayEquals("API:notificationsRule_roadEventArguments.geozones", 
				ReplyToJSON.extractListPathFromJson(createdRule, "$.roadEventArguments.geozones").toArray(), this.limitationValueArr.toArray(), "roadEventArguments.geozones не верен");
		assertArrayEquals("DB:notificationsRule_roadEventArguments.geozones", 
				ReplyToJSON.extractListPathFromJson(jsonMongoReply, "$.RoadEventArguments.Geozones..$oid").toArray(), this.limitationValueArr.toArray(), "roadEventArguments.geozones не верен");
		}
		else if(this.getRoadEventType().equals("GeozoneStanding")){
			assertEquals("API:notificationsRule_roadEventArguments.duration", 
					ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.duration"),  "0."+this.getLimitationValue2(), CONST_ROADARGUMENTS_DURATION_WRONG);
			assertEquals("DB:notificationsRule_roadEventArguments.duration", 
					ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments.Duration"), this.getLimitationValue2(), CONST_ROADARGUMENTS_DURATION_WRONG);
			}
		
		else if(this.getRoadEventType().equals("SharpTurn")||this.getRoadEventType().contains("SharpSpeedup")||this.getRoadEventType().contains("SharpBraking") ){
			assertEquals("API:notificationsRule_roadEventArguments.acceleration", 
					ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.acceleration"), this.getLimitationValue(), "roadEventArguments.acceleration не верен");
			assertEquals("DB:notificationsRule_roadEventArguments.acceleration", 
					Double.parseDouble(ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments.Acceleration")), Double.parseDouble(this.getLimitationValue()), 
					"roadEventArguments.acceleration не верен");
		}
		
		else if(this.getRoadEventType().contains("StopEvent")||this.getRoadEventType().contains("IdleEvent") ){
			assertEquals("API:notificationsRule_roadEventArguments.duration", 
					ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.duration"), "0."+this.getLimitationValue(), CONST_ROADARGUMENTS_DURATION_WRONG);
			assertEquals("DB:notificationsRule_roadEventArguments.duration", 
					ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments.Duration"), this.getLimitationValue(), CONST_ROADARGUMENTS_DURATION_WRONG);
		}
		
		else if(this.getRoadEventType().contains("FuelConsumption")||this.getRoadEventType().contains("Discharge") ||this.getRoadEventType().contains("Refueling")){
			assertEquals("API:notificationsRule_roadEventArguments.amount", 
					ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.amount"), this.getLimitationValue(), "roadEventArguments.amount не верен");
			assertEquals("DB:notificationsRule_roadEventArguments.amount", 
					Double.parseDouble(ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments.Amount")), Double.parseDouble(this.getLimitationValue()), 
					"roadEventArguments.amount не верен");
		}
		else if(this.getRoadEventType().contains("TelematicDeviceMechanism")){
			assertEquals("API:notificationsRule_roadEventArguments.telematicDeviceMechanism", 
					ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.telematicDeviceMechanism"), this.getLimitationValue(), "roadEventArguments.telematicDeviceMechanism не верен");
			assertEquals("DB:notificationsRule_roadEventArguments.telematicDeviceMechanism", 
					ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments.TelematicDeviceMechanism"), this.getLimitationValue(), "roadEventArguments.telematicDeviceMechanism не верен");
		}
		
		else if(this.getRoadEventType().contains("MileageExceedance")){
			assertEquals("API:notificationsRule_roadEventArguments.mileage", 
					ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.mileage"), this.getLimitationValue(), "roadEventArguments.mileage не верен");
			assertEquals("DB:notificationsRule_roadEventArguments.mileage", 
					ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments.Mileage"), this.getLimitationValue(), "roadEventArguments.mileage не верен");
		}
		else if(this.getRoadEventType().contains("TrafficRulesSpeedLimit")  ){
			assertEquals("API:notificationsRule_roadEventArguments.overspeed", 
					ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.overspeed"), this.getLimitationValue(), "roadEventArguments.overspeed не верен");
			assertEquals("DB:notificationsRule_roadEventArguments.overspeed", 
					ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments.Overspeed"), this.getLimitationValue(), "roadEventArguments.overspeed не верен");
		}
		else if(this.getRoadEventType().contains("UserSpeedlimit")){
			assertEquals("API:notificationsRule_roadEventArguments.speedLimit", 
					ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.speedlimit"), this.getLimitationValue(), "roadEventArguments.speedlimit не верен");
			assertEquals("DB:notificationsRule_roadEventArguments.speedLimit", 
					ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments.Speedlimit"), this.getLimitationValue(), "roadEventArguments.speedlimit не верен");
		}
		else if(this.getRoadEventType().contains("SharpTurnSpeedLimit") ){
			assertEquals("API:notificationsRule_roadEventArguments.speedLimit", 
					ReplyToJSON.extractPathFromJson(createdRule, "$.roadEventArguments.speedLimit"), this.getLimitationValue(), "roadEventArguments.speedLimit не верен");
			assertEquals("DB:notificationsRule_roadEventArguments.speedLimit", 
					ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.RoadEventArguments.SpeedLimit"), this.getLimitationValue(), "roadEventArguments.speedLimit не верен");
		}
		else if(this.getRoadEventType().contains("SensorMaximumExceeded") ){
			log.debug("no limitation for SensorMaximumExceeded");
		}
		else if(this.getRoadEventType().contains("SensorMinimumUnderestimated") ){
			log.debug("no limitation for SensorMinimumUnderestimated");
		}
		else if(this.getRoadEventType().contains(CONST_PANICBUTTON)){
			log.debug("no limitation for "+CONST_PANICBUTTON);
		}
		else {
			assertTrue(false, "not implemented param for rule.");
		}
		
		assertNotNull("API:notificationsRule_created", ReplyToJSON.extractPathFromJson(createdRule,"$.created"), "created отсутствует");
		assertNotNull("DB:notificationsRule_created", ReplyToJSON.extractPathFromJson(jsonMongoReply,"$.Created.$date"), "created отсутствует");
/*		assertEquals("API:notificationsRule_CustomerId",ReplyToJSON.extractPathFromJson(createdRule, "$.customerId"), getProperty("testCustomerOldQAId"), "customer не верен");
		assertEquals("DB:notificationsRule_CustomerId",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.CustomerId.$oid"), getProperty("testCustomerOldQAId"), "customer не верен");
		
		assertEquals("API:notificationsRule_CustomerId",ReplyToJSON.extractPathFromJson(createdRule, "$.accountId"), getProperty("t1APIDemoAccountId"), "accountId не верен");
		assertEquals("DB:notificationsRule_CustomerId",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.AccountId.$oid"), getProperty("t1APIDemoAccountId"), "accountId не верен");
*/		
		setToAllureChechkedFields();
		//TODO: add assertions to mongo
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные новогого правила:" + e.toString());

		}
	}
	
	@Override
	public int removeFromSharedContainer() {
		int indexOfRemovedEntity=-1;
		if (!container.isEmpty() && container.containsKey(CONST_NOTIFICATIONRULES) && ! container.get(CONST_NOTIFICATIONRULES).isEmpty()) {
			ArrayList listOfIds = container.get(CONST_NOTIFICATIONRULES);
				// удаление из контейнера
				if (listOfIds.get(0) instanceof com.t1.core.api.NotificationRule) {
					for (int i = 0; i < listOfIds.size(); i++) {
						if (((NotificationRule) listOfIds.get(i)).getRuleId().equals(this.getRuleId())) {
							listOfIds.remove(i);
							indexOfRemovedEntity=i;
						}
					}
				}
		}
		return indexOfRemovedEntity;
	}

	@Override
	public void addToSharedContainer() {
		addToSharedContainer(-1);
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public void addToSharedContainer(int index) {
		if (SharedContainerInterface.getContainers().containsKey(CONST_NOTIFICATIONRULES) && ! SharedContainerInterface.getContainers().get(CONST_NOTIFICATIONRULES).isEmpty()) {
			ArrayList<NotificationRule> existingArray = (ArrayList<NotificationRule>) SharedContainerInterface.getContainers().get(CONST_NOTIFICATIONRULES);
			if(index>=0) {
				existingArray.add(index,this);
			} else {
				existingArray.add(this);
			}
			container.put(CONST_NOTIFICATIONRULES, existingArray);
		} else {
			ArrayList newArray = new ArrayList();
			newArray.add(this);
			container.put(CONST_NOTIFICATIONRULES, newArray);
		}
	}

	@Override
	public void updateInSharedContainer() {
		int index = this.removeFromSharedContainer();
		this.addToSharedContainer(index);
	}


}
