package com.t1.core.api;

import static com.mongodb.client.model.Filters.eq;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.SharedContainerInterface;
import com.t1.core.mongodb.GetDeviceFullRecordsFromMongo;
import com.t1.core.mongodb.GetDeviceHistory;
import com.t1.core.mongodb.GetNotificationRules;
import com.t1.core.mongodb.GetNotifications;
import com.t1.core.mongodb.GetTrackEvents;
import com.t1.core.mongodb.GetVehicleInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Step;

public class Notification  extends AbstractClass implements SharedContainerInterface{
	protected JSONObject urlParameters = new JSONObject();
	protected JSONObject initJson;
	private String token = null;
	private String notificationId;
	protected ArrayList<Employee> employees;
	private ArrayList<String> notificationEvents = new ArrayList<String>();
	private ArrayList<String> groups = new ArrayList<>();
	private ArrayList<String> include = new ArrayList<>();
	private ArrayList<String> exclude = new ArrayList<>();
	protected JSONObject resourcesFilter = new JSONObject();
	protected JSONObject receivedNotifications = new JSONObject();
	protected JSONArray generatedNotificationArray = new JSONArray();
	private String dateFrom;
	private String dateTo;
	private String skip;
	private String limit;
	
	public String getDateTo() {		return dateTo;	}
	public void setDateTo(String dateTo) {		this.dateTo = dateTo;	}
	public String getDateFrom() {		return dateFrom;	}
	public void setDateFrom(String dateFrom) {		this.dateFrom = dateFrom;	}
	
	private  static final String CONST_TOKEN_RECEIVED = "token received";
	private  static final String CONST_USERTOKEN = "userToken";
	private  static final String CONST_AUTH_NEEDED = "для получения оповещения необходимо пройти авторизацию";
	private  static final String CONST_TEST_FAILED = "Test failed: ";
	private  static final String CONST_NOTIFICATIONS = "notifications";
	private  static final String CONST_EVENTNAME = "eventName";
	private  static final String CONST_ROADEVENTTYPE= "roadEventType";
	private  static final String CONST_ROADEVENTTYPE2 = "RoadEventType";
	private  static final String CONST_API_NOTIFICATIONS_ROADEVENTTYPE = "API:notifications.roadEventType";
	private  static final String CONST_RECORDED = "recorded";
	private  static final String CONST_API_NOTIFICATIONS_RECORDED = "API:notifications.recorded";
	private  static final String CONST_LIMITATION = "limitation";
	private  static final String CONST_COMMON = "common";
	private  static final String PATH_ROADEVENTTYPE = "$.roadEventType";
	private  static final String CONST_MODEL = "model";
	private  static final String CONST_TRAFFICRULESPEEDLIMIT = "TrafficRulesSpeedLimit";
	private  static final String CONST_API_NOTIFICATIONS_LIMITATION = "API:notifications.limitation";
	
	
	public Notification (){
		this.urlParameters.clear();
		}
	
	public Notification (String skip, String limit, String dateFrom, String dateTo, String events, String groups, String include, String exclude){
		this.urlParameters.clear();
		
	/*	{
		    "skip": "1",
		    "limit": "10",
		    "dateFrom": "2017-01-01 12:01:59",
		    "dateTo": "2017-12-24 12:01:59",
		    "events": [
		        "PanicButton"
		    ],
		    "resourcesFilter": {
		        "groups": [
		            "5892f5580a2aa44c744086d6"
		        ],
		        "include": [
		            "58b6e18d39c90a189c3f01b8",
		            "58b8415839c90a1e487b7c55"
		        ],
		        "exclude": [
		            "58b680c639c90a19c0fd4adc"
		        ]
		    }
		}
		*/
		
		this.skip=skip;
		this.limit=limit;
		this.setDateFrom(dateFrom);
		this.setDateTo(dateTo);
		this.notificationEvents.addAll(addToArrParams(events,""));
		
		this.groups.addAll(addToArrParams(groups,""));
		this.include.addAll(addToArrParams(include,""));
		this.exclude.addAll(addToArrParams(exclude,""));
		this.resourcesFilter.put("groups", this.groups);
		this.resourcesFilter.put("include", this.include);
		this.resourcesFilter.put("exclude", this.exclude);
		
		this.urlParameters.put("skip", this.skip);
		this.urlParameters.put("limit", this.limit);
		
		if(dateFrom.contains("сутки") || dateTo.contains("сутки")){
			 DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
			 Date lastDay = new Date(System.currentTimeMillis() - 2* 60*60 * 1000); 
				urlParameters.put("dateFrom", df.format(lastDay));
				urlParameters.put("dateTo", getCurrentLocalDateTimePlus5Sec());//+5secs
		} else {
		this.urlParameters.put("dateFrom", this.getDateFrom());
		this.urlParameters.put("dateTo", this.getDateTo());
		}
		this.urlParameters.put("events", this.notificationEvents);
		
		this.urlParameters.put("resources", this.resourcesFilter);
		//this.urlParameters.put("resourcesFilter", this.resourcesFilter);
	}

 /*	public Notification(Boolean allFields, ArrayList<Employee> employeesObjects) {
		ArrayList<String> employeesIds = new ArrayList<String>();
		this.apiMethod = "/Notifications/Generate";
		this.host = getProperty("host");
		this.url = host + apiMethod;
		initJson = initializeInputWithDefaultOrRandomValues("department",getProperty("stdPattern"));
		this.urlParameters.clear();
		this.employees = employeesObjects;
		assertTrue(this.employees.get(0).employeeId != null,	"для создания Департамента необходимо создать\\получить не менее 1 сотрудника");
		this.departmentName = initJson.get("department_name").toString();
		this.urlParameters.put("name", this.departmentName);
		for(Employee employee: employeesObjects){
			employeesIds.add(
					employee.employeeId);
		}
		this.urlParameters.put("employees", employeesIds);

	}*/
	
	@Step("запрос оповещений старше даты {0}")
	public JSONObject getNotificationsLaterThanDate(){
		String apiMethod = "/Notifications/Last?dateFrom="+this.getDateFrom().replace(" ", "%20")+"&dateTo="+this.getDateTo().replace(" ", "%20");
		String url = getProperty("host") + apiMethod;
		try {
			if ((SharedContainer.getContainers().containsKey(CONST_USERTOKEN))){
				token = getToken(CONST_USERTOKEN);
			}else {
				token = getToken("demoToken");
			}

			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			receivedNotifications = (JSONObject) SendRequestToAPI.sendGetRequest( url, "GET","", "",token).get(0);
			
			
		//	mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(reqVehicleId,"");
			
			checkBaseFieldsExistsInReceivedNotifications(/*mongoVehicleInfo.get(0),*/ receivedNotifications, this.urlParameters);
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return receivedNotifications;
		

	}
	
	@Step("запрос оповещений по id {0}")
	public JSONObject getNotificationsDetailsId(String id){
		this.notificationId=id;
		String url = getProperty("host") + "/Notifications/Details/"+id;
		try {
			token = getToken(CONST_USERTOKEN);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			receivedNotifications = (JSONObject) SendRequestToAPI.sendGetRequest( url, "GET","", "",token).get(0);
		//	mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(reqVehicleId,"");
			checkBaseFieldsExistsInReceivedNotifications(/*mongoVehicleInfo.get(0),*/ receivedNotifications, this.urlParameters);
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());
		}
		return receivedNotifications;
	}

	
	@Step("запрос оповещений по фильтру")
	public JSONObject requestNotificationsByFilter (){
		String url = getProperty("host") + "/Notifications/GetByFilter";
		try {
			token = getAnyToken();
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			receivedNotifications = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			List<JSONObject> notifications = (List) receivedNotifications.get(CONST_NOTIFICATIONS);
			JSONObject insertedNotification = (JSONObject) SharedContainer.getObjectsFromSharedContainer("all","insertedNotifications").get(0);
			assertTrue(notifications.size()==1 , "должно было вернуться 1 подходящее оповещение");
			JSONObject notification = notifications.get(0);

			assertContainsMngId("API:notifications.id",ReplyToJSON.extractPathFromJson(notification, "$.id"), "notifications.id не верен");
			
			assertEquals( "API:notifications.message",notification.get("message")   ,insertedNotification.get("Message"), "notification.Message не верен");
			assertEquals( "API:notifications.resourceId",notification.get("resourceId")   ,insertedNotification.get("ResourceId"), "notification.resourceId не верен");
			assertEquals( "API:notifications.isRead",notification.get("isRead")   ,insertedNotification.get("IsRead"), "notification.isRead не верен");
			assertEquals( "API:notifications.isImportant",notification.get("isImportant")   ,insertedNotification.get("IsImportant"), "notification.isImportant не верен");
			assertEquals( "API:notifications.eventName",notification.get(CONST_EVENTNAME)   ,insertedNotification.get("EventName"), "notification.eventName не верен");
			assertEquals( CONST_API_NOTIFICATIONS_ROADEVENTTYPE,notification.get(CONST_ROADEVENTTYPE)   ,insertedNotification.get(CONST_ROADEVENTTYPE2), "notification.roadEventType не верен");
			if(! (notification.get(CONST_ROADEVENTTYPE).equals("IdleEvent") || 
					notification.get(CONST_ROADEVENTTYPE).equals("GeozoneEntrance")||
					notification.get(CONST_ROADEVENTTYPE).equals("GeozoneExit")||
					notification.get(CONST_ROADEVENTTYPE).equals("StopEvent")||
					notification.get(CONST_ROADEVENTTYPE).equals("TelematicDeviceMechanismOn")||
					notification.get(CONST_ROADEVENTTYPE).equals("TelematicDeviceMechanismOff")||
					notification.get(CONST_ROADEVENTTYPE).equals("PanicButton")  )){
			assertEquals( CONST_API_NOTIFICATIONS_RECORDED,notification.get(CONST_RECORDED)   ,insertedNotification.get("Recorded"), "notification.recorded не верен");
			assertEquals( "API:notifications.isImportant",notification.get(CONST_LIMITATION)   ,insertedNotification.get("Limitation"), "notification.limitation не верен");
			}
			setToAllureChechkedFields();
			
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());
			setToAllureChechkedFields();

		}
		return receivedNotifications;
		

	}
	

	
	@Step("Валидация: полученный JSON содержит данные с которыми оповещение было запрошено")
	public void checkBaseFieldsExistsInReceivedNotifications(
			/* JSONObject jsonMongoReply, */ JSONObject jsonApiReply, JSONObject initJson) {
		try {
			
			if( !(jsonApiReply.containsKey(CONST_NOTIFICATIONS) || jsonApiReply.containsKey(CONST_COMMON))  )
			{ assertTrue("API:data_notifications array or common property exists", false, "data.notifications и common отсутствует в ответе"); }
			
			if (jsonApiReply.containsKey(CONST_NOTIFICATIONS)){
			
			JSONArray jsonApiReplyArr = ReplyToJSON.extractListPathFromJson(jsonApiReply,"$.notifications");
			
			for (int i = 0; i < jsonApiReplyArr.size(); i++) {
				JSONObject notification = (JSONObject) jsonApiReplyArr.get(i);
				notificationId = ReplyToJSON.extractPathFromJson(notification, "$.id");
				log.debug("["+i+"] checking notification: "+notificationId);
				String notificationResource = ReplyToJSON.extractPathFromJson(notification, "$.resourceId");
				String notificationDateTime = ReplyToJSON.extractPathFromJson(notification, "$.eventTime");
				
				log.debug("get notif details in mongo ");
				JSONObject notificationDetailsMongo = (JSONObject) GetNotifications.getNotificationInfoById(notificationId).get(0);
				String eventId = notificationDetailsMongo.get("EventId").toString();  
				String notificationRuleId = notificationDetailsMongo.get("NotificationRuleId").toString();    
				log.debug("notif event: "+eventId);
				JSONObject eventDetailsMongo = (JSONObject) GetTrackEvents.getNotificationInfoById(eventId).get(0);
				String eventDeviceTime = ReplyToJSON.extractPathFromJson(eventDetailsMongo, "$.EventDeviceTime.$date"); 
				
				 Date eventDeviceDateTime = new Date(Long.parseLong(eventDeviceTime)); 
				 //SHOULD BE A WAY TO AVAID DO SO MANY REQUESTS TO DB!
				 List<JSONObject> deviceHistCol= 
						 GetDeviceHistory.getDeviceHistoryByObjectIdAndDateTill( notificationResource,  notificationDateTime);
				 String deviceId= ReplyToJSON.extractPathFromJson(deviceHistCol.get(deviceHistCol.size()-1),"$.DeviceId.$oid");
				 
				 Bson filter = Filters.and (eq ("DeviceId",new ObjectId(deviceId)), 
						   Filters.eq("DeviceTime",eventDeviceDateTime));
				 JSONObject mongoDeviceFullRecords = 
							(JSONObject) GetDeviceFullRecordsFromMongo.getDeviceFullRecordsByCustomFilters(filter).get(0);
				log.debug("get notif rule in mongo ");
				 filter = Filters.and (eq ("ResourcesFilter.Include",notificationResource), 
						            Filters.eq (CONST_ROADEVENTTYPE2,ReplyToJSON.extractPathFromJson(notification, PATH_ROADEVENTTYPE)),
						            Filters.eq ("_id", new ObjectId(notificationRuleId) )) ;

				 JSONObject notificationRule = (JSONObject) GetNotificationRules.getNotificationRuleInfoByCustomFilters( filter).get(0);
				 //
				checkBaseFieldsExists (notification,i,notificationRule,mongoDeviceFullRecords);
				assertNotNull("API:notifications.message ["+i+"]", ReplyToJSON.extractPathFromJson(notification,"$.message"), "notifications.message отсутствует");
				assertNotNull("API:notifications.eventName ["+i+"]", ReplyToJSON.extractPathFromJson(notification,"$.eventName"), "notifications.eventName отсутствует");
				assertEquals("API:notification.eventName",notificationRule.get("Title")   ,notification.get(CONST_EVENTNAME), "notification.eventName не верен");
				assertEquals("API:notifications.eventTime ["+i+"]", Long.parseLong(dateTimeToUnixTime(notification.get("eventTime").toString()))/1000, //accuracy up to 1 second 
						Long.parseLong(ReplyToJSON.extractPathFromJson(mongoDeviceFullRecords,"$.DeviceTime.$date"))/1000,  	"eventTime не верен");
				
				assertNotNull("API:notifications.isImportant ["+i+"]", ReplyToJSON.extractPathFromJson(notification,"$.isImportant"), "notifications.isImportant отсутствует");
				assertNotNull("API:notifications.isRead ["+i+"]", ReplyToJSON.extractPathFromJson(notification,"$.isRead"), "notifications.isRead отсутствует");
//TODO: uncomment this assert defect CON-176
				assertNotNull("API:notifications.avatarUri ["+i+"]", ReplyToJSON.extractPathFromJson(notification,"$.avatarUri"), "notifications.avatarUri отсутствует");
				assertContainsMngId("API:notifications.resourceId ["+i+"]",ReplyToJSON.extractPathFromJson(notification, "$.resourceId"), "notifications.resourceId не верен");
				assertNotNull("API:notifications.primaryTitle ["+i+"]", ReplyToJSON.extractPathFromJson(notification,"$.primaryTitle"), "notifications.primaryTitle отсутствует");
				//	when notification it must contain it?
				//	assertNotNull("API:notifications.secondaryTitle ["+i+"]", ReplyToJSON.extractPathFromJson(notification,"$.secondaryTitle"), "notifications.secondaryTitle отсутствует");
				assertNotNull("API:data_totalUnread exists", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.totalUnread"), "data.totalUnread  отсутствует в ответе"); 
			}

			}
			else if(jsonApiReply.containsKey(CONST_COMMON)) {
				JSONObject notifCommon = (JSONObject) jsonApiReply.get(CONST_COMMON);
				
				String notificationRuleId = ReplyToJSON.extractPathFromJson(notifCommon, "$.notificationRuleId");
				String notificationDeviceId = ReplyToJSON.extractPathFromJson(notifCommon, "$.deviceId");
				String notificationDateTime = ReplyToJSON.extractPathFromJson(notifCommon, "$.eventTime");

				JSONObject notificationRule = (JSONObject) GetNotificationRules.getNotificationRuleInfoById(notificationRuleId).get(0);

				JSONObject mongoDeviceFullRecords = 
						(JSONObject) GetDeviceFullRecordsFromMongo.getByDeviceIdAndDates(notificationDeviceId,notificationDateTime,notificationDateTime).get(0);
				
				checkBaseFieldsExists (notifCommon,-1,notificationRule,mongoDeviceFullRecords);
				assertContainsMngId("API:notifications_common.deviceId",ReplyToJSON.extractPathFromJson(notifCommon, "$.deviceId"), "notifications_common.deviceId не верен");
				
				//assertDateIsAfter("API:notifications.eventTime", notifCommon.get("eventTime").toString(), this.date,  	"eventTime не верен");
				
				assertEquals( "API:notifications.eventTime",dateTimeToUnixTime(notifCommon.get("eventTime").toString())   ,ReplyToJSON.extractPathFromJson(mongoDeviceFullRecords,"$.DeviceTime.$date"), "notifications.eventTime не верен");
				assertEquals( "API:notifications.address",notifCommon.get("address")   ,ReplyToJSON.extractPathFromJson(mongoDeviceFullRecords,"$.xgeo_Address"), "notifications.address не верен");
				assertEquals( "API:notifications.latitude",notifCommon.get("latitude").toString().substring(0, 15)   ,
							 ReplyToJSON.extractPathFromJson(mongoDeviceFullRecords,"$.Latitude").substring(0, 15), "notifications.latitude не верен");
				assertEquals( "API:notifications.longitude",notifCommon.get("longitude").toString().substring(0, 15) ,
						      ReplyToJSON.extractPathFromJson(mongoDeviceFullRecords,"$.Longitude").substring(0, 15), "notifications.longitude не верен");
					
			}else if(jsonApiReply.containsKey("vehicle")) {
				JSONObject notifVehicle = (JSONObject) jsonApiReply.get("vehicle");
				String notificationDeviceId = ReplyToJSON.extractPathFromJson(jsonApiReply , "$.common.deviceId");
				String notificationEventTime = ReplyToJSON.extractPathFromJson(jsonApiReply , "$.common.eventTime");
				
				List<JSONObject> deviceHistCol= 
						 GetDeviceHistory.getDeviceHistoryByDeviceIdAndDateTill( notificationDeviceId,  notificationEventTime);
				String resourceId= ReplyToJSON.extractPathFromJson(deviceHistCol.get(deviceHistCol.size()-1),"$.ResourceId.$oid");
				assertEquals("API:notifications.vehicle.type",ReplyToJSON.extractPathFromJson(deviceHistCol.get(deviceHistCol.size()-1),"$.RelatedResourceType"), 
							 "Vehicle","некорректный тип объекта, ожидался Vehicle");
				
				JSONObject mongoVehicleInfo = (JSONObject) GetVehicleInfoFromMongo.getVehicleInfoById(resourceId,"").get(0);

				assertEquals("API:notifications.vehicle_id",ReplyToJSON.extractPathFromJson(notifVehicle,"$.id"), ReplyToJSON.extractPathFromJson(mongoVehicleInfo, "$._id.$oid"),
						     "API:notifications.vehicle_id некорректен");
				assertEquals("API:notifications.vehicle_name",notifVehicle.get("name"), ReplyToJSON.extractPathFromJson(mongoVehicleInfo, "$.Name"), 
						"API:notifications.vehicle_name некорректен");
				assertEquals("API:notifications.vehicle_model",notifVehicle.get(CONST_MODEL),ReplyToJSON.extractPathFromJson(mongoVehicleInfo, "$.Model"),
						"API:notifications.vehicle_model некорректен");
				assertEquals("API:notifications.vehicle_regNumber",notifVehicle.get("regNumber"),ReplyToJSON.extractPathFromJson(mongoVehicleInfo, "$.Number"),  
						"API:notifications.vehicle_regNumber некорректен");
				
				assertEquals("API:notifications.vehicle_avatarUri",notifVehicle.get("avatarUri"),ReplyToJSON.extractPathFromJson(mongoVehicleInfo, "$.AvatarUri"),
						"API:notifications.vehicle_avatarUri некорректен");
				assertEquals("API:notifications.vehicle_type",notifVehicle.get("type"),ReplyToJSON.extractPathFromJson(mongoVehicleInfo, "$.Type"), 
							"API:notifications.vehicle_type некорректен");
				
				assertEquals("API:notifications.vehicle.viewCard_id",ReplyToJSON.extractPathFromJson(notifVehicle,"$.id"), ReplyToJSON.extractPathFromJson(mongoVehicleInfo, "$._id.$oid"),
					     "API:notifications.vehicle.viewCard_id некорректен");
				assertEquals("API:notifications.vehicle.viewCard_avatarUri",notifVehicle.get("avatarUri"), ReplyToJSON.extractPathFromJson(mongoVehicleInfo, "$.AvatarUri"), 
						"API:notifications.vehicle.viewCard_avatarUri некорректен");
			assertEquals("API:notifications.vehicle.viewCard_primaryTitle",notifVehicle.get("primaryTitle"), ReplyToJSON.extractPathFromJson(mongoVehicleInfo, "$.Name"), 
					"API:notifications.vehicle.viewCard_primaryTitle некорректен");
			
			if(mongoVehicleInfo.containsKey(CONST_MODEL)){
				assertEquals("API:vehicle_vehicleViewCard.secondaryTitle",
						ReplyToJSON.extractPathFromJson(notifVehicle,"$.vehicle.vehicleViewCard.secondaryTitle"),
						mongoVehicleInfo.get("mark")+" | "+mongoVehicleInfo.get(CONST_MODEL)+" | "+mongoVehicleInfo.get("number"),"vehicleViewCard.secondaryTitle не верен");
				} else{
					assertEquals("API:vehicle_vehicleViewCard.secondaryTitle",
							ReplyToJSON.extractPathFromJson(notifVehicle,"$.vehicle.vehicleViewCard.secondaryTitle"),
							mongoVehicleInfo.get("mark")+" | " +mongoVehicleInfo.get("number"),"vehicleViewCard.secondaryTitle не верен");
					}
			assertTrue("API:vehicle_vehicleViewCard.status",mongoVehicleInfo.containsKey("status"), "vehicle_vehicleViewCard.status отсутствует");
			
			assertEquals("API:notifications.vehicle.viewCard_relatedResourceType",ReplyToJSON.extractPathFromJson(notifVehicle,"$.vehicle.vehicleViewCard.relatedResourceType"),"Vehicle", 
						"API:notifications.vehicle.viewCard_relatedResourceType некорректен");
			
			/*	"vehicle": {
					"id": "593829b15532f71548deb095",
					"name": "Автобус 510030",
					CONST_MODEL: "510030",
					"regNumber": "У888УУ99",
					"avatarUri": "system/vehicle-default.png",
					"type": "Bus",
					"deviceId": "5919b86023a3530444bddfcd",
					"viewCard": {
						"id": "593829b15532f71548deb095",
						"avatarUri": "system/vehicle-default.png",
						"primaryTitle": "Автобус 510030",
						"secondaryTitle": "Автобус | 510030 | У888УУ99",
						"isOnline": true,
						"status": {
							"value": "Online",
							"statusValue": "В сети",
							"settingDate": "2017-06-30 12:22:00",
							"address": "г. Москва, ул. Москворечье, д.7"
						},
						"relatedResourceType": "Vehicle"
					}
				}*/
			
				
			}else {assertTrue(false, "not implemented assert for property!");}
			
			
			
			setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные полученных оповещений':" + e.toString());
		}
	}
	
	public void checkBaseFieldsExists(JSONObject notification, int arrIndex, JSONObject ruleApplied, JSONObject mongoDeviceFullRecords) throws Exception{
		String fieldPostfix = "";
		if (arrIndex != -1){
			fieldPostfix =  " ["+arrIndex+"]";
		}

//		"eventTime": "2017-06-21 10:35:36",

//		"address": "г.Москва, ул.Большая Лубянка, 13/16",
//		"latitude": 56.846625,
//		"longitude": 60.894127,

//		"deviceId": "58dbae91cc6b751bcc6806c3",
//		"notificationRuleId": "592542205532f31e14d2d21c"
		assertContainsMngId("API:notifications_Id"+fieldPostfix,ReplyToJSON.extractPathFromJson(notification, "$.id"), "notifications.id не верен");
		assertNotNull("API:notifications.eventName"+fieldPostfix, ReplyToJSON.extractPathFromJson(notification,"$.eventName"), "notifications.eventName отсутствует");
			
//		assertDateIsAfter("API:notifications.eventTime"+fieldPostfix, notification.get("eventTime").toString(), this.date,  	"eventTime не верен");
		String roadEventType = ReplyToJSON.extractPathFromJson(notification,PATH_ROADEVENTTYPE);
		assertNotNull(CONST_API_NOTIFICATIONS_ROADEVENTTYPE+fieldPostfix, roadEventType, "notifications.roadEventType отсутствует");
		log.debug("ruleApplied="+ruleApplied);
		log.debug("roadEventType="+roadEventType);
		assertEquals( CONST_API_NOTIFICATIONS_ROADEVENTTYPE,ruleApplied.get(CONST_ROADEVENTTYPE2), roadEventType, "notification.roadEventType не верен");
		
		

		assertNotNull("API:notifications.roadEventTypeName"+fieldPostfix, ReplyToJSON.extractPathFromJson(notification,"$.roadEventTypeName"), "notifications.roadEventTypeName отсутствует");
		assertEquals( "API:notifications.roadEventTypeName"+fieldPostfix,ruleApplied.get("Title")   ,notification.get(CONST_EVENTNAME), "notifications.roadEventTypeName не верен");	
		if (roadEventType.contains("Geozone")){
		assertNotNull("API:notifications.geozones"+fieldPostfix, ReplyToJSON.extractPathFromJson(notification,"$.geozones"), "notifications.geozones отсутствует");
		}
		String[] roadEventTypelookForLimitation = {"GeozoneStanding", "MileageExceedance", "TelematicDeviceMechanismOn", CONST_TRAFFICRULESPEEDLIMIT, "Discharge", "SharpSpeedup", "SharpTurn"};
		for (int j=0; j<roadEventTypelookForLimitation.length; j++) {
		        if (roadEventTypelookForLimitation[j].matches(roadEventType)) { // matches uses regex
		        	assertNotNull(CONST_API_NOTIFICATIONS_LIMITATION+fieldPostfix, ReplyToJSON.extractPathFromJson(notification,"$.limitation"), "notifications.limitation отсутствует");
		        	break;
		        }
		 }
		String[] roadEventTypelookForRecorded = {"GeozoneStanding", "MileageExceedance", CONST_TRAFFICRULESPEEDLIMIT,"UserSpeedlimit", "Discharge", "SharpSpeedup", "SharpTurn"};
		for (int j=0; j<roadEventTypelookForRecorded.length; j++) {
		        if (roadEventTypelookForRecorded[j].matches(roadEventType)) { // matches uses regex
		        	assertNotNull(CONST_API_NOTIFICATIONS_RECORDED+fieldPostfix, ReplyToJSON.extractPathFromJson(notification,"$.recorded"), "notifications.recorded отсутствует");
		        	break;
		        }
		 }
		if(roadEventType.equals("UserSpeedlimit")){
			String notifLimitation = notification.get(CONST_LIMITATION).toString();
			notifLimitation =notifLimitation.substring(0, notifLimitation.length()-5);
			String notifRecorded = notification.get(CONST_RECORDED).toString();
			notifRecorded =notifRecorded.substring(0, notifRecorded.length()-5);
			
			assertEquals( CONST_API_NOTIFICATIONS_LIMITATION,notifLimitation   ,ReplyToJSON.extractPathFromJson(ruleApplied,"$.RoadEventArguments.Speedlimit"), "notifications.limitation не верен");
			assertEquals( CONST_API_NOTIFICATIONS_RECORDED,notifRecorded  ,ReplyToJSON.extractPathFromJson(mongoDeviceFullRecords,"$.Speed"), "notifications.recorded не верен");
			assertTrue(Integer.parseInt(notifLimitation) < Integer.parseInt(notifRecorded) , "notifications скорость ниже ограничения, правило не должно было сработать");
		}
		if(roadEventType.equals(CONST_TRAFFICRULESPEEDLIMIT)){
			String notifLimitation = notification.get(CONST_LIMITATION).toString();
			notifLimitation =notifLimitation.substring(0, notifLimitation.length()-5);
			String notifRecorded = notification.get(CONST_RECORDED).toString();
			notifRecorded =notifRecorded.substring(0, notifRecorded.length()-5);
			assertEquals( CONST_API_NOTIFICATIONS_LIMITATION,notifLimitation   ,
					ReplyToJSON.extractPathFromJson(mongoDeviceFullRecords,"$.xgeo_Speedlimit")//	ReplyToJSON.extractPathFromJson(ruleApplied,"$.RoadEventArguments.Overspeed")
					, "notifications.limitation не верен");
			int ruleLimit = Integer.parseInt(ReplyToJSON.extractPathFromJson(ruleApplied,"$.RoadEventArguments.Overspeed"));
			assertEquals( CONST_API_NOTIFICATIONS_RECORDED,notifRecorded  ,ReplyToJSON.extractPathFromJson(mongoDeviceFullRecords,"$.Speed"), "notifications.recorded не верен");
			assertTrue(ruleLimit + Integer.parseInt(ReplyToJSON.extractPathFromJson(mongoDeviceFullRecords,"$.xgeo_Speedlimit"))
						<= Integer.parseInt(notifRecorded) , 
			"notifications скорость ниже ограничения, правило не должно было сработать");
		
		}
		
		
	}
	
	
	@Override
	public int removeFromSharedContainer() {
		int indexOfRemovedEntity=-1;
		if (!container.isEmpty() && container.containsKey(CONST_NOTIFICATIONS) && ! container.get(CONST_NOTIFICATIONS).isEmpty()) {
			ArrayList listOfIds = container.get(CONST_NOTIFICATIONS);
				// удаление из контейнера
				if (listOfIds.get(0) instanceof com.t1.core.api.Notification) {
					for (int i = 0; i < listOfIds.size(); i++) {
						if (((Notification) listOfIds.get(i)).notificationId.equals(this.notificationId)) {
							listOfIds.remove(i);
							indexOfRemovedEntity=i;
						}
					}
				}
		}
		return indexOfRemovedEntity;
	}
	
	@Override
	public void addToSharedContainer() {
		addToSharedContainer(-1);
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public void addToSharedContainer(int index) {
		if (SharedContainerInterface.getContainers().containsKey(CONST_NOTIFICATIONS) && ! SharedContainerInterface.getContainers().get(CONST_NOTIFICATIONS).isEmpty()) {
			ArrayList<Notification> existingArray = (ArrayList<Notification>) SharedContainerInterface.getContainers().get(CONST_NOTIFICATIONS);
			if(index>=0) {
				existingArray.add(index,this);
			} else {
				existingArray.add(this);
			}
			container.put(CONST_NOTIFICATIONS, existingArray);
		} else {
			ArrayList newArray = new ArrayList();
			newArray.add(this);
			container.put(CONST_NOTIFICATIONS, newArray);
		}
	}

	@Override
	public void updateInSharedContainer() {
		int index = this.removeFromSharedContainer();
		this.addToSharedContainer(index);
	}

}
	
