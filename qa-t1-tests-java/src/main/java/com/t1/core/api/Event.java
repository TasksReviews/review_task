package com.t1.core.api;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.mongodb.GetNotificationRules;

import ru.yandex.qatools.allure.annotations.Step;

public class Event  extends AbstractClass{
	private String host = "";
	private String url;
	private String apiMethod;
	private JSONObject urlParameters = new JSONObject();
	private String deviceId;

	protected JSONObject generatedEventReply = new JSONObject();
	protected ArrayList<JSONObject> generatedEventReplyArr = new ArrayList<>();
	protected JSONArray generatedEventArray = new JSONArray();
	private String resourceType;
	private String timeFrom;
	private String timeTo;
	private ArrayList<String> resourcesFilterInclude;
	protected JSONObject mongoDeviceFullRecords;
	protected List<String> allEventTypes = new ArrayList<>(Arrays.asList(  "UserSpeedLimit", "StopEvent", "IdleEvent",
			 "SharpTurn", "SharpTurnLeft", "SharpTurnRight", "SharpSpeedup", "SharpBraking", "PanicButton", "UserSpeedlimit"
			)); 

	private String dateStart ;
	private String dateEnd ;
	private List<String> eventFilter;
	private int skip ;
	private int limit ;
	private String eventType;
	private String resourceFilter;
	private String resourceName;
	private String resourceId;
	
	protected static String deviceIdStr = "deviceId";
	protected static String resourceIdStr = "resourceId";
	protected static String checkEventStr = "check event[";
	protected static String apiResourcesRoadEvents = "API:Resources.RoadEvents.[";
	protected static String roadEventTypePath = "$.roadEventType";
	protected static String deviceTimeDatePath = "$.DeviceTime.$date";
	protected static String notimplementedStr = "not implemented ";
	
	public String getEventType() {		return eventType;	}
	public void setEventType(String eventType) {		this.eventType = eventType;	}
	public String getDeviceId() {		return deviceId;	}
	public void setDeviceId(String deviceId) {		this.deviceId = deviceId;	}
	public String getDateStart() {		return dateStart;	}
	public void setDateStart(String dateStart) {		this.dateStart = dateStart;	}
	public String getDateEnd() {		return dateEnd;	}
	public void setDateEnd(String dateEnd) {		this.dateEnd = dateEnd;	}
	public int getSkip() {		return skip;	}
	public void setSkip(int skip) {		this.skip = skip;	}
	public int getLimit() {		return limit;	}
	public void setLimit(int limit) {		this.limit = limit;	}
	public String getResourceFilter() {		return resourceFilter;	}
	public void setResourceFilter(String resourceFilter) {		this.resourceFilter = resourceFilter;	}	public String getResourceName() {		return resourceName;	}
	public void setResourceName(String resourceName) {		this.resourceName = resourceName;	}
	public String getResourceId() {		return resourceId;	}
	public void setResourceId(String resourceId) {		this.resourceId = resourceId;	}
	public String getResourceType() {		return resourceType;	}
	public void setResourceType(String resourceType) {		this.resourceType = resourceType;	}
	public List<String> getEventFilter() {		return eventFilter;	}
	public void setEventFilter(List<String> eventFilter) {		this.eventFilter = eventFilter;	}
	
	public Event (){
		this.host = getProperty("host");
		this.url = host + apiMethod;
		this.urlParameters.clear();
		}
	
	public Event (String deviceid, String timeFrom, String timeTo){
		this.apiMethod = "/Notifications/RoadEventsGenerator";
		this.host = getProperty("host");
		this.url = host + apiMethod;
		this.urlParameters.clear();
		
		this.timeFrom=timeFrom;
		this.timeTo=timeTo;
		this.setDeviceId(deviceid);

		
		this.urlParameters.put("timeFrom", timeFrom);
		this.urlParameters.put("timeTo", timeTo);
		this.urlParameters.put(deviceIdStr, getDeviceId());
		
	
		
	}

 	
	
	@Step("запрос событий по {0}")
	public JSONObject requestRoadEvents (){
		JSONObject	receivedEvents = new JSONObject();
		apiMethod = "/Resources/RoadEvents";
		url = getProperty("host") + apiMethod;
		urlParameters.clear();
		if(this.getDeviceId()!= null && ! this.getDeviceId().equals("")){
			this.urlParameters.put(deviceIdStr, this.getDeviceId());
		}
		this.urlParameters.put(resourceIdStr, this.getResourceId());
		this.urlParameters.put("dateStart", this.getDateStart());
		this.urlParameters.put("dateEnd", this.getDateEnd());
		this.urlParameters.put("eventFilter", this.getEventFilter());
		this.urlParameters.put("skip", this.getSkip());
		this.urlParameters.put("limit", this.getLimit());
		
		try {
			String token = getCurAuthAccToken("authorizedAccount_t1client");
			assertTrue("token received",token != null,"для получения ТС необходимо пройти авторизацию");
			receivedEvents = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			checkResourcesRoadEventsAllFieldsReceived(receivedEvents);
		} catch (Exception e) {
			log.error("Test failed: " + e);
			assertTrue(false, e.toString());

		}
		return receivedEvents;
		

	}
	 public void checkResourcesRoadEventsAllFieldsReceived( JSONObject receivedEvents){
		List<JSONObject> eventsList = (List<JSONObject>) receivedEvents.get("events");
		try {
			if (this.getResourceId() != null) {
				assertEquals("API:resourceId", receivedEvents.get(resourceIdStr), this.getResourceId(), "resourceId не верен");
			}
			assertEquals("API:eventsTotalCount", Integer.parseInt(ReplyToJSON.extractPathFromJson(receivedEvents, "$.eventsTotalCount")), eventsList.size(), "resourceId не верен");
//			assertTrue("API:currentAddress", ReplyToJSON.extractPathFromJson(receivedEvents, "$.currentAddress").length() > 0, "currentAddress пуст");
			for (int i = 0; i < eventsList.size(); i++) {
				assertContains(checkEventStr+i+"]", eventsList.get(i).toJSONString(), "address", "field address wasnt found");
//				assertEquals("API:event["+i+"].relatedResourceName", eventsList.get(i).get("relatedResourceName"), this.resourceName, "relatedResourceName не верен");
//				assertEquals("API:event["+i+"].relatedResourceId", eventsList.get(i).get("relatedResourceId"), this.resourceId, "relatedResourceId не верен");
//				assertEquals("API:event["+i+"].relatedResourceType", eventsList.get(i).get("relatedResourceType"), this.resourceType, "relatedResourceType не верен");
				assertContains(checkEventStr+i+"]", eventsList.get(i).toJSONString(), "eventTime", "field eventTime wasnt found");
				assertContains(checkEventStr+i+"]", eventsList.get(i).toJSONString(), "latitude", "field latitude wasnt found");
				assertContains(checkEventStr+i+"]", eventsList.get(i).toJSONString(), "longitude", "field longitude wasnt found");
				assertContains(checkEventStr+i+"]", eventsList.get(i).toJSONString(), "roadEventType", "field roadEventType wasnt found");
			}
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
		}
		setToAllureChechkedFields();
	}
	
	public void checkResourceEvents (JSONObject receivedApiReply,List<JSONObject> insertedTrackPoints){
		 try {
			 JSONObject insertedTrackPoint;
				//из-за пробок события ХХ и стоянок решили растягивать на 1 точку до начала события и 1 после
		 if((this.getEventType() !=null) && (this.getEventType().contains("IdleEvent")|| this.getEventType().contains("StopEvent"))){
			 insertedTrackPoint= insertedTrackPoints.get(0);
		 } else{			 
			 insertedTrackPoint= insertedTrackPoints.get(insertedTrackPoints.size()-1);
		 }
			assertEquals("API:Resources.RoadEvents.deviceId ", 
						ReplyToJSON.extractPathFromJson(receivedApiReply, "$.deviceId"),this.getDeviceId(), "Resources.RoadEvents.deviceId не верен");
		 List<JSONObject> receivedEvents = (List)receivedApiReply.get("events");
		 assertEquals("API:Resources.RoadEvents.eventsTotalCount ", 
					Integer.parseInt(ReplyToJSON.extractPathFromJson(receivedApiReply, "$.eventsTotalCount")),receivedEvents.size(), "Resources.RoadEvents.eventsTotalCount не верен");

		 assertTrue("API:Resources.RoadEvents.eventsTotalCount ", ! receivedEvents.isEmpty(), "Resources.RoadEvents.eventsTotalCount не верен");

		for(int i=0;i<receivedEvents.size();i++){
			JSONObject event = receivedEvents.get(i);
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			 df.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));
			 Date mongoReceivedTime = new Date(Long.parseLong(ReplyToJSON.extractPathFromJson(insertedTrackPoint, "$.ReceivedTime.$date")));

			assertEquals(apiResourcesRoadEvents+i+"]eventTime", 
					ReplyToJSON.extractPathFromJson(event, "$.eventTime") , df.format(mongoReceivedTime) , "Resources.RoadEvents.eventTime не верен");
			
			if(this.getEventType().equals("SharpTurn")){
				assertTrue(apiResourcesRoadEvents+i+"]roadEventType", 
						ReplyToJSON.extractPathFromJson(event, roadEventTypePath).equals("SharpTurnRight") ||
						ReplyToJSON.extractPathFromJson(event, roadEventTypePath).equals("SharpTurnLeft"), "Resources.RoadEvents.roadEventType не верен");
			} else {
				assertEquals(apiResourcesRoadEvents+i+"]roadEventType", 
						ReplyToJSON.extractPathFromJson(event, roadEventTypePath) , this.getEventType(), "Resources.RoadEvents.roadEventType не верен");
			}
			if(this.resourcesFilterInclude!= null){
			assertEquals(apiResourcesRoadEvents+i+"]relatedResourceId", 
					ReplyToJSON.extractPathFromJson(event, "$.relatedResourceId") , this.resourcesFilterInclude.get(0), "Resources.RoadEvents.relatedResourceId не верен");
			} else {
				assertEquals(apiResourcesRoadEvents+i+"]relatedResourceId", 
						ReplyToJSON.extractPathFromJson(event, "$.relatedResourceId") , this.getResourceFilter(), "Resources.RoadEvents.relatedResourceId не верен");
			}
			if(this.getResourceType()!= null){
			assertEquals(apiResourcesRoadEvents+i+"]relatedResourceType", 
					ReplyToJSON.extractPathFromJson(event, "$.relatedResourceType") , this.getResourceType(), "Resources.RoadEvents.relatedResourceType не верен");
			}
			assertEquals(apiResourcesRoadEvents+i+"]relatedResourceName", 
					ReplyToJSON.extractPathFromJson(event, "$.relatedResourceName") , this.getResourceName(), "Resources.RoadEvents.relatedResourceName не верен");

			assertEquals(apiResourcesRoadEvents+i+"]latitude", 
					ReplyToJSON.extractPathFromJson(event, "$.latitude").substring(0, 10) , ReplyToJSON.extractPathFromJson(insertedTrackPoint, "$.Latitude").substring(0, 10), "Resources.RoadEvents.latitude не верен");

			assertEquals(apiResourcesRoadEvents+i+"]longitude", 
					ReplyToJSON.extractPathFromJson(event, "$.longitude").substring(0, 10) , ReplyToJSON.extractPathFromJson(insertedTrackPoint, "$.Longitude").substring(0, 10), "Resources.RoadEvents.longitude не верен");

			setToAllureChechkedFields();
		}
		 
		 		 
		 } catch (Exception e) {
			 log.error(ERROR,e);
			 setToAllureChechkedFields();
			}
	}


	@Step("Валидация: полученный JSON содержит данные с которыми отчет был запрошен")
	public static void checkBaseFieldsOfAnalyticIntervalResults( JSONArray jsonApiReplyArr, JSONObject initJson, String startTime, String endTime, String deviceId) {
		try {
			for (int i = 0; i < jsonApiReplyArr.size(); i++) {
				JSONObject jsonApiReply = (JSONObject) jsonApiReplyArr.get(i);
			
				assertDateIsBetween("API:AnalyticIntervalResults_startTime ["+i+"]", jsonApiReply.get("startTime").toString(), startTime, endTime, 	"startTime не верен");
				assertDateIsBetween("API:AnalyticIntervalResults_endTime ["+i+"]", jsonApiReply.get("endTime").toString(), startTime, endTime, 	"startTime не верен");
				
				assertEquals("API:AnalyticIntervalResults_deviceId", jsonApiReply.get(deviceIdStr), deviceId, "deviceId не верен");
				
				assertTrue("API:AnalyticIntervalResults_intervalSummary exists ["+i+"]", jsonApiReply.containsKey("intervalSummary"), "intervalSummary отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.tripsPartCount ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.tripsPartCount"), "intervalSummary.tripsPartCount отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.tripsTotalDurationSecs ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.tripsTotalDurationSecs"), "intervalSummary.tripsTotalDurationSecs отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.tripsTotalDurationTimespan ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.tripsTotalDurationTimespan"), "intervalSummary.tripsTotalDurationTimespan отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.machineHoursTotalSecs ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.machineHoursTotalSecs"), "intervalSummary.machineHoursTotalSecs отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.machineHoursTotalTimespan ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.machineHoursTotalTimespan"), "intervalSummary.machineHoursTotalTimespan отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.idleTotalCount ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.idleTotalCount"), "intervalSummary.idleTotalCount отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.idleTotalDurationSecs ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.idleTotalDurationSecs"), "intervalSummary.idleTotalDurationSecs отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.idleTotalDurationTimespan ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.idleTotalDurationTimespan"), "intervalSummary.idleTotalDurationTimespan отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.stopsTotalCount ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.stopsTotalCount"), "intervalSummary.stopsTotalCount отсутствует");
				String stopsTotalDurationSecs = ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.stopsTotalDurationSecs");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.stopsTotalDurationSecs ["+i+"]", stopsTotalDurationSecs, "intervalSummary.stopsTotalDurationSecs отсутствует");
				if(!stopsTotalDurationSecs.equals("0") ){
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.stopsTotalDurationTimespan ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.stopsTotalDurationTimespan"), "intervalSummary.stopsTotalDurationTimespan отсутствует");
				}
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.totalMileage ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.totalMileage"), "intervalSummary.totalMileage отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.maxSpeedPoint ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.maxSpeedPoint"), "intervalSummary.maxSpeedPoint отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.endorsements ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.endorsements"), "intervalSummary.endorsements отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.score ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.score"), "intervalSummary.score отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.fuelSpentTotal ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.fuelSpentTotal"), "intervalSummary.fuelSpentTotal отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.visitGeozoneTotalCount ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.visitGeozoneTotalCount"), "intervalSummary.visitGeozoneTotalCount отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.visitGeozoneTotalDurationSecs ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.visitGeozoneTotalDurationSecs"), "intervalSummary.visitGeozoneTotalDurationSecs отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.lossCommunicationTotalCount ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.lossCommunicationTotalCount"), "intervalSummary.lossCommunicationTotalCount отсутствует");
				assertNotNull("API:AnalyticIntervalResults_intervalSummary.lossCommunicationTotalDurationSecs ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.intervalSummary.lossCommunicationTotalDurationSecs"), "intervalSummary.lossCommunicationTotalDurationSecs отсутствует");
			
				assertNotNull("API:AnalyticIntervalResults_tripsParts ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.tripsParts"), "tripsParts отсутствует");
				assertNotNull("API:AnalyticIntervalResults_events ["+i+"]", ReplyToJSON.extractPathFromJson(jsonApiReply,"$.events"), "events отсутствует");
				
				assertContainsMngId("API:AnalyticIntervalResults_Id ["+i+"]",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.id"), "AnalyticIntervalResults.id не верен");
			
			}

			setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные 'проанализированных интервалов':" + e.toString());
		}
		
	}
	

	@Step("Валидация: полученный JSON содержит данные с которыми событие было запрошено")
	public void checkBaseFieldsOfReceivedEvent(List<JSONObject> jsonMongoReply, List<JSONObject> jsonApiReply, JSONObject initJson) {

		try {
			for (Object item :  jsonApiReply ){				
				JSONObject trackEvent   = (JSONObject) ((JSONObject)item).get("trackEvent");
				JSONObject notification = (JSONObject) ((JSONObject)item).get("notification");
					String eventDeviceTimeUnix = 	dateTimeToUnixTime(ReplyToJSON.extractPathFromJson(trackEvent, "$.eventDeviceTime"));
					for (int j=0; j<jsonMongoReply.size();j++){
						String strMongoEventDeviceTime= ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j), deviceTimeDatePath);
						if(strMongoEventDeviceTime.equals(eventDeviceTimeUnix) ){
							
							assertEquals("API:trackEvent.eventDeviceTime ["+j+"]",eventDeviceTimeUnix, 
									ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j), deviceTimeDatePath),"trackEvent.eventDeviceTime не верен");	
							assertEquals("API:trackEvent.relatedRecordId ["+j+"]",trackEvent.get("relatedRecordId"), 
												ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j),"$._id.$oid"),"trackEvent.relatedRecordId не верен");
							
							assertEquals("API:trackEvent.latitude ["+j+"]",trackEvent.get("latitude").toString().substring(0, 15), 
									jsonMongoReply.get(j).get("Latitude").toString().substring(0, 15),"trackEvent.latitude не верен\r\n"+jsonMongoReply.get(j));
							assertEquals("API:trackEvent.longitude ["+j+"]",trackEvent.get("longitude").toString().substring(0, 15),
									jsonMongoReply.get(j).get("Longitude").toString().substring(0, 15),"trackEvent.longitude не верен");
							
							assertEquals("API:notification.deviceId ["+j+"]",notification.get(deviceIdStr).toString().substring(0, 15),
									ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j),"$.DeviceId.$oid").substring(0, 15),"notification.deviceId не верен");
							
							
							assertContainsMngId("API:notification.notificationRuleId", notification.get("notificationRuleId").toString(), 
											 "notification.notificationRuleId не верен");
							JSONObject ruleApplied = (JSONObject) GetNotificationRules.getNotificationRuleInfoById("594a9b655532f3063cdc4c74").get(0);
							
							Calendar cal = Calendar.getInstance();
							cal.setTime( new Date(Long.parseLong(strMongoEventDeviceTime)*1000L)); 
							
							assertContains("API:notification.notificationTime",Arrays.asList(ruleApplied.get("DaysOfWeek"))   ,cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US), 
									"notification.DayOfWeek не верен");	
							
							assertEquals("API:notification.Title",ruleApplied.get("Title")   ,notification.get("eventName"), "notification.Title не верен");	
							  
							assertEquals("API:notification.eventDeviceTime ["+j+"]",dateTimeToUnixTime(ReplyToJSON.extractPathFromJson(notification, "$.eventTime")), 
									ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j), deviceTimeDatePath),"notification.eventTime не верен");	
							
							assertEquals("API:notification.roadEventType",ruleApplied.get("RoadEventType"), notification.get("roadEventType"), "notification.roadEventType не верен");
							
							assertContains("API:notification.resourceId",Arrays.asList(ReplyToJSON.extractPathFromJson(ruleApplied,"$.ResourcesFilter.Include")) , 
														notification.get(resourceIdStr).toString(), "notification.resourceId не верен");
							assertContains("API:trackEvent.relatedResourceId",Arrays.asList(ReplyToJSON.extractPathFromJson(ruleApplied,"$.ResourcesFilter.Include")) , 
									trackEvent.get("relatedResourceId").toString(), "trackEvent.relatedResourceId не верен");
							
							String ruleRoadEventArgumentType =  ReplyToJSON.extractPathFromJson(ruleApplied,"$.RoadEventArguments._t");
							String ruleLimitation = "";
							if(ruleRoadEventArgumentType.equals("TrafficRulesSpeedLimitArguments")){
								ruleLimitation =  ReplyToJSON.extractPathFromJson(ruleApplied,"$.RoadEventArguments.Overspeed");
							
//							DischargeArguments
//							GeozonesEntranceArguments
//							IdleEventArguments
//							MileageExceedanceArguments
//							SharpMoveArguments
//							StopEventArguments
//							TelematicDeviceMechanismTriggeringArguments
							String notifLimit = notification.get("limitation").toString();
							assertEquals("API:notification.limitation",ruleLimitation ,  notifLimit.substring(0, notifLimit.length()-5), "notification.limitation не верен");
							String notifRecorded = notification.get("recorded").toString();
							notifRecorded =notifRecorded.substring(0, notifRecorded.length()-5);
							assertEquals("API:notification.recorded ["+j+"]",jsonMongoReply.get(j).get("Speed").toString() ,notifRecorded  , "notification.resourceId не верен");

//							assertTrue("API:notification.recorded ["+j+"]",limitSet < Integer.parseInt(notifRecorded) , "записанная скорость:"+notifRecorded+" меньше установленного лимита:"+limitSet);
							
							
							}
							else if(ruleRoadEventArgumentType.equals("DischargeArguments")){assertTrue(false,notimplementedStr+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("GeozonesEntranceArguments")){assertTrue(false,notimplementedStr+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("IdleEventArguments")){assertTrue(false,notimplementedStr+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("MileageExceedanceArguments")){assertTrue(false,notimplementedStr+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("SharpMoveArguments")){assertTrue(false,notimplementedStr+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("StopEventArguments")){assertTrue(false,notimplementedStr+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("TelematicDeviceMechanismTriggeringArguments")){
								assertTrue(false,notimplementedStr+ruleRoadEventArgumentType);
							}
							else {
								assertTrue(false,notimplementedStr+ruleRoadEventArgumentType);
							}
							break;
						}
					}
			}
				setToAllureChechkedFields();
			}
		 catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные события:" + e.toString());
		}
	}

}
	
