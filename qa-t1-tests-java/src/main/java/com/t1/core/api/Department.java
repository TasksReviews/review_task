package com.t1.core.api;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.jayway.jsonpath.JsonPath;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetDepartmentInfoFromMongo;
import com.t1.core.mongodb.GetInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Step;

public class Department  extends AbstractClass implements SharedContainerInterface{

	private JSONObject urlParameters = new JSONObject();
	protected JSONObject initJson;
	private String token = null;
	
	private String departmentId;

	private String departmentName;
	protected ArrayList<Employee> employees;

	private String description;

/*	
 	private String  department;
 	private String employeeId;
	private String groupId;
	private String group;
	private String geozones;
	private String geoZoneIds;
	private String vinCode;
	private String  isDeactivated;
	private Employee employeesObject;
	*/
	private String address;
	private String phone;
	private String email;
	

	
	public String getAddress() {		return address;	}
	public void setAddress(String address) {		this.address = address;	}
	public String getDescription() {		return description;	}
	public void setDescription(String description) {		this.description = description;	}
	public String getPhone() {		return phone;	}
	public void setPhone(String phone) {		this.phone = phone;	}
	public String getEmail() {		return email;	}
	public void setEmail(String email) {		this.email = email;	}
	public String getDepartmentId() {		return departmentId;	}
	public void setDepartmentId(String departmentId) {		this.departmentId = departmentId;	}
	public String getDepartmentName() {		return departmentName;	}
	public void setDepartmentName(String departmentName) {		this.departmentName = departmentName;	}
	
	public JSONObject getUrlParameters() {		return urlParameters;	}
	public void setUrlParameters(JSONObject urlParameters) {		this.urlParameters = urlParameters;	}
	public void putUrlParameters(Object key, Object value) {		this.urlParameters.put(key, value);	}
	
	private  static final String CONST_DEPARTMENT = "department";
	private  static final String CONST_DEPARTMENTS = "departments";
	private  static final String CONST_DEPARTMENT_NAME = "department_name";
	private  static final String CONST_EMPLOYEES = "employees";
	private  static final String CONST_PHONE_WRONG = "phone не верен";
	private  static final String CONST_EMAIL_WRONG = "email не верен";
	private  static final String CONST_API_EMPLOYEE = "API:employee";
	private  static final String CONST_TOKEN_RECEIVED = "token received";
	private  static final String CONST_USERTOKEN = "userToken";
	
	public Department (){
		initJson = InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_DEPARTMENT,getProperty("stdPattern"));
		this.urlParameters.clear();
		this.setDepartmentName(initJson.get(CONST_DEPARTMENT_NAME).toString());
		this.employees = SharedContainer.getObjectsFromSharedContainer(1,CONST_EMPLOYEES);
		assertTrue(this.employees.get(0).getEmployeeId() != null,	"для создания Департамента необходимо создать\\получить не менее 1 сотрудника");
		this.urlParameters.put("name", this.getDepartmentName());
		this.urlParameters.put(CONST_EMPLOYEES, this.employees);
		
		}

	public Department(Boolean allFields, ArrayList<Employee> employeesObjects) {
		ArrayList<String> employeesIds = new ArrayList<>();
		initJson = InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_DEPARTMENT,getProperty("stdPattern"));
		this.urlParameters.clear();
		this.employees = employeesObjects;
	//	assertTrue(this.employees.get(0).employeeId != null,	"для создания Департамента необходимо создать\\получить не менее 1 сотрудника");
		this.setDepartmentName(initJson.get(CONST_DEPARTMENT_NAME).toString());
		this.urlParameters.put("name", this.getDepartmentName());
		for(Employee employee: employeesObjects){
			employeesIds.add(
					employee.getEmployeeId());
		}
		this.urlParameters.put(CONST_EMPLOYEES, employeesIds);

		if (allFields) {
			setAllDepertmentFields();
		}

	}
	
	public Department(Boolean allFields, String pattern, ArrayList<Employee> employeesObjects) {
	ArrayList employeesIds = new ArrayList();
		this.employees = employeesObjects;
	//	assertTrue(this.employees.get(0).employeeId != null,	"для создания Департамента необходимо создать\\получить не менее 1 сотрудника");
		initJson = InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_DEPARTMENT,pattern);

		this.setDepartmentName(initJson.get(CONST_DEPARTMENT_NAME).toString());
		this.urlParameters.put("name", this.getDepartmentName());
		
		for(Employee employee: employeesObjects){
			employeesIds.add(employee.getEmployeeId());
		}
		this.urlParameters.put(CONST_EMPLOYEES, employeesIds);

		if (allFields) {
			setAllDepertmentFields();
		}

	}
	
	
	@Step("Создание нового подразделения")
	public List<org.json.simple.JSONObject> createNewDepartment() {
		List<org.json.simple.JSONObject> mongodepartmentInfo = null;
		try {
			String 	url = getProperty("host")+"/Department/Create";
			token = getToken(CONST_USERTOKEN);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для создания Департамента необходимо пройти авторизацию");
			org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			// TODO: add here assertions reply is OK and data exists;
			
			setDepartmentId(ReplyToJSON.extractPathFromJson(registrationReply, "$.id"));
			TearDownExecutor.addDepartmentToTeardown(this.getDepartmentId());
			// get account info from mongo
			mongodepartmentInfo = GetDepartmentInfoFromMongo.getDepartmentInfoById(getDepartmentId(),"");
//			// compare mongo with input
			checkRegistredDepartment(mongodepartmentInfo.get(0), registrationReply, this.initJson, this.employees);
		} catch (Exception e) {
			log.error("Test failed: " + e);
			assertTrue(false, e.toString());

		}
		return mongodepartmentInfo;
	}
	
	@Step("Изменение подразделения")
	public List<org.json.simple.JSONObject> updateDepartment() {
		List<org.json.simple.JSONObject> mongodepartmentInfo = null;
		String url = getProperty("host") + "/Department/Update";
		assertTrue(this.getDepartmentId()!=null, "необходимо указать id изменяемого подразделения");
		urlParameters.put("id", this.getDepartmentId());
		
		try {
			token = getToken(CONST_USERTOKEN);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для изменения Подразделения необходимо пройти авторизацию");
			org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			// TODO: add here assertions reply is OK and data exists;
			
			setDepartmentId(ReplyToJSON.extractPathFromJson(registrationReply, "$.id"));

			// get account info from mongo
			mongodepartmentInfo = GetDepartmentInfoFromMongo.getDepartmentInfoById(getDepartmentId(),"");
//			// compare mongo with input
			checkRegistredDepartment(mongodepartmentInfo.get(0), registrationReply, this.initJson, this.employees);
		} catch (Exception e) {
			log.error("Test failed: " + e);
			assertTrue(false, e.toString());
		}
		return mongodepartmentInfo;
	}
	
	@Step("Удаление Подразделения {0}")
	public static void deleteDepartment(String departmentId) {
		String url = getProperty("host") + "/department/delete/"+departmentId;
		try {
		String	token = getToken(CONST_USERTOKEN);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для удаления Подразделения необходимо пройти авторизацию");
			
			SendRequestToAPI.sendPostRequest( url, "", token,false,true);
			checkDepartmentDeleted(departmentId);
			
			SharedContainer.removeFromContainer(CONST_DEPARTMENTS,  departmentId);
			TearDownExecutor.removeEntityFromTeardown(departmentId,CONST_DEPARTMENTS);

			
		} catch (Exception e) {
			log.error("Test failed: wasn't able to delete department, remove it from containers anyway!",e);
			SharedContainer.removeFromContainer(CONST_DEPARTMENTS,departmentId);
			TearDownExecutor.removeEntityFromTeardown(departmentId,CONST_DEPARTMENTS);
			assertTrue(false, e.toString());

	}
}

	private static void checkDepartmentDeleted(String departmentId) {
	List<JSONObject> mongoDepartment =	(List<JSONObject>)GetDepartmentInfoFromMongo.getDepartmentInfoById(departmentId);
	assertTrue("DB:department удален из коллекции Departments",mongoDepartment.isEmpty(), "удаленное подразделение было найдено в Departments:"+mongoDepartment);
	List<JSONObject> vehiclesRelatedToDepartment =	(List<JSONObject>)GetInfoFromMongo.getInfoByFieldAndIdValue("Vehicles", "DepartmentId", departmentId, "");
	assertTrue("DB:удалены связи подразделения с ТС",vehiclesRelatedToDepartment.isEmpty(), 
			"были найдены связи с удаленным поразделением в Vehicles:"+vehiclesRelatedToDepartment);
	
	List<JSONObject> employeesRelatedToDepartment =	(List<JSONObject>)GetInfoFromMongo.getInfoByFieldAndIdValue("Employees", "DepartmentId", departmentId, "");
	assertTrue("DB:удалены связи подразделения с Сотрудниками",employeesRelatedToDepartment.isEmpty(), 
			"были найдены связи с удаленным поразделением в Employees:"+employeesRelatedToDepartment);
	setToAllureChechkedFields();
	}

	@Step("Валидация: полученный JSON содержит данные с которыми Подразделение было запрошено")
	public void checkRegistredDepartment(JSONObject jsonMongoReply, JSONObject jsonApiRegistrationReply, JSONObject initJson, ArrayList<Employee> employees) {
	
		try {
			assertEquals("DB:department_Id",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$._id.$oid"), this.getDepartmentId(), "departmentId не верен");
			assertEquals("API:department_Id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.id"), this.getDepartmentId(),"departmentId не верен");

			if (this.getDepartmentName() != null) {
				assertEquals("DB:department_name",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Name"), this.getDepartmentName(), "name не верен");
				assertEquals("API:department_name",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.name"), this.getDepartmentName(),"name не верен");
			}
			if (this.getAddress() != null) {
				assertEquals("DB:department_address",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Address"), this.getAddress(), "address не верен");
				assertEquals("API:department_address",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.address"), this.getAddress(),	"address не верен");
			}

			if (this.getDescription() != null) {
				assertEquals("DB:department_description",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Description"), this.getDescription(), "description не верен");
				assertEquals("API:department_description",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.description"), this.getDescription(),"description не верен");
			}

			if (this.getPhone() != null) {
				assertEquals("DB:department_phone",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Phone"), this.getPhone(),	CONST_PHONE_WRONG);
				assertEquals("API:department_phone",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.phone"), this.getPhone(),CONST_PHONE_WRONG);
			}
			if (this.getEmail() != null) {
				assertEquals("DB:department_email",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Email"), this.getEmail(),CONST_EMAIL_WRONG);
				assertEquals("API:department_email",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.email"),this.getEmail(), CONST_EMAIL_WRONG);
			}
			
			JSONArray employeesAPIArray = JsonPath.read(jsonApiRegistrationReply, "$.employees");
			log.debug(employeesAPIArray);
			assertEquals("API:employee total number",employeesAPIArray.size(),employees.size(), "количество сотрудников не верено, ожидалось"+employeesAPIArray.size());
			for (int i=0; i< employees.size(); i++){
				log.debug("start checking employee "+i);
				
				JSONObject employeeAPI = getEmployeeById(employeesAPIArray,employees.get(i).getEmployeeId());
				
				if (employees.get(i).getFirstname() != null) {
					assertEquals(CONST_API_EMPLOYEE+i+"_firstname",ReplyToJSON.extractPathFromJson(employeeAPI, "$.firstName"),employees.get(i).getFirstname(), "firstname не верен");
				}
				if (employees.get(i).getLastname() != null) {
					assertEquals(CONST_API_EMPLOYEE+i+"_lastname",ReplyToJSON.extractPathFromJson(employeeAPI, "$.lastName"),employees.get(i).getLastname(), "lastname не верен");
				}
				if (employees.get(i).getPhone() != null) {
					assertEquals(CONST_API_EMPLOYEE+i+"_phone",ReplyToJSON.extractPathFromJson(employeeAPI, "$.phone"), employees.get(i).getPhone(),CONST_PHONE_WRONG);
				}
				if (employees.get(i).getEmail() != null) {
					assertEquals(CONST_API_EMPLOYEE+i+"_email",ReplyToJSON.extractPathFromJson(employeeAPI, "$.email"), employees.get(i).getEmail(),CONST_EMAIL_WRONG);
				}
				if (employees.get(i).getEmployeeType() != null) {
					assertEquals(CONST_API_EMPLOYEE+i+"_type",ReplyToJSON.extractPathFromJson(employeeAPI, "$.type"), employees.get(i).getEmployeeType(),"type не верен");
				}
				if (employees.get(i).getStatus() != null) {
					assertEquals(CONST_API_EMPLOYEE+i+"_status",ReplyToJSON.extractPathFromJson(employeeAPI, "$.status"),employees.get(i).getStatus(), "status не верен");
				}
				if (employees.get(i).getAvatarUri() != null) {
					assertContains(CONST_API_EMPLOYEE+i+"_avatarUri",ReplyToJSON.extractPathFromJson(employeeAPI, "$.avatarUri"),employees.get(i).getAvatarUri(), "avatarUri не верен");
				}
				setToAllureChechkedFields();
			}
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные новогого подразделения:" + e.toString());
		}
	}
	
	public void setAllDepertmentFields(){
		this.setAddress(initJson.get("address").toString());
		this.setDescription(initJson.get("description").toString());
		this.setPhone(initJson.get("phone").toString());
		this.setEmail(initJson.get("email").toString());


		this.urlParameters.put("address", this.getAddress());
		this.urlParameters.put("description", this.getDescription());
		this.urlParameters.put("phone", this.getPhone());
		this.urlParameters.put("email", this.getEmail());
	}
	
	
	public JSONObject getEmployeeById(List<JSONObject> objList, String expectedId) {
		try {
			for (int i = 0; i < objList.size(); i++) {
				String foundId = ReplyToJSON.extractPathFromJson(objList.get(i), "$.id");
				if (expectedId.equals(foundId)) {
					return objList.get(i);
				}
			}
		} catch (Exception e) {
			log.error(ERROR,e);
		}
		return null;
	}
	
	
	@Override
	public int removeFromSharedContainer() {
		int indexOfRemovedEntity=-1;
		if (!container.isEmpty() && container.containsKey(CONST_DEPARTMENTS) && ! container.get(CONST_DEPARTMENTS).isEmpty()) {
			ArrayList listOfIds = container.get(CONST_DEPARTMENTS);
				// удаление из контейнера
				if (listOfIds.get(0) instanceof com.t1.core.api.Department) {
					for (int i = 0; i < listOfIds.size(); i++) {
						if (((Department) listOfIds.get(i)).getDepartmentId().equals(this.getDepartmentId())) {
							listOfIds.remove(i);
							indexOfRemovedEntity=i;
						}
					}
				}
		}
		return indexOfRemovedEntity;
	}
	
	@Override
	public void addToSharedContainer() {
		addToSharedContainer(-1);
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public void addToSharedContainer(int index) {
		if (SharedContainerInterface.getContainers().containsKey(CONST_DEPARTMENTS) && ! SharedContainerInterface.getContainers().get(CONST_DEPARTMENTS).isEmpty()) {
			ArrayList<Department> existingArray = (ArrayList<Department>) SharedContainerInterface.getContainers().get(CONST_DEPARTMENTS);
			if(index>=0) {
				existingArray.add(index,this);
			} else {
				existingArray.add(this);
			}
			container.put(CONST_DEPARTMENTS, existingArray);
		} else {
			ArrayList newArray = new ArrayList();
			newArray.add(this);
			container.put(CONST_DEPARTMENTS, newArray);
		}
	}

	@Override
	public void updateInSharedContainer() {
		int index = this.removeFromSharedContainer();
		this.addToSharedContainer(index);
	}


}
	
