package com.t1.core.api;

import java.util.List;

import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.SendRequestToAPI;

import ru.yandex.qatools.allure.annotations.Step;

public class TelematicsTrack extends AbstractClass{
	protected JSONObject urlParameters = new JSONObject();
	private String dateStart ;
	private String dateEnd ;
	protected List<String> events;
	protected List<String> resourceIds;
	private boolean showPlayerInfo;
	
	
	public TelematicsTrack(List<String> resourceIds,List<String> events, String dateStart,String dateEnd,boolean showPlayerInfo ){
		this.resourceIds=resourceIds;
		this.events=events;
		this.dateStart=dateStart;
		this.dateEnd=dateEnd;
		this.showPlayerInfo = showPlayerInfo;
		urlParameters.clear();
//		this.urlParameters.put("resourceId", this.resourceId);
		this.urlParameters.put("resourceIds", this.resourceIds);
		this.urlParameters.put("dateStart", this.dateStart);
		this.urlParameters.put("dateEnd", this.dateEnd);
		this.urlParameters.put("events", this.events);
		this.urlParameters.put("showPlayerInfo", this.showPlayerInfo);
	}
	
	@Step("запрос поездки по {0}")
	public JSONObject requestTelematicsTrack() {
		JSONObject	receivedTrack = new JSONObject();
		String url = getProperty("host") + "/Telematics/TracksNew";
		
		try {
			String token = getCurAuthAccToken("authorizedAccount_t1client");
			assertTrue("token received",token != null,"для получения ТС необходимо пройти авторизацию");
			receivedTrack = (JSONObject) SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			//TODO: add here asserts!
			checkTelematicsTracksAllFieldsReceived(receivedTrack);
		} catch (Exception e) {
			log.error("Test failed: " + e);
			assertTrue(false, e.toString());

		}
		return receivedTrack;

	}

	private void checkTelematicsTracksAllFieldsReceived(JSONObject receivedTrack) {
		// TODO add here asserts!
		log.warn("method checkTelematicsTracksAllFieldsReceived has no asserts for /Telematics/Tracks !");
		
	}

}
