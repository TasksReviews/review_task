package com.t1.core.api;

import static com.mongodb.client.model.Filters.eq;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.common.annotations.Beta;
import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetEmployeeInfoFromMongo;
import com.t1.core.mongodb.GetGroupInfoFromMongo;
import com.t1.core.mongodb.GetInfoFromMongo;
import com.t1.core.mongodb.GetVehicleInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Step;

public class Group extends AbstractClass implements SharedContainerInterface{
	private String url;
	private String apiMethod;
	private String customerId;
	private String groupId;
	private String name;
	private String color;
	private ArrayList<String> objects= new ArrayList<>();
	private ArrayList<HashMap<String,String>> resources = new ArrayList<>();
	private Long created;
	private Long updated;
	
	private JSONObject initJson;
	protected HashMap initMap ;
	private JSONObject urlParameters = new JSONObject();

	private String token = null;

	public String getGroupId() {		return groupId;	}
	public void setGroupId(String groupId) {		this.groupId = groupId;	}
	public String getColor() {		return color;	}
	public void setColor(String color) {		this.color = color;	}
	public String getName() {		return name;	}
	public void setName(String name) {		this.name = name;	}
	public String getCustomerId() {		return customerId;	}
	public void setCustomerId(String customerId) {		this.customerId = customerId;	}
	public JSONObject getUrlParameters() {		return urlParameters;	}
	public void setUrlParameters(JSONObject urlParameters) {		this.urlParameters = urlParameters;	}
	public void putUrlParameters(Object key, Object value) {		this.urlParameters.put(key, value);	}
	public JSONObject getInitJson() {		return initJson;	}
	public void setInitJson(JSONObject initJson) {		this.initJson = initJson;	}
	public ArrayList<String> getObjects() {		return objects;	}
	public void setObjects(ArrayList<String> objects) {		this.objects = objects;	}
	public ArrayList<HashMap<String,String>> getResources() {		return resources;	}
	public void setResources(ArrayList<HashMap<String,String>> resources) {		this.resources = resources;	}
	
	
	private  static final String CONST_STDPATTERN = "stdPattern";
	private  static final String CONST_GROUP = "group";
	private  static final String CONST_GROUPS = "Groups";
	private  static final String CONST_GROUPS2 = "groups";
	private  static final String CONST_GROUPID = "groupId";
	private  static final String CONST_RESOURCE_TYPE = "resourceType" ;
	private  static final String CONST_COLOR = "color";
	private  static final String CONST_AUTH_T1CLIENT = "authorizedAccount_t1client";
	private  static final String CONST_TOKEN_RECEIVED = "token received";
	private  static final String CONST_ADM_PANEL_HOST = "adminPanelHost";
	private  static final String CONST_AUTH_ADMPANEL = "authorizedAccount_admPanel";

	private  static final String CONST_UPDATED = "updated";
	private  static final String CONST_EMPLOYEE = "Employee";
	private  static final String CONST_EMPLOYEES = "employees";
	private  static final String CONST_CUSTOMERID = "CustomerId";
	private  static final String CONST_VEHICLE = "Vehicle";
	private  static final String CONST_VEHICLES = "vehicles";
	private  static final String CONST_NAME_WRONG = "name не верен";
	private  static final String CONST_COLOR_WRONG = "color не верен" ;
	private  static final String CONST_TEST_FAILED = "Test failed: ";
	private  static final String PATH_ID = "$._id.$oid";
	private  static final String CONST_VEHICLES_COUNT_STR = "].vehiclesCount";
	private  static final String CONST_API_GROUP_STR = "API:group[" ;
	private  static final String WARN_NOT_READY = "not implemented";
	
	public Group (){
		setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_GROUP,getProperty(CONST_STDPATTERN)));
		setMinimalFields(getInitJson());
		}

	public Group(String pattern) {
		setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_GROUP,getProperty(pattern)));
		setMinimalFields(getInitJson());
	}
	
	public Group(ArrayList<HashMap<String,String>> objects) {
		setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_GROUP,getProperty(CONST_STDPATTERN)));
		for(HashMap<String,String> object : objects){
			
			addObjectToGroup (object.get("id"), object.get(CONST_RESOURCE_TYPE));
		}
		setMinimalFields(getInitJson());

	}
	
	public Group(String objectId, String objectType) {
		setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_GROUP,getProperty(CONST_STDPATTERN)));
		addObjectToGroup (objectId, objectType);
		setMinimalFields(getInitJson());


	}

	public Group(String namePattern, ArrayList<HashMap<String,String>> objects) {
		setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_GROUP,getProperty(namePattern)));
		for(HashMap<String,String> object : objects){
			addObjectToGroup (object.get("id"), object.get(CONST_RESOURCE_TYPE));
		}
		setMinimalFields(getInitJson());

	}

	public void setMinimalFields(JSONObject initMap){
		this.setName(initMap.get("name").toString());
		this.setColor(initMap.get(CONST_COLOR).toString());

		this.urlParameters.clear();
		this.putUrlParameters("name", this.getName());
		this.putUrlParameters(CONST_COLOR,  this.getColor());
		this.putUrlParameters("objects", this.getObjects());
		this.putUrlParameters("resources",  this.getResources());
	

	}

	
	public void addObjectToGroup(String objectId, String objectType){
		HashMap<String,String> resource = new HashMap<>();
		getObjects().add(objectId);
		resource.put("id", objectId);
		resource.put(CONST_RESOURCE_TYPE, objectType);
		getResources().add(resource);
	}

	public void removeObjectInGroup(String objectId){
		getObjects().remove(objectId);
		Iterator it = getResources().iterator();
		while (it.hasNext())
		{
		    HashMap<String,String> resource = (HashMap<String,String>) it.next();
		    if(resource.get("id").equals(objectId)){
		    	it.remove();
		    	break;
		    }
		}
	}

	@Step("запрос создания новой группы")
	public JSONObject addGroupInClientPanel() {
		url = getProperty("host") + "/OmGroups/Add";
		token = getCurAuthAccToken(CONST_AUTH_T1CLIENT);
		return groupAdd(url, token);
	}

	@Step("запрос создания новой группы в панели администратора")
	public JSONObject addGroupInAdminPanelResources(){
		url = getProperty(CONST_ADM_PANEL_HOST) + "/Api/Admin/Groups";
		token = getCurAuthAccToken(CONST_AUTH_ADMPANEL);
		return groupAdd(url, token);
	}
	
	private JSONObject groupAdd(String url, String token){
		try {
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для создания Группы необходимо пройти авторизацию");
		JSONObject	requestReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
		this.created = System.currentTimeMillis();
		this.setGroupId(requestReply.get("id").toString());
		TearDownExecutor.addGroupToTeardown(this.getGroupId());
		JSONObject jsonMongoReply = (JSONObject) GetGroupInfoFromMongo.getGroupsInfoById(this.getGroupId()).get(0);
		checkGroupCommon(jsonMongoReply,requestReply, initMap);
/*		how to count it? try to parseInt?
		assertNotNull("API:group.vehiclesCount",
				ReplyToJSON.extractPathFromJson(requestReply,"$.vehiclesCount"), "group.vehiclesCount не верен");
		assertNotNull("API:group.employeesCount",
				ReplyToJSON.extractPathFromJson(requestReply,"$.employeesCount"), "group.employeesCount не верен");
*/
		return requestReply;
		}catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке создать группу:" + e.toString());
			return null;
		}
	}
	
	@Step("запрос изменения группы")
	//public void updateGroup(String groupId, String type, List<JSONObject> pointsArr ){//too similar to group add, should use common method?
	public void updateGroup() {
		url = getProperty("host") + "/OmGroups/Update";
		putUrlParameters("id", this.getGroupId());
		urlParameters.replace("name", this.getName());
		try{
		token = getCurAuthAccToken(CONST_AUTH_T1CLIENT);
		assertTrue(CONST_TOKEN_RECEIVED,token != null,"для изменения Группы необходимо пройти авторизацию");
		JSONObject	requestReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
		this.updated = System.currentTimeMillis();
		assertEquals("name", requestReply.get("name"), this.getName(), "Имя группы не изменилось!");
		JSONObject jsonMongoReply = (JSONObject) GetGroupInfoFromMongo.getGroupsInfoById(this.getGroupId()).get(0);
		checkGroupCommon(jsonMongoReply,requestReply, initMap);
		SharedContainer.removeFromContainer(CONST_GROUPS2, this.getGroupId());
		SharedContainer.setContainer(CONST_GROUPS2, this);
/*		how to count it? try to parseInt?
		assertNotNull("API:group.vehiclesCount",
				ReplyToJSON.extractPathFromJson(requestReply,"$.vehiclesCount"), "group.vehiclesCount не верен");
		assertNotNull("API:group.employeesCount",
				ReplyToJSON.extractPathFromJson(requestReply,"$.employeesCount"), "group.employeesCount не верен");
*/
		}catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "Ошибка при попытке редактирования группы:" + e.toString());
		}
	}
	
	@Step("запрос изменения группы в панели Администратора")
	//public void updateGroup(String groupId, String type, List<JSONObject> pointsArr ){//too similar to group add, should use common method?
	public JSONObject updateGroupInAdminPanel() {
		url = getProperty(CONST_ADM_PANEL_HOST) + "/Api/Admin/Groups/"+this.getGroupId();
		putUrlParameters("id", this.getGroupId());
		urlParameters.replace("name", this.getName());
		try{
			token = getCurAuthAccToken(CONST_AUTH_ADMPANEL);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для изменения Группы необходимо пройти авторизацию");
			JSONObject	requestReply = SendRequestToAPI.sendPutRequest( url, jsonParamsToString(urlParameters), token,false,true);
			this.updated = System.currentTimeMillis();
			assertEquals("name", requestReply.get("name"), this.getName(), "Имя группы не изменилось!");
			JSONObject jsonMongoReply = (JSONObject) GetGroupInfoFromMongo.getGroupsInfoById(this.getGroupId()).get(0);
			checkGroupCommon(jsonMongoReply,requestReply, initMap);
			SharedContainer.removeFromContainer(CONST_GROUPS2, this.getGroupId());
			SharedContainer.setContainer(CONST_GROUPS2, this);
			return requestReply;
		}catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "Ошибка при попытке редактирования группы:" + e.toString());
			return null;
		}
		
	}
	
	/**
	 * 
	 * @param groupId
	 * @return
	 */
	
	@Beta
	@Step("запрос информации о группе {0}")
	public JSONObject requestGroupInfoById(String groupId){
		log.error(WARN_NOT_READY);
		assertTrue(false,WARN_NOT_READY);
		apiMethod = "/OmGroups/"+groupId;
		url = getProperty("host") + apiMethod;
		try {
			token = getCurAuthAccToken(CONST_AUTH_T1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для получения информации о Группе необходимо пройти авторизацию");
			JSONObject requestReply= (JSONObject) SendRequestToAPI.sendGetRequest( url, "GET","", "",token).get(0);
			JSONObject jsonMongoReply = (JSONObject) GetGroupInfoFromMongo.getGroupsInfoById(groupId).get(0);
			checkGroupCommon(jsonMongoReply,requestReply, initMap);
			

			return requestReply;
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());
			return null;
		}
		
	}

	@Step("удаление группы {0}")
	public void deleteGroup(){
		urlParameters.clear();
		apiMethod = "/OmGroups/Delete/"+ this.getGroupId();
		url = getProperty("host") + apiMethod;
		try {
			token = getCurAuthAccToken(CONST_AUTH_T1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"Для удаления группы необходимо пройти авторизацию!");
		JSONObject	requestReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
		checkGroupDeleted(requestReply);
		SharedContainer.removeFromContainer(CONST_GROUPS2, this.getGroupId());
		TearDownExecutor.removeEntityFromTeardown(this.getGroupId(),CONST_GROUPS2);
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, "Ошибка при попытке удаления группы: " + e.toString());
		}
	}
	
	@Step("удаление группы {0}")
	public void deleteGroupInAdminPanel(){
		urlParameters.clear();
		apiMethod = "/Api/Admin/Groups/"+ this.getGroupId();
		url = getProperty(CONST_ADM_PANEL_HOST) + apiMethod;
		try {
			token = getCurAuthAccToken(CONST_AUTH_ADMPANEL);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"Для удаления группы необходимо пройти авторизацию!");
		JSONObject	requestReply = SendRequestToAPI.sendDeleteRequest( url, token,false);
		checkGroupDeleted(requestReply);
		SharedContainer.removeFromContainer(CONST_GROUPS2, this.getGroupId());
		TearDownExecutor.removeEntityFromTeardown(this.getGroupId(),CONST_GROUPS2);
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, "Ошибка при попытке удаления группы: " + e.toString());
		}
	}
	
	
	
	public void checkGroupCommon(JSONObject jsonMongoReply, JSONObject jsonApiRegistrationReply, HashMap initMap){
		try {

			assertTrue("API response содержит поле data",jsonApiRegistrationReply.keySet().size() > 2, "API response должен содержать поле data!");

			assertContainsMngId("DB:group_Id",ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_ID), "group.id не верен");
			assertContainsMngId("API:group_Id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.id"), "group.id не верен");

			String dateMongo = ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Created.$date");
			String dateApi = jsonApiRegistrationReply.get("created").toString();
			assertTimeEqualsApiVsMongo("Created", dateMongo, dateApi);
			compareDateTimeUpTo1min("group.Created сравнение DB с API и local date с точностью до 1 мин", dateMongo, dateApi, this.created);

			if (this.updated == null){
				assertTrue(!jsonApiRegistrationReply.containsKey(CONST_UPDATED), "Поле Updated заполняется только после изменения группы!");
			} else {
				assertTrue(jsonApiRegistrationReply.containsKey(CONST_UPDATED), "Поле Updated не было заполнено после изменения группы!");
				dateMongo = ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Updated.$date");
				dateApi = jsonApiRegistrationReply.get(CONST_UPDATED).toString();
				assertTimeEqualsApiVsMongo("Updated", dateMongo, dateApi);
				compareDateTimeUpTo1min("group.Updated  сравнение DB с API и local date с точностью до 1 мин", dateMongo,	dateApi, this.updated);
			}

		    assertEquals("DB:group_name",jsonMongoReply.get("Name"), this.getName(), CONST_NAME_WRONG);
		    assertEquals("API:group_name",jsonApiRegistrationReply.get("name"), this.getName(),CONST_NAME_WRONG);

		    assertEquals("DB:group_color", jsonMongoReply.get("Color"), this.getColor(), CONST_COLOR_WRONG);
		    assertEquals("API:group_color", jsonApiRegistrationReply.get(CONST_COLOR), this.getColor(),CONST_COLOR_WRONG);
//check empl\veh updated

		    for(HashMap resource : getResources()){
		    	String id = resource.get("id").toString();
		    	String collection="";
		    	if(resource.get(CONST_RESOURCE_TYPE).equals(CONST_EMPLOYEE)){
		    		collection="Employees";
		    	} else{
		    		collection="Vehicles";
		    	}
		    	JSONObject obj = GetInfoFromMongo.getInfoById(collection, id,  "").get(0);
		    	assertEquals("DB:"+collection+"group_Id",ReplyToJSON.extractPathFromJson(obj,"$.GroupId.$oid"), ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_ID),
		    			"объект "+id+" типа "+collection+" должен содержать корректный id группы");
		    	//TODO: uncomment once CON-290 is fixed!
//		    	long created = Long.parseLong(ReplyToJSON.extractPathFromJson(obj,"$.Created.$date"));
//		    	long updated = Long.parseLong(ReplyToJSON.extractPathFromJson(obj,"$.Updated.$date"));
//		    	assertTrue("DB:"+collection+"_updated > "+collection+"_created", updated >  created, collection+".updated не верен");
		    }
		    setToAllureChechkedFields();
	
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e.toString());
			assertTrue(false, e.toString());
		}

	}
	
	/**
	 * 
	 * @return
	 */
	@Beta
	@Step("Получение списка групп клиента")
	public JSONArray requestAllGroupsByCustomer (){
		log.error(WARN_NOT_READY);
		assertTrue(false,WARN_NOT_READY);
		Bson filter ;
		String customer ="";
		String accountId = ((JSONObject) SharedContainer.getFromContainer(CONST_AUTH_T1CLIENT).get(0)).get("id").toString();
		customer = getAuthorizedUserCustomer(accountId);
		apiMethod = "/OmGroups/Customer?customerId="+customer;

		url = getProperty("host") + apiMethod;
		try {
			token = getCurAuthAccToken(CONST_AUTH_T1CLIENT);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для получения информации о Группах необходимо пройти авторизацию");
			JSONArray receivedReply = SendRequestToAPI.sendGetRequest( url, "GET","", "",token);
			ObjectId objectId = new ObjectId(customer);

				filter = Filters.and ( eq(CONST_CUSTOMERID,objectId )) ;

			long mongoFilteredGroups = GetInfoFromMongo.countFilteredByCustomFilters(CONST_GROUPS, filter);
			 List<JSONObject> groupsFromMongo=	GetInfoFromMongo.getInfoByCustomFilters(CONST_GROUPS, filter, new ArrayList<String>());
			 checkGroupCompareAPIvsDB( mongoFilteredGroups, receivedReply, groupsFromMongo);
			return receivedReply;
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return null;
	}

	/**
	 * 
	 * @param accessType
	 * @param groupName
	 * @return
	 */
	@Beta
	@SuppressWarnings("unchecked")
	@Step("Получение списка групп")
	public JSONArray requestAllGroups(String accessType, String groupName) {
		Bson filter ;
		log.error(WARN_NOT_READY);
		assertTrue(false,WARN_NOT_READY);
		try {
			apiMethod = "/OmGroups/GetAll?accessType="+accessType;
			if(groupName!=null && ! groupName.equals("")){
				apiMethod+="&name="+URLEncoder.encode(groupName, "UTF-8");
			}
			url = getProperty("host") + apiMethod;
		
			token = getToken("userToken");
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для получения информации о Группах необходимо пройти авторизацию");
			JSONArray receivedReply = SendRequestToAPI.sendGetRequest( url, "GET","", "",token);
			ObjectId objectId = new ObjectId(getProperty("testCustomerId"));
			
			if ((groupName==null || groupName.equals("")) && (accessType==null || accessType.equals("")) ){//"",""
				filter = Filters.and ( eq(CONST_CUSTOMERID,objectId )) ;
			}
			else if ((groupName!=null && !groupName.equals("")) && (accessType==null || accessType.equals("")) ){//"val",""
				 filter = Filters.and ( eq(CONST_CUSTOMERID,objectId ) , eq("Name",groupName )   ) ;
			}
			else if ((groupName==null || groupName.equals("")) && (accessType!=null && !accessType.equals("")) ){//"","val"
				 filter = Filters.and (eq ("Access",accessType), eq(CONST_CUSTOMERID,objectId )   ) ;
			}else {																									//"val","val"	
				filter = Filters.and (eq ("Access",accessType), eq(CONST_CUSTOMERID,objectId ), eq("Name",groupName ) ) ;
				
			}

			long mongoFilteredGroups = GetInfoFromMongo.countFilteredByCustomFilters(CONST_GROUPS, filter);
			 List<JSONObject> groupsFromMongo=	GetInfoFromMongo.getInfoByCustomFilters(CONST_GROUPS, filter, new ArrayList<String>());
			 checkGroupCompareAPIvsDB( mongoFilteredGroups, receivedReply, groupsFromMongo);
			return receivedReply;
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return null;
	}
	
	/**
	 * 
	 * @param mongoFilteredGroups
	 * @param receivedReply
	 * @param groupsFromMongo
	 */
	@Beta
	@Step("Валидация: сравнение полученных групп и ожидаемых согласно БД")
	public void checkGroupCompareAPIvsDB(long mongoFilteredGroups,JSONArray receivedReply,List<JSONObject> groupsFromMongo){
		try {
			log.error(WARN_NOT_READY);
			assertTrue(false,WARN_NOT_READY);
			checkGroupCommonCompareAPIvsDB(mongoFilteredGroups, receivedReply, groupsFromMongo, false);

			for(int i=0; i<receivedReply.size();i++){	
				JSONObject apiGroup = (JSONObject) receivedReply.get(i);
				
				assertNotNull(CONST_API_GROUP_STR+i+CONST_VEHICLES_COUNT_STR, ReplyToJSON.extractPathFromJson(apiGroup,"$.vehiclesCount"), "group.vehiclesCount не верен");
				if ( (Integer.parseInt( apiGroup.get("vehiclesCount").toString())) >0    ){
					JSONArray apiGroupVehicleObjects=	(JSONArray) apiGroup.get("vehicleObjects");
					assertEquals(CONST_API_GROUP_STR+i+CONST_VEHICLES_COUNT_STR,
							Integer.parseInt( apiGroup.get("vehiclesCount").toString()), apiGroupVehicleObjects.size(), "group.vehiclesCount не верен");
				
				}
				assertNotNull(CONST_API_GROUP_STR+i+"].employeesCount",
						ReplyToJSON.extractPathFromJson(apiGroup,"$.employeesCount"), "group.employeesCount не верен");
				
				if ( (Integer.parseInt( apiGroup.get("employeesCount").toString()))  >0    ){
					JSONArray apiGroupEmployeeObjects=	(JSONArray) apiGroup.get("employeeObjects");
					assertEquals(CONST_API_GROUP_STR+i+CONST_VEHICLES_COUNT_STR,
							Integer.parseInt( apiGroup.get("employeesCount").toString()), apiGroupEmployeeObjects.size(), "group.employeesCount не верен");
					   
				}	   
				
		}
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
	}
	
	/**
	 * 
	 * @param mongoFilteredGroups
	 * @param receivedReply
	 * @param groupsFromMongo
	 * @param checkCoordinates
	 */
	@Beta
	public void checkGroupCommonCompareAPIvsDB(long mongoFilteredGroups,JSONArray receivedReply,List<JSONObject> groupsFromMongo, boolean checkCoordinates){
		try {
			log.error(WARN_NOT_READY);
			assertTrue(false,WARN_NOT_READY);
		assertEquals("API:всего групп ожидалось",receivedReply.size(),mongoFilteredGroups,"API вернул некорректное количество групп");
		for(int i=0; i<receivedReply.size();i++){
			JSONObject apiGroup = (JSONObject) receivedReply.get(i);
			JSONObject groupFromMongo= findDocumentInList(groupsFromMongo ,  apiGroup.get("id").toString());
			
//			checkGroupCommon (groupFromMongo,apiGroup, new HashMap());
			assertEquals(CONST_API_GROUP_STR+i+"]_name",groupFromMongo.get("Name"), apiGroup.get("name"), CONST_NAME_WRONG);
			assertEquals(CONST_API_GROUP_STR+i+"]_color", groupFromMongo.get("Color"), apiGroup.get(CONST_COLOR), CONST_COLOR_WRONG);
//			assertEquals(CONST_API_GROUP_STR+i+"]_type",groupFromMongo.get("Type"), apiGroup.get("type"), "type не верен");
			
				
		}
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		}

	@Step("Валидация: группа удалена")
	public void checkGroupDeleted(JSONObject jsonApi){
		try {
			assertTrue("API response не содержит поле data",jsonApi.keySet().size() == 2, "API response на удаление группы не должен содержать поле data!");
			List deletedGroup =  GetGroupInfoFromMongo.getGroupsInfoById(this.getGroupId());
			assertTrue("Группа с id " + this.getGroupId() + " была удалена из коллекции \"OmGroups\"" , deletedGroup.isEmpty(),
					"Была найдена информация о группе в коллекции \"OmGroups\":" + deletedGroup.toString());
			this.getResources().forEach(item ->{
				if (item.get(CONST_RESOURCE_TYPE).equals(CONST_EMPLOYEE)) {
					JSONObject empFromDB = (JSONObject) GetEmployeeInfoFromMongo.getEmployeeInfoById(item.get("id")).get(0);
					assertTrue("Поле GroupId удалено для Сотрудника с id = " + item.get("id"), !empFromDB.containsKey("GroupId"),
							"Поле GroupId не удалилось для Сотрудника с id = " + item.get("id"));
				} else if (item.get(CONST_RESOURCE_TYPE).equals(CONST_VEHICLE)) {
					JSONObject vecFromDB = (JSONObject) GetVehicleInfoFromMongo.getVehicleInfoById(item.get("id")).get(0);
					assertTrue("Поле GroupId удалено для ТС с id = " + item.get("id"), !vecFromDB.containsKey("GroupId"),
							"Поле GroupId не удалилось для ТС с id = " + item.get("id"));
					//assertTrue(!vecFromDB.containsKey("GroupId"));
				}

			} );
			setToAllureChechkedFields();
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e.toString());
			assertTrue(false, e.toString());
		}
	}

	@Step("Валидация: groupId удален из объектов группы")
	public void checkGroupIdDeletedFromObjects() {
		//необходимо написать проверку, что ид группы удален из ресурсов
	}
	

	//do we need this method here? should it be moved to abstract class?
	public JSONObject findDocumentInList(List<JSONObject> documents, String id){
		JSONObject foundDoc = new JSONObject();
		try {
		for (JSONObject doc: documents){
				if(ReplyToJSON.extractPathFromJson(doc, PATH_ID).equals(id)){
					foundDoc = doc;
				}
		}
		return foundDoc;
		} catch (Exception e) {
			log.error(ERROR,e);
			return null;
		}
	}
	
	@Override
	public int removeFromSharedContainer() {
		int indexOfRemovedEntity=-1;
		if (!container.isEmpty() && container.containsKey(CONST_GROUPS2) && ! container.get(CONST_GROUPS2).isEmpty()) {
			ArrayList listOfIds = container.get(CONST_GROUPS2);
				// удаление из контейнера
				if (listOfIds.get(0) instanceof com.t1.core.api.Group) {
					for (int i = 0; i < listOfIds.size(); i++) {
						if (((Group) listOfIds.get(i)).getGroupId().equals(this.getGroupId())) {
							listOfIds.remove(i);
							indexOfRemovedEntity=i;
						}
					}
				}
		}
		return indexOfRemovedEntity;
	}
	
	@Override
	public void addToSharedContainer() {
		addToSharedContainer(-1);
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addToSharedContainer(int index) {
		if (SharedContainer.getContainers().containsKey(CONST_GROUPS2) && ! SharedContainer.getContainers().get(CONST_GROUPS2).isEmpty()) {
			ArrayList<Group> existingArray = (ArrayList<Group>) SharedContainer.getContainers().get(CONST_GROUPS2);
			if(index>=0) {
				existingArray.add(index,this);
			} else {
				existingArray.add(this);
			}
			container.put(CONST_GROUPS2, existingArray);
		} else {
			ArrayList newArray = new ArrayList();
			newArray.add(this);
			container.put(CONST_GROUPS2, newArray);
		}
	}

	@Override
	public void updateInSharedContainer() {
		int index = this.removeFromSharedContainer();
		this.addToSharedContainer(index);
	}
	
	public static Group getFromSharedContainer(String groupId) {
		List<Group> listOfGroups =  (List<Group>) SharedContainer.getObjectsFromSharedContainer("all", CONST_GROUPS2);
		Group foundGroup = null;
		for(Group group : listOfGroups){
			if (group.getGroupId().equals(groupId)){
			foundGroup = group;
			break;
			}
		}
		return foundGroup;
	}
	
	@SuppressWarnings("unchecked")
	public void updateGroupInfoInResourcesInSharedContainer(Group group,JSONObject jsonReply){
		HashMap  fields= new HashMap();
		fields.put(CONST_GROUPID, group.getGroupId());
		fields.put(CONST_GROUP, jsonReply);
		fields.put("groupName", group.getName());
		for(HashMap resouce : group.getResources()){
			String resourceId = resouce.get("id").toString();
			if(resouce.get(CONST_RESOURCE_TYPE).equals(CONST_EMPLOYEE)){
			List<Employee> listOfEmpl =	SharedContainer.getObjectsFromSharedContainer("all",CONST_EMPLOYEES);
			for (Employee empl : listOfEmpl){
				if(empl.getEmployeeId().equals(resourceId)){
					empl.putUrlParameters(CONST_GROUPID, group.getGroupId());
					fields.put("urlParameters", empl.getUrlParameters());
				}
			}
				SharedContainer.updateContainer(CONST_EMPLOYEES, resourceId, fields);
				
			}
			if(resouce.get(CONST_RESOURCE_TYPE).equals(CONST_VEHICLE)){
				List<Vehicle> listOfVeh =	SharedContainer.getObjectsFromSharedContainer("all",CONST_VEHICLES);
				for (Vehicle veh : listOfVeh){
					if(veh.getVehicleId().equals(resourceId)){
						veh.putUrlParameters(CONST_GROUPID, group.getGroupId());
						fields.put("urlParameters", veh.getUrlParameters());
					}
				}
				SharedContainer.updateContainer(CONST_VEHICLES, resourceId, fields);
			}
		}
	}
	
	public void updateObjectsGroupInfoInContainerFromResources() {
		HashMap  fields= new HashMap();
		fields.put(CONST_GROUPID, this.getGroupId());
		for(HashMap resouce : this.getResources()){
			if(resouce.get(CONST_RESOURCE_TYPE).equals(CONST_EMPLOYEE)){
				SharedContainer.updateContainer(CONST_EMPLOYEES, resouce.get("id").toString(), fields);
			}
			if(resouce.get(CONST_RESOURCE_TYPE).equals(CONST_VEHICLE)){
				SharedContainer.updateContainer(CONST_VEHICLES, resouce.get("id").toString(), fields);
			}
		}
	}
	
	//should we move it to sharedContainer class or just correct customer creation step??
	public static Customer getCustomerFromContainer(){
		List customerFromPrevStepLst = (List) SharedContainer.getObjectsFromSharedContainer("any","customers");
		Customer customerFromPrevStep;
		if(customerFromPrevStepLst == null || customerFromPrevStepLst.isEmpty()){
			customerFromPrevStep = new Customer();
			JSONObject jsonCust=	 (JSONObject) SharedContainer.getLastObjectFromSharedContainer("customer");
			try {//this is wrong way! but at least we get id and name
				customerFromPrevStep.setCustomerId(ReplyToJSON.extractPathFromJson(jsonCust, PATH_ID));
			} catch (Exception e) {
				log.error(ERROR,e);
			}
			customerFromPrevStep.setName(jsonCust.get("Name").toString());
		} else {
			customerFromPrevStep = (Customer) customerFromPrevStepLst.get(0);
		}
		return customerFromPrevStep;
	}


}
