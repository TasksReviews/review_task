package com.t1.core.api;

import static com.mongodb.client.model.Filters.eq;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.FindGeozones;
import com.t1.core.mongodb.GetInfoFromMongo;
import com.t1.core.mongodb.GetTelematicsGeozonesInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Step;

public class Geozone extends AbstractClass  implements SharedContainerInterface{
	private String geozoneId;
	private String name;
	private String type;
	private String color;
	private String radius;
	private String access;
	private String square;
	private String bufferThickness;
	private String description;
	protected List<JSONObject> geozonePointsArr;
	
	protected JSONObject initJson;
	protected static HashMap initMap ;
	private JSONObject urlParameters = new JSONObject();

	private String token = null;

	public String getAccess() {		return access;	}
	public void setAccess(String access) {		this.access = access;	}
	public String getGeozoneId() {		return geozoneId;	}
	public void setGeozoneId(String geozoneId) {		this.geozoneId = geozoneId;	}
	public String getBufferThickness() {		return bufferThickness;	}
	public void setBufferThickness(String bufferThickness) {		this.bufferThickness = bufferThickness;	}
	public String getType() {		return type;	}
	public void setType(String type) {		this.type = type;	}
	public String getName() {		return name;	}
	public void setName(String name) {		this.name = name;	}
	public JSONObject getUrlParameters() {		return urlParameters;	}
	public void setUrlParameters(JSONObject urlParameters) {		this.urlParameters = urlParameters;	}
	public void putUrlParameters(Object key, Object value) {		this.urlParameters.put(key, value);	}
	
	private  static final String CONST_GEOZONE = "geozone";
	private  static final String CONST_GEOZONES = "geozones";
	private  static final String CONST_GEOZONES2 = "Geozones";
	private  static final String CONST_BUFFERTHINKNESS = "bufferThickness";
	private  static final String CONST_RADIUS = "Radius";
	private  static final String CONST_RADIUS2 = "radius";
	private  static final String CONST_COLOR = "color";
	private  static final String CONST_ROUND = "Round";
	private  static final String CONST_ACCESS = "Access";
	private  static final String CONST_ACCESS2 = "access";
	private  static final String CONST_SQUARE = "square";
	private  static final String CONST_POLYGON = "Polygon";
	private  static final String CONST_DEF_POLYGON_POINTS = "defaultPolygonPoints";
	private  static final String CONST_LINEAR = "Linear";
	private  static final String CONST_DEF_LINEAR_POINTS = "defaultLinearPoints";
	private  static final String CONST_DEF_ROUND_POINTS = "defaultRoundPoints";
	private  static final String CONST_POINTS = "points";
	private  static final String CONST_DESCRIPTION = "Description";
	private  static final String CONST_DESCRIPTION2 = "description";
	private  static final String CONST_USERTOKEN = "userToken";
	private  static final String CONST_TEST_FAILED = "Test failed: ";
	private  static final String CONST_GEOZONE_VEHICLES_COUNT_WRONG = "geozone.vehiclesCount не верен";
	private  static final String CONST_GEOZONE_EMPLOYEES_COUNT_WRONG = "geozone.employeesCount не верен";
	private  static final String CONST_NAME_WRONG = "name не верен";
	private  static final String CONST_COLOR_WRONG = "color не верен" ;
	private  static final String CONST_BUFFERTHINKNESS_WRONG = "bufferThickness не верен";
	private  static final String CONST_RADIUS_WRONG = "radius не верен";
	private  static final String CONST_ACCESS_WRONG = "access не верен";
	private  static final String CONST_SQUARE_WRONG = "square не верен";
	private  static final String CONST_LONGTITUDE_WRONG = "longitude не верен";
	private  static final String CONST_LATITUDE_WRONG = "latitude не верен";
	private  static final String CONST_DESCRIPTION_WRONG = "description не верен";
	private  static final String PATH_EMPLOYEES_COUNT = "$.employeesCount";
	private  static final String PATH_VEHICLES_COUNT = "$.vehiclesCount";
	private  static final String CONST_TOKEN_RECEIVED = "token received";
	private  static final String CONST_CUSTOMERID = "CustomerId";
	private  static final String CONST_AUTH_NEEDED = "для получения информации о Геозонах необходимо пройти авторизацию" ;
	private  static final String CONST_LONGTITUDE_STR = "].longitude";
	private  static final String CONST_LATITUDE_STR = "].latitude";
	private  static final String CONST_POINTS_STR = "$.Points[";
	private  static final String CONST_POINTS_STR2 = "$.points[";
	private  static final String CONST_API_GEOZONE_STR = "API:geozone[" ;
	private  static final String CONST_VEHICLES_COUNT_STR = "].vehiclesCount";
	
	public Geozone (){
		initJson = InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_GEOZONE,getProperty("stdPattern"));
		setMinimalFields(initJson);
		}
	
	public Geozone(String type, Boolean allFields, String pattern) {
		this.setType(type);
		initJson = InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_GEOZONE,getProperty(pattern));
		initJson.put("type", type);
		setMinimalFields(initJson);

		if (allFields) {
			setAllGeozoneFields(initJson);
		}

	}
	
	public Geozone(Boolean allFields, String pattern, String type, String radius, String bufferThickness) {
		initJson = InitDefValues.initializeInputWithDefaultOrRandomValues(CONST_GEOZONE,getProperty(pattern));
		initJson.put("type", type); 
		initJson.put(CONST_BUFFERTHINKNESS, bufferThickness); 
		initJson.put(CONST_RADIUS2, radius); 

		setMinimalFields(initJson);

		if (allFields) {
			setAllGeozoneFields(initJson);
		}

	}
	
	@SuppressWarnings("unchecked")	
	public void setMinimalFields(JSONObject initMap){

		this.setName(initMap.get("name").toString());
		this.color=initMap.get(CONST_COLOR).toString();
		this.setBufferThickness(initMap.get(CONST_BUFFERTHINKNESS).toString());
		this.setType(initMap.get("type").toString());
		if(initMap.get("type").equals(CONST_ROUND))
			this.radius = initMap.get(CONST_RADIUS2).toString();

		this.urlParameters.clear();
		this.urlParameters.put("name", this.getName());
		this.urlParameters.put("type", this.getType());
		this.urlParameters.put(CONST_COLOR,  this.color);
		this.urlParameters.put(CONST_BUFFERTHINKNESS,  this.getBufferThickness());
		if(initMap.get("type").equals(CONST_ROUND))
			this.urlParameters.put(CONST_RADIUS2, this.radius);
	}
	
	@SuppressWarnings("unchecked")
	public void setAllGeozoneFields(JSONObject initMap){

		this.setAccess(initMap.get(CONST_ACCESS2).toString());
		this.square=initMap.get(CONST_SQUARE).toString();
		this.description=initMap.get(CONST_DESCRIPTION2).toString();
		
		this.urlParameters.put(CONST_ACCESS2, this.getAccess());
		this.urlParameters.put(CONST_SQUARE, this.square);
		this.urlParameters.put(CONST_DESCRIPTION2, this.description);
		
	}
	public void addGeozone( ){
		List<JSONObject> pointsArr = null;
		switch(this.getType()){
		case CONST_POLYGON :  pointsArr = (List<JSONObject>) this.initJson.get(CONST_DEF_POLYGON_POINTS); break;
		case CONST_LINEAR :  pointsArr = (List<JSONObject>) this.initJson.get(CONST_DEF_LINEAR_POINTS); break;
		case CONST_ROUND :  pointsArr = (List<JSONObject>) this.initJson.get(CONST_DEF_ROUND_POINTS); break;
		default : 
			throw new IllegalArgumentException("Invalid\\not implemented geozone type " + this.getType()+" no initial default points applied");
		}
		
		this.addGeozone(this.getType(),pointsArr);
	}
	@Step("запрос создания новой геозоны типа {0}")
	public void addGeozone(String type, List<JSONObject> pointsArr) {
		try {
			if (type != null && type != "") {
				this.setType(type);
				urlParameters.put("type", type);
			}
			if (pointsArr == null || pointsArr.size() < 1) {
				if (type != null) {
					if (type.equals(CONST_POLYGON)) {
						this.geozonePointsArr = (List<JSONObject>) initJson.get(CONST_DEF_POLYGON_POINTS);
						urlParameters.put(CONST_POINTS, this.geozonePointsArr);
						if (urlParameters.containsKey(CONST_RADIUS2)) {
							urlParameters.remove(CONST_RADIUS2);
							this.radius = null;
						}
					} else if (type.equals(CONST_ROUND)) {
						urlParameters.put(CONST_RADIUS2, this.radius);
						// this.radius = initJson.get(CONST_RADIUS2).toString();
						this.geozonePointsArr = (List<JSONObject>) initJson.get(CONST_DEF_ROUND_POINTS);
						urlParameters.put(CONST_POINTS, this.geozonePointsArr);

					} else if (type.equals(CONST_LINEAR)) {
						this.geozonePointsArr = (List<JSONObject>) initJson.get(CONST_DEF_LINEAR_POINTS);
						urlParameters.put(CONST_POINTS, this.geozonePointsArr);
					}
				}
			} else {
				this.geozonePointsArr = pointsArr;
				urlParameters.put(CONST_POINTS, this.geozonePointsArr);
			}
			String url = getProperty("host") + "/Geozone/Add";
			token = getAnyToken();
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для создания Геозоны необходимо пройти авторизацию");
		JSONObject	requestReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
		this.setGeozoneId(requestReply.get("id").toString());
		
		TearDownExecutor.addGeozoneToTeardown(this.getGeozoneId());
		JSONObject jsonMongoReply = (JSONObject) GetTelematicsGeozonesInfoFromMongo.getTelematicsGeozonesInfoById(this.getGeozoneId()).get(0);
		
		checkGeozoneCommon(jsonMongoReply,requestReply);
		//how to count it? try to parseInt?
		assertNotNull("API:geozone.vehiclesCount",
				ReplyToJSON.extractPathFromJson(requestReply,PATH_VEHICLES_COUNT), CONST_GEOZONE_VEHICLES_COUNT_WRONG);
		assertNotNull("API:geozone.employeesCount",
				ReplyToJSON.extractPathFromJson(requestReply,PATH_EMPLOYEES_COUNT), CONST_GEOZONE_EMPLOYEES_COUNT_WRONG);
		}catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке создать геозону:" + e.toString());
		}
	}
	

	@Step("запрос изменения геозоны {0} типа {1}")
	public void updateGeozone(String geozoneId, String type, List<JSONObject> pointsArr ){//too similar to geozone add, should use common method?
		try {
			urlParameters.put("id", geozoneId);
		if (type != null && type != "") {
			this.setType(type);
			urlParameters.put("type", type);
		}
		if (pointsArr == null || pointsArr.size() < 1) {
			if (type != null) {
				if (type.equals(CONST_POLYGON)) {
					this.geozonePointsArr = (List<JSONObject>) initJson.get(CONST_DEF_POLYGON_POINTS);
					urlParameters.put(CONST_POINTS, this.geozonePointsArr);
				} else if (type.equals(CONST_ROUND)) {
					urlParameters.put(CONST_RADIUS2, initJson.get(CONST_RADIUS2));
					this.radius = initJson.get(CONST_RADIUS2).toString();
					this.geozonePointsArr = (List<JSONObject>) initJson.get(CONST_DEF_ROUND_POINTS);
					urlParameters.put(CONST_POINTS, this.geozonePointsArr);
					
				} else if (type.equals(CONST_LINEAR)) {
					this.geozonePointsArr = (List<JSONObject>) initJson.get(CONST_DEF_LINEAR_POINTS);
					urlParameters.put(CONST_POINTS, this.geozonePointsArr);
				}
			}	
		} else {
			this.geozonePointsArr =pointsArr;
			urlParameters.put(CONST_POINTS, this.geozonePointsArr);
		}
		String url = getProperty("host") + "/Geozone/Update";
		
			token = getToken(CONST_USERTOKEN);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для изменения Геозоны необходимо пройти авторизацию");
		JSONObject	requestReply = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
		this.setGeozoneId(requestReply.get("id").toString());
		
		JSONObject jsonMongoReply = (JSONObject) GetTelematicsGeozonesInfoFromMongo.getTelematicsGeozonesInfoById(this.getGeozoneId()).get(0);
		
		checkGeozoneCommon(jsonMongoReply,requestReply);
		//how to count it? try to parseInt?
		assertNotNull("API:geozone.vehiclesCount",
				ReplyToJSON.extractPathFromJson(requestReply,PATH_VEHICLES_COUNT), CONST_GEOZONE_VEHICLES_COUNT_WRONG);
		assertNotNull("API:geozone.employeesCount",
				ReplyToJSON.extractPathFromJson(requestReply,PATH_EMPLOYEES_COUNT), CONST_GEOZONE_EMPLOYEES_COUNT_WRONG);
		}catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке создать геозону:" + e.toString());
		}
	}
	
		
	@Step("запрос информации о геозоне {0}")
	public JSONObject requestGeozoneInfoById(String geozoneId){
		String url = getProperty("host") + "/Geozone/"+geozoneId;
		try {
			token = getToken(CONST_USERTOKEN);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для получения информации о Геозоне необходимо пройти авторизацию");
			JSONObject requestReply= (JSONObject) SendRequestToAPI.sendGetRequest( url, "GET","", "",token).get(0);
			JSONObject jsonMongoReply = (JSONObject) GetTelematicsGeozonesInfoFromMongo.getTelematicsGeozonesInfoById(geozoneId).get(0);
			checkGeozoneCommon(jsonMongoReply,requestReply);
			
			JSONObject mongoGeoinfo = (JSONObject) jsonMongoReply.get("GeoInfo");   
			
			JSONObject apiGeoinfo =(JSONObject) requestReply.get("geoInfo");  
			
			assertEquals("DB:geozone_geoInfo.type",mongoGeoinfo.get("type"), CONST_POLYGON/* this.type*/, "geoInfo.type не верен");
			assertEquals("API:geozone_geoInfo.type",apiGeoinfo.get("type"),  CONST_POLYGON/*this.type*/,"geoInfo.type не верен");
			if(apiGeoinfo.get("type").equals(CONST_POLYGON)){
				   JSONArray apiGeoinfoCoordinates = ReplyToJSON.extractListPathFromJson(apiGeoinfo, "$.coordinates.exterior.positions");
				   JSONArray mongoGeoinfoCoordinates=ReplyToJSON.extractListPathFromJson(mongoGeoinfo, "$.coordinates[0]");
				   
				   for(int i=0; i<mongoGeoinfoCoordinates.size();i++){
					  org.json.simple.JSONArray mongoArrCoord = ((org.json.simple.JSONArray) mongoGeoinfoCoordinates.get(i));
					  org.json.simple.JSONArray apiArrCoord = ((org.json.simple.JSONArray) ((JSONObject) apiGeoinfoCoordinates.get(i)).get("values") );

					  assertEquals("geoInfo.Longitude.coordinates["+i+"]=",roundDouble((Double)apiArrCoord.get(0),7), roundDouble((Double)mongoArrCoord.get(0),7), 
							  		"geoInfo.Longitude.coordinates mongo и api значения не совпадают");
					  assertEquals("geoInfo.Latitude.coordinates["+i+"]=",roundDouble((Double)apiArrCoord.get(1),7), roundDouble((Double)mongoArrCoord.get(1),7), 
							  		"geoInfo.Latitude.coordinates mongo и api значения не совпадают");

				   }
			}else {
				assertTrue(false, "asserts for geoInfo.coordinates for "+apiGeoinfo.get("type")+" not implemented!");
			}
			setToAllureChechkedFields();
				
			return requestReply;
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());
			return null;
		}
		
	}
	
	
	@Step("удаление геозоны {0}")
	public void deleteGeozone(String id ){
		urlParameters.clear();
		String url = getProperty("host") + "/Geozone/Delete/"+id;
		try {
			token = getToken(CONST_USERTOKEN); 
			assertTrue(CONST_TOKEN_RECEIVED,token != null,"для удаления Геозоны необходимо пройти авторизацию");
			SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
		
		checkGeozoneDeleted(id);
		SharedContainer.removeFromContainer(CONST_GEOZONES,id);
		TearDownExecutor.removeEntityFromTeardown(id,CONST_GEOZONES);
		}catch (Exception e) {
			log.error("Test failed: wasn't able to delete geozone, remove it from containers anyway!",e);
			SharedContainer.removeFromContainer(CONST_GEOZONES,id);
			TearDownExecutor.removeEntityFromTeardown(id,CONST_GEOZONES);
			assertTrue(false, e.toString());
		}
	}
	
	public void checkGeozoneCommon(JSONObject jsonMongoReply, JSONObject jsonApiRegistrationReply){
		try {
			
			assertContainsMngId("DB:geozone_Id",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$._id.$oid"), "geozone.id не верен");
			assertContainsMngId("API:geozone_Id",ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.id"), "geozone.id не верен");

		assertEquals("DB:geozone_name",jsonMongoReply.get("Name"), this.getName(), CONST_NAME_WRONG);
		assertEquals("API:geozone_name",jsonApiRegistrationReply.get("name"), this.getName(),CONST_NAME_WRONG);

		assertEquals("DB:geozone_color", jsonMongoReply.get("Color"), this.color, CONST_COLOR_WRONG);
		assertEquals("API:geozone_color", jsonApiRegistrationReply.get(CONST_COLOR), this.color,CONST_COLOR_WRONG);

		assertEquals("DB:geozone_type",jsonMongoReply.get("Type"), this.getType(), "type не верен");
		assertEquals("API:geozone_type",jsonApiRegistrationReply.get("type"), this.getType(),CONST_NAME_WRONG);

		assertEquals("DB:geozone_bufferThickness",jsonMongoReply.get("BufferThickness").toString(), this.getBufferThickness()+".0", CONST_BUFFERTHINKNESS_WRONG);
		assertEquals("API:geozone_bufferThickness",jsonApiRegistrationReply.get(CONST_BUFFERTHINKNESS).toString(), this.getBufferThickness(),CONST_BUFFERTHINKNESS_WRONG);

		if (this.radius != null) {
			assertEquals("DB:geozone_radius",
					roundDouble( Double.parseDouble(jsonMongoReply.get(CONST_RADIUS).toString()),9), 
					roundDouble( Double.parseDouble(this.radius),9), CONST_RADIUS_WRONG);
			assertEquals("API:geozone_radius",
					roundDouble( Double.parseDouble(jsonApiRegistrationReply.get(CONST_RADIUS2).toString()),9), 
					roundDouble( Double.parseDouble(this.radius),9),CONST_RADIUS_WRONG);
		}
		
		if (this.getAccess() != null) {
			assertEquals("DB:geozone_access",jsonMongoReply.get(CONST_ACCESS), this.getAccess(), CONST_ACCESS_WRONG);
			assertEquals("API:geozone_access",jsonApiRegistrationReply.get(CONST_ACCESS2), this.getAccess(),CONST_ACCESS_WRONG);
		}
		if (this.square != null) {
			assertEquals("DB:geozone_square",Double.parseDouble(ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Square")), Double.parseDouble(this.square), CONST_SQUARE_WRONG);
			assertEquals("API:geozone_square",Double.parseDouble(ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, "$.square")), Double.parseDouble(this.square),CONST_SQUARE_WRONG);
		}
		if (this.description != null) {
			assertEquals("DB:geozone_description",jsonMongoReply.get(CONST_DESCRIPTION), this.description, CONST_DESCRIPTION_WRONG);
			assertEquals("API:geozone_description",jsonApiRegistrationReply.get(CONST_DESCRIPTION2), this.description,CONST_DESCRIPTION_WRONG);
		}
//		points
		for (int i=0;i< this.geozonePointsArr.size();i++){
			JSONObject point = this.geozonePointsArr.get(i);
			assertEquals("DB:geozone_point["+i+CONST_LONGTITUDE_STR,ReplyToJSON.extractPathFromJson(jsonMongoReply, CONST_POINTS_STR+i+"].Longitude"), point.get("longitude"), CONST_LONGTITUDE_WRONG);
			assertEquals("API:geozone_point["+i+CONST_LONGTITUDE_STR,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, CONST_POINTS_STR2+i+CONST_LONGTITUDE_STR), point.get("longitude"),CONST_LONGTITUDE_WRONG);
			assertEquals("DB:geozone_point["+i+CONST_LATITUDE_STR,ReplyToJSON.extractPathFromJson(jsonMongoReply, CONST_POINTS_STR+i+"].Latitude"), point.get("latitude"), CONST_LATITUDE_WRONG);
			assertEquals("API:geozone_point["+i+CONST_LATITUDE_STR,ReplyToJSON.extractPathFromJson(jsonApiRegistrationReply, CONST_POINTS_STR2+i+CONST_LATITUDE_STR), point.get("latitude"),CONST_LATITUDE_WRONG);

		}

		setToAllureChechkedFields();
	
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e.toString());
			assertTrue(false, e.toString());
		}

	}
	
	@Step("запрос списка геозон включающих в себя точку с координатами lat{0}, lon{1}")
	public List<JSONObject> getGeozonesRelatesToPoint(String lat, String lon){
		String url = getProperty("host") + "/Geozone/FindByPoint?longitude="+lon+"&latitude="+lat;
		try {
			token = getAnyToken();
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			JSONArray receivedReply = SendRequestToAPI.sendGetRequest( url, "GET","", "",token);
			List<JSONObject> mongoFoundGeozones= FindGeozones.getGeozonesInfoBycoordinates(Double.parseDouble(lat), Double.parseDouble(lon), "");
			 checkGeozoneCommonCompareAPIvsDB( mongoFoundGeozones.size(), receivedReply, mongoFoundGeozones,false);
			 setToAllureChechkedFields();
			return receivedReply;
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return null;
	}
	
	@Step("запрос принадлежит точка с координатами lon {1}, lat {2} геозоне {0}")
	public void checkIfPoinRelatesToGeozone(String geozoneId, String lon,String lat){
		boolean poinIntersectGeozone = false ;
		String url = getProperty("host") + "/Geozone/IntersectPoint?id="+geozoneId+"&lat="+lat+"&lon="+lon;
		try {
			token = getAnyToken();
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			JSONObject receivedReply = (JSONObject) SendRequestToAPI.sendGetRequest( url, "GET","", "",token).get(0);
			List<JSONObject> mongoFoundGeozones= GetInfoFromMongo.selectGeozonesFromListPlacedOnPointCoordinates(
																	Arrays.asList(new ObjectId(geozoneId)), Double.parseDouble(lon),Double.parseDouble(lat), "Name");
			if((mongoFoundGeozones.size()==1)
				&& (ReplyToJSON.extractPathFromJson(mongoFoundGeozones.get(0),"_id.$oid").equals(geozoneId))){
					poinIntersectGeozone = true;
			} 
			assertEquals(CONST_API_GEOZONE_STR+geozoneId+"].IntersectPoint",
					String.valueOf(receivedReply.get("messageBody")),String.valueOf(poinIntersectGeozone), CONST_API_GEOZONE_STR+geozoneId+"].IntersectPoint не верен");
				
			setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
	}
	
	public JSONArray requestAllGeozonesByCustomer (){
		Bson filter ;
		String customer ="";
		String accountId = ((JSONObject) SharedContainer.getFromContainer("authorizedAccount_t1client").get(0)).get("id").toString();
		customer = getAuthorizedUserCustomer(accountId);
		String url = getProperty("host") + "/Geozone/Customer?customerId="+customer;
		try {
			token = getAnyToken();
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			JSONArray receivedReply = SendRequestToAPI.sendGetRequest( url, "GET","", "",token);
			ObjectId objectId = new ObjectId(customer);

				filter = Filters.and ( eq(CONST_CUSTOMERID,objectId )) ;

			long mongoFilteredGeozones = GetInfoFromMongo.countFilteredByCustomFilters(CONST_GEOZONES2, filter);
			 List<JSONObject> geozonesFromMongo=	GetInfoFromMongo.getInfoByCustomFilters(CONST_GEOZONES2, filter, new ArrayList<String>());
			 checkGeozoneCompareAPIvsDB( mongoFilteredGeozones, receivedReply, geozonesFromMongo);
			return receivedReply;
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public JSONArray requestAllGeozones(String accessType, String geozoneName) {
		Bson filter ;
		try {
			String apiMethod = "/Geozone/GetAll?accessType="+accessType;
			if(geozoneName!=null && ! geozoneName.equals("")){
				apiMethod+="&name="+URLEncoder.encode(geozoneName, "UTF-8");
			}
			String url = getProperty("host") + apiMethod;
		
			token = getToken(CONST_USERTOKEN);
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			JSONArray receivedReply = SendRequestToAPI.sendGetRequest( url, "GET","", "",token);
			ObjectId objectId = new ObjectId(getProperty("testCustomerId"));
			
			if ((geozoneName==null || geozoneName.equals("")) && (accessType==null || accessType.equals("")) ){//"",""
				filter = Filters.and ( eq(CONST_CUSTOMERID,objectId )) ;
			}
			else if ((geozoneName!=null && !geozoneName.equals("")) && (accessType==null || accessType.equals("")) ){//"val",""
				 filter = Filters.and ( eq(CONST_CUSTOMERID,objectId ) , eq("Name",geozoneName )   ) ;
			}
			else if ((geozoneName==null || geozoneName.equals("")) && (accessType!=null && !accessType.equals("")) ){//"","val"
				 filter = Filters.and (eq (CONST_ACCESS,accessType), eq(CONST_CUSTOMERID,objectId )   ) ;
			}else {																									//"val","val"	
				filter = Filters.and (eq (CONST_ACCESS,accessType), eq(CONST_CUSTOMERID,objectId ), eq("Name",geozoneName ) ) ;
				
			}

			long mongoFilteredGeozones = GetInfoFromMongo.countFilteredByCustomFilters(CONST_GEOZONES2, filter);
			 List<JSONObject> geozonesFromMongo=	GetInfoFromMongo.getInfoByCustomFilters(CONST_GEOZONES2, filter, new ArrayList<String>());
			 checkGeozoneCompareAPIvsDB( mongoFilteredGeozones, receivedReply, geozonesFromMongo);
			return receivedReply;
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return null;
	}
	
	@Step("Валидация: сравнение полученных геозон и ожидаемых согласно БД")
	public void checkGeozoneCompareAPIvsDB(long mongoFilteredGeozones,JSONArray receivedReply,List<JSONObject> geozonesFromMongo){
		try {

			checkGeozoneCommonCompareAPIvsDB(mongoFilteredGeozones, receivedReply, geozonesFromMongo, false);

			for(int i=0; i<receivedReply.size();i++){	
				JSONObject apiGeozone = (JSONObject) receivedReply.get(i);
				
				assertNotNull(CONST_API_GEOZONE_STR+i+CONST_VEHICLES_COUNT_STR, ReplyToJSON.extractPathFromJson(apiGeozone,PATH_VEHICLES_COUNT), CONST_GEOZONE_VEHICLES_COUNT_WRONG);
				if ( (Integer.parseInt( apiGeozone.get("vehiclesCount").toString())) >0    ){
					JSONArray apiGeozoneVehicleObjects=	(JSONArray) apiGeozone.get("vehicleObjects");
					assertEquals(CONST_API_GEOZONE_STR+i+CONST_VEHICLES_COUNT_STR,
							Integer.parseInt( apiGeozone.get("vehiclesCount").toString()), apiGeozoneVehicleObjects.size(), CONST_GEOZONE_VEHICLES_COUNT_WRONG);
				
				}
				assertNotNull(CONST_API_GEOZONE_STR+i+"].employeesCount",
						ReplyToJSON.extractPathFromJson(apiGeozone,PATH_EMPLOYEES_COUNT), CONST_GEOZONE_EMPLOYEES_COUNT_WRONG);
				
				if ( (Integer.parseInt( apiGeozone.get("employeesCount").toString()))  >0    ){
					JSONArray apiGeozoneEmployeeObjects=	(JSONArray) apiGeozone.get("employeeObjects");
					assertEquals(CONST_API_GEOZONE_STR+i+CONST_VEHICLES_COUNT_STR,
							Integer.parseInt( apiGeozone.get("employeesCount").toString()), apiGeozoneEmployeeObjects.size(), CONST_GEOZONE_EMPLOYEES_COUNT_WRONG);
					   
				}	   
				
		}
		setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
	}
	
	public void checkGeozoneCommonCompareAPIvsDB(long mongoFilteredGeozones,JSONArray receivedReply,List<JSONObject> geozonesFromMongo, boolean checkCoordinates){
		try {
		assertEquals("API:всего геозон ожидалось",receivedReply.size(),mongoFilteredGeozones,"API вернул некорректное количество геозон");
		for(int i=0; i<receivedReply.size();i++){
			JSONObject apiGeozone = (JSONObject) receivedReply.get(i);
			JSONObject geozoneFromMongo= findDocumentInList(geozonesFromMongo ,  apiGeozone.get("id").toString());
			
//			checkGeozoneCommon (geozoneFromMongo,apiGeozone, new HashMap());
			assertEquals(CONST_API_GEOZONE_STR+i+"]_name",geozoneFromMongo.get("Name"), apiGeozone.get("name"), CONST_NAME_WRONG);
			assertEquals(CONST_API_GEOZONE_STR+i+"]_color", geozoneFromMongo.get("Color"), apiGeozone.get(CONST_COLOR), CONST_COLOR_WRONG);
			assertEquals(CONST_API_GEOZONE_STR+i+"]_type",geozoneFromMongo.get("Type"), apiGeozone.get("type"), "type не верен");
			assertEquals(CONST_API_GEOZONE_STR+i+"]_bufferThickness",geozoneFromMongo.get("BufferThickness"), Double.parseDouble(apiGeozone.get(CONST_BUFFERTHINKNESS).toString()),
											CONST_BUFFERTHINKNESS_WRONG);

				if (geozoneFromMongo.containsKey(CONST_RADIUS) ) {
					assertEquals(CONST_API_GEOZONE_STR+i+"]_radius",
							roundDouble( Double.parseDouble(geozoneFromMongo.get(CONST_RADIUS).toString()),7), 
							roundDouble( Double.parseDouble(apiGeozone.get(CONST_RADIUS2).toString()),7), CONST_RADIUS_WRONG);
				}
				if (geozoneFromMongo.containsKey(CONST_ACCESS)) {
					assertEquals(CONST_API_GEOZONE_STR+i+"]_access",geozoneFromMongo.get(CONST_ACCESS), apiGeozone.get(CONST_ACCESS2), CONST_ACCESS_WRONG);
				}
				if (geozoneFromMongo.containsKey("Square")) {
					assertEquals(CONST_API_GEOZONE_STR+i+"]_square",
							roundDouble( Double.parseDouble(ReplyToJSON.extractPathFromJson(geozoneFromMongo, "$.Square")),6), 
							roundDouble( Double.parseDouble(apiGeozone.get(CONST_SQUARE).toString()),6), CONST_SQUARE_WRONG);
				}
				if (geozoneFromMongo.containsKey(CONST_DESCRIPTION)) {
					assertEquals(CONST_API_GEOZONE_STR+i+"]_description",geozoneFromMongo.get(CONST_DESCRIPTION), apiGeozone.get(CONST_DESCRIPTION2), CONST_DESCRIPTION_WRONG);
				}
				if (geozoneFromMongo.containsKey("Points")) {
					JSONArray mongoGeozonePoints = (JSONArray) geozoneFromMongo.get("Points");
					for (int p = 0; p < mongoGeozonePoints.size(); p++) {
						assertEquals("geozone["+i+"]_point[" + p + CONST_LONGTITUDE_STR,
								roundDouble(Double.parseDouble( ReplyToJSON.extractPathFromJson(geozoneFromMongo, CONST_POINTS_STR + p + "].Longitude")),10),
								roundDouble(Double.parseDouble( ReplyToJSON.extractPathFromJson(apiGeozone, CONST_POINTS_STR2 + p + CONST_LONGTITUDE_STR)),10),
								CONST_LONGTITUDE_WRONG);
						assertEquals("geozone["+i+"]_point[" + p + CONST_LATITUDE_STR,
								roundDouble(Double.parseDouble( 	ReplyToJSON.extractPathFromJson(geozoneFromMongo, CONST_POINTS_STR + p + "].Latitude")),10),
								roundDouble(Double.parseDouble( 	ReplyToJSON.extractPathFromJson(apiGeozone, CONST_POINTS_STR2 + p + CONST_LATITUDE_STR)),10),
								CONST_LATITUDE_WRONG);
					}
				}
				if(checkCoordinates){
					assertTrue(false, "asserts for coordinates are not implemented!");
				}
				
		}
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		}
	@Step("Валидация: геозона удалена")
	public void checkGeozoneDeleted(String geozoneId){
		try {
			List<org.json.simple.JSONObject> deletedGeozonejsonMongoReply =  GetTelematicsGeozonesInfoFromMongo.getTelematicsGeozonesInfoById(geozoneId);
			assertEquals("check Geozonedeleted",deletedGeozonejsonMongoReply.size(), 0, "была найдена информация о геозоне:"+deletedGeozonejsonMongoReply.toString());
			setToAllureChechkedFields();
		} catch (Exception e) {
			setToAllureChechkedFields();
			log.error(CONST_TEST_FAILED + e.toString());
			assertTrue(false, e.toString());
		}
	}
	

	
	public JSONObject findDocumentInList(List<JSONObject> documents, String id){
		JSONObject foundDoc = new JSONObject();
		try {
		for (JSONObject doc: documents){
				if(ReplyToJSON.extractPathFromJson(doc, "$._id.$oid").equals(id)){
					foundDoc = doc;
				}
		}
		return foundDoc;
		} catch (Exception e) {
			log.error(ERROR,e);
			return null;
		}
	}
	
	@Override
	public int removeFromSharedContainer() {
		int indexOfRemovedEntity=-1;
		if (!container.isEmpty() && container.containsKey(CONST_GEOZONES) && ! container.get(CONST_GEOZONES).isEmpty()) {
			ArrayList listOfIds = container.get(CONST_GEOZONES);
				// удаление из контейнера
				if (listOfIds.get(0) instanceof com.t1.core.api.Geozone) {
					for (int i = 0; i < listOfIds.size(); i++) {
						if (((Geozone) listOfIds.get(i)).getGeozoneId().equals(this.getGeozoneId())) {
							listOfIds.remove(i);
							indexOfRemovedEntity=i;
						}
					}
				}
		}
		return indexOfRemovedEntity;
	}
	
	@Override
	public void addToSharedContainer() {
		addToSharedContainer(-1);
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public void addToSharedContainer(int index) {
		if (SharedContainerInterface.getContainers().containsKey(CONST_GEOZONES) && ! SharedContainerInterface.getContainers().get(CONST_GEOZONES).isEmpty()) {
			ArrayList<Geozone> existingArray = (ArrayList<Geozone>) SharedContainerInterface.getContainers().get(CONST_GEOZONES);
			if(index>=0) {
				existingArray.add(index,this);
			} else {
				existingArray.add(this);
			}
			container.put(CONST_GEOZONES, existingArray);
		} else {
			ArrayList newArray = new ArrayList();
			newArray.add(this);
			container.put(CONST_GEOZONES, newArray);
		}
	}

	@Override
	public void updateInSharedContainer() {
		int index = this.removeFromSharedContainer();
		this.addToSharedContainer(index);
	}

}
