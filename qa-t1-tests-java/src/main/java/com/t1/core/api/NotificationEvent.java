package com.t1.core.api;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.mongodb.GetDeviceFullRecordsFromMongo;
import com.t1.core.mongodb.GetNotificationRules;

import ru.yandex.qatools.allure.annotations.Step;

public class NotificationEvent  extends AbstractClass{
	protected JSONObject urlParameters = new JSONObject();
	protected JSONObject initJson;
	private String token = null;
	protected ArrayList<Employee> employees;
	private String deviceId;
	protected JSONObject generatedEventReply = new JSONObject();
	protected ArrayList<JSONObject> generatedEventReplyArr = new ArrayList<>();
	protected JSONArray generatedEventArray = new JSONArray();
	private String timeFrom;
	private String timeTo;
	protected JSONObject mongoDeviceFullRecords;
	protected List<String> allNotifEventTypes = new ArrayList<>(Arrays.asList( "GeozoneEntrance", "GeozoneExit", "GeozoneStanding", "UserSpeedLimit", "StopEvent", "IdleEvent",
			 "TrafficRulesSpeedLimit", "SharpTurn", "SharpTurnLeft", "SharpTurnRight", "SharpSpeedup", "SharpBraking", "SharpTurnSpeedLimit", "Refueling", "Discharge", 
			 "FuelConsumption", "PanicButton", "TelematicDeviceMechanismOn", "TelematicDeviceMechanismOff", "MileageExceedance", "UserSpeedlimit"
			)); 

	protected List<String> eventFilter;

	private  static final String CONST_TIME_FROM = "timeFrom";
	private  static final String CONST_TIME_TO = "timeTo";
	private  static final String CONST_DEVICEID = "deviceId";
	private  static final String CONST_TOKEN_RECEIVED = "token received";
	private  static final String CONST_AUTH_NEEDED = "для получения оповещения необходимо пройти авторизацию";
	private  static final String CONST_TEST_FAILED = "Test failed: ";
	private  static final String PATH_DEVICETIME_DATE = "$.DeviceTime.$date";
	private  static final String CONST_NOT_IMPLEMENTED = "not implemented ";
	
	public NotificationEvent (){
		this.urlParameters.clear();
		}
	
	public NotificationEvent (String deviceid, String timeFrom, String timeTo){
		this.urlParameters.clear();
		
		this.timeFrom=timeFrom;
		this.timeTo=timeTo;
		this.deviceId=deviceid;

		
		this.urlParameters.put(CONST_TIME_FROM, timeFrom);
		this.urlParameters.put(CONST_TIME_TO, timeTo);
		this.urlParameters.put(CONST_DEVICEID, deviceId);
		
/*		this.urlParameters.put("geozoneIds", this.geoZoneIds);
		this.urlParameters.put("graphTimeUnit", graphTimeUnit);
		this.urlParameters.put("dataTimeUnit", dataTimeUnit);
		this.urlParameters.put("isDeepDetailed", isDeepDetailed);
		this.urlParameters.put("ResourceType", resourceType);
		final JSONObject contentConfig = new JSONObject();
		contentConfig.put("EventType", EventType);
		
		ArrayList reqCriteriaParamsArray = new ArrayList();
	//	if (EventType.equals("EventOnVisitsToGeozones")){
			contentConfig.put("EventCriteria", EventCriteria);
	//	}
		
		this.urlParameters.put("contentConfig", contentConfig);
		
		final JSONObject resourcesFilter = new JSONObject();
		resourcesFilter.put("groups", new ArrayList());
		resourcesFilter.put("include", this.resourcesFilterInclude);
		resourcesFilter.put("exclude", new ArrayList());
		this.urlParameters.put("resourcesFilter", resourcesFilter);
*/		
	
		
	}

 /*	public Event(Boolean allFields, ArrayList<Employee> employeesObjects) {
		ArrayList<String> employeesIds = new ArrayList<String>();
		this.apiMethod = "/Events/Generate";
		this.host = getProperty("host");
		this.url = host + apiMethod;
		initJson = initializeInputWithDefaultOrRandomValues("department",getProperty("stdPattern"));
		this.urlParameters.clear();
		this.employees = employeesObjects;
		assertTrue(this.employees.get(0).employeeId != null,	"для создания Департамента необходимо создать\\получить не менее 1 сотрудника");
		this.departmentName = initJson.get("department_name").toString();
		this.urlParameters.put("name", this.departmentName);
		for(Employee employee: employeesObjects){
			employeesIds.add(
					employee.employeeId);
		}
		this.urlParameters.put("employees", employeesIds);

	}*/
	
	@Step("запрос RoadEventsGenerator")
	public List<JSONObject> requestRoadEventsGenerator(){
		String url = getProperty("host") + "/Notifications/RoadEventsGenerator";
		try {
			token = getToken("demoToken");
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			//TODO: post should return array here.
			generatedEventReplyArr = SendRequestToAPI.sendPostRequestArrRe( url, jsonParamsToString(urlParameters), token,false,true);
			
//			db.getCollection('DeviceFullRecords').find({"DeviceId" : ObjectId("5940ff14bf5efd572883a27f"), 
//				"DeviceTime" : {
//				        $gte: ISODate("2017-06-21T08:45:00.000Z"),
//				        $lt:  ISODate("2017-06-21T08:45:10.000Z")
//				    }})
			
		//	mongoVehicleInfo = GetVehicleInfoFromMongo.getVehicleInfoById(reqVehicleId,"");
			List<JSONObject> mongoDeviceFullRecords = GetDeviceFullRecordsFromMongo.getByDeviceIdAndDates(deviceId,this.timeFrom,this.timeTo);
			checkBaseFieldsOfReceivedEvent(mongoDeviceFullRecords, generatedEventReplyArr, this.urlParameters);
			
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return generatedEventReplyArr;
		

	}
	
	
/*	@Step("запрос событий по {0}")
	public JSONObject requestRoadEents (){
		JSONObject	receivedEvents = new JSONObject();
		apiMethod = "/Resources/RoadEvents";
		url = getProperty("host") + apiMethod;
		urlParameters.clear();
		this.eventFilter=allNotifEventTypes;
		this.deviceId="5926f69623a3532bc4d780e0";//"56d84087efd38d23f4126e06";
		this.dateStart="2017-08-08 00:00:00";
		this.dateEnd="2017-08-08 22:59:59";
		this.skip=0;
		this.limit=100;
		
		this.urlParameters.put(CONST_DEVICEID, this.deviceId);
		this.urlParameters.put("dateStart", this.dateStart);
		this.urlParameters.put("dateEnd", this.dateEnd);
		this.urlParameters.put("eventFilter", this.eventFilter);
		this.urlParameters.put("skip", this.skip);
		this.urlParameters.put("limit", this.limit);
		
//		db.getCollection('DriveStylePenaltyScores').find({
//		    "CustomerId" : ObjectId("59369a505532f433948628a7")
//		    })

		
		
		try {
			token = getToken("demoToken");//demoToken"); 
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			receivedEvents = (JSONObject) SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			
		} catch (Exception e) {
			e.printStackTrace();	
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return receivedEvents;
		

	}
*/	
	@Step("запрос информации проанализированных интервалов")
	public JSONObject generateAnalyticIntervalResults(String deviceid,String startTime, String endTime){
		//apiMethod = "/Events/AnalyticIntervalResults?deviceid=592f428c36214a623c8689a5&startTime=2017-06-01&endTime=2017-06-09";
		startTime=startTime.replace(" ", "+");
		endTime=endTime.replace(" ", "+");
		//apiMethod ="/Events/AnalyticIntervalResults?deviceid=592f428c36214a623c8689a5&startTime=2017-06-08+02:00:00&endTime=2017-06-08+03:00:00";
		String apiMethod = "/Events/AnalyticIntervalResults?deviceid="+deviceid+"&startTime="+startTime+"&endTime="+endTime;
		String url = getProperty("host") + apiMethod;
		try {
			token = getToken("demoToken");
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			generatedEventArray = SendRequestToAPI.sendGetRequest( url, "GET","", "",token);
			
			
		//	mongoDeviceFullRecords = GetDeviceFullRecordsFromMongo.getByDeviceIdAndDates(deviceId,startTime,endTime);
			
//			db.getCollection('DeviceFullRecords').find({"DeviceId" : ObjectId("5940ff14bf5efd572883a27f"), 
//				"DeviceTime" : {
//				        $gte: ISODate("2017-06-21T08:45:00.000Z"),
//				        $lt:  ISODate("2017-06-21T08:45:10.000Z")
//				    }})
			
			Event.checkBaseFieldsOfAnalyticIntervalResults(/*mongoVehicleInfo.get(0),*/ generatedEventArray, this.urlParameters,startTime,endTime,deviceid );
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return generatedEventReply;
		

	}

	@Step("запрос генерирования событий по id и датам")
	public JSONObject requestRoadEventsGenerator (String deviceId,String resourceInclude, NotificationRule ruleFromPrevStep, String dateTimeFrom,String dateTimeTo){
		JSONObject rcvdFromREG = new JSONObject();
		urlParameters.clear();
		if(isADateTime(dateTimeFrom) && isADateTime(dateTimeTo)){
			urlParameters.put(CONST_TIME_FROM, dateTimeFrom);
			urlParameters.put(CONST_TIME_TO, dateTimeTo);
		}
		else {
			if(dateTimeFrom.contains("неделю") || dateTimeTo.contains("неделю")){
				 DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
				 df.setTimeZone(utc3TimeZone);
				 Date lastWeek = new Date(System.currentTimeMillis() - 7*24* 60*60 * 1000); 
					urlParameters.put(CONST_TIME_FROM, df.format(lastWeek));
					urlParameters.put(CONST_TIME_TO, getCurrentLocalDateTimePlus1Min());
					
				 
			}
			if(dateTimeFrom.contains("сутки") || dateTimeTo.contains("сутки")){
				 DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
				 df.setTimeZone(utc3TimeZone);
				 Date lastDay = new Date(System.currentTimeMillis() - 24* 60*60 * 1000); 
					urlParameters.put(CONST_TIME_FROM, df.format(lastDay));
					urlParameters.put(CONST_TIME_TO, getCurrentLocalDateTimePlus1Min());
			}
			
			if(dateTimeFrom.contains("вчера") || dateTimeTo.contains("вчера")){
				 DateFormat df = new SimpleDateFormat(standartTimeFormat, Locale.ENGLISH);
				 df.setTimeZone(utc3TimeZone);
				 Date twodaysAgo = new Date(System.currentTimeMillis() - 2*24* 60*60 * 1000); 
				 Date lastDay = new Date(System.currentTimeMillis() - 24* 60*60 * 1000); 
					urlParameters.put(CONST_TIME_FROM, df.format(twodaysAgo));
					urlParameters.put(CONST_TIME_TO, df.format(lastDay));
			}
		}
		dateTimeFrom = urlParameters.get(CONST_TIME_FROM).toString();
		dateTimeTo = urlParameters.get(CONST_TIME_TO).toString();
		log.debug("urlParameters="+ urlParameters);

		
		urlParameters.put(CONST_DEVICEID, deviceId);
		
		String url = getProperty("host") + "/Notifications/RoadEventsGenerator";
		try {
			token = getToken("userToken");
			assertTrue(CONST_TOKEN_RECEIVED,token != null,CONST_AUTH_NEEDED);
			rcvdFromREG = SendRequestToAPI.sendPostRequest( url, jsonParamsToString(urlParameters), token,false,true);
			// check correct rule triggered
			assertEquals("API:RoadEventsGenerator_trackEvent.eventType",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.eventType")    ,ruleFromPrevStep.getRoadEventType(), "RoadEventsGenerator_trackEvent.eventType не верен");
			assertEquals("API:RoadEventsGenerator_trackEvent.deviceId",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.deviceId")    ,deviceId, "RoadEventsGenerator_trackEvent.deviceId не верен");
			assertEquals("API:RoadEventsGenerator_trackEvent.relatedResourceId",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.relatedResourceId")    ,resourceInclude, "RoadEventsGenerator_trackEvent.relatedResourceId не верен");
			if(ruleFromPrevStep.getRoadEventType().equalsIgnoreCase("geozoneexit")){
				assertContains("API:RoadEventsGenerator_trackEvent.__type",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.__type").toLowerCase()    ,
						"Tone.Core.Data.Mongo.NewTrackEvents.GeozoneLeavingEvent".toLowerCase(), "RoadEventsGenerator_trackEvent.__type не верен");
			} else{
				assertContains("API:RoadEventsGenerator_trackEvent.__type",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.__type").toLowerCase()    ,ruleFromPrevStep.getRoadEventType().toLowerCase(), "RoadEventsGenerator_trackEvent.__type не верен");
			}
			assertDateIsBetween("API:RoadEventsGenerator_trackEvent.eventDeviceTime", ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.eventDeviceTime"), dateTimeFrom, dateTimeTo, "RoadEventsGenerator_trackEvent.eventDeviceTime не верен");
			assertNotNull("API:RoadEventsGenerator_trackEvent.id",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.id") , "RoadEventsGenerator_trackEvent.id не верен");
			assertContainsMngId("API:RoadEventsGenerator_trackEvent.relatedRecordId",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.relatedRecordId") , "RoadEventsGenerator_trackEvent.relatedRecordId не верен");
			
			assertEquals("API:RoadEventsGenerator_notification.accountId",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.accountId")    ,ruleFromPrevStep.getAccountId(), "RoadEventsGenerator_notification.accountId не верен");
			assertEquals("API:RoadEventsGenerator_notification.eventName",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.eventName")    ,ruleFromPrevStep.getTitle(), "RoadEventsGenerator_notification.eventName не верен");
			assertEquals("API:RoadEventsGenerator_notification.isImportant",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.isImportant")    ,String.valueOf(ruleFromPrevStep.isImportant()), "RoadEventsGenerator_notification.isImportant не верен");
			assertEquals("API:RoadEventsGenerator_notification.roadEventType",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.roadEventType")    ,ruleFromPrevStep.getRoadEventType(), "RoadEventsGenerator_notification.roadEventType не верен");
			assertEquals("API:RoadEventsGenerator_notification.resourceId",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.resourceId")    ,resourceInclude, "RoadEventsGenerator_notification.resourceId не верен");
			assertEquals("API:RoadEventsGenerator_notification.deviceId",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.deviceId")    ,deviceId, "RoadEventsGenerator_notification.deviceId не верен");
			assertEquals("API:RoadEventsGenerator_notification.notificationRuleId",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.notificationRuleId")    ,ruleFromPrevStep.getRuleId(), "RoadEventsGenerator_notification.notificationRuleId не верен");
			assertDateIsBetween("API:RoadEventsGenerator_notification.eventTime", ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.eventTime"), dateTimeFrom, dateTimeTo, "RoadEventsGenerator_notification.eventTime не верен");
 
			assertDateIsBetween("API:RoadEventsGenerator_notification.created", ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.created"), dateTimeFrom, dateTimeTo, "RoadEventsGenerator_notification.created не верен");

			if (ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.roadEventType").contains("Geozone")){
				assertContainsMngId("API:RoadEventsGenerator_notification.geozoneId",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.geozoneId") , "RoadEventsGenerator_notification.geozoneId не верен");
				Geozone geozoneFromPrevStep = (Geozone) SharedContainer.getLastObjectFromSharedContainer("geozonesForNotificationRules");
				assertEquals("API:RoadEventsGenerator_notification.geozoneId",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.geozoneId")    ,geozoneFromPrevStep.getGeozoneId(), "RoadEventsGenerator_notification.geozoneId не верен");
			}
			assertEquals("API:RoadEventsGenerator.latitude",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.latitude")   
					   										,ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.latitude"), "RoadEventsGenerator.latitude не верен");
			assertEquals("API:RoadEventsGenerator.longitude",ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.notification.longitude")   
															,ReplyToJSON.extractPathFromJson(rcvdFromREG,"$.trackEvent.longitude"), "RoadEventsGenerator.longitude не верен");

			
			setToAllureChechkedFields();
		} catch (Exception e) {
			log.error(CONST_TEST_FAILED + e);
			assertTrue(false, e.toString());

		}
		return rcvdFromREG;
		

	}
	
	@Step("Валидация: полученный JSON содержит данные с которыми событие было запрошено")
	public void checkBaseFieldsOfReceivedEvent(List<JSONObject> jsonMongoReply, ArrayList<JSONObject> jsonApiReply, JSONObject initJson) {

		try {
			for (Object item :  jsonApiReply ){
				
				JSONObject trackEvent   = (JSONObject) ((JSONObject)item).get("trackEvent");
				JSONObject notification = (JSONObject) ((JSONObject)item).get("notification");
 
					String eventDeviceTimeUnix = 	dateTimeToUnixTime(ReplyToJSON.extractPathFromJson(trackEvent, "$.eventDeviceTime"));
					for (int j=0; j<jsonMongoReply.size();j++){
						String strMongoEventDeviceTime= ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j), PATH_DEVICETIME_DATE);
						if(strMongoEventDeviceTime.equals(eventDeviceTimeUnix) ){
							
							assertEquals("API:trackEvent.eventDeviceTime ["+j+"]",eventDeviceTimeUnix, 
									ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j), PATH_DEVICETIME_DATE),"trackEvent.eventDeviceTime не верен");	
							assertEquals("API:trackEvent.relatedRecordId ["+j+"]",trackEvent.get("relatedRecordId"), 
												ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j),"$._id.$oid"),"trackEvent.relatedRecordId не верен");
							
							assertEquals("API:trackEvent.latitude ["+j+"]",trackEvent.get("latitude").toString().substring(0, 15), 
									jsonMongoReply.get(j).get("Latitude").toString().substring(0, 15),"trackEvent.latitude не верен\r\n"+jsonMongoReply.get(j));
							assertEquals("API:trackEvent.longitude ["+j+"]",trackEvent.get("longitude").toString().substring(0, 15),
									jsonMongoReply.get(j).get("Longitude").toString().substring(0, 15),"trackEvent.longitude не верен");
							
							assertEquals("API:notification.deviceId ["+j+"]",notification.get(CONST_DEVICEID).toString().substring(0, 15),
									ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j),"$.DeviceId.$oid").substring(0, 15),"notification.deviceId не верен");
							
							
							assertContainsMngId("API:notification.notificationRuleId", notification.get("notificationRuleId").toString(), 
											 "notification.notificationRuleId не верен");
							JSONObject ruleApplied = (JSONObject) GetNotificationRules.getNotificationRuleInfoById("594a9b655532f3063cdc4c74").get(0);
							
							Calendar cal = Calendar.getInstance();
							cal.setTime( new Date(Long.parseLong(strMongoEventDeviceTime)*1000L)); 
							
							assertContains("API:notification.notificationTime",Arrays.asList(ruleApplied.get("DaysOfWeek"))   ,cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US), 
									"notification.DayOfWeek не верен");	
							
							assertEquals("API:notification.Title",ruleApplied.get("Title")   ,notification.get("eventName"), "notification.Title не верен");	
							  
							assertEquals("API:notification.eventDeviceTime ["+j+"]",dateTimeToUnixTime(ReplyToJSON.extractPathFromJson(notification, "$.eventTime")), 
									ReplyToJSON.extractPathFromJson(jsonMongoReply.get(j), PATH_DEVICETIME_DATE),"notification.eventTime не верен");	
							
							assertEquals("API:notification.roadEventType",ruleApplied.get("RoadEventType"), notification.get("roadEventType"), "notification.roadEventType не верен");
							
							assertContains("API:notification.resourceId",Arrays.asList(ReplyToJSON.extractPathFromJson(ruleApplied,"$.ResourcesFilter.Include")) , 
														notification.get("resourceId").toString(), "notification.resourceId не верен");
							assertContains("API:trackEvent.relatedResourceId",Arrays.asList(ReplyToJSON.extractPathFromJson(ruleApplied,"$.ResourcesFilter.Include")) , 
									trackEvent.get("relatedResourceId").toString(), "trackEvent.relatedResourceId не верен");
							
//							CONST_DEVICEID: "5940ff14bf5efd572883a27f"	
							String ruleRoadEventArgumentType =  ReplyToJSON.extractPathFromJson(ruleApplied,"$.RoadEventArguments._t");
							String ruleLimitation = "";
							if(ruleRoadEventArgumentType.equals("TrafficRulesSpeedLimitArguments")){
								ruleLimitation =  ReplyToJSON.extractPathFromJson(ruleApplied,"$.RoadEventArguments.Overspeed");
							
//							DischargeArguments
//							GeozonesEntranceArguments
//							IdleEventArguments
//							MileageExceedanceArguments
//							SharpMoveArguments
//							StopEventArguments
//							TelematicDeviceMechanismTriggeringArguments
							String notifLimit = notification.get("limitation").toString();
							assertEquals("API:notification.limitation",ruleLimitation ,  notifLimit.substring(0, notifLimit.length()-5), "notification.limitation не верен");
							String notifRecorded = notification.get("recorded").toString();
							notifRecorded =notifRecorded.substring(0, notifRecorded.length()-5);
							assertEquals("API:notification.recorded ["+j+"]",jsonMongoReply.get(j).get("Speed").toString() ,notifRecorded  , "notification.resourceId не верен");
/*							int limitSet = Integer.parseInt(jsonMongoReply.get(j).get("xgeo_Speedlimit").toString())+Integer.parseInt(ruleLimitation) ;
							assertTrue("API:notification.recorded ["+j+"]",limitSet < Integer.parseInt(notifRecorded) , 
										"записанная скорость:"+notifRecorded+" меньше установленного лимита:"+limitSet);
*/							
							
							}
							else if(ruleRoadEventArgumentType.equals("DischargeArguments")){assertTrue(false,CONST_NOT_IMPLEMENTED+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("GeozonesEntranceArguments")){assertTrue(false,CONST_NOT_IMPLEMENTED+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("IdleEventArguments")){assertTrue(false,CONST_NOT_IMPLEMENTED+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("MileageExceedanceArguments")){assertTrue(false,CONST_NOT_IMPLEMENTED+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("SharpMoveArguments")){assertTrue(false,CONST_NOT_IMPLEMENTED+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("StopEventArguments")){assertTrue(false,CONST_NOT_IMPLEMENTED+ruleRoadEventArgumentType);
							}
							else if(ruleRoadEventArgumentType.equals("TelematicDeviceMechanismTriggeringArguments")){
								assertTrue(false,CONST_NOT_IMPLEMENTED+ruleRoadEventArgumentType);
							}
							else {
								assertTrue(false,CONST_NOT_IMPLEMENTED+ruleRoadEventArgumentType);
							}
							break;
						}
					}
			}
				setToAllureChechkedFields();
			}
		 catch (Exception e) {
			setToAllureChechkedFields();
			log.error(ERROR,e);
			assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные отчета:" + e.toString());
		}
	}
	


}
	
