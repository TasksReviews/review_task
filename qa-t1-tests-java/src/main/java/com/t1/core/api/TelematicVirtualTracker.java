package com.t1.core.api;

import java.net.URLEncoder;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jayway.jsonpath.PathNotFoundException;
import org.json.simple.JSONObject;

import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetTelematicsDevicesInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Step;

public class TelematicVirtualTracker extends AbstractClass implements SharedContainerInterface {
    private String trackerId;
    private JSONObject urlParameters = new JSONObject();
    private JSONObject initJson;
    private String trackerCode;
    private String isDeactivated;
    private String signalLevel;
    private String type;
    private String token = null;
    private String customerId;
    private long currentDateTime;
    private String model;
    private String manufacturer;
    private String simCard;
    private String resourceId;
    private String resourceType;
    private long boundTime;
    private String resourceName;

    public String getTrackerId() {
        return trackerId;
    }

    public void setTrackerId(String trackerId) {
        this.trackerId = trackerId;
    }

    public String getSignalLevel() {
        return signalLevel;
    }

    public void setSignalLevel(String signalLevel) {
        this.signalLevel = signalLevel;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getTrackerCode() {
        return trackerCode;
    }

    public void setTrackerCode(String trackerCode) {
        this.trackerCode = trackerCode;
    }

    public long getBoundTime() {
        return boundTime;
    }

    public void setBoundTime(long boundTime) {
        this.boundTime = boundTime;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getSimCard() {
        return simCard;
    }

    public void setSimCard(String simCard) {
        this.simCard = simCard;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public JSONObject getUrlParameters() {
        return urlParameters;
    }

    public void setUrlParameters(JSONObject urlParameters) {
        this.urlParameters = urlParameters;
    }

    public void putUrlParameters(Object key, Object value) {
        this.urlParameters.put(key, value);
    }

    public JSONObject getInitJson() {
        return initJson;
    }

    public void setInitJson(JSONObject initJson) {
        this.initJson = initJson;
    }


    private static final String FALSE = "false";
    private static final String AUTH_ACCOUNT_ADM_PANEL = "authorizedAccount_admPanel";
    private static final String TOKEN_RECEIVED = "token received";
    private static final String UPDATE_AUTH_NEEDED = "для изменения ТМУ необходимо пройти авторизацию";
    private static final String CREATE_AUTH_NEEDED = "для создания ТМУ необходимо пройти авторизацию";
    private static final String TEST_FAILED = "Test failed: ";
    private static final String VIRTUALTRACKERS = "virtualTrackers";

    public TelematicVirtualTracker(boolean initializeObject) {
        if (initializeObject) {
            setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues("tracker", getProperty("stdPattern")));
            this.urlParameters.clear();
            this.setTrackerCode(getInitJson().get("code").toString());
            this.urlParameters.put("code", this.getTrackerCode());
            this.isDeactivated = FALSE;
            this.setSignalLevel("0");
            this.setType("VirtualTracker");
        }
    }


    public TelematicVirtualTracker(String pattern) {
        setInitJson(InitDefValues.initializeInputWithDefaultOrRandomValues("tracker", pattern));
        this.urlParameters.clear();
        this.setTrackerCode(getInitJson().get("code").toString());
        this.urlParameters.put("code", this.getTrackerCode());
        this.setType("VirtualTracker");
    }


    @Step("Создание нового ТМУ")
    public List<org.json.simple.JSONObject> createNewVirtualTracker() {
        List<org.json.simple.JSONObject> mongoVirtualTrackerInfo = null;
        try {
            String url = getProperty("host") + "/VirtualTelematics/Tracker/Create";
            //since device creation is just a test method, it does not related to admin panel neither client panel, but needs dev rights:
            if ((SharedContainer.getContainers().containsKey(AUTH_ACCOUNT_ADM_PANEL))
                    && SharedContainer.getFromContainer(AUTH_ACCOUNT_ADM_PANEL).get(0) != "") {
                token = getCurAuthAccToken(AUTH_ACCOUNT_ADM_PANEL, "Developer");
            } else {
                token = getCurAuthAccToken(authorizedAccountT1client, "Developer");
            }
            assertTrue(TOKEN_RECEIVED, token != null, CREATE_AUTH_NEEDED);
            org.json.simple.JSONObject createVirtualTrackerReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, false, true);
            // TODO: add here assertions reply is OK and data exists;

            setTrackerId(ReplyToJSON.extractPathFromJson(createVirtualTrackerReply, "$.id"));
            TearDownExecutor.addVirtualTrackerToTeardown(this.getTrackerId());
            // get account info from mongo
            mongoVirtualTrackerInfo = GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(getTrackerId(), "");
//			// compare mongo with input
            checkVirtualTracker(mongoVirtualTrackerInfo.get(0), createVirtualTrackerReply, false);
        } catch (Exception e) {
            log.error(TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
        return mongoVirtualTrackerInfo;
    }

    @Step("Изменение ТМУ в панели администратора")
    public List<org.json.simple.JSONObject> updateVirtualTrackerInAdminPanel() {
        List<org.json.simple.JSONObject> mongoVirtualTrackerInfo = null;
        String url = getProperty("host") + "/Api/Admin/Telematics/Device/Update";

        assertTrue(this.getTrackerId() != null, "необходимо указать id изменяемого ТМУ");
        urlParameters.put("id", this.getTrackerId());

        try {
            token = getCurAuthAccToken(AUTH_ACCOUNT_ADM_PANEL);
            assertTrue(TOKEN_RECEIVED, token != null, UPDATE_AUTH_NEEDED);
            org.json.simple.JSONObject createVirtualTrackerReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, false, true);
            // TODO: add here assertions reply is OK and data exists;

            // get account info from mongo
            mongoVirtualTrackerInfo = GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(getTrackerId(), "");
//			// compare mongo with input
            checkVirtualTracker(mongoVirtualTrackerInfo.get(0), createVirtualTrackerReply, true);
        } catch (Exception e) {
            log.error(TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
        return mongoVirtualTrackerInfo;
    }

    @Step("Изменение ТМУ")
    public List<org.json.simple.JSONObject> updateVirtualTracker() {
        List<org.json.simple.JSONObject> mongoVirtualTrackerInfo = null;
        String url = getProperty("host") + "/Telematics/Device/Update";

        assertTrue(this.getTrackerId() != null, "необходимо указать id изменяемого ТМУ");
        urlParameters.put("id", this.getTrackerId());

        try {
            token = getCurAuthAccToken(authorizedAccountT1client);
            assertTrue(TOKEN_RECEIVED, token != null, UPDATE_AUTH_NEEDED);
            org.json.simple.JSONObject createVirtualTrackerReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, false, true);
            // TODO: add here assertions reply is OK and data exists;

            // get account info from mongo
            mongoVirtualTrackerInfo = GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(getTrackerId(), "");
//			// compare mongo with input
            checkVirtualTracker(mongoVirtualTrackerInfo.get(0), createVirtualTrackerReply, false);
        } catch (Exception e) {
            log.error(TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
        return mongoVirtualTrackerInfo;
    }

    @Step("Отвязка ТМУ от клиента")
    public JSONObject unbindDeviceFromCustomer() {
        org.json.simple.JSONObject mongoVirtualTrackerInfo = new JSONObject();
        String url = getProperty("host") + "/Api/Admin/Telematics/Device/ManageCustomerDevices?customerId=" + this.getCustomerId();

        assertTrue(this.getTrackerId() != null, "необходимо указать id открепляемого ТМУ");
        List<String> unbindDevices = new ArrayList<>();
        unbindDevices.add(this.getTrackerId());
        urlParameters.clear();
        urlParameters.put("unbindDevices", unbindDevices);

        try {
            token = getCurAuthAccToken(AUTH_ACCOUNT_ADM_PANEL);
            assertTrue(TOKEN_RECEIVED, token != null, UPDATE_AUTH_NEEDED);
            // sendPostRequest(urlToRead, urlParameters, accessToken, contentType, new HashMap(), requestIsPositive)
            List<org.json.simple.JSONObject> unbindVirtualTrackerReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, "", new HashMap(), true);
            assertTrue("returned reply is empty", unbindVirtualTrackerReply.isEmpty(), "unexpected reply while unbinding Device");
            // TODO: add here assertions reply is OK and data exists;

            this.setCustomerId("000000000000000000000000");
            // get account info from mongo
            mongoVirtualTrackerInfo = (JSONObject) GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(getTrackerId(), "").get(0);
// 			// compare mongo with input
            checkVirtualTracker(mongoVirtualTrackerInfo, new JSONObject(), false);

            return mongoVirtualTrackerInfo;
        } catch (Exception e) {
            log.error(TEST_FAILED + e);
            assertTrue(false, e.toString());
            return mongoVirtualTrackerInfo;
        }


    }

    @Step("Привязка ТМУ к клиенту")
    public JSONObject bindDeviceToCustomer() {
        org.json.simple.JSONObject mongoVirtualTrackerInfo = new JSONObject();
        String url = getProperty("host") + "/Api/Admin/Telematics/Device/ManageCustomerDevices?customerId=" + this.getCustomerId();

        assertTrue(this.getTrackerId() != null, "необходимо указать id прикрепляемого ТМУ");
        List<String> bindDevices = new ArrayList<String>();
        bindDevices.add(this.getTrackerId());
        urlParameters.clear();
        urlParameters.put("bindDevices", bindDevices);

        try {
            token = getCurAuthAccToken(AUTH_ACCOUNT_ADM_PANEL);
            assertTrue(TOKEN_RECEIVED, token != null, UPDATE_AUTH_NEEDED);
            org.json.simple.JSONObject bindVirtualTrackerReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, false, true);
            // TODO: add here assertions reply is OK and data exists;

            // get account info from mongo
            mongoVirtualTrackerInfo = (JSONObject) GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(getTrackerId(), "").get(0);
// 			// compare mongo with input
            checkVirtualTracker(mongoVirtualTrackerInfo, bindVirtualTrackerReply, true);

            return mongoVirtualTrackerInfo;
        } catch (Exception e) {
            log.error(TEST_FAILED + e);
            assertTrue(false, e.toString());
            return mongoVirtualTrackerInfo;
        }


    }

    @Step("Получение информации о ТМУ по id {0}")
    public List<org.json.simple.JSONObject> getVirtualTrackerById(String trackerId) {
        List<org.json.simple.JSONObject> mongoVirtualTrackerInfo = null;
        try {
            String url = getProperty("host") + "/VirtualTelematics/Tracker/" + trackerId;
            token = getCurAuthAccToken(authorizedAccountT1client);

            assertTrue(TOKEN_RECEIVED, token != null, CREATE_AUTH_NEEDED);
            org.json.simple.JSONObject getVirtualTrackerReply = (JSONObject) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token).get(0);

            // get account info from mongo
            mongoVirtualTrackerInfo = GetTelematicsDevicesInfoFromMongo.getTelematicsDevicesInfoById(trackerId, "");
//			// compare mongo with input

            checkVirtualTracker(mongoVirtualTrackerInfo.get(0), getVirtualTrackerReply, true);

        } catch (Exception e) {
            log.error(TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
        return mongoVirtualTrackerInfo;
    }

    @Step("Получение списка ТМУ")
    public org.json.simple.JSONObject getVirtualTrackersList(int pageSize, int pageNumber, String customerName, String customerRelationStatus) {
        org.json.simple.JSONObject listOfdevicesReply = null;
        try {
            customerName = URLEncoder.encode(customerName, "UTF-8");

            String apiMethod = "/Api/Admin/Telematics/Devices/ByCustomerRelation?pageSize=" + pageSize + "&pageNumber=" + pageNumber
                    + "&searchFilter=" + customerName + "&customerRelationStatus=" + customerRelationStatus;
            String url = getProperty("adminPanelHost") + apiMethod;
            token = getCurAuthAccToken(AUTH_ACCOUNT_ADM_PANEL);
            assertTrue(TOKEN_RECEIVED, token != null, CREATE_AUTH_NEEDED);
            listOfdevicesReply = (JSONObject) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token).get(0);
            //listOfdevices = (List<JSONObject>) listOfdevicesReply.get("items");
            //TODO: need to add here some basic asserts?
            //checkVirtualTracker(mongoVirtualTrackerInfo.get(0), getVirtualTrackerReply,true);
        } catch (Exception e) {
            log.error(TEST_FAILED + e);
            assertTrue(false, e.toString());
            setToAllureChechkedFields();
        }
        return listOfdevicesReply;
    }


    @Step("Удаление ТМУ {0}")
    public static void deleteVirtualTracker(String trackerId) {
        String url = getProperty("host") + "/VirtualTelematics/Tracker/delete/" + trackerId;
        try {
            String token = getToken("devToken");
            assertTrue(TOKEN_RECEIVED, token != null, "для удаления ТМУ необходимо пройти авторизацию");
            SendRequestToAPI.sendPostRequest(url, "", token, true, true);
        } catch (Exception e) {
            log.error(TEST_FAILED + e);
            assertTrue(false, e.toString());

        }
    }

    @Step("Валидация: полученный JSON содержит данные с которыми ТМУ было запрошено")
    public void checkVirtualTracker(JSONObject jsonMongoReply, JSONObject jsonApiReply, boolean nonEmptyReply) {
        try {
            if (!jsonApiReply.keySet().isEmpty() && nonEmptyReply) {
                try {
                    assertEquals("API:VirtualTracker_Id", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.id"), this.getTrackerId(), "trackerId не верен");
                    assertEquals("API:VirtualTracker_type", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.type"), this.getType(), "type не верен");
                    assertEquals("API:VirtualTracker_code", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.code"), this.getTrackerCode(), "code не верен");
                    assertEquals("API:VirtualTracker_signalLevel", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.signalLevel"), "0", "signalLevel не верен");
                    assertEquals("API:VirtualTracker_isDeactivated", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.isDeactivated"), FALSE, "isDeactivated не верен");
                    assertEquals("API:VirtualTracker_currentExecutions", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.currentExecutions"), "[]", "currentExecutions не верен");
                } catch (PathNotFoundException e) {
                    assertEquals("API:VirtualTracker_Id", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.deviceModel.id"), this.getTrackerId(), "trackerId не верен");
                    assertEquals("API:VirtualTracker_type", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.deviceModel.type"), this.getType(), "type не верен");
                    assertEquals("API:VirtualTracker_code", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.deviceModel.code"), this.getTrackerCode(), "code не верен");
                    assertEquals("API:VirtualTracker_signalLevel", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.deviceModel.signalQuality"), "0", "signalLevel не верен");
                    assertEquals("API:VirtualTracker_isDeactivated", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.deviceModel.relatedResource.isDeactivated"), FALSE, "isDeactivated не верен");
                    assertEquals("API:VirtualTracker_currentExecutions", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.deviceRelatedResource.total"), "0", "currentExecutions не верен");
                }
                try {
                    assertEquals("API:VirtualTracker_lastExecuted", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.lastExecuted"), "[]", "lastExecuted не верен");
                } catch (PathNotFoundException e) {
                }
            }
            assertContainsMngId("DB:VirtualTracker_Id", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$._id.$oid"), "trackerId не найден в ответе");
            assertEquals("DB:VirtualTracker_Id", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$._id.$oid"), this.getTrackerId(), "trackerId не верен");
            assertEquals("DB:VirtualTracker_type", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Type"), this.getType(), "type не верен");
            assertEquals("DB:VirtualTracker_code", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Code"), this.getTrackerCode(), "code не верен");
            assertEquals("DB:VirtualTracker_signalLevel", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.SignalLevel"), "0", "signalLevel не верен");
            assertEquals("DB:VirtualTracker_isDeactivated", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.IsDeactivated"), FALSE, "isDeactivated не верен");
            assertContains("DB:VirtualTracker_CurrentExecutions", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.CurrentExecutions"), "[]", "CurrentExecutions не найден");
            assertEquals("DB:VirtualTracker_currentExecutions", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.CurrentExecutions"), "[]", "currentExecutions не верен");
            assertContains("DB:VirtualTracker_LastExecuted", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.LastExecuted"), "[]", "LastExecuted не найден");
            assertEquals("DB:VirtualTracker_lastExecuted", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.LastExecuted"), "[]", "lastExecuted не верен");
            assertContains("DB:VirtualTracker_commands", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Commands"), "[", "commands не найден");
            assertContains("DB:VirtualTracker_MappingConfiguration", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.MappingConfiguration"), "[]", "MappingConfiguration не найден");
            assertEquals("DB:VirtualTracker_CustomerId", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.CustomerId.$oid"),
                    this.getCustomerId() == null ? getProperty("testCustomerId") : this.getCustomerId(), "customer не верен");

            if (jsonApiReply.keySet().isEmpty()) {
                assertTrue("DB:VirtualTracker_UnboundFromCustomer", jsonMongoReply.containsKey("UnboundFromCustomer"), "поле UnboundFromCustomer не найдено в документе");

                compareDateTimeUpTo1min("Created", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.UnboundFromCustomer.$date"), null, this.currentDateTime);
            }
/*			assertContains("DB:VirtualTracker_modules",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Modules"), "[{", "modules не найден");
			assertContains("API:VirtualTracker_modules",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.modules"), "[{","modules не найден");
			List <JSONObject> modules = ReplyToJSON.extractListPathFromJson(jsonMongoReply, "$.Modules");
			for (int i=0; i< modules.size(); i++){
				assertEquals("API:VirtualTracker_Modules.Code",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.modules["+i+"].code"), modules.get(i).get("Code"),
						"VirtualTracker_Modules.Code не верен");
				assertEquals("API:VirtualTracker_Modules.Name",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.modules["+i+"].name"), modules.get(i).get("Name"),
						"VirtualTracker_Modules.Name не верен");
				assertEquals("API:VirtualTracker_Modules.Enabled",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.modules["+i+"].enabled"), modules.get(i).get("Enabled").toString(),
						"VirtualTracker_Modules.Enabled не верен");
			}



			assertContains("DB:VirtualTracker_properties",ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Properties"), "[{", "properties не найден");
			assertContains("API:VirtualTracker_properties",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.properties"), "[{","properties не найден");

			List <JSONObject> properties = ReplyToJSON.extractListPathFromJson(jsonMongoReply, "$.Properties");
			for (int i=0; i< properties.size(); i++){
				assertEquals("API:VirtualTracker_Properties.ModuleIndex",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.properties["+i+"].moduleIndex"),
																		properties.get(i).get("ModuleIndex").toString(), "VirtualTracker_Properties.ModuleIndex не верен");
				assertEquals("API:VirtualTracker_Properties.Code",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.properties["+i+"].code"), properties.get(i).get("Code"),
						"VirtualTracker_Properties.Code не верен");
				assertEquals("API:VirtualTracker_Properties.Name",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.properties["+i+"].name"), properties.get(i).get("Name"),
						"VirtualTracker_Properties.Name не верен");
				assertEquals("API:VirtualTracker_Properties.Type",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.properties["+i+"].type"), properties.get(i).get("Type"),
						"VirtualTracker_Properties.Type не верен");
				assertEquals("API:VirtualTracker_Properties.IsReadOnly",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.properties["+i+"].isReadOnly"),
																		properties.get(i).get("IsReadOnly").toString(), "VirtualTracker_Properties.IsReadOnly не верен");
				assertEquals("API:VirtualTracker_Properties.Enabled",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.properties["+i+"].enabled"),
																	properties.get(i).get("Enabled").toString(), "VirtualTracker_Properties.Enabled не верен");
			}
 */
            if (!jsonApiReply.keySet().isEmpty() && nonEmptyReply) {
                try {
                    assertContains("API:VirtualTracker_commands", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.commands"), "[", "commands не найден");
                } catch (PathNotFoundException e) {

                }
                List<JSONObject> commands = ReplyToJSON.extractListPathFromJson(jsonMongoReply, "$.Commands");
                for (int i = 0; i < commands.size(); i++) {
                    String pathCommands = "$.commands[";

                    assertEquals("API:VirtualTracker_Commands.ModuleIndex", ReplyToJSON.extractPathFromJson(jsonApiReply, pathCommands + i + "].moduleIndex"), commands.get(i).get("ModuleIndex"),
                            "VirtualTracker_Commands.ModuleIndex не верен");
                    assertEquals("API:VirtualTracker_Commands.Code", ReplyToJSON.extractPathFromJson(jsonApiReply, pathCommands + i + "].code"), commands.get(i).get("Code"),
                            "VirtualTracker_Commands.Code не верен");
                    assertEquals("API:VirtualTracker_Commands.Name", ReplyToJSON.extractPathFromJson(jsonApiReply, pathCommands + i + "].name"), commands.get(i).get("Name"),
                            "VirtualTracker_Commands.Name не верен");
                    assertEquals("API:VirtualTracker_Commands.Description", ReplyToJSON.extractPathFromJson(jsonApiReply, pathCommands + i + "].description"), commands.get(i).get("Description"),
                            "VirtualTracker_Commands.Description не верен");
                    assertEquals("API:VirtualTracker_Commands.Arguments", ReplyToJSON.extractPathFromJson(jsonApiReply, pathCommands + i + "].arguments"), commands.get(i).get("Arguments"),
                            "VirtualTracker_Commands.Arguments не верен");
                    assertEquals("API:VirtualTracker_Commands.Enabled", ReplyToJSON.extractPathFromJson(jsonApiReply, pathCommands + i + "].enabled"), commands.get(i).get("Enabled"),
                            "VirtualTracker_Commands.Enabled не верен");
                }
                try {
                    assertContains("API:VirtualTracker_mappingConfiguration", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.mappingConfiguration"), "[]", "mappingConfiguration не найден");
                    assertContains("API:VirtualTracker_currentExecutions", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.currentExecutions"), "[]", "currentExecutions не найден");
                    assertContains("API:VirtualTracker_lastExecuted", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.lastExecuted"), "[]", "lastExecuted не найден");
                    assertEquals("API:VirtualTracker_CustomerId", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.customerId"),
                            this.getCustomerId() == null ? getProperty("testCustomerId") : this.getCustomerId(), "customer не верен");
                } catch (PathNotFoundException e) {
                    assertEquals("API:VirtualTracker_CustomerId", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.deviceModel.customerId"),
                            this.getCustomerId() == null ? getProperty("testCustomerId") : this.getCustomerId(), "customer не верен");
                }
            }
            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
            assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные новогого ТМУ:" + e.toString());
        }

    }

    @Override
    public int removeFromSharedContainer() {
        int indexOfRemovedEntity = -1;
        if (!container.isEmpty() && container.containsKey(VIRTUALTRACKERS) && !container.get(VIRTUALTRACKERS).isEmpty()) {
            ArrayList listOfIds = container.get(VIRTUALTRACKERS);
            // удаление из контейнера
            if (listOfIds.get(0) instanceof com.t1.core.api.TelematicVirtualTracker) {
                for (int i = 0; i < listOfIds.size(); i++) {
                    if (((TelematicVirtualTracker) listOfIds.get(i)).getTrackerId().equals(this.getTrackerId())) {
                        listOfIds.remove(i);
                        indexOfRemovedEntity = i;
                    }
                }
            }
        }
        return indexOfRemovedEntity;
    }

    @Override
    public void addToSharedContainer() {
        addToSharedContainer(-1);
    }

    @Override
    @SuppressWarnings({"unchecked", "serial", "rawtypes"})
    public void addToSharedContainer(int index) {
        if (SharedContainer.getContainers().containsKey(VIRTUALTRACKERS) && !SharedContainer.getContainers().get(VIRTUALTRACKERS).isEmpty()) {
            ArrayList<TelematicVirtualTracker> existingArray = (ArrayList<TelematicVirtualTracker>) SharedContainer.getContainers().get(VIRTUALTRACKERS);
            if (index >= 0) {
                existingArray.add(index, this);
            } else {
                existingArray.add(this);
            }
            container.put(VIRTUALTRACKERS, existingArray);
        } else {
            ArrayList newArray = new ArrayList();
            newArray.add(this);
            container.put(VIRTUALTRACKERS, newArray);
        }
    }

    @Override
    public void updateInSharedContainer() {
        int index = this.removeFromSharedContainer();
        this.addToSharedContainer(index);
    }

    public static TelematicVirtualTracker getFromSharedContainer(String oldDeviceId) {
        ArrayList<TelematicVirtualTracker> telematicsDeviceList = (ArrayList<TelematicVirtualTracker>) SharedContainer.getObjectsFromSharedContainer("all", VIRTUALTRACKERS);
        TelematicVirtualTracker foundDevice = null;
        for (TelematicVirtualTracker device : telematicsDeviceList) {
            if (device.getTrackerId().equals(oldDeviceId)) {
                foundDevice = device;
                break;
            }
        }
        return foundDevice;
    }


}
