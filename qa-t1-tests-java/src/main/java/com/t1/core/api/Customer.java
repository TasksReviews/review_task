package com.t1.core.api;

import static com.mongodb.client.model.Filters.eq;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.mongodb.client.model.Filters;
import com.t1.core.AbstractClass;
import com.t1.core.InitDefValues;
import com.t1.core.ReplyToJSON;
import com.t1.core.SendRequestToAPI;
import com.t1.core.SharedContainer;
import com.t1.core.SharedContainerInterface;
import com.t1.core.TearDownExecutor;
import com.t1.core.mongodb.GetAccountInfoFromMongo;
import com.t1.core.mongodb.GetDevices;
import com.t1.core.mongodb.GetInfoFromMongo;

import ru.yandex.qatools.allure.annotations.Step;

public class Customer extends AbstractClass implements SharedContainerInterface {

    private String customerId;
    private String name;
    private String address;
    private String description;
    private String phone;
    private String email;

    protected static JSONObject urlParameters = new JSONObject();
    private JSONObject initMap = new JSONObject();
    private JSONObject superCustomer = new JSONObject();
    private String token = null;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject getInitMap() {
        return initMap;
    }

    public void setInitMap(JSONObject initMap) {
        this.initMap = initMap;
    }

    private static final String CONST_ADDRESS = "address";
    private static final String CONST_DESCRIPTION = "description";
    private static final String CONST_PHONE = "phone";
    private static final String CONST_EMAIL = "email";
    private static final String CONST_CUSTOMERS = "Customers";
    private static final String CONST_CUSTOMERS2 = "customers";
    private static final String CONST_ACCOUNTS = "accounts";
    private static final String CONST_TESTFAILED = "Test failed: ";
    private static final String CONST_CUSTOMERID_WRONG = "customerId не верен";
    private static final String CONST_TOKEN_RECEIVED = "token received";
    private static final String CONST_AUTH_NEEDED_CLIENT = "для удаления клиента необходимо пройти авторизацию";
    private static final String CONST_CUSTOMERID = "CustomerId";
    private static final String CONST_USERTOKEN = "userToken";
    private static final String CONST_AUTH_NEEDED_VEH = "для получения ТС необходимо пройти авторизацию";

    private static final String PATH_ID = "$._id.$oid";


    public Customer() {
        this.setInitMap(InitDefValues.initializeInputWithDefaultOrRandomValues("customer", getProperty("stdPattern")));
        Customer.urlParameters.clear();
        this.setName(getInitMap().get("customer_name").toString());
        this.address = getInitMap().get(CONST_ADDRESS).toString();
        this.description = getInitMap().get(CONST_DESCRIPTION).toString();
        this.phone = getInitMap().get(CONST_PHONE).toString();
        this.email = getInitMap().get(CONST_EMAIL).toString();
        this.superCustomer.put("id", "000000000000000000000001");
        this.superCustomer.put("name", "T1");
        urlParameters.put("name", this.getName());
        urlParameters.put(CONST_ADDRESS, this.address);
        urlParameters.put(CONST_DESCRIPTION, this.description);
        urlParameters.put(CONST_PHONE, this.phone);
        urlParameters.put(CONST_EMAIL, this.email);
        urlParameters.put("superCustomer", this.superCustomer);
    }

    @Step("Создание нового клиента")
    public List<org.json.simple.JSONObject> customerCreate() {
        token = getAnyToken();
        String url = getProperty("adminPanelHost") + "/Api/Customer/Create";
        try {
            org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, false, true);
            this.setCustomerId(registrationReply.get("id").toString());
            TearDownExecutor.addCustomerToTeardown(this.getCustomerId());
            List<org.json.simple.JSONObject> mongoCustomerInfo = GetInfoFromMongo.getInfoById(CONST_CUSTOMERS, this.getCustomerId(), "");
            checkCreatedCustomer(mongoCustomerInfo.get(0), registrationReply, this.getInitMap(), null, null);
            return mongoCustomerInfo;
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(ERROR, e);
            assert (false);
        }
        return null;
    }


    private void checkCustomers(List<JSONObject> mongoCustomersInfo, JSONArray apiRequestReply, Long totalAccounts, Long totalDevices) {
        try {
            for (JSONObject customerMongo : mongoCustomersInfo) {
                JSONObject apiCustomer = new JSONObject();
                String id = ReplyToJSON.extractPathFromJson(customerMongo, PATH_ID);
                for (int j = 0; j < apiRequestReply.size(); j++) {
                    if (((JSONObject) apiRequestReply.get(j)).get("id").toString().matches(id)) {
                        apiCustomer = (JSONObject) apiRequestReply.get(j);
                    }
                }
                checkCreatedCustomer(customerMongo, apiCustomer, new JSONObject(), totalAccounts, totalDevices);
            }
        } catch (Exception e) {
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());
        }
    }

    private void checkCreatedCustomer(JSONObject jsonMongoReply, JSONObject jsonApiReply, JSONObject initMap, Long totalAccounts, Long totalDevices) {
        String curCustomerId = "";
        log.debug("check start");
        try {
            assertContainsMngId("API:customer_Id", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.id"), CONST_CUSTOMERID_WRONG);
            if (this.getCustomerId() != null) {
                assertEquals("DB:customer_Id", ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_ID), this.getCustomerId(), CONST_CUSTOMERID_WRONG);
                curCustomerId = this.getCustomerId();
            } else {
                curCustomerId = ReplyToJSON.extractPathFromJson(jsonMongoReply, PATH_ID);
            }

            if (initMap != null && !initMap.keySet().isEmpty()) {
                this.name = initMap.get("customer_name").toString();
                this.address = initMap.get(CONST_ADDRESS).toString();
                this.description = initMap.get(CONST_DESCRIPTION).toString();
                this.phone = initMap.get(CONST_PHONE).toString();
                this.email = initMap.get(CONST_EMAIL).toString();


                assertEquals("DB:customer_name", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Name"), this.name, "name не верен");
                assertEquals("DB:customer_address", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Address"), this.address, "address не верен");
                assertEquals("DB:customer_description", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Description"), this.description, "description не верен");
                assertEquals("DB:customer_phone", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Phone"), this.phone, "phone не верен");
                assertEquals("DB:customer_email", ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Email"), this.email, "email не верен");

            }

            assertEquals("API:customer_Id", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.id"), curCustomerId, CONST_CUSTOMERID_WRONG);
            assertEquals("API:customer_name", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.name"),
                    ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Name"), "name не верен");
            assertEquals("API:customer_address", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.address"),
                    ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Address"), "address не верен");
            assertEquals("API:customer_description", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.description"),
                    ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Description"), "description не верен");
            assertEquals("API:customer_phone", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.phone"),
                    ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Phone"), "phone не верен");
            assertEquals("API:customer_email", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.email"),
                    ReplyToJSON.extractPathFromJson(jsonMongoReply, "$.Email"), "email не верен");

            if (jsonApiReply.containsKey("devices")) {
                if (totalDevices == null)
                    totalDevices = GetDevices.getDevicesCountInfo();
                Long customerDevices = GetDevices.getDevicesCountInfoByCustomer(curCustomerId);
//			assertEquals("API:customer_devices.total",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.devices.total"),totalDevices.toString(), "customer_devices.total не верен");
                assertEquals("API:customer_devices.customer", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.devices.customer"), customerDevices.toString(), "customer_devices.customer не верен");
            }
            if (jsonApiReply.containsKey(CONST_ACCOUNTS)) {
//			if(totalAccounts== null){
//				totalAccounts =	GetAccountInfoFromMongo.getAccountsCountInfo();
//			}
                Long customerAccounts = GetAccountInfoFromMongo.getAccountsCountInfoByCustomer(curCustomerId);
//			assertEquals("API:customer_accounts.total",ReplyToJSON.extractPathFromJson(jsonApiReply, "$.accounts.total"),totalAccounts.toString(), "customer_accounts.total не верен");
                assertEquals("API:customer_accounts.customer", ReplyToJSON.extractPathFromJson(jsonApiReply, "$.accounts.customer"), customerAccounts.toString(), "customer_accounts.customer не верен");

            }
            setToAllureChechkedFields();

        } catch (Exception e) {
            log.error(ERROR, e);
            setToAllureChechkedFields();
            assertTrue(false, "ошибка при попытке сравнить запрошенные и фактические даные новогого клиента:" + e.toString());
        }

    }

    @Step("Добавление связи между аккаунтом {0} и клиентом {1}")
    public static org.json.simple.JSONObject addAccountRelationToCustomer(String accountId, String customerId) {
        org.json.simple.JSONObject relationReqReply = null;
        try {
            urlParameters.clear();
            String url = getProperty("host") + "/Api/Customer/AddAccount?customerId=" + customerId + "&accountId=" + accountId;
            relationReqReply = SendRequestToAPI.sendPostRequest(url, "", getAnyToken(), false, true);
            assertEquals("API:accountrelationAdd.code", relationReqReply.get("code"), "Ok", "accountPhoneUpdate.code не верен");
            org.json.simple.JSONObject mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(accountId, "").get(0);
            assertEquals("DB:accountrelationAdd.customerId", ReplyToJSON.extractPathFromJson(mongoAccountInfo, "CustomerId.$oid"), customerId, "accountrelationAdd.customerId не верен");
            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());
        }
        return relationReqReply;
    }

    @Step("Удаление связи между аккаунтом {0} и клиентом {1}")
    public static org.json.simple.JSONObject removeAccountRelationToCustomer(String accountId, String customerId) {
        org.json.simple.JSONObject removeRelationReqReply = null;
        try {
            urlParameters.clear();
            String url = getProperty("host") + "/Api/Customer/RemoveAccount?customerId=" + customerId + "&accountId=" + accountId;
            removeRelationReqReply = SendRequestToAPI.sendPostRequest(url, "", getAnyToken(), false, true);
            assertEquals("API:accountrelationAdd.code", removeRelationReqReply.get("code"), "Ok", "accountrelationAdd.code не верен");
            org.json.simple.JSONObject mongoAccountInfo = GetAccountInfoFromMongo.getAccountInfoByAccountId(accountId, "").get(0);
            assertEquals("DB:accountrelationAdd.customerId", ReplyToJSON.extractPathFromJson(mongoAccountInfo, "CustomerId.$oid"),
                    "000000000000000000000000", "accountrelationAdd.customerId не верен");
            setToAllureChechkedFields();
        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());
        }
        return removeRelationReqReply;

    }


    @Step("Изменение клиента {0}")
    public void updateCustomer() {
        String url = getProperty("host") + "/Api/Customer/Update";
        try {
            token = getToken("devToken");
            assertTrue(CONST_TOKEN_RECEIVED, token != null, CONST_AUTH_NEEDED_CLIENT);
            urlParameters.put("id", this.getCustomerId());
            org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, false, true);
            List<org.json.simple.JSONObject> mongoCustomerInfo = GetInfoFromMongo.getInfoById(CONST_CUSTOMERS, getCustomerId(), "");
            checkCreatedCustomer(mongoCustomerInfo.get(0), registrationReply, this.getInitMap(), null, null);
        } catch (Exception e) {
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());

        }

    }

    @Step("Изменение настроек клиента {0}")
    public void updateCustomerSettings(String customerId, JSONObject settingsToSend) {
        String url = getProperty("host") + "/DriveStyleSettings/Manage";
        try {
            token = getCurAuthAccToken("authorizedAccount_t1client");
            assertTrue(CONST_TOKEN_RECEIVED, token != null, CONST_AUTH_NEEDED_CLIENT);
            Customer.urlParameters = settingsToSend;
            org.json.simple.JSONObject registrationReply = SendRequestToAPI.sendPostRequest(url, jsonParamsToString(urlParameters), token, false, true);
            List<org.json.simple.JSONObject> mongoCustomerInfo = GetInfoFromMongo.getInfoById(CONST_CUSTOMERS, customerId, "");
//			what to check here ? returns same as sent.
            //checkCreatedCustomer(mongoCustomerInfo.get(0), registrationReply,this.initMap, null,null);
        } catch (Exception e) {
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());

        }

    }


    @Step("Удаление клиента {0}")
    public void deleteCustomer(String customerId) {
        String url = getProperty("host") + "/Api/Customer/Delete/" + customerId;
        try {
            token = getToken("devToken");
            assertTrue(CONST_TOKEN_RECEIVED, token != null, CONST_AUTH_NEEDED_CLIENT);

            SendRequestToAPI.sendPostRequest(url, "", token, true, true);
            checkCustomerDeleted(customerId);
        } catch (Exception e) {
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());

        }

    }

    public void checkCustomerDeleted(String customerId) {
        try {

            //Коллекции БД: Customers, Devices, Accounts, Employees, Vehicles.

//				db.getCollection('DeviceHistory').find({})   			(отвязано
//				db.getCollection('Employees').find({}) 					(удалено)
//				db.getCollection('Vehicles').find({}) 					(удалена связь)			
//				db.getCollection('NotificationRules').find({}) 			(НЕ удалено)
//				db.getCollection('OmGroups').find({}) 					(удалено из группы)
//				db.getCollection('ReportTemplates').find({})			(удалено)

            List<org.json.simple.JSONObject> deletedCustomerjsonMongoReply = GetInfoFromMongo.getInfoById(CONST_CUSTOMERS, this.getCustomerId(), "");
            assertTrue("DB: checkCustomerDeleted", deletedCustomerjsonMongoReply.isEmpty(),
                    "была найдена информация о сотруднике в коллекции \"Customers\":" + deletedCustomerjsonMongoReply.toString());

            if (SharedContainer.getContainers().containsKey(CONST_ACCOUNTS)) {
                List<org.json.simple.JSONObject> mongoAccountsRelatedToCustomer = (List<org.json.simple.JSONObject>) GetAccountInfoFromMongo.getAccountsListInfoByCustomer(this.getCustomerId());
                assertTrue("DB: AccountsRelatedToCustomer", mongoAccountsRelatedToCustomer.isEmpty(),
                        "была найдена информация о аккаунте связанном с удаленным клиентом в коллекции \"Accounts\":" + mongoAccountsRelatedToCustomer.toString());
            }

            if (SharedContainer.getContainers().containsKey("virtualTrackers")) {
                Bson filter = Filters.and(eq(CONST_CUSTOMERID, new ObjectId(customerId)));
                List<org.json.simple.JSONObject> mongoDevicesRelatedToCustomer = GetInfoFromMongo.getInfoByCustomFilters("Devices", filter, new ArrayList<String>());
                assertTrue("DB: DevicesRelatedToCustomer", mongoDevicesRelatedToCustomer.isEmpty(),
                        "была найдена информация о ТМУ связанном с удаленным клиентом в коллекции \"Devices\":" + mongoDevicesRelatedToCustomer.toString());
            }

            if (SharedContainer.getContainers().containsKey("employees")) {
                Bson filter = Filters.and(eq(CONST_CUSTOMERID, new ObjectId(customerId)));
                List<org.json.simple.JSONObject> mongoEmployeesRelatedToCustomer = GetInfoFromMongo.getInfoByCustomFilters("Employees", filter, new ArrayList<String>());
                assertTrue("DB: EmployeesRelatedToCustomer", mongoEmployeesRelatedToCustomer.isEmpty(),
                        "была найдена информация о сотруднике связанном с удаленным клиентом в коллекции \"Employees\":" + mongoEmployeesRelatedToCustomer.toString());
            }

            if (SharedContainer.getContainers().containsKey("vehicles")) {
                Bson filter = Filters.and(eq(CONST_CUSTOMERID, new ObjectId(customerId)));
                List<org.json.simple.JSONObject> mongoVehiclesRelatedToCustomer = GetInfoFromMongo.getInfoByCustomFilters("Vehicles", filter, new ArrayList<String>());
                assertTrue("DB: VehiclesRelatedToCustomer", mongoVehiclesRelatedToCustomer.isEmpty(),
                        "была найдена информация о ТС связанном с удаленным клиентом в коллекции \"Vehicles\":" + mongoVehiclesRelatedToCustomer.toString());
            }
	

/*	if(this.groupId!=null && this.groupId!=""){
		JSONObject groupInfo =	(JSONObject) GetGroupInfoFromMongo.getGroupsInfoById(this.groupId).get(0);
		
		List<String> objectsInGroup = ReplyToJSON.extractListPathFromJson(groupInfo,"$.Objects..$oid");
		
		assertTrue("DB:OmGroups.Objects", ! objectsInGroup.contains(employeeId),
				   "была найдена информация о сотруднике в коллекции \"OmGroups\":"+groupInfo.toString());
	}

		if(this.notifRuleRelated!=null && this.notifRuleRelated!=""){
		List foundNotificationRules = GetNotificationRules.getnotificationRuleInfoById(this.notifRuleRelated);
		if(foundNotificationRules.size()>0){
		JSONObject notifRuleInfo = (JSONObject) foundNotificationRules.get(0);
		List<String> objectsInRule = ReplyToJSON.extractListPathFromJson(notifRuleInfo,"$.ResourcesFilter.Include");
		
		assertTrue("DB:NotificationRules.ResourcesFilter.Include", ! objectsInRule.contains(employeeId),
				   "была найдена информация о сотруднике в коллекции \"NotificationRules\":"+notifRuleInfo.toString());
	log.debug(notifRuleInfo);
		}else{
			log.warn("rule wasnt found, is it ok?");
		}
	}
		teardownData.remove(CONST_CUSTOMERS2,  Arrays.asList(customerId));
		SharedContainer.removeFromContainer("customer",  customerId);
*/
            setToAllureChechkedFields();


        } catch (Exception e) {
            setToAllureChechkedFields();
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());
        }
    }


    @Step("запрос информации о клиенте {0}")
    public JSONObject getCustomerInfoById(String customerId) {
        String url = getProperty("host") + "/Api/Customer/" + customerId;
        return getCustomer(url);
    }

    public List<JSONObject> getAllCustomers() {
        String url = getProperty("host") + "/Api/Customer/GetAll";
        try {
            token = getToken(CONST_USERTOKEN);
            assertTrue(CONST_TOKEN_RECEIVED, token != null, CONST_AUTH_NEEDED_VEH);
            JSONArray requestReply = (JSONArray) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token);
            List<org.json.simple.JSONObject> mongoCustomerInfo = GetInfoFromMongo.getInfoByCustomFilters(CONST_CUSTOMERS, null, new ArrayList());
            Long totalDevices = GetDevices.getDevicesCountInfo();
            Long totalAccounts = GetAccountInfoFromMongo.getAccountsCountInfo();
            checkCustomers(mongoCustomerInfo, requestReply, totalAccounts, totalDevices);


            return requestReply;
        } catch (Exception e) {
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());
            return null;
        }

    }

    public JSONObject getCurrentCustomerInfo(String customerId) {
        String apiMethod = "";
        if (customerId == null || customerId.equals("")) {
            apiMethod = "/Api/Customer/Current";
        } else {
            apiMethod = "/Api/Customer/Current?CustomerId=" + customerId;
        }
        String url = getProperty("host") + apiMethod;

        return getCustomer(url);
    }

    public JSONObject getCustomer(String url) {
        try {
            token = getToken(CONST_USERTOKEN);
            assertTrue(CONST_TOKEN_RECEIVED, token != null, CONST_AUTH_NEEDED_VEH);
            JSONObject requestReply = (JSONObject) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token).get(0);
            List<org.json.simple.JSONObject> mongoCustomerInfo = GetInfoFromMongo.getInfoById(CONST_CUSTOMERS, this.getCustomerId(), "");
            checkCreatedCustomer(mongoCustomerInfo.get(0), requestReply, this.getInitMap(), null, null);
            return requestReply;
        } catch (Exception e) {
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());
            return null;
        }
    }


    public List<JSONObject> getAcctsBoundToCustomer(String customerId, int skip, int limit) {
        String url = getProperty("host") + "/Api/Customer/BoundAccounts?customerId=" + customerId + "&skip=" + skip + "&limit=" + limit;
        try {
            token = getToken(CONST_USERTOKEN);
            assertTrue(CONST_TOKEN_RECEIVED, token != null, CONST_AUTH_NEEDED_VEH);
            List<JSONObject> requestReply = (List<JSONObject>) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token);
            JSONObject accounts = (JSONObject) requestReply.get(0).get("accounts");
            Long customer = (Long) accounts.get("customer");
            List<org.json.simple.JSONObject> mongoCustomersAccountsInfo = GetAccountInfoFromMongo.getAccountsListInfoByCustomer(customerId);
            int totalMongoAccounts = mongoCustomersAccountsInfo.size();
            if (totalMongoAccounts > skip) {
                totalMongoAccounts = totalMongoAccounts - skip;
                assertTrue("API: accounts related to customer",
                        customer == totalMongoAccounts ||
                                customer == limit, "количество аккаунтов которое вернул API некорректно");
//                compareMongoAndApiLists(mongoCustomersAccountsInfo, requestReply);
            } else if (skip >= totalMongoAccounts) {
                totalMongoAccounts = 0;
            }
            return requestReply;
        } catch (Exception e) {
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());
            return null;
        }

    }


    void compareMongoAndApiLists(List<JSONObject> mongoCustomersAccountsInfo, List<JSONObject> requestReply) {
        try {

            for (JSONObject apiAccount : requestReply) {
                JSONObject mongoAccount = new JSONObject();
                String id = apiAccount.get("id").toString();
                for (int j = 0; j < mongoCustomersAccountsInfo.size(); j++) {

                    if (ReplyToJSON.extractPathFromJson((JSONObject) mongoCustomersAccountsInfo.get(j), PATH_ID).matches(id)) {
                        mongoAccount = mongoCustomersAccountsInfo.get(j);
                        break;
                    }
                    AccountInfoAndSettings.checkCustomerAccountsSettings(mongoAccount, apiAccount);
                }

            }

        } catch (Exception e) {
            log.error(ERROR, e);
        }
    }

    public List<JSONObject> getAcctsUnBoundToCustomer(String dateTimeCreated, int skip, int limit) {
        if (dateTimeCreated == null || dateTimeCreated.equals("")) {
            dateTimeCreated = "0000-01-01 00:00:00".replace(" ", "%20");
        }
        String url = getProperty("host") + "/Api/Customer/UnboundAccounts?dateCreated=" + dateTimeCreated.replace(" ", "%20") + "&skip=" + skip + "&limit=" + limit;
        try {
            token = getToken(CONST_USERTOKEN);
            assertTrue(CONST_TOKEN_RECEIVED, token != null, CONST_AUTH_NEEDED_VEH);
            List<JSONObject> requestReply = (List<JSONObject>) SendRequestToAPI.sendGetRequest(url, "GET", "", "", token);
            JSONObject accounts = (JSONObject) requestReply.get(0).get("accounts");
            Long customer = (Long) accounts.get("unbound");
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone("UTC+3:00"));

            Bson filter = Filters.and(Filters.eq(CONST_CUSTOMERID, new ObjectId("000000000000000000000000")),
                    Filters.gte("Created", format.parse(dateTimeCreated.replace("%20", " "))));
            List<org.json.simple.JSONObject> mongoCustomersAccountsInfo = GetInfoFromMongo.getInfoByCustomFilters("Accounts", filter, new ArrayList<String>());
            int totalMongoAccounts = mongoCustomersAccountsInfo.size();
            if (totalMongoAccounts > skip) {
//                totalMongoAccounts = totalMongoAccounts - skip;
                assertTrue("API: accounts related to customer",
                        customer == totalMongoAccounts ||
                                customer == limit, "количество аккаунтов которое вернул API некорректно");
//                compareMongoAndApiLists(mongoCustomersAccountsInfo, requestReply);
            } else if (skip > totalMongoAccounts) {
                totalMongoAccounts = 0;
            }
            return requestReply;
        } catch (Exception e) {
            log.error(CONST_TESTFAILED + e);
            assertTrue(false, e.toString());
            return null;
        }

    }

    @Override
    public int removeFromSharedContainer() {
        int indexOfRemovedEntity = -1;
        if (!container.isEmpty() && container.containsKey(CONST_CUSTOMERS2) && !container.get(CONST_CUSTOMERS2).isEmpty()) {
            ArrayList listOfIds = container.get(CONST_CUSTOMERS2);
            // удаление из контейнера
            if (listOfIds.get(0) instanceof com.t1.core.api.Customer) {
                for (int i = 0; i < listOfIds.size(); i++) {
                    if (((Customer) listOfIds.get(i)).getCustomerId().equals(this.getCustomerId())) {
                        listOfIds.remove(i);
                        indexOfRemovedEntity = i;
                    }
                }
            }
        }
        return indexOfRemovedEntity;
    }

    @Override
    public void addToSharedContainer() {
        addToSharedContainer(-1);
    }

    @Override
    @SuppressWarnings({"unchecked", "serial", "rawtypes"})
    public void addToSharedContainer(int index) {
        if (SharedContainerInterface.getContainers().containsKey(CONST_CUSTOMERS2) && !SharedContainerInterface.getContainers().get(CONST_CUSTOMERS2).isEmpty()) {
            ArrayList<Customer> existingArray = (ArrayList<Customer>) SharedContainerInterface.getContainers().get(CONST_CUSTOMERS2);
            if (index >= 0) {
                existingArray.add(index, this);
            } else {
                existingArray.add(this);
            }
            container.put(CONST_CUSTOMERS2, existingArray);
        } else {
            ArrayList newArray = new ArrayList();
            newArray.add(this);
            container.put(CONST_CUSTOMERS2, newArray);
        }
    }

    @Override
    public void updateInSharedContainer() {
        int index = this.removeFromSharedContainer();
        this.addToSharedContainer(index);
    }
}
