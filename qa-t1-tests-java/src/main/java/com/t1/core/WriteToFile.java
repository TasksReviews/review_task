package com.t1.core;

import java.io.FileWriter;

import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class WriteToFile {
	private WriteToFile() {}
	protected static final String ERROR = "Error";
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("WriteToFile");
	public static Boolean writeJsonToFile(String path, JSONObject obj) {
		try (FileWriter file = new FileWriter(path)){
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String jsonOutput = gson.toJson(obj);
				file.write(jsonOutput);
				file.flush();
				file.close();
				return true;
		} catch (Exception e) {
			log.error(ERROR,e);
			return false;
		}
	}
	

		
}
