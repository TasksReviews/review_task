package com.t1.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.bson.Document;
import org.bson.UuidRepresentation;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.UuidCodec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.json.JsonWriterSettings;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.mongodb.MongoClient;

public class ReplyToJSON {
	
	private ReplyToJSON(){}
	
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("ReplyToJSON");
	protected static final String ERROR = "Error";
	protected static final String ERR_PATH = "not able to get requested JSON field by path ";
	
	public static String extractPathFromJson(LinkedHashMap jsonString, String jsonPathExpression) {
		return JsonPath.read(jsonString, jsonPathExpression);
	}

	public static String extractPathFromJson(HashMap jsonString, String jsonPathExpression) {
		return JsonPath.read(jsonString, jsonPathExpression);
		}
	
	public static String extractPathFromJson(org.json.simple.JSONObject jsonString, String jsonPathExpression){
		try {
			return JsonPath.read(jsonString, jsonPathExpression).toString();
		} catch (Exception e) {
			if(JsonPath.read(jsonString, jsonPathExpression) !=null){
				log.error("not able to convert requested JSON field to String");
			}
			return null;
		}

	}
	
	public static JSONArray extractListPathFromJson(org.json.simple.JSONObject jsonString, String jsonPathExpression) {
		JSONParser parser = new JSONParser();
		try {
			String result = JsonPath.read(jsonString, jsonPathExpression).toString();
		// if returned array of string datetime, convert to type Date? otherwise get json parser exception for :
			return (org.json.simple.JSONArray) parser.parse(result);
		} catch (Exception e) {
			if(JsonPath.read(jsonString, jsonPathExpression) !=null){
				log.error(ERR_PATH+jsonPathExpression);
			}
			return null;
		}

	}
	
	public static JSONArray extractListPathFromJson(org.json.simple.JSONArray jsonStringArr, String jsonPathExpression) {
		JSONParser parser = new JSONParser();
		try {
			String result = JsonPath.read(jsonStringArr, jsonPathExpression).toString();
		// if returned array of string datetime, convert to type Date? otherwise get json parser exception for :
			return (org.json.simple.JSONArray) parser.parse(result);
		} catch (Exception e) {
			if(JsonPath.read(jsonStringArr, jsonPathExpression) !=null){
				log.error(ERR_PATH+jsonPathExpression);
			}
			return null;
		}

	}
	
	public static JSONArray extractListPathFromJson(ArrayList jsonStringArr, String jsonPathExpression) {
		JSONParser parser = new JSONParser();
		try {
			String result = JsonPath.read(jsonStringArr, jsonPathExpression).toString();
		// if returned array of string datetime, convert to type Date? otherwise get json parser exception for :
			return (org.json.simple.JSONArray) parser.parse(result);
		} catch (Exception e) {
			if(JsonPath.read(jsonStringArr, jsonPathExpression) !=null){
				log.error(ERR_PATH+jsonPathExpression);
			}
			return null;
		}

	}
	
	public static String getJsonPathFromDocument(Document document, String jsonPathExpression) {
		try {
			Object odocument = Configuration.defaultConfiguration().jsonProvider().parse(document.toJson());
			return JsonPath.read(odocument, jsonPathExpression);
		}
		catch(Exception e) {
			log.error(ERROR,e);
    		return null;
        }
	}
	
	public static org.json.simple.JSONObject getJsonObjectFromDocument(Document document) {
		try {
			CodecRegistry codecRegistry = CodecRegistries.fromRegistries(
					CodecRegistries.fromCodecs(new UuidCodec(
							UuidRepresentation.STANDARD)), MongoClient
							.getDefaultCodecRegistry());
			String result = document.toJson(new JsonWriterSettings(), new DocumentCodec(codecRegistry));
		return AbstractClass.toJson(result);
		        
		}
		catch(Exception e) {
			log.error(ERROR,e);
    		return null;
        }
	}

	public static List getJsonObjectFromDocumentList(List<Document> documentList) {
		List results =  new ArrayList();
			for (Document document: documentList){
				results.add(getJsonObjectFromDocument(document));
			}
		return results;
	}

}
