package com.t1.core;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import ru.yandex.qatools.allure.annotations.Step;

public class SendRequestToAPI extends AbstractClass{
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("SendHttpRequestToAPI");
	static String crlf = "\r\n";
	static String twoHyphens = "--";
	static String boundary =  RandomStringUtils.random(33,"-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
	
	static String httpConnectTimeout = "httpConnectTimeout"; 
	static String httpReadTimeout = "httpReadTimeout";
	static String authorization  = "Authorization" ;
	static String multipartStr = "multipart";
	static String failedDueTo = "failed due to : ";
	static String contentTypeImage = "Content-Type: image/";
	
	
	/**
	 * send get request with known token
	 * @param urlToRead
	 * @param requestType
	 * @param user
	 * @param pwd
	 * @param accessToken
	 * @return
	 * @throws IOException 
	 * @throws Exception
	 */
	@Step ("отправка GET запроса {0}")
	public static org.json.simple.JSONArray sendGetRequest(String urlToRead, String requestType, String user, String pwd, String accessToken) throws IOException {
		JSONArray jsonReplyData = new JSONArray();
		if(requestType==null || requestType=="")
		requestType = "GET";
	      URL url = new URL(urlToRead);
	      log.info("Sending 'GET' request to URL : " + urlToRead);
	      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	      conn.setConnectTimeout(Integer.parseInt(getProperty(httpConnectTimeout))); //30secs
	      conn.setReadTimeout   (Integer.parseInt(getProperty(httpReadTimeout)));	   //29 secs   
	      conn.setRequestMethod(requestType);
	      //set header with authorization token:
	      if(accessToken != ""){
	    	  log.debug("Authorization: "+accessToken);
	       conn.setRequestProperty(authorization, accessToken);
	      }
	       log.info("request properties: " + conn.getRequestProperties());
	       StringBuilder result =	processResponse ( conn, true,false);
	       JSONObject jsonReply = toJson(result);
	       if (jsonReply.get("data") instanceof org.json.simple.JSONArray){
	    	   jsonReplyData = (JSONArray) jsonReply.get("data"); 
	       }else{
	    	   
			jsonReplyData.add(toJson(jsonReply.get("data")));
	       }

			return jsonReplyData;
	}
	
	@Step ("отправка DELETE запроса,url: {0}")
	public static JSONObject sendDeleteRequest(String urlToRead, String accessToken, Boolean requestIsPositive) {
		JSONObject jsonReplyData = new JSONObject();
		try {
			String requestType = "DELETE";
			URL url = new URL(urlToRead);
		
	      log.info("Sending 'DELTE' request to URL : " + urlToRead);
	      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	      conn.setRequestMethod(requestType);
	      //set header with authorization token:
	      if(accessToken != ""){
	       conn.setRequestProperty(authorization, accessToken);
	      }
	       log.info("request properties: " + conn.getRequestProperties());
	       StringBuilder result = processResponse ( conn, requestIsPositive,false);
	       JSONObject jsonReply = toJson(result);
	       
			if (jsonReply == null) {// something went wrong, nothing to return?
				assertTrue(false, "http ответ на DELETE запрос не содержит тела ответа");
				jsonReplyData = null;
			} else {

				if (requestIsPositive) {
					jsonReplyData = (JSONObject) jsonReply.get("data");
				} else {
					jsonReplyData = jsonReply;
				}
			}
		} catch (IOException e) {
			log.error(ERROR,e);
		}
			return jsonReplyData;
	}
	
	/**
	 * 
	 * @param urlToRead
	 * @param urlParameters
	 * @param accessToken
//	 * @param multipart
	 * @param files
	 * @param requestIsPositive
	 * @return
	 * @throws Exception
	 */

		@Step ("отправка POST запроса,url: {0}")
		public static ArrayList<org.json.simple.JSONObject> sendPostRequest(String urlToRead, String urlParameters, String accessToken,String contentTpe,HashMap files,Boolean requestIsPositive) {
			JSONObject jsonReply = null;
			ArrayList<org.json.simple.JSONObject> jsonReplyDataArr = new ArrayList<>();
			StringBuilder result = sendRequest(urlToRead, "POST", urlParameters, accessToken,contentTpe, files, requestIsPositive);

			if (result != null && (!result.toString().equals(""))) {
				jsonReply = toJson(result);
				if (jsonReply.containsKey("data") && !jsonReply.get("data").toString().equals("{}")) {
					if(jsonReply.get("data") instanceof ArrayList){
						jsonReplyDataArr = (JSONArray) jsonReply.get("data");
					}else if( jsonReply.get("data").equals("\"Пересчет интервалов закончен\"")){
						jsonReplyDataArr.add(jsonReply);
					}else{
						jsonReplyDataArr.add(toJson(jsonReply.get("data")));
					}
					return jsonReplyDataArr;
				}
			} else if (!requestIsPositive) {  //reply is empty but that was negative request\test
				jsonReplyDataArr.add(jsonReply);
				return jsonReplyDataArr;
			} 
			if(jsonReply==null){//something went wrong, nothing to return?
			assertTrue(false,"http ответ на POST запрос (позитивного теста) не содержит тела ответа");
			}
			
			jsonReplyDataArr.add(jsonReply);
			return jsonReplyDataArr;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Step ("отправка PUT запроса,url: {0}")
		public static ArrayList<org.json.simple.JSONObject> sendPutRequest(String urlToRead, String urlParameters, String accessToken,String contentTpe,HashMap files,Boolean requestIsPositive) {
			JSONObject jsonReply = null;
			ArrayList<org.json.simple.JSONObject> jsonReplyDataArr = new ArrayList();
			StringBuilder result = sendRequest(urlToRead, "PUT", urlParameters, accessToken,contentTpe, files, requestIsPositive);

			if (result != null && (!result.toString().equals(""))) {
				jsonReply = toJson(result);
				if (jsonReply.containsKey("data") && !jsonReply.get("data").toString().equals("{}")) {
					if(jsonReply.get("data") instanceof ArrayList){
						jsonReplyDataArr = (JSONArray) jsonReply.get("data");
					}else{
						jsonReplyDataArr.add(toJson(jsonReply.get("data")));
					}
					return jsonReplyDataArr;
				}
			} else if (!requestIsPositive) {  //reply is empty but that was negative request\test
				jsonReplyDataArr.add(jsonReply);
				return jsonReplyDataArr;
			} 
			if(jsonReply==null){//something went wrong, nothing to return?
			assertTrue(false,"http ответ на POST запрос (позитивного теста) не содержит тела ответа");
			}
			
			jsonReplyDataArr.add(jsonReply);
			return jsonReplyDataArr;
		}
/**
 * 
 * @param urlToRead
 * @param urlParameters
 * @param accessToken
 * @param multipart
 * @param requestIsPositive
 * @return
 * @throws Exception
 */

//	@Step ("отправка POST запроса,url: {0}")
	@SuppressWarnings("rawtypes")
	public static org.json.simple.JSONObject sendPostRequest(String urlToRead, String urlParameters, String accessToken,Boolean multipart,Boolean requestIsPositive) throws Exception {
		String contentType="";
		if (multipart)
			contentType =multipartStr;
		return sendPostRequest(urlToRead, urlParameters, accessToken, contentType, new HashMap(), requestIsPositive).get(0);
	}
	
	@SuppressWarnings("rawtypes")
	public static org.json.simple.JSONObject sendPutRequest(String urlToRead, String urlParameters, String accessToken,Boolean multipart,Boolean requestIsPositive) throws Exception {
		String contentType="";
		if (multipart)
			contentType =multipartStr;
		return sendPutRequest(urlToRead, urlParameters, accessToken, contentType, new HashMap(), requestIsPositive).get(0);
	}
	
	@SuppressWarnings("rawtypes")
	public static org.json.simple.JSONObject sendPostRequest(String urlToRead, String urlParameters, String accessToken,String contentType,Boolean requestIsPositive) throws Exception {
		return sendPostRequest(urlToRead, urlParameters, accessToken, contentType, new HashMap(), requestIsPositive).get(0);
	}

	public static ArrayList<org.json.simple.JSONObject> sendPostRequestArrRe(String urlToRead, String urlParameters, String accessToken,Boolean multipart,Boolean requestIsPositive) throws Exception {
		String contentType="";
		if (multipart)
			contentType =multipartStr;
		return sendPostRequest(urlToRead, urlParameters, accessToken, contentType, new HashMap(), requestIsPositive);
	}


	public static StringBuilder sendRequest(String urlToRead, String requestType, String urlParameters, String accessToken,String contentType, HashMap<String,String> files, Boolean requestIsPositive) {
		StringBuilder result = new StringBuilder();
		Boolean multipart =false;
		try {
			URL url = new URL(urlToRead);
			HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
			httpUrlConnection.setRequestMethod(requestType);
			httpUrlConnection.setUseCaches(false);
			httpUrlConnection.setDoOutput(true);
			httpUrlConnection.setRequestProperty("Accept-Charset", "UTF-8");
			httpUrlConnection.setRequestProperty("Accept", "*/*");
			httpUrlConnection.setConnectTimeout(Integer.parseInt(getProperty(httpConnectTimeout))); //30secs
			httpUrlConnection.setReadTimeout   (Integer.parseInt(getProperty(httpReadTimeout)));	   //29 secs   
			httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
			// Send post request
			httpUrlConnection.setFollowRedirects(false);
			if (!(accessToken.isEmpty() || accessToken.equals(""))) {
				httpUrlConnection.setRequestProperty(authorization, accessToken);
			}
			if(contentType.equals(multipartStr)){
				multipart = true;
				httpUrlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			}else if(contentType.equals("application/json")){
				httpUrlConnection.setConnectTimeout(Integer.parseInt(getProperty(httpConnectTimeout))); //30secs
				httpUrlConnection.setReadTimeout   (Integer.parseInt(getProperty(httpReadTimeout)));	   //29 secs   
				httpUrlConnection.setRequestProperty("Content-Type", "application/json");
				}
			try (DataOutputStream request = new DataOutputStream(httpUrlConnection.getOutputStream())) {
				if(contentType.equals(multipartStr)){//
					if(files.keySet()!=null){
						
						request.writeBytes(twoHyphens + boundary + crlf);
						request.writeBytes("Content-Disposition: form-data; name=\"data\"" + crlf+crlf);
						request.write(urlParameters.getBytes());
						request.writeBytes(crlf);
						for(String name:files.keySet()){
						String path =	files.get(name);
						addUploadingFileToPost(request, name,  path);
						}
				}
					else {
//					Start content wrapper:
						request.writeBytes(twoHyphens + boundary + crlf);
						request.writeBytes("Content-Disposition: form-data; name=\"data\"" + crlf+crlf+urlParameters);
						request.writeBytes(crlf);
					}
						request.writeBytes(crlf);
						request.writeBytes(twoHyphens + boundary + 
						    twoHyphens + crlf);
//						Flush output buffer:
					}
					else{
						request.writeBytes(urlParameters);
					}
				request.flush();
			} catch (Exception e) {
				log.error(failedDueTo+e);
				assertTrue(false,e.toString());
			}
			result = processResponse(httpUrlConnection, requestIsPositive,multipart);
		} catch (IOException e) {
			log.error(failedDueTo+e);
			assertTrue(false,e.toString());
		}
		return result;
	}

	/**
	 * 
//	 * @param urlToRead
//	 * @param urlParameters
//	 * @param accessToken
//	 * @param filePath
//	 * @param url
//	 * @param requestIsPositive
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */

	private static String getPostParams(HttpURLConnection conn) {
		String posterParams = "";
		try {

			// if there exists a beter way to get post values we really send,
			// just fix it!
			Field posterField = conn.getClass().getDeclaredField("poster");
			posterField.setAccessible(true);
			if (posterField.get(conn) == null) {
				Field httpField = conn.getClass().getDeclaredField("http");
				httpField.setAccessible(true);
				HttpClient httpClient = (HttpClient) httpField.get(conn);
				if (httpClient != null) {
					Field httpPosterField = httpClient.getClass().getDeclaredField("poster");
					httpPosterField.setAccessible(true);
					if (httpPosterField.get(httpClient) != null) {
						posterParams = httpPosterField.get(httpClient).toString();
					}
				}
			} else {
				posterParams = posterField.get(conn).toString();
			}

			if (!(posterParams == "" || posterParams.isEmpty()) && posterParams.contains("{") ) {
				if(posterParams.indexOf("Content-Type: image")>0){
					posterParams=	withoutfileContentFromParams(posterParams);
				}else {
					posterParams = posterParams.substring(posterParams.indexOf('{'), posterParams.lastIndexOf('}') + 1);
				}
			}
			Field userHeadersField = conn.getClass().getDeclaredField("userHeaders");
			userHeadersField.setAccessible(true);
			String auth = userHeadersField.get(conn).toString();
			if (auth != null && auth.contains(authorization)) {
				auth = auth.substring(auth.indexOf(authorization), auth.lastIndexOf('}'));
				posterParams += "\r\n" + auth;
			}
		} catch (Exception e) {
			log.error("failed due to :" + e);
			assertTrue(false,e.toString());
			// TODO: handle exception
		}
		return posterParams;
	}

	/**
	 * 
	 * @param conn
	 * @param requestIsPositive
	 * @param multipart
	 * @return
	 */
	public synchronized static StringBuilder processResponse (HttpURLConnection conn,Boolean requestIsPositive, Boolean multipart){

		StringBuilder result = new StringBuilder();
		String connResponseMessage="";
		String connRequestMethod="";
		int responseCode =-1;
		String posterParams="";
	    try {
	    	posterParams= getPostParams(conn);

	   // multiple line breaks to single one
	    posterParams  =posterParams.replaceAll("[\r\n]+", "\r\n");
		addPostRequestDetailsToAllureReport(posterParams.replace("\",\"", "\",\r\n\""));
	    	connResponseMessage = conn.getResponseMessage();
	    	connRequestMethod = conn.getRequestMethod();
	    	responseCode = conn.getResponseCode();
    			if(connRequestMethod.equals("POST") || connRequestMethod.equals("PUT")){
    				if(multipart){
    					log.info("request method : " + connRequestMethod+" multipart");
    				}else {
    					log.info("request method : " + connRequestMethod);
    				}
    			
    			log.info("Sending '"+connRequestMethod+"' request to URL : " + conn.getURL());
   	    	 log.info("Post parameters : " + posterParams);
    		}
    		if(connRequestMethod.equals("DELETE")){
    			log.info("Post parameters : " + posterParams);
    		}	
	  	      log.info("HTTP Response Code : " + responseCode);
	  	      log.info("HTTP response message:"+ connResponseMessage);
	  	    InputStream inputStream;
			if(responseCode !=200){
	  	    	inputStream = conn.getErrorStream();
	  	    }else{
	  	    	inputStream = conn.getInputStream();
	  	    }
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(inputStream,"UTF-8"));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				result.append(inputLine);
			}
			in.close();

			String resultString = result.toString();
			log.info("response body:"+ resultString);
			textToAllureRep("ответ WebAPI","Code : " + responseCode+", body: "+ resultString);	
	    	if (requestIsPositive){
	    		assertEquals("responseCode:200",responseCode, 200, "unexpected response code, reply:" + result.toString());
				assertEquals("connResponseMessage:OK",connResponseMessage, "OK",
						"unexpected response message, reason:" + result.toString());
				if ( result.toString().contains("Пересчет интервалов")){
					JSONObject dataObj = new JSONObject();
					dataObj.put("data", result.toString());
					result = new StringBuilder(dataObj.toString());
				}else{
				String resultCode = toJson(result).get("code").toString();
				assertTrue("resultCode:Ok||NeedConfirm",resultCode.equals("Ok") ||resultCode.equals("NeedConfirm") ,"unexpected response body code:\""+toJson(result).get("code")+"\". Message: \""+toJson(result).get("message")+"\", reason:");
				}
	    	}
			assertNotNull("response body ",resultString,"response reply has no data");
	    } catch (Exception e) {
	    	setToAllureChechkedFields();
	    	assertTrue(false,"ошибка при получении ответа от WebAPI,Запрос:"+connRequestMethod+"\r\n параметры:"+posterParams
	    						+"\r\nОтвет: Code : " + responseCode+", message: "+ connResponseMessage+" " + e.toString());
	    	log.error("failed due to :"+e);
	    }
		return result;
	   
	}

	private static void addPostRequestDetailsToAllureReport(String urlParameters) {
		if (!(urlParameters == null || urlParameters.isEmpty())) {
			String attachName = " параметры запроса:";
			textToAllureRep(attachName, urlParameters);
		}
	}

	private static void addGetRequestDetailsToAllureReport(String login, String urlParameters) {
		String attachName = "";
		if (!(login == null || login.isEmpty())) {
			attachName = "отправка запроса от пользователя: '" + login + "',";
			textToAllureRep(attachName, login);
		}

		if (!(urlParameters == null || urlParameters.isEmpty())) {
			attachName = " параметры запроса:";
			textToAllureRep(attachName, urlParameters);
		}
	}


	
	public static DataOutputStream addUploadingFileToPost(DataOutputStream request, String name, String path) {
		String fileName = path.substring(path.lastIndexOf('\\')+1, path.length());
		try {
			request.writeBytes(twoHyphens + boundary + crlf);

			request.writeBytes("Content-Disposition: form-data; name=\"" + name + "\"; filename=\""+fileName+"\"" + crlf);
			request.writeBytes("Content-Type: image/jpeg" + crlf + crlf);
			Path paths = Paths.get(path);
			byte[] data = Files.readAllBytes(paths);
			request.write(data);
			request.writeBytes(crlf);

		} catch (IOException e) {
			log.error(ERROR,e);
		}
		return request;
	}
	
public static String withoutfileContentFromParams(String posterParams){
	if(posterParams.contains(contentTypeImage)){
    List<Integer> contentDispositionlist = new ArrayList();
    contentDispositionlist.add(0);
    List<Integer> contentTypelist = new ArrayList();
	for (int index = posterParams.indexOf("Content-Disposition");
		     index >= 0;
		     index = posterParams.indexOf("Content-Disposition", index + 1))
		{
		contentDispositionlist.add(index);
		}
	for (int index = posterParams.indexOf(contentTypeImage);
		     index >= 0;
		     index = posterParams.indexOf(contentTypeImage, index + 1))
		{
		contentTypelist.add(index);
		}
	for(int i=1;i<contentDispositionlist.size();){
		if(contentDispositionlist.get(i) < contentTypelist.get(0)){
			contentDispositionlist.remove(i);
			
		}else {
			i++;
			}
	}
	StringBuilder newposterParams=new StringBuilder();
	for(int i=0; i<contentTypelist.size();i++){
		newposterParams.append( posterParams.substring(contentDispositionlist.get(i), contentTypelist.get(i)+25)+"<actual file content, not shown here>\r\n");
	}
	
	return newposterParams.toString();
	} else{
		return posterParams;
	}
	}



}
