package com.t1.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

public abstract class Asserts{

	protected Asserts(){}
	
	protected static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("Validation");
	protected static String standartTimeFormat = "yyyy-MM-dd HH:mm:ss";
	protected static TimeZone utc3TimeZone = TimeZone.getTimeZone("UTC+3:00");
	private static String assertErrStr = "Ошибка проверки: ";
	private static String errStr = "Error";
	protected static String equalsToStr = "equals to ";
	//asserts that accumulate list of checked fields
	public static void assertTrue(String field, boolean condition, String message) {
		try {
			org.junit.Assert.assertTrue(message, condition);
			SharedContainer.addCheckedFieldToContainer(field, condition + "");
		} catch ( AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertTrue(String field, boolean condition) {
		try {
			assertTrue(condition);
			SharedContainer.addCheckedFieldToContainer(field, condition + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertFalse(String field, boolean condition, String message) {
		try {
			assertFalse(condition, message);
			SharedContainer.addCheckedFieldToContainer(field, condition + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertFalse(String field, boolean condition) {
		try {
			assertFalse(condition);
			SharedContainer.addCheckedFieldToContainer(field, condition + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Object actual, Object expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected.toString());
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, int actual, int expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, String actual, String expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Object actual, Object expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected.toString());
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, double actual, double expected, double delta, String message) {
		try {
			assertEquals(actual, expected, delta, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, double actual, double expected, double delta) {
		try {
			assertEquals(actual, expected, delta);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, float actual, float expected, float delta, String message) {
		try {
			assertEquals(actual, expected, delta, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, float actual, float expected, float delta) {
		try {
			assertEquals(actual, expected, delta);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, long actual, long expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, long actual, long expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, boolean actual, boolean expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, boolean actual, boolean expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, byte actual, byte expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, byte actual, byte expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, char actual, char expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, char actual, char expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, short actual, short expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, short actual, short expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, int actual, int expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Collection<?> actual, Collection<?> expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Collection<?> actual, Collection<?> expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Iterator<?> actual, Iterator<?> expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Iterator<?> actual, Iterator<?> expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Iterable<?> actual, Iterable<?> expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Iterable<?> actual, Iterable<?> expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertArrayEquals(String field, Object[] actual, Object[] expected, String message) {
		try {
			assertArrayEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field,  expected);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}
	
	public static void assertArrayEqualsAnyOrder(String field, Object[] actual, Object[] expected, String message) {
		try {
			Arrays.sort(actual);
			Arrays.sort(expected);
			assertArrayEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field,  expected);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertArrayEquals(String field, Object[] actual, Object[] expected) {
		try {
			assertArrayEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, Arrays.toString(expected));
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Set<?> actual, Set<?> expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Set<?> actual, Set<?> expected, String message) {
		try {
			assertEquals(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertEquals(String field, Map<?, ?> actual, Map<?, ?> expected) {
		try {
			assertEquals(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNotEquals(String field, float actual1, float actual2, float delta, String message) {
		try {
			assertNotEquals(actual1, actual2, delta, message);
			SharedContainer.addCheckedFieldToContainer(field, actual1 + "!=" + actual2);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNotEquals(String field, float actual1, float actual2, float delta) {
		try {
			assertNotEquals(actual1, actual2, delta);
			SharedContainer.addCheckedFieldToContainer(field, actual1 + "!=" + actual2);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNotEquals(String field, double actual1, double actual2, double delta, String message) {
		try {
			assertNotEquals(actual1, actual2, delta, message);
			SharedContainer.addCheckedFieldToContainer(field, actual1 + "!=" + actual2);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNotEquals(String field, double actual1, double actual2, double delta) {
		try {
			assertNotEquals(actual1, actual2, delta);
			SharedContainer.addCheckedFieldToContainer(field, actual1 + "!=" + actual2);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNotNull(String field, Object object) {
		try {
			assertNotNull(object);
			SharedContainer.addCheckedFieldToContainer(field, object.toString());
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNotNull(String field, Object object, String message) {
		try {
			assertNotNull(object, message);
			int lastChar = (object.toString().length() >80)? 80 : object.toString().length();
			if(lastChar<80){
				SharedContainer.addCheckedFieldToContainer(field+" is not null; ", object.toString());
			}else {
				SharedContainer.addCheckedFieldToContainer(field+" is not null; ", object.toString().substring(0, lastChar)+"...");	
			}
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNull(String field, Object object) {
		try {
			assertNull(object);
			SharedContainer.addCheckedFieldToContainer(field, object.toString());
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNull(String field, Object object, String message) {
		try {
			assertNull(object, message);
			SharedContainer.addCheckedFieldToContainer(field, object.toString());
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertSame(String field, Object actual, Object expected, String message) {
		try {
			assertSame(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected.toString());

		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertSame(String field, Object actual, Object expected) {
		try {
			assertSame(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected.toString());
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNotSame(String field, Object actual, Object expected, String message) {
		try {
			assertNotSame(actual, expected, message);
			SharedContainer.addCheckedFieldToContainer(field, expected.toString());
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertNotSame(String field, Object actual, Object expected) {
		try {
			assertNotSame(actual, expected);
			SharedContainer.addCheckedFieldToContainer(field, expected.toString());
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertContains(String field, String actual, String expectedPart, String message) {
		try {
			assertTrue(actual.contains(expectedPart),message);
			SharedContainer.addCheckedFieldToContainer(field, "contains:" + expectedPart);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertContains(String field, List actualList, String expectedPart, String message) {
		try {
			for (int i=0;i<actualList.size();i++){
				if(actualList.get(i).equals(expectedPart)){
					assertEquals(actualList.get(i), expectedPart,message);
					break;
				}
			}
			SharedContainer.addCheckedFieldToContainer(field, "contains:" + expectedPart);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}
	

	public static void assertContainsMngId(String field, String actual, String message) {
		String checkForHexRegExp = "^[a-f0-9]{24}$";
		try {
			assertTrue(actual.matches(checkForHexRegExp), message);
			SharedContainer.addCheckedFieldToContainer(field, "is ID:" + actual);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}
	
	public static void assertContainsGUID(String field, String actual, String message) {
		String checkForHexRegExp = "^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$";
		try {
			assertTrue(actual.matches(checkForHexRegExp), message);
			SharedContainer.addCheckedFieldToContainer(field, "is GUID:" + actual);
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}
	
	
	public static void assertDateIsBetween(String field, String actualValue, String from, String till, String message) {
		DateFormat dfmDateTime = new SimpleDateFormat(standartTimeFormat);
		DateFormat dfmDate = new SimpleDateFormat(standartTimeFormat);
		long actualValueUnix = 0;		
		long fromUnix = 0;		
		long tillUnix = 0;

		dfmDateTime.setTimeZone(utc3TimeZone);
		dfmDate.setTimeZone(utc3TimeZone);// Specify your timezone
		try {
			if (actualValue.length() <= 10) {
				actualValueUnix = dfmDate.parse(actualValue).getTime();
			} else {
				actualValueUnix = dfmDateTime.parse(actualValue).getTime();
			}
			if (from.length() <= 10) {
				fromUnix = dfmDate.parse(from).getTime();
			} else {
				fromUnix = dfmDateTime.parse(from).getTime();
			}
			if (till.length() <= 10) {
				tillUnix = dfmDate.parse(till).getTime();
			} else {
				tillUnix = dfmDateTime.parse(till).getTime();
			}

			assertTrue(fromUnix < actualValueUnix && actualValueUnix < tillUnix, message);
			SharedContainer.addCheckedFieldToContainer(field,
					equalsToStr + actualValue + ",and is between:" + from + " and " + till);
		} catch (java.text.ParseException e) {
			log.error(errStr, e);
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
		} catch (AssertionError e) {
			log.error(errStr, e);
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}
	
	public static void assertDateIsBefore(String field, String actualValue, String till, String message) {
		DateFormat dfmDateTime = new SimpleDateFormat(standartTimeFormat);
		DateFormat dfmDate = new SimpleDateFormat(standartTimeFormat);
		long actualValueUnix = 0;		
		long tillUnix = 0;

		dfmDateTime.setTimeZone(utc3TimeZone);
		dfmDate.setTimeZone(utc3TimeZone);// Specify your timezone
		try {
			if (actualValue.length() <= 10) {
				actualValueUnix = dfmDate.parse(actualValue).getTime();
			} else {
				actualValueUnix = dfmDateTime.parse(actualValue).getTime();
			}
			if (till.length() <= 10) {
				tillUnix = dfmDate.parse(till).getTime();
			} else {
				tillUnix = dfmDateTime.parse(till).getTime();
			}

			assertTrue(actualValueUnix < tillUnix, message);
			SharedContainer.addCheckedFieldToContainer(field, equalsToStr + actualValue + ",and is before:" + till);
		} catch (java.text.ParseException e) {
			log.error(errStr, e);
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();

		} catch (AssertionError e) {
			log.error(errStr, e);
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}
	
	public static void assertDateIsBeforeOrSame(String field, String actualValue, String till, String message) {
		DateFormat dfmDateTime = new SimpleDateFormat(standartTimeFormat);
		DateFormat dfmDate = new SimpleDateFormat(standartTimeFormat);
		long actualValueUnix = 0;		
		long tillUnix = 0;

		dfmDateTime.setTimeZone(utc3TimeZone);
		dfmDate.setTimeZone(utc3TimeZone);// Specify your timezone
		try {
			if (actualValue.length() <= 10) {
				actualValueUnix = dfmDate.parse(actualValue).getTime();
			} else {
				actualValueUnix = dfmDateTime.parse(actualValue).getTime();
			}
			if (till.length() <= 10) {
				tillUnix = dfmDate.parse(till).getTime();
			} else {
				tillUnix = dfmDateTime.parse(till).getTime();
			}
			log.info("added 5 secs to till dateTime in case time difference on local machines");
			assertTrue(actualValueUnix <= tillUnix + 5000, message);
			SharedContainer.addCheckedFieldToContainer(field, equalsToStr + actualValue + ",and is before:" + till);
		} catch (java.text.ParseException e) {
			log.error(errStr, e);
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();

		} catch (AssertionError e) {
			log.error(errStr, e);
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	public static void assertDateIsAfter(String field, String actualValue, String from, String message) {
		DateFormat dfmDateTime = new SimpleDateFormat(standartTimeFormat);
		DateFormat dfmDate = new SimpleDateFormat(standartTimeFormat);
		long actualValueUnix = 0;		
		long fromUnix = 0;

		dfmDateTime.setTimeZone(utc3TimeZone);
		dfmDate.setTimeZone(utc3TimeZone);// Specify your timezone
		try {
			if (actualValue.length() <= 10) {
				actualValueUnix = dfmDate.parse(actualValue).getTime();
			} else {
				actualValueUnix = dfmDateTime.parse(actualValue).getTime();
			}
			if (from.length() <= 10) {
				fromUnix = dfmDate.parse(from).getTime();
			} else {
				fromUnix = dfmDateTime.parse(from).getTime();
			}
			assertTrue(fromUnix < actualValueUnix, message);
			SharedContainer.addCheckedFieldToContainer(field, equalsToStr + actualValue + ",and is after:" + from);
		} catch (java.text.ParseException e) {
			log.error(errStr, e);
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();

		} catch (AssertionError e) {
			log.error(errStr, e);
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}
	
	
	//standart overloaded asserts
	protected static void assertTrue(boolean condition, String message) {
		try {
//			org.junit.Assert.assertTrue(message, condition);
			SharedContainer.addCheckedFieldToContainer("assert condition", condition + "");
		} catch (AssertionError e) {
			com.t1.core.AbstractClass.textToAllureRep(assertErrStr, e.toString());
			AbstractClass.setToAllureChechkedFields();
			throw e;
		}
	}

	private static void assertTrue(boolean condition) {
		org.junit.Assert.assertTrue(condition);
	}

	private static void assertFalse(boolean condition, String message) {
		org.junit.Assert.assertFalse(message, condition);
	}

	private static void assertFalse(boolean condition) {
		org.junit.Assert.assertFalse(condition);
	}

	private static void assertEquals(Object actual, Object expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(int actual, int expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(String actual, String expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(Object actual, Object expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(String actual, String expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(double actual, double expected, double delta, String message) {
		org.junit.Assert.assertEquals(message, expected, actual, delta);
	}

	private static void assertEquals(double actual, double expected, double delta) {
		org.junit.Assert.assertEquals(expected, actual, delta);
	}

	private static void assertEquals(float actual, float expected, float delta, String message) {
		org.junit.Assert.assertEquals(message, expected, actual, delta);
	}

	private static void assertEquals(float actual, float expected, float delta) {
		org.junit.Assert.assertEquals(expected, actual, delta);
	}

	private static void assertEquals(long actual, long expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(long actual, long expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(boolean actual, boolean expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(boolean actual, boolean expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(byte actual, byte expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(byte actual, byte expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(char actual, char expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(char actual, char expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(short actual, short expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(short actual, short expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(int actual, int expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertNotNull(Object object) {
		org.junit.Assert.assertNotNull(object);
	}

	private static void assertNotNull(Object object, String message) {
		org.junit.Assert.assertNotNull(message, object);
	}

	private static void assertNull(Object object) {
		org.junit.Assert.assertNull(object);
	}

	private static void assertNull(Object object, String message) {
		org.junit.Assert.assertNull(message,object);
	}

	private static void assertSame(Object actual, Object expected, String message) {
		org.junit.Assert.assertSame(message,actual,expected);
	}

	private static void assertSame(Object actual, Object expected) {
		org.junit.Assert.assertSame(actual,expected);
	}

	private static void assertNotSame(Object actual, Object expected, String message) {
		org.junit.Assert.assertNotSame(message,actual,expected);
	}

	private static void assertNotSame(Object actual, Object expected) {
		org.junit.Assert.assertNotSame(actual,expected);
	}

	private static void assertEquals(Collection<?> actual, Collection<?> expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(Collection<?> actual, Collection<?> expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(Iterator<?> actual, Iterator<?> expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(Iterator<?> actual, Iterator<?> expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(Iterable<?> actual, Iterable<?> expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(Iterable<?> actual, Iterable<?> expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertArrayEquals(Object[] actual, Object[] expected, String message) {
		org.junit.Assert.assertArrayEquals(message, expected, actual);
	}


	private static void assertArrayEquals(Object[] actual, Object[] expected) {
		org.junit.Assert.assertArrayEquals(expected, actual);
	}


	private static void assertEquals(Set<?> actual, Set<?> expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertEquals(Set<?> actual, Set<?> expected, String message) {
		org.junit.Assert.assertEquals(message, expected, actual);
	}

	private static void assertEquals(Map<?, ?> actual, Map<?, ?> expected) {
		org.junit.Assert.assertEquals(expected, actual);
	}

	private static void assertNotEquals(float actual1, float actual2, float delta, String message) {
		org.junit.Assert.assertNotEquals(message, actual1, actual2, delta);
	}

	private static void assertNotEquals(float actual1, float actual2, float delta) {
		org.junit.Assert.assertNotEquals(actual1, actual2, delta);
	}

	private static void assertNotEquals(double actual1, double actual2, double delta, String message) {
		org.junit.Assert.assertNotEquals(message, actual1, actual2, delta);
	}

	private static void assertNotEquals(double actual1, double actual2, double delta) {
		org.junit.Assert.assertNotEquals(actual1, actual2, delta);
	}

	public static void compareDateTimeUpTo1min(String field, String mongo, String api, long currentDateTime){
		DateFormat dfm = new SimpleDateFormat(standartTimeFormat);  
		dfm.setTimeZone(utc3TimeZone);
	    long unixApi = 0;
	    if(api != null){
		 try{ 
			 unixApi = dfm.parse(api).getTime();  
		    } 
		    catch (java.text.ParseException e) {
		    	log.error(errStr,e);
		    }
		 assertTrue ("API:"+field,(unixApi - currentDateTime <30000)   ||(unixApi - currentDateTime >-30000),"некорректное время"+field+",отличие:"+(unixApi - currentDateTime));
	    }
		long unixMongo = Long.parseLong(mongo);
		assertTrue ("DB:"+field,(unixMongo - currentDateTime <10000) ||(unixMongo - currentDateTime >-10000),"некорректное время"+field+",отличие:"+(unixMongo - currentDateTime));
	}

	public static void assertTimeEqualsApiVsMongo(String field, String mongo, String api){
		DateFormat df = new SimpleDateFormat(standartTimeFormat);
		df.setTimeZone(utc3TimeZone);
		assertEquals(field, df.format(Long.parseLong(mongo)),api, "Разница по времени более 1 сек!");
	}

}
