package com.t1.core;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;

public class ReplytoHashMap extends AbstractClass{

	public static HashMap documentToHashMap(Document document) {
		try {
		return
		        new ObjectMapper().readValue(document.toJson(), HashMap.class);
		}
		catch(Exception e) {
			log.error(ERROR,e);
    		return null;
        }
	}
	
	public static List<HashMap> findIterableDocumentToHashMap(FindIterable<Document> response) {
		
		List<HashMap> results =  new ArrayList<>();
		for (Document document : response) {
        	results.add(ReplytoHashMap.documentToHashMap(document));
	        }
		
		return results;
	}

	
/*	public static HashMap getFieldsFromSubDocument(Document document, String subdocumentName) {
		try {
	
			ArrayList tmparr = (ArrayList) document.get(subdocumentName);
			 Document tmpdoc = (Document) tmparr.get(0);
			 return new ObjectMapper().readValue(tmpdoc.toJson(), HashMap.class);
				}
				catch(Exception e) {
					log.error(ERROR,e);
		    		return null;
		        }
	}
	
	public static HashMap subDocumentToHashMap(Document document) {
try {
			
			ArrayList fff = (ArrayList) document.get("Contacts");
			 Document ggg = (Document) fff.get(0);
			 
			 JSONParser parser = new JSONParser();
				JSONObject resultObject = (JSONObject) parser.parse(ggg.toJson());
				HashMap someVarzz = new ObjectMapper().readValue(ggg.toJson(), HashMap.class);
				return someVarzz;
				}
				catch(Exception e) {
					log.error(ERROR,e);
		    		return null;
		        }
	}
	
	
	@Deprecated
	public static HashMap JSONtoHashMap(Document document) throws Exception{
		HashMap<String, Object> resultMap=new HashMap<String, Object>();
		 JsonFactory factory = new JsonFactory();

	        ObjectMapper mapper = new ObjectMapper(factory);
	        JsonNode rootNode = mapper.readTree(JSON.serialize(document));  
	        
	      resultMap = mapper.convertValue(rootNode, HashMap.class);
	        return resultMap;
		
		
	}
	*/
	@SuppressWarnings("unchecked")
	public static List<HashMap<String, Object>> jsonStringtoHashMap(String JSONStringReply)
			throws ParseException, IOException {
		log.debug("start JSONStringtoHashMap");
		HashMap<String, Object> dataHashMap = new HashMap<>();
		List<HashMap<String, Object>> dataArray = new ArrayList<>();
		// get only data from reply
		JSONParser parser = new JSONParser();
		JSONObject resultObject = (JSONObject) parser.parse(JSONStringReply);
		if (resultObject.containsKey("data")) {
			if (resultObject.get("data") instanceof org.json.simple.JSONArray) {
				JSONArray resultArray = (JSONArray) parser.parse(resultObject.get("data").toString());
				for (int i = 0; i < resultArray.size() - 1; i++) {
					dataHashMap = new ObjectMapper().readValue(resultArray.get(i).toString(), HashMap.class);
					dataArray.add(dataHashMap);
				}
			}
			else {
				String resultObjData = resultObject.get("data").toString();
				dataHashMap = new ObjectMapper().readValue(resultObjData, HashMap.class);
				dataArray.add(dataHashMap);
			}
		} else {
			dataArray.add(dataHashMap);
		}
		return dataArray;
	}

}
