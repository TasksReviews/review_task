package cucumber_test_runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

// Чтобы запустить файл в maven - достаточно написать в консоли mvn clean test
// Результаты теста можно найти в файле target/site/allure-maven.html
// Для того, чтобы отобразить результат на локальной машине в браузере необходимо установить Allure по ссылке https://docs.qameta.io/allure/

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/xrayFeatures/",
        plugin = {"html:target/cucumber-reports/html/", "junit:target/cucumber-reports/junit.xml",
                "json:target/cucumber-reports/jsonreport.json"},
        glue = {"classpath:cucumber_test_runner","classpath:YandexTask"})
public class TestRunner {

}
