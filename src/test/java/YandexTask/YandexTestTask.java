package YandexTask;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

public class YandexTestTask {

    private WebDriver driver;

    public YandexTestTask() {
        // Также можно перенести в файл
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\malinovskiyv\\Documents\\git\\review_task\\chromedriver_win32\\chromedriver.exe");
    }

    // Метод, который отображает результат в Allure reports
    @Step
    @Given("^Открыть сайт \"([^\"]*)\"$")
    public void открытьСайт(String arg0) {
        driver = new ChromeDriver();
        driver.get(arg0);
    }

    // Метод, который отображает результат в Allure reports
    @Step
    @And("^Ввести строку = \"([^\"]*)\" для поиска$")
    public void ввестиСтрокуДляПоиска(String arg0) {
        findInSite(arg0);
    }

    // Метод, который отображает результат в Allure reports
    @Attachment
    private WebElement findInSite(String arg0) {
        WebElement element = null;
        try {
            element = driver.findElement(By.name("text"));
            element.sendKeys(arg0);
            driver.findElement(By.cssSelector(".suggest2-form__button")).click();
        } catch (NullPointerException e) {
            Logger.getLogger(">>>").error(e.getMessage());
        }
        return element;
    }

    // Метод, который отображает результат в Allure reports
    @Step
    @Then("^Проверить, что в списке результатов есть сайт \"([^\"]*)\"$")
    public void проверитьЧтоВСпискеРезультатовЕстьСайт(String arg0) {
        getAnswer(arg0);
    }

    @Attachment
    private boolean getAnswer(String arg0) {
        try {
            Assert.assertEquals("Проверка найденных ресурсов: ", driver.getPageSource().contains(arg0), true);
            //TODO другие проверки
            driver.close();
            return true;
        } catch (AssertionError error) {
            Logger.getLogger(">>>").error(error.getMessage());
            driver.close();
            return false;
        }
    }
}
