Feature: Проверка работы кнопки поиска в Yandex

    #Файл запускается как JUNIT Test

  Scenario: Проверка работы кнопки поиска в Yandex
    #Первый шаг открывает браузер и перходит по переданной в параметре "параметр"
    Given Открыть сайт "https://yandex.ru/"
    #Второй шаг передает в браузер параметр "параметр"
    And Ввести строку = "что такое CSS selector" для поиска
    #Третий шаг проверяет, что среди найденных ресурсов есть ссылка на "параметр"
    Then Проверить, что в списке результатов есть сайт "htmlbook.ru"
